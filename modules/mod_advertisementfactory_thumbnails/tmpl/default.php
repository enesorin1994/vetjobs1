<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="mod_advertisementfactory_thumbnails<?php echo $moduleclass_sfx; ?> <?php echo $modules ? 'mod_advertisementfactory_thumbnails_debug' : ''; ?>">

    <table>
        <?php for ($i = 0; $i < $rows; $i++): ?>
            <?php if (!isset($ads[$i * $columns]) && !$showempty) break; ?>
            <tr>
                <?php for ($j = 0; $j < $columns; $j++): ?>
                    <td style="height: <?php echo $height; ?>px; width: <?php echo $width; ?>px;">
                        <?php if (isset($ads[$i * $columns + $j])): $ad = $ads[$i * $columns + $j]; ?>
                            <a href="<?php echo $ad->redirectLink; ?>" <?php echo $target; ?>>
                                <img src="<?php echo $folder; ?>/<?php echo $ad->filename; ?>"/>
                            </a>
                        <?php elseif ($showempty): ?>
                            <a href="<?php echo $your_link; ?>">
                                <?php echo JText::_('MOD_ADVERTISEMENTFACTORY_THUMBNAILS_YOUR_LINK_HERE'); ?>
                            </a>
                        <?php endif; ?>
                    </td>
                <?php endfor; ?>
            </tr>
        <?php endfor; ?>
    </table>

    <?php if ($modules): ?>
        <div class="mod_advertisementfactory_thumbnails_debug_box">
            <div><?php echo $module->title; ?></div>
        </div>
    <?php endif; ?>

</div>
