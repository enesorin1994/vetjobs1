<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

jimport('joomla.application.component.model');
JLoader::discover('Factory', JPATH_ADMINISTRATOR . '/components/com_advertisementfactory/lib');
JModelLegacy::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'models');

$rows = $params->get('rows', 2);
$columns = $params->get('columns', 2);
$modules = JFactory::getApplication()->input->getInt('modules');
$height = $params->get('height', 50);
$width = $params->get('width', 50);
$mode = $params->get('display', 1) ? 'vertical' : 'horizontal';
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$folder = JURI::root() . 'components/com_advertisementfactory/storage/images';
$showempty = $params->get('showempty', 1);
$your_link = JRoute::_('index.php?option=com_advertisementfactory&view=purchasetypes&type=thumbnail&module_id=' . $module->id, false);
$settings = JComponentHelper::getParams('com_advertisementfactory');
$target = $settings->get('ads_open_new_window', 0) ? 'target="_blank"' : '';

// Check if ad type is enabled.
if (!in_array('thumbnail', $settings->get('enabled_ad_types', array()))) {
    return false;
}

if ($modules) {
    $ads = array();
    $params->set('showempty', 1);
} else {
    $model = JModelLegacy::getInstance('ServeAd', 'AdvertisementFactoryModel');
    $ads = $model->getAds('thumbnail', array('module_id' => $module->id, 'limit' => $rows * $columns));
}

JHtml::_('stylesheet', 'modules/mod_advertisementfactory_thumbnails/template.css');

require JModuleHelper::getLayoutPath('mod_advertisementfactory_thumbnails', $params->get('layout', 'default'));
