<?php
/**------------------------------------------------------------------------
mod_jobsfactorytree - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

$show_counter = (int) $params->get('category_counter', 0);
$lang=JFactory::getLanguage();
$lang->load('mod_jobsfactory');

require_once (dirname(__FILE__).DS.'helper.php');
$force_itemid   = intval( $params->get( 'force_itemid', null ) );
if (!$force_itemid){
    require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."helpers".DS."tools.php");
    $force_itemid =JobsHelperTools::getMenuItemByTaskName(array("task"=>"listjobs"));
}
// Load mooTools
JHtml::_('behavior.framework', true);

$jdoc = JFactory::getDocument();
$jdoc->addStyleSheet(JURI::root(). 'modules/mod_jobsfactorytree/js/mootree.css');
$jdoc->addScript(JURI::root(). 'modules/mod_jobsfactorytree/js/mootree.js');
$jdoc->addScriptDeclaration("
    var tree;
    window.addEvent('domready', function(){
    	tree = new MooTreeControl({
    		div: 'mytree',
    		mode: 'folders',
            theme: '".JURI::root(). "/modules/mod_jobsfactorytree/js/mootree.gif',
    		grid: true,
    		onSelect: function(node, state) {
    			if (state) {
    			 if ( (node.data != undefined) && (node.data.url != undefined) ) {
    			     if ((node.data.target != undefined) && node.data.target == '_blank') {
    			         window.open(node.data.url);
			         }else{
			             window.location = node.data.url;
                     }
			     }else{
			         node.toggle(false);
                 }
                }
    		}
    	},{
    		text: '".JText::_("MOD_JOBS_JOB_CATEGORIES")."',
    		open: true
    	});
        tree.adopt('mod_jobsfactorytree_ul');
    });
");

$cats = modJobsCategoryTreeHelper::getCategories();

require JModuleHelper::getLayoutPath('mod_jobsfactorytree', $params->get('layout', 'default'));
