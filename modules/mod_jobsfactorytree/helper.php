<?php
/**------------------------------------------------------------------------
mod_jobsfactorytree - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class modJobsCategoryTreeHelper
{
    static function getCategories($categoryID = 0)
    {
        $database = JFactory::getDbo();
        $database->setQuery("
            SELECT c.id,c.catname, count(distinct a.id) nr_a
            FROM #__jobsfactory_categories c
            LEFT JOIN #__jobsfactory_jobs a on c.id=a.cat
                                    AND a.close_by_admin <> 1
                                    AND a.close_offer = 0
                                    AND a.published = 1
            WHERE c.parent = $categoryID
            GROUP BY c.id
		");

        $cats = $database->loadObjectList();
        foreach ($cats as &$cat) {
            $cat->subcategories = self::getCategories($cat->id);
        }
        unset($cat);
        return $cats;

    }

    static function catList($categories, $show_counter, $Itemid)
    {
        static $depth = 0;
        if (!count($categories)) return;
        if (!$Itemid)
            $Itemid=JobsHelperTools::getMenuItemByTaskName(array("task"=>"listjobs"));

        echo ($depth == 0) ? "<ul id='mod_jobsfactorytree_ul'>\n" : "<ul>\n";
        $depth++;
        foreach ($categories as $cat)
        {
            echo "<li>\n";
            $link_cat = JRoute::_("index.php?option=com_jobsfactory&task=listjobs&cat=" . $cat->id . "&Itemid=" . $Itemid);

            $jobs_number = ($show_counter == 1) ? " (".$cat->nr_a. ")" : '';

            echo "<a href='$link_cat'>", $cat->catname.$jobs_number, "</a>\n";
            self::catList($cat->subcategories, $show_counter, $Itemid);
            echo "</li>\n";
        }
        echo "</ul>\n";
    }

}
