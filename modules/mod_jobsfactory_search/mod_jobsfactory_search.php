<?php
/**------------------------------------------------------------------------
mod_jobsfactory_search - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

$search_in_description = $params->get("search_in_description",0);
$force_itemid   = intval( $params->get( 'force_itemid', null ) );
$lang = JFactory::getLanguage();
$lang->load('mod_jobsfactory');
$keyword = "";
$app= JFactory::getApplication();
$task = $app->input->getCmd('task');

if (in_array($task,array('listjobs','showSearchResults','tags'))) {
    $keyword=$app->getUserState('com_jobsfactory.model_Jobsfactory.filters.keyword');
}

if (!$force_itemid)
{
    require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."helpers".DS."tools.php");
    $force_itemid =JobsHelperTools::getMenuItemByTaskName(array("task"=>"listjobs"));
}

require JModuleHelper::getLayoutPath('mod_jobsfactory_search', $params->get('layout', 'vertical'));
