<?php 
/**------------------------------------------------------------------------
mod_jobsfactory_search - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="get" name="mod_jobsForm" >
<input type="hidden" name="task" value="showSearchResults"/>
<input type="hidden" name="option" value="com_jobsfactory"/>
<input type="hidden" name="Itemid" value="<?php echo $force_itemid;?>"/>
<input type="hidden"  name="in_description" value="<?php echo $search_in_description;?>" />

    <div class="search<?php echo $params->get('moduleclass_sfx') ?>">
        <div class="controls">
            <span class="input-prepend">
                <input type="text" name="keyword" value="<?php echo (isset($keyword))? $keyword : '';?>" class="inputbox input-large" style="max-width: 180px !important; margin-bottom: 5px;" />
            </span>
        </div>
        <div class="controls">
            <input type="submit" class="btn btn-primary btn-block" value="<?php echo JText::_('MOD_JOBS_SEARCH'); ?>" />
        </div>
    </div>

</form>



