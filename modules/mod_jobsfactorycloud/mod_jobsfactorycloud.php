<?php
/**------------------------------------------------------------------------
mod_jobsfactorycloud - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
require_once (dirname(__FILE__) . DS . 'helper.php');

$max_tags = intval($params->get('max_tags', 40));
$min_font = intval($params->get('min_font_size', 8));
$max_font = intval($params->get('max_font_size', 20));
$min_word_length = intval($params->get('min_tag_lenght', 4));
$max_word_length = intval($params->get('max_tag_lenght', 15));
$force_itemid = intval($params->get('force_itemid', null));
$lang = JFactory::getLanguage();
$lang->load('mod_jobsfactory');

if (!$force_itemid) {
    require_once(JPATH_ROOT . DS . "components" . DS . "com_jobsfactory" . DS . "helpers" . DS . "tools.php");
    $force_itemid =JobsHelperTools::getMenuItemByTaskName(array("task" => "listjobs"));

}

$tags = mod_jobsfactorycloudHelper::getTags($max_tags, $min_word_length, $max_word_length);
mod_jobsfactorycloudHelper::prepareTags($tags, $min_font, $max_font);
require JModuleHelper::getLayoutPath('mod_jobsfactorycloud', $params->get('layout', 'default'));
