<?php
/**------------------------------------------------------------------------
mod_jobsfactorycloud - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class mod_jobsfactorycloudHelper
{

    static function getTags($max_tags, $min_length, $max_length)
    {
        $db = JFactory::getDBO();
        $result = null;
        $query = "
            select t.tagname,count(*) nr from 
            #__jobsfactory_tags t
            left join #__jobsfactory_jobs a on t.job_id=a.id
            where 
            a.close_offer=0 and a.close_by_admin=0 and a.published = 1 and a.start_date <= NOW()
            and LENGTH(t.tagname) between $min_length and $max_length
            GROUP BY t.tagname order by nr desc        
        ";

        $db->setQuery($query, 0, $max_tags);
        $result = $db->loadObjectList();

        return $result;
    }

    static function prepareTags(&$tags, $min_font, $max_font)
    {
        if (!count($tags)) return;

        $min = end($tags);
        $max = reset($tags);

        foreach ($tags as &$tag)
            if ($max->nr != $min->nr)
                $tag->fontsize = intval($min_font + ($tag->nr - $min->nr) * ($max_font - $min_font) / ($max->nr - $min->nr));
            else
                $tag->fontsize = intval($min_font + ($max_font - $min_font) / 2);
        unset($tag);
        shuffle($tags);
    }
}

