<?php 
/**------------------------------------------------------------------------
mod_jobsfactorycloud - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<div class="bidsCloudModule">
    <?php	
    if (count($tags)>0)
    	foreach($tags as $k => $tag){
    	   $link=JRoute::_("index.php?option=com_jobsfactory&task=tags&Itemid={$force_itemid}&tag=".urldecode($tag->tagname));
          ?>
            <a href="<?php echo $link;?>"><span style="font-size: <?php echo $tag->fontsize;?>px ;"><?php echo $tag->tagname;?></span></a>&nbsp;
          <?php
    	}  
    ?>
</div>