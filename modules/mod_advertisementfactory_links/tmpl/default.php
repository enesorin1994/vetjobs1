<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="mod_advertisementfactory_links<?php echo $moduleclass_sfx; ?> mod_advertisementfactory_links_<?php echo $mode; ?> <?php echo $modules ? 'mod_advertisementfactory_links_debug' : ''; ?>">
    <ul>
        <?php if (count($ads)): ?>
            <?php foreach ($ads as $ad): ?>
                <li>
                    <a href="<?php echo $ad->redirectLink; ?>" <?php echo $target; ?>><?php echo $ad->title; ?></a>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php for ($i = 0; $i < $emptylinks; $i++): ?>
            <li>
                <a href="<?php echo $your_link; ?>"><?php echo JText::_('MOD_ADVERTISEMENTFACTORY_LINKS_YOUR_LINK_HERE'); ?></a>
            </li>
        <?php endfor; ?>
    </ul>

    <?php if ($modules): ?>
        <div class="mod_advertisementfactory_links_debug_box">
            <div><?php echo $module->title; ?></div>
        </div>
    <?php endif; ?>
</div>

<div style="clear: both;"></div>
