<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

jimport('joomla.application.component.model');
JLoader::discover('Factory', JPATH_ADMINISTRATOR . '/components/com_advertisementfactory/lib');
JModelLegacy::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'models');

$emptylinks = 0;
$modules = JFactory::getApplication()->input->getInt('modules');
$mode = $params->get('display', 1) ? 'vertical' : 'horizontal';
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$your_link = JRoute::_('index.php?option=com_advertisementfactory&view=purchasetypes&type=link&module_id=' . $module->id, false);
$settings = JComponentHelper::getParams('com_advertisementfactory');
$target = $settings->get('ads_open_new_window', 0) ? 'target="_blank"' : '';

// Check if ad type is enabled.
if (!in_array('link', $settings->get('enabled_ad_types', array()))) {
    return false;
}

if ($modules) {
    $ads = array();
    $params->set('showemptylinks', 1);
} else {
    $model = JModelLegacy::getInstance('ServeAd', 'AdvertisementFactoryModel');
    $ads = $model->getAds(
        'link',
        array(
            'module_id'   => $module->id,
            'limit'       => $params->get('slots', 5),
            'module_type' => $params->get('module_type', 1),
        ));
}

if ($params->get('showemptylinks', 1)) {
    $emptylinks = $params->get('slots', 5) - count($ads);
}

JHtml::_('stylesheet', 'modules/mod_advertisementfactory_links/template.css');

require JModuleHelper::getLayoutPath('mod_advertisementfactory_links', $params->get('layout', 'default'));
