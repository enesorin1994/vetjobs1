<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

jimport('joomla.application.component.model');
JLoader::discover('Factory', JPATH_ADMINISTRATOR . '/components/com_advertisementfactory/lib');
JModelLegacy::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'models');

$modules = JFactory::getApplication()->input->getInt('modules');
$height = $params->get('height');
$width = $params->get('width');
$showempty = $params->get('showempty', 1);
$show_bottom = $params->get('show_bottom', 1);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$folder = JURI::root() . 'components/com_advertisementfactory/storage/images';
$your_link = JRoute::_('index.php?option=com_advertisementfactory&view=purchasetypes&type=banner', false);
$settings = JComponentHelper::getParams('com_advertisementfactory');
$target = $settings->get('ads_open_new_window', 0) ? 'target="_blank"' : '';

// Check if ad type is enabled.
if (!in_array('banner', $settings->get('enabled_ad_types', array()))) {
    return false;
}

if ($modules) {
    $banner = null;
    $params->set('showempty', 1);
} else {
    $model = JModelLegacy::getInstance('ServeAd', 'AdvertisementFactoryModel');
    $ads = $model->getAds('banner', array('module_id' => $module->id));
    $banner = isset($ads[0]) ? $ads[0] : null;
}

JHtml::_('stylesheet', 'modules/mod_advertisementfactory_banners/template.css');

require JModuleHelper::getLayoutPath('mod_advertisementfactory_banners', $params->get('layout', 'default'));
