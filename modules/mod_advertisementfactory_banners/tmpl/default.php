<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="mod_advertisementfactory_banners<?php echo $moduleclass_sfx; ?> <?php echo $modules ? 'mod_advertisementfactory_banners_debug' : ''; ?>">
    <?php if ($banner): ?>
        <a href="<?php echo $banner->redirectLink; ?>" <?php echo $target; ?>>
            <img src="<?php echo $folder; ?>/<?php echo $banner->filename; ?>"/>
        </a>
    <?php elseif ($showempty): ?>
        <div style="height: <?php echo $height; ?>px; width: <?php echo $width; ?>px;"
             class="mod_advertisementfactory_banners_empty<?php echo $moduleclass_sfx; ?>">
            <br/>
            <a href="<?php echo $your_link; ?>">
                <?php echo JText::_('MOD_ADVERTISEMENTFACTORY_BANNERS_YOUR_BANNER_HERE'); ?>
            </a>
        </div>
    <?php endif; ?>

    <?php if ($banner && $show_bottom): ?>
        <div class="mod_advertisementfactory_banners_bottom<?php echo $moduleclass_sfx; ?>">
            <small>
                <small>
                    <a href="<?php echo $your_link; ?>">
                        <?php echo JText::_('MOD_ADVERTISEMENTFACTORY_BANNERS_YOUR_BANNER_HERE'); ?>
                    </a>
                </small>
            </small>
        </div>
    <?php endif; ?>

    <?php if ($modules): ?>
        <div class="mod_advertisementfactory_banners_debug_box">
            <div><?php echo $module->title; ?></div>
        </div>
    <?php endif; ?>
</div>

<div style="clear: both;"></div>
