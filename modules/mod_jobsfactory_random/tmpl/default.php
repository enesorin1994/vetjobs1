<?php 
/**------------------------------------------------------------------------
mod_jobsfactory_random - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<table width="100%">
	<?php
	for($i=0;$i<count($rows);$i++){
		$title_str = JText::_("MOD_JOBS_JOB_PLACED_BY")." ".$rows[$i]->username." \r\n ";
		$alt_class="table_module_".($i%2+1);
		$link_to_job = JRoute::_("index.php?option=com_jobsfactory&amp;task=viewapplications&amp;id=".$rows[$i]->id."&amp;Itemid=".$force_itemid);
		?>
		<tr class="<?php echo $alt_class;?>">
		 <td>
             <?php echo ($i+1);?>. <?php echo JHtml::_('link', $link_to_job, "<span class='hasTip' title='$title_str'>". $rows[$i]->title . "</span>"); ?>
		 </td>
		</tr>
		<?php
	}?>
</table>
