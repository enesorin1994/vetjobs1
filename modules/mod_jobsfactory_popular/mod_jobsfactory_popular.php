<?php
/**------------------------------------------------------------------------
mod_jobsfactory_popular - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
$module_type = $params->get("type_display");
$nr_jobs = $params->get("nr_jobs_displayed",5);
$force_itemid   = intval( $params->get( 'force_itemid', null ) );
$lang=JFactory::getLanguage();
$lang->load('mod_jobsfactory');

if (!$force_itemid)
{
    require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."helpers".DS."tools.php");
    $force_itemid =JobsHelperTools::getMenuItemByTaskName(array("task" => "listjobs"));
}

$database = JFactory::getDBO();
$query="SELECT j.*,u.username
    FROM #__jobsfactory_jobs j
    LEFT JOIN #__users u ON j.userid=u.id
    where j.close_offer=0 AND j.close_by_admin=0 AND j.published = 1 AND j.start_date <= NOW()
    ORDER BY j.hits DESC ";
$database->setQuery($query,0,$nr_jobs);
$rows = $database->loadObjectList();

require JModuleHelper::getLayoutPath('mod_jobsfactory_popular', $params->get('layout', 'default'));

