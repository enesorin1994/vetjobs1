<?php 
/**------------------------------------------------------------------------
mod_jobsfactory_filters - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
  * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<div class="column-left">

  <form action="index.php?option=com_jobsfactory" method="post" name="mod_jobsForm1" class="form-inline">
  <input type="hidden" name="task" value="showSearchResults"/>
  <input type="hidden" name="option" value="com_jobsfactory"/>
  <input type="hidden" name="Itemid" value="<?php echo $force_itemid;?>"/>

  <?php
      $padding_top = 2;
      $padding_bottom = 2;
      $imgHeight = 9;
      $imgWidth = 9;
      $padding = "padding-left: 10px;";
      $site_root = JURI::root();
      $link1 = JRoute::_('index.php?option=com_jobsfactory&amp;task=showSearchResults&amp;jinterval=1'); //1 day
      $link2 = JRoute::_('index.php?option=com_jobsfactory&amp;task=showSearchResults&amp;jinterval=7');
      $link3 = JRoute::_('index.php?option=com_jobsfactory&amp;task=showSearchResults&amp;jinterval=14');
      $link4 = JRoute::_('index.php?option=com_jobsfactory&amp;task=showSearchResults&amp;jinterval=30');
      $j=1;
      $filter_task = ($task == 'myjobs') ? 'myjobs' : 'listjobs';

      echo '<div id="Tree" align="left" class="Tree" style="padding-top:'.$padding_top.'px;padding-bottom:'.$padding_bottom.'px">';

     // echo '<span class="st_tree" onclick="hideShow(\''.$j.'\',\''.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/\')">
      //<img id="pic'.$j.'" width="'.$imgWidth.'" height="'.$imgHeight.'" src="'.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/arrow2.png"/></span>&nbsp;
     // <span class="st_tree" onclick="openLink(\''.$link1.'\',\'_parent\')"> '.'catname'.'</span>';
  ?>

  <div id="styFolder">

      <?php echo '<span class="st_tree" onclick="hideShow(\'1\',\''.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/\')">
                <img id="pic1" width="'.$imgWidth.'" height="'.$imgHeight.'" src="'.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/arrow1.png"/>
            </span>&nbsp;';
      ?>

      <span class="st_tree"><?php echo JText::_('MOD_JOBS_DATE_POSTED');?></span>

      <div class="styNodeRegion" style="display: block;" id='1'>
          <?php
              $jinterval_filter = $app->input->getString('jinterval','');
              switch ($jinterval_filter) {
                  case 1:
                      $selectedIntervalName =  JText::_('MOD_JOBS_LAST_24_HOURS');
                      break;
                  case 7:
                      $selectedIntervalName =  JText::_('MOD_JOBS_LAST_7_DAYS');
                      break;
                  case 14:
                      $selectedIntervalName =  JText::_('MOD_JOBS_LAST_14_DAYS');
                      break;
                  case 30:
                      $selectedIntervalName =  JText::_('MOD_JOBS_LAST_30_DAYS');
                      break;
                  default:
                      $selectedIntervalName =  '';
                      break;
              }
          ?>
          <div id="job_intervalSelectedName" class="slink">
            <strong><?php echo $selectedIntervalName;?></strong>
            <?php if($selectedIntervalName){ ?>
                    <a href="<?php echo JRoute::_('index.php?option=com_jobsfactory&task='.$filter_task.'&reset=all');?>">
                    <img border="0" style="vertical-align: top;"
                        onmouseout="this.src='<?php echo $site_root.'components/com_jobsfactory/images/remove_filter1.png';?>';"
                        onmouseover="this.src='<?php echo $site_root.'components/com_jobsfactory/images/remove_filter2.png';?>';"
                        alt="Remove filter" title="Remove Filters" src="<?php echo $site_root.'components/com_jobsfactory/images/remove_filter1.png';?>"></a>
            <?php } ?>
          </div>

          <div id="styLink" class="slink">
             <?php echo "<img  width=".$imgWidth." height=".$imgHeight." src='".$site_root."modules/mod_jobsfactory_filters/filterstree/images/indent4.png' style=".$padding." />"; ?>
            <span onclick='<?php echo $link1; ?>'><?php echo JHtml::_("link", $link1, JText::_('MOD_JOBS_LAST_24_HOURS')); ?></span>
          </div>

          <div id='styLink' class='slink'>
              <?php echo "<img  width=".$imgWidth." height=".$imgHeight." src='".$site_root."modules/mod_jobsfactory_filters/filterstree/images/indent4.png' style=".$padding." />"; ?>
                     <span onclick='<?php echo $link2; ?>'><?php echo JHtml::_("link", $link2, JText::_('MOD_JOBS_LAST_7_DAYS')); ?></span>
          </div>
          <div id='styLink' class='slink'>
              <?php echo "<img  width=".$imgWidth." height=".$imgHeight." src='".$site_root."modules/mod_jobsfactory_filters/filterstree/images/indent4.png' style=".$padding." />"; ?>
              <span onclick='<?php echo $link3; ?>'><?php echo JHtml::_("link", $link3, JText::_('MOD_JOBS_LAST_14_DAYS')); ?></span>
          </div>
          <div id='styLink' class='slink'>
              <?php echo "<img  width=".$imgWidth." height=".$imgHeight." src='".$site_root."modules/mod_jobsfactory_filters/filterstree/images/indent4.png' style=".$padding." />"; ?>
              <span onclick='<?php echo $link4; ?>'><?php echo JHtml::_("link", $link4, JText::_('MOD_JOBS_LAST_30_DAYS')); ?></span>
          </div>
      </div><!--styNodeRegion-->
  </div>

     <div id="styFolder">
        <?php echo '<span class="st_tree" onclick="hideShow(\'2\',\''.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/\')">
                  <img id="pic2" width="'.$imgWidth.'" height="'.$imgHeight.'" src="'.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/arrow1.png"/>
              </span>&nbsp;';
        ?>
        <span class="st_tree"><?php echo JText::_('MOD_JOBS_INDUSTRY_SECTOR');?></span>
        <div class="styNodeRegion" style="display: block;" id='2'>
            <div id="styLink" class="jobs-filter-wrapper slink1">
                <ul id='mod_jobsfactorytree_ul1' class='facet-list'>
                <?php mod_jobsfactoryfiltersHelper::catListHtml($categories,$force_itemid); ?>
                </ul>
            </div>
        </div>
     </div>


  <div id="styFolder">
    <?php echo '<span class="st_tree" onclick="hideShow(\'3\',\''.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/\')">
            <img id="pic3" width="'.$imgWidth.'" height="'.$imgHeight.'" src="'.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/arrow1.png"/>
        </span>&nbsp;';
    ?>
    <span class="st_tree"><?php echo JText::_('MOD_JOBS_JOB_TYPE');?></span>
    <div class="styNodeRegion" style="display: block;" id='3'>

        <?php
            //$selectedJobTypeName = '<strong>'.JText::_('MOD_JOBS_ALL').'</strong>';
            $selectedJobTypeName = '';
            if($tId = $app->input->getString('job_type')) {
                $selectedJobTypeName = '<strong>'.$job_types[$tId]->text.'</strong>';
            }
        ?>
        <div id="job_typeSelectedName" class="slink">
            <strong><?php echo $selectedJobTypeName;?></strong>
            <?php if($selectedJobTypeName) { ?>
            <a href="<?php echo JRoute::_('index.php?option=com_jobsfactory&task='.$filter_task.'&reset=all');?>">
                <img border="0" style="vertical-align: top;"
                     onmouseout="this.src='<?php echo $site_root.'components/com_jobsfactory/images/remove_filter1.png';?>';"
                     onmouseover="this.src='<?php echo $site_root.'components/com_jobsfactory/images/remove_filter2.png';?>';"
                     alt="Remove filter" title="Remove Filters" src="<?php echo $site_root.'components/com_jobsfactory/images/remove_filter1.png';?>"></a>
            <?php } ?>
        </div>
            <?php foreach ($job_types as $job_type) {
                $job_type_link = JRoute::_('index.php?option=com_jobsfactory&amp;task=showSearchResults&amp;job_type='.$job_type->value.'');
            ?>

            <div id='styLink' class='slink'>
                   <?php echo "<img  width='".$imgWidth."' height='".$imgHeight."' src='".$site_root."modules/mod_jobsfactory_filters/filterstree/images/indent4.png' style='".$padding."' />"; ?>
                    <a href="<?php echo $job_type_link; ?>"><?php echo stripslashes($job_type->text); ?></a>
            </div>
        <?php } ?>

            <input type="hidden" name="job_type" value="<?php echo $tId; ?>" id="jobtypeIdSearch" />
         <!-- Seasonal
               Internship
                Volunteer -->
    </div>
  </div>

  <div id="styFolder">
      <?php echo '<span class="st_tree" onclick="hideShow(\'4\',\''.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/\')">
              <img id="pic4" width="'.$imgWidth.'" height="'.$imgHeight.'" src="'.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/arrow1.png"/>
          </span>&nbsp;';
      ?>
      <span class="st_tree"><?php echo JText::_('MOD_JOBS_EDUCATION');?></span>
      <div class="styNodeRegion" style="display: block;" id='4'>

          <?php
              //$selectedEduName = '<strong>'.JText::_('MOD_JOBS_ALL').'</strong>';
              $selectedEduName = '';
              if($tId = $app->input->getString('studies_request')) {
                  $selectedEduName = '<strong>'.$education[$tId]->text.'</strong>';
              }
          ?>
          <div id="educationSelectedName" class="slink">
              <strong><?php echo $selectedEduName;?></strong>
              <?php if($selectedEduName) { ?>
              <a href="<?php echo JRoute::_('index.php?option=com_jobsfactory&task='.$filter_task.'&reset=all');?>">
                  <img border="0"  style="vertical-align: top;"
                       onmouseout="this.src='<?php echo $site_root.'components/com_jobsfactory/images/remove_filter1.png';?>';"
                       onmouseover="this.src='<?php echo $site_root.'components/com_jobsfactory/images/remove_filter2.png';?>';"
                       alt="Remove filter" title="Remove Filters" src="<?php echo $site_root.'components/com_jobsfactory/images/remove_filter1.png';?>"></a>
              <?php } ?>
          </div>
        <?php foreach ($education as $edu) {
            $edu_link = JRoute::_('index.php?option=com_jobsfactory&amp;task=showSearchResults&amp;studies_request='.$edu->value.'');
        ?>
            <div id='styLink' class='slink'>
                  <?php echo "<img  width=".$imgWidth." height=".$imgHeight." src='".$site_root."modules/mod_jobsfactory_filters/filterstree/images/indent4.png' style=".$padding." />"; ?>
                    <a href="<?php echo $edu_link; ?>"><?php echo stripslashes($edu->text); ?></a>
            </div>
        <?php } ?>
      </div>
  </div>

  <div id="styFolder">
        <?php echo '<span class="st_tree" onclick="hideShow(\'5\',\''.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/\')">
             <img id="pic5" width="'.$imgWidth.'" height="'.$imgHeight.'" src="'.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/arrow1.png"/>
         </span>&nbsp;';
        ?>
        <span class="st_tree"><?php echo JText::_('MOD_JOBS_EXPERIENCE');?></span>
        <div class="styNodeRegion" style="display: block;" id='5'>
            <?php
              //$selectedExpName = '<strong>'.JText::_('MOD_JOBS_ALL').'</strong>';
              $selectedExpName = '';
              if($tId = $app->input->getString('experience_request')) {
                  $selectedExpName = '<strong>'.$experience_levels[$tId]->text.'</strong>';
              }
            ?>
            <div id="experienceSelectedName" class="slink">
              <strong><?php echo $selectedExpName;?></strong>
                <?php if($selectedExpName) { ?>
                <a href="<?php echo JRoute::_('index.php?option=com_jobsfactory&task='.$filter_task.'&reset=all');?>">
                  <img border="0" style="vertical-align: top;"
                       onmouseout="this.src='<?php echo $site_root.'components/com_jobsfactory/images/remove_filter1.png';?>';"
                       onmouseover="this.src='<?php echo $site_root.'components/com_jobsfactory/images/remove_filter2.png';?>';"
                       alt="Remove filter" title="Remove Filters" src="<?php echo $site_root.'components/com_jobsfactory/images/remove_filter1.png';?>"></a>
                <?php } ?>
            </div>

        <?php foreach ($experience_levels as $experience_level) {
            $exp_link = JRoute::_('index.php?option=com_jobsfactory&amp;task=showSearchResults&amp;experience_request='.$experience_level->value.'');
            ?>

            <div id='styLink' class='slink'>
                  <?php echo "<img  width=".$imgWidth." height=".$imgHeight." src='".$site_root."modules/mod_jobsfactory_filters/filterstree/images/indent4.png' style=".$padding." />"; ?>
                    <a href="<?php echo $exp_link; ?>"><?php echo stripslashes($experience_level->text); ?></a>
            </div>
        <?php } ?>
        </div>
  </div>

  <div id="styFolder">
      <?php echo '<span class="st_tree" onclick="hideShow(\'6\',\''.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/\')">
           <img id="pic6" width="'.$imgWidth.'" height="'.$imgHeight.'" src="'.$site_root.'/modules/mod_jobsfactory_filters/filterstree/images/arrow2.png"/>
       </span>&nbsp;';
      ?>
      <span class="st_tree"><?php echo JText::_('MOD_JOBS_CITY');?></span>
      <div class="styNodeRegion" style="display: none;" id='6'>
          <div id="styLink" class="jobs-filter-wrapper slink">
             <ul id='mod_jobsfactorycities_ul1' class='facet-list'>
                <?php mod_jobsfactoryfiltersHelper::cityListHtml($cities_list); ?>
             </ul>
          </div>
      </div>
</div>
</div>
</form>


</div><!--filters-->