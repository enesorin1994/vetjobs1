<?php
/**------------------------------------------------------------------------
mod_jobsfactory_filters - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
  * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

 class mod_jobsfactoryfiltersHelper
 {

    static function getExperiencelevels()
    {
       $db = JFactory::getDbo();

       $db->setQuery("SELECT id AS `value`, `levelname` as text FROM `#__jobsfactory_experiencelevel` WHERE `published`=1 ORDER BY `ordering`,`levelname`");
       return $db->loadObjectList('value');
    }


    public static function getJobTypes()
    {
        $db = JFactory::getDbo();
        $db->setQuery("select `typename` as text,`id` as value from `#__jobsfactory_jobtype` WHERE `published` = 1 order by `ordering`,`typename`");
        return $db->loadObjectList('value');
    }

     static function getCompanyActivityFields() {
         $db = JFactory::getDbo();
         $ActivityFields = '';

         $db->setQuery("SELECT GROUP_CONCAT( DISTINCT c.catname SEPARATOR  ', ' ) AS 'activityfields'
                         FROM `#__jobsfactory_categories` c ") ;
         $ActivityFields = $db->loadObjectList();

         return $ActivityFields;
     }

     static function getCategories($categoryID = 0)
     {
         $database = JFactory::getDbo();
         $database->setQuery("
             SELECT c.id,c.catname
             FROM #__jobsfactory_categories c
             LEFT JOIN #__jobsfactory_jobs a on c.id=a.cat
                                 AND a.close_by_admin <> 1
                                 AND a.close_offer = 0
                                 AND a.published = 1
             WHERE c.parent = $categoryID
             GROUP BY c.id
        ");

         $cats = $database->loadObjectList();
         foreach ($cats as &$cat) {
             $cat->subcategories = self::getCategories($cat->id);
         }
         unset($cat);
         return $cats;
     }

     static function catListHtml($categories, $Itemid)
     {
         if (!count($categories)) return;
         if (!$Itemid)
             $Itemid=JobsHelperTools::getMenuItemByTaskName(array("task"=>"listjobs"));

         foreach ($categories as $cat)
         {
             echo "<li class='facet-item'>\n";
             $link_cat = JRoute::_("index.php?option=com_jobsfactory&task=listjobs&cat=" . $cat->id . "&Itemid=" . $Itemid);

             echo "<a href='$link_cat'>", $cat->catname, "</a>\n";
             self::catListHtml($cat->subcategories, $Itemid);
             echo "</li>\n";
         }
     }


    public static function getStudyLevels(){
        $db = JFactory::getDbo();
        $db->setQuery("select id as `value`, `levelname` as text from `#__jobsfactory_studieslevel` where `published`=1 order by `ordering`,`id`");
        $levelnames = $db->loadObjectList('value');

        return $levelnames;
    }


    public static function getCityList()
    {
        $db = JFactory::getDbo();
        $cfg = new JobsfactoryConfig();

        if ($cfg->enable_location_management) {

            $db->setQuery("SELECT city.id as cityid, city.cityname, IF ( j.job_cityid = 0 , j.job_cityname, (SELECT cityname FROM `#__jobsfactory_cities` as cit WHERE cit.id=j.job_cityid) ) as jcityname
                      FROM `#__jobsfactory_jobs` AS `j`
                      LEFT JOIN `#__jobsfactory_cities` AS `city` ON `j`.`job_cityid`=`city`.`id`
                      WHERE city.status = 1
                       GROUP BY city.id
                      ");
        } else {
            $db->setQuery("SELECT DISTINCT `j`.`job_cityname` as jcityname  FROM `#__jobsfactory_jobs` AS `j` ");
        }

        return $db->loadObjectList();
    }


     static function cityListHtml($cities)
     {
        if (!count($cities)) return;
        $parents_disabled = false;
         $cfg = new JobsfactoryConfig();

        //job_cityname
        foreach ($cities as $city)
        {
            $city->depth = 1;

            echo "<li class='facet-item'>\n";

            if ($cfg->enable_location_management) {
                $link_city = JRoute::_("index.php?option=com_jobsfactory&task=listjobs&job_cityid=" . $city->cityid);
            } else {
                $link_city = JRoute::_("index.php?option=com_jobsfactory&task=listjobs&city=" . $city->jcityname);
            }
              echo "<a href='$link_city'>", stripslashes($city->jcityname), "</a>\n";
              echo "</li>\n";
        }
     }


    function getOrdering()
    {
        $app	= JFactory::getApplication();
        $this->ordering	= $app->getUserStateFromRequest($this->context.'ordering', 'ordering', 'ordering', 'string');
        $this->ordering_dir	= $app->getUserStateFromRequest($this->context.'ordering_dir', 'ordering_dir', 'ASC', 'string');
    }

}

