<?php
/**------------------------------------------------------------------------
mod_jobsfactory_filters - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author ThePHPFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
require_once (dirname(__FILE__) . DS . 'helper.php');

$site_root = JURI::root();
$jdoc = JFactory::getDocument();
$jdoc->addScript(JURI::root().'components/com_jobsfactory/js/jquery/jquery.cookie.js');
$jdoc->addStyleSheet(JURI::root(). 'modules/mod_jobsfactory_filters/filterstree/tree.css');
$jdoc->addScript(JURI::root(). 'modules/mod_jobsfactory_filters/filterstree/tree.js');

$force_itemid   = intval( $params->get( 'force_itemid', null ) );
$lang = JFactory::getLanguage();
$lang->load('mod_jobsfactory');

require_once(JPATH_ROOT.DS. 'components' . DS . 'com_jobsfactory' . DS . 'options.php');
$cfg    = new JobsfactoryConfig();
$app    = JFactory::getApplication();
$task   = $app->input->getCmd('task');

if (!$force_itemid) {
    require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."helpers".DS."tools.php");
    $force_itemid =JobsHelperTools::getMenuItemByTaskName(array("task"=>"listjobs"));
}

require_once(JPATH_ROOT.DS."administrator". DS . 'components' . DS . 'com_jobsfactory'. DS . 'thefactory' . DS . 'application' . DS . 'application.class.php');
$categories = mod_jobsfactoryfiltersHelper::getCategories();
$experience_levels = mod_jobsfactoryfiltersHelper::getExperiencelevels();
$job_types = mod_jobsfactoryfiltersHelper::getJobTypes();
$education = mod_jobsfactoryfiltersHelper::getStudyLevels();

// list of cities
$cities_list = mod_jobsfactoryfiltersHelper::getCityList();

require JModuleHelper::getLayoutPath('mod_jobsfactory_filters', $params->get('layout', 'default'));
