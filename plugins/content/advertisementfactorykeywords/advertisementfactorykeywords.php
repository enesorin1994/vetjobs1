<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

jimport('joomla.plugin.plugin');

class plgContentAdvertisementFactoryKeywords extends JPlugin
{
    public function __construct(&$subject, $config = array())
    {
        parent::__construct($subject, $config);

        JLoader::discover('Factory', JPATH_ADMINISTRATOR . '/components/com_advertisementfactory/lib');

        // Add models path
        JModelLegacy::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'models');
    }

    public function onContentPrepare($context, $article, $params, $limitstart)
    {
        JHtml::_('behavior.tooltip');

        // Check if ad type is enabled.
        $settings = JComponentHelper::getParams('com_advertisementfactory');
        if (!in_array('keyword', $settings->get('enabled_ad_types', array()))) {
            return false;
        }

        // Initialise variables.
        $settings = JComponentHelper::getParams('com_advertisementfactory');
        $limit = $settings->get('keywords_max_ads', 3);
        $options = array();

        // Check the limit of keywords ads per article.
        if ($limit > 0) {
            $options['limit'] = $limit;
        }

        // Get the ads.
        $model = JModelLegacy::getInstance('ServeAd', 'AdvertisementFactoryModel');
        $ads = $model->getAds('keyword', $options);
        $keywords = array();

        // Check if any ads returned.
        if (!$ads) {
            return false;
        }

        $settings = JComponentHelper::getParams('com_advertisementfactory');
        $target = $settings->get('ads_open_new_window', 0) ? 'target="_blank"' : '';

        // Load the language file.
        $language = JFactory::getLanguage();
        $language->load('com_advertisementfactory');

        // Check the replacement mode
        $replacement = 1 == $settings->get('keywords_replacement_mode', 1) ? 1 : -1;

        // Parse ads and replace article content.
        foreach ($ads as $ad) {
            $content = $ad->contents . '<br /><br /><div style=\'text-align: right;\'><small><small>' . JText::_('KEYWORD_AD_ADVERTISEMENT_INFO') . '</small></small></div>';

            $replace = '<a href="' . $ad->redirectLink . '" class="hasTip" title="' . $ad->title . '::' . $content . '" ' . $target . '>' . $ad->keywords . '</a>';

            $article->text = preg_replace('/' . $ad->keywords . '/i', $replace, $article->text, $replacement, $count);

            if ($count) {
                $keywords[$ad->id] = $ad;
            }
        }

        return true;
    }
}
