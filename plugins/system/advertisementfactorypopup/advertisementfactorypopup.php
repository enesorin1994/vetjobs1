<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

jimport('joomla.plugin.plugin');
jimport('joomla.application.component.model');

class plgSystemAdvertisementFactoryPopup extends JPlugin
{
    protected $debug = false;

    public function __construct(&$subject, $config = array())
    {
        parent::__construct($subject, $config);

        JLoader::discover('Factory', JPATH_ADMINISTRATOR . '/components/com_advertisementfactory/lib');

        // Add models path
        JModelLegacy::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'models');
    }

    public function onAfterDispatch()
    {
        // Are we allowed to show modal?
        if (!$this->allowedToShow()) {
            return false;
        }

        if ($this->debug) {
            $this->debug();
        }

        // Check for ads shown this session
        if (!$this->checkSessionAdsShown()) {
            return false;
        }

        // Check for number of clicks
        if (!$this->checkSessionPageViews()) {
            return false;
        }

        $model = JModelLegacy::getInstance('ServeAd', 'AdvertisementFactoryModel');
        $ads = $model->getAds('popup');
        $ad = isset($ads[0]) ? $ads[0] : null;

        // Check if there is ad available
        if (!$ad) {
            return false;
        }

        $this->showAd($ad);
    }

    protected function checkSessionAdsShown()
    {
        // Check if we're previewing an ad
        if ($this->previewAd()) {
            return true;
        }

        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $component = JComponentHelper::getComponent('com_advertisementfactory');
        $session = JFactory::getSession();

        $adsShown = $session->get('advertisementfactory.popup.shown', 0);

        return $adsShown < $component->params->get('popupads_displays_per_session' . $suffix);
    }

    protected function checkSessionPageViews()
    {
        // Check if we're previewing an ad
        if ($this->previewAd()) {
            return true;
        }

        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $component = JComponentHelper::getComponent('com_advertisementfactory');
        $session = JFactory::getSession();

        // Get current page views
        $sessionPageViews = $session->get('advertisementfactory.popup.sessionpageviews', 1);

        // Update page views
        $session->set('advertisementfactory.popup.sessionpageviews', $sessionPageViews + 1);

        if (!$component->params->get('popupads_clicks_to_display' . $suffix, 100)) {
            return true;
        }

        return $sessionPageViews % $component->params->get('popupads_clicks_to_display' . $suffix, 100) == 0;
    }

    protected function allowedToShow()
    {
        // Check if ad type is enabled.
        $params = JComponentHelper::getParams('com_advertisementfactory');
        if (!in_array('popup', $params->get('enabled_ad_types', array()))) {
            return false;
        }

        // Check if we're previewing an ad
        if ($this->previewAd()) {
            return true;
        }

        // Check if we're on the frontend
        $app = JFactory::getApplication();
        if ($app->getName() == 'administrator') {
            return false;
        }

        // Check if we're on the Full Page Ad page
        $view = JFactory::getApplication()->input->getString('view');
        $option = JFactory::getApplication()->input->getString('option');

        if ($option == 'com_advertisementfactory') {
            return false;
        }

        // Check if is Ajax request
        if ($this->isAjaxRequest()) {
            return false;
        }

        // Check if is Modal request
        if ($this->isModalRequest()) {
            return false;
        }

        // Check if user group is excluded
        if ($this->isGroupExcluded()) {
            return false;
        }

        // All seems fine
        return true;
    }

    protected function isAjaxRequest()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    protected function isModalRequest()
    {
        $tmpl = JFactory::getApplication()->input->getCmd('tmpl');

        return $tmpl == 'component';
    }

    protected function isGroupExcluded()
    {
        $user = JFactory::getUser();
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        return array_intersect($component->params->get('popupads_excluded_groups', array()), JFactory::getUser()->groups);
    }

    protected function debug()
    {
        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $session = JFactory::getSession();
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        echo '<pre>';
        var_dump('Per session: ' . $session->get('advertisementfactory.popup.shown', 0) . ' / ' . $component->params->get('popupads_displays_per_session' . $suffix));
        echo '</pre>';

        echo '<pre>';
        var_dump('Clicks: ' . $session->get('advertisementfactory.popup.sessionpageviews', 1) . ' / ' . $component->params->get('popupads_clicks_to_display' . $suffix, 100));
        echo '</pre>';
    }

    protected function previewAd()
    {
        $view = JFactory::getApplication()->input->getString('view');
        $option = JFactory::getApplication()->input->getString('option');

        return $view == 'previsualize' && $option == 'com_advertisementfactory';
    }

    protected function showAd($ad)
    {
        // Initialise variables.
        $component = JComponentHelper::getComponent('com_advertisementfactory');
        $document = JFactory::getDocument();
        $url = JRoute::_('index.php?option=com_advertisementfactory&view=popupad&tmpl=component', false);

        // Add popup script
        $document->addScriptDeclaration('window.onload = function () { var popup = window.open("' . $url . '", "advertisementfactorypopup", "height=' . (int)$component->params->get('popupads_window_height', 100) . ', width=' . (int)$component->params->get('popupads_window_width', 100) . ', location=0, menubar=0, resizable=0, scrollbars=0, status=0, titlebar=0, toolbar=0"); popup.focus(); }');

        // Mark shown ad.
        $session = JFactory::getSession();
        $session->set('advertisementfactory.popup.shown', (int)$session->get('advertisementfactory.popup.shown', 0) + 1);

        $session->set('advertisementfactory.popup.nextad', $ad->id);

        return true;
    }
}
