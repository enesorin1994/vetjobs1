<?php
// no direct access
defined('_JEXEC') or die;
jimport('joomla.plugin.plugin');

//Open Graph implementation http://ogp.me/

/*
 * og:title - The title of your object as it should appear within the graph, e.g., "The Rock".
 * og:type - The type of your object, e.g., "video.movie". Depending on the type you specify, other properties may also be required.
 * og:image - An image URL which should represent your object within the graph.
 * og:url - The canonical URL of your object that will be used as its permanent ID in the graph, e.g., "http://www.imdb.com/title/tt0117500/".
 * og:audio - A URL to an audio file to accompany this object.
 * og:description - A one to two sentence description of your object.
 * og:determiner - The word that appears before this object's title in a sentence. An enum of (a, an, the, "", auto). If auto is chosen, the consumer of your data should chose between "a" or "an". Default is "" (blank).
 * og:locale - The locale these tags are marked up in. Of the format language_TERRITORY. Default is en_US.
 * og:locale:alternate - An array of other locales this page is available in.
 * og:site_name - If your object is part of a larger web site, the name which should be displayed for the overall site. e.g., "IMDb".
 * og:video - A URL to a video file that complements this object.
 */

class plgSystemFactoryOpenGraph extends JPlugin
{

	function __construct($subject, $config = array())
	{
		$this->document = JFactory::getDocument();

		parent::__construct($subject, $config);
	}

	/**
	 * system - facebook fix Plugin
	 *
	 * @package		Joomla.Plugin
	 * @subpakage	XS_NRG.facebookfix
	 */
	public function onAfterRoute()
	{
		$app = JFactory::getApplication();
		if ( $app->isAdmin() )
		{
			return;
		}
		$unsupported = false;

		if (isset($_SERVER['HTTP_USER_AGENT']))
		{
			/* Facebook User Agent
			 * facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)
			 * LinkedIn User Agent
			 * LinkedInBot/1.0 (compatible; Mozilla/5.0; Jakarta Commons-HttpClient/3.1 +http://www.linkedin.com)
			 */
			$pattern = strtolower('/facebookexternalhit|LinkedInBot/x');
			if (preg_match($pattern, strtolower($_SERVER['HTTP_USER_AGENT'])))
			{
				$unsupported = true;
			}
		}
		if (($app->get('gzip') == 1) && $unsupported)
		{
			$app->set('gzip', 0);
		}
	}

	public function ogBasicData($title = null, $description = null, $type = null, $other = array())
	{
		$opengraph = null;

		// Inferred OpenGraph tags.
			$opengraph .= '<meta property="og:url" content="' . JRoute::_(JURI::getInstance()->toString()) . '"/>' . "\n";
			$opengraph .= '<meta property="og:site_name" content="' . JFactory::getConfig()->get('sitename') . '"/>' . "\n";


		if ($title)
		{
			$opengraph .= '<meta property="og:title" content="' . $title . '"/>' . "\n";
		}

		if ($description)
		{
			$opengraph .= '<meta property="og:description" content="' . $description . '"/>' . "\n";
		}

		if (!$type)
		{
			$opengraph .= '<meta property="og:type" content="article"/>' . "\n";
		}
		else
		{
			$opengraph .= '<meta property="og:type" content="' . $type . '"/>' . "\n";
		}

		foreach($other as $k => $v)
		{
			$opengraph .= '<meta property="og:' . $k . '" content="' . $v . '"/>' . "\n";
		}

		$this->document->addCustomTag($opengraph);
	}

	public function ogAddImage($imgurl, $params=array())
	{
		$opengraph = null;
		$opengraph .= '<meta property="og:' . 'image' . '" content="' . $imgurl . '"/>' . "\n";

		foreach($params as $k => $v)
		{
			$opengraph .= '<meta property="og:' . $k . '" content="' . $v . '"/>' . "\n";
		}

		$this->document->addCustomTag($opengraph);
	}

	public function ogSetTitle($title)
	{
		$opengraph = null;
		$opengraph .= '<meta property="og:title" content="' . $title . '"/>' . "\n";

		$this->document->addCustomTag($opengraph);
	}

	public function ogSetDescription($description)
	{
		$opengraph = null;
		$opengraph .= '<meta property="og:description" content="' . $description . '"/>' . "\n";

		$this->document->addCustomTag($opengraph);
	}

	public function ogSetType($type)
	{
		$opengraph = null;
		$opengraph .= '<meta property="og:type" content="' . $type . '"/>' . "\n";

		$this->document->addCustomTag($opengraph);
	}


}