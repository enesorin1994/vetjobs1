<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

jimport('joomla.plugin.plugin');
jimport('joomla.application.component.model');

class plgSystemAdvertisementFactoryModal extends JPlugin
{
    protected $debug = false;

    public function __construct(&$subject, $config = array())
    {
        parent::__construct($subject, $config);

        JLoader::discover('Factory', JPATH_ADMINISTRATOR . '/components/com_advertisementfactory/lib');

        // Add models path
        JModelLegacy::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'models');
    }

    public function onAfterDispatch()
    {
        // Are we allowed to show modal?
        if (!$this->allowedToShow()) {
            return false;
        }

        if ($this->debug) {
            $this->debug();
        }

        // Check for ads shown this session
        if (!$this->checkSessionAdsShown()) {
            return false;
        }

        // Check for number of clicks
        if (!$this->checkSessionPageViews()) {
            return false;
        }

        $model = JModelLegacy::getInstance('ServeAd', 'AdvertisementFactoryModel');
        $ads = $model->getAds('modal');
        $modalAd = isset($ads[0]) ? $ads[0] : null;

        // Check if there is ad available
        if (!$modalAd) {
            return false;
        }

        $this->showModalAd($modalAd);
    }

    protected function checkSessionAdsShown()
    {
        // Check if we're previewing an ad
        if ($this->previewAd()) {
            return true;
        }

        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $component = JComponentHelper::getComponent('com_advertisementfactory');
        $session = JFactory::getSession();

        $adsShown = $session->get('advertisementfactory.modal.shown', 0);

        return $adsShown < $component->params->get('modalads_displays_per_session' . $suffix);
    }

    protected function checkSessionPageViews()
    {
        // Check if we're previewing an ad
        if ($this->previewAd()) {
            return true;
        }

        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $component = JComponentHelper::getComponent('com_advertisementfactory');
        $session = JFactory::getSession();

        // Get current page views
        $sessionPageViews = $session->get('advertisementfactory.modal.sessionpageviews', 1);

        // Update page views
        $session->set('advertisementfactory.modal.sessionpageviews', $sessionPageViews + 1);

        if (!$component->params->get('modalads_clicks_to_display' . $suffix, 100)) {
            return true;
        }

        return $sessionPageViews % $component->params->get('modalads_clicks_to_display' . $suffix, 100) == 0;
    }

    protected function allowedToShow()
    {
        // Check if ad type is enabled.
        $params = JComponentHelper::getParams('com_advertisementfactory');
        if (!in_array('modal', $params->get('enabled_ad_types', array()))) {
            return false;
        }

        // Check if we're previewing an ad
        if ($this->previewAd()) {
            return true;
        }

        // Check if we're on the frontend
        $app = JFactory::getApplication();
        if ($app->getName() == 'administrator') {
            return false;
        }

        // Check if we're on the Full Page Ad page
        $view = JFactory::getApplication()->input->getString('view');
        $option = JFactory::getApplication()->input->getString('option');

        if ($option == 'com_advertisementfactory') {
            return false;
        }

        // Check if is Ajax request
        if ($this->isAjaxRequest()) {
            return false;
        }

        // Check if is Modal request
        if ($this->isModalRequest()) {
            return false;
        }

        // Check if user group is excluded
        if ($this->isGroupExcluded()) {
            return false;
        }

        // All seems fine
        return true;
    }

    protected function isAjaxRequest()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    protected function isModalRequest()
    {
        $tmpl = JFactory::getApplication()->input->getCmd('tmpl');

        return $tmpl == 'component';
    }

    protected function isGroupExcluded()
    {
        $user = JFactory::getUser();
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        return array_intersect($component->params->get('modalads_excluded_groups', array()), JFactory::getUser()->groups);
    }

    protected function debug()
    {
        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $session = JFactory::getSession();
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        echo '<pre>';
        var_dump('Per session: ' . $session->get('advertisementfactory.modal.shown', 0) . ' / ' . $component->params->get('modalads_displays_per_session' . $suffix));
        echo '</pre>';

        echo '<pre>';
        var_dump('Clicks: ' . $session->get('advertisementfactory.modal.sessionpageviews', 1) . ' / ' . $component->params->get('modalads_clicks_to_display' . $suffix, 100));
        echo '</pre>';
    }

    protected function previewAd()
    {
        $view = JFactory::getApplication()->input->getString('view');
        $option = JFactory::getApplication()->input->getString('option');

        return $view == 'previsualize' && $option == 'com_advertisementfactory';
    }

    protected function showModalAd($ad)
    {
        // Load component language
        $lang = JFactory::getLanguage();
        $lang->load('com_advertisementfactory');

        // Mark shown ad
        $session = JFactory::getSession();
        $session->set('advertisementfactory.modal.shown', (int)$session->get('advertisementfactory.modal.shown', 0) + 1);

        //
        JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'tables');
        $table = JTable::getInstance('Ad', 'AdvertisementFactoryTable');
        $table->bind($ad);

        $table->type = 'modal';

        // Include javascript
        JHtml::_('behavior.modal');
        JHtml::script('components/com_advertisementfactory/assets/js/object.js');
        JHtml::script('components/com_advertisementfactory/assets/js/modal.js');

        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $document = JFactory::getDocument();
        $component = JComponentHelper::getComponent('com_advertisementfactory');
        $target = $component->params->get('ads_open_new_window', 0) ? 'target="_blank"' : '';

        // Append content to document
        $content = '<div style="display: none;">'
            . '  <div id="advertisementfactory-modal-ad">'
            . '    <div>'
            . '    </div>'
            . '    <div id="all">'
            . '      <div id="main">'
            . '        <div style="text-align: right; border-bottom: 1px dotted #000000; padding-bottom: 10px; margin-bottom: 10px;">'
            . '          ' . JText::sprintf('MODALAD_CLOSE_MESSAGE', (int)$component->params->get('modalads_seconds_to_display' . $suffix, 20))
            . '          <a href="#" id="advertisementfactory-modal-close">' . JText::_('MODALAD_CLOSE_NOW_LINK') . '</a>'
            . '        </div>'

            . ('' != $table->title ? '<h1>' . $table->title . '</h1>' : '')
            . ('' != $table->contents ? '<p>' . $table->contents . '</p>' : '')

            . '        <div style="margin: 0 auto; text-align: center;">'
            . '          <a href="' . $ad->redirectLink . '" ' . $target . '>'
            . '            <img src="' . $table->getFileSource() . '" style="max-width: 80%;" />'
            . '          </a>'
            . '        </div>'
            . '      </div>'
            . '    </div>'
            . '  </div>'
            . '</div>';

        $document->setBuffer($document->getBuffer('component') . $content, 'component');

        $document->addScriptDeclaration('AdvertisementFactory.set("window_width", ' . (int)$component->params->get('modalads_window_width', 600) . ');');
        $document->addScriptDeclaration('AdvertisementFactory.set("window_height", ' . (int)$component->params->get('modalads_window_height', 500) . ');');
        $document->addScriptDeclaration('AdvertisementFactory.set("window_close_seconds", ' . (int)$component->params->get('modalads_seconds_to_display' . $suffix, 20) . ');');
    }
}
