<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');
jimport('joomla.application.component.model');

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

class plgSystemAdvertisementFactoryFullPage extends JPlugin
{
    protected $debug = false;

    public function __construct(&$subject, $config = array())
    {
        parent::__construct($subject, $config);

        JLoader::discover('Factory', JPATH_ADMINISTRATOR . '/components/com_advertisementfactory/lib');

        // Add models path
        JModelLegacy::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'models');
    }

    public function onAfterRoute()
    {
        $app = JFactory::getApplication();

        // Are we allowed to redirect?
        if (!$this->allowedToRedirect()) {
            return false;
        }

        if ($this->debug) {
            $this->debug();
        }

        // Check for ads shown this session
        if (!$this->checkSessionAdsShown()) {
            return false;
        }

        // Check for number of clicks
        if (!$this->checkSessionPageViews()) {
            return false;
        }

        // Check if user group is excluded
        if ($this->isGroupExcluded()) {
            return false;
        }

        $model = JModelLegacy::getInstance('ServeAd', 'AdvertisementFactoryModel');
        $ads = $model->getAds('fullpage');
        $fullPageAd = isset($ads[0]) ? $ads[0] : null;

        // Check if there is ad available
        if (!$fullPageAd) {
            return false;
        }

        $this->prepareToShowFullAd($fullPageAd->id);

        $app->redirect(JRoute::_('index.php?option=com_advertisementfactory&view=fullpagead&tmpl=component', false));
    }

    protected function checkSessionAdsShown()
    {
        // Check if we're previewing an ad
        if ($this->previewAd()) {
            return true;
        }

        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $component = JComponentHelper::getComponent('com_advertisementfactory');
        $session = JFactory::getSession();

        $adsShown = $session->get('advertisementfactory.fullpage.shown', 0);

        return $adsShown < $component->params->get('fullpageads_displays_per_session' . $suffix);
    }

    protected function checkSessionPageViews()
    {
        // Check if we're previewing an ad
        if ($this->previewAd()) {
            return true;
        }

        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $component = JComponentHelper::getComponent('com_advertisementfactory');
        $session = JFactory::getSession();

        // Get current page views
        $sessionPageViews = $session->get('advertisementfactory.fullpage.sessionpageviews', 1);

        // Update page views
        $session->set('advertisementfactory.fullpage.sessionpageviews', $sessionPageViews + 1);

        if (!$component->params->get('fullpageads_clicks_to_display' . $suffix, 100)) {
            return true;
        }

        return $sessionPageViews % $component->params->get('fullpageads_clicks_to_display' . $suffix, 100) == 0;
    }

    protected function allowedToRedirect()
    {
        // Check if ad type is enabled.
        $params = JComponentHelper::getParams('com_advertisementfactory');
        if (!in_array('fullpage', $params->get('enabled_ad_types', array()))) {
            return false;
        }

        // Check if we're previewing an ad
        if ($this->previewAd()) {
            return true;
        }

        // Check if we're on the frontend
        $app = JFactory::getApplication();
        if ($app->getName() == 'administrator') {
            return false;
        }

        // Check if we're on the Full Page Ad page
        $view = JFactory::getApplication()->input->getString('view');
        $option = JFactory::getApplication()->input->getString('option');

        if ($option == 'com_advertisementfactory') {
            return false;
        }

        // Check if method is GET
        $method = JFactory::getApplication()->input->getMethod();
        if ($method != 'GET') {
            return false;
        }

        // Check if is Ajax request
        if ($this->isAjaxRequest()) {
            return false;
        }

        // All seems fine
        return true;
    }

    protected function isAjaxRequest()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    protected function isGroupExcluded()
    {
        $user = JFactory::getUser();
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        return array_intersect($component->params->get('fullpageads_excluded_groups', array()), JFactory::getUser()->groups);
    }

    protected function prepareToShowFullAd($fullPageAdId)
    {
        $session = JFactory::getSession();

        $path = $this->previewAd() ? @$_SERVER['HTTP_REFERER'] : JUri::getInstance()->toString(array('path', 'query'));
        $redirect = base64_encode($path);

        $session->set('advertisementfactory.fullpage.nextad', $fullPageAdId);
        $session->set('advertisementfactory.fullpage.redirect', $redirect);

        $session->set('advertisementfactory.fullpage.shown', (int)$session->get('advertisementfactory.fullpage.shown', 0) + 1);
    }

    protected function debug()
    {
        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $session = JFactory::getSession();
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        echo '<pre>';
        var_dump('Per session: ' . $session->get('advertisementfactory.fullpage.shown', 0) . ' / ' . $component->params->get('fullpageads_displays_per_session' . $suffix));
        echo '</pre>';

        echo '<pre>';
        var_dump('Clicks: ' . $session->get('advertisementfactory.fullpage.sessionpageviews', 1) . ' / ' . $component->params->get('fullpageads_clicks_to_display' . $suffix, 100));
        echo '</pre>';
    }

    protected function previewAd()
    {
        $view = JFactory::getApplication()->input->getString('view');
        $option = JFactory::getApplication()->input->getString('option');
        $id = JFactory::getApplication()->input->getInt('ad_id');

        return $view == 'previsualize' && $option == 'com_advertisementfactory' && $id;
    }
}
