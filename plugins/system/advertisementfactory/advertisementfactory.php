<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

class plgSystemAdvertisementFactory extends JPlugin
{
    public function onAfterDispatch()
    {
        // Check if we're on the frontend.
        if (JFactory::getApplication()->isAdmin()) {
            return true;
        }

        // Initialise variables.
        $url = JUri::getInstance()->toString();
        $session = JFactory::getSession();
        $input = JFactory::getApplication()->input;
        $option = $input->getCmd('option');
        $view = $input->getCmd('view');

        // Store current url.
        if ('com_advertisementfactory' != $option || 'fullpagead' != $view) {
            $session->set('com_advertisementfactory.url', $url);
        }

        return true;
    }
}
