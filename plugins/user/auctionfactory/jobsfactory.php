<?php
	/**-----------------------------------------------------------------------------
	 * User plugin for Jobs Factory - Jobs Factory
	 * -----------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @package   : Jobs Factory
	 * @subpackage: Plugins/User/JobsFactory
	 **----------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
	/**
	 *
	 */
	defined('DS') || define('DS', DIRECTORY_SEPARATOR);
	defined('APP_EXTENSION') || define('APP_EXTENSION', 'com_jobsfactory');

	/**
	 * Class plgUserAuctionFactory
	 */
	class plgUserJobsFactory extends JPlugin
	{

		/**
		 * _is_valid
		 *
		 * @var bool
		 */
		protected $_is_valid = TRUE;
		/**
		 * @var BidConfig
		 */
		protected $cfg;

		/**
		 * extname
		 *
		 * @var string
		 */
		protected $extname = 'com_jobsfactory';

		/**
		 * @param object $subject
		 * @param array  $config
		 *
		 * @throws Exception
		 */
		public function __construct(&$subject, $config)
		{
			parent::__construct($subject, $config);

			$app                 = JFactory::getApplication();
			$front_extensionpath = JPATH_SITE . DS . '/components/' . $this->extname;
			$theFactoryAppPath   = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . $this->extname . DS . 'thefactory' . DS;
			$filepath            = $front_extensionpath . '/options.php';

			if ($app->isAdmin() || !file_exists($filepath)) {
				$this->_is_valid = FALSE;

				return FALSE;
			}

			//load component settings
			require_once($filepath);

		}

		/**
		 * onContentPrepareForm
		 *
		 * @param JForm $form
		 * @param       $data
		 *
		 * @return bool
		 * @throws Exception
		 */
		public function onContentPrepareForm($form, $data)
		{

			if (!$this->_is_valid) {
				return TRUE;
			}
			$app = JFactory::getApplication();
			if ($app->isAdmin()) {
				return TRUE;
			}
			if ($form->getName() != 'com_users.registration') {
				return TRUE;
			}

			$lang = JFactory::getLanguage();
			$lang->load($this->extname);

			$xml = "<fieldset name='jobsprofilefields'>";
			$xml .= "<field name='usergrouptype' type='list' description='COM_JOBS_PROFILE_TYPE_DESC'
			          label='COM_JOBS_PROFILE_TYPE_LABEL' required='false' size='1'>";
			$xml .= "<option value='1'>" . JText::_('COM_JOBS_FACTORY_COMPANY') . "</option>";
			$xml .= "<option value='2'>" . JText::_('COM_JOBS_FACTORY_CANDIDATE') . "</option>";
			$xml .= "<option value='0'>" . JText::_('COM_JOBS_FACTORY_NO_PREFERENCE') . "</option>";
			$xml .= "</field>";
			$xml .= "</fieldset>";
			$form->setField(new SimpleXMLElement($xml));

			return true;
		}

		/**
		 * xmlEscape
		 *
		 * @param $string
		 *
		 * @return mixed
		 */
		public static function xmlEscape($string)
		{
			return str_replace(array('&', '<', '>', '\'', '"'), array('&amp;', '&lt;', '&gt;', '&apos;', '&quot;'), $string);
		}

		/**
		 * onUserBeforeSave
		 *
		 * @param $oldUser
		 * @param $isNew
		 * @param $newUser
		 *
		 * @return bool|void
		 * @throws Exception
		 */
		public function onUserBeforeSave($oldUser, $isNew, $newUser)
		{
			/*
			if (!$this->_is_valid) {
				return TRUE;
			}

			$app    = JFactory::getApplication();
			$db     = JFactory::getDbo();
			$post   = $app->input->getArray($_POST);
			$option = $app->input->getWord('option');

			if (!defined('APP_PREFIX')) define('APP_PREFIX', 'bid');
			JLoader::discover('AuctionfactoryHelper', JPATH_ROOT . DS . 'components' . DS . $this->extname . DS . 'helpers');

			// Masquerade $_FILES items with type image in order to prevent CF handler to upload them. See FactoryFieldsTbl -> bind()
			// Handler for files related to CF image type is defined this on onUserAfterSave()

			if ('com_users' == $option && isset($_FILES['jform'])) {
				$_FILES = AuctionfactoryHelperTools::reformatMultiFileArray($_FILES['jform']);
			}

			if (count($_FILES)) {
				foreach ($_FILES as $CFName => $fileData) {
					$imageExt = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'tiff');
					$ext      = JFile::getExt($fileData['name']);

					// Masquerade only images
					if (in_array(strtolower($ext), $imageExt)) {
						$_FILES[$CFName . '__protect'] = $fileData;
						unset($_FILES[$CFName]);
					}
				}
			}

			if (isset($post['jform']) && is_array($post['jform'])) {

				foreach ($post['jform'] as $k => $v) {
					$_POST[$k] = $v;
				}
				unset($_POST['jform']);
			}

			// Save the country name instead of country id in user profile in case Joomla registration is used
			if ('com_users' == $option && isset($_POST['country']) && is_numeric($_POST['country'])) {
				$query = $db->getQuery(TRUE);
				$query->select('name')->from('#__bid_country')->where('id=' . $db->quote($_POST['country']));
				$db->setQuery($query);
				$_POST['country'] = $db->loadResult();
			}


			if ('com_users' != $option || 'com_auctionfactory' != $option) {
				return TRUE;
			}

			// @var $bidprofile JTableBidUser
			$bidprofile = JTable::getInstance('biduser');

			$this->bind2Profile($bidprofile, $_POST);

			if (!$bidprofile->check()) {
				$app->redirect('index.php?option=' . $this->extname . '&task=registerform');

				return;
			}
			*/
		}

		/**
		 * onUserAfterSave
		 *
		 * @param $user
		 * @param $isnew
		 * @param $success
		 * @param $msg
		 *
		 * @return bool
		 */
		public function onUserAfterSave($user, $isnew, $success, $msg)
		{

			if (!$this->_is_valid) {
				return TRUE;
			}
			/*
			jimport('joomla.filesystem.file');
			require_once(JPATH_SITE . '/components/' . $this->extname . '/thefactory/front.images.php');
			$imgTrans = new JTheFactoryImages();

			$lang = JFactory::getLanguage();
			$lang->load($this->extname);
			// Holder for all processed fields which are required to be appended to user profile
			$fieldsForProfileTemp = array();
			*/

			$front_extensionpath = JPATH_SITE . DS . '/components/' . $this->extname;
			$theFactoryAppPath   = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . $this->extname . DS . 'thefactory' . DS;
			$filepath            = $front_extensionpath . '/options.php';

			require_once($filepath);
			require_once(JPATH_SITE . '/components/com_jobsfactory/helpers/user.php');
			require_once(JPATH_SITE . '/components/com_jobsfactory/thefactory/front.helper.php');
			require_once(JPATH_SITE . '/administrator/components/com_jobsfactory/thefactory/application/application.class.php');

			//C:\wamp\www\dev\jobs\administrator\components\com_jobsfactory\thefactory\application\application.class.php

			JLoader::register('FactoryFieldsTbl', $theFactoryAppPath . 'fields/tables/table.class.php');
			JLoader::register('CustomFieldsFactory', $theFactoryAppPath . 'fields/helper/factory.class.php');
			JLoader::register('FactoryFieldTypes', $theFactoryAppPath . 'fields/plugins/field_types.php');
			JLoader::register('FactoryFieldValidator', $theFactoryAppPath . 'fields/plugins/field_validator.php');
			JLoader::register('JTheFactoryListModel', $theFactoryAppPath . 'fields/models/listmodel.php');
			JHtml::addIncludePath($theFactoryAppPath . 'fields/html');
			JTable::addIncludePath($theFactoryAppPath . 'fields/tables');

			if ($user['id'] && $success && $user['usergrouptype'])
			{
				$db = JFactory::getDbo();
				$new_user = new stdClass();
				$new_user->userid = $user['id'];
				$new_user->isCompany = $user['usergrouptype'] ? (string)'1' : (string)'2';

				$db->insertObject('#__jobsfactory_users', $new_user);

				JobsHelperUser::setUserGroup($user['id'], (string)$user['usergrouptype']);
			}

			return true;
		}

		/**
		 * onUserAfterDelete
		 *
		 * @param $user
		 * @param $succes
		 * @param $msg
		 *
		 * @return bool
		 */
		/*public function onUserAfterDelete($user, $succes, $msg)
		{
			if (!$succes) {
				JError::raiseWarning(1, 'Could not delete jobs for userid=' . $user['id']);

				return FALSE;
			}

			$db = JFactory::getDbo();

			$db->setQuery('DELETE FROM `#__jobs_jobs` WHERE `userid`=' . (int)$user['id']);
			$db->execute();

			return TRUE;
		}*/
	}
