<?php
	/**-----------------------------------------------------------------------------
	 * plg_user_adsfactory - Ads Factory
	 * -----------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @package   : Ads Factory
	 * @subpackage: Plugins/User/Ads
	 **----------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.plugin.plugin');
	/**
	 * Directory separator
	 */
	defined('DS') || define('DS', DIRECTORY_SEPARATOR);

	/**
	 * Class plgUserRbids
	 */
	class plgUserAds extends JPlugin
	{

		/**
		 * @var bool
		 */
		protected $_is_valid = TRUE;
		/**
		 * @var SwapConfig
		 */
		protected $cfg;

		protected $extname = 'com_adsfactory';

		/**
		 * @param object $subject
		 * @param array  $config
		 */
		function __construct(&$subject, $config)
		{

			parent::__construct($subject, $config);

			$app                 = JFactory::getApplication();
			$admin_extensionpath = JPATH_ADMINISTRATOR . DS . '/components/' . $this->extname;
			$front_extensionpath = JPATH_SITE . DS . '/components/' . $this->extname;

			$optionfile = $front_extensionpath . '/options.php';
			if ($app->isAdmin() || !file_exists($optionfile)) {
				$this->_is_valid = FALSE;

				return FALSE;
			}

			//load component settings
			/** @noinspection PhpIncludeInspection */
			require_once($optionfile);
			$this->cfg = new AdsConfig();

			// I assume this is same as registration_mode from reverse.
			$reg_mode = isset($this->cfg->profile_mode) ? $this->cfg->profile_mode : 'joomla';

			if ('component' != $reg_mode && !$this->params->get('integrate', FALSE)) {
				$this->_is_valid = FALSE;

				return FALSE;
			}

			defined('APP_PREFIX') || define('APP_PREFIX', 'ads');
			JTable::addIncludePath($admin_extensionpath . '/tables');
			JTable::addIncludePath($admin_extensionpath . '/thefactory/fields/tables');
			JLoader::register('FactoryFieldsTbl', $admin_extensionpath . '/thefactory/fields/tables/table.class.php');
			JLoader::register('CustomFieldsFactory', $admin_extensionpath . '/thefactory/fields/helper/factory.class.php');
			JLoader::register('FactoryFieldTypes', $admin_extensionpath . '/thefactory/fields/plugins/field_types.php');
			JLoader::register('FactoryFieldValidator', $admin_extensionpath . '/thefactory/fields/plugins/field_validator.php');
			JLoader::register('JTheFactoryListModel', $admin_extensionpath . '/thefactory/fields/models/listmodel.php');
			JLoader::register('JTheFactoryApplication', $admin_extensionpath . '/thefactory/application/application.class.php');


		}

		public function onContentPrepareForm($form, $data)
		{
			if (!$this->_is_valid) {
				return TRUE;
			}
			$app = JFactory::getApplication();
			if ($app->isAdmin()) {
				return TRUE;
			}
			if ($form->getName() != 'com_users.registration') {
				return TRUE;
			}
			$db   = JFactory::getDbo();
			$lang = JFactory::getLanguage();
			$lang->load($this->extname);

			$MyApp  = JTheFactoryApplication::getInstance(JPATH_SITE . '/administrator/components/' . $this->extname . '/application.ini');
			$fields = CustomFieldsFactory::getFieldsList("user_profile");
			$form->removeField('name');

			$xml = "<fieldset name='adsprofilefields'>";
			$xml .= "<field name='name' type='text' description='COM_USERS_REGISTER_NAME_DESC'
			         filter='string' label='COM_USERS_REGISTER_NAME_LABEL' required='true' size='30'/>";
			$xml .= "<field name='surname' type='text' label='COM_ADS_SURNAME' filter='string'  required='true' size='30'/>";
			$xml .= "</fieldset>";
			$form->setField(new SimpleXMLElement($xml));

			if (count($fields) > 0 && $this->params->get('showcustomfields', TRUE)) {
				foreach ($fields as $field) {
					$field_type   = CustomFieldsFactory::getFieldType($field->ftype);
					$field_object = JTable::getInstance('FieldsTable', 'JTheFactory');
					$field_object->bind($field);

					$xml = "<fieldset name='adscustomfields'>";

					$fieldid = "user_profile_" . $field->db_name;
					$fieldid = strtolower($fieldid);
					$fieldid = str_replace(array(" ", "'", '"', "(", ")", ".", "!", "?"), "_", $fieldid);


					$fld = "<field type='{ftype}' name='{fname}' label='{label}' description='{desc}' required='{req}'  multiple='{multiple}' class='{class}' id='{id}' {params}>";
					$fld = str_replace("{fname}", self::xmlEscape($field->db_name), $fld);
					$fld = str_replace("{label}", self::xmlEscape($field->name), $fld);
					$fld = str_replace("{desc}", self::xmlEscape($field->help), $fld);
					$fld = str_replace("{req}", $field->compulsory ? 'true' : 'false', $fld);
					$fld = str_replace("{class}", self::xmlEscape($field->css_class), $fld);
					$fld = str_replace("{id}", self::xmlEscape($fieldid), $fld);

					$params = "";
					switch ($field->ftype) {
						case 'checkBox':
							$options = $field_object->getOptions();
							if (count($options) > 1) {
								$fld = str_replace("{ftype}", 'checkboxes', $fld);
								foreach ($options as $option) {
									$fld .= "<option value='{$option->option_name}'>{$option->option_name}</option>";
								}

							} elseif (count($options) == 1) {
								$params = "value='" . $options[0]->option_name . "'";
							}
							$fld = str_replace("{ftype}", 'checkbox', $fld);
							break;
						case 'date':
							$fld = str_replace("{ftype}", 'calendar', $fld);
//                            $format=$field_type->dateFormatConversion($field_object->getParam('date_format', 'Y-m-d'));
//                            $size=$field_object->getParam('size', '15');
//                            $params="maxlength='$size' format='$format'";                            
							break;
						case 'hidden':
							$fld    = str_replace("{ftype}", 'hidden', $fld);
							$val    = $field_object->getParam('default_value', '');
							$params = "default='$val'";
							break;
						case 'image':
							$fld = str_replace("{ftype}", 'file', $fld);
							break;
						case 'radioBox':
							$options = $field_object->getOptions();
							if (count($options) > 0) {
								foreach ($options as $option) {
									$fld .= "<option value='{$option->option_name}'>{$option->option_name}</option>";
								}

							}
							$fld = str_replace("{ftype}", 'radio', $fld);
							break;
						case 'selectList':
							$fld  = str_replace("{ftype}", 'list', $fld);
							$size = $field_object->getParam('size', '');
							if ($size) $params = "size='$size'";
							$options = $field_object->getOptions();
							if (count($options) > 0) {
								foreach ($options as $option) {
									$fld .= "<option value='{$option->option_name}'>{$option->option_name}</option>";
								}

							}
							break;
						case 'selectmultiple':
							$fld  = str_replace("{ftype}", 'list', $fld);
							$fld  = str_replace("{multiple}", "true", $fld);
							$size = $field_object->getParam('size', '');
							if ($size) $params = "size='$size'";
							$options = $field_object->getOptions();
							if (count($options) > 0) {
								foreach ($options as $option) {
									$fld .= "<option value='{$option->option_name}'>{$option->option_name}</option>";
								}

							}
							break;
						case 'editor':
						case 'textArea':
							$fld    = str_replace("{ftype}", 'textarea', $fld);
							$cols   = $field_object->getParam('cols', 60);
							$rows   = $field_object->getParam('rows', 15);
							$params = "rows='$rows' cols='$cols'";
							break;
						default:
						case 'inputBox':
							$fld  = str_replace("{ftype}", 'text', $fld);
							$size = $field_object->getParam('size', '');
							if ($size) $params = "size='$size'";
							break;

					}
					$fld = str_replace("{multiple}", "false", $fld);
					$fld = str_replace("{params}", $params, $fld);

					$xml .= $fld . "</field>
            				</fieldset>
            				";
					$form->setField(new SimpleXMLElement($xml));
				}
			}

			if ($this->params->get('showaddressfields', TRUE)) {
				$xml = "<fieldset name='adsaddressfields'>";
				$xml .= "<field name='address' type='text' label='COM_ADS_ADDRESS' filter='string'  size='30'/>";
				$xml .= "<field name='city' type='text' label='COM_ADS_CITY' filter='string'  size='30'/>";
				$db->setQuery("SELECT `name` AS `text`,`id` AS `value`
    						  FROM `#__" . APP_PREFIX . "_country`
    						  WHERE `active`= 1 ORDER BY `name` ASC");
				$countries = $db->loadObjectList();
				if (count($countries) > 0) {
					$xml .= "<field name='country' type='list' label='COM_ADS_COUNTRY' filter='string'>";
					foreach ($countries as $option) {
						$xml .= "<option value='{$option->text}'>{$option->text}</option>";
					}
					$xml .= "</field>";

				}
				$xml .= "<field name='phone' type='text' label='COM_ADS_PHONE' filter='string'  size='30'/>";
				$xml .= "</fieldset>";
				$form->setField(new SimpleXMLElement($xml));
			}
			/*
			if ($this->params->get('showaboutme', TRUE)) {
				$xml = "<fieldset name='adsaboutmefields'>";

				$db->setQuery("SELECT `catname` AS `text`,`id` AS `value`
    						  FROM `#__" . APP_PREFIX . "_categories`
    						  WHERE `status`= 1 AND `parent` = 0 ORDER BY `catname` ASC");
				$categories = $db->loadObjectList();
				if (count($categories) > 0) {
					$xml .= "<field name='activity_domains' type='list' label='COM_ADS_ACTIVITY_DOMAINS' filter='string'  size='5' multiple='true'>";
					foreach ($categories as $option) {
						$xml .= "<option value='{$option->value}'>{$option->text}</option>";
					}
					$xml .= "</field>";
				}
				$xml .= "<field name='about_me' type='textarea' label='COM_ADS_ABOUT_ME' filter='string'  cols='70' rows='10'/>";
				$xml .= "</fieldset>";
				$form->setField(new SimpleXMLElement($xml));
			}*/
			/*if ($this->cfg->allow_paypal && $this->params->get('showpaypalemail', TRUE)) {
				$xml = "<fieldset name='adssocialfields'><field name='paypalemail' type='text' label='COM_ADS_PAYPAL_EMAIL' size='30'/></fieldset>";
				$form->setField(new SimpleXMLElement($xml));
			}*/
		}

		public static function xmlEscape($string)
		{
			return str_replace(array('&', '<', '>', '\'', '"'), array('&amp;', '&lt;', '&gt;', '&apos;', '&quot;'), $string);
		}


		/**
		 * @param $oldUser
		 * @param $isNew
		 * @param $newUser
		 *
		 * @return bool
		 */
		public function onUserBeforeSave($oldUser, $isNew, $newUser)
		{
			return;
			if (!$this->_is_valid) {
				return TRUE;
			}

			$app    = JFactory::getApplication();
			$input  = $app->input;
			$option = $input->getWord('option');

			$profile_array = ($option == 'com_users') ? ($input->post->get('jform', array(), 'array')) : $input->getArray($_POST);

			/* @var $adsprofile TableUsers */
			$adsprofile = JTable::getInstance('users', 'Table');

			$this->bind2Profile($adsprofile, $profile_array);

			if (!$adsprofile->check()) {

				$app->redirect(
					($option == 'com_users') ?
						JRoute::_('index.php?option=com_users&view=registration') :
						JRoute::_('index.php?option=com_adsfactory&controller=user&task=registerform')
				);

				return FALSE;
			}
		}

		/**
		 * @param $user
		 * @param $isnew
		 * @param $success
		 * @param $msg
		 *
		 * @return bool
		 */
		public function onUserAfterSave($user, $isnew, $success, $msg)
		{

			if (!$this->_is_valid) {
				return TRUE;
			}
			jimport('joomla.filesystem.file');
			require_once(JPATH_SITE . '/components/' . $this->extname . '/thefactory/front.images.php');
			$imgTrans = new JTheFactoryImages();

			$lang = JFactory::getLanguage();
			$lang->load($this->extname);

			if ($user['id'] && $success) {

				$db            = JFactory::getDbo();
				$app           = JFactory::getApplication();
				$input         = $app->input;
				$option        = $input->getWord('option');
				$profile_array = ($option == 'com_users') ? ($input->post->get('jform', array(), 'array')) : $input->getArray($_POST);
				// Reverse users table
				$bidprofile = JTable::getInstance('users', 'Table');
				//new profile
				if (!$bidprofile->load($user['id'])) {
					$db->setQuery("INSERT INTO `#__" . APP_PREFIX . "_users` (`userid`) VALUES (" . $db->quote($user['id']) . ")");
					$db->execute();
				}

				$bidprofile->userid = $user['id'];

				// Prepare activity_domains to be saved to db
				/*if (isset($profile_array['activity_domains'])) {
					$profile_array['activity_domains'] = implode(',', $profile_array['activity_domains']);
				}*/
				$fields = CustomFieldsFactory::getFieldsList("user_profile");


				// Save all custom fields defined as image type

				foreach ($fields as $fld) {

					// No image uploaded so go to bind 2 profile
					if (!count($_FILES['jform']['name'])) {
						continue;
					}

					if ('image' != $fld->ftype && 'user_profile' != $fld->page) {
						continue;
					}
					// Get CF params
					$params   = new Joomla\Registry\Registry($fld->params);
					$CFParams = $params->toObject();

					// Check if extension match with expected
					$allowedExtensions = explode(',', $CFParams->file_extensions);
					$extMatch          = FALSE;


					foreach ($_FILES['jform']['name'] as $CFname => $fName) {

						// Check form file field match current CF
						if ($CFname != $fld->db_name) {
							continue;
						}

						$ext = JFile::getExt($fName);

						// Check if file is allowed
						if (count($allowedExtensions)) {

							if (!$extMatch && in_array(strtolower($ext), $allowedExtensions)) {
								$extMatch = TRUE;
							}
						} else {
							// Change extMatch flag in order to accept file
							$extMatch = TRUE;
						}

						// We do not have to save this file
						if (!$extMatch) {
							JFactory::getApplication()->enqueueMessage(
								JText::sprintf('COM_ADS_ERR_EXTENSION_FOR_UPLOADED_FILE', $fld->name, $allowedExtensions),
								'warning'
							);
							continue;
						}

						// Finalize uploaded image
						$upload_path = $CFParams->upload_path ? $CFParams->upload_path : 'media/' . $this->extname . '/files/' . trim($fld->db_name);

						$time       = time();
						$fileName   = JFile::makesafe('fld-db_name-' . trim($fld->db_name) . '-uid-' . $bidprofile->userid . '-t-' . $time . ".$ext");
						$thFileName = JFile::makesafe('th-fld-db_name-' . trim($fld->db_name) . '-uid-' . $bidprofile->userid . '-t-' . $time . ".$ext");

						JFile::upload($_FILES['jform']['tmp_name'][$CFname], $upload_path . '/' . $fileName);
						JFile::copy($upload_path . '/' . $fileName, $upload_path . '/' . $thFileName);

						// Resize to CF sizes definition
						$imgTrans->resize_image_no_prefix($upload_path . '/' . $fileName, $CFParams->image_width - 10, $CFParams->image_height - 10);
						$imgTrans->resize_image_no_prefix($upload_path . '/' . $thFileName, $CFParams->thumbnail_width - 10, $CFParams->thumbnail_height - 10);

						// Add field name in user profile
						$profile_array[$fld->db_name] = $fileName;
					}
				}

				$this->bind2Profile($bidprofile, $profile_array);

				//hack - because the html input is callded jform[name]
				$bidprofile->name = $user['name'];
				$bidprofile->store();
			}
		}

		/**
		 * @param $user
		 * @param $succes
		 * @param $msg
		 *
		 * @return bool
		 */
		public function onUserAfterDelete($user, $succes, $msg)
		{
			if (!$succes) {
				JError::raiseWarning(1, 'Could not delete ads for `userid` = ' . $user['id']);

				return FALSE;
			}

			$db = JFactory::getDbo();


			$db->setQuery('DELETE FROM `#__ads_report` WHERE `userid` = ' . (int)$user['id']);
			$db->execute();

			$db->setQuery('DELETE FROM `#__ads_users` WHERE `userid` = ' . (int)$user['id']);
			$db->execute();

			$db->setQuery('DELETE FROM `#__ads_watchlist` WHERE `userid` = ' . (int)$user['id']);
			$db->execute();

			$db->setQuery('UPDATE `#__ads` SET `userid` = NULL WHERE `userid` = ' . (int)$user['id']);
			$db->execute();

			return TRUE;
		}

		/**
		 * @param $rowProfile
		 * @param $data
		 */
		protected function bind2Profile(&$rowProfile, $data)
		{
			return;
			$rowProfile->bind($data);
			$rowProfile->modified = gmdate('Y-m-d H:i:s');

			$notEditable = array('verified', 'isBidder', 'isBuyer', 'powerbuyer');
			foreach ($notEditable as $f) {
				$rowProfile->$f = NULL;
			}
		}
	}
