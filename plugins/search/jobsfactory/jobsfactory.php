<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.5.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/


defined( '_JEXEC' ) or die( 'Restricted access' );
if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

class plgSearchJobsfactory extends JPlugin {

    public function __construct(& $subject, $config) {

        parent::__construct($subject, $config);
        $this->loadLanguage();
    }

    function onContentSearchAreas() {

        static $areas = array(
        'jobsfactory' => 'PLG_SEARCH_JOBSFACTORY'
        );
        return $areas;
    }

    function onContentSearch($text, $phrase='', $ordering='', $areas=null) {
        $db		= JFactory::getDBO();
        $user	= JFactory::getUser();

        $searchText = $text;

        $limit = $this->params->def( 'search_limit', 50 );

        $text = trim( $text );
        if ($text == '') {
            return array();
        }
        $section 	= JText::_( 'Jobs Factory' );

        $wheres 	= array();
        switch ($phrase)
        {
            case 'exact':
                $text		= $db->Quote( '%'.$db->escape( $text, true ).'%', false );
                $wheres2 	= array();
                $wheres2[] 	= 'a.shortdescription LIKE '.$text;
                $wheres2[] 	= 'a.description LIKE '.$text;
                $wheres2[] 	= 'a.title LIKE '.$text;
                $where 		= '(' . implode( ') OR (', $wheres2 ) . ')';
                break;

            case 'all':
            case 'any':
            default:
                $words 	= explode( ' ', $text );
                $wheres = array();
                foreach ($words as $word)
                {
                    $word		= $db->Quote( '%'.$db->escape( $word, true ).'%', false );
                    $wheres2 	= array();
                    $wheres2[] 	= 'a.shortdescription LIKE '.$word;
                    $wheres2[] 	= 'a.description LIKE '.$word;
                    $wheres2[] 	= 'a.title LIKE '.$word;
                    $wheres[] 	= implode( ' OR ', $wheres2 );
                }
                $where 	= '(' . implode( ($phrase == 'all' ? ') AND (' : ') OR ('), $wheres ) . ')';
                break;
        }

        switch ( $ordering ) {
            case 'alpha':
                $order = 'title ASC';
                break;

            case 'category':
                $order = 'catname ASC, title ASC';
                break;

            case 'oldest':
                $order = 'start_date ASC, title ASC';
                break;
            case 'newest':
                $order = 'start_date DESC, title ASC';
                break;
            case 'popular':
            default:
                $order = 'hits DESC';
        }
        require_once (JPATH_ROOT.DS.'components'.DS.'com_jobsfactory'.DS.'helpers'.DS.'tools.php');
		
        $tmp_id = JobsHelperTools::getMenuItemId(array("task"=>"listjobs"));
        if( !$tmp_id )
          $tmp_id = JobsHelperTools::getMenuItemId(array("view"=>"jobs", "layout"=>""));

        $query = "SELECT title AS title,"
        . "\n start_date AS created,"
        . "\n CONCAT(a.shortdescription,' ',a.description) AS text,"
        . "\n concat('Cat: ',catname) AS section,"
        . "\n CONCAT( 'index.php?option=com_jobsfactory&task=viewapplications&Itemid={$tmp_id}&id=', a.id ) AS href,"
        . "\n '1' AS browsernav"
        . "\n FROM #__jobsfactory_jobs a "
        . "\n LEFT JOIN #__jobsfactory_categories AS b ON b.id = a.cat"
        . "\n WHERE ( $where )"
        . "\n AND a.close_offer <> 1 AND a.close_by_admin <> 1 AND a.start_date<now()"
        . "\n ORDER BY $order"
        ;

        $db->setQuery( $query, 0, $limit );
        $rows = $db->loadObjectList();

        $return = array();
        foreach($rows AS $key => $weblink) {
            if(searchHelper::checkNoHTML($weblink, $searchText, array('url', 'text', 'title'))) {
                $return[] = $weblink;
            }
        }

        return $return;
    }
}