-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12 Apr 2017 la 09:45
-- Versiune server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vet1_calisto1`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_advertisementfactory_ads`
--

CREATE TABLE `sorin_advertisementfactory_ads` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `contents` mediumtext NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `url` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `module_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `params` mediumtext NOT NULL,
  `history` mediumtext NOT NULL,
  `price_type` varchar(20) NOT NULL,
  `purchased` int(11) NOT NULL,
  `served` int(11) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_advertisementfactory_gateways`
--

CREATE TABLE `sorin_advertisementfactory_gateways` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL DEFAULT '',
  `enabled` tinyint(4) DEFAULT '1',
  `logo` varchar(255) DEFAULT NULL,
  `params` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_advertisementfactory_messages`
--

CREATE TABLE `sorin_advertisementfactory_messages` (
  `id` int(11) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` mediumtext NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_advertisementfactory_notifications`
--

CREATE TABLE `sorin_advertisementfactory_notifications` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` mediumtext NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_advertisementfactory_orders`
--

CREATE TABLE `sorin_advertisementfactory_orders` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `price_saved` mediumtext NOT NULL,
  `params` mediumtext NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currency` varchar(20) NOT NULL,
  `gateway` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_advertisementfactory_payments`
--

CREATE TABLE `sorin_advertisementfactory_payments` (
  `id` int(11) NOT NULL,
  `received_at` datetime NOT NULL,
  `payment_date` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `gateway` varchar(255) NOT NULL,
  `log` mediumtext NOT NULL,
  `refnumber` varchar(255) NOT NULL,
  `errors` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_advertisementfactory_prices`
--

CREATE TABLE `sorin_advertisementfactory_prices` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `price_type` varchar(20) NOT NULL,
  `value` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `module_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_advertisementfactory_track`
--

CREATE TABLE `sorin_advertisementfactory_track` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `referer` varchar(400) NOT NULL,
  `url` varchar(400) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_assets`
--

CREATE TABLE `sorin_assets` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) UNSIGNED NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_assets`
--

INSERT INTO `sorin_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 205, 0, 'root.1', 'Root Asset', '{\"core.login.site\":{\"6\":1,\"2\":1},\"core.login.admin\":{\"6\":1},\"core.login.offline\":{\"6\":1},\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1},\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 18, 1, 'com_contact', 'com_contact', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(8, 1, 19, 42, 1, 'com_content', 'com_content', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(9, 1, 43, 44, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 45, 46, 1, 'com_installer', 'com_installer', '{\"core.admin\":[],\"core.manage\":{\"7\":0},\"core.delete\":{\"7\":0},\"core.edit.state\":{\"7\":0}}'),
(11, 1, 47, 48, 1, 'com_languages', 'com_languages', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(12, 1, 49, 50, 1, 'com_login', 'com_login', '{}'),
(13, 1, 51, 52, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 53, 54, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 55, 56, 1, 'com_media', 'com_media', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":{\"5\":1}}'),
(16, 1, 57, 68, 1, 'com_menus', 'com_menus', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(17, 1, 69, 70, 1, 'com_messages', 'com_messages', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(18, 1, 71, 160, 1, 'com_modules', 'com_modules', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(19, 1, 161, 164, 1, 'com_newsfeeds', 'com_newsfeeds', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(20, 1, 165, 166, 1, 'com_plugins', 'com_plugins', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(21, 1, 167, 168, 1, 'com_redirect', 'com_redirect', '{\"core.admin\":{\"7\":1},\"core.manage\":[]}'),
(22, 1, 169, 170, 1, 'com_search', 'com_search', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(23, 1, 171, 172, 1, 'com_templates', 'com_templates', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(24, 1, 173, 176, 1, 'com_users', 'com_users', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(26, 1, 177, 178, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 20, 23, 2, 'com_content.category.2', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(30, 19, 162, 163, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(32, 24, 174, 175, 1, 'com_users.category.7', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(33, 1, 179, 180, 1, 'com_finder', 'com_finder', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(34, 1, 181, 182, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{\"core.admin\":[],\"core.manage\":[],\"core.delete\":[],\"core.edit.state\":[]}'),
(35, 1, 183, 184, 1, 'com_tags', 'com_tags', '{\"core.admin\":[],\"core.manage\":[],\"core.manage\":[],\"core.delete\":[],\"core.edit.state\":[]}'),
(36, 1, 185, 186, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 187, 188, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 189, 190, 1, 'com_postinstall', 'com_postinstall', '{}'),
(40, 18, 72, 73, 2, 'com_modules.module.2', 'Login', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(41, 18, 74, 75, 2, 'com_modules.module.3', 'Popular Articles', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(42, 18, 76, 77, 2, 'com_modules.module.4', 'Recently Added Articles', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(43, 18, 78, 79, 2, 'com_modules.module.8', 'Toolbar', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(44, 18, 80, 81, 2, 'com_modules.module.9', 'Quick Icons', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(45, 18, 82, 83, 2, 'com_modules.module.10', 'Logged-in Users', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(46, 18, 84, 85, 2, 'com_modules.module.12', 'Admin Menu', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(47, 18, 86, 87, 2, 'com_modules.module.13', 'Admin Submenu', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(48, 18, 88, 89, 2, 'com_modules.module.14', 'User Status', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(49, 18, 90, 91, 2, 'com_modules.module.15', 'Title', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(52, 18, 92, 93, 2, 'com_modules.module.79', 'Multilanguage status', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(53, 18, 94, 95, 2, 'com_modules.module.86', 'Joomla Version', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(54, 1, 191, 192, 1, 'com_gantry5', 'com_gantry5', '{}'),
(55, 1, 193, 194, 1, 'com_roksprocket', 'RokSprocket', '{}'),
(56, 18, 96, 97, 2, 'com_modules.module.87', 'RokSprocket Module', ''),
(57, 18, 98, 99, 2, 'com_modules.module.88', 'FP RokSprocket Features - Slideshow', '{}'),
(58, 18, 100, 101, 2, 'com_modules.module.89', 'RokAjaxSearch', ''),
(59, 18, 102, 103, 2, 'com_modules.module.90', 'Search Our Site', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"module.edit.frontend\":[]}'),
(60, 18, 104, 105, 2, 'com_modules.module.91', 'FP RokSprocket Tabs', ''),
(61, 18, 106, 107, 2, 'com_modules.module.92', 'FP RokSprocket Headlines', '{}'),
(62, 18, 108, 109, 2, 'com_modules.module.93', 'FP RokSprocket Strips', '{}'),
(63, 18, 110, 111, 2, 'com_modules.module.94', 'FP RokSprocket Lists', ''),
(64, 18, 112, 113, 2, 'com_modules.module.95', 'FP RokSprocket Features - Slideshow (copy)', ''),
(65, 8, 24, 41, 2, 'com_content.category.8', 'Sample Blog', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(66, 65, 25, 26, 3, 'com_content.article.1', 'Completely pursue scalable customer service ', '{\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1}}'),
(67, 65, 27, 28, 3, 'com_content.article.2', ' Globally incubate standards compliant channels ', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(68, 65, 29, 30, 3, 'com_content.article.3', 'Interactively procrastinate high-payoff content ', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(69, 65, 31, 32, 3, 'com_content.article.4', 'Credibly innovate granular internal ', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(70, 65, 33, 34, 3, 'com_content.article.5', 'Quickly maximize timely deliverables for real-time schemas', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(71, 65, 35, 36, 3, 'com_content.article.6', 'Dramatically visualize customer directed convergence without media value', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(72, 65, 37, 38, 3, 'com_content.article.7', 'Dynamically procrastinate B2C users after installed base benefits', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(73, 65, 39, 40, 3, 'com_content.article.8', 'Collaboratively administrate empowered markets via plug-and-play networks', '{\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1}}'),
(74, 18, 114, 115, 2, 'com_modules.module.96', 'Main Menu', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"module.edit.frontend\":[]}'),
(75, 18, 116, 117, 2, 'com_modules.module.97', 'Login', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"module.edit.frontend\":[]}'),
(76, 18, 118, 119, 2, 'com_modules.module.98', 'Who\'s Online', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"module.edit.frontend\":[]}'),
(77, 7, 16, 17, 2, 'com_contact.category.9', 'Sample Contact', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(78, 18, 120, 121, 2, 'com_modules.module.99', 'Gantry 5 Particle', ''),
(79, 1, 195, 196, 1, 'com_jobsfactory', 'JobsFactory', '{}'),
(80, 16, 58, 59, 2, 'com_menus.menu.16', 'Jobs MainMenu', '{}'),
(81, 18, 122, 123, 2, 'com_modules.module.100', 'Jobs MainMenu', '{}'),
(82, 18, 124, 125, 2, 'com_modules.module.101', 'Joburi Promovate', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(83, 18, 126, 127, 2, 'com_modules.module.102', 'jobsFactory Filters module', '{}'),
(84, 18, 128, 129, 2, 'com_modules.module.103', 'Ultimele Joburi', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(85, 18, 130, 131, 2, 'com_modules.module.104', 'jobsFactory Popular Jobs', '{}'),
(86, 18, 132, 133, 2, 'com_modules.module.105', 'jobsFactory Random Jobs', '{}'),
(87, 18, 134, 135, 2, 'com_modules.module.106', 'Cauta job-uri', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(88, 18, 136, 137, 2, 'com_modules.module.107', 'jobsFactory Tag Cloud', '{}'),
(89, 18, 138, 139, 2, 'com_modules.module.108', 'jobsFactory Category Tree Module', '{}'),
(90, 16, 60, 61, 2, 'com_menus.menu.17', 'Vet Menu', '{}'),
(96, 16, 62, 63, 2, 'com_menus.menu.18', 'Events Factory', '{}'),
(97, 18, 140, 141, 2, 'com_modules.module.112', 'Events Factory', '{}'),
(100, 18, 142, 143, 2, 'com_modules.module.113', 'Ads Factory Menu', '{}'),
(109, 27, 21, 22, 3, 'com_content.article.9', 'Welcome!', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(112, 18, 144, 145, 2, 'com_modules.module.122', 'Ads Factory Menu', '{}'),
(121, 16, 64, 65, 2, 'com_menus.menu.21', 'Vet2', '{}'),
(122, 1, 197, 198, 1, 'com_thephpfactoryupdate', 'thephpfactoryupdate', '{}'),
(123, 18, 146, 147, 2, 'com_modules.module.131', 'Search:', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(124, 1, 199, 200, 1, 'com_advertisementfactory', 'com_Advertisementfactory', '{\"frontend.manage\":{\"9\":1},\"backend.manage\":{\"6\":1,\"7\":1}}'),
(125, 16, 66, 67, 2, 'com_menus.menu.22', 'Advertisement Factory Menu', '{}'),
(126, 18, 148, 149, 2, 'com_modules.module.132', 'Advertisement Factory Menu', '{}'),
(127, 18, 150, 151, 2, 'com_modules.module.133', 'mod_advertisementfactory_banners', '{}'),
(128, 18, 152, 153, 2, 'com_modules.module.134', 'mod_advertisementfactory_links', '{}'),
(129, 18, 154, 155, 2, 'com_modules.module.135', 'mod_advertisementfactory_thumbnails', '{}'),
(130, 18, 156, 157, 2, 'com_modules.module.136', 'ultimelejoburi2', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(131, 18, 158, 159, 2, 'com_modules.module.137', 'Improved AJAX Login and Register', '{}'),
(132, 1, 201, 202, 1, 'com_improved_ajax_login', 'com_improved_ajax_login', '{}'),
(133, 1, 203, 204, 1, 'com_offlajn_installer', 'offlajn_installer', '{}');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_associations`
--

CREATE TABLE `sorin_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_banners`
--

CREATE TABLE `sorin_banners` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custombannercode` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sticky` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_banner_clients`
--

CREATE TABLE `sorin_banner_clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extrainfo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_banner_tracks`
--

CREATE TABLE `sorin_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) UNSIGNED NOT NULL,
  `banner_id` int(10) UNSIGNED NOT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_categories`
--

CREATE TABLE `sorin_categories` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_categories`
--

INSERT INTO `sorin_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 15, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 631, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 631, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 631, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 631, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 631, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 9, 10, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 631, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 65, 1, 11, 12, 1, 'sample-blog', 'com_content', 'Sample Blog', 'sample-blog', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 631, '2015-05-07 13:13:28', 0, '2015-05-07 13:13:28', 0, '*', 1),
(9, 77, 1, 13, 14, 1, 'sample-contact', 'com_contact', 'Sample Contact', 'sample-contact', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 631, '2015-05-07 13:35:26', 0, '2015-05-07 13:35:26', 0, '*', 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_contact_details`
--

CREATE TABLE `sorin_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_contact_details`
--

INSERT INTO `sorin_contact_details` (`id`, `name`, `alias`, `con_position`, `address`, `suburb`, `state`, `country`, `postcode`, `telephone`, `fax`, `misc`, `image`, `email_to`, `default_con`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`, `user_id`, `catid`, `access`, `mobile`, `webpage`, `sortname1`, `sortname2`, `sortname3`, `language`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `featured`, `xreference`, `publish_up`, `publish_down`, `version`, `hits`) VALUES
(1, 'Callisto', 'callisto', '', '', '', '', '', '', '', '', '', '', '', 0, 1, 0, '0000-00-00 00:00:00', 1, '{\"show_contact_category\":\"hide\",\"show_contact_list\":\"0\",\"presentation_style\":\"plain\",\"show_tags\":\"0\",\"show_name\":\"0\",\"show_position\":\"0\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"\",\"linka_name\":\"\",\"linka\":false,\"linkb_name\":\"\",\"linkb\":false,\"linkc_name\":\"\",\"linkc\":false,\"linkd_name\":\"\",\"linkd\":false,\"linke_name\":\"\",\"linke\":false,\"contact_layout\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\"}', 976, 4, 1, '', '', '', '', '', '*', '2015-05-07 13:36:45', 631, '', '2015-05-07 13:39:43', 976, '', '', '{\"robots\":\"\",\"rights\":\"\"}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 31);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_content`
--

CREATE TABLE `sorin_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_content`
--

INSERT INTO `sorin_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 66, 'Completely pursue scalable customer service ', 'completely-pursue-scalable-customer-service', '<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p>', '', 1, 8, '2015-05-07 13:18:05', 631, '', '2015-05-07 13:18:14', 976, 0, '0000-00-00 00:00:00', '2015-05-07 13:18:05', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 8, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(2, 67, ' Globally incubate standards compliant channels ', 'globally-incubate-standards-compliant-channels', '<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p>', '', 1, 8, '2015-05-07 13:18:05', 631, '', '2015-05-07 13:19:51', 0, 0, '0000-00-00 00:00:00', '2015-05-07 13:18:05', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 7, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(3, 68, 'Interactively procrastinate high-payoff content ', 'interactively-procrastinate-high-payoff-content', '<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p>', '', 1, 8, '2015-05-07 13:18:05', 631, '', '2015-05-07 13:20:13', 0, 0, '0000-00-00 00:00:00', '2015-05-07 13:18:05', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 6, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(4, 69, 'Credibly innovate granular internal ', 'credibly-innovate-granular-internal', '<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p>', '', 1, 8, '2015-05-07 13:18:05', 631, '', '2015-05-07 13:20:31', 0, 0, '0000-00-00 00:00:00', '2015-05-07 13:18:05', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 5, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(5, 70, 'Quickly maximize timely deliverables for real-time schemas', 'quickly-maximize-timely-deliverables-for-real-time-schemas', '<p><img alt=\"image\" src=\"templates/rt_callisto/images/demo/pages/pages/blog/img-04.jpg\"></p>\r\n<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p>\r\n<a class=\"button\" href=\"#\">Read More</a>', '', 1, 8, '2015-05-07 13:18:05', 631, '', '2015-05-07 13:20:49', 0, 0, '0000-00-00 00:00:00', '2015-05-07 13:18:05', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 4, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(6, 71, 'Dramatically visualize customer directed convergence without media value', 'dramatically-visualize-customer-directed-convergence-without-media-value', '<p><img alt=\"image\" src=\"templates/rt_callisto/images/demo/pages/pages/blog/img-03.jpg\"></p>\r\n<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p>\r\n<a class=\"button\" href=\"#\">Read More</a>', '', 1, 8, '2015-05-07 13:18:05', 631, '', '2015-05-07 13:21:10', 0, 631, '2017-03-09 14:56:31', '2015-05-07 13:18:05', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 3, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(7, 72, 'Dynamically procrastinate B2C users after installed base benefits', 'dynamically-procrastinate-b2c-users-after-installed-base-benefits', '<p><img alt=\"image\" src=\"templates/rt_callisto/images/demo/pages/pages/blog/img-02.jpg\"></p>\r\n<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p>\r\n<a class=\"button\" href=\"#\">Read More</a>', '', 1, 8, '2015-05-07 13:18:05', 631, '', '2015-05-07 13:21:37', 0, 0, '0000-00-00 00:00:00', '2015-05-07 13:18:05', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 2, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(8, 73, 'Collaboratively administrate empowered markets via plug-and-play networks', 'collaboratively-administrate-empowered-markets-via-plug-and-play-networks', '<p><img alt=\"image\" src=\"templates/rt_callisto/images/demo/pages/pages/blog/img-01.jpg\"></p>\r\n<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p>\r\n<a class=\"button\" href=\"#\">Read More</a>', '', 1, 8, '2015-05-07 13:18:05', 631, '', '2015-05-08 11:01:25', 976, 631, '2017-03-10 10:18:53', '2015-05-07 13:18:05', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 5, 1, '', '', 1, 11, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(9, 109, 'Welcome!', 'welcome', '<p>Welcome to Vetjobs.ro !!</p>\r\n<p> </p>\r\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!</p>\r\n<p>In order to apply for Jobs, you will first need to complete your Profile. Link is in the Main Menu.</p>\r\n<p style=\"text-align: left;\">Do you want to develop your career by attending or creating courses? You will have to first complete your profile <a href=\"index.php?option=com_eventsfactory&amp;view=profile.show&amp;Itemid=454\">here</a> . </p>\r\n<p style=\"text-align: left;\"> </p>\r\n<p style=\"text-align: left;\"> </p>\r\n<p style=\"text-align: left;\"> </p>', '', 1, 2, '2017-03-08 11:28:55', 631, '', '2017-03-14 12:37:33', 631, 0, '0000-00-00 00:00:00', '2017-03-08 11:28:55', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 10, 0, '', '', 1, 202, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 1, '*', '');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_contentitem_tag_map`
--

CREATE TABLE `sorin_contentitem_tag_map` (
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `core_content_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_content_frontpage`
--

CREATE TABLE `sorin_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_content_frontpage`
--

INSERT INTO `sorin_content_frontpage` (`content_id`, `ordering`) VALUES
(9, 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_content_rating`
--

CREATE TABLE `sorin_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_content_types`
--

CREATE TABLE `sorin_content_types` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_title` varchar(255) NOT NULL DEFAULT '',
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `table` varchar(255) NOT NULL DEFAULT '',
  `rules` text NOT NULL,
  `field_mappings` text NOT NULL,
  `router` varchar(255) NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) DEFAULT NULL COMMENT 'JSON string for com_contenthistory options'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_content_types`
--

INSERT INTO `sorin_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{\"special\":{\"dbtable\":\"#__content\",\"key\":\"id\",\"type\":\"Content\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"introtext\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"attribs\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"asset_id\"}, \"special\":{\"fulltext\":\"fulltext\"}}', 'ContentHelperRoute::getArticleRoute', '{\"formFile\":\"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(2, 'Contact', 'com_contact.contact', '{\"special\":{\"dbtable\":\"#__contact_details\",\"key\":\"id\",\"type\":\"Contact\",\"prefix\":\"ContactTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"address\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"image\", \"core_urls\":\"webpage\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"con_position\":\"con_position\",\"suburb\":\"suburb\",\"state\":\"state\",\"country\":\"country\",\"postcode\":\"postcode\",\"telephone\":\"telephone\",\"fax\":\"fax\",\"misc\":\"misc\",\"email_to\":\"email_to\",\"default_con\":\"default_con\",\"user_id\":\"user_id\",\"mobile\":\"mobile\",\"sortname1\":\"sortname1\",\"sortname2\":\"sortname2\",\"sortname3\":\"sortname3\"}}', 'ContactHelperRoute::getContactRoute', '{\"formFile\":\"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml\",\"hideFields\":[\"default_con\",\"checked_out\",\"checked_out_time\",\"version\",\"xreference\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[ {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ] }'),
(3, 'Newsfeed', 'com_newsfeeds.newsfeed', '{\"special\":{\"dbtable\":\"#__newsfeeds\",\"key\":\"id\",\"type\":\"Newsfeed\",\"prefix\":\"NewsfeedsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"numarticles\":\"numarticles\",\"cache_time\":\"cache_time\",\"rtl\":\"rtl\"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{\"formFile\":\"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(4, 'User', 'com_users.user', '{\"special\":{\"dbtable\":\"#__users\",\"key\":\"id\",\"type\":\"User\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"username\",\"core_created_time\":\"registerdate\",\"core_modified_time\":\"lastvisitDate\",\"core_body\":\"null\", \"core_hits\":\"null\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"access\":\"null\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"null\", \"core_language\":\"null\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"null\", \"core_ordering\":\"null\", \"core_metakey\":\"null\", \"core_metadesc\":\"null\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{}}', 'UsersHelperRoute::getUserRoute', ''),
(5, 'Article Category', 'com_content.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContentHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(6, 'Contact Category', 'com_contact.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContactHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(7, 'Newsfeeds Category', 'com_newsfeeds.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(8, 'Tag', 'com_tags.tag', '{\"special\":{\"dbtable\":\"#__tags\",\"key\":\"tag_id\",\"type\":\"Tag\",\"prefix\":\"TagsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\"}}', 'TagsHelperRoute::getTagRoute', '{\"formFile\":\"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"lft\", \"rgt\", \"level\", \"path\", \"urls\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(9, 'Banner', 'com_banners.banner', '{\"special\":{\"dbtable\":\"#__banners\",\"key\":\"id\",\"type\":\"Banner\",\"prefix\":\"BannersTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"null\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"imptotal\":\"imptotal\", \"impmade\":\"impmade\", \"clicks\":\"clicks\", \"clickurl\":\"clickurl\", \"custombannercode\":\"custombannercode\", \"cid\":\"cid\", \"purchase_type\":\"purchase_type\", \"track_impressions\":\"track_impressions\", \"track_clicks\":\"track_clicks\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"reset\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"imptotal\", \"impmade\", \"reset\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"cid\",\"targetTable\":\"#__banner_clients\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(10, 'Banners Category', 'com_banners.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(11, 'Banner Client', 'com_banners.client', '{\"special\":{\"dbtable\":\"#__banner_clients\",\"key\":\"id\",\"type\":\"Client\",\"prefix\":\"BannersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\"], \"ignoreChanges\":[\"checked_out\", \"checked_out_time\"], \"convertToInt\":[], \"displayLookup\":[]}'),
(12, 'User Notes', 'com_users.note', '{\"special\":{\"dbtable\":\"#__user_notes\",\"key\":\"id\",\"type\":\"Note\",\"prefix\":\"UsersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\"], \"convertToInt\":[\"publish_up\", \"publish_down\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(13, 'User Notes Category', 'com_users.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_core_log_searches`
--

CREATE TABLE `sorin_core_log_searches` (
  `search_term` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_extensions`
--

CREATE TABLE `sorin_extensions` (
  `extension_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_extensions`
--

INSERT INTO `sorin_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{\"name\":\"com_mailto\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MAILTO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mailto\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{\"name\":\"com_wrapper\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{\"name\":\"com_admin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_ADMIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{\"name\":\"com_banners\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"banners\"}', '{\"purchase_type\":\"3\",\"track_impressions\":\"0\",\"track_clicks\":\"0\",\"metakey_prefix\":\"\",\"save_history\":\"1\",\"history_limit\":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{\"name\":\"com_cache\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CACHE_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{\"name\":\"com_categories\",\"type\":\"component\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{\"name\":\"com_checkin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CHECKIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{\"name\":\"com_contact\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '{\"show_contact_category\":\"hide\",\"save_history\":\"1\",\"history_limit\":10,\"show_contact_list\":\"0\",\"presentation_style\":\"sliders\",\"show_name\":\"1\",\"show_position\":\"1\",\"show_email\":\"0\",\"show_street_address\":\"1\",\"show_suburb\":\"1\",\"show_state\":\"1\",\"show_postcode\":\"1\",\"show_country\":\"1\",\"show_telephone\":\"1\",\"show_mobile\":\"1\",\"show_fax\":\"1\",\"show_webpage\":\"1\",\"show_misc\":\"1\",\"show_image\":\"1\",\"image\":\"\",\"allow_vcard\":\"0\",\"show_articles\":\"0\",\"show_profile\":\"0\",\"show_links\":\"0\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"contact_icons\":\"0\",\"icon_address\":\"\",\"icon_email\":\"\",\"icon_telephone\":\"\",\"icon_mobile\":\"\",\"icon_fax\":\"\",\"icon_misc\":\"\",\"show_headings\":\"1\",\"show_position_headings\":\"1\",\"show_email_headings\":\"0\",\"show_telephone_headings\":\"1\",\"show_mobile_headings\":\"0\",\"show_fax_headings\":\"0\",\"allow_vcard_headings\":\"0\",\"show_suburb_headings\":\"1\",\"show_state_headings\":\"1\",\"show_country_headings\":\"1\",\"show_email_form\":\"1\",\"show_email_copy\":\"1\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"0\",\"redirect\":\"\",\"show_category_crumb\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{\"name\":\"com_cpanel\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CPANEL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{\"name\":\"com_installer\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_INSTALLER_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{\"name\":\"com_languages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"administrator\":\"en-GB\",\"site\":\"ro-RO\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{\"name\":\"com_login\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{\"name\":\"com_media\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '{\"upload_extensions\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\",\"upload_maxsize\":\"10\",\"file_path\":\"images\",\"image_path\":\"images\",\"restrict_uploads\":\"1\",\"allowed_media_usergroup\":\"3\",\"check_mime\":\"1\",\"image_extensions\":\"bmp,gif,jpg,png\",\"ignore_extensions\":\"\",\"upload_mime\":\"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip\",\"upload_mime_illegal\":\"text\\/html\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{\"name\":\"com_menus\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MENUS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{\"name\":\"com_messages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MESSAGES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{\"name\":\"com_modules\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MODULES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{\"name\":\"com_newsfeeds\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"newsfeed_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_feed_image\":\"1\",\"show_feed_description\":\"1\",\"show_item_description\":\"1\",\"feed_character_count\":\"0\",\"feed_display_order\":\"des\",\"float_first\":\"right\",\"float_second\":\"right\",\"show_tags\":\"1\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"0\",\"show_subcat_desc\":\"1\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_headings\":\"1\",\"show_articles\":\"0\",\"show_link\":\"1\",\"show_pagination\":\"1\",\"show_pagination_results\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{\"name\":\"com_plugins\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_PLUGINS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{\"name\":\"com_search\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"search\"}', '{\"enabled\":\"0\",\"show_date\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{\"name\":\"com_templates\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"template_positions_display\":\"0\",\"upload_limit\":\"2\",\"image_formats\":\"gif,bmp,jpg,jpeg,png\",\"source_formats\":\"txt,less,ini,xml,js,php,css,scss,yaml,twig\",\"font_formats\":\"woff,ttf,otf,eot,svg\",\"compressed_formats\":\"zip\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{\"name\":\"com_content\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"article_layout\":\"_:default\",\"show_title\":\"1\",\"link_titles\":\"1\",\"show_intro\":\"1\",\"show_category\":\"1\",\"link_category\":\"1\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_author\":\"1\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"1\",\"show_item_navigation\":\"1\",\"show_vote\":\"0\",\"show_readmore\":\"1\",\"show_readmore_title\":\"1\",\"readmore_limit\":\"100\",\"show_icons\":\"1\",\"show_print_icon\":\"1\",\"show_email_icon\":\"1\",\"show_hits\":\"1\",\"show_noauth\":\"0\",\"show_publishing_options\":\"1\",\"show_article_options\":\"1\",\"save_history\":\"1\",\"history_limit\":10,\"show_urls_images_frontend\":\"0\",\"show_urls_images_backend\":\"1\",\"targeta\":0,\"targetb\":0,\"targetc\":0,\"float_intro\":\"left\",\"float_fulltext\":\"left\",\"category_layout\":\"_:blog\",\"show_category_title\":\"0\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"1\",\"show_empty_categories\":\"0\",\"show_no_articles\":\"1\",\"show_subcat_desc\":\"1\",\"show_cat_num_articles\":\"0\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_num_articles_cat\":\"1\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"0\",\"show_subcategory_content\":\"0\",\"show_pagination_limit\":\"1\",\"filter_field\":\"hide\",\"show_headings\":\"1\",\"list_show_date\":\"0\",\"date_format\":\"\",\"list_show_hits\":\"1\",\"list_show_author\":\"1\",\"orderby_pri\":\"order\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{\"name\":\"com_config\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONFIG_XML_DESCRIPTION\",\"group\":\"\"}', '{\"filters\":{\"1\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"9\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"6\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"7\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"2\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"3\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"4\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"5\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"11\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"10\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"8\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{\"name\":\"com_redirect\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{\"name\":\"com_users\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_USERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"users\"}', '{\"allowUserRegistration\":\"1\",\"new_usertype\":\"2\",\"guest_usergroup\":\"9\",\"sendpassword\":\"1\",\"useractivation\":\"1\",\"mail_to_admin\":\"0\",\"captcha\":\"\",\"frontend_userparams\":\"1\",\"site_language\":\"0\",\"change_login_name\":\"0\",\"reset_count\":\"10\",\"reset_time\":\"1\",\"minimum_length\":\"4\",\"minimum_integers\":\"0\",\"minimum_symbols\":\"0\",\"minimum_uppercase\":\"0\",\"save_history\":\"1\",\"history_limit\":5,\"mailSubjectPrefix\":\"\",\"mailBodySuffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{\"name\":\"com_finder\",\"type\":\"component\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '{\"show_description\":\"1\",\"description_length\":255,\"allow_empty_query\":\"0\",\"show_url\":\"1\",\"show_advanced\":\"1\",\"expand_advanced\":\"0\",\"show_date_filters\":\"0\",\"highlight_terms\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\",\"batch_size\":\"50\",\"memory_table_limit\":30000,\"title_multiplier\":\"1.7\",\"text_multiplier\":\"0.7\",\"meta_multiplier\":\"1.2\",\"path_multiplier\":\"2.0\",\"misc_multiplier\":\"0.3\",\"stemmer\":\"snowball\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{\"name\":\"com_joomlaupdate\",\"type\":\"component\",\"creationDate\":\"February 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"COM_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{\"name\":\"com_tags\",\"type\":\"component\",\"creationDate\":\"December 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"COM_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"tag_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_tag_title\":\"0\",\"tag_list_show_tag_image\":\"0\",\"tag_list_show_tag_description\":\"0\",\"tag_list_image\":\"\",\"show_tag_num_items\":\"0\",\"tag_list_orderby\":\"title\",\"tag_list_orderby_direction\":\"ASC\",\"show_headings\":\"0\",\"tag_list_show_date\":\"0\",\"tag_list_show_item_image\":\"0\",\"tag_list_show_item_description\":\"0\",\"tag_list_item_maximum_characters\":0,\"return_any_or_all\":\"1\",\"include_children\":\"0\",\"maximum\":200,\"tag_list_language_filter\":\"all\",\"tags_layout\":\"_:default\",\"all_tags_orderby\":\"title\",\"all_tags_orderby_direction\":\"ASC\",\"all_tags_show_tag_image\":\"0\",\"all_tags_show_tag_descripion\":\"0\",\"all_tags_tag_maximum_characters\":20,\"all_tags_show_tag_hits\":\"0\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"tag_field_ajax_mode\":\"1\",\"show_feed_link\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{\"name\":\"com_contenthistory\",\"type\":\"component\",\"creationDate\":\"May 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_CONTENTHISTORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contenthistory\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 1, '{\"name\":\"com_ajax\",\"type\":\"component\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_AJAX_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ajax\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{\"name\":\"com_postinstall\",\"type\":\"component\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_POSTINSTALL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 'LIB_PHPUTF8', 'library', 'phputf8', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPUTF8\",\"type\":\"library\",\"creationDate\":\"2006\",\"author\":\"Harry Fuecks\",\"copyright\":\"Copyright various authors\",\"authorEmail\":\"hfuecks@gmail.com\",\"authorUrl\":\"http:\\/\\/sourceforge.net\\/projects\\/phputf8\",\"version\":\"0.5\",\"description\":\"LIB_PHPUTF8_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phputf8\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 'LIB_JOOMLA', 'library', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"LIB_JOOMLA\",\"type\":\"library\",\"creationDate\":\"2008\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"https:\\/\\/www.joomla.org\",\"version\":\"13.1\",\"description\":\"LIB_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"mediaversion\":\"adc3f41925564fc470e74549b628c33b\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 'LIB_IDNA', 'library', 'idna_convert', '', 0, 1, 1, 1, '{\"name\":\"LIB_IDNA\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"phlyLabs\",\"copyright\":\"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de\",\"authorEmail\":\"phlymail@phlylabs.de\",\"authorUrl\":\"http:\\/\\/phlylabs.de\",\"version\":\"0.8.0\",\"description\":\"LIB_IDNA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"idna_convert\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{\"name\":\"FOF\",\"type\":\"library\",\"creationDate\":\"2015-04-22 13:15:32\",\"author\":\"Nicholas K. Dionysopoulos \\/ Akeeba Ltd\",\"copyright\":\"(C)2011-2015 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"2.4.3\",\"description\":\"LIB_FOF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fof\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 'LIB_PHPASS', 'library', 'phpass', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPASS\",\"type\":\"library\",\"creationDate\":\"2004-2006\",\"author\":\"Solar Designer\",\"copyright\":\"\",\"authorEmail\":\"solar@openwall.com\",\"authorUrl\":\"http:\\/\\/www.openwall.com\\/phpass\\/\",\"version\":\"0.3\",\"description\":\"LIB_PHPASS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpass\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_archive\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_archive\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_popular\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{\"name\":\"mod_banners\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_banners\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{\"name\":\"mod_breadcrumbs\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BREADCRUMBS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_breadcrumbs\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{\"name\":\"mod_footer\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FOOTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_footer\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_news\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_news\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{\"name\":\"mod_random_image\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RANDOM_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_random_image\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{\"name\":\"mod_related_items\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RELATED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_related_items\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{\"name\":\"mod_search\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_search\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{\"name\":\"mod_stats\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{\"name\":\"mod_syndicate\",\"type\":\"module\",\"creationDate\":\"May 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SYNDICATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_syndicate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_users_latest\",\"type\":\"module\",\"creationDate\":\"December 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_USERS_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_users_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{\"name\":\"mod_whosonline\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WHOSONLINE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_whosonline\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{\"name\":\"mod_wrapper\",\"type\":\"module\",\"creationDate\":\"October 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_category\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_category\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_categories\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{\"name\":\"mod_languages\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"MOD_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_languages\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{\"name\":\"mod_finder\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{\"name\":\"mod_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{\"name\":\"mod_logged\",\"type\":\"module\",\"creationDate\":\"January 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGGED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_logged\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"March 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{\"name\":\"mod_popular\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{\"name\":\"mod_quickicon\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_QUICKICON_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_quickicon\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{\"name\":\"mod_status\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_status\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{\"name\":\"mod_submenu\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SUBMENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_submenu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{\"name\":\"mod_title\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TITLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_title\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{\"name\":\"mod_toolbar\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TOOLBAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_toolbar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{\"name\":\"mod_multilangstatus\",\"type\":\"module\",\"creationDate\":\"September 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MULTILANGSTATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_multilangstatus\"}', '{\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{\"name\":\"mod_version\",\"type\":\"module\",\"creationDate\":\"January 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_VERSION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_version\"}', '{\"format\":\"short\",\"product\":\"1\",\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{\"name\":\"mod_stats_admin\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats_admin\"}', '{\"serverinfo\":\"0\",\"siteinfo\":\"0\",\"counter\":\"0\",\"increase\":\"0\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_popular\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_popular\"}', '{\"maximum\":\"5\",\"timeframe\":\"alltime\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_similar\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_SIMILAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_similar\"}', '{\"maximum\":\"5\",\"matchtype\":\"any\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_gmail\",\"type\":\"plugin\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_GMAIL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"gmail\"}', '{\"applysuffix\":\"0\",\"suffix\":\"\",\"verifypeer\":\"1\",\"user_blacklist\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{\"name\":\"plg_authentication_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_ldap\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LDAP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ldap\"}', '{\"host\":\"\",\"port\":\"389\",\"use_ldapV3\":\"0\",\"negotiate_tls\":\"0\",\"no_referrals\":\"0\",\"auth_method\":\"bind\",\"base_dn\":\"\",\"search_string\":\"\",\"users_dn\":\"\",\"username\":\"admin\",\"password\":\"bobby7\",\"ldap_fullname\":\"fullName\",\"ldap_email\":\"mail\",\"ldap_uid\":\"uid\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_contact\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.2\",\"description\":\"PLG_CONTENT_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_emailcloak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"emailcloak\"}', '{\"mode\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_loadmodule\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOADMODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"loadmodule\"}', '{\"style\":\"xhtml\"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '{\"title\":\"1\",\"multipage_toc\":\"1\",\"showall\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagenavigation\",\"type\":\"plugin\",\"creationDate\":\"January 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_PAGENAVIGATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagenavigation\"}', '{\"position\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 5, 0);
INSERT INTO `sorin_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(409, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_vote\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_VOTE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"vote\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_codemirror\",\"type\":\"plugin\",\"creationDate\":\"28 March 2011\",\"author\":\"Marijn Haverbeke\",\"copyright\":\"Copyright (C) 2014 by Marijn Haverbeke <marijnh@gmail.com> and others\",\"authorEmail\":\"marijnh@gmail.com\",\"authorUrl\":\"http:\\/\\/codemirror.net\\/\",\"version\":\"5.18.0\",\"description\":\"PLG_CODEMIRROR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"codemirror\"}', '{\"lineNumbers\":\"1\",\"lineWrapping\":\"1\",\"matchTags\":\"1\",\"matchBrackets\":\"1\",\"marker-gutter\":\"1\",\"autoCloseTags\":\"1\",\"autoCloseBrackets\":\"1\",\"autoFocus\":\"1\",\"theme\":\"default\",\"tabmode\":\"indent\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_none\",\"type\":\"plugin\",\"creationDate\":\"September 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_NONE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"none\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{\"name\":\"plg_editors_tinymce\",\"type\":\"plugin\",\"creationDate\":\"2005-2016\",\"author\":\"Ephox Corporation\",\"copyright\":\"Ephox Corporation\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"http:\\/\\/www.tinymce.com\",\"version\":\"4.4.3\",\"description\":\"PLG_TINY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tinymce\"}', '{\"mode\":\"1\",\"skin\":\"0\",\"mobile\":\"0\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"extended_elements\":\"\",\"html_height\":\"550\",\"html_width\":\"750\",\"resizing\":\"1\",\"element_path\":\"1\",\"fonts\":\"1\",\"paste\":\"1\",\"searchreplace\":\"1\",\"insertdate\":\"1\",\"colors\":\"1\",\"table\":\"1\",\"smilies\":\"1\",\"hr\":\"1\",\"link\":\"1\",\"media\":\"1\",\"print\":\"1\",\"directionality\":\"1\",\"fullscreen\":\"1\",\"alignment\":\"1\",\"visualchars\":\"1\",\"visualblocks\":\"1\",\"nonbreaking\":\"1\",\"template\":\"1\",\"blockquote\":\"1\",\"wordcount\":\"1\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"inlinepopups\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_article\",\"type\":\"plugin\",\"creationDate\":\"October 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_ARTICLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"article\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_image\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"image\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_readmore\",\"type\":\"plugin\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_READMORE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"readmore\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_categories\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_contacts\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_content\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_languagefilter\",\"type\":\"plugin\",\"creationDate\":\"July 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagefilter\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_p3p\",\"type\":\"plugin\",\"creationDate\":\"September 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_P3P_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"p3p\"}', '{\"headers\":\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_cache\",\"type\":\"plugin\",\"creationDate\":\"February 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CACHE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cache\"}', '{\"browsercache\":\"0\",\"cachetime\":\"15\"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_debug\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_DEBUG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"debug\"}', '{\"profile\":\"1\",\"queries\":\"1\",\"memory\":\"1\",\"language_files\":\"1\",\"language_strings\":\"1\",\"strip-first\":\"1\",\"strip-prefix\":\"\",\"strip-suffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_log\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"log\"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_redirect\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"redirect\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_remember\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_REMEMBER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"remember\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_sef\",\"type\":\"plugin\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sef\"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_logout\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logout\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_contactcreator\",\"type\":\"plugin\",\"creationDate\":\"August 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTACTCREATOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contactcreator\"}', '{\"autowebpage\":\"\",\"category\":\"34\",\"autopublish\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{\"name\":\"plg_user_joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"autoregister\":\"1\",\"mail_to_user\":\"1\",\"forceLogout\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_profile\",\"type\":\"plugin\",\"creationDate\":\"January 2008\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_PROFILE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"profile\"}', '{\"register-require_address1\":\"1\",\"register-require_address2\":\"1\",\"register-require_city\":\"1\",\"register-require_region\":\"1\",\"register-require_country\":\"1\",\"register-require_postal_code\":\"1\",\"register-require_phone\":\"1\",\"register-require_website\":\"1\",\"register-require_favoritebook\":\"1\",\"register-require_aboutme\":\"1\",\"register-require_tos\":\"1\",\"register-require_dob\":\"1\",\"profile-require_address1\":\"1\",\"profile-require_address2\":\"1\",\"profile-require_city\":\"1\",\"profile-require_region\":\"1\",\"profile-require_country\":\"1\",\"profile-require_postal_code\":\"1\",\"profile-require_phone\":\"1\",\"profile-require_website\":\"1\",\"profile-require_favoritebook\":\"1\",\"profile-require_aboutme\":\"1\",\"profile-require_tos\":\"1\",\"profile-require_dob\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{\"name\":\"plg_extension_joomla\",\"type\":\"plugin\",\"creationDate\":\"May 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_languagecode\",\"type\":\"plugin\",\"creationDate\":\"November 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagecode\"}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_joomlaupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomlaupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_extensionupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"extensionupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{\"name\":\"plg_captcha_recaptcha\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.0\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha\"}', '{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_highlight\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"highlight\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_finder\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_categories\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_contacts\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_content\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_tags\",\"type\":\"plugin\",\"creationDate\":\"February 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_totp\",\"type\":\"plugin\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"totp\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{\"name\":\"plg_authentication_cookie\",\"type\":\"plugin\",\"creationDate\":\"July 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_COOKIE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cookie\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_yubikey\",\"type\":\"plugin\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"yubikey\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_tags\",\"type\":\"plugin\",\"creationDate\":\"March 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"search_limit\":\"50\",\"show_tagged_items\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(455, 'plg_installer_packageinstaller', 'plugin', 'packageinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"plg_installer_packageinstaller\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_PACKAGEINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"packageinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(456, 'PLG_INSTALLER_FOLDERINSTALLER', 'plugin', 'folderinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_FOLDERINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_FOLDERINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"folderinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(457, 'PLG_INSTALLER_URLINSTALLER', 'plugin', 'urlinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_URLINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_URLINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"urlinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(503, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{\"name\":\"beez3\",\"type\":\"template\",\"creationDate\":\"25 November 2009\",\"author\":\"Angie Radtke\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"a.radtke@derauftritt.de\",\"authorUrl\":\"http:\\/\\/www.der-auftritt.de\",\"version\":\"3.1.0\",\"description\":\"TPL_BEEZ3_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"navposition\":\"center\",\"templatecolor\":\"nature\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{\"name\":\"hathor\",\"type\":\"template\",\"creationDate\":\"May 2010\",\"author\":\"Andrea Tarr\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"3.0.0\",\"description\":\"TPL_HATHOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"showSiteName\":\"0\",\"colourChoice\":\"0\",\"boldText\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{\"name\":\"protostar\",\"type\":\"template\",\"creationDate\":\"4\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_PROTOSTAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{\"name\":\"isis\",\"type\":\"template\",\"creationDate\":\"3\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_ISIS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"December 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.5\",\"description\":\"en-GB site language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"December 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.5\",\"description\":\"en-GB administrator language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"files_joomla\",\"type\":\"file\",\"creationDate\":\"December 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.5\",\"description\":\"FILES_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(703, 'com_gantry5', 'component', 'com_gantry5', '', 1, 1, 1, 1, '{\"name\":\"com_gantry5\",\"type\":\"component\",\"creationDate\":\"March 10, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.10\",\"description\":\"COM_GANTRY5_DESCRIPTION\",\"group\":\"\",\"filename\":\"gantry5\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(705, 'plg_quickicon_gantry5', 'plugin', 'gantry5', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_gantry5\",\"type\":\"plugin\",\"creationDate\":\"March 10, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.10\",\"description\":\"PLG_QUICKICON_GANTRY5_DESCRIPTION\",\"group\":\"\",\"filename\":\"gantry5\"}', '{\"context\":\"mod_quickicon\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(706, 'plg_system_gantry5', 'plugin', 'gantry5', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_gantry5\",\"type\":\"plugin\",\"creationDate\":\"March 10, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.10\",\"description\":\"PLG_SYSTEM_GANTRY5_DESCRIPTION\",\"group\":\"\",\"filename\":\"gantry5\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(711, 'System - RokCommon', 'plugin', 'rokcommon', 'system', 0, 1, 1, 0, '{\"name\":\"System - RokCommon\",\"type\":\"plugin\",\"creationDate\":\"August 4, 2016\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"3.2.5\",\"description\":\"RokCommon System Plugin\",\"group\":\"\",\"filename\":\"rokcommon\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(712, 'RokSprocket', 'component', 'com_roksprocket', '', 1, 1, 0, 0, '{\"name\":\"RokSprocket\",\"type\":\"component\",\"creationDate\":\"March 8, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"2.1.19\",\"description\":\"Parent for all PHP based projects\",\"group\":\"\",\"filename\":\"roksprocket\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(713, 'RokSprocket Module', 'module', 'mod_roksprocket', '', 0, 1, 1, 0, '{\"name\":\"RokSprocket Module\",\"type\":\"module\",\"creationDate\":\"March 8, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"2.1.19\",\"description\":\"RokSprocket makes it easy to display content in a dynamic, visual layout.\",\"group\":\"\",\"filename\":\"mod_roksprocket\"}', '{\"run_content_plugins\":\"onmodule\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(714, 'System - RokSprocket', 'plugin', 'roksprocket', 'system', 0, 1, 1, 0, '{\"name\":\"System - RokSprocket\",\"type\":\"plugin\",\"creationDate\":\"March 8, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2017 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"2.1.19\",\"description\":\"RokSprocket System Plugin\",\"group\":\"\",\"filename\":\"roksprocket\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(715, 'Content - RokInjectModule', 'plugin', 'rokinjectmodule', 'content', 0, 1, 1, 0, '{\"name\":\"Content - RokInjectModule\",\"type\":\"plugin\",\"creationDate\":\"August 20, 2015\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2015 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"1.7\",\"description\":\"RokInjectModule Content Plugin.  Use format [module-{moduleid}] in your article to inject a module. You can also specify a style, eg: [module-28 style=xhtml]\",\"group\":\"\",\"filename\":\"rokinjectmodule\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(717, 'RokAjaxSearch', 'module', 'mod_rokajaxsearch', '', 0, 1, 1, 0, '{\"name\":\"RokAjaxSearch\",\"type\":\"module\",\"creationDate\":\"February 23, 2015\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2015 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"2.0.4\",\"description\":\"RokAjaxSearch is an ajaxian searcher that displays the results immediately and allows to choose the\\n        proper one.\\n    \",\"group\":\"\",\"filename\":\"mod_rokajaxsearch\"}', '{\"search_page\":\"index.php?option=com_search&view=search&tmpl=component\",\"adv_search_page\":\"index.php?option=com_search&view=search\",\"include_css\":\"1\",\"theme\":\"blue\",\"searchphrase\":\"any\",\"ordering\":\"newest\",\"limit\":\"10\",\"perpage\":\"3\",\"websearch\":\"0\",\"blogsearch\":\"0\",\"imagesearch\":\"0\",\"videosearch\":\"0\",\"websearch_api\":\"\",\"show_pagination\":\"1\",\"safesearch\":\"MODERATE\",\"image_size\":\"MEDIUM\",\"show_estimated\":\"1\",\"hide_divs\":\"\",\"include_link\":\"1\",\"show_description\":\"1\",\"include_category\":\"1\",\"show_readmore\":\"1\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(719, 'System - RokBox', 'plugin', 'rokbox', 'system', 0, 1, 1, 0, '{\"name\":\"System - RokBox\",\"type\":\"plugin\",\"creationDate\":\"March 21, 2016\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"2.0.13\",\"description\":\"\\n        <div class=\\\"rokbox-description\\\">\\n        <img src=\\\"..\\/plugins\\/system\\/rokbox\\/assets\\/admin\\/images\\/rokbox.jpg\\\" class=\\\"img-padding\\\" \\/><h1>RokBox2<\\/h1>\\n        <p>RokBox2 is a fully responsive modal pop-up plug-in for Joomla.  It displays <strong>images<\\/strong>, <strong>videos<\\/strong>, <strong>embedded widgets<\\/strong>, <strong>Ajax content<\\/strong> and <strong>Joomla modules<\\/strong>.<\\/p>\\n\\n        <p>RokBox2 contains a <strong>Content plug-in<\\/strong> and <strong>Button Editor plug-in<\\/strong>. The <strong>Content plug-in<\\/strong> can auto-generate thumbnails of <strong>local images<\\/strong> that can be used as content for your RokBox2 links. All thumbnails generated are <strong>responsive<\\/strong> so that they can easily adapt to any device. The <strong>Button Editor plug-in<\\/strong> allows for easy creation of RokBox2 style snippets with just a few clicks.<\\/p>\\n        <p>RokBox2 also provides backward compatibility for RokBox1 style <code>{rokbox}<\\/code> syntax.<\\/p>\\n\\n        <h2>Key Features:<\\/h2>\\n        <ul class=\\\"features\\\">\\n            <li>HTML5 and CSS3<\\/li>\\n            <li>Fully Responsive<\\/li>\\n            <li>Auto thumbnails generator<\\/li>\\n            <li>Captions supporting HTML syntax<\\/li>\\n            <li>Ajax Content listener<\\/li>\\n            <li>Multiple media types supported:\\n                <ul class=\\\"features\\\">\\n                    <li>Images: base64 encoded, jpg, gif, png, bmp, webp<\\/li>\\n                    <li>HTML5 Video and Audio<\\/li>\\n                    <li>TwitPic<\\/li>\\n                    <li>Instagram<\\/li>\\n                    <li>YouTube<\\/li>\\n                    <li>Vimeo<\\/li>\\n                    <li>Telly (ex TwitVid)<\\/li>\\n                    <li>Metacafe<\\/li>\\n                    <li>Dailymotion<\\/li>\\n                    <li>Spotify<\\/li>\\n                    <li>Google Maps<\\/li>\\n                <\\/ul>\\n            <\\/li>\\n            <li>Fit\\/Unfit Screen: If an image is too big it gets shrunk to fit the view-port but you can always click the Fit Screen icon to expand it and scroll.<\\/li>\\n            <li>Albums to group related images<\\/li>\\n            <li>Key Bindings for easy navigation: <code>&larr;<\\/code> (Previous), <code>&rarr;<\\/code> (Next), <code>f<\\/code> Fitscreen\\/Unfitscreen, <code>esc<\\/code> Close<\\/li>\\n        <\\/ul>\\n\\n        <p class=\\\"note\\\"><strong>NOTE:<\\/strong> RokBox2 consists of 3 plug-ins: <strong>Content<\\/strong>, <strong>System<\\/strong> and <strong>Editor Button<\\/strong>. Make sure to have a look at all three plug-in manager pages to get an idea of what they do.<\\/p>\\n        <\\/div>\\n        \",\"group\":\"\",\"filename\":\"rokbox\"}', '{\"backwards_compat\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(720, 'Content - RokBox', 'plugin', 'rokbox', 'content', 0, 1, 1, 0, '{\"name\":\"Content - RokBox\",\"type\":\"plugin\",\"creationDate\":\"March 21, 2016\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"2.0.13\",\"description\":\"\\n        <div class=\\\"rokbox-description\\\">\\n        <img src=\\\"..\\/plugins\\/system\\/rokbox\\/assets\\/admin\\/images\\/rokbox.jpg\\\" class=\\\"img-padding\\\" \\/><h1>RokBox2<\\/h1>\\n        <p>Auto generates thumbnails of <strong>local images<\\/strong> that can be used as content for your RokBox2 links. All thumbnails generated are <strong>responsive<\\/strong> so that they can be easily adapted to any device. RokBox2 Content plugins also provides backward compatibility for the old discontinued RokBox1 that allowed writing snippets through the <code>{rokbox}<\\/code> syntax.<\\/p>\\n\\n        <p class=\\\"note\\\"><strong>NOTE:<\\/strong> RokBox2 comes with 3 plugins: <strong>Content<\\/strong>, <strong>System<\\/strong> and <strong>Editor Button<\\/strong>. Make sure to have a look at all three plugin manager pages to get an idea of what they do.<\\/p>\\n        <\\/div>\\n        \",\"group\":\"\",\"filename\":\"rokbox\"}', '{\"backwards_compat\":\"0\",\"thumb_width\":\"150\",\"thumb_height\":\"100\",\"thumb_quality\":\"90\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(721, 'Button - RokBox', 'plugin', 'rokbox', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"Button - RokBox\",\"type\":\"plugin\",\"creationDate\":\"March 21, 2016\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"2.0.13\",\"description\":\"This is an Editor Button to allow injection of RokBox snippets into your Content\",\"group\":\"\",\"filename\":\"rokbox\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(724, 'rt_callisto', 'template', 'rt_callisto', '', 0, 1, 1, 0, '{\"name\":\"rt_callisto\",\"type\":\"template\",\"creationDate\":\"May 17, 2016\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2007 - 2015 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"1.3.5\",\"description\":\"TPL_RT_CALLISTO_DESC\",\"group\":\"\",\"filename\":\"templateDetails\"}', '[]', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(727, 'plg_gantry5_preset', 'plugin', 'preset', 'gantry5', 0, 0, 1, 1, '{\"name\":\"plg_gantry5_preset\",\"type\":\"plugin\",\"creationDate\":\"March 10, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.10\",\"description\":\"PLG_GANTRY5_PRESET_DESCRIPTION\",\"group\":\"\",\"filename\":\"preset\"}', '{\"preset\":\"presets\",\"reset\":\"reset-settings\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(730, 'Editor - RokPad', 'plugin', 'rokpad', 'editors', 0, 1, 1, 0, '{\"name\":\"Editor - RokPad\",\"type\":\"plugin\",\"creationDate\":\"February 23, 2015\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2015 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"2.1.9\",\"description\":\"\\n        <div class=\\\"rokpad-description\\\">\\n        <img src=\\\"..\\/plugins\\/editors\\/rokpad\\/assets\\/admin\\/images\\/rokpad.jpg\\\" class=\\\"img-padding\\\" \\/><h1>RokPad<\\/h1>\\n        <h2>The Power of Desktop Text Editor in a Joomla!<\\/h2>\\n        <p>Ever wished you could use a powerful desktop text editor such as Textmate, SublimeText, or UltraEdit directly on a Joomla! web site?  Now with RokPad we provide many features and capabilities that traditionally would only be found in a powerful desktop editor.  RokPad provides advanced functions such as an Ajax saving action, syntax highlighting, configurable themes, multiple cursors and selections, shortcut keys, multiple language support, and many other incredible features.<\\/p>\\n\\n        <h2>Key Features:<\\/h2>\\n        <ul class=\\\"features\\\">\\n\\t\\t\\t<li>Code Highlighter built on the great ACE Editor<\\/li>\\n\\t        <li>Support for CSS, HTML, JavaScript, JSON, LESS, Markdown, PHP, SQL, Plain Text, Textile, XML and more...<\\/li>\\n\\t        <li>Ajax save and Automatic Save options. You\'ll never loose your content again, nor wait until the page has finished reloading after a save!<\\/li>\\n\\t        <li>Save, Undo, Redo, Goto line, Advanced Search and Search &amp; Replace, Full Screen. Settings like Themes, Font Size, Code Folding, Wrap Mode, Invisible Characters, Print Margin, Highlight of selected word<\\/li>\\n\\t        <li>26 Themes to choose from<\\/li>\\n\\t        <li>Resizable Editor by dragging the Statusbar<\\/li>\\n\\t        <li>Keyboard shortcuts<\\/li>\\n\\t        <li>Brackets match<\\/li>\\n\\t        <li>Multiple cursors and selections<\\/li>\\n\\t        <li>Vertical Selection<\\/li>\\n\\t        <li>Ability to insert at multiple locations xtd-buttons shortcodes, all at once.<\\/li>\\n\\t        <li>Shortcodes and Universal Tag Insertion<\\/li>\\n\\t        <li>Drag &amp; Drop of text from external applications such as other Browser Tabs\\/windows or Native Applications (Supported on Firefox, Chrome, IE10 and Safari)<\\/li>\\n\\t        <li>Import local files by Drag &amp; Drop directly from your desktop! (Supported on Firefox, Chrome, IE10 and Safari 6+)<\\/li>\\n\\t        <li>And much more!<\\/li>\\n        <\\/ul>\\n        <\\/div>\\n        \",\"group\":\"\",\"filename\":\"rokpad\"}', '{\"autosave-enabled\":\"0\",\"autosave-time\":\"5\",\"theme\":\"fluidvision\",\"font-size\":\"12px\",\"fold-style\":\"markbeginend\",\"use-wrap-mode\":\"free\",\"selection-style\":\"1\",\"highlight-active-line\":\"1\",\"highlight-selected-word\":\"1\",\"show-invisibles\":\"0\",\"show-gutter\":\"1\",\"show-print-margin\":\"1\",\"fade-fold-widgets\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(732, 'System - RokBooster', 'plugin', 'rokbooster', 'system', 0, 0, 1, 0, '{\"name\":\"System - RokBooster\",\"type\":\"plugin\",\"creationDate\":\"June 14, 2016\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"1.1.16\",\"description\":\"\\n        \\n        <div class=\\\"booster-description\\\">\\n        <img src=\\\"..\\/plugins\\/system\\/rokbooster\\/assets\\/images\\/rokbooster.jpg\\\" class=\\\"img-padding\\\" \\/><h1>RokBooster<\\/h1>\\n        <p>Increase the speed of your site by enabling the RokBooster plug-in.  This advanced extensions will <strong>compress<\\/strong> and <strong>combine<\\/strong> your <strong>CSS<\\/strong> and <strong>JavaScript<\\/strong> into as few files as possible each.  RokBooster can dramatically reduce the number of HTTP calls a browser has to make, and sending those compressed files GZipped means your pages will load faster with less load on your server.<\\/p>\\n\\n        <h2>Key Features:<\\/h2>\\n        <ul class=\\\"features\\\">\\n            <li>Combine and compress CSS and JavaScript into as few files as possible<\\/li>\\n            <li>GZip compression used to send CSS and JavaScript files<\\/li>\\n            <li>Compress Inline CSS and JavaScript<\\/li>\\n            <li>Customizable cache timeout<\\/li>\\n            <li>Background rendering, so initial file processing won\'t slow your users down<\\/li>\\n            <li>Full page scan allows for non-header JavaScript and CSS to be included<\\/li>\\n            <li>Ability to ignore specific CSS and JavaScript files<\\/li>\\n        <\\/ul>\\n        <p class=\\\"note\\\"><strong>NOTE:<\\/strong> Clearing the Joomla cache will remove the RokBooster compressed cache files causing them to regenerate on the next page hit<\\/p>\\n        <\\/div>\\n        \\n    \",\"group\":\"\",\"filename\":\"rokbooster\"}', '{\"cache_time\":\"60\",\"compress_css\":\"compress\",\"style_sort\":\"RokBooster_Compressor_Sort_ExternalOnTop\",\"inline_css\":\"1\",\"imported_css\":\"1\",\"compress_js\":\"combine\",\"script_sort\":\"RokBooster_Compressor_Sort_ExternalOnTop\",\"inline_js\":\"1\",\"max_data_uri_image_size\":\"21612\",\"convert_css_images\":\"1\",\"convert_page_images\":\"1\",\"max_data_uri_font_size\":\"21612\",\"convert_css_fonts\":\"1\",\"ignored_pages\":\"\",\"ignored_files\":\"\\/media\\/editors\\/tinymce\\/jscripts\\/tiny_mce\\/tiny_mce.js\",\"scan_method\":\"header\",\"use_background_processing\":\"1\",\"disable_for_ie\":\"2\",\"cache_file_permissions\":\"0644\",\"data_storage\":\"default\",\"debugloglevel\":\"63\"}', '', '', 0, '0000-00-00 00:00:00', 100, 0),
(734, 'gantry5_nucleus', 'file', 'gantry5_nucleus', '', 0, 1, 0, 1, '{\"name\":\"gantry5_nucleus\",\"type\":\"file\",\"creationDate\":\"March 10, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.10\",\"description\":\"GANTRY5_NUCLEUS_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(735, 'pkg_gantry5', 'package', 'pkg_gantry5', '', 0, 1, 1, 0, '{\"name\":\"pkg_gantry5\",\"type\":\"package\",\"creationDate\":\"March 10, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.10\",\"description\":\"PKG_GANTRY5_DESCRIPTION\",\"group\":\"\",\"filename\":\"pkg_gantry5\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(742, 'mod_gantry5_particle', 'module', 'mod_gantry5_particle', '', 0, 1, 0, 1, '{\"name\":\"mod_gantry5_particle\",\"type\":\"module\",\"creationDate\":\"March 10, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.10\",\"description\":\"MOD_GANTRY5_PARTICLE_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_gantry5_particle\"}', '{\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(802, 'English (en-GB) Language Pack', 'package', 'pkg_en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB) Language Pack\",\"type\":\"package\",\"creationDate\":\"December 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.5.1\",\"description\":\"en-GB language pack\",\"group\":\"\",\"filename\":\"pkg_en-GB\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(804, 'JobsFactory', 'component', 'com_jobsfactory', '', 1, 1, 0, 0, '{\"name\":\"JobsFactory\",\"type\":\"component\",\"creationDate\":\"05-09-2016\",\"author\":\"thePHPfactory\",\"copyright\":\"thePHPfactory\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.8.0\",\"description\":\"Jobs Factory\",\"group\":\"\",\"filename\":\"jobsfactory\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `sorin_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(805, 'jobsFactory Featured module', 'module', 'mod_jobsfactory_featured', '', 0, 1, 0, 0, '{\"name\":\"jobsFactory Featured module\",\"type\":\"module\",\"creationDate\":\"December 2014\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.5.0\",\"description\":\"Module for Jobs Factory\",\"group\":\"\",\"filename\":\"mod_jobsfactory_featured\"}', '{\"force_itemid\":\"\",\"nr_jobs_displayed\":\"5\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(806, 'jobsFactory Filters module', 'module', 'mod_jobsfactory_filters', '', 0, 1, 0, 0, '{\"name\":\"jobsFactory Filters module\",\"type\":\"module\",\"creationDate\":\"December 2014\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.5.0\",\"description\":\"Module for Jobs Factory\",\"group\":\"\",\"filename\":\"mod_jobsfactory_filters\"}', '{\"force_itemid\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(807, 'jobsFactory Latest Jobs', 'module', 'mod_jobsfactory_latest', '', 0, 1, 0, 0, '{\"name\":\"jobsFactory Latest Jobs\",\"type\":\"module\",\"creationDate\":\"December 2014\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.5.0\",\"description\":\"Module for Jobs Factory\",\"group\":\"\",\"filename\":\"mod_jobsfactory_latest\"}', '{\"force_itemid\":\"\",\"nr_jobs_displayed\":\"5\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(808, 'jobsFactory Popular Jobs', 'module', 'mod_jobsfactory_popular', '', 0, 1, 0, 0, '{\"name\":\"jobsFactory Popular Jobs\",\"type\":\"module\",\"creationDate\":\"December 2014\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.5.0\",\"description\":\"Module for Jobs Factory\",\"group\":\"\",\"filename\":\"mod_jobsfactory_popular\"}', '{\"force_itemid\":\"\",\"nr_jobs_displayed\":\"5\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(809, 'jobsFactory Random Jobs', 'module', 'mod_jobsfactory_random', '', 0, 1, 0, 0, '{\"name\":\"jobsFactory Random Jobs\",\"type\":\"module\",\"creationDate\":\"December 2014\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.5.0\",\"description\":\"Module for Jobs Factory Random Jobs\",\"group\":\"\",\"filename\":\"mod_jobsfactory_random\"}', '{\"force_itemid\":\"\",\"nr_jobs_displayed\":\"5\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(810, 'jobsFactory Search module', 'module', 'mod_jobsfactory_search', '', 0, 1, 0, 0, '{\"name\":\"jobsFactory Search module\",\"type\":\"module\",\"creationDate\":\"December 2014\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.5.0\",\"description\":\"Module for Jobs Factory\",\"group\":\"\",\"filename\":\"mod_jobsfactory_search\"}', '{\"force_itemid\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(811, 'jobsFactory Tag Cloud', 'module', 'mod_jobsfactorycloud', '', 0, 1, 0, 0, '{\"name\":\"jobsFactory Tag Cloud\",\"type\":\"module\",\"creationDate\":\"December 2014\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.5.0\",\"description\":\"Jobs Tag Clouds generates a Cloud of the most ocurrences jobs tags\",\"group\":\"\",\"filename\":\"mod_jobsfactorycloud\"}', '{\"max_tags\":\"40\",\"force_itemid\":\"\",\"min_font_size\":\"8\",\"max_font_size\":\"20\",\"min_tag_lenght\":\"4\",\"max_tag_lenght\":\"15\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(812, 'jobsFactory Category Tree Module', 'module', 'mod_jobsfactorytree', '', 0, 1, 0, 0, '{\"name\":\"jobsFactory Category Tree Module\",\"type\":\"module\",\"creationDate\":\"December 2014\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.5.0\",\"description\":\"Module for Jobs Categories\",\"group\":\"\",\"filename\":\"mod_jobsfactorytree\"}', '{\"category_counter\":\"0\",\"force_itemid\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(813, 'plg_search_jobsfactory', 'plugin', 'jobsfactory', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_jobsfactory\",\"type\":\"plugin\",\"creationDate\":\"December 2014\",\"author\":\"thePHPfactory\",\"copyright\":\"thePHPfactory\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.5.0\",\"description\":\"Allows Searching of Jobs Component\",\"group\":\"\",\"filename\":\"jobsfactory\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(814, 'Jobs Factory User plugin', 'plugin', 'auctionfactory', 'user', 0, 0, 1, 0, '{\"name\":\"Jobs Factory User plugin\",\"type\":\"plugin\",\"creationDate\":\"October 2015\",\"author\":\"thePHPfactory\",\"copyright\":\"\",\"authorEmail\":\"contact@thePHPfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.0\",\"description\":\"Jobs Factory - User plugin\",\"group\":\"\",\"filename\":\"auctionfactory\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(815, 'thePHPfactory Open Graph Plugin', 'plugin', 'factoryopengraph', 'system', 0, 1, 1, 0, '{\"name\":\"thePHPfactory Open Graph Plugin\",\"type\":\"plugin\",\"creationDate\":\"November 2015\",\"author\":\"thePHPfactory\",\"copyright\":\"\",\"authorEmail\":\"contact@thePHPfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.0\",\"description\":\"thePHPfactory - Open Graph plugin\",\"group\":\"\",\"filename\":\"factoryopengraph\"}', '{}', '', '', 631, '2017-03-16 09:34:44', 0, 0),
(816, 'JobsFactory', 'package', 'pkg_jobsfactory', '', 0, 1, 1, 0, '{\"name\":\"JobsFactory\",\"type\":\"package\",\"creationDate\":\"September 2016\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.8.0\",\"description\":\"Jobs Factory Installer\",\"group\":\"\",\"filename\":\"pkg_jobsfactory\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(838, 'PLG_USER_ADS', 'plugin', 'ads', 'user', 0, 0, 1, 0, '{\"name\":\"PLG_USER_ADS\",\"type\":\"plugin\",\"creationDate\":\"October 2015\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thePHPfactory.com\",\"authorUrl\":\"http:\\/\\/www.thePHPfactory.com\",\"version\":\"1.0\",\"description\":\"Ads Factory - User Plugin\",\"group\":\"\",\"filename\":\"ads\"}', '{\"integrate\":\"0\",\"showpaypalemail\":\"1\",\"showcustomfields\":\"1\",\"showaboutme\":\"1\",\"showaddressfields\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(852, 'RokCommon', 'library', 'lib_rokcommon', '', 0, 1, 1, 0, '{\"name\":\"RokCommon\",\"type\":\"library\",\"creationDate\":\"August 4, 2016\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"3.2.5\",\"description\":\"RokCommon Shared Library\",\"group\":\"\",\"filename\":\"lib_rokcommon\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(853, 'Gantry 5 Framework', 'library', 'gantry5', '', 0, 1, 1, 1, '{\"name\":\"Gantry 5 Framework\",\"type\":\"library\",\"creationDate\":\"March 10, 2017\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2016 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"5.4.10\",\"description\":\"LIB_GANTRY5_DESCRIPTION\",\"group\":\"\",\"filename\":\"gantry5\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(854, 'thephpfactoryupdate', 'component', 'com_thephpfactoryupdate', '', 1, 1, 0, 0, '{\"name\":\"thephpfactoryupdate\",\"type\":\"component\",\"creationDate\":\"20-02-2014 15:38\",\"author\":\"The PHP Factory\",\"copyright\":\"The PHP Factory\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"1.0.0\",\"description\":\"Update Instructions for thePHPfactory Extensions\",\"group\":\"\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(855, 'plg_search_adsfactory', 'plugin', 'adsfactory', 'search', 0, 0, 1, 0, '{\"name\":\"plg_search_adsfactory\",\"type\":\"plugin\",\"creationDate\":\"Decembrie 2014\",\"author\":\"ThePHPFactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"www.thePHPfactory.com\",\"version\":\"4.2.9\",\"description\":\"Allows Searching of Adsfactory Component\",\"group\":\"\",\"filename\":\"adsfactory\"}', '{\"search_limit\":\"50\",\"search_content\":\"0\",\"search_archived\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(856, 'com_Advertisementfactory', 'component', 'com_advertisementfactory', '', 1, 1, 0, 0, '{\"name\":\"com_Advertisementfactory\",\"type\":\"component\",\"creationDate\":\"February 2011\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"3.1.2\",\"description\":\"Advertisement & Marketing Component from thePHPFactory\",\"group\":\"\",\"filename\":\"advertisementfactory\"}', '{\"backend_preview_token\":\"94cefb603a54e6ad4af955c0d34115ed\",\"currency\":\"EUR\",\"enabled_ad_types\":[\"link\",\"thumbnail\",\"banner\",\"modal\",\"fullpage\",\"keyword\"],\"available_ad_types\":[\"link\",\"thumbnail\",\"banner\",\"modal\",\"fullpage\",\"keyword\"],\"fullpageads_image_width\":\"400\",\"fullpageads_image_height\":\"200\",\"fullpageads_displays_per_session\":\"3\",\"fullpageads_clicks_to_display\":\"10\",\"fullpageads_seconds_to_display\":\"10\",\"fullpageads_displays_per_session_guests\":\"10\",\"fullpageads_clicks_to_display_guests\":\"3\",\"fullpageads_seconds_to_display_guests\":\"20\",\"modalads_window_width\":\"600\",\"modalads_window_height\":\"500\",\"modalads_image_width\":\"400\",\"modalads_image_height\":\"200\",\"modalads_displays_per_session\":\"3\",\"modalads_clicks_to_display\":\"10\",\"modalads_seconds_to_display\":\"20\",\"modalads_displays_per_session_guests\":\"3\",\"modalads_clicks_to_display_guests\":\"10\",\"modalads_seconds_to_display_guests\":\"20\",\"keywords_min_length\":\"3\",\"keywords_banned\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(857, 'mod_advertisementfactory_banners', 'module', 'mod_advertisementfactory_banners', '', 0, 1, 0, 0, '{\"name\":\"mod_advertisementfactory_banners\",\"type\":\"module\",\"creationDate\":\"February 2011\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"3.1.2\",\"description\":\"Advertisement Factory Banners Module\",\"group\":\"\",\"filename\":\"mod_advertisementfactory_banners\"}', '{\"module_type\":\"1\",\"showempty\":\"1\",\"show_bottom\":\"1\",\"width\":\"200\",\"height\":\"50\",\"force_size\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(858, 'mod_advertisementfactory_links', 'module', 'mod_advertisementfactory_links', '', 0, 1, 0, 0, '{\"name\":\"mod_advertisementfactory_links\",\"type\":\"module\",\"creationDate\":\"February 2011\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"3.1.2\",\"description\":\"Advertisement Factory Links Module\",\"group\":\"\",\"filename\":\"mod_advertisementfactory_links\"}', '{\"module_type\":\"1\",\"slots\":\"5\",\"showemptylinks\":\"1\",\"display\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(859, 'mod_advertisementfactory_thumbnails', 'module', 'mod_advertisementfactory_thumbnails', '', 0, 1, 0, 0, '{\"name\":\"mod_advertisementfactory_thumbnails\",\"type\":\"module\",\"creationDate\":\"February 2011\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"3.1.2\",\"description\":\"Advertisement Factory Thumbnail Module\",\"group\":\"\",\"filename\":\"mod_advertisementfactory_thumbnails\"}', '{\"module_type\":\"1\",\"showempty\":\"1\",\"rows\":\"2\",\"columns\":\"2\",\"width\":\"200\",\"height\":\"50\",\"force_size\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(860, 'plg_advertisementfactory', 'plugin', 'advertisementfactory', 'system', 0, 0, 1, 0, '{\"name\":\"plg_advertisementfactory\",\"type\":\"plugin\",\"creationDate\":\"February 2011\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"3.1.2\",\"description\":\"Advertisement Factory System Plugin\",\"group\":\"\",\"filename\":\"advertisementfactory\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(861, 'plg_advertisementfactoryfullpage', 'plugin', 'advertisementfactoryfullpage', 'system', 0, 0, 1, 0, '{\"name\":\"plg_advertisementfactoryfullpage\",\"type\":\"plugin\",\"creationDate\":\"February 2011\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"3.1.2\",\"description\":\"Advertisement Factory Full Page Ads Plugin\",\"group\":\"\",\"filename\":\"advertisementfactoryfullpage\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(862, 'plg_advertisementfactorymodal', 'plugin', 'advertisementfactorymodal', 'system', 0, 0, 1, 0, '{\"name\":\"plg_advertisementfactorymodal\",\"type\":\"plugin\",\"creationDate\":\"February 2011\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"3.1.2\",\"description\":\"Advertisement Factory Modal Ads Plugin\",\"group\":\"\",\"filename\":\"advertisementfactorymodal\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(863, 'plg_advertisementfactorypopup', 'plugin', 'advertisementfactorypopup', 'system', 0, 0, 1, 0, '{\"name\":\"plg_advertisementfactorypopup\",\"type\":\"plugin\",\"creationDate\":\"February 2011\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"3.1.2\",\"description\":\"Advertisement Factory Popup Ads Plugin\",\"group\":\"\",\"filename\":\"advertisementfactorypopup\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(864, 'plg_advertisementfactorykeywords', 'plugin', 'advertisementfactorykeywords', 'content', 0, 0, 1, 0, '{\"name\":\"plg_advertisementfactorykeywords\",\"type\":\"plugin\",\"creationDate\":\"February 2011\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"3.1.2\",\"description\":\"Advertisement Factory Keywords Ads Plugin\",\"group\":\"\",\"filename\":\"advertisementfactorykeywords\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(865, 'Advertisement Factory', 'package', 'pkg_AdvertisementFactorySuite', '', 0, 1, 1, 0, '{\"name\":\"Advertisement Factory\",\"type\":\"package\",\"creationDate\":\"February 2011\",\"author\":\"thePHPfactory\",\"copyright\":\"SKEPSIS Consult SRL\",\"authorEmail\":\"contact@thephpfactory.com\",\"authorUrl\":\"http:\\/\\/www.thephpfactory.com\",\"version\":\"3.1.2\",\"description\":\"Advertisement Factory Component, Modules and Plugins from thePHPFactory\",\"group\":\"\",\"filename\":\"pkg_AdvertisementFactorySuite\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(866, 'offlajn_installer', 'component', 'com_offlajn_installer', '', 1, 1, 0, 0, '{\"name\":\"offlajn_installer\",\"type\":\"component\",\"creationDate\":\"2012 January\",\"author\":\"Balint Polgarfi\",\"copyright\":\"Copyright (C) 2012 All rights reserved.\",\"authorEmail\":\"balint.polgarfi|at|offlajn.com\",\"authorUrl\":\"http:\\/\\/www.offlajn.com\",\"version\":\"1.0\",\"description\":\"\",\"group\":\"\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(867, 'Improved AJAX Login and Register', 'module', 'mod_improved_ajax_login', '', 0, 1, 0, 0, '{\"name\":\"Improved AJAX Login and Register\",\"type\":\"module\",\"creationDate\":\"2012-05-07\",\"author\":\"Balint Polgarfi\",\"copyright\":\"Copyright (C) 2012 All rights reserved.\",\"authorEmail\":\"balint.polgarfi|at|offlajn.com\",\"authorUrl\":\"www.offlajn.com\",\"version\":\"2.194\",\"description\":\"\",\"group\":\"\",\"filename\":\"mod_improved_ajax_login\"}', '[]', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(868, 'Offlajn Dojo Loader', 'plugin', 'dojoloader', 'system', 0, 1, 1, 0, '{\"name\":\"Offlajn Dojo Loader\",\"type\":\"plugin\",\"creationDate\":\"May 2012\",\"author\":\"Roland Soos\",\"copyright\":\"Copyright (C) 2012 Offlajn.com. All rights reserved.\",\"authorEmail\":\"info@offlajn.com\",\"authorUrl\":\"http:\\/\\/offlajn.com\",\"version\":\"1.0\",\"description\":\"Dojo javascript library loader\",\"group\":\"\",\"filename\":\"dojoloader\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(869, 'System - Improved AJAX Login and Register', 'plugin', 'improved_ajax_login', 'system', 0, 1, 1, 0, '{\"name\":\"System - Improved AJAX Login and Register\",\"type\":\"plugin\",\"creationDate\":\"March 2013\",\"author\":\"Balint Polgarfi\",\"copyright\":\"Copyright (C) 2013 All rights reserved.\",\"authorEmail\":\"balint.polgarfi|at|offlajn.com\",\"authorUrl\":\"www.offlajn.com\",\"version\":\"2.0\",\"description\":\"\\n\\t\\tSystem - Improved AJAX Login and Register is a system plugin for AJAX Login and Registration.\\n    It\'s also able to override the default Joomla Login and Registration forms with Improved AJAX Login and Register.\\n  \",\"group\":\"\",\"filename\":\"improved_ajax_login\"}', '{\"override\":\"1\",\"autologin\":\"1\",\"generate\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(870, 'User - Improved Profile', 'plugin', 'improved_profile', 'user', 0, 1, 1, 0, '{\"name\":\"User - Improved Profile\",\"type\":\"plugin\",\"creationDate\":\"Juni 2013\",\"author\":\"Balint Polgarfi\",\"copyright\":\"(C) 2013 All rights reserved.\",\"authorEmail\":\"balint.polgarfi@offlajn.com\",\"authorUrl\":\"offlajn.com\",\"version\":\"2.0\",\"description\":\"\",\"group\":\"\",\"filename\":\"improved_profile\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(871, 'System - Offlajn Params', 'plugin', 'offlajnparams', 'system', 0, 1, 1, 0, '{\"name\":\"System - Offlajn Params\",\"type\":\"plugin\",\"creationDate\":\"Apr 2012\",\"author\":\"So\\u00f3s Roland\",\"copyright\":\"Copyright (C) Offlajn.com\",\"authorEmail\":\"info@offlajn.com\",\"authorUrl\":\"http:\\/\\/offlajn.com\",\"version\":\"1.0.0\",\"description\":\"\",\"group\":\"\",\"filename\":\"offlajnparams\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(872, 'Offlajn Joomla 3.0 compatibility', 'plugin', 'offlajnjoomla3compat', 'system', 0, 1, 1, 0, '{\"name\":\"Offlajn Joomla 3.0 compatibility\",\"type\":\"plugin\",\"creationDate\":\"Oct 2012\",\"author\":\"Jeno Kovacs\",\"copyright\":\"Copyright (C) 2012 Offlajn.com. All rights reserved.\",\"authorEmail\":\"jeno.kovacs@offlajn.com\",\"authorUrl\":\"http:\\/\\/offlajn.com\",\"version\":\"1.0\",\"description\":\"\",\"group\":\"\",\"filename\":\"offlajnjoomla3compat\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(873, 'com_improved_ajax_login', 'component', 'com_improved_ajax_login', '', 1, 1, 0, 0, '{\"name\":\"com_improved_ajax_login\",\"type\":\"component\",\"creationDate\":\"2013-05-15\",\"author\":\"Balint Polgarfi\",\"copyright\":\"Copyright (C) 2013. All rights reserved.\",\"authorEmail\":\"balint.polgarfi|at|offlajn.com\",\"authorUrl\":\"http:\\/\\/offlajn.com\",\"version\":\"2.194\",\"description\":\"\",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(874, 'RomnaRomnia', 'language', 'ro-RO', '', 0, 1, 0, 0, '{\"name\":\"Rom\\u00e2na (Rom\\u00e2nia)\",\"type\":\"language\",\"creationDate\":\"24-07-2016\",\"author\":\"Horia Negura - Quanta\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"fakemoth@gmail.com\",\"authorUrl\":\"www.quanta-group.ro\",\"version\":\"3.6.0.1\",\"description\":\"Joomla! 3.6 Romanian (ro-RO) Website Language Pack - Version 3.6.0.1\",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(875, 'RomnaRomnia', 'language', 'ro-RO', '', 1, 1, 0, 0, '{\"name\":\"Rom\\u00e2na (Rom\\u00e2nia)\",\"type\":\"language\",\"creationDate\":\"24-07-2016\",\"author\":\"Horia Negura - Quanta\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"fakemoth@gmail.com\",\"authorUrl\":\"www.quanta-group.ro\",\"version\":\"3.6.0.1\",\"description\":\"Joomla! 3.6 Romanian (ro-RO) Administrator Language Pack - Version 3.6.0.1\",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(876, 'Romanian Language Pack', 'package', 'pkg_ro-RO', '', 0, 1, 1, 0, '{\"name\":\"Romanian Language Pack\",\"type\":\"package\",\"creationDate\":\"24-07-2016\",\"author\":\"Horia Negura - Quanta\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. Toate drepturile rezervate.\",\"authorEmail\":\"fakemoth@gmail.com\",\"authorUrl\":\"www.quanta-group.ro\",\"version\":\"3.6.0.1\",\"description\":\"Joomla! 3.6 Full Romanian (ro-RO) Language Package - Version 3.6.0v1\",\"group\":\"\",\"filename\":\"pkg_ro-RO\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_filters`
--

CREATE TABLE `sorin_finder_filters` (
  `filter_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links`
--

CREATE TABLE `sorin_finder_links` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double UNSIGNED NOT NULL DEFAULT '0',
  `sale_price` double UNSIGNED NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_terms0`
--

CREATE TABLE `sorin_finder_links_terms0` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_terms1`
--

CREATE TABLE `sorin_finder_links_terms1` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_terms2`
--

CREATE TABLE `sorin_finder_links_terms2` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_terms3`
--

CREATE TABLE `sorin_finder_links_terms3` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_terms4`
--

CREATE TABLE `sorin_finder_links_terms4` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_terms5`
--

CREATE TABLE `sorin_finder_links_terms5` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_terms6`
--

CREATE TABLE `sorin_finder_links_terms6` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_terms7`
--

CREATE TABLE `sorin_finder_links_terms7` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_terms8`
--

CREATE TABLE `sorin_finder_links_terms8` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_terms9`
--

CREATE TABLE `sorin_finder_links_terms9` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_termsa`
--

CREATE TABLE `sorin_finder_links_termsa` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_termsb`
--

CREATE TABLE `sorin_finder_links_termsb` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_termsc`
--

CREATE TABLE `sorin_finder_links_termsc` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_termsd`
--

CREATE TABLE `sorin_finder_links_termsd` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_termse`
--

CREATE TABLE `sorin_finder_links_termse` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_links_termsf`
--

CREATE TABLE `sorin_finder_links_termsf` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_taxonomy`
--

CREATE TABLE `sorin_finder_taxonomy` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `access` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Salvarea datelor din tabel `sorin_finder_taxonomy`
--

INSERT INTO `sorin_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_taxonomy_map`
--

CREATE TABLE `sorin_finder_taxonomy_map` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_terms`
--

CREATE TABLE `sorin_finder_terms` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_terms_common`
--

CREATE TABLE `sorin_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Salvarea datelor din tabel `sorin_finder_terms_common`
--

INSERT INTO `sorin_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('ani', 'en'),
('any', 'en'),
('are', 'en'),
('aren\'t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn\'t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('noth', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('onli', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('veri', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('whi', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_tokens`
--

CREATE TABLE `sorin_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '1',
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_tokens_aggregate`
--

CREATE TABLE `sorin_finder_tokens_aggregate` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `term_weight` float UNSIGNED NOT NULL,
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `context_weight` float UNSIGNED NOT NULL,
  `total_weight` float UNSIGNED NOT NULL,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_finder_types`
--

CREATE TABLE `sorin_finder_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Salvarea datelor din tabel `sorin_finder_types`
--

INSERT INTO `sorin_finder_types` (`id`, `title`, `mime`) VALUES
(1, 'Tag', ''),
(2, 'Category', ''),
(3, 'Contact', ''),
(4, 'Article', ''),
(5, 'News Feed', '');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_attachments`
--

CREATE TABLE `sorin_jobsfactory_attachments` (
  `id` int(11) NOT NULL,
  `fileName` varchar(255) NOT NULL,
  `fileExt` varchar(10) NOT NULL,
  `fileType` enum('attachment','image') NOT NULL,
  `jobId` int(11) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_candidates`
--

CREATE TABLE `sorin_jobsfactory_candidates` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `comments` text NOT NULL,
  `cancel` int(11) DEFAULT NULL,
  `action` varchar(100) NOT NULL,
  `accept` tinyint(4) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_categories`
--

CREATE TABLE `sorin_jobsfactory_categories` (
  `id` int(11) NOT NULL,
  `catname` varchar(250) NOT NULL,
  `parent` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_categories`
--

INSERT INTO `sorin_jobsfactory_categories` (`id`, `catname`, `parent`, `hash`, `description`, `ordering`, `status`) VALUES
(1, 'Animale mici / de companie', 0, '4250499c1ada36c77689fee336e481a7', '', NULL, 1),
(2, 'Animale de ferma', 0, '6058114e2e357f62f7ab193c9d9315c5', '', NULL, 1),
(3, 'Clinica mixta', 0, '0a35b78b7d13e649b1182ec0bdb91581', '', NULL, 1),
(4, 'Ecvine', 0, '3aa0fb6f937c7c79fb6644fbdbe2d4fe', '', NULL, 1),
(5, 'Animale exotice', 0, '60a30d1f11f69799433c941a6f654e7a', '', NULL, 1),
(6, 'Management', 0, 'd10af457daa1deed54e2c36b5f295e7e', '', NULL, 1),
(7, 'Acvacultura', 0, '7ee8c3e91f27022d44cea4f7edd5cce8', '', NULL, 1),
(8, 'Siguranta alimentelor', 0, '8886f6c9dc4fecf6db9f0ec900d24bba', '', NULL, 1),
(9, 'Circumscriptie veterinara', 0, '268bbf83d65184ce7ba83bafb1f4f8d1', '', NULL, 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_cities`
--

CREATE TABLE `sorin_jobsfactory_cities` (
  `id` int(11) NOT NULL,
  `cityname` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_cities`
--

INSERT INTO `sorin_jobsfactory_cities` (`id`, `cityname`, `location`, `hash`, `ordering`, `status`) VALUES
(1, 'Bucharest', '', '15c3903a3e8b69198d090670d18535fa', NULL, 1),
(2, 'Timisoara', '', 'eadb8dc4971e1db14326598426d0d300', NULL, 1),
(3, 'Iasi', '', 'bc7d8f97fb4b7c5b77686baeaf2d5092', NULL, 1),
(4, 'Craiova', '', '5fb150483f08d3d5e72bddaa4dc2ede7', NULL, 1),
(5, 'Constanta', '', '3a1436c0020df67648220a778ae0c0eb', NULL, 1),
(6, 'Brasov', '', 'e29cccfec5dfd5d3a134755caa9a6c97', NULL, 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_companies_categories`
--

CREATE TABLE `sorin_jobsfactory_companies_categories` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `cid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_companies_categories`
--

INSERT INTO `sorin_jobsfactory_companies_categories` (`id`, `company_id`, `cid`) VALUES
(10, 0, 1),
(11, 0, 2),
(12, 0, 3),
(13, 0, 4),
(14, 0, 5),
(15, 0, 6),
(16, 0, 7),
(17, 0, 8),
(18, 0, 9),
(19, 1, 1),
(20, 1, 2),
(21, 1, 3),
(22, 1, 4),
(23, 1, 5),
(24, 1, 6),
(25, 1, 7),
(26, 1, 8);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_country`
--

CREATE TABLE `sorin_jobsfactory_country` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL DEFAULT '',
  `simbol` char(3) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_country`
--

INSERT INTO `sorin_jobsfactory_country` (`id`, `name`, `simbol`, `active`) VALUES
(4, 'AFGHANISTAN', 'AF', 1),
(5, 'ALAND ISLANDS', 'AX', 0),
(6, 'ALBANIA', 'AL', 1),
(7, 'ALGERIA', 'DZ', 0),
(8, 'AMERICAN SAMOA', 'AS', 0),
(9, 'ANDORRA', 'AD', 0),
(10, 'ANGOLA', 'AO', 0),
(11, 'ANGUILLA', 'AI', 0),
(12, 'ANTARCTICA', 'AQ', 0),
(13, 'ANTIGUA AND BARBUDA', 'AG', 0),
(14, 'ARGENTINA', 'AR', 0),
(15, 'ARMENIA', 'AM', 0),
(16, 'ARUBA', 'AW', 0),
(17, 'AUSTRALIA', 'AU', 1),
(18, 'AUSTRIA', 'AT', 1),
(19, 'AZERBAIJAN', 'AZ', 0),
(20, 'BAHAMAS', 'BS', 0),
(21, 'BAHRAIN', 'BH', 0),
(22, 'BANGLADESH', 'BD', 0),
(23, 'BARBADOS', 'BB', 0),
(24, 'BELARUS', 'BY', 0),
(25, 'BELGIUM', 'BE', 0),
(26, 'BELIZE', 'BZ', 0),
(27, 'BENIN', 'BJ', 0),
(28, 'BERMUDA', 'BM', 0),
(29, 'BHUTAN', 'BT', 0),
(30, 'BOLIVIA', 'BO', 0),
(31, 'BOSNIA AND HERZEGOVINA', 'BA', 0),
(32, 'BOTSWANA', 'BW', 0),
(33, 'BOUVET ISLAND', 'BV', 0),
(34, 'BRAZIL', 'BR', 0),
(35, 'BRITISH INDIAN OCEAN TERRITORY', 'IO', 0),
(36, 'BRUNEI DARUSSALAM', 'BN', 0),
(37, 'BULGARIA', 'BG', 0),
(38, 'BURKINA FASO', 'BF', 0),
(39, 'BURUNDI', 'BI', 0),
(40, 'CAMBODIA', 'KH', 0),
(41, 'CAMEROON', 'CM', 0),
(42, 'CANADA', 'CA', 0),
(43, 'CAPE VERDE', 'CV', 0),
(44, 'CAYMAN ISLANDS', 'KY', 0),
(45, 'CENTRAL AFRICAN REPUBLIC', 'CF', 0),
(46, 'CHAD', 'TD', 0),
(47, 'CHILE', 'CL', 0),
(48, 'CHINA', 'CN', 0),
(49, 'CHRISTMAS ISLAND', 'CX', 0),
(50, 'COCOS (KEELING) ISLANDS', 'CC', 0),
(51, 'COLOMBIA', 'CO', 0),
(52, 'COMOROS', 'KM', 0),
(53, 'CONGO', 'CG', 0),
(54, 'CONGO,THE DEMOCRATIC REPUBLIC OF THE', 'CD', 0),
(55, 'COOK ISLANDS', 'CK', 0),
(56, 'COSTA RICA', 'CR', 0),
(57, 'CROATIA', 'HR', 0),
(58, 'CUBA', 'CU', 0),
(59, 'CYPRUS', 'CY', 0),
(60, 'CZECH REPUBLIC', 'CZ', 0),
(61, 'DENMARK', 'DK', 0),
(62, 'DJIBOUTI', 'DJ', 0),
(63, 'DOMINICA', 'DM', 0),
(64, 'DOMINICAN REPUBLIC', 'DO', 0),
(65, 'ECUADOR', 'EC', 0),
(66, 'EGYPT', 'EG', 0),
(67, 'EL SALVADOR', 'SV', 0),
(68, 'EQUATORIAL GUINEA', 'GQ', 0),
(69, 'ERITREA', 'ER', 0),
(70, 'ESTONIA', 'EE', 0),
(71, 'ETHIOPIA', 'ET', 0),
(72, 'FALKLAND ISLANDS (MALVINAS)', 'FK', 0),
(73, 'FAROE ISLANDS', 'FO', 0),
(74, 'FIJI', 'FJ', 0),
(75, 'FINLAND', 'FI', 0),
(76, 'FRANCE', 'FR', 1),
(77, 'FRENCH GUIANA', 'GF', 1),
(78, 'FRENCH POLYNESIA', 'PF', 0),
(79, 'FRENCH SOUTHERN TERRITORIES', 'TF', 0),
(80, 'GABON', 'GA', 0),
(81, 'GAMBIA', 'GM', 0),
(82, 'GEORGIA', 'GE', 0),
(83, 'GERMANY', 'DE', 1),
(84, 'GHANA', 'GH', 0),
(85, 'GIBRALTAR', 'GI', 0),
(86, 'GREECE', 'GR', 0),
(87, 'GREENLAND', 'GL', 0),
(88, 'GRENADA', 'GD', 0),
(89, 'GUADELOUPE', 'GP', 0),
(90, 'GUAM', 'GU', 0),
(91, 'GUATEMALA', 'GT', 0),
(92, 'GUERNSEY', 'GG', 0),
(93, 'GUINEA', 'GN', 0),
(94, 'GUINEA-BISSAU', 'GW', 0),
(95, 'GUYANA', 'GY', 0),
(96, 'HAITI', 'HT', 0),
(97, 'HEARD ISLAND AND MCDONALD ISLANDS', 'HM', 0),
(98, 'HONDURAS', 'HN', 0),
(99, 'HONG KONG', 'HK', 0),
(100, 'HUNGARY', 'HU', 0),
(101, 'ICELAND', 'IS', 0),
(102, 'INDIA', 'IN', 0),
(103, 'INDONESIA', 'ID', 0),
(104, 'IRAN, ISLAMIC REPUBLIC OF', 'IR', 0),
(105, 'IRAQ', 'IQ', 0),
(106, 'IRELAND', 'IE', 0),
(107, 'ISLE OF MAN', 'IM', 0),
(108, 'ISRAEL', 'IL', 0),
(109, 'ITALY', 'IT', 0),
(110, 'JAMAICA', 'JM', 0),
(111, 'JAPAN', 'JP', 0),
(112, 'JERSEY', 'JE', 0),
(113, 'JORDAN', 'JO', 0),
(114, 'KAZAKHSTAN', 'KZ', 0),
(115, 'KENYA', 'KE', 0),
(116, 'KIRIBATI', 'KI', 0),
(117, 'KOREA, REPUBLIC OF', 'KR', 0),
(118, 'KUWAIT', 'KW', 0),
(119, 'KYRGYZSTAN', 'KG', 0),
(120, 'LATVIA', 'LV', 0),
(121, 'LEBANON', 'LB', 0),
(122, 'LESOTHO', 'LS', 0),
(123, 'LIBERIA', 'LR', 0),
(124, 'LIBYAN ARAB JAMAHIRIYA', 'LY', 0),
(125, 'LIECHTENSTEIN', 'LI', 0),
(126, 'LITHUANIA', 'LT', 0),
(127, 'LUXEMBOURG', 'LU', 0),
(128, 'MACAO', 'MO', 0),
(129, 'MACEDONI, THE FORMER YUGOSLAV REPUBLIC OF', 'MK', 0),
(130, 'MADAGASCAR', 'MG', 0),
(131, 'MALAWI', 'MW', 0),
(132, 'MALAYSIA', 'MY', 0),
(133, 'MALDIVES', 'MV', 0),
(134, 'MALI', 'ML', 0),
(135, 'MALTA', 'MT', 0),
(136, 'MARSHALL ISLANDS', 'MH', 0),
(137, 'MARTINIQUE', 'MQ', 0),
(138, 'MAURITANIA', 'MR', 0),
(139, 'MAURITIUS', 'MU', 0),
(140, 'MAYOTTE', 'YT', 0),
(141, 'MEXICO', 'MX', 0),
(142, 'MICRONESIA, FEDERATED STATES OF', 'FM', 0),
(143, 'MOLDOVA, REPUBLIC OF', 'MD', 0),
(144, 'MONACO', 'MC', 0),
(145, 'MONGOLIA', 'MN', 0),
(146, 'MONTENEGRO', 'ME', 0),
(147, 'MONTSERRAT', 'MS', 0),
(148, 'MOROCCO', 'MA', 0),
(149, 'MOZAMBIQUE', 'MZ', 0),
(150, 'MYANMAR', 'MM', 0),
(151, 'NAMIBIA', 'NA', 0),
(152, 'NAURU', 'NR', 0),
(153, 'NEPAL', 'NP', 0),
(154, 'NETHERLANDS', 'NL', 0),
(155, 'NETHERLANDS ANTILLES', 'AN', 0),
(156, 'NEW CALEDONIA', 'NC', 0),
(157, 'NEW ZEALAND', 'NZ', 0),
(158, 'NICARAGUA', 'NI', 0),
(159, 'NIGER', 'NE', 0),
(160, 'NIGERIA', 'NG', 0),
(161, 'NIUE', 'NU', 0),
(162, 'NORFOLK ISLAND', 'NF', 0),
(163, 'NORTHERN MARIANA ISLANDS', 'MP', 0),
(164, 'NORWAY', 'NO', 0),
(165, 'OMAN', 'OM', 0),
(166, 'PAKISTAN', 'PK', 0),
(167, 'PALAU', 'PW', 0),
(168, 'PALESTINIAN TERRITORY, OCCUPIED', 'PS', 0),
(169, 'PANAMA', 'PA', 0),
(170, 'PAPUA NEW GUINEA', 'PG', 0),
(171, 'PARAGUAY', 'PY', 0),
(172, 'PERU', 'PE', 0),
(173, 'PHILIPPINES', 'PH', 0),
(174, 'PITCAIRN', 'PN', 0),
(175, 'POLAND', 'PL', 0),
(176, 'PORTUGAL', 'PT', 0),
(177, 'PUERTO RICO', 'PR', 0),
(178, 'QATAR', 'QA', 0),
(179, 'R�UNION', 'RE', 0),
(180, 'ROMANIA', 'RO', 1),
(181, 'RUSSIAN FEDERATION', 'RU', 0),
(182, 'RWANDA', 'RW', 0),
(183, 'SAINT HELENA', 'SH', 0),
(184, 'SAINT KITTS AND NEVIS', 'KN', 0),
(185, 'SAINT LUCIA', 'LC', 0),
(186, 'SAINT PIERRE AND MIQUELON', 'PM', 0),
(187, 'SAINT VINCENT AND THE GRENADINES', 'VC', 0),
(188, 'SAMOA', 'WS', 0),
(189, 'SAN MARINO', 'SM', 0),
(190, 'SAO TOME AND PRINCIPE', 'ST', 0),
(191, 'SAUDI ARABIA', 'SA', 0),
(192, 'SENEGAL', 'SN', 0),
(193, 'SERBIA', 'RS', 0),
(194, 'SEYCHELLES', 'SC', 0),
(195, 'SIERRA LEONE', 'SL', 0),
(196, 'SINGAPORE', 'SG', 0),
(197, 'SLOVAKIA', 'SK', 0),
(198, 'SLOVENIA', 'SI', 0),
(199, 'SOLOMON ISLANDS', 'SB', 0),
(200, 'SOMALIA', 'SO', 0),
(201, 'SOUTH AFRICA', 'ZA', 0),
(202, 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'GS', 0),
(203, 'SPAIN', 'ES', 0),
(204, 'SRI LANKA', 'LK', 0),
(205, 'SUDAN', 'SD', 0),
(206, 'SURINAME', 'SR', 0),
(207, 'SVALBARD AND JAN MAYEN', 'SJ', 0),
(208, 'SWAZILAND', 'SZ', 0),
(209, 'SWEDEN', 'SE', 0),
(210, 'SWITZERLAND', 'CH', 1),
(211, 'SYRIAN ARAB REPUBLIC', 'SY', 0),
(212, 'TAIWAN, PROVINCE OF CHINA', 'TW', 0),
(213, 'TAJIKISTAN', 'TJ', 0),
(214, 'TANZANIA, UNITED REPUBLIC OF', 'TZ', 0),
(215, 'THAILAND', 'TH', 0),
(216, 'TIMOR-LESTE', 'TL', 0),
(217, 'TOGO', 'TG', 0),
(218, 'TOKELAU', 'TK', 0),
(219, 'TONGA', 'TO', 0),
(220, 'TRINIDAD AND TOBAGO', 'TT', 0),
(221, 'TUNISIA', 'TN', 0),
(222, 'TURKEY', 'TR', 0),
(223, 'TURKMENISTAN', 'TM', 0),
(224, 'TURKS AND CAICOS ISLANDS', 'TC', 0),
(225, 'TUVALU', 'TV', 0),
(226, 'UGANDA', 'UG', 0),
(227, 'UKRAINE', 'UA', 0),
(228, 'UNITED ARAB EMIRATES', 'AE', 0),
(229, 'UNITED KINGDOM', 'GB', 1),
(230, 'UNITED STATES', 'US', 1),
(231, 'UNITED STATES MINOR OUTLYING ISLANDS', 'UM', 0),
(232, 'URUGUAY', 'UY', 0),
(233, 'UZBEKISTAN', 'UZ', 0),
(234, 'VANUATU', 'VU', 0),
(235, 'VATICAN CITY STATE (HOLY SEE)', 'VA', 0),
(236, 'VENEZUELA', 'VE', 0),
(237, 'VIET NAM', 'VN', 0),
(238, 'VIRGIN ISLANDS, BRITISH', 'VG', 0),
(239, 'VIRGIN ISLANDS, U.S.', 'VI', 0),
(240, 'WALLIS AND FUTUNA', 'WF', 0),
(241, 'WESTERN SAHARA', 'EH', 0),
(242, 'YEMEN', 'YE', 0),
(243, 'ZAMBIA', 'ZM', 0),
(244, 'ZIMBABWE', 'ZW', 0);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_currency`
--

CREATE TABLE `sorin_jobsfactory_currency` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `convert` decimal(15,5) DEFAULT NULL,
  `default` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_currency`
--

INSERT INTO `sorin_jobsfactory_currency` (`id`, `name`, `ordering`, `convert`, `default`) VALUES
(1, 'USD', 4, '0.17166', NULL),
(2, 'EUR', 5, '0.22727', NULL),
(5, 'CHF', 2, '0.18598', NULL),
(8, 'AUD', 3, '0.14793', NULL),
(9, 'LEU', NULL, '1.00000', 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_education`
--

CREATE TABLE `sorin_jobsfactory_education` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  `resume_id` int(11) NOT NULL,
  `study_level` int(4) NOT NULL,
  `edu_institution` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_experiencelevel`
--

CREATE TABLE `sorin_jobsfactory_experiencelevel` (
  `id` int(11) NOT NULL,
  `levelname` varchar(150) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_experiencelevel`
--

INSERT INTO `sorin_jobsfactory_experiencelevel` (`id`, `levelname`, `published`, `ordering`) VALUES
(1, 'No experience', 1, NULL),
(2, '0-6 months', 1, NULL),
(3, '6-12 months', 1, NULL),
(4, '1-2 years', 1, NULL),
(5, '2-5 years', 1, NULL),
(6, '5-10 years', 1, NULL),
(7, 'over 10 years', 1, NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_fields`
--

CREATE TABLE `sorin_jobsfactory_fields` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `db_name` varchar(50) NOT NULL,
  `page` varchar(100) NOT NULL,
  `ftype` varchar(150) NOT NULL,
  `compulsory` tinyint(1) NOT NULL,
  `categoryfilter` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `own_table` varchar(100) NOT NULL,
  `validate_type` varchar(100) NOT NULL,
  `css_class` varchar(100) NOT NULL,
  `style_attr` text NOT NULL,
  `search` tinyint(1) NOT NULL,
  `params` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `help` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_fields`
--

INSERT INTO `sorin_jobsfactory_fields` (`id`, `name`, `db_name`, `page`, `ftype`, `compulsory`, `categoryfilter`, `status`, `own_table`, `validate_type`, `css_class`, `style_attr`, `search`, `params`, `ordering`, `help`) VALUES
(1, 'Sectors', 'sectors', 'jobs', 'selectmultiple', 1, 1, 1, '#__jobsfactory_jobs', '', '', '', 1, 'size=\"\"\nwidth=\"\"', 1, ''),
(2, 'Specialitate', 'specialitate', 'jobs', 'checkBox', 1, 1, 1, '#__jobsfactory_jobs', '', '', '', 1, '', 2, '');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_fields_assoc`
--

CREATE TABLE `sorin_jobsfactory_fields_assoc` (
  `id` int(11) NOT NULL,
  `field` varchar(50) DEFAULT NULL,
  `assoc_field` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_fields_assoc`
--

INSERT INTO `sorin_jobsfactory_fields_assoc` (`id`, `field`, `assoc_field`) VALUES
(1, 'name', ''),
(2, 'surname', ''),
(3, 'address', ''),
(4, 'city', ''),
(5, 'country', ''),
(6, 'phone', ''),
(7, 'paypalemail', ''),
(8, 'birth_date', ''),
(9, 'candidate_email', ''),
(10, 'linkedIN', ''),
(11, 'facebook', ''),
(12, 'isVisible', ''),
(13, 'about_us', ''),
(14, 'modified', ''),
(15, 'website', ''),
(16, 'email', ''),
(17, 'twitter', ''),
(18, 'googleMaps_x', ''),
(19, 'googleMaps_y', ''),
(20, 'YM', ''),
(21, 'Hotmail', ''),
(22, 'Skype', ''),
(23, 'verified', ''),
(24, 'powerseller', ''),
(25, 'useruniqueid', ''),
(26, 'contactname', ''),
(27, 'shortdescription', ''),
(28, 'contactposition', ''),
(29, 'contactphone', ''),
(30, 'contactemail', '');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_fields_categories`
--

CREATE TABLE `sorin_jobsfactory_fields_categories` (
  `id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `cid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_fields_categories`
--

INSERT INTO `sorin_jobsfactory_fields_categories` (`id`, `fid`, `cid`) VALUES
(37, 1, 1),
(38, 1, 2),
(39, 1, 3),
(40, 1, 4),
(41, 1, 5),
(42, 1, 6),
(43, 1, 7),
(44, 1, 8),
(45, 1, 9),
(46, 2, 1),
(47, 2, 2),
(48, 2, 3),
(49, 2, 4),
(50, 2, 5),
(51, 2, 6),
(52, 2, 7),
(53, 2, 8),
(54, 2, 9);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_fields_options`
--

CREATE TABLE `sorin_jobsfactory_fields_options` (
  `id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `option_name` varchar(255) NOT NULL,
  `ordering` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_fields_options`
--

INSERT INTO `sorin_jobsfactory_fields_options` (`id`, `fid`, `option_name`, `ordering`) VALUES
(1, 1, 'Clinici veterinare private', 1),
(2, 1, 'Ambulanta unei clinici veterinare', 2),
(3, 1, 'Clinici veterinare sociale sau subventionate', 3),
(4, 1, 'Universitati/Facultati', 4),
(5, 1, 'Liceu agricol/veterinar', 5),
(6, 1, 'Cercetare', 6),
(7, 1, 'Vanzari/promovare/domenii veterinare conexe', 7),
(8, 1, 'Receptie/personal auxiliar', 8),
(9, 1, 'Laborator', 9),
(10, 1, 'Institutie de stat de tip ANSVA/DSVSA, DSP etc', 10),
(11, 2, 'Chirurgie', 1),
(12, 2, 'Ecografie', 2),
(13, 2, 'Analize de laborator', 3),
(14, 2, 'Ortopedie', 4);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_fields_positions`
--

CREATE TABLE `sorin_jobsfactory_fields_positions` (
  `id` int(11) NOT NULL,
  `fieldid` int(11) NOT NULL,
  `templatepage` varchar(100) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `params` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_fields_positions`
--

INSERT INTO `sorin_jobsfactory_fields_positions` (`id`, `fieldid`, `templatepage`, `position`, `ordering`, `params`) VALUES
(4, 1, 'listjobs', 'cell-middle', 1, NULL),
(5, 2, 'listjobs', 'cell-middle', 2, NULL),
(6, 1, 'jobdetails', 'detail-right', 1, NULL),
(7, 2, 'jobdetails', 'detail-right', 2, NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_jobs`
--

CREATE TABLE `sorin_jobsfactory_jobs` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) NOT NULL DEFAULT '',
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `job_type` int(11) NOT NULL DEFAULT '0',
  `shortdescription` text,
  `description` text NOT NULL,
  `languages_request` mediumtext NOT NULL,
  `experience_request` text NOT NULL,
  `studies_request` tinyint(1) NOT NULL,
  `benefits` text NOT NULL,
  `job_cityname` varchar(255) NOT NULL,
  `job_cityid` int(11) NOT NULL,
  `job_location_state` int(11) NOT NULL,
  `job_country` int(11) NOT NULL,
  `about_visibility` tinyint(4) NOT NULL,
  `employer_visibility` tinyint(4) NOT NULL DEFAULT '0',
  `jobuniqueID` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jobs_available` int(4) NOT NULL DEFAULT '1',
  `closed_date` date NOT NULL DEFAULT '0000-00-00',
  `close_offer` int(11) NOT NULL DEFAULT '0',
  `close_by_admin` int(11) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cat` int(11) NOT NULL DEFAULT '0',
  `has_file` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(255) NOT NULL,
  `show_candidate_nr` tinyint(2) DEFAULT NULL,
  `featured` enum('featured','none') DEFAULT 'none',
  `cancel_reason` text NOT NULL,
  `googlex` varchar(255) NOT NULL,
  `googley` varchar(255) NOT NULL,
  `approved` tinyint(2) NOT NULL DEFAULT '0',
  `sectors` varchar(250) DEFAULT NULL,
  `specialitate` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_jobs`
--

INSERT INTO `sorin_jobsfactory_jobs` (`id`, `userid`, `title`, `published`, `job_type`, `shortdescription`, `description`, `languages_request`, `experience_request`, `studies_request`, `benefits`, `job_cityname`, `job_cityid`, `job_location_state`, `job_country`, `about_visibility`, `employer_visibility`, `jobuniqueID`, `start_date`, `end_date`, `jobs_available`, `closed_date`, `close_offer`, `close_by_admin`, `hits`, `modified`, `cat`, `has_file`, `file_name`, `show_candidate_nr`, `featured`, `cancel_reason`, `googlex`, `googley`, `approved`, `sectors`, `specialitate`) VALUES
(1, 632, 'Asistent Medicina Veterinara', 1, 1, '', '<p>eqwewqeqwew</p>', '', '1', 4, '', 'Bucharest', 0, 0, 0, 0, 0, '58bfd84f64da9', '2017-03-08 09:08:39', '2018-03-22 00:00:00', 1, '0000-00-00', 0, 0, 78, '2017-03-16 08:54:30', 2, 0, '', 1, 'none', '', '44.43377984606822', '26.103515625', 1, 'Clinici veterinare sociale sau subventionate', NULL),
(2, 632, 'Medic veterinar ecvine', 1, 0, '', '<p>sdvgdsgrtet</p>', '', '1', 0, '', 'Bucharest', 0, 0, 0, 1, 1, '58bfd8c6b4bdf', '2017-03-08 09:10:43', '2017-03-31 00:00:00', 1, '0000-00-00', 0, 0, 1, '2017-03-16 08:55:20', 4, 0, '', 1, 'none', '', '', '', 1, '', NULL),
(3, 632, 'Medic specialist', 1, 5, '', '<p>cvcsdbds</p>', '', '1', 3, '', 'Bucharest', 0, 0, 0, 0, 0, '58c14f0415708', '2017-03-09 11:47:28', '2017-03-30 00:00:00', 1, '0000-00-00', 0, 0, 0, '2017-03-15 12:21:57', 3, 0, '', 1, 'none', '', '45.460130637921004', '24.85107421875', 1, 'Cercetare', NULL),
(4, 632, 'Internship', 1, 5, '', '<p>eqweqe</p>', '', '1', 0, '', 'Bucharest', 0, 0, 0, 1, 1, '58c14fb5dc439', '2017-03-09 11:50:37', '2017-03-31 00:00:00', 1, '0000-00-00', 0, 0, 1, '2017-03-16 08:56:33', 1, 0, '', 1, 'none', '', '44.482789890501586', '26.034164428710938', 1, 'Clinici veterinare private,Clinici veterinare sociale sau subventionate', NULL),
(5, 632, 'Medic Primar animale exotice', 1, 0, '', '<p>animal</p>', '', '4', 4, '', 'Otopeni', 0, 0, 0, 1, 1, '58ca55d6f0a38', '2017-03-16 08:05:33', '2017-03-23 00:00:00', 1, '0000-00-00', 0, 0, 6, '2017-03-16 08:05:33', 5, 0, '', 1, 'none', '', '44.55329208318496', '26.07433319091797', 1, 'Clinici veterinare private', 'Chirurgie, Ecografie'),
(6, 632, 'Featured1', 1, 0, '', '<p>addasdasd</p>', '', '1', 0, '', 'Bucharest', 0, 0, 0, 0, 1, '58d8eb7216e5d', '2017-03-27 08:36:21', '2017-04-03 00:00:00', 1, '0000-00-00', 0, 0, 20, '2017-03-27 10:39:41', 2, 0, '', 1, 'featured', '', '44.40631625266136', '26.195526123046875', 1, 'Clinici veterinare private', 'Chirurgie, Ecografie, Analize de laborator');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_jobtype`
--

CREATE TABLE `sorin_jobsfactory_jobtype` (
  `id` int(11) NOT NULL,
  `typename` varchar(150) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_jobtype`
--

INSERT INTO `sorin_jobsfactory_jobtype` (`id`, `typename`, `published`, `ordering`) VALUES
(1, 'Full time', 1, 1),
(2, 'Part time', 1, 2),
(3, 'Project-based', 0, 3),
(4, 'Temporary', 1, NULL),
(5, 'Practice', 1, NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_locations`
--

CREATE TABLE `sorin_jobsfactory_locations` (
  `id` int(11) NOT NULL,
  `locname` varchar(250) NOT NULL,
  `country` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_log`
--

CREATE TABLE `sorin_jobsfactory_log` (
  `id` int(11) NOT NULL,
  `priority` enum('log','notice','warning','error','system') DEFAULT 'log',
  `event` varchar(50) DEFAULT NULL,
  `logtime` datetime DEFAULT NULL,
  `log` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_mails`
--

CREATE TABLE `sorin_jobsfactory_mails` (
  `id` int(11) NOT NULL,
  `mail_type` varchar(250) DEFAULT NULL,
  `group` enum('candidate','company','admin','watchlist','job') DEFAULT 'company',
  `content` text,
  `subject` varchar(250) DEFAULT NULL,
  `enabled` int(1) UNSIGNED ZEROFILL DEFAULT '0',
  `ordering` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_mails`
--

INSERT INTO `sorin_jobsfactory_mails` (`id`, `mail_type`, `group`, `content`, `subject`, `enabled`, `ordering`) VALUES
(1, 'new_message', 'candidate', 'Dear %NAME%  %SURNAME%,  You received a new reply to your application regarding the Job:  %JOBTITLE%  You can see the message here  %JOBLINK%  You must login to read the message.', 'New message regarding Job %JOBTITLE%', 1, 4),
(2, 'new_application', 'company', 'Dear %NAME% %SURNAME%,  You received a new Application for your job %JOBTITLE%  You can get more details on the webpage  %JOBLINK%', 'New application on your Job %JOBTITLE%', 1, 2),
(3, 'new_resume_created', 'candidate', 'Dear %NAME% %SURNAME%,  Your resume has been created successfully.', 'Your resume has been created successfully', 0, 3),
(4, 'job_application_canceled', 'job', 'One application was cancelled', 'One application was canceled', 0, 20),
(5, 'job_watchlist_canceled', 'watchlist', 'Dear %NAME% %SURNAME%,  The job %JOBTITLE% from your watchlist was canceled!  You can get more details on the webpage  %JOBLINK%', 'Informations about your Watched Job  %JOBTITLE%', 0, 19),
(6, 'job_canceled', 'candidate', 'Dear %NAME% %SURNAME%,  The job %JOBTITLE%  was canceled!  You can get more details on the webpage  %JOBLINK%', 'Informations about job on  %JOBTITLE%', 1, 17),
(7, 'job_watchlist_closed', 'watchlist', 'Dear %NAME% %SURNAME%,  The job %JOBTITLE% from your watchlist was closed !  You can get more details on the webpage  %JOBLINK%', 'Informations about your Watched Job %JOBTITLE%', 0, 18),
(8, 'job_closed', 'candidate', 'Dear %NAME% %SURNAME%,  The job %JOBTITLE% was closed !  You can get more details on the webpage  %JOBLINK%', 'Informations about job on %JOBTITLE%', 1, 16),
(9, 'job_admin_message', 'company', 'Dear %NAME% %SURNAME%,  You received a message from one of the Administrators regarding your job %JOBTITLE%  !  You can get more details on the webpage  %JOBLINK%', 'New Message from an Admin regarding %JOBTITLE%', 1, 5),
(10, 'job_your_will_expire', 'company', 'Dear %NAME% %SURNAME%,  Your Job %JOBTITLE% will soon expire !  You can get more details on the webpage  %JOBLINK%', 'Your Job %JOBTITLE% will soon expire', 1, 10),
(11, 'job_watchlist_will_expire', 'watchlist', 'Dear %NAME% %SURNAME%,  The Job you watched: %JOBTITLE% will soon expire !  You can get more details on the webpage  %JOBLINK%', 'Informations about your watched Job  %JOBTITLE%', 1, 11),
(12, 'new_job_watch', 'watchlist', 'Dear %NAME% %SURNAME%,  There is a new Job in the category you watched: %JOBTITLE%  You can get more details on the webpage  %JOBLINK%', 'Informations about your watched Category  %CATTITLE%', 0, 6),
(13, 'new_job_watch_company', 'watchlist', 'Dear %NAME% %SURNAME%,  There is a new Job from the company you watched: %JOBTITLE%  You can get more details on the webpage  %JOBLINK%', 'Informations about your watched company  %NAME%', 0, 7),
(14, 'new_job_watch_location', 'watchlist', 'Dear %NAME% %SURNAME%,  There is a new Job in the location you watched: %JOBTITLE%  You can get more details on the webpage  %JOBLINK%', 'Informations about your watched location', 0, 8),
(15, 'new_job_watch_city', 'watchlist', 'Dear %NAME% %SURNAME%,  There is a new Job in the city you watched: %JOBTITLE%  You can get more details on the webpage  %JOBLINK%', 'Informations about your watched city', 0, 9),
(16, 'new_job_created', 'company', 'Dear %NAME% %SURNAME%,  Your Job has been created successfully: %JOBTITLE%  You can get more details on the webpage  %JOBLINK%', 'Your Job: %JOBTITLE% has been created successfully', 1, 1),
(17, 'job_admin_close_job', 'company', 'Dear %NAME% %SURNAME%,  Your Job has been closed by admin - %JOBTITLE%  You can get more details on the webpage  %JOBLINK%', 'Your Job: %JOBTITLE% has been closed by Admin', 0, 15),
(18, 'job_admin_approval', 'admin', 'Dear %NAME% %SURNAME%,  A new job was placed  - %JOBTITLE%  and it needs Admin Approval.', 'A new job has been placed: %JOBTITLE%', 1, 12),
(19, 'admin_job_approved', 'admin', 'Dear %NAME% %SURNAME%,  Your job was approved by Admin - %JOBTITLE%.', 'Approved job link: %JOBTITLE%', 1, 13),
(20, 'admin_job_unapproved', 'admin', 'Dear %NAME% %SURNAME%,  Your job was unApproved by Admin - %JOBTITLE% .', 'UnApproved job link: %JOBTITLE%', 1, 14);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_messages`
--

CREATE TABLE `sorin_jobsfactory_messages` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL DEFAULT '0',
  `userid1` int(11) NOT NULL DEFAULT '0',
  `userid2` int(11) NOT NULL DEFAULT '0',
  `parent_message` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `app_id` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `wasread` int(1) DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_payment_balance`
--

CREATE TABLE `sorin_jobsfactory_payment_balance` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `balance` decimal(11,2) DEFAULT NULL,
  `req_withdraw` decimal(11,0) NOT NULL DEFAULT '0' COMMENT 'Requested withdraw amount',
  `last_withdraw_date` date NOT NULL,
  `paid_withdraw_date` date NOT NULL,
  `withdrawn_until_now` decimal(11,0) NOT NULL DEFAULT '0' COMMENT 'Withdraw until now',
  `currency` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_payment_balance`
--

INSERT INTO `sorin_jobsfactory_payment_balance` (`id`, `userid`, `balance`, `req_withdraw`, `last_withdraw_date`, `paid_withdraw_date`, `withdrawn_until_now`, `currency`) VALUES
(1, 632, '0.00', '0', '0000-00-00', '0000-00-00', '0', 'LEU'),
(2, 631, '0.00', '0', '0000-00-00', '0000-00-00', '0', 'LEU'),
(3, 633, '0.00', '0', '0000-00-00', '0000-00-00', '0', 'LEU'),
(4, 634, '0.00', '0', '0000-00-00', '0000-00-00', '0', 'LEU');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_payment_log`
--

CREATE TABLE `sorin_jobsfactory_payment_log` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `currency` varchar(11) DEFAULT NULL,
  `refnumber` varchar(100) DEFAULT NULL,
  `invoice` varchar(50) DEFAULT NULL,
  `ipn_response` text,
  `ipn_ip` varchar(100) DEFAULT NULL,
  `status` enum('ok','error','manual_check','cancelled','refunded') DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `orderid` int(11) NOT NULL,
  `gatewayid` int(11) DEFAULT NULL,
  `payment_method` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_payment_log`
--

INSERT INTO `sorin_jobsfactory_payment_log` (`id`, `date`, `amount`, `currency`, `refnumber`, `invoice`, `ipn_response`, `ipn_ip`, `status`, `userid`, `orderid`, `gatewayid`, `payment_method`) VALUES
(1, '2017-03-27 10:39:41', '50.00', 'LEU', 'Admin approved', '1', '', '::1', 'ok', 632, 1, NULL, 'Admin approved');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_payment_orderitems`
--

CREATE TABLE `sorin_jobsfactory_payment_orderitems` (
  `id` int(11) NOT NULL,
  `orderid` int(11) DEFAULT NULL,
  `itemname` varchar(30) DEFAULT NULL,
  `itemdetails` varchar(250) DEFAULT NULL,
  `iteminfo` varchar(150) DEFAULT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `quantity` int(11) DEFAULT '1',
  `params` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_payment_orderitems`
--

INSERT INTO `sorin_jobsfactory_payment_orderitems` (`id`, `orderid`, `itemname`, `itemdetails`, `iteminfo`, `price`, `currency`, `quantity`, `params`) VALUES
(1, 1, 'featured', 'Pay for Featured', '6', '50.00', 'LEU', 1, '');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_payment_orders`
--

CREATE TABLE `sorin_jobsfactory_payment_orders` (
  `id` int(11) NOT NULL,
  `orderdate` datetime DEFAULT NULL,
  `modifydate` datetime DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `order_total` decimal(11,2) DEFAULT NULL,
  `order_currency` varchar(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `paylogid` int(11) DEFAULT NULL,
  `params` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_payment_orders`
--

INSERT INTO `sorin_jobsfactory_payment_orders` (`id`, `orderdate`, `modifydate`, `userid`, `order_total`, `order_currency`, `status`, `paylogid`, `params`) VALUES
(1, '2017-03-27 10:37:53', NULL, 632, '50.00', 'LEU', 'C', 1, NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_paysystems`
--

CREATE TABLE `sorin_jobsfactory_paysystems` (
  `id` int(11) NOT NULL,
  `paysystem` varchar(50) DEFAULT NULL,
  `classname` varchar(50) DEFAULT NULL,
  `enabled` int(1) DEFAULT '1',
  `params` text,
  `ordering` int(11) DEFAULT NULL,
  `isdefault` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_paysystems`
--

INSERT INTO `sorin_jobsfactory_paysystems` (`id`, `paysystem`, `classname`, `enabled`, `params`, `ordering`, `isdefault`) VALUES
(1, 'Paypal', 'pay_paypal', 1, 'paypalemail=\"change@me.com1\"\nuse_sandbox=\"1\"\nauto_accept=\"1\"', 1, 1),
(2, 'Moneybookers', 'pay_moneybookers', 1, 'email=\"change@me.com1\"', 2, 0),
(3, '2Checkout', 'pay_2checkout', 0, 'x_login=\"change@me.com2\"\ntest_mode=\"1\"', 3, 0),
(4, 'BankTransfer', 'pay_banktransfer', 1, 'bank_info=\"<p>Info</p>\"', 4, 0),
(5, 'Sagepayments.net', 'pay_sagepayments', 0, 'Terminal_id=\"2131231\"\nLogo_url=\"\"\nB_color=\"\"\nBF_color=\"\"\nM_color=\"\"\nF_color=\"\"', 6, 0),
(6, 'Authorize.net', 'pay_authnet', 1, 'loginID=\"\"\r\ntransactionKey=\"\"\r\nsandboxMode=\"\"\r\nheader_html_payment_form=\"\"\r\nfooter_html_payment_form=\"\"\r\nheader2_html_payment_form=\"\"\r\nfooter2_html_payment_form=\"\"\r\ncolor_background=\"\"\r\ncolor_link=\"\"\r\ncolor_text=\"\"\r\nlogo_url=\"\"\r\nbackground_url=\"\"\r\ncancel_url_text=\"\"', NULL, 0),
(7, 'Mollie', 'pay_mollie', 0, NULL, 7, 0);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_pricing`
--

CREATE TABLE `sorin_jobsfactory_pricing` (
  `id` int(11) NOT NULL,
  `itemname` varchar(50) DEFAULT NULL,
  `pricetype` enum('percent','fixed') DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` decimal(9,2) DEFAULT NULL,
  `currency` varchar(11) DEFAULT NULL,
  `enabled` int(1) DEFAULT NULL,
  `params` text,
  `ordering` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_pricing`
--

INSERT INTO `sorin_jobsfactory_pricing` (`id`, `itemname`, `pricetype`, `name`, `price`, `currency`, `enabled`, `params`, `ordering`) VALUES
(1, 'featured', 'fixed', 'Featured Job', '50.00', 'LEU', 1, 'price_powerseller=\"37\"\nprice_verified=\"28\"\ncategory_pricing_enabled=', 2),
(2, 'listing', 'fixed', 'Pay per Listing', '10.00', 'USD', 0, 'price_powerseller=\"\"\nprice_verified=\"\"\ncategory_pricing_enabled=\nemail_text=\"Hello %SURNAME%,\nWe kindly remind you that you have an outstanding payment of %BALANCE% for the services provided by our site.\nLog in using your username \"%USERNAME%\" and follow this link in order to pay the fees:\n%LINK%\"', 1),
(3, 'comission', 'percent', 'Earn Commission', '10.00', 'USD', 0, 'price_powerseller=\"5\"\nprice_verified=\"1\"\ncategory_pricing_enabled=\nemail_text=\"PHA+SGVsbG8gJVNVUk5BTUUlLGE8L3A+DQo8cD7CoDwvcD4NCjxwPmFkIC0gJUJBTEFOQ0UlPC9wPg0KPHA+YXMgLSAlTElOSyU8L3A+DQo8cD5kYTwvcD4NCjxwPnNkPC9wPg0KPHA+YXM8L3A+\"', 2),
(4, 'contact', 'fixed', 'Pay per Contact', '44.00', 'USD', 0, 'price_powerseller=\"4\"\nprice_verified=\"2\"\ncategory_pricing_enabled=', 2);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_pricing_categories`
--

CREATE TABLE `sorin_jobsfactory_pricing_categories` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `price` decimal(11,2) NOT NULL,
  `itemname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_pricing_comissions`
--

CREATE TABLE `sorin_jobsfactory_pricing_comissions` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `app_id` int(11) NOT NULL,
  `comission_date` datetime DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `currency` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_pricing_contacts`
--

CREATE TABLE `sorin_jobsfactory_pricing_contacts` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `purchase_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_ratings`
--

CREATE TABLE `sorin_jobsfactory_ratings` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL COMMENT 'gives rating',
  `rated_userid` int(11) NOT NULL,
  `rating_user` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_ratings`
--

INSERT INTO `sorin_jobsfactory_ratings` (`id`, `userid`, `rated_userid`, `rating_user`) VALUES
(1, 631, 632, 5);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_report_jobs`
--

CREATE TABLE `sorin_jobsfactory_report_jobs` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `message` varchar(200) NOT NULL DEFAULT '',
  `processing` int(11) NOT NULL DEFAULT '0',
  `solved` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_studieslevel`
--

CREATE TABLE `sorin_jobsfactory_studieslevel` (
  `id` int(11) NOT NULL,
  `levelname` varchar(150) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_studieslevel`
--

INSERT INTO `sorin_jobsfactory_studieslevel` (`id`, `levelname`, `published`, `ordering`) VALUES
(1, 'School', 1, NULL),
(2, 'Highschool', 1, NULL),
(3, 'College being', 1, NULL),
(4, 'College graduate', 1, NULL),
(5, 'Vocational school', 1, NULL),
(6, 'University', 0, NULL),
(7, 'Master', 1, NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_tags`
--

CREATE TABLE `sorin_jobsfactory_tags` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `tagname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_userexperience`
--

CREATE TABLE `sorin_jobsfactory_userexperience` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `salary` int(11) NOT NULL,
  `city` varchar(150) NOT NULL,
  `country` int(11) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `exp_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_users`
--

CREATE TABLE `sorin_jobsfactory_users` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  `isCompany` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `surname` varchar(50) NOT NULL DEFAULT '',
  `birth_date` datetime NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `marital_status` varchar(200) NOT NULL,
  `address` varchar(150) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(150) NOT NULL DEFAULT '',
  `phone` text NOT NULL,
  `email` varchar(150) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `googleMaps_x` varchar(255) DEFAULT NULL,
  `googleMaps_y` varchar(255) DEFAULT NULL,
  `shortdescription` text NOT NULL,
  `about_us` text NOT NULL,
  `website` varchar(200) DEFAULT NULL,
  `activitydomain` varchar(255) DEFAULT NULL,
  `contactname` varchar(100) NOT NULL,
  `contactposition` varchar(255) NOT NULL,
  `contactphone` text NOT NULL,
  `contactemail` varchar(255) NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `YM` varchar(255) DEFAULT NULL,
  `Hotmail` varchar(255) DEFAULT NULL,
  `Skype` varchar(255) DEFAULT NULL,
  `linkedIN` text NOT NULL,
  `total_experience` int(4) NOT NULL,
  `desired_salary` int(4) NOT NULL,
  `user_achievements` text NOT NULL,
  `user_goals` text NOT NULL,
  `user_hobbies` text NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `verified` int(1) DEFAULT '0',
  `powerseller` int(1) DEFAULT '0',
  `isVisible` tinyint(1) NOT NULL,
  `useruniqueid` varchar(255) NOT NULL,
  `cv_notified` int(1) DEFAULT '0',
  `cv_date_notified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_users`
--

INSERT INTO `sorin_jobsfactory_users` (`id`, `userid`, `isCompany`, `name`, `surname`, `birth_date`, `gender`, `marital_status`, `address`, `city`, `country`, `phone`, `email`, `picture`, `googleMaps_x`, `googleMaps_y`, `shortdescription`, `about_us`, `website`, `activitydomain`, `contactname`, `contactposition`, `contactphone`, `contactemail`, `facebook`, `twitter`, `YM`, `Hotmail`, `Skype`, `linkedIN`, `total_experience`, `desired_salary`, `user_achievements`, `user_goals`, `user_hobbies`, `file_name`, `modified`, `verified`, `powerseller`, `isVisible`, `useruniqueid`, `cv_notified`, `cv_date_notified`) VALUES
(1, 632, 1, 'Company1', '', '0000-00-00 00:00:00', 0, '', 'dambului str nr 54', 'Bucharest', '180', '', '', 'logo_632.png', '44.43377984606822', '26.103515625', 'company', '<p>rwerewwr</p>', 'www.company1.ro', NULL, 'company`', 'kbj', '786786', 'snnca@yahoo.com', 'www.facebook.com/company11', 'ewqewqqwe', NULL, NULL, NULL, '', 0, 0, '', '', '', '', '2017-03-20 11:39:48', 0, 0, 1, '58bfd22df297e', 0, NULL),
(2, 631, 0, 'sorin', '', '2002-03-08 00:00:00', 1, '', '', '', '', '', '', '', NULL, NULL, '', '', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 1, 0, '', '', '', '', '2017-03-08 10:45:18', 0, 0, 1, '58bfe0be566eb', 0, NULL),
(3, 633, 0, 'Suzie', '', '2002-03-16 00:00:00', 0, '', '', '', '180', '', '', '', NULL, NULL, '', '', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 4, 900, '', '', '', '', '2017-03-16 09:00:23', 0, 0, 1, '58ca54277bcb9', 0, NULL),
(4, 634, 1, 'Company2', '', '0000-00-00 00:00:00', 0, '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', NULL, NULL, NULL, '', 0, 0, '', '', '', '', '2017-03-16 12:05:37', 0, 0, 0, '58ca7f915eab3', 0, NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_users_rating`
--

CREATE TABLE `sorin_jobsfactory_users_rating` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL COMMENT 'rated user',
  `user_rating` decimal(5,2) NOT NULL,
  `users_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_users_rating`
--

INSERT INTO `sorin_jobsfactory_users_rating` (`id`, `userid`, `user_rating`, `users_count`) VALUES
(1, 632, '5.00', 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_watchlist`
--

CREATE TABLE `sorin_jobsfactory_watchlist` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  `job_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_jobsfactory_watchlist`
--

INSERT INTO `sorin_jobsfactory_watchlist` (`id`, `userid`, `job_id`) VALUES
(1, 631, 1),
(2, 633, 5);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_watchlist_cats`
--

CREATE TABLE `sorin_jobsfactory_watchlist_cats` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_watchlist_city`
--

CREATE TABLE `sorin_jobsfactory_watchlist_city` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  `cityid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_watchlist_companies`
--

CREATE TABLE `sorin_jobsfactory_watchlist_companies` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  `companyid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_jobsfactory_watchlist_loc`
--

CREATE TABLE `sorin_jobsfactory_watchlist_loc` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  `locationid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_languages`
--

CREATE TABLE `sorin_languages` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(11) NOT NULL,
  `lang_code` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_native` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sef` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_languages`
--

INSERT INTO `sorin_languages` (`lang_id`, `asset_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 0, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_menu`
--

CREATE TABLE `sorin_menu` (
  `id` int(11) NOT NULL,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_menu`
--

INSERT INTO `sorin_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 211, 0, '*', 0),
(2, 'menu', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 0, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'menu', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'menu', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 0, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'menu', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'menu', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'menu', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 0, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 43, 48, 0, '*', 1),
(8, 'menu', 'com_contact', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 0, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 44, 45, 0, '*', 1),
(9, 'menu', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 0, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 46, 47, 0, '*', 1),
(10, 'menu', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 0, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 49, 54, 0, '*', 1),
(11, 'menu', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 50, 51, 0, '*', 1),
(12, 'menu', 'com_messages_read', 'Read Private Message', '', 'Messaging/Read Private Message', 'index.php?option=com_messages', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-read', 0, '', 52, 53, 0, '*', 1),
(13, 'menu', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 55, 60, 0, '*', 1),
(14, 'menu', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 56, 57, 0, '*', 1),
(15, 'menu', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 0, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 58, 59, 0, '*', 1),
(16, 'menu', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 0, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 61, 62, 0, '*', 1),
(17, 'menu', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 0, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 63, 64, 0, '*', 1),
(18, 'menu', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 0, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 65, 66, 0, '*', 1),
(19, 'menu', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 67, 68, 0, '*', 1),
(20, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 0, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 69, 70, 0, '', 1),
(21, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 0, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 71, 72, 0, '*', 1),
(355, 'callisto-theme', 'Home', 'callisto', '', 'callisto', 'index.php?option=com_gantry5&view=custom', 'component', 1, 1, 1, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 73, 74, 0, '*', 0),
(356, 'callisto-theme', 'Features', 'callisto-features', '', 'callisto-features', 'index.php?option=com_gantry5&view=custom', 'separator', 1, 1, 1, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 75, 86, 0, '*', 0),
(357, 'callisto-theme', 'Overview', 'overview', '', 'callisto-features/overview', 'index.php?option=com_gantry5&view=custom', 'component', 1, 356, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 76, 77, 0, '*', 0),
(358, 'callisto-theme', 'Typography', 'typography', '', 'callisto-features/typography', 'index.php?option=com_gantry5&view=custom', 'component', 1, 356, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 78, 79, 0, '*', 0),
(359, 'callisto-theme', 'Block Variations', 'variations', '', 'callisto-features/variations', 'index.php?option=com_gantry5&view=custom', 'component', 1, 356, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 80, 81, 0, '*', 0),
(360, 'callisto-theme', 'Documentation', 'documentation', '', 'callisto-features/documentation', 'http://www.rockettheme.com/docs/joomla/templates/callisto', 'url', 1, 356, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 82, 83, 0, '*', 0),
(361, 'callisto-theme', 'Forum Support', 'support', '', 'callisto-features/support', 'http://www.rockettheme.com/forum/joomla-template-callisto', 'url', 1, 356, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 84, 85, 0, '*', 0),
(362, 'callisto-theme', 'Pages', 'callisto-pages', '', 'callisto-pages', 'index.php?option=com_gantry5&view=custom', 'separator', 1, 1, 1, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 87, 112, 0, '*', 0),
(363, 'callisto-theme', 'About Us', 'about-us', '', 'callisto-pages/about-us', 'index.php?option=com_gantry5&view=custom', 'component', 1, 362, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 88, 89, 0, '*', 0),
(364, 'callisto-theme', 'Our Team', 'our-team', '', 'callisto-pages/our-team', 'index.php?option=com_gantry5&view=custom', 'component', 1, 362, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 90, 91, 0, '*', 0),
(365, 'callisto-theme', 'Services', 'services', '', 'callisto-pages/services', 'index.php?option=com_gantry5&view=custom', 'component', 1, 362, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 92, 93, 0, '*', 0),
(366, 'callisto-theme', 'Pricing', 'pricing', '', 'callisto-pages/pricing', 'index.php?option=com_gantry5&view=custom', 'component', 1, 362, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 94, 95, 0, '*', 0),
(367, 'callisto-theme', 'Portfolio', 'portfolio', '', 'callisto-pages/portfolio', 'index.php?option=com_gantry5&view=custom', 'component', 1, 362, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 96, 97, 0, '*', 0),
(368, 'callisto-theme', 'Clients', 'clients', '', 'callisto-pages/clients', 'index.php?option=com_gantry5&view=custom', 'component', 1, 362, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 98, 99, 0, '*', 0),
(369, 'callisto-theme', 'Faq', 'faq', '', 'callisto-pages/faq', 'index.php?option=com_gantry5&view=custom', 'component', 1, 362, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 100, 101, 0, '*', 0),
(370, 'callisto-theme', 'Blog', 'blog', '', 'callisto-pages/blog', 'index.php?option=com_content&view=category&layout=blog&id=8', 'component', 1, 362, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"layout_type\":\"blog\",\"show_category_heading_title_text\":\"\",\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"show_cat_tags\":\"\",\"page_subheading\":\"\",\"num_leading_articles\":\"0\",\"num_intro_articles\":\"4\",\"num_columns\":\"1\",\"num_links\":\"4\",\"multi_column_order\":\"\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"order\",\"order_date\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"show_featured\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 102, 103, 0, '*', 0),
(371, 'callisto-theme', 'Contact', 'contact', '', 'callisto-pages/contact', 'index.php?option=com_contact&view=contact&id=1', 'component', 1, 362, 2, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"presentation_style\":\"\",\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"show_tags\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_links\":\"\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 104, 105, 0, '*', 0),
(372, 'callisto-theme', 'Error', 'error', '', 'callisto-pages/error', 'index.php?option=com_gantry5&view=error', 'component', 1, 362, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 106, 107, 0, '*', 0),
(373, 'callisto-theme', 'Coming Soon', 'coming-soon', '', 'callisto-pages/coming-soon', 'index.php?option=com_gantry5&view=custom', 'component', 1, 362, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 108, 109, 0, '*', 0),
(374, 'callisto-theme', 'Offline', 'offline', '', 'callisto-pages/offline', 'index.php?option=com_gantry5&view=custom', 'component', 1, 362, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 110, 111, 0, '*', 0),
(375, 'callisto-theme', 'Styles', 'callisto-styles', '', 'callisto-styles', 'index.php?option=com_gantry5&view=custom', 'separator', 1, 1, 1, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 113, 114, 0, '*', 0),
(376, 'callisto-theme', 'Download', 'callisto-download', '', 'callisto-download', 'http://www.rockettheme.com/joomla/templates/callisto', 'url', 1, 1, 1, 703, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{}', 115, 116, 0, '*', 0),
(389, 'main', 'COM_JOBS', 'com-jobs', '', 'com-jobs', 'index.php?option=com_jobsfactory', 'component', 0, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 117, 134, 0, '', 1),
(390, 'main', 'COM_JOBS_MENU_LIST', 'com-jobs-menu-list', '', 'com-jobs/com-jobs-menu-list', 'index.php?option=com_jobsfactory&task=jobs', 'component', 0, 389, 2, 804, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 118, 119, 0, '', 1),
(391, 'main', 'COM_JOBS_MENU_PAYMENTS', 'com-jobs-menu-payments', '', 'com-jobs/com-jobs-menu-payments', 'index.php?option=com_jobsfactory&task=payments.listing', 'component', 0, 389, 2, 804, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 120, 121, 0, '', 1),
(392, 'main', 'COM_JOBS_MENU_MESSAGES', 'com-jobs-menu-messages', '', 'com-jobs/com-jobs-menu-messages', 'index.php?option=com_jobsfactory&task=comments_administrator', 'component', 0, 389, 2, 804, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 122, 123, 0, '', 1),
(393, 'main', 'COM_JOBS_MENU_REPORTED', 'com-jobs-menu-reported', '', 'com-jobs/com-jobs-menu-reported', 'index.php?option=com_jobsfactory&task=reported_jobs', 'component', 0, 389, 2, 804, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 124, 125, 0, '', 1),
(394, 'main', 'COM_JOBS_MENU_USERS', 'com-jobs-menu-users', '', 'com-jobs/com-jobs-menu-users', 'index.php?option=com_jobsfactory&task=users', 'component', 0, 389, 2, 804, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 126, 127, 0, '', 1),
(395, 'main', 'COM_JOBS_MENU_SETTINGS', 'com-jobs-menu-settings', '', 'com-jobs/com-jobs-menu-settings', 'index.php?option=com_jobsfactory&task=settingsmanager', 'component', 0, 389, 2, 804, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 128, 129, 0, '', 1),
(396, 'main', 'COM_JOBS_MENU_DASHBOARD', 'com-jobs-menu-dashboard', '', 'com-jobs/com-jobs-menu-dashboard', 'index.php?option=com_jobsfactory&task=dashboard', 'component', 0, 389, 2, 804, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 130, 131, 0, '', 1),
(397, 'main', 'COM_JOBS_MENU_ABOUT', 'com-jobs-menu-about', '', 'com-jobs/com-jobs-menu-about', 'index.php?option=com_jobsfactory&task=about.main', 'component', 0, 389, 2, 804, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 132, 133, 0, '', 1),
(398, 'jobsmainmenu', 'Jobs', 'show-jobs', '', 'show-jobs', 'index.php?option=com_jobsfactory&task=listjobs', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1}', 135, 136, 0, '', 0),
(399, 'jobsmainmenu', 'Categories', 'categories', '', 'categories', 'index.php?option=com_jobsfactory&task=categories', 'component', -2, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1}', 137, 138, 0, '', 0),
(400, 'jobsmainmenu', 'Search', 'search-jobs', '', 'search-jobs', 'index.php?option=com_jobsfactory&task=show_search', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1}', 139, 140, 0, '', 0),
(401, 'jobsmainmenu', 'Companies List', 'all-companies-list', '', 'all-companies-list', 'index.php?option=com_jobsfactory&view=companies&task=listcompanies', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1}', 141, 142, 0, '', 0),
(402, 'jobsmainmenu', 'Candidates List', 'all-candidates-list', '', 'all-candidates-list', 'index.php?option=com_jobsfactory&view=candidates&layout=listcandidates&task=listcandidates', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 8, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 143, 144, 0, '*', 0),
(403, 'jobsmainmenu', 'My Jobs Openings', 'my-jobs', '', 'my-jobs', 'index.php?option=com_jobsfactory&view=jobs&layout=myjobs&task=myjobs', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 8, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 145, 146, 0, '*', 0),
(404, 'jobsmainmenu', 'Add New Job', 'add-new-job', '', 'add-new-job', 'index.php?option=com_jobsfactory&view=job&layout=form&task=form', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 8, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 147, 148, 0, '*', 0),
(405, 'jobsmainmenu', 'Applications', 'my-applications', '', 'my-applications', 'index.php?option=com_jobsfactory&view=candidates&layout=myapplications&task=myapplications', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 7, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 149, 150, 0, '*', 0),
(406, 'jobsmainmenu', 'Watchlist', 'my-watchlist', '', 'my-watchlist', 'index.php?option=com_jobsfactory&controller=watchlist&task=watchlist', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1}', 151, 152, 0, '', 0),
(407, 'jobsmainmenu', 'Profile', 'jobsfactory-profile', '', 'jobsfactory-profile', 'index.php?option=com_jobsfactory&task=userdetails&controller=user', 'component', 1, 1, 1, 804, 631, '2017-03-07 10:02:48', 0, 2, ' ', 0, '{\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1}', 153, 154, 0, '', 0),
(419, 'events-factory', 'Conversation', 'conversation-index', '', 'conversation-index', 'index.php?option=com_eventsfactory&view=conversation.index', 'component', 1, 1, 1, 819, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{}', 167, 168, 0, '*', 0),
(420, 'events-factory', 'Events calendar', 'event-calendar', '', 'event-calendar', 'index.php?option=com_eventsfactory&view=event.calendar', 'component', 1, 1, 1, 819, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{}', 165, 166, 0, '*', 0),
(421, 'events-factory', 'Event', 'event-details', '', 'event-details', 'index.php?option=com_eventsfactory&view=event.details', 'component', 1, 1, 1, 819, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{}', 163, 164, 0, '*', 0),
(422, 'events-factory', 'Events list', 'event-index', '', 'event-index', 'index.php?option=com_eventsfactory&view=event.index', 'component', 1, 1, 1, 819, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{}', 161, 162, 0, '*', 0),
(423, 'events-factory', 'Events map', 'event-map', '', 'event-map', 'index.php?option=com_eventsfactory&view=event.map', 'component', 1, 1, 1, 819, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{}', 159, 160, 0, '*', 0),
(424, 'events-factory', 'Organize event', 'event-organize', '', 'event-organize', 'index.php?option=com_eventsfactory&view=event.organize', 'component', 1, 1, 1, 819, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{}', 157, 158, 0, '*', 0),
(425, 'events-factory', 'Profile', 'profile-show', '', 'profile-show', 'index.php?option=com_eventsfactory&view=profile.show', 'component', 1, 1, 1, 819, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{}', 155, 156, 0, '*', 0),
(441, 'main-menu', 'Job-uri', 'jobs', '', 'jobs', 'index.php?option=com_jobsfactory&view=jobs&layout=listjobs&task=listjobs', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 1, ' ', 67, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 13, 14, 0, '*', 0),
(442, 'main-menu', 'Categorii', 'job-categories', '', 'job-categories', 'index.php?option=com_jobsfactory&view=jobs&layout=watchlist&task=watchlist&controller=watchlist', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 1, ' ', 68, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 19, 20, 0, '*', 0),
(443, 'main-menu', 'Companii', 'companies', '', 'companies', 'index.php?option=com_jobsfactory&view=companies&layout=listcompanies&task=listcompanies', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 15, 16, 0, '*', 0),
(444, 'main-menu', 'Candidati', 'candidates', '', 'candidates', 'index.php?option=com_jobsfactory&view=candidates&layout=listcandidates&task=listcandidates', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 8, ' ', 66, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 17, 18, 0, '*', 0),
(445, 'main-menu', 'Job-urile mele', 'my-job-list', '', 'my-job-list', 'index.php?option=com_jobsfactory&view=jobs&layout=myjobs&task=myjobs', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 8, ' ', 66, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 21, 22, 0, '*', 0),
(446, 'main-menu', 'Adauga Job', 'add-job', '', 'add-job', 'index.php?option=com_jobsfactory&view=job&layout=form&task=form', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 8, ' ', 66, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 23, 24, 0, '*', 0),
(447, 'main-menu', 'Watchlist', 'watchlist', '', 'watchlist', 'index.php?option=com_jobsfactory&view=jobs&layout=watchlist&task=watchlist&controller=watchlist', 'component', -2, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 1, ' ', 66, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 25, 26, 0, '*', 0),
(448, 'main-menu', 'Profil', 'profile', '', 'profile', 'index.php?option=com_jobsfactory&view=user&task=userdetails&controller=user&usergrouptype=0', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 2, ' ', 66, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 27, 28, 0, '*', 0),
(449, 'main-menu', 'Ads', 'advertisements', '', 'advertisements', 'index.php?option=com_adsfactory&view=adsfactory&layout=listads&task=listads', 'component', -2, 1, 1, 842, 0, '0000-00-00 00:00:00', 0, 1, ' ', 82, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 29, 30, 0, '*', 0),
(450, 'main-menu', 'New Ad', 'new-ad', '', 'new-ad', 'index.php?option=com_adsfactory&view=ad&layout=form&task=form', 'component', -2, 1, 1, 842, 0, '0000-00-00 00:00:00', 0, 8, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 31, 32, 0, '*', 0),
(451, 'main-menu', 'My Ads', 'my-advertisements', '', 'my-advertisements', 'index.php?option=com_adsfactory&view=adsfactory&layout=myads&task=myads', 'component', -2, 1, 1, 842, 0, '0000-00-00 00:00:00', 0, 8, ' ', 66, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 33, 34, 0, '*', 0),
(452, 'main-menu', 'New Course', 'new-course', '', 'new-course', 'index.php?option=com_eventsfactory&view=event.organize', 'component', -2, 1, 1, 819, 0, '0000-00-00 00:00:00', 0, 8, ' ', 66, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 35, 36, 0, '*', 0),
(453, 'main-menu', 'Available Courses', 'available-courses', '', 'available-courses', 'index.php?option=com_content&view=category&layout=blog&id=2', 'component', -2, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 7, ' ', 66, '{\"layout_type\":\"blog\",\"show_category_heading_title_text\":\"\",\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"show_cat_tags\":\"\",\"page_subheading\":\"\",\"num_leading_articles\":\"\",\"num_intro_articles\":\"\",\"num_columns\":\"\",\"num_links\":\"\",\"multi_column_order\":\"\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"\",\"order_date\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"show_featured\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":0,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 37, 38, 0, '*', 0),
(454, 'main-menu', 'Events Profile', 'events-profile', '', 'events-profile', 'index.php?option=com_eventsfactory&view=profile.show', 'component', -2, 1, 1, 819, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":0,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 39, 40, 0, '*', 0),
(455, 'main-menu', 'Acasa', 'home', '', 'home', 'index.php?option=com_jobsfactory&view=maps&task=googlemaps&controller=maps', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 1, ' ', 83, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 11, 12, 1, '*', 0),
(495, 'vet2', 'Category', 'category', '', 'category', 'index.php?option=com_jobsfactory&view=category&layout=tree&task=tree', 'component', 1, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 169, 170, 0, '*', 0),
(496, 'vet2', 'Sector', 'sector', '', 'sector', '', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu-anchor_rel\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1}', 171, 172, 0, '*', 0),
(497, 'main-menu', 'Ads Profileee', 'ads-profileee', '', 'ads-profileee', 'index.php?option=com_adsfactory&view=user&task=userdetails&controller=user', 'component', -2, 1, 1, 842, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":0,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 41, 42, 0, '*', 0),
(506, 'main', 'COM_GANTRY5', 'com-gantry5', '', 'com-gantry5', 'index.php?option=com_gantry5', 'component', 0, 1, 1, 703, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 173, 178, 0, '', 1),
(507, 'main', 'COM_GANTRY5_ADMIN_MENU_THEMES', 'com-gantry5-admin-menu-themes', '', 'com-gantry5/com-gantry5-admin-menu-themes', 'index.php?option=com_gantry5&view=themes', 'component', 0, 506, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 174, 175, 0, '', 1),
(508, 'main', 'COM_GANTRY5_ADMIN_MENU_THEME', 'com-gantry5-admin-menu-theme', '', 'com-gantry5/com-gantry5-admin-menu-theme', 'index.php?option=com_gantry5', 'component', 0, 506, 2, 703, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 176, 177, 0, '', 1),
(533, 'main', 'COM_ADVERTISEMENTFACTORY_COMPONENT_TITLE', 'com-advertisementfactory-component-title', '', 'com-advertisementfactory-component-title', 'index.php?option=com_advertisementfactory', 'component', 0, 1, 1, 856, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 179, 194, 0, '', 1),
(534, 'main', 'COM_ADVERTISEMENTFACTORY_SUBMENU_DASHBOARD', 'com-advertisementfactory-submenu-dashboard', '', 'com-advertisementfactory-component-title/com-advertisementfactory-submenu-dashboard', 'index.php?option=com_advertisementfactory&view=dashboard', 'component', 0, 533, 2, 856, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 180, 181, 0, '', 1),
(535, 'main', 'COM_ADVERTISEMENTFACTORY_SUBMENU_ADS', 'com-advertisementfactory-submenu-ads', '', 'com-advertisementfactory-component-title/com-advertisementfactory-submenu-ads', 'index.php?option=com_advertisementfactory&view=ads', 'component', 0, 533, 2, 856, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 182, 183, 0, '', 1),
(536, 'main', 'COM_ADVERTISEMENTFACTORY_SUBMENU_PRICES', 'com-advertisementfactory-submenu-prices', '', 'com-advertisementfactory-component-title/com-advertisementfactory-submenu-prices', 'index.php?option=com_advertisementfactory&view=prices', 'component', 0, 533, 2, 856, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 184, 185, 0, '', 1),
(537, 'main', 'COM_ADVERTISEMENTFACTORY_SUBMENU_PAYMENTS', 'com-advertisementfactory-submenu-payments', '', 'com-advertisementfactory-component-title/com-advertisementfactory-submenu-payments', 'index.php?option=com_advertisementfactory&view=payments', 'component', 0, 533, 2, 856, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 186, 187, 0, '', 1),
(538, 'main', 'COM_ADVERTISEMENTFACTORY_SUBMENU_NOTIFICATIONS', 'com-advertisementfactory-submenu-notifications', '', 'com-advertisementfactory-component-title/com-advertisementfactory-submenu-notifications', 'index.php?option=com_advertisementfactory&view=notifications', 'component', 0, 533, 2, 856, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 188, 189, 0, '', 1),
(539, 'main', 'COM_ADVERTISEMENTFACTORY_SUBMENU_SETTINGS', 'com-advertisementfactory-submenu-settings', '', 'com-advertisementfactory-component-title/com-advertisementfactory-submenu-settings', 'index.php?option=com_advertisementfactory&view=settings', 'component', 0, 533, 2, 856, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 190, 191, 0, '', 1),
(540, 'main', 'COM_ADVERTISEMENTFACTORY_SUBMENU_ABOUT', 'com-advertisementfactory-submenu-about', '', 'com-advertisementfactory-component-title/com-advertisementfactory-submenu-about', 'index.php?option=com_advertisementfactory&view=about', 'component', 0, 533, 2, 856, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 192, 193, 0, '', 1),
(541, 'advertisement-factory', 'Advertisement Zones', 'advertisement-zones', '', 'advertisement-zones', 'index.php?option=com_advertisementfactory&view=previsualize&modules=1', 'component', 1, 1, 1, 856, 0, '0000-00-00 00:00:00', 0, 2, '', 0, '', 209, 210, 0, '*', 0),
(542, 'advertisement-factory', 'Available Advertisements', 'available-advertisements', '', 'available-advertisements', 'index.php?option=com_advertisementfactory&view=purchasetypes', 'component', 1, 1, 1, 856, 0, '0000-00-00 00:00:00', 0, 2, '', 0, '', 207, 208, 0, '*', 0),
(543, 'advertisement-factory', 'My Ads', 'my-ads', '', 'my-ads', 'index.php?option=com_advertisementfactory&view=ads', 'component', 1, 1, 1, 856, 0, '0000-00-00 00:00:00', 0, 2, '', 0, '', 205, 206, 0, '*', 0),
(544, 'main-menu', 'Jobs2', 'jobs2', '', 'jobs2', 'index.php?option=com_jobsfactory&view=jobs&layout=listjobs&task=listjobs', 'component', -2, 1, 1, 804, 0, '0000-00-00 00:00:00', 0, 1, ' ', 80, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 195, 196, 0, '*', 0),
(545, 'main', 'COM_IMPROVED_AJAX_LOGIN', 'com-improved-ajax-login', '', 'com-improved-ajax-login', 'index.php?option=com_improved_ajax_login', 'component', 0, 1, 1, 873, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_improved_ajax_login/assets/images/s_icon.png', 0, '{}', 197, 204, 0, '', 1),
(546, 'main', 'COM_IMPROVED_AJAX_LOGIN_TITLE_MODULES', 'com-improved-ajax-login-title-modules', '', 'com-improved-ajax-login/com-improved-ajax-login-title-modules', 'index.php?option=com_improved_ajax_login&view=modules', 'component', 0, 545, 2, 873, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_improved_ajax_login/assets/images/s_modules.png', 0, '{}', 198, 199, 0, '', 1),
(547, 'main', 'COM_IMPROVED_AJAX_LOGIN_TITLE_FORMS', 'com-improved-ajax-login-title-forms', '', 'com-improved-ajax-login/com-improved-ajax-login-title-forms', 'index.php?option=com_improved_ajax_login&view=forms', 'component', 0, 545, 2, 873, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_improved_ajax_login/assets/images/s_forms.png', 0, '{}', 200, 201, 0, '', 1),
(548, 'main', 'COM_IMPROVED_AJAX_LOGIN_TITLE_OAUTHS', 'com-improved-ajax-login-title-oauths', '', 'com-improved-ajax-login/com-improved-ajax-login-title-oauths', 'index.php?option=com_improved_ajax_login&view=oauths', 'component', 0, 545, 2, 873, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_improved_ajax_login/assets/images/s_oauths.png', 0, '{}', 202, 203, 0, '', 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_menu_types`
--

CREATE TABLE `sorin_menu_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(11) NOT NULL,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_menu_types`
--

INSERT INTO `sorin_menu_types` (`id`, `asset_id`, `menutype`, `title`, `description`) VALUES
(15, 0, 'callisto-theme', 'Callisto Theme', 'Sample menu for Callisto theme.'),
(16, 80, 'jobsmainmenu', 'Jobs MainMenu', ''),
(17, 90, 'main-menu', 'Vet Menu', ''),
(18, 96, 'events-factory', 'Events Factory', ''),
(21, 121, 'vet2', 'Vet2', ''),
(22, 125, 'advertisement-factory', 'Advertisement Factory Menu', 'Advertisement Factory Menu');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_messages`
--

CREATE TABLE `sorin_messages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id_from` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id_to` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_messages`
--

INSERT INTO `sorin_messages` (`message_id`, `user_id_from`, `user_id_to`, `folder_id`, `date_time`, `state`, `priority`, `subject`, `message`) VALUES
(2, 0, 0, 0, '2017-03-16 08:58:30', 0, 0, 'Error sending email', 'An error was encountered when sending the user registration email. The error is: Could not instantiate mail function. The user who attempted to register is: suzie'),
(3, 0, 0, 0, '2017-03-16 09:10:48', 0, 0, 'Error sending email', 'An error was encountered when sending the user registration email. The error is: Could not instantiate mail function. The user who attempted to register is: Company2');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_messages_cfg`
--

CREATE TABLE `sorin_messages_cfg` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_modules`
--

CREATE TABLE `sorin_modules` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_modules`
--

INSERT INTO `sorin_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(2, 40, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 41, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{\"count\":\"5\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}', 1, '*'),
(4, 42, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{\"count\":\"5\",\"ordering\":\"c_dsc\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}', 1, '*'),
(8, 43, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 44, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 45, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{\"count\":\"5\",\"name\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}', 1, '*'),
(12, 46, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{\"layout\":\"\",\"moduleclass_sfx\":\"\",\"shownew\":\"1\",\"showhelp\":\"1\",\"cache\":\"0\"}', 1, '*'),
(13, 47, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 48, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 49, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(79, 52, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(86, 53, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{\"format\":\"short\",\"product\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(88, 57, 'FP RokSprocket Features - Slideshow', '', '', 1, 'showcase', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_roksprocket', 1, 0, '{\"run_content_plugins\":\"onmodule\",\"provider\":\"simple\",\"layout\":\"features\",\"simple_sort\":\"manual\",\"display_limit\":\"\\u221e\",\"features_themes\":\"slideshow\",\"features_show_title\":\"1\",\"features_show_article_text\":\"1\",\"features_previews_length\":\"\\u221e\",\"features_strip_html_tags\":\"0\",\"features_show_arrows\":\"show\",\"features_show_pagination\":\"1\",\"features_animation\":\"crossfade\",\"features_autoplay\":\"0\",\"features_autoplay_delay\":\"5\",\"features_resize_enable\":\"0\",\"features_resize_width\":\"100\",\"features_resize_height\":\"50\",\"features_title_default\":\"title\",\"features_description_default\":\"primary\",\"features_image_default\":\"primary\",\"features_image_default_custom\":\"\",\"features_link_default\":\"primary\",\"features_link_default_custom\":\"\",\"cache\":\"0\",\"moduleclass_sfx\":\"fp-roksprocket-showcase-image\",\"module_cache\":\"1\",\"cache_time\":\"900\"}', 0, '*'),
(90, 59, 'Search Our Site', '', '', 1, 'sidefeature-a', 0, '0000-00-00 00:00:00', '2017-03-21 11:52:40', '0000-00-00 00:00:00', 1, 'mod_rokajaxsearch', 1, 1, '{\"search_page\":\"index.php?option=com_search&view=search&tmpl=component\",\"adv_search_page\":\"index.php?option=com_search&view=search\",\"include_css\":\"1\",\"theme\":\"light\",\"searchphrase\":\"any\",\"ordering\":\"newest\",\"limit\":\"10\",\"perpage\":\"3\",\"websearch\":\"0\",\"blogsearch\":\"0\",\"imagesearch\":\"0\",\"videosearch\":\"0\",\"websearch_api\":\"\",\"show_pagination\":\"1\",\"safesearch\":\"MODERATE\",\"image_size\":\"MEDIUM\",\"show_estimated\":\"1\",\"hide_divs\":\"\",\"include_link\":\"1\",\"show_description\":\"1\",\"include_category\":\"1\",\"show_readmore\":\"1\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(91, 60, 'FP RokSprocket Tabs', '', '', 1, 'sidefeature-b', 631, '2017-03-15 08:48:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_roksprocket', 1, 0, '{\"run_content_plugins\":\"onmodule\",\"provider\":\"simple\",\"layout\":\"tabs\",\"simple_sort\":\"manual\",\"tabs_themes\":\"default\",\"display_limit\":\"∞\",\"tabs_position\":\"top\",\"tabs_animation\":\"slideandfade\",\"tabs_autoplay\":\"0\",\"tabs_autoplay_delay\":\"5\",\"tabs_resize_enable\":\"0\",\"tabs_resize_width\":\"0\",\"tabs_resize_height\":\"0\",\"tabs_previews_length\":\"0\",\"tabs_strip_html_tags\":\"0\",\"tabs_title_default\":\"title\",\"tabs_icon_default\":\"primary\",\"tabs_icon_default_custom\":\"\",\"tabs_link_default\":\"primary\",\"tabs_link_default_custom\":\"\",\"tabs_description_default\":\"primary\",\"cache\":\"0\",\"moduleclass_sfx\":\"\",\"module_cache\":\"1\",\"cache_time\":\"900\"}', 0, '*'),
(92, 61, 'FP RokSprocket Headlines', '', '', 1, 'sidebar-a', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_roksprocket', 1, 0, '{\"run_content_plugins\":\"onmodule\",\"provider\":\"simple\",\"layout\":\"headlines\",\"simple_sort\":\"manual\",\"headlines_themes\":\"default\",\"display_limit\":\"\\u221e\",\"headlines_label_text\":\"Newsflash:\",\"headlines_previews_length\":\"\\u221e\",\"headlines_strip_html_tags\":\"0\",\"headlines_show_arrows\":\"show\",\"headlines_animation\":\"slideandfade\",\"headlines_autoplay\":\"0\",\"headlines_autoplay_delay\":\"5\",\"headlines_resize_enable\":\"0\",\"headlines_resize_width\":\"0\",\"headlines_resize_height\":\"0\",\"headlines_description_default\":\"primary\",\"headlines_image_default\":\"primary\",\"headlines_image_default_custom\":\"\",\"headlines_link_default\":\"primary\",\"headlines_link_default_custom\":\"\",\"cache\":\"0\",\"moduleclass_sfx\":\"\",\"module_cache\":\"1\",\"cache_time\":\"900\"}', 0, '*'),
(93, 62, 'FP RokSprocket Strips', '', '', 1, 'extension-b', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_roksprocket', 1, 0, '{\"run_content_plugins\":\"onmodule\",\"provider\":\"simple\",\"layout\":\"strips\",\"simple_sort\":\"manual\",\"strips_themes\":\"default\",\"display_limit\":\"\\u221e\",\"strips_previews_length\":\"\\u221e\",\"strips_strip_html_tags\":\"0\",\"strips_items_per_page\":\"4\",\"strips_items_per_row\":\"4\",\"strips_show_arrows\":\"show\",\"strips_show_pagination\":\"1\",\"strips_animation\":\"fadeDelay\",\"strips_autoplay\":\"0\",\"strips_autoplay_delay\":\"5\",\"strips_resize_enable\":\"0\",\"strips_resize_width\":\"0\",\"strips_resize_height\":\"0\",\"strips_title_default\":\"title\",\"strips_description_default\":\"primary\",\"strips_image_default\":\"primary\",\"strips_image_default_custom\":\"\",\"strips_link_default\":\"primary\",\"strips_link_default_custom\":\"\",\"cache\":\"0\",\"moduleclass_sfx\":\"\",\"module_cache\":\"1\",\"cache_time\":\"900\"}', 0, '*'),
(94, 63, 'FP RokSprocket Lists', '', '', 1, 'extension-c', 631, '2017-03-16 14:33:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_roksprocket', 1, 0, '{\"run_content_plugins\":\"onmodule\",\"provider\":\"simple\",\"layout\":\"lists\",\"simple_sort\":\"manual\",\"lists_themes\":\"default\",\"display_limit\":\"∞\",\"lists_enable_accordion\":\"1\",\"lists_previews_length\":\"∞\",\"lists_strip_html_tags\":\"0\",\"lists_items_per_page\":\"2\",\"lists_show_arrows\":\"show\",\"lists_show_pagination\":\"1\",\"lists_autoplay\":\"0\",\"lists_autoplay_delay\":\"5\",\"lists_resize_enable\":\"0\",\"lists_resize_width\":\"0\",\"lists_resize_height\":\"0\",\"lists_title_default\":\"title\",\"lists_description_default\":\"primary\",\"lists_image_default\":\"primary\",\"lists_image_default_custom\":\"\",\"lists_link_default\":\"primary\",\"lists_link_default_custom\":\"\",\"cache\":\"0\",\"moduleclass_sfx\":\"\",\"module_cache\":\"1\",\"cache_time\":\"900\"}', 0, '*'),
(95, 64, 'FP RokSprocket Features - Slideshow (copy)', '', '', 1, 'showcase', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_roksprocket', 1, 0, '{\"run_content_plugins\":\"onmodule\",\"provider\":\"simple\",\"layout\":\"features\",\"simple_sort\":\"manual\",\"display_limit\":\"∞\",\"features_themes\":\"slideshow\",\"features_show_title\":\"1\",\"features_show_article_text\":\"1\",\"features_previews_length\":\"∞\",\"features_strip_html_tags\":\"0\",\"features_show_arrows\":\"show\",\"features_show_pagination\":\"1\",\"features_animation\":\"crossfade\",\"features_autoplay\":\"0\",\"features_autoplay_delay\":\"5\",\"features_resize_enable\":\"0\",\"features_resize_width\":\"0\",\"features_resize_height\":\"0\",\"features_title_default\":\"title\",\"features_description_default\":\"primary\",\"features_image_default\":\"primary\",\"features_image_default_custom\":\"\",\"features_link_default\":\"primary\",\"features_link_default_custom\":\"\",\"cache\":\"0\",\"moduleclass_sfx\":\"\",\"module_cache\":\"1\",\"cache_time\":\"900\"}', 0, '*'),
(96, 74, 'Main Menu', '', '', 0, 'sidebar-a', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"callisto-theme\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(97, 75, 'Login', '', '', 1, 'sidebar-c', 0, '0000-00-00 00:00:00', '2017-03-23 13:18:27', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '{\"pretext\":\"\",\"posttext\":\"\",\"login\":\"\",\"logout\":\"\",\"greeting\":\"1\",\"name\":\"0\",\"usesecure\":\"0\",\"usetext\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(98, 76, 'Who\'s Online', '', '', 0, 'sidebar-c', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_whosonline', 1, 1, '{\"showmode\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"filter_groups\":\"0\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(99, 78, 'Gantry 5 Particle', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_gantry5_particle', 1, 1, '', 0, '*'),
(100, 81, 'Jobs MainMenu', '', '', 3, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"jobsmainmenu\"}', 0, ''),
(101, 82, 'Joburi Promovate', '', '', 1, 'showcase', 0, '0000-00-00 00:00:00', '2017-03-08 10:02:22', '0000-00-00 00:00:00', 1, 'mod_jobsfactory_featured', 1, 1, '{\"force_itemid\":\"\",\"nr_jobs_displayed\":\"5\",\"moduleclass_sfx\":\"\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(102, 83, 'jobsFactory Filters module', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_jobsfactory_filters', 1, 1, '', 0, '*'),
(103, 84, 'Ultimele Joburi', '', '', 4, 'showcase', 0, '0000-00-00 00:00:00', '2017-03-08 10:06:41', '0000-00-00 00:00:00', 1, 'mod_jobsfactory_latest', 1, 1, '{\"force_itemid\":\"\",\"nr_jobs_displayed\":\"3\",\"moduleclass_sfx\":\"\",\"module_tag\":\"div\",\"bootstrap_size\":\"1\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"System-rounded\"}', 0, '*'),
(104, 85, 'jobsFactory Popular Jobs', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_jobsfactory_popular', 1, 1, '', 0, '*'),
(105, 86, 'jobsFactory Random Jobs', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_jobsfactory_random', 1, 1, '', 0, '*'),
(106, 87, 'Cauta job-uri', '', '', 1, 'sidefeature-a', 0, '0000-00-00 00:00:00', '2017-03-09 13:03:41', '0000-00-00 00:00:00', 1, 'mod_jobsfactory_search', 1, 0, '{\"force_itemid\":\"\",\"moduleclass_sfx\":\"\",\"layout\":\"_:horizontal\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h2\",\"header_class\":\"button button-3\",\"style\":\"System-rounded\"}', 0, '*'),
(107, 88, 'jobsFactory Tag Cloud', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_jobsfactorycloud', 1, 1, '', 0, '*'),
(108, 89, 'jobsFactory Category Tree Module', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_jobsfactorytree', 1, 1, '', 0, '*'),
(112, 97, 'Events Factory', '', '', 2, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"events-factory\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"0\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', 0, '*'),
(131, 123, 'Search:', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_search', 1, 1, '{\"label\":\"Search site:\",\"width\":\"50\",\"text\":\"50\",\"button\":\"1\",\"button_pos\":\"left\",\"imagebutton\":\"0\",\"button_text\":\"Search\",\"opensearch\":\"1\",\"opensearch_title\":\"\",\"set_itemid\":\"449\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(132, 126, 'Advertisement Factory Menu', '', '', 0, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 2, 1, '{\"menutype\":\"advertisement-factory\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"0\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', 0, 'en-GB'),
(133, 127, 'mod_advertisementfactory_banners', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_advertisementfactory_banners', 1, 1, '', 0, '*'),
(134, 128, 'mod_advertisementfactory_links', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_advertisementfactory_links', 1, 1, '', 0, '*'),
(135, 129, 'mod_advertisementfactory_thumbnails', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_advertisementfactory_thumbnails', 1, 1, '', 0, '*'),
(136, 130, 'ultimelejoburi2', '', '', 1, 'showcase', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_jobsfactory_latest', 1, 1, '{\"force_itemid\":\"\",\"nr_jobs_displayed\":\"3\",\"moduleclass_sfx\":\"\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(137, 131, 'Improved AJAX Login and Register', '', '', 0, '', 631, '2017-03-23 13:18:45', '2017-03-23 13:18:09', '0000-00-00 00:00:00', 0, 'mod_improved_ajax_login', 1, 1, '', 0, '*');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_modules_menu`
--

CREATE TABLE `sorin_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_modules_menu`
--

INSERT INTO `sorin_modules_menu` (`moduleid`, `menuid`) VALUES
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(79, 0),
(86, 0),
(88, 455),
(90, 0),
(91, 101),
(91, 355),
(92, 442),
(93, 442),
(94, 101),
(94, 355),
(95, 101),
(95, 103),
(96, 370),
(97, 0),
(98, 370),
(100, 0),
(101, 0),
(103, 0),
(106, 0),
(112, 0),
(113, 0),
(122, 0),
(131, 0),
(132, 0),
(136, 0);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_newsfeeds`
--

CREATE TABLE `sorin_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cache_time` int(10) UNSIGNED NOT NULL DEFAULT '3600',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_offlajn_forms`
--

CREATE TABLE `sorin_offlajn_forms` (
  `id` int(11) UNSIGNED NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `theme` varchar(255) NOT NULL,
  `props` text NOT NULL,
  `fields` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_offlajn_forms`
--

INSERT INTO `sorin_offlajn_forms` (`id`, `ordering`, `state`, `checked_out`, `checked_out_time`, `created_by`, `title`, `type`, `theme`, `props`, `fields`) VALUES
(1, 0, 1, 0, '0000-00-00 00:00:00', 0, 'Registration Form', 'registration', 'elegant', '{\"layout\":{\"jform[layout_columns]\":\"2\",\"jform[layout_margin]\":\"10px\",\"jform[layout_width]\":\"190px\"}}', '{\"page\":[{\"elem\":[{\"jform[elem_type]\":{\"value\":\"header\",\"predefined\":\"title\",\"readonly\":true,\"button\":\"Title\",\"icon\":\"icon-quote icon-font\"},\"jform[elem_wide]\":{\"checked\":true},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"MOD_LOGIN_REGISTER\",\"placeholder\":\"Create an account\"},\"jform[elem_subtitle]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_REQUIRED\",\"placeholder\":\"* Required field\"}},{\"jform[elem_type]\":{\"value\":\"textfield\",\"defaultValue\":\"text\",\"predefined\":\"name\",\"readonly\":true,\"button\":\"Name\",\"icon\":\"icon-user\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":false},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_id]\":{\"value\":\"ial-name\"},\"jform[elem_name]\":{\"value\":\"name\",\"prefix\":\"jform[\",\"readonly\":true},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_NAME_LABEL\",\"placeholder\":\"Name:\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_NAME_DESC\",\"placeholder\":\"Enter your full name\"},\"jform[elem_pattern]\":{\"value\":\".+\",\"placeholder\":\".+\"},\"jform[elem_error]\":{\"value\":\"\",\"defaultValue\":\"JLIB_FORM_VALIDATE_FIELD_INVALID\",\"placeholder\":\"Invalid field: \"}},{\"jform[elem_type]\":{\"value\":\"textfield\",\"defaultValue\":\"text\",\"predefined\":\"username\",\"readonly\":true,\"button\":\"Username\",\"icon\":\"icon-user\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_id]\":{\"value\":\"ial-username\"},\"jform[elem_name]\":{\"value\":\"username\",\"prefix\":\"jform[\",\"readonly\":true},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_USERNAME_LABEL\",\"placeholder\":\"Username:\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_USERNAME_DESC\",\"placeholder\":\"Enter your desired username\"},\"jform[elem_error]\":{\"value\":\"\",\"defaultValue\":\"JLIB_DATABASE_ERROR_VALID_AZ09\",\"placeholder\":\"Please enter a valid username. No spaces, at least 2 characters and must not contain the following characters:  \\\\ \\\" \' % ; ( ) &\"},\"jform[elem_pattern]\":{\"value\":\"^[^\\\\\\\\&%\'\\\";\\\\(\\\\)]{2,}$\",\"placeholder\":\"^[^\\\\\\\\&%\'\\\";\\\\(\\\\)]{2,}$\"},\"jform[elem_ajax]\":\"username\"},{\"jform[elem_type]\":{\"value\":\"password1\",\"defaultValue\":\"password\",\"predefined\":\"password1\",\"readonly\":true,\"button\":\"Password\",\"icon\":\"icon-lock\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_id]\":{\"value\":\"ial-password1\"},\"jform[elem_name]\":{\"value\":\"password1\",\"prefix\":\"jform[\",\"readonly\":true},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_PASSWORD1_LABEL\",\"placeholder\":\"Password:\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_DESIRED_PASSWORD\",\"placeholder\":\"Enter your desired password - Enter a minimum of 4 characters\"},\"jform[elem_error]\":{\"value\":\"\",\"defaultValue\":\"JLIB_FORM_VALIDATE_FIELD_INVALID\",\"placeholder\":\"Invalid field: \"}},{\"jform[elem_type]\":{\"value\":\"password2\",\"defaultValue\":\"password\",\"predefined\":\"password2\",\"readonly\":true,\"button\":\"Password again\",\"icon\":\"icon-lock\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_id]\":{\"value\":\"ial-password2\"},\"jform[elem_name]\":{\"value\":\"password2\",\"prefix\":\"jform[\",\"readonly\":true},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_PASSWORD2_LABEL\",\"placeholder\":\"Confirm Password:\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_PASSWORD2_DESC\",\"placeholder\":\"Confirm your password\"},\"jform[elem_error]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_PASSWORD1_MESSAGE\",\"placeholder\":\"The passwords you entered do not match. Please enter your desired password in the password field and confirm your entry by entering it in the confirm password field.\"}},{\"jform[elem_type]\":{\"value\":\"textfield\",\"defaultValue\":\"text\",\"predefined\":\"email\",\"readonly\":true,\"button\":\"Email\",\"icon\":\"icon-mail-2 icon-envelope\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_id]\":{\"value\":\"ial-email1\"},\"jform[elem_name]\":{\"value\":\"email1\",\"prefix\":\"jform[\",\"readonly\":true},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_EMAIL1_LABEL\",\"placeholder\":\"Email Address:\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_EMAIL1_DESC\",\"placeholder\":\"Enter your email address\"},\"jform[elem_error]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_INVALID_EMAIL\",\"placeholder\":\"Invalid email address\"},\"jform[elem_pattern]\":{\"value\":\"^([\\\\w0-9\\\\.\\\\-])+\\\\@(([a-zA-Z0-9\\\\-])+\\\\.)+[a-zA-Z]{2,4}$\",\"placeholder\":\"^([\\\\w0-9\\\\.\\\\-])+\\\\@(([a-zA-Z0-9\\\\-])+\\\\.)+[a-zA-Z]{2,4}$\"},\"jform[elem_ajax]\":\"email\"},{\"jform[elem_type]\":{\"value\":\"textfield\",\"defaultValue\":\"text\",\"predefined\":\"email2\",\"readonly\":true,\"button\":\"Email again\",\"icon\":\"icon-mail-2 icon-envelope\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_id]\":{\"value\":\"ial-email2\"},\"jform[elem_name]\":{\"value\":\"email2\",\"prefix\":\"jform[\",\"readonly\":true},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_EMAIL2_LABEL\",\"placeholder\":\"Confirm email Address:\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_EMAIL2_DESC\",\"placeholder\":\"Confirm your email address\"},\"jform[elem_error]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_EMAIL2_MESSAGE\",\"placeholder\":\"The email addresses you entered do not match. Please enter your email address in the email address field and confirm your entry by entering it in the confirm email field.\"}},{\"jform[elem_type]\":{\"profile\":1,\"value\":\"tos\",\"predefined\":\"tos\",\"readonly\":true,\"button\":\"Terms of services\",\"icon\":\"icon-checkbox icon-ok-circle\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":true},\"jform[elem_checked]\":{\"checked\":false,\"disabled\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_id]\":{\"value\":\"ial-tos\"},\"jform[elem_name]\":{\"value\":\"tos\",\"prefix\":\"jform[improved][\",\"readonly\":true},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"PLG_USER_PROFILE_OPTION_AGREE\",\"placeholder\":\"Agree\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"PLG_USER_PROFILE_FIELD_TOS_DESC\",\"placeholder\":\"Agree to terms of service\"},\"jform[elem_article]\":{\"value\":\"\"},\"jform[elem_article_name]\":{\"value\":\"\",\"placeholder\":\"Select article for TOS\"}},{\"jform[elem_type]\":{\"value\":\"button\",\"predefined\":\"submit\",\"readonly\":true,\"button\":\"Submit\",\"icon\":\"icon-arrow-right\"},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"JREGISTER\",\"placeholder\":\"Register\"},\"jform[elem_subtitle]\":{\"value\":\"\",\"placeholder\":\"\"}}]}]}'),
(2, 0, 1, 0, '0000-00-00 00:00:00', 0, 'Registration Form', 'registration/hikashop', 'elegant', '{\"layout\":{\"jform[layout_columns]\":\"2\",\"jform[layout_margin]\":\"10px\",\"jform[layout_width]\":\"200px\"}}', '{\"page\":[{\"elem\":[{\"jform[elem_type]\":{\"value\":\"header\",\"predefined\":\"title\",\"readonly\":true,\"button\":\"Title\",\"icon\":\"icon-quote icon-font\"},\"jform[elem_wide]\":{\"checked\":true,\"value\":true},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"MOD_LOGIN_REGISTER\",\"placeholder\":\"Create an account\"},\"jform[elem_subtitle]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_REQUIRED\",\"placeholder\":\"* Required field\"}},{\"jform[elem_type]\":{\"value\":\"textfield\",\"defaultValue\":\"text\",\"predefined\":\"username\",\"readonly\":true,\"button\":\"Username\",\"icon\":\"icon-user\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":true,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-username\"},\"jform[elem_name]\":{\"value\":\"username\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[register]\"},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_USERNAME_LABEL\",\"placeholder\":\"Username:\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_USERNAME_DESC\",\"placeholder\":\"Enter your desired username\"},\"jform[elem_error]\":{\"value\":\"\",\"defaultValue\":\"JLIB_DATABASE_ERROR_VALID_AZ09\",\"placeholder\":\"Please enter a valid username. No space at beginning or end, at least 2 characters and must not contain the following characters:  \\\\ \\\" \' % ; ( ) &\"},\"jform[elem_pattern]\":{\"value\":\"^[^\\\\\\\\&%\'\\\";\\\\(\\\\)]{2,}$\",\"placeholder\":\"^[^\\\\\\\\&%\'\\\";\\\\(\\\\)]{2,}$\"},\"jform[elem_ajax]\":\"username\"},{\"jform[elem_type]\":{\"value\":\"textfield\",\"defaultValue\":\"text\",\"predefined\":\"email\",\"readonly\":true,\"button\":\"Email\",\"icon\":\"icon-mail-2 icon-envelope\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":true,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-email1\"},\"jform[elem_name]\":{\"value\":\"email\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[register]\"},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_EMAIL1_LABEL\",\"placeholder\":\"Email Address:\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_EMAIL1_DESC\",\"placeholder\":\"Enter your email address\"},\"jform[elem_error]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_INVALID_EMAIL\",\"placeholder\":\"Invalid email address\"},\"jform[elem_pattern]\":{\"value\":\"^([\\\\w0-9\\\\.\\\\-])+\\\\@(([a-zA-Z0-9\\\\-])+\\\\.)+[a-zA-Z]{2,4}$\",\"placeholder\":\"^([\\\\w0-9\\\\.\\\\-])+\\\\@(([a-zA-Z0-9\\\\-])+\\\\.)+[a-zA-Z]{2,4}$\"},\"jform[elem_ajax]\":\"email\"},{\"jform[elem_type]\":{\"value\":\"password1\",\"defaultValue\":\"password\",\"predefined\":\"password1\",\"readonly\":true,\"button\":\"Password\",\"icon\":\"icon-lock\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":true,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-password1\"},\"jform[elem_name]\":{\"value\":\"password1\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[register]\"},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_PASSWORD1_LABEL\",\"placeholder\":\"Password:\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_DESIRED_PASSWORD\",\"placeholder\":\"Enter your desired password.\"},\"jform[elem_error]\":{\"value\":\"\",\"defaultValue\":\"JLIB_FORM_FIELD_INVALID\",\"placeholder\":\"Invalid field: \"}},{\"jform[elem_type]\":{\"value\":\"password2\",\"defaultValue\":\"password\",\"predefined\":\"password2\",\"readonly\":true,\"button\":\"Password again\",\"icon\":\"icon-lock\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":true,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-password2\"},\"jform[elem_name]\":{\"value\":\"password2\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[register]\"},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_PASSWORD2_LABEL\",\"placeholder\":\"Confirm Password:\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_PASSWORD2_DESC\",\"placeholder\":\"Confirm your password\"},\"jform[elem_error]\":{\"value\":\"\",\"defaultValue\":\"COM_USERS_REGISTER_PASSWORD1_MESSAGE\",\"placeholder\":\"The passwords you entered do not match. Please enter your desired password in the password field and confirm your entry by entering it in the confirm password field.\"}},{\"jform[elem_type]\":{\"value\":\"header\",\"readonly\":true,\"button\":\"Header\",\"icon\":\"icon-quote icon-font\"},\"jform[elem_wide]\":{\"checked\":true,\"value\":true},\"jform[elem_label]\":{\"value\":\"Address information\",\"placeholder\":\"Header text\"},\"jform[elem_subtitle]\":{\"value\":\"\",\"placeholder\":\"\"}},{\"jform[elem_type]\":{\"profile\":1,\"value\":\"textfield\",\"defaultValue\":\"text\",\"readonly\":true,\"button\":\"Telephone\",\"icon\":\"icon-pencil\",\"predefined\":\"address_telephone\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":false,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-phone\"},\"jform[elem_name]\":{\"value\":\"address_telephone\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[address]\"},\"jform[elem_label]\":{\"value\":\"Telephone\",\"placeholder\":\"Textfield:\"},\"jform[elem_value]\":{\"value\":\"\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_error]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_pattern]\":{\"value\":\".+\",\"placeholder\":\".+\"}},{\"jform[elem_type]\":{\"profile\":1,\"value\":\"select\",\"readonly\":true,\"button\":\"Title\",\"icon\":\"icon-chevron-down\",\"predefined\":\"address_title\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":false,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-title\"},\"jform[elem_name]\":{\"value\":\"address_title\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[address]\"},\"jform[elem_label]\":{\"value\":\"Title\",\"placeholder\":\"Select:\"},\"jform[elem_select]\":{\"value\":\"[option selected=\\\"selected\\\" value=\\\"Mr\\\"]Mr[/option][option value=\\\"Mrs\\\"]Mrs[/option][option value=\\\"Miss\\\"]Miss[/option][option value=\\\"Ms\\\"]Ms[/option][option value=\\\"Dr\\\"]Dr[/option]\"},\"jform[elem_title]\":{\"value\":\"\",\"placeholder\":\"\"}},{\"jform[elem_type]\":{\"profile\":1,\"value\":\"textfield\",\"defaultValue\":\"text\",\"readonly\":true,\"button\":\"Firstname\",\"icon\":\"icon-pencil\",\"predefined\":\"address_firstname\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":false,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-name\"},\"jform[elem_name]\":{\"value\":\"address_firstname\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[address]\"},\"jform[elem_label]\":{\"value\":\"Firstname\",\"placeholder\":\"Textfield:\"},\"jform[elem_value]\":{\"value\":\"\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_error]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_pattern]\":{\"value\":\".+\",\"placeholder\":\".+\"}},{\"jform[elem_type]\":{\"profile\":1,\"value\":\"textfield\",\"defaultValue\":\"text\",\"readonly\":true,\"button\":\"Lastname\",\"icon\":\"icon-pencil\",\"predefined\":\"address_lastname\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":false,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-name\"},\"jform[elem_name]\":{\"value\":\"address_lastname\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[address]\"},\"jform[elem_label]\":{\"value\":\"Lastname\",\"placeholder\":\"Textfield:\"},\"jform[elem_value]\":{\"value\":\"\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_error]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_pattern]\":{\"value\":\".+\",\"placeholder\":\".+\"}},{\"jform[elem_type]\":{\"profile\":1,\"value\":\"textfield\",\"defaultValue\":\"text\",\"readonly\":true,\"button\":\"Street\",\"icon\":\"icon-pencil\",\"predefined\":\"address_street\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":false,\"value\":true},\"jform[elem_wide]\":{\"checked\":true,\"value\":true},\"jform[elem_class]\":{\"value\":\"ial-address1\"},\"jform[elem_name]\":{\"value\":\"address_street\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[address]\"},\"jform[elem_label]\":{\"value\":\"Street\",\"placeholder\":\"Textfield:\"},\"jform[elem_value]\":{\"value\":\"\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_error]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_pattern]\":{\"value\":\".+\",\"placeholder\":\".+\"}},{\"jform[elem_type]\":{\"profile\":1,\"value\":\"textfield\",\"defaultValue\":\"text\",\"readonly\":true,\"button\":\"City\",\"icon\":\"icon-pencil\",\"predefined\":\"address_city\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":false,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-city\"},\"jform[elem_name]\":{\"value\":\"address_city\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[address]\"},\"jform[elem_label]\":{\"value\":\"City\",\"placeholder\":\"Textfield:\"},\"jform[elem_value]\":{\"value\":\"\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_error]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_pattern]\":{\"value\":\".+\",\"placeholder\":\".+\"}},{\"jform[elem_type]\":{\"profile\":1,\"value\":\"textfield\",\"defaultValue\":\"text\",\"readonly\":true,\"button\":\"Post code\",\"icon\":\"icon-pencil\",\"predefined\":\"address_post_code\"},\"jform[elem_required]\":{\"checked\":false,\"disabled\":false},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-postal_code\"},\"jform[elem_name]\":{\"value\":\"address_post_code\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[address]\"},\"jform[elem_label]\":{\"value\":\"Post code\",\"placeholder\":\"Textfield:\"},\"jform[elem_value]\":{\"value\":\"\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_error]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_pattern]\":{\"value\":\".+\",\"placeholder\":\".+\"}},{\"jform[elem_type]\":{\"profile\":1,\"value\":\"select\",\"readonly\":true,\"button\":\"Country\",\"icon\":\"icon-chevron-down\",\"predefined\":\"address_country\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":false,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-country\"},\"jform[elem_name]\":{\"value\":\"address_country\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[address]\"},\"jform[elem_label]\":{\"value\":\"Country\",\"placeholder\":\"Select:\"},\"jform[elem_select]\":{\"value\":\"[option value=\\\"country_Afghanistan_1\\\"]Afghanistan (افغانستان)[/option][option value=\\\"country_Albania_2\\\"]Albania (Shqipëria)[/option][option value=\\\"country_Algeria_3\\\"]Algeria (الجزائر)[/option][option value=\\\"country_American_Samoa_4\\\"]American Samoa[/option][option value=\\\"country_Andorra_5\\\"]Andorra[/option][option value=\\\"country_Angola_6\\\"]Angola[/option][option value=\\\"country_Anguilla_7\\\"]Anguilla[/option][option value=\\\"country_Antarctica_8\\\"]Antarctica[/option][option value=\\\"country_Antigua_and_Barbuda_9\\\"]Antigua and Barbuda[/option][option value=\\\"country_Argentina_10\\\"]Argentina[/option][option value=\\\"country_Armenia_11\\\"]Armenia (Հայաստան)[/option][option value=\\\"country_Aruba_12\\\"]Aruba[/option][option value=\\\"country_Australia_13\\\"]Australia[/option][option value=\\\"country_Austria_14\\\"]Austria (Österreich)[/option][option value=\\\"country_Azerbaijan_15\\\"]Azerbaijan (Azərbaycan)[/option][option value=\\\"country_Bahamas_16\\\"]Bahamas[/option][option value=\\\"country_Bahrain_17\\\"]Bahrain (البحرين)[/option][option value=\\\"country_Bangladesh_18\\\"]Bangladesh (বাংলাদেশ\')[/option][option value=\\\"country_Barbados_19\\\"]Barbados[/option][option value=\\\"country_Belarus_20\\\"]Belarus (Беларусь)[/option][option value=\\\"country_Belgium_21\\\"]Belgium (België • Belgique • Belgien)[/option][option value=\\\"country_Belize_22\\\"]Belize[/option][option value=\\\"country_Benin_23\\\"]Benin (Bénin)[/option][option value=\\\"country_Bermuda_24\\\"]Bermuda[/option][option value=\\\"country_Bhutan_25\\\"]Bhutan (འབྲུག་ཡུལ་)[/option][option value=\\\"country_Bolivia_26\\\"]Bolivia (Wuliwya • Volívia • Buliwya)[/option][option value=\\\"country_Bosnia_and_Herzegowina_27\\\"]Bosnia and Herzegowina (Bosna i Hercegovina)[/option][option value=\\\"country_Botswana_28\\\"]Botswana[/option][option value=\\\"country_Bouvet_Island_29\\\"]Bouvet Island[/option][option value=\\\"country_Brazil_30\\\"]Brazil[/option][option value=\\\"country_British_Indian_Ocean_Territory_31\\\"]British Indian Ocean Territory[/option][option value=\\\"country_Brunei_Darussalam_32\\\"]Brunei Darussalam[/option][option value=\\\"country_Bulgaria_33\\\"]Bulgaria (България)[/option][option value=\\\"country_Burkina_Faso_34\\\"]Burkina Faso[/option][option value=\\\"country_Burundi_35\\\"]Burundi (Uburundi)[/option][option value=\\\"country_Cambodia_36\\\"]Cambodia (កម្ពុជា)[/option][option value=\\\"country_Cameroon_37\\\"]Cameroon (Cameroun)[/option][option value=\\\"country_Canada_38\\\"]Canada[/option][option value=\\\"country_Cape_Verde_39\\\"]Cape Verde (Cabo Verde)[/option][option value=\\\"country_Cayman_Islands_40\\\"]Cayman Islands[/option][option value=\\\"country_Central_African_Republic_41\\\"]Central African Republic (Centrafrique • Bêafrîka)[/option][option value=\\\"country_Chad_42\\\"]Chad (Tchad • تشاد)[/option][option value=\\\"country_Chile_43\\\"]Chile[/option][option value=\\\"country_China_44\\\"]China (中國 • 中国)[/option][option value=\\\"country_Christmas_Island_45\\\"]Christmas Island[/option][option value=\\\"country_Cocos__Keeling__Islands_46\\\"]Cocos (Keeling) Islands[/option][option value=\\\"country_Colombia_47\\\"]Colombia[/option][option value=\\\"country_Comoros_48\\\"]Comoros (Komori • Comores • جزر القمر)[/option][option value=\\\"country_Congo_49\\\"]Congo[/option][option value=\\\"country_Cook_Islands_50\\\"]Cook Islands[/option][option value=\\\"country_Costa_Rica_51\\\"]Costa Rica[/option][option value=\\\"country_Cote_D_Ivoire_52\\\"]Cote D\'Ivoire[/option][option value=\\\"country_Croatia_53\\\"]Croatia (Hrvatska)[/option][option value=\\\"country_Cuba_54\\\"]Cuba[/option][option value=\\\"country_Cyprus_55\\\"]Cyprus (Κύπρος • Kıbrıs)[/option][option value=\\\"country_Czech_Republic_56\\\"]Czech Republic (Česko)[/option][option value=\\\"country_Denmark_57\\\"]Denmark (Danmark)[/option][option value=\\\"country_Djibouti_58\\\"]Djibouti (جيبوتي)[/option][option value=\\\"country_Dominica_59\\\"]Dominica[/option][option value=\\\"country_Dominican_Republic_60\\\"]Dominican Republic (República Dominicana)[/option][option value=\\\"country_East_Timor_61\\\"]East Timor (Timór-Leste)[/option][option value=\\\"country_Ecuador_62\\\"]Ecuador[/option][option value=\\\"country_Egypt_63\\\"]Egypt (مصر)[/option][option value=\\\"country_El_Salvador_64\\\"]El Salvador[/option][option value=\\\"country_Equatorial_Guinea_65\\\"]Equatorial Guinea (Guinée équatoriale)[/option][option value=\\\"country_Eritrea_66\\\"]Eritrea (ኤርትራ • إرتريا)[/option][option value=\\\"country_Estonia_67\\\"]Estonia (Eesti)[/option][option value=\\\"country_Ethiopia_68\\\"]Ethiopia (ኢትዮጵያ)[/option][option value=\\\"country_Falkland_Islands__Malvinas__69\\\"]Falkland Islands (Malvinas)[/option][option value=\\\"country_Faroe_Islands_70\\\"]Faroe Islands[/option][option value=\\\"country_Fiji_71\\\"]Fiji (Viti • फ़िजी)[/option][option value=\\\"country_Finland_72\\\"]Finland (Suomi)[/option][option value=\\\"country_France_73\\\"]France[/option][option value=\\\"country_French_Guiana_75\\\"]French Guiana[/option][option value=\\\"country_French_Polynesia_76\\\"]French Polynesia[/option][option value=\\\"country_French_Southern_Territories_77\\\"]French Southern Territories[/option][option value=\\\"country_Gabon_78\\\"]Gabon[/option][option value=\\\"country_Gambia_79\\\"]Gambia[/option][option value=\\\"country_Georgia_80\\\"]Georgia (საქართველო)[/option][option value=\\\"country_Germany_81\\\"]Germany (Deutschland)[/option][option value=\\\"country_Ghana_82\\\"]Ghana[/option][option value=\\\"country_Gibraltar_83\\\"]Gibraltar[/option][option value=\\\"country_Greece_84\\\"]Greece (Ελλάδα)[/option][option value=\\\"country_Greenland_85\\\"]Greenland[/option][option value=\\\"country_Grenada_86\\\"]Grenada[/option][option value=\\\"country_Guam_88\\\"]Guam[/option][option value=\\\"country_Guatemala_89\\\"]Guatemala[/option][option value=\\\"country_Guinea_90\\\"]Guinea (Guinée)[/option][option value=\\\"country_Guinea_Bissau_91\\\"]Guinea-Bissau (Guiné-Bissau)[/option][option value=\\\"country_Guyana_92\\\"]Guyana[/option][option value=\\\"country_Haiti_93\\\"]Haiti (Haïti • Ayiti)[/option][option value=\\\"country_Heard_and_McDonald_Islands_94\\\"]Heard and McDonald Islands[/option][option value=\\\"country_Honduras_95\\\"]Honduras[/option][option value=\\\"country_Hong_Kong_96\\\"]Hong Kong (香港)[/option][option selected=\\\"selected\\\" value=\\\"country_Hungary_97\\\"]Hungary (Magyarország)[/option][option value=\\\"country_Iceland_98\\\"]Iceland (Ísland)[/option][option value=\\\"country_India_99\\\"]India (भारत)[/option][option value=\\\"country_Indonesia_100\\\"]Indonesia[/option][option value=\\\"country_Iran_101\\\"]Iran (ايران)[/option][option value=\\\"country_Iraq_102\\\"]Iraq (عێراق • العراق)[/option][option value=\\\"country_Ireland_103\\\"]Ireland (Éire)[/option][option value=\\\"country_Israel_104\\\"]Israel (إسرائيل • ישראל)[/option][option value=\\\"country_Italy_105\\\"]Italy (Italia)[/option][option value=\\\"country_Jamaica_106\\\"]Jamaica[/option][option value=\\\"country_Japan_107\\\"]Japan (日本)[/option][option value=\\\"country_Jordan_108\\\"]Jordan (الأردنّ)[/option][option value=\\\"country_Kazakhstan_109\\\"]Kazakhstan (Қазақстан)[/option][option value=\\\"country_Kenya_110\\\"]Kenya[/option][option value=\\\"country_Kiribati_111\\\"]Kiribati[/option][option value=\\\"country_Korea__North_112\\\"]Korea, North (북조선)[/option][option value=\\\"country_Korea__South_113\\\"]Korea, South (한국)[/option][option value=\\\"country_Kuwait_114\\\"]Kuwait (الكويت)[/option][option value=\\\"country_Kyrgyzstan_115\\\"]Kyrgyzstan (Кыргызстан)[/option][option value=\\\"country_Laos_116\\\"]Laos (ເມືອງລາວ)[/option][option value=\\\"country_Latvia_117\\\"]Latvia (Latvija)[/option][option value=\\\"country_Lebanon_118\\\"]Lebanon (لبنان)[/option][option value=\\\"country_Lesotho_119\\\"]Lesotho[/option][option value=\\\"country_Liberia_120\\\"]Liberia[/option][option value=\\\"country_Libyan_Arab_Jamahiriya_121\\\"]Libyan Arab Jamahiriya[/option][option value=\\\"country_Liechtenstein_122\\\"]Liechtenstein[/option][option value=\\\"country_Lithuania_123\\\"]Lithuania (Lietuva)[/option][option value=\\\"country_Luxembourg_124\\\"]Luxembourg (Luxemburg • Lëtzebuerg)[/option][option value=\\\"country_Macau_125\\\"]Macau (澳门 • 澳門)[/option][option value=\\\"country_Macedonia_126\\\"]Macedonia (Македонија)[/option][option value=\\\"country_Madagascar_127\\\"]Madagascar (Madagasikara)[/option][option value=\\\"country_Malawi_128\\\"]Malawi (Malaŵi)[/option][option value=\\\"country_Malaysia_129\\\"]Malaysia[/option][option value=\\\"country_Maldives_130\\\"]Maldives (ދިވެހިރާއްޖެ)[/option][option value=\\\"country_Mali_131\\\"]Mali[/option][option value=\\\"country_Malta_132\\\"]Malta[/option][option value=\\\"country_Marshall_Islands_133\\\"]Marshall Islands (Aelōn̄ in M̧ajeļ)[/option][option value=\\\"country_Mauritania_135\\\"]Mauritania (موريتانيا • Mauritanie)[/option][option value=\\\"country_Mauritius_136\\\"]Mauritius (Maurice)[/option][option value=\\\"country_Mayotte_137\\\"]Mayotte[/option][option value=\\\"country_Mexico_138\\\"]Mexico (México • Mēxihco)[/option][option value=\\\"country_Micronesia_139\\\"]Micronesia[/option][option value=\\\"country_Moldova_140\\\"]Moldova[/option][option value=\\\"country_Monaco_141\\\"]Monaco[/option][option value=\\\"country_Mongolia_142\\\"]Mongolia (Монгол улс)[/option][option value=\\\"country_Montenegro_19668\\\"]Montenegro (Црна Гора)[/option][option value=\\\"country_Montserrat_143\\\"]Montserrat[/option][option value=\\\"country_Morocco_144\\\"]Morocco (المغرب)[/option][option value=\\\"country_Mozambique_145\\\"]Mozambique (Moçambique)[/option][option value=\\\"country_Myanmar_146\\\"]Myanmar[/option][option value=\\\"country_Namibia_147\\\"]Namibia[/option][option value=\\\"country_Nauru_148\\\"]Nauru[/option][option value=\\\"country_Nepal_149\\\"]Nepal (नेपाल)[/option][option value=\\\"country_Netherlands_150\\\"]Netherlands (Nederland)[/option][option value=\\\"country_Netherlands_Antilles_151\\\"]Netherlands Antilles[/option][option value=\\\"country_New_Caledonia_152\\\"]New Caledonia (Nouvelle-Calédonie)[/option][option value=\\\"country_New_Zealand_153\\\"]New Zealand (Aotearoa)[/option][option value=\\\"country_Nicaragua_154\\\"]Nicaragua[/option][option value=\\\"country_Niger_155\\\"]Niger[/option][option value=\\\"country_Nigeria_156\\\"]Nigeria[/option][option value=\\\"country_Niue_157\\\"]Niue[/option][option value=\\\"country_Norfolk_Island_158\\\"]Norfolk Island[/option][option value=\\\"country_Northern_Mariana_Islands_159\\\"]Northern Mariana Islands[/option][option value=\\\"country_Norway_160\\\"]Norway (Norge / Noreg)[/option][option value=\\\"country_Oman_161\\\"]Oman (عمان)[/option][option value=\\\"country_Pakistan_162\\\"]Pakistan (پاکستان)[/option][option value=\\\"country_Palau_163\\\"]Palau (Belau)[/option][option value=\\\"country_Panama_164\\\"]Panama (Panamá)[/option][option value=\\\"country_Papua_New_Guinea_165\\\"]Papua New Guinea (Papua Niugini)[/option][option value=\\\"country_Paraguay_166\\\"]Paraguay (Paraguái)[/option][option value=\\\"country_Peru_167\\\"]Peru (Perú)[/option][option value=\\\"country_Philippines_168\\\"]Philippines (Pilipinas)[/option][option value=\\\"country_Pitcairn_169\\\"]Pitcairn[/option][option value=\\\"country_Poland_170\\\"]Poland (Polska)[/option][option value=\\\"country_Portugal_171\\\"]Portugal[/option][option value=\\\"country_Puerto_Rico_172\\\"]Puerto Rico[/option][option value=\\\"country_Qatar_173\\\"]Qatar (دولة قطر)[/option][option value=\\\"country_Romania_175\\\"]Romania (România)[/option][option value=\\\"country_Russia_176\\\"]Russia (Россия)[/option][option value=\\\"country_Rwanda_177\\\"]Rwanda[/option][option value=\\\"country_Saint_Kitts_and_Nevis_178\\\"]Saint Kitts and Nevis[/option][option value=\\\"country_Saint_Lucia_179\\\"]Saint Lucia[/option][option value=\\\"country_Saint_Vincent_and_the_Grenadines_180\\\"]Saint Vincent and the Grenadines[/option][option value=\\\"country_Samoa_181\\\"]Samoa (Sāmoa)[/option][option value=\\\"country_San_Marino_182\\\"]San Marino[/option][option value=\\\"country_Sao_Tome_and_Principe_183\\\"]Sao Tome and Principe (São Tomé e Príncipe)[/option][option value=\\\"country_Saudi_Arabia_184\\\"]Saudi Arabia (العربية السعودية)[/option][option value=\\\"country_Senegal_185\\\"]Senegal (Sénégal)[/option][option value=\\\"country_Serbia_4503\\\"]Serbia (Србија)[/option][option value=\\\"country_Seychelles_186\\\"]Seychelles (Sesel)[/option][option value=\\\"country_Sierra_Leone_187\\\"]Sierra Leone[/option][option value=\\\"country_Singapore_188\\\"]Singapore (新加坡 • Singapura • சிங்கப்பூர்)[/option][option value=\\\"country_Slovakia_189\\\"]Slovakia (Slovensko)[/option][option value=\\\"country_Slovenia_190\\\"]Slovenia (Slovenija)[/option][option value=\\\"country_Solomon_Islands_191\\\"]Solomon Islands[/option][option value=\\\"country_Somalia_192\\\"]Somalia (Soomaaliya • الصومال)[/option][option value=\\\"country_South_Africa_193\\\"]South Africa (Suid-Afrika)[/option][option value=\\\"country_Spain_195\\\"]Spain (España)[/option][option value=\\\"country_Sri_Lanka_196\\\"]Sri Lanka (ශ්‍රී ලංකාව • இலங்கை)[/option][option value=\\\"country_St__Helena_197\\\"]St. Helena[/option][option value=\\\"country_St__Pierre_and_Miquelon_198\\\"]St. Pierre and Miquelon[/option][option value=\\\"country_Sudan_199\\\"]Sudan (السودان)[/option][option value=\\\"country_Suriname_200\\\"]Suriname[/option][option value=\\\"country_Svalbard_and_Jan_Mayen_Islands_201\\\"]Svalbard and Jan Mayen Islands[/option][option value=\\\"country_Swaziland_202\\\"]Swaziland (eSwatini)[/option][option value=\\\"country_Sweden_203\\\"]Sweden (Sverige)[/option][option value=\\\"country_Switzerland_204\\\"]Switzerland (Schweiz • Suisse • Svizzera • Svizra)[/option][option value=\\\"country_Syrian_Arab_Republic_205\\\"]Syrian Arab Republic (سورية‎)[/option][option value=\\\"country_Taiwan_206\\\"]Taiwan (臺灣 • 台灣)[/option][option value=\\\"country_Tajikistan_207\\\"]Tajikistan (Тоҷикистон)[/option][option value=\\\"country_Tanzania_208\\\"]Tanzania[/option][option value=\\\"country_Thailand_209\\\"]Thailand (ประเทศไทย)[/option][option value=\\\"country_Togo_210\\\"]Togo[/option][option value=\\\"country_Tokelau_211\\\"]Tokelau[/option][option value=\\\"country_Tonga_212\\\"]Tonga[/option][option value=\\\"country_Trinidad_and_Tobago_213\\\"]Trinidad and Tobago[/option][option value=\\\"country_Tunisia_214\\\"]Tunisia (تونس‎)[/option][option value=\\\"country_Turkey_215\\\"]Turkey (Türkiye)[/option][option value=\\\"country_Turkmenistan_216\\\"]Turkmenistan (Türkmenistan)[/option][option value=\\\"country_Turks_and_Caicos_Islands_217\\\"]Turks and Caicos Islands[/option][option value=\\\"country_Tuvalu_218\\\"]Tuvalu[/option][option value=\\\"country_Uganda_219\\\"]Uganda[/option][option value=\\\"country_Ukraine_220\\\"]Ukraine (Україна)[/option][option value=\\\"country_United_Arab_Emirates_221\\\"]United Arab Emirates (الإمارات العربية المتحدة)[/option][option value=\\\"country_United_Kingdom_222\\\"]United Kingdom[/option][option value=\\\"country_United_States_Minor_Outlying_Islands_224\\\"]United States Minor Outlying Islands[/option][option value=\\\"country_United_States_of_America_223\\\"]United States of America[/option][option value=\\\"country_Uruguay_225\\\"]Uruguay[/option][option value=\\\"country_Uzbekistan_226\\\"]Uzbekistan (Oʻzbekiston)[/option][option value=\\\"country_Vanuatu_227\\\"]Vanuatu[/option][option value=\\\"country_Vatican_City_State__Holy_See__228\\\"]Vatican City State (Vaticanum)[/option][option value=\\\"country_Venezuela_229\\\"]Venezuela[/option][option value=\\\"country_Vietnam_230\\\"]Vietnam (Việt Nam)[/option][option value=\\\"country_Virgin_Islands__British__231\\\"]Virgin Islands (British)[/option][option value=\\\"country_Virgin_Islands__U_S___232\\\"]Virgin Islands (U.S.)[/option][option value=\\\"country_Wallis_and_Futuna_Islands_233\\\"]Wallis and Futuna Islands[/option][option value=\\\"country_Western_Sahara_234\\\"]Western Sahara (الصحراء الغربية)[/option][option value=\\\"country_Yemen_235\\\"]Yemen (اليمن)[/option][option value=\\\"country_Zaire_237\\\"]Zaire[/option][option value=\\\"country_Zambia_238\\\"]Zambia[/option][option value=\\\"country_Zimbabwe_239\\\"]Zimbabwe[/option]\"},\"jform[elem_title]\":{\"value\":\"\",\"placeholder\":\"\"}},{\"jform[elem_type]\":{\"profile\":1,\"value\":\"textfield\",\"defaultValue\":\"text\",\"readonly\":true,\"button\":\"State\",\"icon\":\"icon-pencil\",\"predefined\":\"address_state\"},\"jform[elem_required]\":{\"checked\":true,\"disabled\":false,\"value\":true},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_class]\":{\"value\":\"ial-region\"},\"jform[elem_name]\":{\"value\":\"address_state\",\"readonly\":true},\"jform[elem_prefix]\":{\"value\":\"data[address]\"},\"jform[elem_label]\":{\"value\":\"State\",\"placeholder\":\"Textfield:\"},\"jform[elem_value]\":{\"value\":\"\"},\"jform[elem_placeholder]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_title]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_error]\":{\"value\":\"\",\"placeholder\":\"\"},\"jform[elem_pattern]\":{\"value\":\".+\",\"placeholder\":\".+\"}},{\"jform[elem_type]\":{\"value\":\"button\",\"predefined\":\"submit\",\"readonly\":true,\"button\":\"Submit\",\"icon\":\"icon-arrow-right\"},\"jform[elem_wide]\":{\"checked\":false},\"jform[elem_label]\":{\"value\":\"\",\"defaultValue\":\"JREGISTER\",\"placeholder\":\"Register\"},\"jform[elem_subtitle]\":{\"value\":\"&nbsp\",\"placeholder\":\"\"}}]}]}');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_offlajn_oauths`
--

CREATE TABLE `sorin_offlajn_oauths` (
  `id` int(11) NOT NULL,
  `name` varchar(127) NOT NULL,
  `alias` varchar(127) NOT NULL,
  `app_id` varchar(127) DEFAULT NULL,
  `app_secret` varchar(127) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `create_app` varchar(255) NOT NULL,
  `auth` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `userinfo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_offlajn_oauths`
--

INSERT INTO `sorin_offlajn_oauths` (`id`, `name`, `alias`, `app_id`, `app_secret`, `published`, `create_app`, `auth`, `token`, `userinfo`) VALUES
(1, 'Facebook', 'facebook', NULL, NULL, 0, 'https://developers.facebook.com/apps', 'https://www.facebook.com/dialog/oauth?scope=email&response_type=code&display=popup', 'https://graph.facebook.com/oauth/access_token', 'https://graph.facebook.com/me?access_token='),
(2, 'Google', 'google', NULL, NULL, 0, 'https://code.google.com/apis/console/', 'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&response_type=code&display=popup', 'https://accounts.google.com/o/oauth2/token', 'https://www.googleapis.com/oauth2/v2/userinfo?access_token='),
(3, 'Twitter', 'twitter', NULL, NULL, 0, 'https://dev.twitter.com/apps', '', '', ''),
(4, 'Microsoft', 'windows', NULL, NULL, 0, 'https://manage.dev.live.com/Applications', 'https://login.live.com/oauth20_authorize.srf?scope=wl.emails&response_type=code&display=popup', 'https://login.live.com/oauth20_token.srf', 'https://apis.live.net/v5.0/me?access_token='),
(5, 'LinkedIn', 'linkedin', NULL, NULL, 0, 'https://www.linkedin.com/secure/developer', 'https://www.linkedin.com/uas/oauth2/authorization?response_type=code&scope=r_basicprofile%20r_emailaddress&state=rdt136sdh5f', 'https://www.linkedin.com/uas/oauth2/accessToken', 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)?format=json&oauth2_access_token=');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_offlajn_users`
--

CREATE TABLE `sorin_offlajn_users` (
  `user_id` int(11) NOT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `windows_id` varchar(255) DEFAULT NULL,
  `twitter_id` varchar(255) DEFAULT NULL,
  `linkedin_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_overrider`
--

CREATE TABLE `sorin_overrider` (
  `id` int(10) NOT NULL COMMENT 'Primary Key',
  `constant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_postinstall_messages`
--

CREATE TABLE `sorin_postinstall_messages` (
  `postinstall_message_id` bigint(20) UNSIGNED NOT NULL,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language_extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_postinstall_messages`
--

INSERT INTO `sorin_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 1),
(3, 700, 'COM_CPANEL_MSG_STATS_COLLECTION_TITLE', 'COM_CPANEL_MSG_STATS_COLLECTION_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/statscollection.php', 'admin_postinstall_statscollection_condition', '3.5.0', 1),
(4, 700, 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_BODY', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_ACTION', 'plg_system_updatenotification', 1, 'action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_condition', '3.6.3', 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_redirect_links`
--

CREATE TABLE `sorin_redirect_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_url` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_url` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_rokcommon_configs`
--

CREATE TABLE `sorin_rokcommon_configs` (
  `id` int(11) NOT NULL,
  `extension` varchar(45) NOT NULL DEFAULT '',
  `type` varchar(45) NOT NULL,
  `file` varchar(256) NOT NULL,
  `priority` int(10) NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `sorin_rokcommon_configs`
--

INSERT INTO `sorin_rokcommon_configs` (`id`, `extension`, `type`, `file`, `priority`) VALUES
(7, 'roksprocket', 'container', '/components/com_roksprocket/container.xml', 10),
(8, 'roksprocket', 'library', '/components/com_roksprocket/lib', 10);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_roksprocket_items`
--

CREATE TABLE `sorin_roksprocket_items` (
  `id` int(11) NOT NULL,
  `module_id` varchar(45) NOT NULL,
  `provider` varchar(45) NOT NULL,
  `provider_id` varchar(45) NOT NULL,
  `order` int(10) UNSIGNED NOT NULL,
  `params` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_roksprocket_items`
--

INSERT INTO `sorin_roksprocket_items` (`id`, `module_id`, `provider`, `provider_id`, `order`, `params`) VALUES
(101, '95', 'simple', '3', 0, '{\"_article_title\":\"Item 01\",\"features_item_title\":\"The Gantry5 revolution has finally arrived!\",\"features_item_description\":\"<p>Callisto is the first RocketTheme Club template to be released on the new Gantry5 Framework, marking the dawn of a new era of powerful, highly customizable and mobile friendly web building.<\\/p>\\r\\n<a class=\\\"button button-3\\\" href=\\\"#\\\">Read More<\\/a>\",\"features_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'templates\\/rt_callisto\\/common\\/images\\/demo\\/home\\/showcase\\/roksprocket-showcase\\/img-01.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_3__params_features_item_image\'}\",\"features_item_link\":\"-none-\",\"lists_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"grids_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"grids_item_image\":\"-default-\",\"tabs_item_link\":\"-default-\",\"tables_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"tabs_item_description\":\"-default-\",\"quotes_item_author\":\"-none-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"mosaic_item_link\":\"-default-\",\"lists_item_link\":\"-default-\",\"sliders_item_title\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"tables_item_price\":\"-none-\",\"quotes_item_subtext\":\"-none-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"lists_item_description\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"strips_item_description\":\"-default-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"headlines_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(102, '95', 'simple', '2', 1, '{\"_article_title\":\"Item 02\",\"features_item_title\":\"Built in Menu Support with Custom Editor\",\"features_item_description\":\"<p>Gantry5 benefits from its own built-in menu system, negating the need for auxiliary extensions, as well as featuring a drag and drop setup for dropdown configuration.<\\/p>\\r\\n                    <a class=\\\"button button-3\\\" href=\\\"#\\\">Read More<\\/a>\",\"features_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'templates\\/rt_callisto\\/common\\/images\\/demo\\/home\\/showcase\\/roksprocket-showcase\\/img-02.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_2__params_features_item_image\'}\",\"features_item_link\":\"-none-\",\"lists_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"grids_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"grids_item_image\":\"-default-\",\"tabs_item_link\":\"-default-\",\"tables_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"tabs_item_description\":\"-default-\",\"quotes_item_author\":\"-none-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"mosaic_item_link\":\"-default-\",\"lists_item_link\":\"-default-\",\"sliders_item_title\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"tables_item_price\":\"-none-\",\"quotes_item_subtext\":\"-none-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"lists_item_description\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"strips_item_description\":\"-default-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"headlines_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(103, '95', 'simple', '1', 2, '{\"_article_title\":\"Item 03\",\"features_item_title\":\"Choose and Configure the Six Preset Styles\",\"features_item_description\":\"<p>Callisto is distributed with six preset styles, with light and dark variations. Each can be customized in the Style Settings to individualize the template with ease and speed.<\\/p>\\r\\n                    <a class=\\\"button button-3\\\" href=\\\"#\\\">Read More<\\/a>\",\"features_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'templates\\/rt_callisto\\/common\\/images\\/demo\\/home\\/showcase\\/roksprocket-showcase\\/img-03.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_1__params_features_item_image\'}\",\"features_item_link\":\"-default-\",\"lists_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"grids_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"grids_item_image\":\"-default-\",\"tabs_item_link\":\"-default-\",\"tables_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"tabs_item_description\":\"-default-\",\"quotes_item_author\":\"-none-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"mosaic_item_link\":\"-default-\",\"lists_item_link\":\"-default-\",\"sliders_item_title\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"tables_item_price\":\"-none-\",\"quotes_item_subtext\":\"-none-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"lists_item_description\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"strips_item_description\":\"-default-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"headlines_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(206, '94', 'simple', '4', 0, '{\"_article_title\":\"Item 01\",\"lists_item_title\":\"YAML-driven<span class=\\\"hidden-tablet\\\"> Configuration<\\/span>\",\"lists_item_image\":\"-default-\",\"lists_item_link\":\"#\",\"lists_item_description\":\"<div><span>YAML is a human-readable data serialization<span class=\\\"visible-large\\\">, making it easy to create and configure blueprints for back-end configuration options<\\/span>.<\\/span><\\/div>\",\"mosaic_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"grids_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"grids_item_image\":\"-default-\",\"tabs_item_link\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"tabs_item_description\":\"-default-\",\"quotes_item_author\":\"-none-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"mosaic_item_link\":\"-default-\",\"sliders_item_title\":\"-default-\",\"features_item_title\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"tables_item_price\":\"-none-\",\"quotes_item_subtext\":\"-none-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"features_item_description\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"strips_item_description\":\"-default-\",\"sliders_item_image\":\"-default-\",\"features_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"headlines_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"features_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(207, '94', 'simple', '3', 1, '{\"_article_title\":\"Item 02\",\"lists_item_title\":\"Twig Templating\",\"lists_item_image\":\"-default-\",\"lists_item_link\":\"#\",\"lists_item_description\":\"<div><span>Gantry 5 utilizes Twig - a <span class=\\\"hidden-tablet\\\">flexible, fast, and secure <\\/span>template engine for PHP<span class=\\\"visible-large\\\"> - to make creating powerful, dynamic themes quick and easy<\\/span>.<\\/span><\\/div>\",\"mosaic_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"grids_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"grids_item_image\":\"-default-\",\"tabs_item_link\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"tabs_item_description\":\"-default-\",\"quotes_item_author\":\"-none-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"mosaic_item_link\":\"-default-\",\"sliders_item_title\":\"-default-\",\"features_item_title\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"tables_item_price\":\"-none-\",\"quotes_item_subtext\":\"-none-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"features_item_description\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"strips_item_description\":\"-default-\",\"sliders_item_image\":\"-default-\",\"features_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"headlines_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"features_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(208, '94', 'simple', '2', 2, '{\"_article_title\":\"Item 03\",\"lists_item_title\":\"Particles System\",\"lists_item_image\":\"-default-\",\"lists_item_link\":\"#\",\"lists_item_description\":\"<div class=\\\"hidden-tablet\\\"><span>Gantry 5\'s Particles system makes it easy to <span class=\\\"visible-large\\\">create, configure, and <\\/span>manage content blocks<span class=\\\"visible-large\\\"> on multiple levels<\\/span>.<\\/span><\\/div>\\t\\t\\t\\t\\t<div class=\\\"visible-tablet\\\">Gantry 5\'s Particle system for managing content blocks.<\\/div>\",\"mosaic_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"grids_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"grids_item_image\":\"-default-\",\"tabs_item_link\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"tabs_item_description\":\"-default-\",\"quotes_item_author\":\"-none-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"mosaic_item_link\":\"-default-\",\"sliders_item_title\":\"-default-\",\"features_item_title\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"tables_item_price\":\"-none-\",\"quotes_item_subtext\":\"-none-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"features_item_description\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"strips_item_description\":\"-default-\",\"sliders_item_image\":\"-default-\",\"features_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"headlines_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"features_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(209, '94', 'simple', '1', 3, '{\"_article_title\":\"Item 04\",\"lists_item_title\":\"Use SCSS, LESS or CSS\",\"lists_item_image\":\"-default-\",\"lists_item_link\":\"#\",\"lists_item_description\":\"<div><span><span class=\\\"visible-large\\\">The <\\/span>Gantry 5<span class=\\\"visible-large\\\"> Framework<\\/span> has built-in support for SCSS<span class=\\\"visible-tablet\\\">, <\\/span><span class=\\\"hidden-tablet\\\"> and pure <\\/span>CSS,<span class=\\\"visible-large\\\">with the ability to support<\\/span><span class=\\\"hidden-large\\\"> and<\\/span> LESS.<\\/span><\\/div>\",\"mosaic_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"grids_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"grids_item_image\":\"-default-\",\"tabs_item_link\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"tabs_item_description\":\"-default-\",\"quotes_item_author\":\"-none-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"mosaic_item_link\":\"-default-\",\"sliders_item_title\":\"-default-\",\"features_item_title\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"tables_item_price\":\"-none-\",\"quotes_item_subtext\":\"-none-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"features_item_description\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"strips_item_description\":\"-default-\",\"sliders_item_image\":\"-default-\",\"features_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"headlines_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"features_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(236, '91', 'simple', '2', 0, '{\"_article_title\":\"Item 01\",\"tabs_item_title\":\"Guide\",\"tabs_item_icon\":\"-default-\",\"tabs_item_link\":\"-default-\",\"tabs_item_description\":\"<div class=\\\"g-infolist\\\">\\r\\n\\t<div class=\\\"g-infolist-item g-infolist-with-img\\\">\\r\\n\\t\\t<div class=\\\"g-infolist-item-img g-block size-22 hidden-phone\\\">\\r\\n\\t\\t\\t<img src=\\\"templates\\/rt_callisto\\/images\\/demo\\/home\\/feature\\/roksprocket-tabs\\/img-01.jpg\\\" alt=\\\"image\\\">\\r\\n\\t\\t<\\/div>\\r\\n\\t\\t<div class=\\\"g-infolist-item-text g-block size-68\\\">\\r\\n\\t\\t\\t<div class=\\\"g-infolist-item-title\\\">\\r\\n\\t\\t\\t\\t<a href=\\\"http:\\/\\/docs.gantry.org\\\"><span>Introduce yourself to the <span class=\\\"visible-large\\\">new and <\\/span><span class=\\\"hidden-tablet\\\">powerful <\\/span>Gantry 5 Framework<\\/span><\\/a>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"g-infolist-item-desc\\\">\\r\\n\\t\\t\\t\\t<span>Gantry benefits from an extensive, and ever expanding, catalog of online and freely available documentation covering basic to <span class=\\\"hidden-tablet\\\">more <\\/span>advanced topics.<\\/span>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n\\t<div class=\\\"g-infolist-item g-infolist-with-img\\\">\\r\\n\\t\\t<div class=\\\"g-infolist-item-img g-block size-22 hidden-phone\\\">\\r\\n\\t\\t\\t<img src=\\\"templates\\/rt_callisto\\/images\\/demo\\/home\\/feature\\/roksprocket-tabs\\/img-02.jpg\\\" alt=\\\"image\\\">\\r\\n\\t\\t<\\/div>\\r\\n\\t\\t<div class=\\\"g-infolist-item-text g-block size-68\\\">\\r\\n\\t\\t\\t<div class=\\\"g-infolist-item-title\\\">\\r\\n\\t\\t\\t\\t<a href=\\\"http:\\/\\/www.rockettheme.com\\/docs\\\"><span>Template <span class=\\\"hidden-tablet\\\">specific <\\/span>help guides to assist in <span class=\\\"visible-large\\\">installation,<\\/span> setup and <span class=\\\"visible-tablet\\\">use<\\/span><span class=\\\"hidden-tablet\\\">configuration<\\/span><\\/span><\\/a>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"g-infolist-item-desc\\\">\\r\\n\\t\\t\\t\\t<span>The RocketTheme site provides free documentation for all our templates and extensions, to supplement understanding<span class=\\\"visible-large\\\"> of their various features and capabilities<\\/span>.<\\/span>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n\",\"lists_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"strips_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"grids_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"grids_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_author\":\"-none-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"mosaic_item_link\":\"-default-\",\"lists_item_link\":\"-default-\",\"sliders_item_title\":\"-default-\",\"features_item_title\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"tables_item_price\":\"-none-\",\"quotes_item_subtext\":\"-none-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"features_item_description\":\"-default-\",\"lists_item_description\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"strips_item_description\":\"-default-\",\"sliders_item_image\":\"-default-\",\"features_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"headlines_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"features_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(237, '91', 'simple', '1', 1, '{\"_article_title\":\"Item 02\",\"tabs_item_title\":\"Support\",\"tabs_item_icon\":\"-default-\",\"tabs_item_link\":\"-default-\",\"tabs_item_description\":\"<div class=\\\"g-infolist\\\">\\r\\n\\t<div class=\\\"g-infolist-item g-infolist-with-img\\\">\\r\\n\\t\\t<div class=\\\"g-infolist-item-img g-block size-22 hidden-phone\\\">\\r\\n\\t\\t\\t<img src=\\\"templates\\/rt_callisto\\/images\\/demo\\/home\\/feature\\/roksprocket-tabs\\/img-03.jpg\\\" alt=\\\"image\\\">\\r\\n\\t\\t<\\/div>\\r\\n\\t\\t<div class=\\\"g-infolist-item-text g-block size-68\\\">\\r\\n\\t\\t\\t<div class=\\\"g-infolist-item-title\\\">\\r\\n\\t\\t\\t\\t<a href=\\\"http:\\/\\/www.rockettheme.com\\/forum\\\"><span>A <span class=\\\"hidden-tablet\\\">community<\\/span> forum to ask the <span class=\\\"visible-large\\\">moderators and <\\/span>developers questions<\\/span><\\/a>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"g-infolist-item-desc\\\">\\r\\n\\t\\t\\t\\tOur forums are monitored by a team of moderators, as well as the developers, to assist in general questions and problems relating to the templates and extensions.\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n\\t<div class=\\\"g-infolist-item g-infolist-with-img\\\">\\r\\n\\t\\t<div class=\\\"g-infolist-item-img g-block size-22 hidden-phone\\\">\\r\\n\\t\\t\\t<img src=\\\"templates\\/rt_callisto\\/images\\/demo\\/home\\/feature\\/roksprocket-tabs\\/img-04.jpg\\\" alt=\\\"image\\\">\\r\\n\\t\\t<\\/div>\\r\\n\\t\\t<div class=\\\"g-infolist-item-text g-block size-68\\\">\\r\\n\\t\\t\\t<div class=\\\"g-infolist-item-title\\\">\\r\\n\\t\\t\\t\\t<a href=\\\"https:\\/\\/github.com\\/gantry\\/\\\">Gantry is community driven<span class=\\\"visible-large\\\"> and offers direct chat via Gitter<\\/span><\\/a>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"g-infolist-item-desc\\\">\\r\\n\\t\\t\\t\\tGithub allows for an system where you can contribute code, post issues and feedback on the Gantry open source project.<span class=\\\"hidden-tablet\\\"> Discuss Gantry live with us via Gitter.<\\/span>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n\",\"lists_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"strips_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"grids_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"grids_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_author\":\"-none-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"mosaic_item_link\":\"-default-\",\"lists_item_link\":\"-default-\",\"sliders_item_title\":\"-default-\",\"features_item_title\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"tables_item_price\":\"-none-\",\"quotes_item_subtext\":\"-none-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"features_item_description\":\"-default-\",\"lists_item_description\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"strips_item_description\":\"-default-\",\"sliders_item_image\":\"-default-\",\"features_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"headlines_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"features_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(333, '92', 'simple', '3', 0, '{\"_article_title\":\"Item 01\",\"headlines_item_image\":\"-default-\",\"headlines_item_link\":\"#\",\"headlines_item_description\":\"Built with SASS - a powerful, professional CSS extension language\\u2026\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"features_item_title\":\"-default-\",\"lists_item_description\":\"-default-\",\"strips_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"features_item_description\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"features_item_image\":\"-default-\",\"features_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(334, '92', 'simple', '2', 1, '{\"_article_title\":\"Item 02\",\"headlines_item_image\":\"-default-\",\"headlines_item_link\":\"#\",\"headlines_item_description\":\"Bundled with the FontAwesome icon library to diversify content style\\u2026\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"features_item_title\":\"-default-\",\"lists_item_description\":\"-default-\",\"strips_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"features_item_description\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"features_item_image\":\"-default-\",\"features_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(453, '93', 'simple', '8', 0, '{\"_article_title\":\"Item 01\",\"strips_item_title\":\"<span class=\\\"sprocket-strips-tag\\\">category<\\/span>Animale mici, de companie\",\"strips_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/categories\\/13-cele-mai-mici-animale-din-lume11.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_8__params_strips_item_image\'}\",\"strips_item_link\":\"http:\\/\\/localhost\\/vet\\/index.php?option=com_jobsfactory&amp;task=listjobs&amp;cat=1&amp;Itemid=442\",\"strips_item_description\":\"\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"features_item_title\":\"-default-\",\"lists_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"features_item_description\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"features_item_image\":\"-default-\",\"features_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(454, '93', 'simple', '7', 1, '{\"_article_title\":\"Item 02\",\"strips_item_title\":\"<span class=\\\"sprocket-strips-tag\\\">Category<\\/span>Animale de ferma\",\"strips_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/categories\\/2011_06_17_83355377_rsz.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_7__params_strips_item_image\'}\",\"strips_item_link\":\"http:\\/\\/localhost\\/vet\\/index.php?option=com_jobsfactory&amp;task=listjobs&amp;cat=2&amp;Itemid=442\",\"strips_item_description\":\"\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"features_item_title\":\"-default-\",\"lists_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"features_item_description\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"features_item_image\":\"-default-\",\"features_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(455, '93', 'simple', '6', 2, '{\"_article_title\":\"Item 03\",\"strips_item_title\":\"<span class=\\\"sprocket-strips-tag\\\">Category<\\/span>Clinica Mixta\",\"strips_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/categories\\/biovet1.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_6__params_strips_item_image\'}\",\"strips_item_link\":\"http:\\/\\/localhost\\/vet\\/index.php?option=com_jobsfactory&amp;task=listjobs&amp;cat=3&amp;Itemid=442\",\"strips_item_description\":\"\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"features_item_title\":\"-default-\",\"lists_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"features_item_description\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"features_item_image\":\"-default-\",\"features_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(456, '93', 'simple', '5', 3, '{\"_article_title\":\"Item 04\",\"strips_item_title\":\"<span class=\\\"sprocket-strips-tag\\\">Category<\\/span>Ecvine\",\"strips_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/categories\\/fsf.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_5__params_strips_item_image\'}\",\"strips_item_link\":\"http:\\/\\/localhost\\/vet\\/index.php?option=com_jobsfactory&amp;task=listjobs&amp;cat=4&amp;Itemid=442\",\"strips_item_description\":\"\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"features_item_title\":\"-default-\",\"lists_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"features_item_description\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"features_item_image\":\"-default-\",\"features_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(457, '93', 'simple', '4', 4, '{\"_article_title\":\"Item 05\",\"strips_item_title\":\"<span class=\\\"sprocket-strips-tag\\\">Category<\\/span>Animale exotice\",\"strips_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/categories\\/829131_62618688.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_4__params_strips_item_image\'}\",\"strips_item_link\":\"http:\\/\\/localhost\\/vet\\/index.php?option=com_jobsfactory&amp;task=listjobs&amp;cat=5&amp;Itemid=442\",\"strips_item_description\":\"\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"features_item_title\":\"-default-\",\"lists_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"features_item_description\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"features_item_image\":\"-default-\",\"features_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(458, '93', 'simple', '3', 5, '{\"_article_title\":\"Item 06\",\"strips_item_title\":\"<span class=\\\"sprocket-strips-tag\\\">Category<\\/span>Management\",\"strips_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/categories\\/23_Management_Board_3831143_-e1292691142264.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_3__params_strips_item_image\'}\",\"strips_item_link\":\"http:\\/\\/localhost\\/vet\\/index.php?option=com_jobsfactory&amp;task=listjobs&amp;cat=6&amp;Itemid=442\",\"strips_item_description\":\"\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"features_item_title\":\"-default-\",\"lists_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"features_item_description\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"features_item_image\":\"-default-\",\"features_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(459, '93', 'simple', '2', 6, '{\"_article_title\":\"Item 07\",\"strips_item_title\":\"<span class=\\\"sprocket-strips-tag\\\">category<\\/span>Acvacultura\",\"strips_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/categories\\/acvacultura.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_2__params_strips_item_image\'}\",\"strips_item_link\":\"http:\\/\\/localhost\\/vet\\/index.php?option=com_jobsfactory&amp;task=listjobs&amp;cat=7&amp;Itemid=442\",\"strips_item_description\":\"\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"features_item_title\":\"-default-\",\"lists_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"features_item_description\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"features_item_image\":\"-default-\",\"features_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(460, '93', 'simple', '1', 7, '{\"_article_title\":\"Item 08\",\"strips_item_title\":\"<span class=\\\"sprocket-strips-tag\\\">Category<\\/span>Circumscriptie Veterinara\",\"strips_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/categories\\/3ambulanta.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_1__params_strips_item_image\'}\",\"strips_item_link\":\"http:\\/\\/localhost\\/vet\\/index.php?option=com_jobsfactory&amp;task=listjobs&amp;cat=9&amp;Itemid=442\",\"strips_item_description\":\"\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"features_item_title\":\"-default-\",\"lists_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"features_item_description\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"features_item_image\":\"-default-\",\"features_item_link\":\"-default-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(466, '88', 'simple', '3', 0, '{\"_article_title\":\"Item 01\",\"features_item_title\":\"<strong>Bun venit!<\\/strong>\",\"features_item_description\":\"<p>Bun venit pe Vet.ro, platforma ta online pentru medicina veterinara!.<\\/p><a class=\\\"button button-3\\\" href=\\\"#\\\">Citeste mai mult<\\/a>\",\"features_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/hd-dog-wallpaper-with-a-dog-with-yellow-roses-in-his-mouth-hd-dogs-backgrounds-animal.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_3__params_features_item_image\'}\",\"features_item_link\":\"\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"lists_item_description\":\"-default-\",\"strips_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}');
INSERT INTO `sorin_roksprocket_items` (`id`, `module_id`, `provider`, `provider_id`, `order`, `params`) VALUES
(467, '88', 'simple', '2', 1, '{\"_article_title\":\"Item 02\",\"features_item_title\":\"Informatii,  joburi, reclame!\",\"features_item_description\":\"<p>Vetjobs.ro ofera o comunitate completa in domeniul medicinii veterinare. Esti in cautarea unui job, sau doar te uiti dupa shopping pentru animalul tau? Esti in locul potrivit!<\\/p>                    \",\"features_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/hamster_rodent_animal_cute_100093_1920x1080.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_2__params_features_item_image\'}\",\"features_item_link\":\"-none-\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"lists_item_description\":\"-default-\",\"strips_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(468, '88', 'simple', '1', 2, '{\"_article_title\":\"Item 03\",\"features_item_title\":\"<strong>VetJobs<\\/strong>\",\"features_item_description\":\"<p><strong>Suntem dedicati medicinii veterinare. Fie ca esti student, proaspat absolvent, medic cu experienta ori angajator, avem de toate pentru toata lumea.<\\/strong><\\/p>                    <a class=\\\"button button-3\\\" href=\\\"http:\\/\\/localhost\\/vet\\/index.php?option=com_jobsfactory&view=jobs&layout=listjobs&task=listjobs&Itemid=441\\\">Go to Jobs<\\/a>\",\"features_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/1920x1080-Cute-Cat-HD-Animal-Wallpaper.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_1__params_features_item_image\'}\",\"features_item_link\":\"\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"lists_item_description\":\"-default-\",\"strips_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(469, '88', 'simple', '4', 3, '{\"_article_title\":\"Simple Item 4\",\"features_item_title\":\"Publicitate\",\"features_item_description\":\"<p>Ai ceva de vandut\\/cumparat pentru animalutul tau? Esti in locul potrivit!<\\/p><a class=\\\"button button-3\\\" href=\\\"http:\\/\\/localhost\\/vet\\/index.php?option=com_adsfactory&view=user&task=userdetails&controller=user&Itemid=497\\\">Completeaza-ti profilul de Reclame!<\\/a>\",\"features_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/p01teh.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_4__params_features_item_image\'}\",\"features_item_link\":\"-default-\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"lists_item_description\":\"-default-\",\"strips_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}'),
(470, '88', 'simple', '5', 4, '{\"_article_title\":\"Simple Item 5\",\"features_item_title\":\"Cursuri dezvoltare\",\"features_item_description\":\"Esti o companie care organizeaza cursuri de dezvoltare profesionala in domeniul medicinii veterinare? Iti poti promova cursul aici!<a class=\\\"button button-3\\\" href=\\\"http:\\/\\/localhost\\/vet\\/index.php?option=com_eventsfactory&view=profile.show&Itemid=454\\\">Completeaza-ti profilul de Eventuri!<\\/a>\",\"features_item_image\":\"{\'type\':\'mediamanager\',\'path\':\'images\\/desktop-hd-parrots-birds-images.jpg\',\'preview\':\'\',\'link\':\'index.php?option=com_media&view=images&layout=default&tmpl=component&e_name=items_simple_5__params_features_item_image\'}\",\"features_item_link\":\"-default-\",\"strips_item_title\":\"-default-\",\"tabs_item_title\":\"-default-\",\"tables_item_title\":\"-default-\",\"lists_item_title\":\"-default-\",\"grids_item_title\":\"-default-\",\"mosaic_item_title\":\"-default-\",\"tables_item_description\":\"-default-\",\"mosaic_item_description\":\"-default-\",\"quotes_item_description\":\"-default-\",\"tabs_item_icon\":\"-default-\",\"grids_item_description\":\"-default-\",\"tabs_item_link\":\"-default-\",\"grids_item_image\":\"-default-\",\"lists_item_image\":\"-default-\",\"mosaic_item_image\":\"-default-\",\"quotes_item_image\":\"-default-\",\"tables_item_image\":\"-default-\",\"strips_item_image\":\"-default-\",\"mosaic_item_link\":\"-default-\",\"quotes_item_author\":\"-none-\",\"lists_item_link\":\"-default-\",\"strips_item_link\":\"-default-\",\"grids_item_link\":\"-default-\",\"tables_item_class\":\"-none-\",\"tabs_item_description\":\"-default-\",\"mosaic_item_tags\":\"-article-\",\"sliders_item_title\":\"-default-\",\"quotes_item_subtext\":\"-none-\",\"tables_item_price\":\"-none-\",\"lists_item_description\":\"-default-\",\"strips_item_description\":\"-default-\",\"sliders_item_description\":\"-default-\",\"headlines_item_image\":\"-default-\",\"quotes_item_link\":\"-default-\",\"tables_item_feature_1\":\"-none-\",\"headlines_item_link\":\"-default-\",\"quotes_item_direction\":\"-bottomleft-\",\"sliders_item_image\":\"-default-\",\"tables_item_feature_2\":\"-none-\",\"sliders_item_link\":\"-default-\",\"tables_item_feature_3\":\"-none-\",\"headlines_item_description\":\"-default-\",\"tables_item_feature_4\":\"-none-\",\"tables_item_link\":\"-default-\",\"tables_item_link_text\":\"Buy Now\"}');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_schemas`
--

CREATE TABLE `sorin_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_schemas`
--

INSERT INTO `sorin_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.6.3-2016-08-16');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_session`
--

CREATE TABLE `sorin_session` (
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `guest` tinyint(4) UNSIGNED DEFAULT '1',
  `time` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_session`
--

INSERT INTO `sorin_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('1gkc9jv6p0oaeqd7jo8l18p456', 0, 1, '1491983000', 'joomla|s:712:\"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjozOntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjM6e3M6NzoiY291bnRlciI7aTo2O3M6NToidGltZXIiO086ODoic3RkQ2xhc3MiOjM6e3M6NToic3RhcnQiO2k6MTQ5MTk4Mjk0NztzOjQ6Imxhc3QiO2k6MTQ5MTk4Mjk5MTtzOjM6Im5vdyI7aToxNDkxOTgyOTk4O31zOjU6InRva2VuIjtzOjMyOiJ3SGttYXlLaHFFNVM3aE1Zc09LMWEwTzNRbmZVQkNGbCI7fXM6ODoicmVnaXN0cnkiO086MjQ6Ikpvb21sYVxSZWdpc3RyeVxSZWdpc3RyeSI6Mzp7czo3OiIAKgBkYXRhIjtPOjg6InN0ZENsYXNzIjowOnt9czoxNDoiACoAaW5pdGlhbGl6ZWQiO2I6MTtzOjk6InNlcGFyYXRvciI7czoxOiIuIjt9czo0OiJ1c2VyIjtPOjU6IkpVc2VyIjoxOntzOjI6ImlkIjtpOjA7fX19czoxNDoiACoAaW5pdGlhbGl6ZWQiO2I6MDtzOjk6InNlcGFyYXRvciI7czoxOiIuIjt9\";', 0, '');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_tags`
--

CREATE TABLE `sorin_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_tags`
--

INSERT INTO `sorin_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 631, '2011-01-01 00:00:01', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_template_styles`
--

CREATE TABLE `sorin_template_styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `sorin_template_styles`
--

INSERT INTO `sorin_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Default', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"logo\":\"images\\/joomla_black.gif\",\"sitetitle\":\"Joomla!\",\"sitedescription\":\"Open Source Content Management\",\"navposition\":\"left\",\"templatecolor\":\"personal\",\"html5\":\"0\"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{\"showSiteName\":\"0\",\"colourChoice\":\"\",\"boldText\":\"0\"}'),
(7, 'protostar', 0, '0', 'protostar - Default', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}'),
(8, 'isis', 1, '1', 'isis - Default', '{\"templateColor\":\"\",\"logoFile\":\"\"}'),
(66, 'rt_callisto', 0, '1', 'Callisto - VetJobs - Default', '{\"configuration\":\"66\",\"preset\":\"default\"}'),
(67, 'rt_callisto', 0, '0', 'Callisto - VetJobs - Jobs', '{\"configuration\":\"67\",\"preset\":\"home\"}'),
(68, 'rt_callisto', 0, '0', 'Callisto - VetJobs - Categories', '{\"configuration\":\"68\",\"preset\":\"features_-_overview\"}'),
(69, 'rt_callisto', 0, '0', 'Callisto - Features - Typography', '{\"preset\":\"features_-_typography\",\"configuration\":\"69\"}'),
(70, 'rt_callisto', 0, '0', 'Callisto - Features - Variations', '{\"preset\":\"features_-_variations\",\"configuration\":\"70\"}'),
(71, 'rt_callisto', 0, '0', 'Callisto - Pages - About Us', '{\"preset\":\"pages_-_about_us\",\"configuration\":\"71\"}'),
(72, 'rt_callisto', 0, '0', 'Callisto - Pages - Our Team', '{\"preset\":\"pages_-_our_team\",\"configuration\":\"72\"}'),
(73, 'rt_callisto', 0, '0', 'Callisto - Pages - Services', '{\"preset\":\"pages_-_services\",\"configuration\":\"73\"}'),
(74, 'rt_callisto', 0, '0', 'Callisto - Pages - Pricing', '{\"preset\":\"pages_-_pricing\",\"configuration\":\"74\"}'),
(75, 'rt_callisto', 0, '0', 'Callisto - Pages - Portfolio', '{\"preset\":\"pages_-_portfolio\",\"configuration\":\"75\"}'),
(76, 'rt_callisto', 0, '0', 'Callisto - Pages - Clients', '{\"preset\":\"pages_-_clients\",\"configuration\":\"76\"}'),
(77, 'rt_callisto', 0, '0', 'Callisto - Pages - Faq', '{\"preset\":\"pages_-_faq\",\"configuration\":\"77\"}'),
(78, 'rt_callisto', 0, '0', 'Callisto - Pages - Blog', '{\"preset\":\"pages_-_blog\",\"configuration\":\"78\"}'),
(79, 'rt_callisto', 0, '0', 'Callisto - Pages - Contact', '{\"preset\":\"pages_-_contact\",\"configuration\":\"79\"}'),
(81, 'rt_callisto', 0, '0', 'Callisto - Pages - Offline', '{\"preset\":\"pages_-_offline\",\"configuration\":\"81\"}'),
(82, 'rt_callisto', 0, '0', 'Callisto - Pages - Ads', '{\"configuration\":\"82\",\"preset\":\"pages_-_clients\"}'),
(83, 'rt_callisto', 0, '0', 'Callisto - VetJobs - Home', '{\"configuration\":\"83\",\"preset\":\"default\"}');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_ucm_base`
--

CREATE TABLE `sorin_ucm_base` (
  `ucm_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_ucm_content`
--

CREATE TABLE `sorin_ucm_content` (
  `core_content_id` int(10) UNSIGNED NOT NULL,
  `core_type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `core_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_checked_out_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_featured` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_publish_up` datetime NOT NULL,
  `core_publish_down` datetime NOT NULL,
  `core_content_item_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID from the individual type table',
  `asset_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'FK to the #__assets table.',
  `core_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Contains core content data in name spaced fields';

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_ucm_history`
--

CREATE TABLE `sorin_ucm_history` (
  `version_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) UNSIGNED NOT NULL,
  `ucm_type_id` int(10) UNSIGNED NOT NULL,
  `version_note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `character_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_ucm_history`
--

INSERT INTO `sorin_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(1, 9, 1, '', '2017-03-08 11:28:55', 631, 2137, '4d9f16cd62dd0d9d13c1b1d3211624bfa9c5952d', '{\"id\":9,\"asset_id\":109,\"title\":\"Welcome!\",\"alias\":\"welcome\",\"introtext\":\"<p>Welcome to PulaPeLantvet.ro !!<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"8\",\"created\":\"2017-03-08 11:28:55\",\"created_by\":\"631\",\"created_by_alias\":\"\",\"modified\":\"2017-03-08 11:28:55\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2017-03-08 11:28:55\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"1\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(2, 9, 1, '', '2017-03-08 11:30:12', 631, 2156, '3262a134436c3eb21864cf2509eb72569a5fcd18', '{\"id\":9,\"asset_id\":\"109\",\"title\":\"Welcome!\",\"alias\":\"welcome\",\"introtext\":\"<p>Welcome to PulaPeLantvet.ro !!<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-03-08 11:28:55\",\"created_by\":\"631\",\"created_by_alias\":\"\",\"modified\":\"2017-03-08 11:30:12\",\"modified_by\":\"631\",\"checked_out\":\"631\",\"checked_out_time\":\"2017-03-08 11:30:04\",\"publish_up\":\"2017-03-08 11:28:55\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"1\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"1\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(3, 9, 1, '', '2017-03-08 11:38:26', 631, 2722, '582667f6f2753ff25bad334c9cdd6db943076f92', '{\"id\":9,\"asset_id\":\"109\",\"title\":\"Welcome!\",\"alias\":\"welcome\",\"introtext\":\"<p>Welcome to PulaPeLantvet.ro !!<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!<\\/p>\\r\\n<p>In order to apply for Jobs, you will first need to complete your Profile. Link is in the Main Menu.<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to develop your career by attending or creating courses? You will have to first complete your profile\\u00a0<a title=\\\"Profile\\\" href=\\\"index.php\\/events-profile\\\">Editing Course Profile<\\/a>\\u00a0.\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to post ads or buy products which are already advertised? You will have to complete your profile\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-03-08 11:28:55\",\"created_by\":\"631\",\"created_by_alias\":\"\",\"modified\":\"2017-03-08 11:38:26\",\"modified_by\":\"631\",\"checked_out\":\"631\",\"checked_out_time\":\"2017-03-08 11:30:45\",\"publish_up\":\"2017-03-08 11:28:55\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":3,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"2\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"1\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(4, 9, 1, '', '2017-03-09 09:34:57', 631, 2838, 'd4fc5323e252d7272dfe31d3aa0a2abe6ab6da55', '{\"id\":9,\"asset_id\":\"109\",\"title\":\"Welcome!\",\"alias\":\"welcome\",\"introtext\":\"<p>Welcome to PulaPeLantvet.ro !!<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!<\\/p>\\r\\n<p>In order to apply for Jobs, you will first need to complete your Profile. Link is in the Main Menu.<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to develop your career by attending or creating courses? You will have to first complete your profile\\u00a0<a title=\\\"Profile\\\" href=\\\"index.php\\/events-profile\\\">Editing Course Profile<\\/a>\\u00a0.\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to post ads or buy products which are already advertised? You will have to complete your profile\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Post a new add\\u00a0<a href=\\\"localhost\\/vet\\/index.php\\/new-ad\\\">here<\\/a><\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-03-08 11:28:55\",\"created_by\":\"631\",\"created_by_alias\":\"\",\"modified\":\"2017-03-09 09:34:57\",\"modified_by\":\"631\",\"checked_out\":\"631\",\"checked_out_time\":\"2017-03-09 09:30:09\",\"publish_up\":\"2017-03-08 11:28:55\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":4,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"18\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"1\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(5, 9, 1, '', '2017-03-09 09:35:46', 631, 2822, '16533cbf228f0536f69052ab6e233064c526fee9', '{\"id\":9,\"asset_id\":\"109\",\"title\":\"Welcome!\",\"alias\":\"welcome\",\"introtext\":\"<p>Welcome to PulaPeLantvet.ro !!<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!<\\/p>\\r\\n<p>In order to apply for Jobs, you will first need to complete your Profile. Link is in the Main Menu.<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to develop your career by attending or creating courses? You will have to first complete your profile\\u00a0<a title=\\\"Profile\\\" href=\\\"index.php\\/events-profile\\\">Editing Course Profile<\\/a>\\u00a0.\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to post ads or buy products which are already advertised? You will have to complete your profile\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Post a new add\\u00a0<a href=\\\"indes.php\\/new-ad\\\">Here<\\/a><\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-03-08 11:28:55\",\"created_by\":\"631\",\"created_by_alias\":\"\",\"modified\":\"2017-03-09 09:35:46\",\"modified_by\":\"631\",\"checked_out\":\"631\",\"checked_out_time\":\"2017-03-09 09:35:19\",\"publish_up\":\"2017-03-08 11:28:55\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":5,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"20\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"1\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(6, 9, 1, '', '2017-03-09 09:36:24', 631, 2835, '01026bbc6d847ddd7d6f85c0a5937021f3859395', '{\"id\":9,\"asset_id\":\"109\",\"title\":\"Welcome!\",\"alias\":\"welcome\",\"introtext\":\"<p>Welcome to PulaPeLantvet.ro !!<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!<\\/p>\\r\\n<p>In order to apply for Jobs, you will first need to complete your Profile. Link is in the Main Menu.<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to develop your career by attending or creating courses? You will have to first complete your profile\\u00a0<a title=\\\"Profile\\\" href=\\\"index.php\\/events-profile\\\">Editing Course Profile<\\/a>\\u00a0.\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to post ads or buy products which are already advertised? You will have to complete your profile\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Post a new add\\u00a0<a href=\\\"index.php\\/new-ad\\\">index.php\\/new-ad<\\/a><\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-03-08 11:28:55\",\"created_by\":\"631\",\"created_by_alias\":\"\",\"modified\":\"2017-03-09 09:36:24\",\"modified_by\":\"631\",\"checked_out\":\"631\",\"checked_out_time\":\"2017-03-09 09:36:03\",\"publish_up\":\"2017-03-08 11:28:55\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":6,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"21\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"1\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(7, 9, 1, '', '2017-03-09 09:37:04', 631, 2769, '1286c5f5a871b9d2ebd84dd051c16b69b71d0ab7', '{\"id\":9,\"asset_id\":\"109\",\"title\":\"Welcome!\",\"alias\":\"welcome\",\"introtext\":\"<p>Welcome to PulaPeLantvet.ro !!<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!<\\/p>\\r\\n<p>In order to apply for Jobs, you will first need to complete your Profile. Link is in the Main Menu.<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to develop your career by attending or creating courses? You will have to first complete your profile\\u00a0<a title=\\\"Profile\\\" href=\\\"index.php\\/events-profile\\\">Editing Course Profile<\\/a>\\u00a0.\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to post ads or buy products which are already advertised? You will have to complete your profile\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-03-08 11:28:55\",\"created_by\":\"631\",\"created_by_alias\":\"\",\"modified\":\"2017-03-09 09:37:04\",\"modified_by\":\"631\",\"checked_out\":\"631\",\"checked_out_time\":\"2017-03-09 09:36:54\",\"publish_up\":\"2017-03-08 11:28:55\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":7,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"24\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"1\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(8, 9, 1, '', '2017-03-09 11:42:37', 631, 2779, '6fcf19c806a679306365648abd1d984e60394117', '{\"id\":9,\"asset_id\":\"109\",\"title\":\"Welcome!\",\"alias\":\"welcome\",\"introtext\":\"<p>Welcome to PulaPeLantvet.ro !!<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!<\\/p>\\r\\n<p>In order to apply for Jobs, you will first need to complete your Profile. Link is in the Main Menu.<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to develop your career by attending or creating courses? You will have to first complete your profile\\u00a0<a href=\\\"index.php?option=com_eventsfactory&amp;view=profile.show&amp;Itemid=454\\\">here<\\/a>\\u00a0.\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to post ads or buy products which are already advertised? You will have to complete your profile\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-03-08 11:28:55\",\"created_by\":\"631\",\"created_by_alias\":\"\",\"modified\":\"2017-03-09 11:42:37\",\"modified_by\":\"631\",\"checked_out\":\"631\",\"checked_out_time\":\"2017-03-09 11:42:06\",\"publish_up\":\"2017-03-08 11:28:55\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":8,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"46\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"1\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(9, 9, 1, '', '2017-03-14 12:30:50', 631, 2904, '4031bf3424c331700b8f10934fd9e8d6960d97bc', '{\"id\":9,\"asset_id\":\"109\",\"title\":\"Welcome!\",\"alias\":\"welcome\",\"introtext\":\"<p>Welcome to Vetjobs.ro !!<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!<\\/p>\\r\\n<p>In order to apply for Jobs, you will first need to complete your Profile. Link is in the Main Menu.<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to develop your career by attending or creating courses? You will have to first complete your profile\\u00a0<a href=\\\"index.php?option=com_eventsfactory&amp;view=profile.show&amp;Itemid=454\\\">here<\\/a>\\u00a0.\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to post ads or buy products which are already advertised? You will have to complete your profile\\u00a0<a href=\\\"index.php?option=com_adsfactory&amp;view=user&amp;task=userdetails&amp;controller=user&amp;Itemid=497\\\">here<\\/a>\\u00a0.<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-03-08 11:28:55\",\"created_by\":\"631\",\"created_by_alias\":\"\",\"modified\":\"2017-03-14 12:30:50\",\"modified_by\":\"631\",\"checked_out\":\"631\",\"checked_out_time\":\"2017-03-14 12:06:48\",\"publish_up\":\"2017-03-08 11:28:55\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":9,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"117\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"1\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(10, 9, 1, '', '2017-03-14 12:37:33', 631, 2667, '304939d8c0760eccb2c2c0bbac6c321ebebffee8', '{\"id\":9,\"asset_id\":\"109\",\"title\":\"Welcome!\",\"alias\":\"welcome\",\"introtext\":\"<p>Welcome to Vetjobs.ro !!<\\/p>\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>We are here to make your dreams come true? Are you looking for a job in the Veterinary Medicine Industry? Are you a company that needs new employees? Do you have something to sale, which will benefit this industry? Do you organize or want to participate in development courses? This is the place for you!<\\/p>\\r\\n<p>In order to apply for Jobs, you will first need to complete your Profile. Link is in the Main Menu.<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">Do you want to develop your career by attending or creating courses? You will have to first complete your profile\\u00a0<a href=\\\"index.php?option=com_eventsfactory&amp;view=profile.show&amp;Itemid=454\\\">here<\\/a>\\u00a0.\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\\r\\n<p style=\\\"text-align: left;\\\">\\u00a0<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-03-08 11:28:55\",\"created_by\":\"631\",\"created_by_alias\":\"\",\"modified\":\"2017-03-14 12:37:33\",\"modified_by\":\"631\",\"checked_out\":\"631\",\"checked_out_time\":\"2017-03-14 12:34:46\",\"publish_up\":\"2017-03-08 11:28:55\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/Cabinet-veterinar-si-farmacie-veterinara-Cata-Vet-Farm-Titu-1887.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":10,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"128\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"1\",\"language\":\"*\",\"xreference\":\"\"}', 0);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_updates`
--

CREATE TABLE `sorin_updates` (
  `update_id` int(11) NOT NULL,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `folder` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsurl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `infourl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Available Updates';

--
-- Salvarea datelor din tabel `sorin_updates`
--

INSERT INTO `sorin_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1, 3, 0, 'Bulgarian', '', 'pkg_bg-BG', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/bg-BG_details.xml', '', ''),
(2, 3, 0, 'Catalan', '', 'pkg_ca-ES', 'package', '', 0, '3.6.5.2', '', 'https://update.joomla.org/language/details3/ca-ES_details.xml', '', ''),
(3, 3, 0, 'Chinese Simplified', '', 'pkg_zh-CN', 'package', '', 0, '3.4.1.1', '', 'https://update.joomla.org/language/details3/zh-CN_details.xml', '', ''),
(4, 3, 0, 'Croatian', '', 'pkg_hr-HR', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/hr-HR_details.xml', '', ''),
(5, 3, 0, 'Czech', '', 'pkg_cs-CZ', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/cs-CZ_details.xml', '', ''),
(6, 3, 0, 'Danish', '', 'pkg_da-DK', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/da-DK_details.xml', '', ''),
(7, 3, 0, 'Dutch', '', 'pkg_nl-NL', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/nl-NL_details.xml', '', ''),
(8, 3, 0, 'Estonian', '', 'pkg_et-EE', 'package', '', 0, '3.6.0.1', '', 'https://update.joomla.org/language/details3/et-EE_details.xml', '', ''),
(9, 3, 0, 'Italian', '', 'pkg_it-IT', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/it-IT_details.xml', '', ''),
(10, 3, 0, 'Khmer', '', 'pkg_km-KH', 'package', '', 0, '3.4.5.1', '', 'https://update.joomla.org/language/details3/km-KH_details.xml', '', ''),
(11, 3, 0, 'Korean', '', 'pkg_ko-KR', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/ko-KR_details.xml', '', ''),
(12, 3, 0, 'Latvian', '', 'pkg_lv-LV', 'package', '', 0, '3.6.2.2', '', 'https://update.joomla.org/language/details3/lv-LV_details.xml', '', ''),
(13, 3, 0, 'Macedonian', '', 'pkg_mk-MK', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/mk-MK_details.xml', '', ''),
(14, 3, 0, 'Norwegian Bokmal', '', 'pkg_nb-NO', 'package', '', 0, '3.5.1.1', '', 'https://update.joomla.org/language/details3/nb-NO_details.xml', '', ''),
(15, 3, 0, 'Norwegian Nynorsk', '', 'pkg_nn-NO', 'package', '', 0, '3.4.2.1', '', 'https://update.joomla.org/language/details3/nn-NO_details.xml', '', ''),
(16, 3, 0, 'Persian', '', 'pkg_fa-IR', 'package', '', 0, '3.6.5.3', '', 'https://update.joomla.org/language/details3/fa-IR_details.xml', '', ''),
(17, 3, 0, 'Polish', '', 'pkg_pl-PL', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/pl-PL_details.xml', '', ''),
(18, 3, 0, 'Portuguese', '', 'pkg_pt-PT', 'package', '', 0, '3.6.4.3', '', 'https://update.joomla.org/language/details3/pt-PT_details.xml', '', ''),
(19, 3, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '3.6.2.2', '', 'https://update.joomla.org/language/details3/ru-RU_details.xml', '', ''),
(20, 3, 0, 'English AU', '', 'pkg_en-AU', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-AU_details.xml', '', ''),
(21, 3, 0, 'Slovak', '', 'pkg_sk-SK', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/sk-SK_details.xml', '', ''),
(22, 3, 0, 'English US', '', 'pkg_en-US', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-US_details.xml', '', ''),
(23, 3, 0, 'Swedish', '', 'pkg_sv-SE', 'package', '', 0, '3.6.3.1', '', 'https://update.joomla.org/language/details3/sv-SE_details.xml', '', ''),
(24, 3, 0, 'Syriac', '', 'pkg_sy-IQ', 'package', '', 0, '3.4.5.1', '', 'https://update.joomla.org/language/details3/sy-IQ_details.xml', '', ''),
(25, 3, 0, 'Tamil', '', 'pkg_ta-IN', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/ta-IN_details.xml', '', ''),
(26, 3, 0, 'Thai', '', 'pkg_th-TH', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/th-TH_details.xml', '', ''),
(27, 3, 0, 'Turkish', '', 'pkg_tr-TR', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/tr-TR_details.xml', '', ''),
(28, 3, 0, 'Ukrainian', '', 'pkg_uk-UA', 'package', '', 0, '3.6.3.1', '', 'https://update.joomla.org/language/details3/uk-UA_details.xml', '', ''),
(29, 3, 0, 'Uyghur', '', 'pkg_ug-CN', 'package', '', 0, '3.3.0.1', '', 'https://update.joomla.org/language/details3/ug-CN_details.xml', '', ''),
(30, 3, 0, 'Albanian', '', 'pkg_sq-AL', 'package', '', 0, '3.1.1.1', '', 'https://update.joomla.org/language/details3/sq-AL_details.xml', '', ''),
(31, 3, 0, 'Basque', '', 'pkg_eu-ES', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/eu-ES_details.xml', '', ''),
(32, 3, 0, 'Hindi', '', 'pkg_hi-IN', 'package', '', 0, '3.3.6.2', '', 'https://update.joomla.org/language/details3/hi-IN_details.xml', '', ''),
(33, 3, 0, 'German DE', '', 'pkg_de-DE', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/de-DE_details.xml', '', ''),
(34, 3, 0, 'Portuguese Brazil', '', 'pkg_pt-BR', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/pt-BR_details.xml', '', ''),
(35, 3, 0, 'Serbian Latin', '', 'pkg_sr-YU', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/sr-YU_details.xml', '', ''),
(36, 3, 0, 'Spanish', '', 'pkg_es-ES', 'package', '', 0, '3.6.3.1', '', 'https://update.joomla.org/language/details3/es-ES_details.xml', '', ''),
(37, 3, 0, 'Bosnian', '', 'pkg_bs-BA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/bs-BA_details.xml', '', ''),
(38, 3, 0, 'Serbian Cyrillic', '', 'pkg_sr-RS', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/sr-RS_details.xml', '', ''),
(39, 3, 0, 'Vietnamese', '', 'pkg_vi-VN', 'package', '', 0, '3.2.1.1', '', 'https://update.joomla.org/language/details3/vi-VN_details.xml', '', ''),
(40, 3, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/id-ID_details.xml', '', ''),
(41, 3, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/fi-FI_details.xml', '', ''),
(42, 3, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/sw-KE_details.xml', '', ''),
(43, 3, 0, 'Montenegrin', '', 'pkg_srp-ME', 'package', '', 0, '3.3.1.1', '', 'https://update.joomla.org/language/details3/srp-ME_details.xml', '', ''),
(44, 3, 0, 'English CA', '', 'pkg_en-CA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-CA_details.xml', '', ''),
(45, 3, 0, 'French CA', '', 'pkg_fr-CA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/fr-CA_details.xml', '', ''),
(46, 3, 0, 'Welsh', '', 'pkg_cy-GB', 'package', '', 0, '3.3.0.2', '', 'https://update.joomla.org/language/details3/cy-GB_details.xml', '', ''),
(47, 3, 0, 'Sinhala', '', 'pkg_si-LK', 'package', '', 0, '3.3.1.1', '', 'https://update.joomla.org/language/details3/si-LK_details.xml', '', ''),
(48, 3, 0, 'Dari Persian', '', 'pkg_prs-AF', 'package', '', 0, '3.4.4.2', '', 'https://update.joomla.org/language/details3/prs-AF_details.xml', '', ''),
(49, 3, 0, 'Turkmen', '', 'pkg_tk-TM', 'package', '', 0, '3.5.0.2', '', 'https://update.joomla.org/language/details3/tk-TM_details.xml', '', ''),
(50, 3, 0, 'Irish', '', 'pkg_ga-IE', 'package', '', 0, '3.6.4.1', '', 'https://update.joomla.org/language/details3/ga-IE_details.xml', '', ''),
(51, 3, 0, 'Dzongkha', '', 'pkg_dz-BT', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/dz-BT_details.xml', '', ''),
(52, 3, 0, 'Slovenian', '', 'pkg_sl-SI', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/sl-SI_details.xml', '', ''),
(53, 3, 0, 'Spanish CO', '', 'pkg_es-CO', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/es-CO_details.xml', '', ''),
(54, 3, 0, 'German CH', '', 'pkg_de-CH', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/de-CH_details.xml', '', ''),
(55, 3, 0, 'German AT', '', 'pkg_de-AT', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/de-AT_details.xml', '', ''),
(56, 3, 0, 'German LI', '', 'pkg_de-LI', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/de-LI_details.xml', '', ''),
(57, 3, 0, 'German LU', '', 'pkg_de-LU', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/de-LU_details.xml', '', ''),
(58, 3, 0, 'English NZ', '', 'pkg_en-NZ', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-NZ_details.xml', '', ''),
(59, 5, 0, 'RokGallery Extension', '', 'mod_rokgallery', 'module', '', 0, '2.42', '0802', 'http://updates.rockettheme.com/joomla/286/21ffe006', '', ''),
(60, 5, 0, 'RokWeather Module', '', 'mod_rokweather', 'module', '', 0, '2.0.4', '0802', 'http://updates.rockettheme.com/joomla/292/a0cbba72', '', ''),
(61, 5, 0, 'RokStock Module', '', 'mod_rokstock', 'module', '', 0, '2.0.3', '0802', 'http://updates.rockettheme.com/joomla/295/87c1121c', '', ''),
(62, 5, 0, 'RokMiniEvents3 Module', '', 'mod_rokminievents3', 'module', '', 0, '3.0.3', '0802', 'http://updates.rockettheme.com/joomla/297/1686051690', '', ''),
(63, 5, 0, 'RokQuickCart Extension', '', 'com_rokquickcart', 'component', '', 1, '2.1.5', '0802', 'http://updates.rockettheme.com/joomla/298/ddfa10eb', '', ''),
(64, 5, 0, 'RokNavMenu Module', '', 'mod_roknavmenu', 'module', '', 0, '2.0.9', '0802', 'http://updates.rockettheme.com/joomla/300/5a9aa468', '', ''),
(65, 5, 0, 'RokCandy Extension', '', 'rokcandy', 'plugin', 'system', 0, '2.0.2', '0802', 'http://updates.rockettheme.com/joomla/302/2df8a4e2', '', ''),
(66, 5, 0, 'RokComments Plugin', '', 'rokcomments', 'plugin', 'content', 0, '2.0.3', '0802', 'http://updates.rockettheme.com/joomla/303/d1fd7b76', '', ''),
(67, 5, 0, 'RokSocialButtons Plugin', '', 'roksocialbuttons', 'plugin', 'content', 0, '1.5.3', '0802', 'http://updates.rockettheme.com/joomla/381/269989291', '', ''),
(68, 7, 0, 'Hydrogen', 'Hydrogen Template', 'g5_hydrogen', 'template', '', 0, '5.4.10', '', 'http://updates.gantry.org/5.0/joomla/tpl_g5_hydrogen.xml', '', ''),
(69, 7, 0, 'Helium', 'Helium Template', 'g5_helium', 'template', '', 0, '5.4.10', '', 'http://updates.gantry.org/5.0/joomla/tpl_g5_helium.xml', '', ''),
(70, 8, 0, 'Test 1', '', 'com_empty_ext', 'component', '', 1, '3.1.1', '', 'thephpfactory.com/versions/update.xml', '', ''),
(71, 8, 0, 'Wall Factory', '', 'com_wallfactory', 'component', '', 1, '4.0.2', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(72, 8, 0, 'Media Mall Factory', '', 'com_mediamall', 'component', '', 1, '4.2.2', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(73, 8, 0, 'Collection Factory', '', 'com_collectionfactory', 'component', '', 1, '4.1.8', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(74, 8, 0, 'RSS Factory PRO', '', 'com_rssfactory_pro', 'component', '', 1, '4.2.4', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(75, 8, 0, 'RSS Factory', '', 'com_rssfactory', 'component', '', 1, '4.2.4', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(76, 8, 0, 'RSS 2 Image Factory', '', '', 'component', '', 1, '4.2.2', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(77, 8, 0, 'eBook Factory', '', 'com_ebooks', 'component', '', 1, '4.2.9', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(78, 8, 0, 'Briefcase Factory', '', 'com_brief', 'component', '', 1, '4.1.4', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(79, 8, 0, 'HOT or NOT Factory', '', 'com_hotornotfactory', 'component', '', 1, '4.1.5', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(80, 8, 0, 'Photo Battle Factory', '', 'com_photobattlefactory', 'component', '', 1, '4.1.5', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(81, 8, 0, 'Music Battle Factory', '', 'com_musicbattlefactory', 'component', '', 1, '4.1.5', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(82, 8, 0, 'Feedback Factory', '', 'com_feedbackfactory', 'component', '', 1, '3.0.2', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(83, 8, 0, 'Events Factory', '', 'com_eventsfactory', 'component', '', 1, '1.0.2', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(84, 8, 0, 'Blog Factory', '', 'com_blogfactory', 'component', '', 1, '4.3.3', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(85, 8, 0, 'Social Factory', '', 'com_socialfactory', 'component', '', 1, '3.7.7', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(86, 8, 0, 'Love Factory', '', 'com_lovefactory', 'component', '', 1, '4.4.9', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(87, 8, 0, 'Chat Factory', '', 'com_chatfactory', 'component', '', 1, '4.4.3', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(88, 8, 0, 'Auction Factory', '', 'com_auctionfactory', 'component', '', 1, '4.5.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(89, 8, 0, 'Reverse Auction Factory', '', 'com_rbids', 'component', '', 1, '4.3.3', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(90, 8, 0, 'Ads Factory', '', 'com_adsfactory', 'component', '', 1, '4.4.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(91, 8, 0, 'Article Factory Manager', '', 'com_articlefactory', 'component', '', 1, '4.3.5', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(92, 8, 0, 'Dutch Auction Factory', '', 'com_dutchfactory', 'component', '', 1, '2.0.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(93, 8, 804, 'Jobs Factory', '', 'com_jobsfactory', 'component', '', 1, '2.0.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(94, 8, 0, 'Penny Auction Factory', '', 'com_pennyfactory', 'component', '', 1, '2.0.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(95, 8, 0, 'Micro Deal Factory', '', 'com_microdeal', 'component', '', 1, '2.4.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(96, 8, 0, 'Raffle Factory', '', 'com_rafflefactory', 'component', '', 1, '3.5.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(97, 8, 0, 'jAnswers Factory', '', 'com_jooanswers', 'component', '', 1, '4.2.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(98, 8, 0, 'Spam Protect Factory', '', 'com_spamprotectfactory', 'component', '', 1, '1.1.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(99, 8, 0, 'SEO Keyword Factory', '', 'com_seokeywordfactory', 'component', '', 1, '3.4.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(100, 8, 0, 'Swap Factory', '', 'com_swapfactory', 'component', '', 1, '2.0.0', '', 'http://thephpfactory.com/versions/update.xml', '', ''),
(101, 9, 0, 'Armenian', '', 'pkg_hy-AM', 'package', '', 0, '3.4.4.1', '', 'https://update.joomla.org/language/details3/hy-AM_details.xml', '', ''),
(102, 9, 0, 'Malay', '', 'pkg_ms-MY', 'package', '', 0, '3.4.1.2', '', 'https://update.joomla.org/language/details3/ms-MY_details.xml', '', ''),
(103, 9, 0, 'Flemish', '', 'pkg_nl-BE', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/nl-BE_details.xml', '', ''),
(104, 9, 0, 'Chinese Traditional', '', 'pkg_zh-TW', 'package', '', 0, '3.6.3.1', '', 'https://update.joomla.org/language/details3/zh-TW_details.xml', '', ''),
(105, 9, 0, 'French', '', 'pkg_fr-FR', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/fr-FR_details.xml', '', ''),
(106, 9, 0, 'Galician', '', 'pkg_gl-ES', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/gl-ES_details.xml', '', ''),
(107, 9, 0, 'Georgian', '', 'pkg_ka-GE', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/ka-GE_details.xml', '', ''),
(108, 9, 0, 'Greek', '', 'pkg_el-GR', 'package', '', 0, '3.6.3.2', '', 'https://update.joomla.org/language/details3/el-GR_details.xml', '', ''),
(109, 9, 0, 'Japanese', '', 'pkg_ja-JP', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/ja-JP_details.xml', '', ''),
(110, 9, 0, 'Hebrew', '', 'pkg_he-IL', 'package', '', 0, '3.1.1.1', '', 'https://update.joomla.org/language/details3/he-IL_details.xml', '', ''),
(111, 9, 0, 'Hungarian', '', 'pkg_hu-HU', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/hu-HU_details.xml', '', ''),
(112, 9, 0, 'Afrikaans', '', 'pkg_af-ZA', 'package', '', 0, '3.6.3.1', '', 'https://update.joomla.org/language/details3/af-ZA_details.xml', '', ''),
(113, 9, 0, 'Arabic Unitag', '', 'pkg_ar-AA', 'package', '', 0, '3.6.5.2', '', 'https://update.joomla.org/language/details3/ar-AA_details.xml', '', ''),
(114, 9, 0, 'Belarusian', '', 'pkg_be-BY', 'package', '', 0, '3.2.1.1', '', 'https://update.joomla.org/language/details3/be-BY_details.xml', '', '');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_update_sites`
--

CREATE TABLE `sorin_update_sites` (
  `update_site_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Update Sites';

--
-- Salvarea datelor din tabel `sorin_update_sites`
--

INSERT INTO `sorin_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'https://update.joomla.org/core/list.xml', 1, 1490858982, ''),
(2, 'Joomla! Extension Directory', 'collection', 'https://update.joomla.org/jed/list.xml', 1, 1490858982, ''),
(3, 'Accredited Joomla! Translations', 'collection', 'https://update.joomla.org/language/translationlist_3.xml', 1, 1490858985, ''),
(4, 'Joomla! Update Component Update Site', 'extension', 'https://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 1490858985, ''),
(5, 'RocketTheme Update Directory', 'collection', 'http://updates.rockettheme.com/joomla/updates.xml', 1, 1490858986, ''),
(6, 'Gantry 5', 'extension', 'http://updates.gantry.org/5.0/joomla/pkg_gantry5.xml', 0, 0, ''),
(7, 'Gantry 5', 'collection', 'http://updates.gantry.org/5.0/joomla/list.xml', 1, 1490858987, ''),
(8, 'The PHP Factory Extensions Update', 'collection', 'http://thephpfactory.com/versions/update-c.xml', 1, 1490858989, ''),
(9, 'Accredited Joomla! Translations', 'collection', 'http://update.joomla.org/language/translationlist_3.xml', 1, 1490858991, '');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_update_sites_extensions`
--

CREATE TABLE `sorin_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';

--
-- Salvarea datelor din tabel `sorin_update_sites_extensions`
--

INSERT INTO `sorin_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 700),
(3, 802),
(4, 28),
(5, 713),
(5, 715),
(5, 717),
(5, 719),
(5, 730),
(5, 732),
(5, 852),
(6, 735),
(7, 735),
(8, 856),
(8, 857),
(8, 858),
(8, 859),
(8, 860),
(8, 861),
(8, 862),
(8, 863),
(8, 864),
(8, 865),
(9, 876);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_usergroups`
--

CREATE TABLE `sorin_usergroups` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_usergroups`
--

INSERT INTO `sorin_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 22, 'Public'),
(2, 1, 8, 19, 'Registered'),
(3, 2, 9, 14, 'Author'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 20, 21, 'Super Users'),
(9, 1, 2, 3, 'Guest'),
(10, 2, 17, 18, 'Companies'),
(11, 2, 15, 16, 'Candidates');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_users`
--

CREATE TABLE `sorin_users` (
  `id` int(11) NOT NULL,
  `name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_users`
--

INSERT INTO `sorin_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(631, 'Super User', 'sorin', 'sorin@yahoo.com', '$2y$10$NrA1XYQjYmqRRhBnNSe2d.jk0CgyuNci65tPCcg43NT56yWZKu6xu', 0, 1, '2017-03-07 11:39:15', '2017-03-30 07:29:35', '0', '{}', '0000-00-00 00:00:00', 0, '', '', 0),
(632, 'Company1', 'Company1', 'company1@yahoo.com', '$2y$10$tcN0W0hsm8.G4UNRez9SuOV3MWxVUPS199lzKn8E38xHGZl.XhTXG', 0, 0, '2017-03-08 09:42:00', '2017-03-28 08:39:07', '', '{}', '0000-00-00 00:00:00', 0, '', '', 0),
(633, 'suzie', 'suzie', 'suzie@yahoo.com', '$2y$10$4j/5.5axg10BnF7SBVKdhenpH8Y4IjZegFkCRHHepr2Il8F1M.jXO', 0, 0, '2017-03-16 08:58:27', '2017-03-17 10:07:35', '', '{}', '0000-00-00 00:00:00', 0, '', '', 0),
(634, 'srl', 'Company2', 'Company2@yahoo.com', '$2y$10$/k7dfRLMBYwXaRnG9KUJWuRuWxYVA5ustdjXnlw1NdpZLg30GpVbu', 0, 0, '2017-03-16 09:10:46', '2017-03-16 13:48:24', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_user_keys`
--

CREATE TABLE `sorin_user_keys` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `series` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uastring` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_user_keys`
--

INSERT INTO `sorin_user_keys` (`id`, `user_id`, `token`, `series`, `invalid`, `time`, `uastring`) VALUES
(1, 'sorin', '$2y$10$6xlrrxmpUN1tKcJeNSMsC.wm/VFXsfJsDWWd0vaQH8K7jbPIo1rne', 'q3XF46CZqYArSwaiGXv2', 0, '1495965495', 'joomla_remember_me_7fd41864fb234c986deb10b909b81823');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_user_notes`
--

CREATE TABLE `sorin_user_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_user_profiles`
--

CREATE TABLE `sorin_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_user_usergroup_map`
--

CREATE TABLE `sorin_user_usergroup_map` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_user_usergroup_map`
--

INSERT INTO `sorin_user_usergroup_map` (`user_id`, `group_id`) VALUES
(631, 8),
(631, 11),
(632, 2),
(632, 10),
(633, 2),
(633, 11),
(634, 2),
(634, 10);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_utf8_conversion`
--

CREATE TABLE `sorin_utf8_conversion` (
  `converted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_utf8_conversion`
--

INSERT INTO `sorin_utf8_conversion` (`converted`) VALUES
(2);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `sorin_viewlevels`
--

CREATE TABLE `sorin_viewlevels` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Salvarea datelor din tabel `sorin_viewlevels`
--

INSERT INTO `sorin_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[6,2,8]'),
(3, 'Special', 3, '[6,3,8]'),
(5, 'Guest', 1, '[9]'),
(6, 'Super Users', 4, '[8]'),
(7, 'Candidates', 0, '[11]'),
(8, 'Companies', 0, '[10]');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sorin_advertisementfactory_ads`
--
ALTER TABLE `sorin_advertisementfactory_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_advertisementfactory_gateways`
--
ALTER TABLE `sorin_advertisementfactory_gateways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_advertisementfactory_messages`
--
ALTER TABLE `sorin_advertisementfactory_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_advertisementfactory_notifications`
--
ALTER TABLE `sorin_advertisementfactory_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_advertisementfactory_orders`
--
ALTER TABLE `sorin_advertisementfactory_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_advertisementfactory_payments`
--
ALTER TABLE `sorin_advertisementfactory_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_advertisementfactory_prices`
--
ALTER TABLE `sorin_advertisementfactory_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_advertisementfactory_track`
--
ALTER TABLE `sorin_advertisementfactory_track`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_ad_id` (`ad_id`),
  ADD KEY `idx_user_id` (`user_id`),
  ADD KEY `idx_ip` (`ip`),
  ADD KEY `idx_type` (`type`);

--
-- Indexes for table `sorin_assets`
--
ALTER TABLE `sorin_assets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_asset_name` (`name`),
  ADD KEY `idx_lft_rgt` (`lft`,`rgt`),
  ADD KEY `idx_parent_id` (`parent_id`);

--
-- Indexes for table `sorin_associations`
--
ALTER TABLE `sorin_associations`
  ADD PRIMARY KEY (`context`,`id`),
  ADD KEY `idx_key` (`key`);

--
-- Indexes for table `sorin_banners`
--
ALTER TABLE `sorin_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100)),
  ADD KEY `idx_banner_catid` (`catid`),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `sorin_banner_clients`
--
ALTER TABLE `sorin_banner_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100));

--
-- Indexes for table `sorin_banner_tracks`
--
ALTER TABLE `sorin_banner_tracks`
  ADD PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  ADD KEY `idx_track_date` (`track_date`),
  ADD KEY `idx_track_type` (`track_type`),
  ADD KEY `idx_banner_id` (`banner_id`);

--
-- Indexes for table `sorin_categories`
--
ALTER TABLE `sorin_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_idx` (`extension`,`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `sorin_contact_details`
--
ALTER TABLE `sorin_contact_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `sorin_content`
--
ALTER TABLE `sorin_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `sorin_contentitem_tag_map`
--
ALTER TABLE `sorin_contentitem_tag_map`
  ADD UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  ADD KEY `idx_tag_type` (`tag_id`,`type_id`),
  ADD KEY `idx_date_id` (`tag_date`,`tag_id`),
  ADD KEY `idx_tag` (`tag_id`),
  ADD KEY `idx_type` (`type_id`),
  ADD KEY `idx_core_content_id` (`core_content_id`);

--
-- Indexes for table `sorin_content_frontpage`
--
ALTER TABLE `sorin_content_frontpage`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `sorin_content_rating`
--
ALTER TABLE `sorin_content_rating`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `sorin_content_types`
--
ALTER TABLE `sorin_content_types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `idx_alias` (`type_alias`);

--
-- Indexes for table `sorin_extensions`
--
ALTER TABLE `sorin_extensions`
  ADD PRIMARY KEY (`extension_id`),
  ADD KEY `element_clientid` (`element`,`client_id`),
  ADD KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  ADD KEY `extension` (`type`,`element`,`folder`,`client_id`);

--
-- Indexes for table `sorin_finder_filters`
--
ALTER TABLE `sorin_finder_filters`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `sorin_finder_links`
--
ALTER TABLE `sorin_finder_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `idx_type` (`type_id`),
  ADD KEY `idx_title` (`title`(100)),
  ADD KEY `idx_md5` (`md5sum`),
  ADD KEY `idx_url` (`url`(75)),
  ADD KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  ADD KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`);

--
-- Indexes for table `sorin_finder_links_terms0`
--
ALTER TABLE `sorin_finder_links_terms0`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_terms1`
--
ALTER TABLE `sorin_finder_links_terms1`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_terms2`
--
ALTER TABLE `sorin_finder_links_terms2`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_terms3`
--
ALTER TABLE `sorin_finder_links_terms3`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_terms4`
--
ALTER TABLE `sorin_finder_links_terms4`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_terms5`
--
ALTER TABLE `sorin_finder_links_terms5`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_terms6`
--
ALTER TABLE `sorin_finder_links_terms6`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_terms7`
--
ALTER TABLE `sorin_finder_links_terms7`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_terms8`
--
ALTER TABLE `sorin_finder_links_terms8`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_terms9`
--
ALTER TABLE `sorin_finder_links_terms9`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_termsa`
--
ALTER TABLE `sorin_finder_links_termsa`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_termsb`
--
ALTER TABLE `sorin_finder_links_termsb`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_termsc`
--
ALTER TABLE `sorin_finder_links_termsc`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_termsd`
--
ALTER TABLE `sorin_finder_links_termsd`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_termse`
--
ALTER TABLE `sorin_finder_links_termse`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_links_termsf`
--
ALTER TABLE `sorin_finder_links_termsf`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `sorin_finder_taxonomy`
--
ALTER TABLE `sorin_finder_taxonomy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `state` (`state`),
  ADD KEY `ordering` (`ordering`),
  ADD KEY `access` (`access`),
  ADD KEY `idx_parent_published` (`parent_id`,`state`,`access`);

--
-- Indexes for table `sorin_finder_taxonomy_map`
--
ALTER TABLE `sorin_finder_taxonomy_map`
  ADD PRIMARY KEY (`link_id`,`node_id`),
  ADD KEY `link_id` (`link_id`),
  ADD KEY `node_id` (`node_id`);

--
-- Indexes for table `sorin_finder_terms`
--
ALTER TABLE `sorin_finder_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD UNIQUE KEY `idx_term` (`term`),
  ADD KEY `idx_term_phrase` (`term`,`phrase`),
  ADD KEY `idx_stem_phrase` (`stem`,`phrase`),
  ADD KEY `idx_soundex_phrase` (`soundex`,`phrase`);

--
-- Indexes for table `sorin_finder_terms_common`
--
ALTER TABLE `sorin_finder_terms_common`
  ADD KEY `idx_word_lang` (`term`,`language`),
  ADD KEY `idx_lang` (`language`);

--
-- Indexes for table `sorin_finder_tokens`
--
ALTER TABLE `sorin_finder_tokens`
  ADD KEY `idx_word` (`term`),
  ADD KEY `idx_context` (`context`);

--
-- Indexes for table `sorin_finder_tokens_aggregate`
--
ALTER TABLE `sorin_finder_tokens_aggregate`
  ADD KEY `token` (`term`),
  ADD KEY `keyword_id` (`term_id`);

--
-- Indexes for table `sorin_finder_types`
--
ALTER TABLE `sorin_finder_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `sorin_jobsfactory_attachments`
--
ALTER TABLE `sorin_jobsfactory_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobId` (`jobId`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `sorin_jobsfactory_candidates`
--
ALTER TABLE `sorin_jobsfactory_candidates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_jobid` (`job_id`),
  ADD KEY `ix_userid` (`userid`),
  ADD KEY `ixaccept` (`accept`);

--
-- Indexes for table `sorin_jobsfactory_categories`
--
ALTER TABLE `sorin_jobsfactory_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iparent` (`parent`),
  ADD KEY `ihash` (`hash`),
  ADD KEY `istatus` (`status`);

--
-- Indexes for table `sorin_jobsfactory_cities`
--
ALTER TABLE `sorin_jobsfactory_cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixcityname` (`cityname`),
  ADD KEY `istatus` (`status`);

--
-- Indexes for table `sorin_jobsfactory_companies_categories`
--
ALTER TABLE `sorin_jobsfactory_companies_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `cid` (`cid`);

--
-- Indexes for table `sorin_jobsfactory_country`
--
ALTER TABLE `sorin_jobsfactory_country`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iname` (`name`),
  ADD KEY `iactive` (`active`);

--
-- Indexes for table `sorin_jobsfactory_currency`
--
ALTER TABLE `sorin_jobsfactory_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_jobsfactory_education`
--
ALTER TABLE `sorin_jobsfactory_education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_userid` (`userid`);

--
-- Indexes for table `sorin_jobsfactory_experiencelevel`
--
ALTER TABLE `sorin_jobsfactory_experiencelevel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_jobsfactory_fields`
--
ALTER TABLE `sorin_jobsfactory_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `istatus` (`status`);

--
-- Indexes for table `sorin_jobsfactory_fields_assoc`
--
ALTER TABLE `sorin_jobsfactory_fields_assoc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_jobsfactory_fields_categories`
--
ALTER TABLE `sorin_jobsfactory_fields_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fid` (`fid`),
  ADD KEY `cid` (`cid`);

--
-- Indexes for table `sorin_jobsfactory_fields_options`
--
ALTER TABLE `sorin_jobsfactory_fields_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixfid` (`fid`),
  ADD KEY `ixordering` (`ordering`);

--
-- Indexes for table `sorin_jobsfactory_fields_positions`
--
ALTER TABLE `sorin_jobsfactory_fields_positions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixfieldid` (`fieldid`),
  ADD KEY `ixpage` (`templatepage`),
  ADD KEY `ixpageposition` (`templatepage`,`position`);

--
-- Indexes for table `sorin_jobsfactory_jobs`
--
ALTER TABLE `sorin_jobsfactory_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iuserid` (`userid`),
  ADD KEY `ititle` (`title`),
  ADD KEY `icat` (`cat`),
  ADD KEY `idate1` (`start_date`),
  ADD KEY `idate2` (`end_date`),
  ADD KEY `ipublished` (`published`),
  ADD KEY `icloseoffer` (`close_offer`),
  ADD KEY `icloseadmin` (`close_by_admin`),
  ADD KEY `ijobcityid` (`job_cityid`);

--
-- Indexes for table `sorin_jobsfactory_jobtype`
--
ALTER TABLE `sorin_jobsfactory_jobtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_jobsfactory_locations`
--
ALTER TABLE `sorin_jobsfactory_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ihash` (`hash`),
  ADD KEY `istatus` (`status`),
  ADD KEY `icountry` (`country`);

--
-- Indexes for table `sorin_jobsfactory_log`
--
ALTER TABLE `sorin_jobsfactory_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixeventtype` (`event`),
  ADD KEY `ixdate` (`logtime`);

--
-- Indexes for table `sorin_jobsfactory_mails`
--
ALTER TABLE `sorin_jobsfactory_mails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixmailtype` (`mail_type`);

--
-- Indexes for table `sorin_jobsfactory_messages`
--
ALTER TABLE `sorin_jobsfactory_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_id` (`job_id`),
  ADD KEY `iuserid1` (`userid1`),
  ADD KEY `iuserid2` (`userid2`),
  ADD KEY `iparent` (`parent_message`),
  ADD KEY `iapp` (`app_id`);

--
-- Indexes for table `sorin_jobsfactory_payment_balance`
--
ALTER TABLE `sorin_jobsfactory_payment_balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixuserid` (`userid`);

--
-- Indexes for table `sorin_jobsfactory_payment_log`
--
ALTER TABLE `sorin_jobsfactory_payment_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixdate` (`date`),
  ADD KEY `ixuserid` (`userid`),
  ADD KEY `ixstatus` (`status`),
  ADD KEY `ixref` (`refnumber`),
  ADD KEY `ixinvoice` (`invoice`),
  ADD KEY `ixobjectid` (`orderid`);

--
-- Indexes for table `sorin_jobsfactory_payment_orderitems`
--
ALTER TABLE `sorin_jobsfactory_payment_orderitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixorderid` (`orderid`),
  ADD KEY `ixiteminfo` (`iteminfo`),
  ADD KEY `ixitemname` (`itemname`);

--
-- Indexes for table `sorin_jobsfactory_payment_orders`
--
ALTER TABLE `sorin_jobsfactory_payment_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_jobsfactory_paysystems`
--
ALTER TABLE `sorin_jobsfactory_paysystems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_jobsfactory_pricing`
--
ALTER TABLE `sorin_jobsfactory_pricing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixitemname` (`itemname`);

--
-- Indexes for table `sorin_jobsfactory_pricing_categories`
--
ALTER TABLE `sorin_jobsfactory_pricing_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `icat` (`category`),
  ADD KEY `ipriceitem` (`itemname`);

--
-- Indexes for table `sorin_jobsfactory_pricing_comissions`
--
ALTER TABLE `sorin_jobsfactory_pricing_comissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixuserid` (`userid`),
  ADD KEY `ixjobid` (`job_id`),
  ADD KEY `ixappid` (`app_id`);

--
-- Indexes for table `sorin_jobsfactory_pricing_contacts`
--
ALTER TABLE `sorin_jobsfactory_pricing_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixuserid` (`userid`),
  ADD KEY `ixcontactid` (`contact_id`);

--
-- Indexes for table `sorin_jobsfactory_ratings`
--
ALTER TABLE `sorin_jobsfactory_ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `rated_userid` (`rated_userid`);

--
-- Indexes for table `sorin_jobsfactory_report_jobs`
--
ALTER TABLE `sorin_jobsfactory_report_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_id` (`job_id`),
  ADD KEY `ixuserid` (`userid`);

--
-- Indexes for table `sorin_jobsfactory_studieslevel`
--
ALTER TABLE `sorin_jobsfactory_studieslevel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_jobsfactory_tags`
--
ALTER TABLE `sorin_jobsfactory_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixjobs` (`job_id`),
  ADD KEY `ixtags` (`tagname`);

--
-- Indexes for table `sorin_jobsfactory_userexperience`
--
ALTER TABLE `sorin_jobsfactory_userexperience`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixtags` (`company`),
  ADD KEY `ixjobs` (`userid`);

--
-- Indexes for table `sorin_jobsfactory_users`
--
ALTER TABLE `sorin_jobsfactory_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `ixname` (`name`),
  ADD KEY `ixsurname` (`surname`),
  ADD KEY `ixcity` (`city`),
  ADD KEY `ixcountry` (`country`);

--
-- Indexes for table `sorin_jobsfactory_users_rating`
--
ALTER TABLE `sorin_jobsfactory_users_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `sorin_jobsfactory_watchlist`
--
ALTER TABLE `sorin_jobsfactory_watchlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixjobid` (`job_id`),
  ADD KEY `ixuserid` (`userid`);

--
-- Indexes for table `sorin_jobsfactory_watchlist_cats`
--
ALTER TABLE `sorin_jobsfactory_watchlist_cats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixcatid` (`catid`),
  ADD KEY `ixuserid` (`userid`);

--
-- Indexes for table `sorin_jobsfactory_watchlist_city`
--
ALTER TABLE `sorin_jobsfactory_watchlist_city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixcatid` (`cityid`),
  ADD KEY `ixuserid` (`userid`);

--
-- Indexes for table `sorin_jobsfactory_watchlist_companies`
--
ALTER TABLE `sorin_jobsfactory_watchlist_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixcatid` (`companyid`),
  ADD KEY `ixuserid` (`userid`);

--
-- Indexes for table `sorin_jobsfactory_watchlist_loc`
--
ALTER TABLE `sorin_jobsfactory_watchlist_loc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ixcatid` (`locationid`),
  ADD KEY `ixuserid` (`userid`);

--
-- Indexes for table `sorin_languages`
--
ALTER TABLE `sorin_languages`
  ADD PRIMARY KEY (`lang_id`),
  ADD UNIQUE KEY `idx_sef` (`sef`),
  ADD UNIQUE KEY `idx_image` (`image`),
  ADD UNIQUE KEY `idx_langcode` (`lang_code`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `sorin_menu`
--
ALTER TABLE `sorin_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`),
  ADD KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  ADD KEY `idx_menutype` (`menutype`),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`),
  ADD KEY `idx_path` (`path`(255)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `sorin_menu_types`
--
ALTER TABLE `sorin_menu_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_menutype` (`menutype`);

--
-- Indexes for table `sorin_messages`
--
ALTER TABLE `sorin_messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `useridto_state` (`user_id_to`,`state`);

--
-- Indexes for table `sorin_messages_cfg`
--
ALTER TABLE `sorin_messages_cfg`
  ADD UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`);

--
-- Indexes for table `sorin_modules`
--
ALTER TABLE `sorin_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `published` (`published`,`access`),
  ADD KEY `newsfeeds` (`module`,`published`),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `sorin_modules_menu`
--
ALTER TABLE `sorin_modules_menu`
  ADD PRIMARY KEY (`moduleid`,`menuid`);

--
-- Indexes for table `sorin_newsfeeds`
--
ALTER TABLE `sorin_newsfeeds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `sorin_offlajn_forms`
--
ALTER TABLE `sorin_offlajn_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_offlajn_oauths`
--
ALTER TABLE `sorin_offlajn_oauths`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_offlajn_users`
--
ALTER TABLE `sorin_offlajn_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `sorin_overrider`
--
ALTER TABLE `sorin_overrider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_postinstall_messages`
--
ALTER TABLE `sorin_postinstall_messages`
  ADD PRIMARY KEY (`postinstall_message_id`);

--
-- Indexes for table `sorin_redirect_links`
--
ALTER TABLE `sorin_redirect_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_old_url` (`old_url`(100)),
  ADD KEY `idx_link_modifed` (`modified_date`);

--
-- Indexes for table `sorin_rokcommon_configs`
--
ALTER TABLE `sorin_rokcommon_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sorin_roksprocket_items`
--
ALTER TABLE `sorin_roksprocket_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_module` (`module_id`),
  ADD KEY `idx_module_order` (`module_id`,`order`);

--
-- Indexes for table `sorin_schemas`
--
ALTER TABLE `sorin_schemas`
  ADD PRIMARY KEY (`extension_id`,`version_id`);

--
-- Indexes for table `sorin_session`
--
ALTER TABLE `sorin_session`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `time` (`time`);

--
-- Indexes for table `sorin_tags`
--
ALTER TABLE `sorin_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_idx` (`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `sorin_template_styles`
--
ALTER TABLE `sorin_template_styles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_template` (`template`),
  ADD KEY `idx_home` (`home`);

--
-- Indexes for table `sorin_ucm_base`
--
ALTER TABLE `sorin_ucm_base`
  ADD PRIMARY KEY (`ucm_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_item_id`),
  ADD KEY `idx_ucm_type_id` (`ucm_type_id`),
  ADD KEY `idx_ucm_language_id` (`ucm_language_id`);

--
-- Indexes for table `sorin_ucm_content`
--
ALTER TABLE `sorin_ucm_content`
  ADD PRIMARY KEY (`core_content_id`),
  ADD KEY `tag_idx` (`core_state`,`core_access`),
  ADD KEY `idx_access` (`core_access`),
  ADD KEY `idx_alias` (`core_alias`(100)),
  ADD KEY `idx_language` (`core_language`),
  ADD KEY `idx_title` (`core_title`(100)),
  ADD KEY `idx_modified_time` (`core_modified_time`),
  ADD KEY `idx_created_time` (`core_created_time`),
  ADD KEY `idx_content_type` (`core_type_alias`(100)),
  ADD KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  ADD KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  ADD KEY `idx_core_created_user_id` (`core_created_user_id`),
  ADD KEY `idx_core_type_id` (`core_type_id`);

--
-- Indexes for table `sorin_ucm_history`
--
ALTER TABLE `sorin_ucm_history`
  ADD PRIMARY KEY (`version_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  ADD KEY `idx_save_date` (`save_date`);

--
-- Indexes for table `sorin_updates`
--
ALTER TABLE `sorin_updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Indexes for table `sorin_update_sites`
--
ALTER TABLE `sorin_update_sites`
  ADD PRIMARY KEY (`update_site_id`);

--
-- Indexes for table `sorin_update_sites_extensions`
--
ALTER TABLE `sorin_update_sites_extensions`
  ADD PRIMARY KEY (`update_site_id`,`extension_id`);

--
-- Indexes for table `sorin_usergroups`
--
ALTER TABLE `sorin_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  ADD KEY `idx_usergroup_title_lookup` (`title`),
  ADD KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  ADD KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE;

--
-- Indexes for table `sorin_users`
--
ALTER TABLE `sorin_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_name` (`name`(100)),
  ADD KEY `idx_block` (`block`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `sorin_user_keys`
--
ALTER TABLE `sorin_user_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `series` (`series`),
  ADD UNIQUE KEY `series_2` (`series`),
  ADD UNIQUE KEY `series_3` (`series`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `sorin_user_notes`
--
ALTER TABLE `sorin_user_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`),
  ADD KEY `idx_category_id` (`catid`);

--
-- Indexes for table `sorin_user_profiles`
--
ALTER TABLE `sorin_user_profiles`
  ADD UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`);

--
-- Indexes for table `sorin_user_usergroup_map`
--
ALTER TABLE `sorin_user_usergroup_map`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `sorin_viewlevels`
--
ALTER TABLE `sorin_viewlevels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_assetgroup_title_lookup` (`title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sorin_advertisementfactory_ads`
--
ALTER TABLE `sorin_advertisementfactory_ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_advertisementfactory_gateways`
--
ALTER TABLE `sorin_advertisementfactory_gateways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_advertisementfactory_messages`
--
ALTER TABLE `sorin_advertisementfactory_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_advertisementfactory_notifications`
--
ALTER TABLE `sorin_advertisementfactory_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_advertisementfactory_orders`
--
ALTER TABLE `sorin_advertisementfactory_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_advertisementfactory_payments`
--
ALTER TABLE `sorin_advertisementfactory_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_advertisementfactory_prices`
--
ALTER TABLE `sorin_advertisementfactory_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_advertisementfactory_track`
--
ALTER TABLE `sorin_advertisementfactory_track`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_assets`
--
ALTER TABLE `sorin_assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT for table `sorin_banners`
--
ALTER TABLE `sorin_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_banner_clients`
--
ALTER TABLE `sorin_banner_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_categories`
--
ALTER TABLE `sorin_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sorin_contact_details`
--
ALTER TABLE `sorin_contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sorin_content`
--
ALTER TABLE `sorin_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sorin_content_types`
--
ALTER TABLE `sorin_content_types`
  MODIFY `type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `sorin_extensions`
--
ALTER TABLE `sorin_extensions`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=877;
--
-- AUTO_INCREMENT for table `sorin_finder_filters`
--
ALTER TABLE `sorin_finder_filters`
  MODIFY `filter_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_finder_links`
--
ALTER TABLE `sorin_finder_links`
  MODIFY `link_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_finder_taxonomy`
--
ALTER TABLE `sorin_finder_taxonomy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sorin_finder_terms`
--
ALTER TABLE `sorin_finder_terms`
  MODIFY `term_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_finder_types`
--
ALTER TABLE `sorin_finder_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_attachments`
--
ALTER TABLE `sorin_jobsfactory_attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_candidates`
--
ALTER TABLE `sorin_jobsfactory_candidates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_categories`
--
ALTER TABLE `sorin_jobsfactory_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_cities`
--
ALTER TABLE `sorin_jobsfactory_cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_companies_categories`
--
ALTER TABLE `sorin_jobsfactory_companies_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_country`
--
ALTER TABLE `sorin_jobsfactory_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_currency`
--
ALTER TABLE `sorin_jobsfactory_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_education`
--
ALTER TABLE `sorin_jobsfactory_education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_experiencelevel`
--
ALTER TABLE `sorin_jobsfactory_experiencelevel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_fields`
--
ALTER TABLE `sorin_jobsfactory_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_fields_assoc`
--
ALTER TABLE `sorin_jobsfactory_fields_assoc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_fields_categories`
--
ALTER TABLE `sorin_jobsfactory_fields_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_fields_options`
--
ALTER TABLE `sorin_jobsfactory_fields_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_fields_positions`
--
ALTER TABLE `sorin_jobsfactory_fields_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_jobs`
--
ALTER TABLE `sorin_jobsfactory_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_jobtype`
--
ALTER TABLE `sorin_jobsfactory_jobtype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_locations`
--
ALTER TABLE `sorin_jobsfactory_locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_log`
--
ALTER TABLE `sorin_jobsfactory_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_mails`
--
ALTER TABLE `sorin_jobsfactory_mails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_messages`
--
ALTER TABLE `sorin_jobsfactory_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_payment_balance`
--
ALTER TABLE `sorin_jobsfactory_payment_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_payment_log`
--
ALTER TABLE `sorin_jobsfactory_payment_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_payment_orderitems`
--
ALTER TABLE `sorin_jobsfactory_payment_orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_payment_orders`
--
ALTER TABLE `sorin_jobsfactory_payment_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_paysystems`
--
ALTER TABLE `sorin_jobsfactory_paysystems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_pricing`
--
ALTER TABLE `sorin_jobsfactory_pricing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_pricing_categories`
--
ALTER TABLE `sorin_jobsfactory_pricing_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_pricing_comissions`
--
ALTER TABLE `sorin_jobsfactory_pricing_comissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_pricing_contacts`
--
ALTER TABLE `sorin_jobsfactory_pricing_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_ratings`
--
ALTER TABLE `sorin_jobsfactory_ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_report_jobs`
--
ALTER TABLE `sorin_jobsfactory_report_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_studieslevel`
--
ALTER TABLE `sorin_jobsfactory_studieslevel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_tags`
--
ALTER TABLE `sorin_jobsfactory_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_userexperience`
--
ALTER TABLE `sorin_jobsfactory_userexperience`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_users`
--
ALTER TABLE `sorin_jobsfactory_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_users_rating`
--
ALTER TABLE `sorin_jobsfactory_users_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_watchlist`
--
ALTER TABLE `sorin_jobsfactory_watchlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_watchlist_cats`
--
ALTER TABLE `sorin_jobsfactory_watchlist_cats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_watchlist_city`
--
ALTER TABLE `sorin_jobsfactory_watchlist_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_watchlist_companies`
--
ALTER TABLE `sorin_jobsfactory_watchlist_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_jobsfactory_watchlist_loc`
--
ALTER TABLE `sorin_jobsfactory_watchlist_loc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_languages`
--
ALTER TABLE `sorin_languages`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sorin_menu`
--
ALTER TABLE `sorin_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=549;
--
-- AUTO_INCREMENT for table `sorin_menu_types`
--
ALTER TABLE `sorin_menu_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `sorin_messages`
--
ALTER TABLE `sorin_messages`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sorin_modules`
--
ALTER TABLE `sorin_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `sorin_newsfeeds`
--
ALTER TABLE `sorin_newsfeeds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_offlajn_forms`
--
ALTER TABLE `sorin_offlajn_forms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sorin_offlajn_oauths`
--
ALTER TABLE `sorin_offlajn_oauths`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sorin_overrider`
--
ALTER TABLE `sorin_overrider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `sorin_postinstall_messages`
--
ALTER TABLE `sorin_postinstall_messages`
  MODIFY `postinstall_message_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sorin_redirect_links`
--
ALTER TABLE `sorin_redirect_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_rokcommon_configs`
--
ALTER TABLE `sorin_rokcommon_configs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sorin_roksprocket_items`
--
ALTER TABLE `sorin_roksprocket_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=471;
--
-- AUTO_INCREMENT for table `sorin_tags`
--
ALTER TABLE `sorin_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sorin_template_styles`
--
ALTER TABLE `sorin_template_styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `sorin_ucm_content`
--
ALTER TABLE `sorin_ucm_content`
  MODIFY `core_content_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_ucm_history`
--
ALTER TABLE `sorin_ucm_history`
  MODIFY `version_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sorin_updates`
--
ALTER TABLE `sorin_updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `sorin_update_sites`
--
ALTER TABLE `sorin_update_sites`
  MODIFY `update_site_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sorin_usergroups`
--
ALTER TABLE `sorin_usergroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sorin_users`
--
ALTER TABLE `sorin_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=635;
--
-- AUTO_INCREMENT for table `sorin_user_keys`
--
ALTER TABLE `sorin_user_keys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sorin_user_notes`
--
ALTER TABLE `sorin_user_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sorin_viewlevels`
--
ALTER TABLE `sorin_viewlevels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
