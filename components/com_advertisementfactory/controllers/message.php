<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class AdvertisementFactoryControllerMessage extends JControllerLegacy
{
    public function send()
    {
        $id = $this->input->getInt('id');
        $message = $this->input->getString('message');
        $model = $this->getModel('Message');

        if ($model->send($message, $id)) {
            $msg = JText::_('COM_ADVERTISEMENTFACTORY_MESSAGE_SEND_SUCCESS');
        } else {
            $msg = JText::_('COM_ADVERTISEMENTFACTORY_MESSAGE_SEND_ERROR');
            throw new Exception($model->getError(), 500);
        }

        $this->setRedirect(JRoute::_('index.php?option=com_advertisementfactory&task=ad.edit&id=' . $id, false), $msg);
    }
}
