<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

class AdvertisementFactoryControllerAd extends JControllerForm
{
    public function save($key = null, $urlVar = null)
    {
        $lang = JFactory::getLanguage();
        $lang->load('com_advertisementfactory', JPATH_ADMINISTRATOR);

        $this->prepareData();

        $result = parent::save($key, $urlVar);

        if ($result && $this->isModal() && 'save' == $this->getTask()) {
            $price_id = $this->input->getInt('price_id');
            $this->setRedirect('index.php?option=com_advertisementfactory&view=close&tmpl=component&ad_id=' . $this->ad_id . '&price_id=' . $price_id);
            return true;
        }

        return $result;
    }

    protected function postSaveHook(JModelLegacy &$model, $validData = array())
    {
        $this->ad_id = $model->getState('ad.id');
    }

    public function cancel($key = null)
    {
        $result = parent::cancel($key);

        if ($result && $this->isModal()) {
            $this->setRedirect('index.php?option=com_advertisementfactory&view=close&tmpl=component');
            return true;
        }

        return $result;
    }

    public function purchase()
    {
        $id = $this->input->getInt('id');
        $price_id = $this->input->getInt('price_id');
        $model = $this->getModel();

        if (!$model->validateForPurchase($id, $price_id)) {
            throw new Exception($model->getError(), 500);
            $link = 'index.php?option=com_advertisementfactory&view=purchasetypes';
        } else {
            $app = JFactory::getApplication();
            $context = "$this->option.purchase.$this->context";

            $app->setUserState($context . '.data', array('ad_id' => $id, 'price_id' => $price_id));

            $link = 'index.php?option=com_advertisementfactory&view=purchaseform';
        }

        $this->setRedirect(JRoute::_($link, false));

        return true;
    }

    public function preview()
    {
        $form = $this->input->get('jform', array(), 'array');
        $module_id = isset($form['module_id']) ? (int)$form['module_id'] : 0;
        $ad_id = $this->input->getInt('id');

        $link = 'index.php?option=com_advertisementfactory&view=previsualize&ad_id=' . $ad_id;

        $link .= $module_id ? '&module_id=' . $module_id : '';

        $this->setRedirect(JRoute::_($link, false));

        return true;
    }

    public function url()
    {
        JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'tables');

        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $id = JFactory::getApplication()->input->getInt('id');
        $ad = JTable::getInstance('Ad', 'AdvertisementFactoryTable');

        // Load ad
        $ad->load($id);

        // Ad not found
        if (!$ad->id) {
            throw new Exception(JText::_('COM_ADVERTISEMENTFACTORY_AD_NOT_FOUND'));
            $app->redirect('index.php');

            return false;
        }

        $preview = JFactory::getApplication()->input->getInt('preview');

        if ($ad->state != 1 && ($user->id != $ad->user_id && !$user->authorise('backend.manage', 'com_advertisementfactory'))) {
            throw new Exception(JText::_('COM_ADVERTISEMENTFACTORY_AD_NOT_PUBLISHED'));
            $app->redirect('index.php');

            return false;
        }

        if (2 == $ad->price_type && !$preview) {
            $ad->hit('click');
        }

        $app->redirect($ad->url);

        return true;
    }

    public function updateForm()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Initialise variables.
        $app = JFactory::getApplication();
        $lang = JFactory::getLanguage();
        $model = $this->getModel();
        $table = $model->getTable();
        $data = JFactory::getApplication()->input->get('jform', array(), 'array');
        $checkin = property_exists($table, 'checked_out');
        $context = "$this->option.edit.$this->context";
        $task = $this->getTask();

        // Determine the name of the primary key for the data.
        if (empty($key)) {
            $key = $table->getKeyName();
        }

        // The urlVar may be different from the primary key to avoid data collisions.
        if (empty($urlVar)) {
            $urlVar = $key;
        }

        $recordId = JFactory::getApplication()->input->getInt($urlVar);

        $session = JFactory::getSession();
        $registry = $session->get('registry');

        // Populate the row id from the session.
        $data[$key] = $recordId;

        // Validate the posted data.
        // Sometimes the form needs some posted data, such as for plugins and modules.
        $form = $model->getForm($data, false);

        if (!$form) {
            $app->enqueueMessage($model->getError(), 'error');

            return false;
        }

        $old_data = $app->getUserState($context . '.data');
        if (!is_null($old_data)) {
            if ($old_data['type'] != $data['type']) {
                $data['module_id'] = '';
            }
        } else {
            $table->load($recordId);

            if ($table->type != $data['type']) {
                $data['module_id'] = '';
            }
        }

        // Save the data in the session.
        $app->setUserState($context . '.data', $data);

        // Redirect back to the edit screen.
        $this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId, $key), false));

        return true;
    }

    public function submitForValidation()
    {
        $id = JFactory::getApplication()->input->getInt('id');
        $model = $this->getModel();

        if (!$model->submitForValidation($id)) {
            throw new Exception($model->getError());

            $msg = JText::_('COM_ADVERTISEMENTFACTORY_MYAD_TASK_SUBMITFORVALIDATION_ERROR');
            $link = 'index.php?option=com_advertisementfactory&view=ad&layout=edit&id=' . $id;
        } else {
            $msg = JText::_('COM_ADVERTISEMENTFACTORY_MYAD_TASK_SUBMITFORVALIDATION_SUCCESS');
            $link = 'index.php?option=com_advertisementfactory&view=ads';
        }

        $this->setRedirect(JRoute::_($link, false), $msg);

        return true;
    }

    protected function prepareData()
    {
        $data = $this->input->post->get('jform', array(), 'array');
        $data['user_id'] = JFactory::getUser()->id;

        $this->input->post->set('jform', $data);
    }

    protected function allowAdd($data = array())
    {
        if (empty($data)) {
            return true;
        }

        // If user is super admin or has access to backend.manage, allow him
        if (JFactory::getUser()->authorise('backend.manage', 'com_advertisementfactory') ||
            JFactory::getUser()->authorise('core.admin')
        ) {
            return true;
        }

        $component = JComponentHelper::getComponent('com_advertisementfactory');
        $user = JFactory::getUser();

        // Check if type is enabled and available
        $ad_types = $component->params->get('enabled_ad_types', array());

        if (!in_array($data['type'], $ad_types)) {
            throw new Exception('Ad type not allowed!');
            return false;
        }

        return true;
    }

    protected function allowEdit($data = array(), $key = 'id')
    {
        $user = JFactory::getUser();
        $model = $this->getModel();
        $table = $model->getTable();

        $table->load($data[$key]);

        if ($table->user_id != $user->id) {
            throw new Exception('Ad not found!', 404);
            return false;
        }

        if ($table->state == 1 && !JAccess::check($user->id, 'backend.manage', 'com_advertisementfactory')) {
            throw new Exception('Ad already published!');
            return false;
        }

        return true;
    }

    protected function isModal()
    {
        $tmpl = JFactory::getApplication()->input->getWord('tmpl');

        return $tmpl == 'component';
    }
}
