<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

class AdvertisementFactoryControllerAds extends JControllerAdmin
{
    public function filter()
    {
        $model = $this->getModel();
        $model->getState();

        $this->setRedirect(JRoute::_('index.php?option=com_advertisementfactory&view=ads', false));
    }

    public function getModel($name = 'Ad', $prefix = 'AdvertisementFactoryModel', $config = array('ignore_request' => true))
    {
        $model = parent::getModel($name, $prefix, $config);

        return $model;
    }
}
