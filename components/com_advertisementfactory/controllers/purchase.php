<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class AdvertisementFactoryControllerPurchase extends JControllerLegacy
{
    public function selectgateway()
    {
        $method = $this->input->getCmd('method');
        $model = JModelLegacy::getInstance('PaymentProxy', 'AdvertisementFactoryModel');

        if (!$model->validateGateway($method)) {
            throw new Exception(JText::_('COM_ADVERTISEMENTFACTORY_PURCHASE_TASK_SELECTGATEWAY_NOT_FOUND'), 404);
            $link = 'index.php?option=com_advertisementfactory&view=purchaseform';
        } else {
            $app = JFactory::getApplication();
            $context = 'com_advertisementfactory.purchase.ad';

            $app->setUserState($context . '.method', $method);

            $link = 'index.php?option=com_advertisementfactory&task=purchase.gateway';
        }

        $this->setRedirect(JRoute::_($link, false));

        return true;
    }

    public function gateway()
    {
        $user = JFactory::getUser();
        $app = JFactory::getApplication();
        $context = 'com_advertisementfactory.purchase.ad';

        $method = $app->getUserState($context . '.method', null);
        $data = $app->getUserState($context . '.data', array());
        $step = $this->input->getInt('step', 1);

        // Validate gateway
        $model = JModelLegacy::getInstance('PaymentProxy', 'AdvertisementFactoryModel');
        $gateway = $model->getGateway($method);

        if (!$gateway || !$gateway->getEnabled()) {
            throw new Exception(JText::_('COM_ADVERTISEMENTFACTORY_PURCHASE_TASK_GATEWAY_NOT_FOUND'), 404);
            return false;
        }

        // Validate price and ad
        if (!isset($data['price_id']) || !isset($data['ad_id'])) {
            throw new Exception(JText::_('COM_ADVERTISEMENTFACTORY_PURCHASE_TASK_GATEWAY_SELECT_AD_AND_PRICE'), 404);
            return false;
        }

        // Validate price
        $price = JTable::getInstance('Price', 'AdvertisementFactoryTable');
        $price->load($data['price_id']);

        if ($price->state != 1) {
            throw new Exception(JText::_('COM_ADVERTISEMENTFACTORY_PURCHASE_TASK_GATEWAY_PRICE_NOT_FOUND'), 404);
            return false;
        }

        // Validate ad
        $ad = JTable::getInstance('Ad', 'AdvertisementFactoryTable');
        $ad->load($data['ad_id']);

        if ($ad->user_id != $user->id) {
            throw new Exception(JText::_('COM_ADVERTISEMENTFACTORY_PURCHASE_TASK_GATEWAY_AD_NOT_FOUND'), 404);
            return false;
        }

        if ($ad->paid == 1) {
            throw new Exception(JText::_('COM_ADVERTISEMENTFACTORY_PURCHASE_TASK_GATEWAY_AD_PAID'));
            return false;
        }

        if ($ad->state == 1) {
            throw new Exception(JText::_('COM_ADVERTISEMENTFACTORY_PURCHASE_TASK_GATEWAY_AD_PUBLISHED'));
            return false;
        }

        // Process gateway step
        $gateway->loadInfo($data);

        $gateway->executeStep($step);

        return true;
    }

    public function notify()
    {
        $method = JFactory::getApplication()->input->getCmd('gateway');
        $model = JModelLegacy::getInstance('PaymentProxy', 'AdvertisementFactoryModel');
        $gateway = $model->getGateway($method);

        $gateway->processIpn();

        jexit();
    }
}
