<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

abstract class JHtmlAdvertisementFactory
{
    static function paid($value = 0, $extra = true, $variables = array())
    {
        $states = array(
            0 => 'disabled.png',      // not paid
            1 => 'featured.png',      // paid
            2 => 'icon-16-clear.png', // refunded
        );

        $html[] = '<img src="' . JURI::root() . 'media/com_advertisementfactory/assets/frontend/images/' . $states[$value] . '" />';

        if ($extra) {
            if (0 == $value) {
                $html[] = '<br /><a href="' . JRoute::_('index.php?option=com_advertisementfactory&view=purchasetypes&ad_id=' . $variables['ad_id']) . '">' . JText::_('COM_ADVERTISEMENTFACTORY_AD_BUY_NOW') . '</a>';
            }
        }

        return implode("\n", $html);
    }

    static function state($value = 0)
    {
        $states = array(
            0 => 'disabled.png',            // unpublished
            1 => 'tick.png',                // published
            2 => 'publish_r.png',           // expired
            3 => 'icon-16-deny.png',        // rejected
            4 => 'icon-16-notice-note.png', // pending
            5 => 'icon-16-apply.png',       // published, but pending
        );

        $html = '<img src="' . JURI::root() . 'media/com_advertisementfactory/assets/frontend/images/' . $states[$value] . '" />';

        return $html;
    }
}
