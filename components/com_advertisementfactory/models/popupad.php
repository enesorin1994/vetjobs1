<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelPopupAd extends JModelLegacy
{
    public function getData()
    {
        $id = $this->getNextAdId();
        $fullPageAd = $this->getTable('Ad', 'AdvertisementFactoryTable');

        $fullPageAd->load($id);

        return $fullPageAd;
    }

    public function getTimeout()
    {
        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        return $component->params->get('popupads_seconds_to_display' . $suffix, 10);
    }

    public function getNextAdId()
    {
        $session = JFactory::getSession();

        return $session->get('advertisementfactory.popup.nextad', 0);
    }
}
