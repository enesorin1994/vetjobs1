<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AdvertisementFactoryModelPurchaseTypes extends JModelList
{
    public function getTypes()
    {
        $dbo = JFactory::getDbo();

        $query = $dbo->getQuery(true)
            ->select('DISTINCT p.type AS value, p.type AS text')
            ->from('#__advertisementfactory_prices p')
            ->leftJoin('#__modules m ON m.id = p.module_id')
            ->where('p.state = 1')
            ->where('IF (m.id IS NOT NULL, m.published, 1) = 1')
            ->order('p.type ASC');
        $dbo->setQuery($query);

        $types = $dbo->loadObjectList();

        foreach ($types as &$type) {
            $type->text = ucfirst($type->text);
        }

        return $types;
    }

    public function getPrices()
    {
        $dbo = JFactory::getDbo();
        $type = JFactory::getApplication()->input->getCmd('type');
        $module_id = JFactory::getApplication()->input->getInt('module_id');

        $ad = $this->getAd();
        if ($ad) {
            $type = $ad->type;
            $module_id = $ad->module_id;
        }

        // Check modals restriction
        $settings = JComponentHelper::getParams('com_advertisementfactory');
        $modal_slots = $settings->get('modalads_slots', 0);
        $restrict_modals = false;

        if ($modal_slots) {
            $query = $dbo->getQuery(true)
                ->select('COUNT(1)')
                ->from('#__advertisementfactory_ads a')
                ->where('a.type = ' . $dbo->quote('modal'))
                ->where('a.state = ' . $dbo->quote(1));
            $result = $dbo->setQuery($query)
                ->loadResult();

            $restrict_modals = $result >= $modal_slots;
        }

        $query = $dbo->getQuery(true)
            ->select('p.*')
            ->from('#__advertisementfactory_prices p')
            ->where('p.state = 1')
            ->order('m.title ASC, p.price ASC');

        if ($restrict_modals) {
            $query->where('p.type <> ' . $dbo->quote('modal'));
        }

        $query->select('m.title AS module_title, m.params')
            ->leftJoin('#__modules m ON m.id = p.module_id')
            ->where('IF (m.id IS NOT NULL, m.published, 1) = 1');

        if ($type != '') {
            $query->where('p.type = ' . $dbo->quote($type));
        }

        if ($module_id) {
            $query->where('m.id = ' . $dbo->quote($module_id));
        }

        $dbo->setQuery($query);

        $array = array();
        $prices = $dbo->loadObjectList();

        foreach ($prices as $price) {
            if (!isset($array[$price->type])) {
                $array[$price->type] = array();
            }

            if (!isset($array[$price->type][$price->module_id])) {
                $array[$price->type][$price->module_id] = array(
                    'title'        => $price->module_title,
                    'ads'          => array(),
                    'availability' => $price->module_id ? $this->getAvailabilityForModule($price->module_id, $price->params) : null);
            }

            $array[$price->type][$price->module_id]['ads'][] = $price;
        }

        return $array;
    }

    public function getCurrency()
    {
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        return $component->params->get('currency');
    }

    public function getShowMore()
    {
        $type = JFactory::getApplication()->input->getCmd('type');
        $module_id = JFactory::getApplication()->input->getInt('module_id');

        return $type != '' || $module_id;
    }

    public function getAvailabilityForModule($module_id, $params)
    {
        $params = new JRegistry($params);
        $type = $params->get('module_type', 1); // 1 - imp, 0 - period
        $noSlots = $params->get('slots', 0); // 1 - imp, 0 - period

        if ($params->get('rows', '') && $params->get('columns', '')) {
            $noSlots = $params->get('rows') * $params->get('columns');
        }

        if (1 == $type) {
            return null;
        }

        $dbo = JFactory::getDbo();
        $query = $dbo->getQuery(true)
            ->select('a.id, a.publish_up, a.publish_down')
            ->from('#__advertisementfactory_ads a')
            ->where('a.module_id = ' . $dbo->quote((int)$module_id))
            ->where('a.state = 1')
            ->order('a.publish_up');
        $dbo->setQuery($query);

        $orders = $dbo->loadObjectList();
        $slots = array();

        if (count($orders) < $noSlots) {
            return null;
        }

        foreach ($orders as $order) {
            $init = false;
            for ($i = 0; $i < $noSlots; $i++) {
                if (!isset($slots[$i])) {
                    $slots[$i] = $order;
                    $init = true;

                    break;
                }
            }

            if ($init) {
                continue;
            }

            for ($i = 0; $i < $noSlots; $i++) {
                if ($order->publish_up == $slots[$i]->publish_down) {
                    $slots[$i] = $order;
                    break;
                }
            }
        }

        $min = null;
        foreach ($slots as $slot) {
            if ($slot->publish_down < $min || is_null($min)) {
                $min = $slot->publish_down;
            }
        }

        return $min;
    }

    public function getAd()
    {
        static $items = array();

        $id = JFactory::getApplication()->input->getInt('ad_id');

        if (!isset($items[$id])) {
            $table = JTable::getInstance('Ad', 'AdvertisementFactoryTable');
            $table->load($id);

            if ($table->user_id != JFactory::getUser()->id) {
                $items[$id] = null;
            } else {
                $items[$id] = $table;
            }
        }

        return $items[$id];
    }
}
