<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'adbase.php');

class AdvertisementFactoryModelAd extends AdvertisementFactoryModelAdBase
{
    public function getTemplate()
    {
        return JFactory::getApplication()->input->getWord('tmpl');
    }

    public function validateForPurchase($ad_id, $price_id)
    {
        // Validate ad
        $ad = $this->getTable();
        $ad->load($ad_id);

        if ($ad->user_id != JFactory::getUser()->id) {
            $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_MYAD_TASK_VALIDATE_AD_NOT_FOUND'));
            return false;
        }

        if ($ad->state == 1) {
            $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_MYAD_TASK_VALIDATE_AD_PUBLISHED'));
            return false;
        }

        // Validate price
        $price = JTable::getInstance('Price', 'AdvertisementFactoryTable');
        $price->load($price_id);

        if ($price->state != 1) {
            $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_MYAD_TASK_VALIDATE_PRICE_NOT_FOUND'));
            return false;
        }

        if ($price->module_id != $ad->module_id) {
            $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_MYAD_TASK_VALIDATE_AD_NOT_AVAILABLE_ZONE'));
            return false;
        }

        // All it's ok
        return true;
    }

    public function submitForValidation($id)
    {
        $user = JFactory::getUser();
        $date = JFactory::getDate();

        $ad = $this->getTable();
        $ad->load($id);

        // Check it's my ad
        if ($ad->user_id != $user->id) {
            $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_MYAD_TASK_SUBMITFORVALIDATION_AD_NOT_FOUND'));
            return false;
        }

        // Check it's rejected
        if ($ad->state != 3) {
            $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_MYAD_TASK_SUBMITFORVALIDATION_ONLY_REJECTED'));
            return false;
        }

        $ad->state = 4;

        return $ad->store();
    }

    public function getMessages()
    {
        $id = JFactory::getApplication()->input->getInt('id');

        $dbo = JFactory::getDbo();
        $query = $dbo->getQuery(true)
            ->select('m.*')
            ->from('#__advertisementfactory_messages m')
            ->where('m.ad_id = ' . $dbo->quote($id))
            ->order('m.created_at DESC');

        $query->select('u.username')
            ->leftJoin('#__users u ON u.id = m.user_id');

        $results = $dbo->setQuery($query)
            ->loadObjectList();

        return $results;
    }

    protected function canDelete($record)
    {
        return $record->user_id = JFactory::getUser()->id;
    }

    protected function preprocessForm(JForm $form, $data, $group = 'content')
    {
        $form = parent::preprocessForm($form, $data, $group);

        $form->removeField('price_type');

        if (isset($this->item) && $this->item->id) {
            $form->setFieldAttribute('type', 'readonly', 'true');
            $form->setFieldAttribute('module_id', 'readonly', 'true');

            return $form;
        }

        $data = (array)$data;

        $type = JFactory::getApplication()->input->getCmd($data['type'] ? $data['type'] : '');
        $module_id = JFactory::getApplication()->input->getInt('module_id', isset($data['module_id']) ? $data['module_id'] : 0);

        if ('' != $type) {
            $form->setFieldAttribute('type', 'readonly', 'true');
        }

        if ($module_id) {
            $form->setFieldAttribute('module_id', 'readonly', 'true');
        }

        return $form;
    }
}
