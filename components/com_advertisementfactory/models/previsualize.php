<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelPrevisualize extends JModelLegacy
{
    public function getArticle()
    {
        $dispatcher = JEventDispatcher::getInstance();
        $content = JTable::getInstance('content');

        $model = JModelLegacy::getInstance('ServeAd', 'AdvertisementFactoryModel');
        $preview = $model->getPreviewAd('keyword');
        $preview = isset($preview[0]) ? $preview[0] : null;

        $text = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas arcu %s, semper ac ultricies at, pretium at sapien. Donec nibh sem, lobortis vitae ultrices et, vestibulum vel quam.</p>'
            . '<p>Sed ullamcorper %s leo eu elementum. Nunc at nulla id tellus interdum dictum. Curabitur semper rhoncus augue congue elementum. Morbi ac purus vel odio tincidunt suscipit sed ut nibh. Ut placerat nibh vitae metus ornare porta.</p>'
            . '<p>Nunc elementum turpis in nibh imperdiet dapibus. Sed justo tortor, mollis sed consequat sit amet, ultrices eget felis. Proin a ante %s, in accumsan enim.</p>'
            . '<p>Quisque commodo lacus eu tellus ultricies %s tristique lorem aliquet.</p>';

        $content->text = str_replace('%s', $preview ? $preview->keywords : 'ipsum', $text);

        JPluginHelper::importPlugin('content');
        $params = array();
        $results = $dispatcher->trigger('onContentPrepare', array('com_content.article', &$content, &$params, 0));

        return $content;
    }
}
