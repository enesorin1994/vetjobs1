<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelHistory extends JModelLegacy
{
    public function getItem()
    {
        $user = JFactory::getUser();
        $id = JFactory::getApplication()->input->getInt('id');
        $ad = JTable::getInstance('Ad', 'AdvertisementFactoryTable');

        $ad->load($id);

        if ($ad->user_id != $user->id) {
            return null;
        }

        $history = array();
        foreach (explode("\n", $ad->history) as $line) {
            $history[] = JText::_($line);
        }

        return implode("\n", $history);
    }
}
