<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AdvertisementFactoryModelAds extends JModelList
{
    public function getItems()
    {
        $items = parent::getItems();

        $price_id = JFactory::getApplication()->input->getInt('price_id');

        foreach ($items as &$item) {
            $item->edit_link = JRoute::_('index.php?option=com_advertisementfactory&task=ad.edit&id=' . $item->id);
            $item->purchase_link = JRoute::_('index.php?option=com_advertisementfactory&task=ad.purchase&id=' . $item->id . '&price_id=' . $price_id);
            $item->preview_link = JRoute::_('index.php?option=com_advertisementfactory&task=ad.preview&id=' . $item->id);

            // Status
            if (3 == $item->price_type) {
                $item->status = JText::sprintf('COM_ADVERTISEMENTFACTORY_MYADS_STATUS_3', $item->publish_up, $item->publish_down);
            } elseif (in_array($item->price_type, array(1, 2))) {
                $item->status = JText::sprintf('COM_ADVERTISEMENTFACTORY_MYADS_STATUS_' . $item->price_type, $item->served, $item->purchased);
            } else {
                $item->status = '-';
            }

            $date = JFactory::getDate();
            if ($item->state == 1 && $item->publish_up > $date->toSql()) {
                $item->state = 5;
            }
        }

        return $items;
    }

    public function getListQuery()
    {
        $dbo = JFactory::getDbo();
        $user = JFactory::getUser();

        $query = $dbo->getQuery(true)
            ->select('a.id, a.name, a.state, a.type, a.module_id, a.history, a.paid')
            ->select('a.price_type, a.publish_up, a.publish_down, a.served, a.purchased')
            ->from('#__advertisementfactory_ads a');

        // Filter by user id
        $query->where('a.user_id = ' . $user->id);

        if (!$select = $this->selectAd()) {
            // Filter by paid status
            $paid = $this->getState('filter.paid', '');
            if ($paid != '') {
                $query->where('a.paid = ' . (int)$paid);
            }

            // Filter by published status
            $published = $this->getState('filter.published', '');
            if ($published != '') {
                $query->where('a.state = ' . (int)$published);
            }

            // Filter by type status
            $type = $this->getState('filter.type', '');
            if ($type != '') {
                $query->where('a.type = ' . $dbo->quote($type));
            }
        } else {
            // Filter by state
            $query->where('a.state <> 1');

            // Filter by type
            $type = JFactory::getApplication()->input->getCmd('type', 'link');
            $query->where('a.type = ' . $dbo->quote($type));

            // Filter by module
            $module_id = JFactory::getApplication()->input->getInt('module_id');
            $query->where('a.module_id = ' . $dbo->quote($module_id));
        }

        return $query;
    }

    public function getFilterPaid()
    {
        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'fields' . DS . 'adpaid.php');
        $field = new JFormFieldAdPaid();
        $options = $field->getOptionsForFilter();

        return JHtml::_('select.genericlist', $options, 'filter_paid', 'onchange="this.form.submit();"', 'value', 'text', $this->getState('filter.paid'));
    }

    public function getFilterPublished()
    {
        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'fields' . DS . 'adstate.php');
        $field = new JFormFieldAdState();
        $options = $field->getOptionsForFilter();

        return JHtml::_('select.genericlist', $options, 'filter_published', 'onchange="this.form.submit();"', 'value', 'text', $this->getState('filter.published'));
    }

    public function getFilterType()
    {
        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'fields' . DS . 'adtype.php');
        $field = new JFormFieldAdType();
        $options = $field->getOptionsForFilter();

        return JHtml::_('select.genericlist', $options, 'filter_type', 'onchange="this.form.submit();"', 'value', 'text', $this->getState('filter.type'));
    }

    public function getType()
    {
        return JFactory::getApplication()->input->getCmd('type');
    }

    public function getModuleId()
    {
        return JFactory::getApplication()->input->getInt('module_id');
    }

    protected function populateState($ordering = 'ordering', $direction = 'ASC')
    {
        $app = JFactory::getApplication();

        // List state information
        $value = JFactory::getApplication()->input->getInt('limit', $app->get('list_limit', 0));
        $this->setState('list.limit', $value);

        $value = JFactory::getApplication()->input->getInt('limitstart', 0);
        $this->setState('list.start', $value);

        // Load the paid state
        $paid = $this->getUserStateFromRequest($this->context . '.filter.paid', 'filter_paid', '', 'string');
        $this->setState('filter.paid', $paid);

        // Load the type state
        $type = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type', '', 'string');
        $this->setState('filter.type', $type);

        // Load the publsihed state
        $published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '', 'string');
        $this->setState('filter.published', $published);
    }

    protected function selectAd()
    {
        $tmpl = JFactory::getApplication()->input->getCmd('tmpl');

        return $tmpl == 'component';
    }
}
