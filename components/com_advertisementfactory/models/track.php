<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AdvertisementFactoryModelTrack extends JModelList
{
    public function getAd()
    {
        static $ads = array();

        $id = JFactory::getApplication()->input->getInt('id');

        if (!isset($ads[$id])) {
            $ads[$id] = false;

            $table = JTable::getInstance('Ad', 'AdvertisementFactoryTable');
            $table->load($id);

            if ($id && $table->load($id)) {
                if (JFactory::getUser()->authorise('core.admin') || $ads[$id]->user_id == JFactory::getUser()->id) {
                    $ads[$id] = $table;
                }
            }
        }

        return $ads[$id];
    }

    protected function getListQuery()
    {
        $query = parent::getListQuery();
        $ad = $this->getAd();
        $id = isset($ad->id) ? $ad->id : 0;

        $query->select('t.*')
            ->from('#__advertisementfactory_track t')
            ->where('t.ad_id = ' . $query->quote($id))
            ->order('t.created_at DESC');

        $query->select('u.username')
            ->leftJoin('#__users u ON u.id = t.user_id');

        return $query;
    }
}
