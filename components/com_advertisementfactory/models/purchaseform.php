<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelPurchaseForm extends JModelLegacy
{
    public function getGateways()
    {
        $payment = JModelLegacy::getInstance('PaymentProxy', 'AdvertisementFactoryModel');

        return $payment->getGatewaySelect();
    }

    public function getTitle()
    {
        $app = JFactory::getApplication();
        $context = 'com_advertisementfactory.purchase.ad';

        $data = $app->getUserState($context . '.data', array());

        $ad = JTable::getInstance('Ad', 'AdvertisementFactoryTable');
        $ad->load($data['ad_id']);

        $price = JTable::getInstance('Price', 'AdvertisementFactoryTable');
        $price->load($data['price_id']);

        $settings = JComponentHelper::getParams('com_advertisementfactory');

        return JText::plural('COM_ADVERTISEMENTFACTORY_PURCHASETYPES_TYPE_' . strtoupper($price->price_type), $price->value, $price->price, $settings->get('currency'));
    }
}
