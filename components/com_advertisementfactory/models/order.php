<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelOrder extends JModelLegacy
{
    public function create($gateway, $data)
    {
        $order = $this->getTable('Order', 'AdvertisementFactoryTable');
        $price = $this->getTable('Price', 'AdvertisementFactoryTable');
        $ad = $this->getTable('Ad', 'AdvertisementFactoryTable');
        $module = JTable::getInstance('Module');
        $component = JComponentHelper::getComponent('com_advertisementfactory');
        $user = JFactory::getUser();

        $price->load($data['price_id']);
        $ad->load($data['ad_id']);
        $module->load($ad->module_id);

        $order->gateway = $gateway;
        $order->ad_id = $data['ad_id'];
        $order->price_id = $data['price_id'];

        if ($ad->module_id) {
            $order->title = JText::plural(
                'COM_ADVERTISEMENTFACTORY_ORDER_TITLE_MODULE_' . (strtoupper($price->price_type)),
                $price->value,
                $price->price,
                $component->params->get('currency'),
                $module->title);
        } else {
            $order->title = JText::sprintf(
                'COM_ADVERTISEMENTFACTORY_ORDER_TITLE_' . (strtoupper($price->price_type)),
                $price->value,
                $price->price,
                $component->params->get('currency'));
        }

        $params = new JRegistry($price);
        $order->price_saved = $params->toString();

        $order->amount = $price->price;
        $order->currency = $component->params->get('currency');
        $order->user_id = $user->id;
        $order->status = 'pending';

        $order->store();

        return $order;
    }

    public function getOrderId($id)
    {
        $table = $this->getTable('Order', 'AdvertisementFactoryTable');

        $table->load($id);

        return $table;
    }
}
