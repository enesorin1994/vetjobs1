<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelMessage extends JModelLegacy
{
    protected $_errors = array();

    public function send($message, $ad_id)
    {
        // Check the message is not empty.
        if ('' == $message) {
            $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_MESSAGE_SEND_ERROR_MESSAGE_EMPTY'));
            return false;
        }

        // Find ad.
        $ad = JTable::getInstance('Ad', 'AdvertisementFactoryTable');
        $ad->load($ad_id);

        // Check if ad belongs to the user.
        if (JFactory::getApplication()->isSite() && JFactory::getUser()->id != $ad->user_id) {
            $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_MESSAGE_SEND_ERROR_AD_NOT_FOUND'));
            return false;
        }

        // Send the message.
        $table = JTable::getInstance('Message', 'AdvertisementFactoryTable');

        $table->ad_id = $ad_id;
        $table->message = $message;

        // Store the ad.
        if (!$table->store()) {
            $this->setError($table->getError());
            return false;
        }

        $this->sendNotification($ad, $message, $table->created_at);

        return true;
    }

    protected function sendNotification($ad, $message, $date)
    {
        JLoader::discover('Factory', JPATH_COMPONENT_ADMINISTRATOR . DS . 'lib');
        $notifications = FactoryNotification::getInstance();

        if (JFactory::getApplication()->isAdmin()) {
            $receivers = array($ad->user_id);
        } else {
            $settings = JComponentHelper::getParams('com_advertisementfactory');
            $groups = $settings->get('notification_admin_groups', array());
            $receivers = array();

            foreach ($groups as $group) {
                $receivers = array_merge($receivers, JAccess::getUsersByGroup($group));
            }
        }

        $notifications->sendNotification('ad_message_new', $receivers, array(
            'ad_name' => $ad->name,
            'message' => $message,
            'date'    => $date,
        ));

        return true;
    }

    public function getError($i = null, $toString = true)
    {
        // Find the error
        if ($i === null) {
            // Default, return the last message
            $error = end($this->_errors);
        } elseif (!array_key_exists($i, $this->_errors)) {
            // If $i has been specified but does not exist, return false
            return false;
        } else {
            $error = $this->_errors[$i];
        }

        // Check if only the string is requested
        if ($error instanceof Exception && $toString) {
            return (string)$error;
        }

        return $error;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function setError($error)
    {
        array_push($this->_errors, $error);
    }
}
