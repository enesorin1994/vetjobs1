<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelPaymentProxy extends JModelLegacy
{
    protected $options;

    public function __construct($properties = null)
    {
        parent::__construct($properties);

        $order = JModelLegacy::getInstance('Order', 'AdvertisementFactoryModel');
        $payment = JTable::getInstance('Payment', 'AdvertisementFactoryTable');

        $this->options = array(
            'table_prefix' => 'advertisementfactory',
            'component'    => 'com_advertisementfactory',
            'order'        => $order,
            'payment'      => $payment,
            'basepath'     => JPATH_COMPONENT_ADMINISTRATOR . DS . 'payment',
            'sourcepath'   => JURI::root() . 'administrator/components/com_advertisementfactory/payment/',
            'dbo'          => JFactory::getDbo(),
            'date'         => JFactory::getDate(),
            'user'         => JFactory::getUser(),
            'language'     => JFactory::getLanguage(),
        );

        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'payment' . DS . 'TheFactoryPaymentPlugins.php');
    }

    public function getGatewaySelect()
    {
        $payment = new TheFactoryPaymentPlugins($this->options);

        return $payment->getGatewaySelect();
    }

    public function validateGateway($gateway)
    {
        $payment = new TheFactoryPaymentPlugins($this->options);

        return $payment->validateGateway($gateway);
    }

    public function getGateway($gateway)
    {
        $payment = new TheFactoryPaymentPlugins($this->options);

        return $payment->getGateway($gateway);
    }

    public function updateGateways()
    {
        $payment = new TheFactoryPaymentPlugins($this->options);

        return $payment->updateGateways();
    }
}
