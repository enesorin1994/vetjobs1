<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelServeAd extends JModelLegacy
{
    public function getAds($type = '', $options = array())
    {
        // Check for expired ads.
        $this->checkExpired();

        // Check if preview mode is enabled.
        if ($this->previewMode()) {
            return $this->getPreviewAd($type, $options);
        }

        // Initialise variables.
        $dbo = JFactory::getDbo();
        $date = JFactory::getDate();

        $nullDate = $dbo->quote($dbo->getNullDate());
        $nowDate = $dbo->quote($date->toSql());

        // Select the orders
        $query = $dbo->getQuery(true)
            ->select('a.id, a.title, a.url, a.filename, a.contents, a.keywords, a.price_type, a.purchased, a.served')
            ->from('#__advertisementfactory_ads a')
            ->where('a.type = ' . $dbo->quote($type))
            ->where('a.state = 1');

        // Filter by impressions left
        $query->where('IF (a.price_type = "1", a.served < a.purchased, 1)');

        // Filter by publish dates
        $query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')')
            ->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');

        // Filter by module
        if (isset($options['module_id'])) {
            $query->where('a.module_id = ' . $dbo->quote((int)$options['module_id']));
        }

        $query->order('RAND()');
        $dbo->setQuery($query, 0, isset($options['limit']) ? $options['limit'] : 0);
        $ads = $dbo->loadObjectList();

        if ($ads) {
            $this->markAsShown($ads);

            return $this->prepare($ads);
        }

        return null;
    }

    public function checkExpired()
    {
        static $checkExpired = null;

        if (!is_null($checkExpired)) {
            return true;
        }

        JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'tables');
        $checkExpired = true;

        $dbo = JFactory::getDbo();
        $date = JFactory::getDate();

        $dateNow = $date->toSql();
        $ad = JTable::getInstance('Ad', 'AdvertisementFactoryTable');

        $query = $dbo->getQuery(true)
            ->update('#__advertisementfactory_ads')
            ->set('state = 2')
            ->set('paid = 0')
            ->set('history = CONCAT(' . $dbo->quote($ad->appendToHistory(2, null, true)) . ', history)')
            ->where('price_type = 3')
            ->where('state = 1')
            ->where('publish_down <= ' . $dbo->quote($dateNow));
        $dbo->setQuery($query);

        return $dbo->execute();
    }

    public function getPreviewAd($type, $options = array())
    {
        static $ad = null;

        if (is_null($ad)) {
            JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'tables');
            $ad_id = JFactory::getApplication()->input->getInt('ad_id');
            $ad = JTable::getInstance('Ad', 'AdvertisementFactoryTable');

            // Load ad
            $ad->load($ad_id);
        }

        $user = JFactory::getUser();
        $module_id = isset($options['module_id']) ? (int)$options['module_id'] : null;

        // Show only if it's my ad or i have backend.manage access or i have backend preview token
        $settings = JComponentHelper::getParams('com_advertisementfactory');
        if ($ad->user_id != $user->id &&
            (!JAccess::check($user->id, 'backend.manage', 'com_advertisementfactory') && !JAccess::check($user->id, 'core.admin')) &&
            $settings->get('backend_preview_token', null) != JFactory::getApplication()->input->getCmd('token')
        ) {
            return null;
        }

        // Show only if the type is right
        if ($ad->type != $type) {
            return null;
        }

        // Check if module is valid
        if (!is_null($module_id) && $ad->module_id != $module_id) {
            return null;
        }

        return $this->prepare(array($ad));
    }

    protected function markAsShown($ads)
    {
        static $marked = array();

        if (!$ads) {
            return false;
        }

        JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'tables');

        foreach ($ads as $ad) {
            if (1 != $ad->price_type || in_array($ad->id, $marked)) {
                continue;
            }

            $table = JTable::getInstance('Ad', 'AdvertisementFactoryTable');
            $table->bind($ad);

            $table->hit();

            $marked[] = $ad->id;
        }

        return true;
    }

    protected function prepare($ads)
    {
        if (!$ads) {
            return $ads;
        }

        if (!is_array($ads)) {
            $ads = array($ads);
        }

        $preview = $this->previewMode() ? '&preview=1' : '';

        foreach ($ads as &$ad) {
            $ad->redirectLink = JRoute::_('index.php?option=com_advertisementfactory&task=ad.url&id=' . $ad->id . $preview);
        }

        return $ads;
    }

    protected function previewMode()
    {
        $view = JFactory::getApplication()->input->getCmd('view');
        $option = JFactory::getApplication()->input->getCmd('option');

        return 'previsualize' == $view && 'com_advertisementfactory' == $option;
    }
}
