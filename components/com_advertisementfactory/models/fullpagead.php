<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelFullPageAd extends JModelLegacy
{
    public function getData()
    {
        $id = $this->getNextAdId();
        $fullPageAd = $this->getTable('Ad', 'AdvertisementFactoryTable');

        $fullPageAd->load($id);

        return $fullPageAd;
    }

    public function getTimeout()
    {
        $suffix = JFactory::getUser()->guest ? '_guests' : '';
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        return $component->params->get('fullpageads_seconds_to_display' . $suffix, 10);
    }

    public function getNextAdId()
    {
        $session = JFactory::getSession();

        return $session->get('advertisementfactory.fullpage.nextad', 0);
    }

    public function getRedirect()
    {
        $session = JFactory::getSession();

        return base64_decode($session->get('advertisementfactory.fullpage.redirect', ''));
    }

    public function getTarget()
    {
        $settings = JComponentHelper::getParams('com_advertisementfactory');

        return $settings->get('ads_open_new_window', 0) ? 'target="_blank"' : '';
    }
}
