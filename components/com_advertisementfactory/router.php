<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

function getAdvertisementFactoryRoutes()
{
    $routes = array(
        'ads' => array('view' => 'ads'),
    );

    return $routes;
}

function AdvertisementFactoryBuildRoute(&$query)
{
    if (!isset($query['view']) && !isset($query['task'])) {
        return array();
    }

    $function = 'get' . str_replace('BuildRoute', '', __FUNCTION__) . 'Routes';
    $routes = $function();
    $segments = array();

    foreach ($routes as $alias => $route) {
        if (isset($query['view'])) {
            if (!isset($route['view']) || $route['view'] != $query['view']) {
                continue;
            }
        } elseif (isset($query['task'])) {
            if (!isset($route['task']) || $route['task'] != $query['task']) {
                continue;
            }
        }

        $valid = true;

        if (isset($route['params'])) {
            foreach ($route['params'] as $type => $param) {
                if (!isset($query[$param])) {
                    if ('optional' !== $type) {
                        $valid = false;
                        break;
                    }
                }
            }
        }

        if (!$valid) {
            continue;
        }

        $segments[] = $alias;
        if (isset($query['view'])) {
            unset($query['view']);
        } else {
            unset($query['task']);
        }

        if (isset($route['params'])) {
            foreach ($route['params'] as $param) {
                if (isset($query[$param])) {
                    $segments[] = $query[$param];
                    unset($query[$param]);
                }
            }
        }

        break;
    }

    // Set default values if no route found.
    if (!$segments) {
        // Only do this for views.
        if (isset($query['view'])) {
            $segments[] = $query['view'];
            unset($query['view']);
        }
    }

    return $segments;
}

function AdvertisementFactoryParseRoute($segments)
{
    $function = 'get' . str_replace('ParseRoute', '', __FUNCTION__) . 'Routes';
    $routes = $function();
    $vars = array();

    foreach ($segments as $i => $segment) {
        $segments[$i] = str_replace(':', '-', $segment);
    }

    if (array_key_exists($segments[0], $routes)) {
        $route = $routes[$segments[0]];

        if (isset($route['view'])) {
            $vars['view'] = $route['view'];
        } else {
            $vars['task'] = $route['task'];
        }

        if (isset($route['params'])) {
            $i = 1;
            foreach ($route['params'] as $type => $param) {
                if (isset($segments[$i])) {
                    $vars[$param] = $segments[$i];
                }

                $i++;
            }
        }
    }

    // Set default values if no route found.
    if (!$vars) {
        // We're assuming the route is a view.
        $vars['view'] = $segments[0];
    }

    return $vars;
}
