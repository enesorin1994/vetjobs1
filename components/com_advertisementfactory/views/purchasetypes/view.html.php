<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewPurchaseTypes extends JViewLegacy
{
    function display($tpl = null)
    {
        $this->ad = $this->get('Ad');
        $this->prices = $this->get('Prices');
        $this->currency = $this->get('Currency');
        $this->showmore = $this->get('ShowMore');

        JHtml::_('behavior.modal');
        JHtml::stylesheet('media/com_advertisementfactory/assets/frontend/css/main.css');
        JHtml::stylesheet('media/com_advertisementfactory/assets/frontend/css/views/purchasetypes.css');

        parent::display($tpl);
    }
}
