<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

if (isset($this->prices[$this->type])): ?>
    <div class="advertisementfactory-ad-type">
        <h2><?php echo JText::_('COM_ADVERTISEMENTFACTORY_PURCHASETYPES_TYPE_' . strtoupper($this->type)); ?></h2>

        <?php $this->items = $this->prices[$this->type][0];
        echo $this->loadTemplate('item'); ?>
    </div>
<?php endif; ?>
