<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="advertisementfactory-wrapper">
    <h1>
        <?php if ($this->ad): ?>
            <?php echo JText::sprintf('COM_ADVERTISEMENTFACTORY_PURCHASETYPES_AD', $this->ad->name); ?>
        <?php else: ?>
            <?php echo JText::_('COM_ADVERTISEMENTFACTORY_PURCHASETYPES_PAGE_TITLE'); ?>
        <?php endif; ?>
    </h1>

    <?php $this->type = 'link';
    echo $this->loadTemplate('module'); ?>
    <?php $this->type = 'thumbnail';
    echo $this->loadTemplate('module'); ?>
    <?php $this->type = 'banner';
    echo $this->loadTemplate('module'); ?>
    <?php $this->type = 'modal';
    echo $this->loadTemplate('simple'); ?>
    <?php $this->type = 'fullpage';
    echo $this->loadTemplate('simple'); ?>
    <?php $this->type = 'keyword';
    echo $this->loadTemplate('simple'); ?>
    <?php $this->type = 'article';
    echo $this->loadTemplate('simple'); ?>
    <?php $this->type = 'popup';
    echo $this->loadTemplate('simple'); ?>

    <?php if ($this->showmore): ?>
        <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=purchasetypes'); ?>"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_PURCHASETPYES_ALL_AVAILABLE'); ?></a>
    <?php endif; ?>
</div>
