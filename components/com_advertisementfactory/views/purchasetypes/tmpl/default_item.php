<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<ul class="advertisementfactory-simple">
    <?php foreach ($this->items['ads'] as $item): ?>
        <li>
            <?php if ($this->ad): ?>
                <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&task=ad.edit&id=' . $this->ad->id); ?>"><?php echo $this->ad->name; ?></a> /
            <?php endif; ?>

            <?php echo JText::plural('COM_ADVERTISEMENTFACTORY_PURCHASETYPES_TYPE_' . strtoupper($item->price_type), $item->value, $item->price, $this->currency); ?>

            <?php if ($this->ad): ?>
                /
                <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&task=ad.purchase&id=' . $this->ad->id . '&price_id=' . $item->id . '&module_id=' . (int)$item->module_id); ?>"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_AD_BUY_NOW'); ?></a>
            <?php else: ?>
                <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=ads&layout=select&tmpl=component&price_id=' . $item->id . '&type=' . $item->type . '&module_id=' . (int)$item->module_id); ?>"
                   class="modal advertisementfactory-button button-bullet-go"
                   rel="{handler:'iframe', size: {x: 800, y: 600}}">
                    <?php echo JText::_('COM_ADVERTISEMENTFACTORY_PURCHASETYPES_SELECT_AVAILABLE'); ?></a>

                <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=ad&tmpl=component&type=' . $item->type . '&module_id=' . (int)$item->module_id . '&module_id=' . (int)$item->module_id . '&price_id=' . $item->id); ?>"
                   class="modal advertisementfactory-button button-add"
                   rel="{handler:'iframe', size: {x: 800, y: 600}}">
                    <?php echo JText::_('COM_ADVERTISEMENTFACTORY_PURCHASETYPES_CREATE_NEW'); ?></a>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>
