<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

if (isset($this->prices[$this->type])): ?>
    <div class="advertisementfactory-ad-type">
        <h2><?php echo JText::_('COM_ADVERTISEMENTFACTORY_PURCHASETYPES_TYPE_' . strtoupper($this->type)); ?></h2>

        <ul class="advertisementfactory-module">
            <?php foreach ($this->prices[$this->type] as $this->items): ?>
                <li>
                    <h3><?php echo $this->items['title']; ?></h3>

                    <?php if (!is_null($this->items['availability'])): ?>
                        <span class="advertisementfactory-button button-exclamation"
                              style="color: #ff0000;"><?php echo JText::sprintf('COM_ADVERTISEMENTFACTORY_PURCHASETYPES_MODULE_DELAY', $this->items['availability']); ?></span>
                    <?php endif; ?>

                    <?php echo $this->loadTemplate('item'); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
