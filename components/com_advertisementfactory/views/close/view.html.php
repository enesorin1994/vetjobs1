<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewClose extends JViewLegacy
{
    function display($tpl = null)
    {
        //JFactory::getDocument()->addScriptDeclaration('window.parent.SqueezeBox.close();');

        $ad_id = JFactory::getApplication()->input->getInt('ad_id');
        $price_id = JFactory::getApplication()->input->getInt('price_id');

        $link = JRoute::_('index.php?option=com_advertisementfactory&task=ad.purchase&id=' . $ad_id . '&price_id=' . $price_id, false);

        JFactory::getDocument()->addScriptDeclaration('window.parent.location.href = "' . $link . '";');
    }
}
