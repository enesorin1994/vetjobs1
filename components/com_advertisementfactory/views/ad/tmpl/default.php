<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<h1><?php echo $this->item->id ? JText::_('COM_ADVERTISEMENTFACTORY_MYAD_EDIT') : JText::_('COM_ADVERTISEMENTFACTORY_MYAD_NEW'); ?></h1>

<div class="edit">
    <form action="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=ad&id=' . (int)$this->item->id) . '&price_id=' . (int)JFactory::getApplication()->input->getInt('price_id') . '&type=' . JFactory::getApplication()->input->getCmd('type'); ?>"
          method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
        <fieldset>
            <legend><?php echo $this->item->id ? JText::_('COM_ADVERTISEMENTFACTORY_MYAD_EDIT') : JText::_('COM_ADVERTISEMENTFACTORY_MYAD_NEW'); ?></legend>

            <div class="formelm">
                <?php echo $this->form->getLabel('name'); ?>
                <?php echo $this->form->getInput('name'); ?>
            </div>

            <div class="formelm">
                <?php echo $this->form->getLabel('type'); ?>
                <?php echo $this->form->getInput('type'); ?>
            </div>

            <div class="formelm">
                <?php echo $this->form->getLabel('module_id'); ?>
                <?php echo $this->form->getInput('module_id'); ?>
            </div>

            <div class="formelm">
                <?php echo $this->form->getLabel('title'); ?>
                <?php echo $this->form->getInput('title'); ?>
            </div>

            <div class="formelm">
                <?php echo $this->form->getLabel('contents'); ?>
                <?php echo $this->form->getInput('contents'); ?>
            </div>

            <div class="formelm">
                <?php echo $this->form->getLabel('category'); ?>
                <?php echo $this->form->getInput('category'); ?>
            </div>

            <div class="formelm">
                <?php echo $this->form->getLabel('keywords'); ?>
                <?php echo $this->form->getInput('keywords'); ?>
            </div>

            <div class="formelm">
                <?php echo $this->form->getLabel('url'); ?>
                <?php echo $this->form->getInput('url'); ?>
            </div>

            <?php foreach ($this->form->getFieldset('params') as $field): ?>
                <div class="formelm">
                    <?php echo $field->label; ?>
                    <?php echo $field->input; ?>
                </div>
            <?php endforeach; ?>

            <div style="clear: both;"></div>

            <div class="formelm" style="float: left;">
                <div style="float: left; margin-right: 5px;">
                    <?php echo $this->form->getLabel('filename'); ?>
                </div>
                <?php echo $this->form->getInput('filename'); ?>
            </div>

            <div style="clear: both;"></div>

            <div class="formelm-buttons">
                <?php if ($this->item->state == 3): ?>
                    <button class="btn" type="button"
                            onclick="Joomla.submitbutton('ad.submitforvalidation'); return false;" style="float: left;">
                        <?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYAD_SUBMIT_FOR_VALIDATION') ?>
                    </button>
                <?php endif; ?>

                <?php if ($this->item->id): ?>
                    <button class="btn" type="button" onclick="Joomla.submitbutton('ad.preview'); return false;">
                        <?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYAD_PREVIEW') ?>
                    </button>
                <?php endif; ?>

                <button class="btn" type="button" onclick="Joomla.submitbutton('ad.apply'); return false;">
                    <?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYAD_APPLY') ?>
                </button>

                <button class="btn" type="button" onclick="Joomla.submitbutton('ad.save'); return false;">
                    <?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYAD_SAVE') ?>
                </button>

                <button class="btn" type="button" onclick="Joomla.submitbutton('ad.cancel'); return false;">
                    <?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYAD_CANCEL') ?>
                </button>
            </div>
        </fieldset>

        <input type="hidden" name="task" value=""/>
        <input type="hidden" name="tmpl" value="<?php echo $this->tmpl; ?>"/>
        <?php echo JHTML::_('form.token'); ?>

    </form>
</div>

<?php if ($this->item->id): ?>
    <?php echo $this->loadTemplate('messages'); ?>
<?php endif; ?>
