<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewAd extends JViewLegacy
{
    public function __construct($properties = null)
    {
        parent::__construct($properties);

        $lang = JFactory::getLanguage();
        $lang->load('com_advertisementfactory', JPATH_ADMINISTRATOR);
    }

    public function display($tpl = null)
    {
        $this->item = $this->get('Item');
        $this->form = $this->get('Form');
        $this->state = $this->get('State');
        $this->tmpl = $this->get('Template');
        $this->messages = $this->get('Messages');

        JHtml::_('behavior.formvalidation');
        JHtml::_('behavior.tooltip');
        JHtml::stylesheet('media/com_advertisementfactory/assets/frontendcss/views/myad.css');

        parent::display($tpl);
    }
}
