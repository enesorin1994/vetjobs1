<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="advertisementfactory-wrapper">
    <h1><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_PAGE_TITLE'); ?></h1>

    <form action="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=ads'); ?>" method="POST"
          id="adminForm">
        <div class="filters">
            <?php echo $this->filterType; ?>
            <?php echo $this->filterPaid; ?>
            <?php echo $this->filterPublished; ?>

            <?php echo $this->loadTemplate('legend'); ?>
        </div>

        <table class="list">
            <thead>
            <tr>
                <th width="1%"><input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this)"/>
                </th>
                <th><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_TITLE'); ?></th>
                <th style="width:150px;"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_STATUS'); ?></th>
                <th style="width: 50px;"
                    class="hidden-phone"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_TYPE'); ?></th>
                <th class="center" style="width: 70px;">
                    <?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_PAID'); ?>
                </th>
                <th class="center hidden-phone hidden-tablet" style="width: 50px;">
                    <?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_PUBLISHED'); ?>
                </th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <td colspan="10" class="pagination">
                    <?php echo $this->pagination->getPagesLinks(); ?>
                </td>
            </tr>
            </tfoot>

            <tbody>
            <?php if (!$this->items): ?>
                <tr>
                    <td colspan="10"><?php echo $this->pagination->getResultsCounter(); ?></td>
                </tr>
            <?php else: ?>
                <?php foreach ($this->items as $i => $item): ?>
                    <tr class="<?php echo $i % 2 ? 'alternate' : ''; ?>">
                        <td><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
                        <td>
                            <a href="<?php echo $item->edit_link; ?>"><?php echo $item->name; ?></a>
                            <br/>
                            <?php if ($item->type != 'article'): ?>
                                <small><a href="<?php echo $item->preview_link; ?>"
                                          class="advertisementfactory-button button-preview"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_PREVIEW'); ?></a>
                                </small><?php endif; ?>
                            <?php if ($item->history): ?>
                                <small>
                                    <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=history&id=' . $item->id); ?>"
                                       class="advertisementfactory-button button-article"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_HISTORY'); ?></a>
                                </small>
                            <?php endif; ?>
                        </td>

                        <td>
                            <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=track&id=' . $item->id); ?>"><?php echo $item->status; ?></a>
                        </td>

                        <td class="hidden-phone"><?php echo $item->type; ?></td>
                        <td class="center"><?php echo JHtml::_('advertisementfactory.paid', $item->paid, true, array('ad_id' => $item->id)); ?></td>
                        <td class="center hidden-phone hidden-tablet"><?php echo JHtml::_('advertisementfactory.state', $item->state); ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>

        <input type="hidden" name="task" value=""/>
        <?php echo JHTML::_('form.token'); ?>
    </form>

    <div>
        <a href="#" onclick="Joomla.submitbutton('ad.add'); return false;"
           class="advertisementfactory-button button-new"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_CREATE_AD'); ?></a>
        <a href="#" onclick="Joomla.submitbutton('ads.delete'); return false;"
           class="advertisementfactory-button button-delete"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_DELETE'); ?></a>
    </div>
</div>
