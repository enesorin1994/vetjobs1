<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<a href="#" class="hasTip advertisementfactory-button button-info"
   title="::<?php echo JText::sprintf('COM_ADVERTISEMENTFACTORY_MYADS_LEGEND_TEXT',
       htmlentities(JHtml::_('advertisementfactory.paid', 0, false)),
       htmlentities(JHtml::_('advertisementfactory.paid', 1)),
       htmlentities(JHtml::_('advertisementfactory.paid', 2)),
       htmlentities(JHtml::_('advertisementfactory.state', 0)),
       htmlentities(JHtml::_('advertisementfactory.state', 1)),
       htmlentities(JHtml::_('advertisementfactory.state', 2)),
       htmlentities(JHtml::_('advertisementfactory.state', 3)),
       htmlentities(JHtml::_('advertisementfactory.state', 4)),
       htmlentities(JHtml::_('advertisementfactory.state', 5))
   ); ?>"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_LEGEND_TITLE'); ?></a>
