<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="advertisementfactory-wrapper">
    <h1>
        <?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYAD_SELECT_AD'); ?>
    </h1>

    <?php if (!$this->items): ?>
        <?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_SELECT_NOT_FOUND'); ?>
        <br/><br/>
        <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=ad&tmpl=component&type=' . $this->type . '&module_id=' . (int)$this->moduleId); ?>"
           class="modal advertisementfactory-button button-add" rel="{handler:'iframe', size: {x: 800, y: 600}}">
            <?php echo JText::_('COM_ADVERTISEMENTFACTORY_PURCHASETYPES_CREATE_NEW'); ?></a>
    <?php else: ?>

        <table style="width: 100%;">
            <thead>
            <tr>
                <th><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYADS_TITLE'); ?></th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <td colspan="6" class="pagination">
                    <?php echo $this->pagination->getPagesLinks(); ?>
                </td>
            </tr>
            </tfoot>

            <tbody>
            <?php foreach ($this->items as $item): ?>
                <tr>
                    <td><a href="#"
                           onclick="parent.location.href='<?php echo $item->purchase_link; ?>';"><?php echo $item->name; ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
