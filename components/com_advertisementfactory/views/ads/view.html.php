<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewAds extends JViewLegacy
{
    public function __construct($properties = null)
    {
        parent::__construct($properties);

        $lang = JFactory::getLanguage();
        $lang->load('com_advertisementfactory', JPATH_ADMINISTRATOR);
    }

    public function display($tpl = null)
    {
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');

        $this->filterPaid = $this->get('FilterPaid');
        $this->filterPublished = $this->get('FilterPublished');
        $this->filterType = $this->get('FilterType');

        $this->type = $this->get('Type');
        $this->moduleId = $this->get('ModuleId');

        require_once(JPATH_COMPONENT_SITE . DS . 'lib' . DS . 'html' . DS . 'item.php');

        JHtml::stylesheet('media/com_advertisementfactory/assets/frontend/css/main.css');
        JHtml::stylesheet('media/com_advertisementfactory/assets/frontend/css/views/myads.css');

        JHtml::_('behavior.tooltip');

        parent::display($tpl);
    }
}
