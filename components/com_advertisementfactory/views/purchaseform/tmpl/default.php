<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="advertisementfactory-wrapper">
    <form action="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=ads'); ?>" method="POST"
          id="adminForm" name="adminForm" class="form-validate">
        <h1><?php echo JText::_('COM_ADVERTISEMENTFACTORY_PURCHASEFORM_PAGE_TITLE'); ?></h1>

        <p><?php echo JText::sprintf('COM_ADVERTISEMENTFACTORY_PURCHASEFORM_SELECT_GATEWAY', $this->title); ?>:</p>

        <?php echo $this->gateways; ?>

        <br/>

        <button type="button" onclick="Joomla.submitbutton('purchase.selectgateway'); return false;">
            <?php echo JText::_('COM_ADVERTISEMENTFACTORY_PURCHASEFORM_SUBMIT') ?>
        </button>

        <input type="hidden" name="task" value=""/>
        <?php echo JHTML::_('form.token'); ?>
    </form>
</div>
