<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewFullPageAd extends JViewLegacy
{
    function display($tpl = null)
    {
        $this->fullpagead = $this->get('Data');
        $this->redirect = $this->get('Redirect');
        $this->timeout = $this->get('Timeout');
        $this->target = $this->get('Target');

        JHtml::stylesheet('media/com_advertisementfactory/assets/frontend/css/views/fullpagead.css');
        JHtml::script('media/com_advertisementfactory/assets/frontend/js/object.js', true);
        JHtml::script('media/com_advertisementfactory/assets/frontend/js/views/fullpagead.js');

        $document = JFactory::getDocument();

        $document->addScriptDeclaration('AdvertisementFactory.set("redirect", "' . $this->redirect . '")');
        $document->addScriptDeclaration('AdvertisementFactory.set("timeout",  "' . $this->timeout . '")');

        parent::display($tpl);
    }
}
