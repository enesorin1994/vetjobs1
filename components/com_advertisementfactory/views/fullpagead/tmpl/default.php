<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="advertisementfactory-fullpagead-header">
    <?php echo JText::_('FULLPAGEAD_REDIRECT_MESSAGE'); ?>
    <a href="<?php echo $this->redirect; ?>"><?php echo JText::_('FULLPAGEAD_REDIRECT_NOW_LINK'); ?></a>
</div>

<div class="advertisementfactory-fullpagead-content"">
<?php if ('' != $this->fullpagead->title): ?>
    <h1><?php echo $this->fullpagead->title; ?></h1>
<?php endif; ?>

<?php if ('' != $this->fullpagead->contents): ?>
    <p><?php echo $this->fullpagead->contents; ?></p>
<?php endif; ?>

<div style="text-align: center;">
    <a href="<?php echo $this->fullpagead->getUrl(); ?>" <?php echo $this->target; ?>>
        <img src="<?php echo $this->fullpagead->getFileSource(); ?>" style="max-width: 80%;"/>
    </a>
</div>
</div>
