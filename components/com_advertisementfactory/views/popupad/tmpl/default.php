<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="advertisementfactory-popupad-header">
    <?php echo JText::_('COM_ADVERTISEMENTFACTORY_POPUPAD_REDIRECT_MESSAGE'); ?>
    <a href="#" onclick="window.close();"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_POPUPAD_CLOSE_NOW_LINK'); ?></a>
</div>

<div class="advertisementfactory-popupad-content"">
<?php if ('' != $this->ad->title): ?>
    <h1><?php echo $this->ad->title; ?></h1>
<?php endif; ?>

<?php if ('' != $this->ad->contents): ?>
    <p><?php echo $this->ad->contents; ?></p>
<?php endif; ?>

<div style="text-align: center;">
    <a href="<?php echo $this->ad->getUrl(); ?>" target="_blank">
        <img src="<?php echo $this->ad->getFileSource(); ?>" style="max-width: 80%;"/>
    </a>
</div>
</div>
