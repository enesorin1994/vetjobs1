<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewPopupAd extends JViewLegacy
{
    function display($tpl = null)
    {
        $this->ad = $this->get('Data');
        $this->timeout = $this->get('Timeout');

        JHtml::stylesheet('media/com_advertisementfactory/assets/frontend/css/views/popupad.css');
        JHtml::script('media/com_advertisementfactory/assets/frontend/js/object.js', true);
        JHtml::script('media/com_advertisementfactory/assets/frotnend/js/views/popupad.js');

        $document = JFactory::getDocument();

        $document->addScriptDeclaration('AdvertisementFactory.set("timeout",  "' . $this->timeout . '")');

        parent::display($tpl);
    }
}
