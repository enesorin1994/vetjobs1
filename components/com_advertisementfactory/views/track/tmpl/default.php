<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="">
    <?php if (!$this->ad): ?>
        <h1><?php echo JText::_('COM_ADVERTISEMENTFACTORY_TRACK_PAGE_HEADING_NOT_FOUND'); ?></h1>
        <p><?php echo JText::_('COM_ADVERTISEMENTFACTORY_TRACK_PAGE_TEXT_NOT_FOUND'); ?></p>
    <?php else: ?>
        <h1><?php echo JText::sprintf('COM_ADVERTISEMENTFACTORY_TRACK_PAGE_HEADING', $this->ad->title); ?></h1>

        <?php if (!$this->items): ?>
            <p><?php echo JText::_('COM_ADVERTISEMENTFACTORY_TRACK_STATS_NOT_FOUND'); ?></p>
        <?php else: ?>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th style="width: 120px;"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_TRACK_HEADING_DATE'); ?></th>
                    <th style="width: 120px;"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_TRACK_HEADING_USER_ID'); ?></th>
                    <th style="width: 100px;"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_TRACK_HEADING_TYPE'); ?></th>
                    <th><?php echo JText::_('COM_ADVERTISEMENTFACTORY_TRACK_HEADING_URL'); ?></th>
                </tr>
                </thead>

                <?php if ($this->pagination->pagesTotal > 1): ?>
                    <tfoot>
                    <tr>
                        <td colspan="10">
                            <div class="pagination">
                                <?php echo $this->pagination->getPagesLinks(); ?>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                <?php endif; ?>

                <tbody>
                <?php foreach ($this->items as $this->item): ?>
                    <tr>
                        <td>
                            <?php echo JHtml::_('date', $this->item->created_at, 'DATE_FORMAT_LC2'); ?>
                        </td>

                        <td>
                            <?php if ($this->item->user_id): ?>
                                <?php echo $this->item->username; ?>
                            <?php else: ?>
                                <?php echo $this->item->ip; ?>
                            <?php endif; ?>
                        </td>

                        <td>
                            <?php echo JText::_('COM_ADVERTISEMENTFACTORY_TRACK_TYPE_' . strtoupper($this->item->type)); ?>
                        </td>

                        <td>
                            <?php if ('click' == $this->item->type): ?>
                                <a href="<?php echo $this->item->url; ?>"><?php echo JHtml::_('string.abridge', $this->item->url); ?></a>
                            <?php else: ?>
                                <a href="<?php echo $this->item->referer; ?>"><?php echo JHtml::_('string.abridge', $this->item->referer); ?></a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    <?php endif; ?>

    <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=ads'); ?>"
       class="btn"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_TRACK_BACK_TO_ADS'); ?></a>
</div>
