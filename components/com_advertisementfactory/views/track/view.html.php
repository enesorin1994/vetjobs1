<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewTrack extends JViewLegacy
{
    public function __construct($properties = null)
    {
        parent::__construct($properties);

        $lang = JFactory::getLanguage();
        $lang->load('com_advertisementfactory', JPATH_ADMINISTRATOR);
    }

    public function display($tpl = null)
    {
        $this->ad = $this->get('Ad');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');

        JHtml::_('behavior.tooltip');

        parent::display($tpl);
    }
}
