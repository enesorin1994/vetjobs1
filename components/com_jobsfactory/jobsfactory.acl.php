<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JobsACL
{
    /* Unmapped tasks are considered Allowed for all */
    var $taskmapping = array(
        /* Seller tasks */
        "myjobs" => "seller",
        "newjob" => "seller",
        "form" => "seller",
        "accept" => "seller",
        "save" => "seller",
        "canceljob" => "seller",
        "editjob" => "seller",
        "republish" => "seller",
        "set_featured" => "seller",
        "buy_listing" => "seller",

        /* Candidate tasks */
        "editExperience" => "candidate",
        "editEducation" => "candidate",
        "saveExperience" => "candidate",
        "saveEducation" => "candidate",
        "sendjobapplication" => "candidate",
        "setUserGroup" => "registereduser"

    );

    var $publicTasks = array(
		"ipn",
		"searchBookmarks",
        "viewapplications",
        "listjobs",
        "listcompanies",
        "search",
        'show_search',
        "showsearchresults",
        "showSearchResults",
        "categories",
        "tree",
        "googlemap",
        "listlocations",
        "searchusers",
        "showusers",
        "googlemaps",
        "showusers",
        "searchbookmarks",
        "showbookmarks",
        "showBookmarks",
        "profile",
        "displayprofile",
        "watchlist",
        "tags"
    );

    var $anonTasks = array(
        "userdetails",
        "UserDetails",
        "myapplications",
        "saveuserdetails",
        "saveUserDetails",
        "googlemap_tool",
        "userprofile",
        "UserProfile",
        "paymentbalance",
        "cron"
    );

    var $profileTasks = array(
       "setUserGroup" => "registereduser"
    );

    function __construct()
    {
        $cfg = JTheFactoryHelper::getConfig();
        if ($cfg->allow_guest_messaging == 1) {
            array_push($this->publicTasks, "savemessage");
        }
    }

}

