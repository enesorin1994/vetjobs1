<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

// Check to ensure this file is included in Joomla!
jimport('joomla.application.component.controller');

class JJobsfactoryController extends JControllerLegacy
{
    var $_name = 'jobs';
    var $name = 'jobs';

    function __construct($config = array())
    {

        $this->input = JFactory::getApplication()->input;

        parent::__construct($config);

        // Register Extra tasks
        $this->registerTask('new', 'form');
        $this->registerTask('editjob', 'form');
        $this->registerTask('newjob', 'form');
        $this->registerTask('republish', 'form');
        $this->registerTask('details', 'viewapplications');
        $this->registerTask('listcats', 'categories');
        $this->registerTask('showSearchResults', 'listjobs');
        $this->registerTask('tags', 'listjobs');
        $this->registerTask('companyjobs', 'listjobs');
        $this->registerTask('showsearch', 'show_search');
        $this->registerTask('search', 'show_search');
        $this->registerTask('tree', 'categories');
		$this->registerDefaultTask('listjobs');
    }

    /**
     *          CATEGORY TASKS
     */
    function SelectCat()
    {
        //TODO:optimize category selection
        $task = $this->input->getCmd('task');
        $catModel = JobsHelperTools::getCategoryModel();
        $items = $catModel->getCategoryTree();

        $task = ($task == "selectcat") ? "edit" : $task;

        $view = $this->getView('category', 'html');
        $view->task=  $task;
        $view->categories = $items;

        $view->display("t_catselect.tpl");
    }

    function Categories()
    {
        $task = $this->input->getCmd('task');
        $filter_cat = $this->input->getInt("cat", 0);
        $filter_letter = $this->input->getString('filter_letter', 'all');

        $categoryModel = JobsHelperTools::getCategoryModel();
        if (!$categoryModel->getCategoryCount($filter_cat)) {
            $this->setRedirect(JobsHelperRoute::getJobListRoute(array('cat' => $filter_cat)));
            return;
        }

        if ($task == "tree") {
            $depth_level = null; //endless
            $template = "t_category_tree.tpl";
            JobsHelperView::loadJQuery();
        } else {
            $depth_level = 1;
            $template = "t_categories.tpl";
        }

        $user = JFactory::getUser();

        $JobsCatModel = JModelLegacy::getInstance('RCategory', 'jobsModel');
        $categories = $JobsCatModel->getJobsCategoryTree($filter_cat, $depth_level, $user->id, $filter_letter);

        JobsHelperView::prepareCategoryTree($categories, $task);

        $current_cat = null;

        if ($filter_cat) {
            $current_cat = JobsHelperTools::getCategoryTable();
            $current_cat->load($filter_cat);
        }

        $isCompany = 0;
        if ($user->id) {
            $isCompany = JobsHelperTools::isCompany($user->id);
        }

        $view = $this->getView('category', 'html');
        $view->filter_letter = JobsHelperView::buildLetterFilter($filter_cat);
        $view->current_cat = $current_cat;
        $view->categories = $categories;
        $view->isCompany = $isCompany;

        $view->display($template);
    }

    /**
     *          JOB LISTING TASKS
     */

    /**
     * JobsFactoryController::Form()
     *     New or Edit Jobs
     * @return
     */
    function Form()
    {
        $app = JFactory::getApplication();
        $id = $this->input->getInt('id');
        $task = $this->input->getCmd('task');
        $category = $this->input->getInt('category');
        $cfg = JTheFactoryHelper::getConfig();

        $categoryModel = JobsHelperTools::getCategoryModel();
        $locationModel = JobsHelperTools::getLocationModel();

        $job = JTable::getInstance('jobs', 'Table');
        if ($id) $job->load($id);
        $job->loadFromSession();

        if ($id) $job->id = $id; // Force ID if EDIT
        if ($category) $job->cat = $category; //Force Category if chosen

        if ($id && !$job->isMyJob()) {
            $app->enqueueMessage(JText::_("COM_JOBS_YOU_CAN_EDIT_ONLY_YOUR_JOBS"),'warning');
            return;
        }

        if ($id && $job->close_by_admin) {
            $app->enqueueMessage(JText::_("COM_JOBS_THIS_JOB_WAS_BANNED_BY_THE_SITE_ADMINISTRATOR"),'warning');
            return;
        }

        if ($cfg->workflow == 'catpage' && !$job->cat) {
            // Category selection is mandatory
            $nrCats = $categoryModel->getCategoryCount();
            if ($nrCats && !$id) {
                //New Job -> go to Select Categories
                $this->setRedirect(JobsHelperRoute::getSelectCategoryRoute());
                return;
            }
            $cat_obj = $categoryModel->getFirstCategory();
            $category = $cat_obj->id;
            $_REQUEST['category'] = $category;
            $_GET['category'] = $category;
            $_POST['category'] = $category;
            $job->cat = $category;
        }

		JModelLegacy::addIncludePath(JPATH_COMPONENT.'/models/');
        $jobModel = JModelLegacy::getInstance('Job', 'jobsModel');

        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile();

        $editorName = JFactory::getConfig()->get('editor');
        $editor = JEditor::getInstance($editorName);

        $listing_enabled = 0;
        $db=JFactory::getDbo();
        $db->setQuery("SELECT `enabled` FROM `#__jobsfactory_pricing` WHERE `itemname`='listing' ");
        $listing_enabled= $db->loadResult();

        $allow_edit = 1;
        if ($id) {
           $startdate = JobsHelperDateTime::DateToIso($job->start_date);
           $diff24 = JobsHelperDateTime::dateDiff($startdate);
           $hours = round($diff24 / 3600,0);

           if ( $hours > 24 && ($job->featured != 'none' || $listing_enabled) ) {
               $allow_edit = 0;
           }
        }

        $doc = JFactory::getDocument();

        if($id && !$allow_edit)
        {
           $lists["description"] = $job->description."<input type='hidden' name='description' value='".htmlentities($job->description,ENT_QUOTES, 'UTF-8')."' />";
        } else {
           $lists["description"] = $editor->display('description', $job->description, '100%', '400', '70', '15', false );

            $js_editor='function validateEditor(){
                  var content_description = '.$editor->getContent("description").';

                  if( content_description == "") {
                     alert("Please fill in the description field. This field is required!");
                     return false;
                  }
                  return true;
            }';

            $doc->addScriptDeclaration($js_editor);

        }

        $editorBenefitsName = JFactory::getConfig()->get('editor');
        $editorBenefits = JEditor::getInstance($editorBenefitsName);

        if($id && !$allow_edit)
        {
            $lists["benefits"] =   $job->benefits."<input type='hidden' name='benefits' value='".htmlentities($job->benefits,ENT_QUOTES, 'UTF-8')."' />";
        } else {
            $lists["benefits"] = $editorBenefits->display('benefits', $job->benefits, '100%', '400', '70', '15', false);
        }

        $lists['catname'] = $categoryModel->getCategoryPathString($job->cat);
        if ($cfg->workflow == 'catpage') {
            $lists['cats'] = '<input type="hidden" name="cat" value="' . $job->cat . '" /><span>' . $lists['catname'] . '</span>';
        } else
            $lists['cats'] = JHtml::_('factorycategory.select', 'cat', "onchange='jobsRefreshCustomFields(this);'", $job->cat);


        if ($cfg->enable_location_management) {
            if ($id) {

                $lists['country'] = JHtml::_('country.selectlist', 'job_country', "id='job_country' class='required' onchange='jobsRefreshLocations(this);'", $job->job_country);

                $job_location_state = $locationModel->getLocationName($job->job_location_state);
                $lists['states'] = JHtml::_('factorylocation.select', 'job_location_state', "onchange='jobsRefreshCities(this);'", $job->job_location_state, false); //$city->country
                $lists['cities'] = JHtml::_('factorycity.select', 'job_cityid', "", $job->job_cityid, false);
            } else {
                $r = new JObject();
                $lists['country'] = JHtml::_('country.selectlist', 'job_country', "id='job_country' class='required' onchange='jobsRefreshLocations(this);'", $r->get('com_jobsfactory.model_Jobs.filters.country'));
                $lists['states'] = JHtml::_('factorylocation.select', 'job_location_state', "onchange='jobsRefreshCities(this);'", '', false); //$city->country
                $lists['cities'] = JHtml::_('factorycity.select', 'job_cityid', "", '', false);
            }
        }

        if (!$cfg->employervisibility_enable) { // && $cfg->employervisibility_val
            $visibility_value = ($cfg->employervisibility_val == '0') ?  JText::_('COM_JOBS_CONFIDENTIAL') : JText::_('COM_JOBS_PUBLIC');
            $lists['employervisibility'] = "<input type='hidden' name='employer_visibility' value='" . $cfg->employervisibility_val . "' >" . $visibility_value;
        }
        else {
            $lists['employervisibility'] = JHtml::_('employervisibility.selectlist', 'employer_visibility', '', $job->employer_visibility);
        }

        if ($id) {
            $lists['aboutusvisibility'] = JHtml::_('employervisibility.selectlist', 'about_visibility', '', $job->about_visibility);
        } else {
            $lists['aboutusvisibility'] = JHtml::_('employervisibility.selectlist', 'about_visibility', '', '');
        }
		$lists['jobtypes'] = JHtml::_('jobtype.selectlist', 'job_type', '', $job->job_type); //full time, part time

        $scripttoggle = 'onchange="if(this.value==\'0\') {

                var el = document.getElementById(\'job_country\');
                jQuery(\'#job_country\').attr(\'disabled\', true).trigger(\'liszt:updated\');
                if (el.className == \'required chzn-done\')
                    el.setAttribute(\'class\', \'\');

                document.getElementById(\'countrylist\').style.display=\'none\';
                document.getElementById(\'statelist\').style.display=\'none\'; } else {
                jQuery(\'#job_country\').attr(\'disabled\', false).trigger(\'liszt:updated\');
                document.getElementById(\'countrylist\').style.display=\'\';
                document.getElementById(\'statelist\').style.display=\'\'; } "
        ';

        if (($cfg->enable_location_management) || ($job->id && $job->job_country) ) {
            $lists['use_location'] = JHtml::_('jobuselocation.selectlist', 'use_location', $scripttoggle, 1);
        }
        else
            $lists['use_location'] = JHtml::_('jobuselocation.selectlist', 'use_location', $scripttoggle, 0);

        if (($cfg->jobpublish_enable) || ($job->id && !$job->published) ) {
            //second part of the IF is for unpublished jobs due to lack of credits
            $selected = ($id != null ) ? $job->published : 1;
            $lists['published'] = JHtml::_('jobpublished.selectlist', 'published', '', $selected);
        }
        else
            $lists['published'] = "<input type='hidden' name='published' value='" . $cfg->jobpublish_val . "' >";

        //Date Time
        $lists['end_hour'] = '00';
        $lists['end_minute'] = '00';


        $config = JFactory::getConfig();
        $offset = $config->get("config.offset");
        $time   = JobsHelperDateTime::getNow();
        $add_days = JobsHelperDateTime::apendDays($time, $cfg->default_end_date_offset);

        $now = JobsHelperDateTime::isoDateToUTC($time + 3600*$offset);
        $end = JobsHelperDateTime::isoDateToUTC($add_days);

        $timestamp_start = JobsHelperDateTime::DateToIso($job->start_date);
        $timestamp_end = JobsHelperDateTime::DateToIso($job->end_date);

        $lists['start_date_html'] = JHtml::_('jobdate.calendar', ($job->start_date != null) ? $timestamp_start : $now, 'start_date', array('class' => 'cal-input','readonly' => 1), 'start_date');
        $lists['end_date_html'] = JHtml::_('jobdate.calendar', ($job->end_date != null) ? $timestamp_end : $end, 'end_date', array('class' => 'cal-input','readonly' => 1), 'end_date');

        if ($job->end_date) //start and end date are stored as ISO
        {
            if ($cfg->enable_hour) {
                $lists['end_hour'] = JHtml::date($job->end_date, 'H', false);
                $lists['end_minute'] = JHtml::date($job->end_date, 'i', false);
            }
        }

        $lists['tip_max_availability'] = "<span class='hasTip' title='" . sprintf(JText::_('COM_JOBS_JOB_MAX_AVAILABILITY'), $cfg->availability) . "'>
     									<img alt='Tooltip' src='" . JURI::root() . "components/" . APP_EXTENSION . "/images/tooltip.png' /></span>";

        $fields = CustomFieldsFactory::getFieldsList("jobs");
        $fields_html = JHtml::_('listJobs.displayfieldshtml', $job, $fields);
        //$fields_html = JHtml::_('customfields.displayfieldshtml', $job, $fields);

        $custom_fields_with_cat = $jobModel->getNrFieldsWithFilters();

        JFilterOutput::objectHTMLSafe($job, ENT_QUOTES);
        JTheFactoryEventsHelper::triggerEvent('onBeforeEditJob', array($job));

        $view = $this->getView('Job', 'html');
        $assigned = array();

        $model = JModelLegacy::getInstance('Job', 'jobsModel');
        $assignedStr = $model->getRequestedExperiences($job);

        if (!empty($assignedStr)) {
            $assigned = explode(',', $assignedStr);
        } else {
            $assigned[] = '1';
        }

        $lists['experience_request'] 	 = JHtml::_('experiencelevel.selectlist','experience_request[]','style="width:200px" multiple="multiple" size=7', $assigned);

        if ($id) {
            $lists['studies_request']         = JHtml::_('studieslevel.selectlist', 'studies_request', '', $job->studies_request); //full time, part time
        } else {
            $lists['studies_request']         = JHtml::_('studieslevel.selectlist', 'studies_request', '', null); //full time, part time
        }

        if ($task == "republish") {
            $job->id = null;
            $view->oldid = $id;
        }

        $lists['fieldRequiredlink'] = JHtml::image(JUri::root() . 'components/com_jobsfactory/images/requiredField.png', JText::_('COM_JOBS_REQUIRED'),'class="jobs_required"');

        $view->listing_enabled          = $listing_enabled;
        $view->allow_edit               = $allow_edit ;
        $view->job                      = $job;
        $view->user                     = $userprofile;
        $view->cfg                      = $cfg;
        $view->lists                    = $lists;
        $view->custom_fields_html       = $fields_html;
        $view->custom_fields            = $fields;
        $edit_24hours_info              = JText::_('COM_JOBS_EDIT_24HOURS');
        $view->edit_24hours_info        = $edit_24hours_info;
        $view->custom_fields_with_cat   = $custom_fields_with_cat ? 1 : 0;
        $view->terms_and_conditions     = (strip_tags($cfg->terms_and_conditions)) ? 1 : 0;

        if ($cfg->enable_location_management && $id) {
            $view->job_location_state = $job_location_state;
        }

        JHtml::_('behavior.calendar');
        JHtml::_('bootstrap.tooltip');
        JHtml::_('behavior.multiselect');
        JHtml::_('dropdown.init');
        JHtml::_('formbehavior.chosen', 'select');

        $view->display("t_editjob.tpl");

    }

    function Save()
    {

        if (isset($_POST['cancel'])) {
            $this->setRedirect(JobsHelperRoute::getJobListRoute());
            return null;
        }

        $id = $this->input->getInt('id');

        $job = JTable::getInstance('jobs', 'Table');
        $model = JModelLegacy::getInstance('Job', 'jobsModel');
        //$model->bind(JTheFactoryHelper::getRequestArray('post'), $_FILES);

        $model->bindJob($job);

        JTheFactoryEventsHelper::triggerEvent('onBeforeSaveJob', array($job));
        $err = $model->saveJob($job);

        if (count($err) || !$job->id) {
            //Some errors!
            $job->saveToSession();

            $err_message = implode('<br/>', $err);
            if ($id)
                $this->setRedirect(JobsHelperRoute::getJobEditRoute($id), $err_message);
            else
                $this->setRedirect(JobsHelperRoute::getNewJobRoute(), $err_message);

            JTheFactoryEventsHelper::triggerEvent('onAfterSaveJobError', array($job, $err));
        } else {
            $_POST['isNew'] = !$id ? 1 : 0;

            JTheFactoryEventsHelper::triggerEvent('onAfterSaveJobSuccess', array($job, $err));

            $job->clearSavedSession();
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($job->id), JText::_("COM_JOBS_JOB_SAVED"));
        }
    }

    function RefreshCity()
    {
        $parentid = $this->input->getInt('locationid');
        $db = JFactory::getDbo();
        //if ($enabled_only)
        $filter = "`status`=1 and ";
        $db->setQuery("SELECT c.* FROM `#__jobsfactory_cities` c WHERE {$filter} `location`=" . $parentid . " order by `ordering`");

        $cities = $db->loadObjectList();
        $options = array();

        foreach ($cities as $city)
        {
            $city->depth = 1;
            $spacer = str_pad('', ($city->depth - 1) * 3, '-');
            $options[] = JHtml::_('select.option', $city->id, $spacer . stripslashes($city->cityname));
        }

        if (!empty($options)) {
            $html_tree = JHtml::_('select.genericlist', $options, 'job_cityid', "", 'value', 'text', $options[0]->value);
        } else {
            $html_tree = '<input class="inputbox required" type="text" size="60" name="job_cityname" value="" alt="job_cityname">';
        }

        echo $html_tree;
    }

    function RefreshLocation() {
        $parentName = $this->input->getCmd('countryid');
        $db = JFactory::getDbo();
        //if ($enabled_only)
        $filter = "`status`=1 and ";
        $db->setQuery("SELECT l.* FROM `#__jobsfactory_locations` l WHERE {$filter} `country`='" . $parentName . "' order by `ordering`");

        $locations = $db->loadObjectList();
        $options = array();

        foreach ($locations as $location)
        {
            $location->depth = 1;
            $spacer = str_pad('', ($location->depth - 1) * 3, '-');
            $options[] = JHtml::_('select.option', $location->id, $spacer . stripslashes($location->locname));
        }

        if (!empty($options)) {
            //$html_tree = JHtml::_('select.genericlist', $options, 'job_locationid', "", 'value', 'text', $options[0]->value);
            $html_tree = JHtml::_('select.genericlist', $options, 'job_location_state', "onchange='jobsRefreshCities(this);'", 'value', 'text', $options[0]->value);
        } else {
            $html_tree = '';
        }

        echo $html_tree;
    }

    function RefreshCategory()
    {
        $id = $this->input->getInt('id');
        $oldid = $this->input->getInt('oldid');

        $job = JTable::getInstance('jobs', 'Table');
        $model = JModelLegacy::getInstance('Job', 'jobsModel');
        $model->bindJob($job);
        $job->saveToSession();

        if ($oldid)
            $this->setRedirect(JobsHelperRoute::getJobRepublishRoute($oldid));
        elseif ($id)
            $this->setRedirect(JobsHelperRoute::getJobEditRoute($id));
        else
            $this->setRedirect(JobsHelperRoute::getNewJobRoute());
    }

    function CancelJob()
    {
        $id = $this->input->getInt('id');
        $job = JTable::getInstance('jobs', 'Table');
        // load the row from the db table
        $job->load($id);

        if (!$job->isMyJob()) {
            JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_THIS_JOB_DOES_NOT_BELONG_TO_YOU"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($id));
            return;
        }
        if ($job->close_by_admin) {
            $this->setRedirect(JobsHelperRoute::getJobListRoute(), JText::_("COM_JOBS_THIS_JOB_WAS_BANNED_BY_THE_SITE_ADMINISTRATOR"));
            return;
        }

        $job->close_offer = 1;
        $job->cancel_reason = $this->input->getString('cancel_reason', '');
        $job->closed_date = gmdate('Y-m-d H:i:s');
        JTheFactoryEventsHelper::triggerEvent('onBeforeCancelJob', array($job));

        if ($job->store(true)) {
            JTheFactoryEventsHelper::triggerEvent('onAfterCancelJob', array($job));
            $msg = JText::_("COM_JOBS_JOB_WAS_CANCELED");
        } else {
            $msg = JText::_("COM_JOBS_JOB_COULD_NOT_BE_CANCELED");
        }
        $this->setRedirect(JobsHelperRoute::getJobDetailRoute($id), $msg);
    }

    function ViewApplications()
    {
        $id = $this->input->getInt('id');

        $job = JTable::getInstance('jobs', 'Table');
        $my =  JFactory::getUser();
        $cfg = JTheFactoryHelper::getConfig();
        $app = JFactory::getApplication();

        JTheFactoryHelper::tableIncludePath('category');

        if (!$job->load($id)) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_DOES_NOT_EXIST"),'warning');
            return;
        }
        if ($job->close_by_admin) {
            $this->setRedirect(JobsHelperRoute::getJobListRoute(), JText::_("COM_JOBS_THIS_JOB_WAS_BANNED_BY_THE_SITE_ADMINISTRATOR"));
            return;
        }
        if ($cfg->admin_approval && !$job->approved && !$job->isMyJob()) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobListRoute(null,false));
            return;
        }

        if (JobsHelperDateTime::dateDiff($job->start_date) < 0 && !$job->isMyJob()) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobListRoute(null,false));
            return;
        }

        if (!$job->isMyJob())
		{
            $job->hit();
		}

		if ($job->isMyJob())
		{
			$job->messages = $job->getMessages();
		}


        $modelCompanyUser   = JModelLegacy::getInstance('Company', 'jobsModel');
        $modelUser          = JModelLegacy::getInstance('User', 'jobsModel');
        $user               = $modelCompanyUser->getCompanyData($job->userid);

		if (isset($user->country)) {
			$countrytable = JTable::getInstance('country', 'Table');
			$user->country      = $countrytable->getCountryName($user->country);
		} else {
			$user->country      = '';
		}
		
        // get application list for a job_id = $id
        $jobs = $job->getApplicationList();
        $lists['companyName'] = $job->getCompany($job->userid);

        if (!$lists['companyName'])
                $lists['companyName'] = '';

        $lists['jobType'] = $job->getJobType($job->job_type);

        $arrayExperienceRequested = $job->getExperienceRequested($job->experience_request);
        $lists['jobExperienceRequested'] = $job->getExperienceRequested($job->experience_request);
        $lists['jobStudiesRequested'] = $job->getStudiesRequested($job->studies_request);

        $locationModel = JobsHelperTools::getLocationModel();
        $cityModel = JobsHelperTools::getCityModel();
        if ($cfg->enable_location_management) {
            $lists['job_location_state'] = $locationModel->getLocationName($job->job_location_state);
            $lists['job_location_city'] = $cityModel->getCityName($job->job_cityid);
        } else {
            $lists['job_location_city'] = $job->job_cityname;
        }

        // user has applied
        $lists['hasApplied'] = $modelUser->hasApplied($job->id, $my->id);

        $lists['isCompany'] = JobsHelperTools::isCompany($my->id);
        $lists['isCandidate'] = JobsHelperTools::isCandidate($my->id);

        /* JOOMLA DOCUMENT Meta information */
        $doc = JFactory::getDocument();
        $doc->setTitle($job->title);
        $doc->setMetaData('description', strip_tags($job->shortdescription));
        $doc->setMetaData('abstract', strip_tags($job->description));
        $doc->setMetaData('keywords', $job->get('tags'));


		// set OpenGraph data.
		$dispatcher = JEventDispatcher::getInstance();

		$og_data = array();
		$og_data['description'] = $job->shortdescription;
		$og_data['title'] = $job->title;
		$og_data['type'] = 'article';
		$og_data['other']['article:published_time'] = $job->start_date;

		$dispatcher->trigger('ogBasicData', $og_data);

		$app = JFactory::getApplication();
        $pathway = $app->getPathway();
        $pathway->addItem($job->title, JobsHelperRoute::getJobDetailRoute($job->id));

        if ($cfg->enable_countdown) {
            $js_declaration = "var days='".JText::_('ADS_DAYS').",';	var expired='".JText::_('COM_JOBS_EXPIRED')."';var nrcounters=1;";
            $doc->addScriptDeclaration( $js_declaration );

           if ($job->id) {
              JobsHelperDateTime::process_countdown($job);
           }
        }

        $lists['location_url'] = JUri::getInstance();

        $view = $this->getView('Job', 'html');

        $view->terms_and_conditions = (strip_tags($cfg->terms_and_conditions)) ? 1 : 0;
        $view->job = $job;
        $view->employer = $user;
        $view->jobs = $jobs;
        $view->lists = $lists;

        $view->display("t_jobdetails.tpl");
    }


    /**
     *              SEARCH and LISTING tasks
     */
    function Show_Search()
    {
        JHtml::_('behavior.calendar');

        $cfg = JTheFactoryHelper::getConfig();
        $reload = $this->input->get('reload');
        $r = new JObject();

        if ($reload) {
            $session = JFactory::getSession();
            $r = $session->get('registry');
        }

        $levelnames = JobsHelperUser::getExperienceLevels();
        $emptyopt[] = JHTML::_('select.option', '', JText::_("COM_JOBS__ANY_LEVEL"));
        $levelnames = array_merge($emptyopt, $levelnames);

        $locationModel = JobsHelperTools::getLocationModel();
        $lists['cats'] = JHtml::_('factorycategory.select', 'cat', '', $r->get('com_jobsfactory.model_Jobs.filters.cat'), false, false, true);
        $lists['users'] = JHtml::_('jobsuser.selectlist', 'userid', '', $r->get('com_jobsfactory.model_Jobs.filters.userid'));
        $lists['country'] = JHtml::_('country.selectlist', 'country', 'id="country"', $r->get('com_jobsfactory.model_Jobs.filters.country'),true);
        $lists['afterd'] = JHtml::_('jobdate.calendar', JobsHelperDateTime::DateToIso($r->get('com_jobsfactory.model_Jobs.filters.afterd')), 'afterd', array('class' => 'cal-input', 'readonly' => 1), 'afterd');
        $lists['befored'] = JHtml::_('jobdate.calendar', JobsHelperDateTime::DateToIso($r->get('com_jobsfactory.model_Jobs.filters.befored')), 'befored', array('class' => 'cal-input', 'readonly' => 1), 'befored');
        $lists['job_type'] = JHtml::_('jobtype.selectlist', 'job_type', '', $r->get('com_jobsfactory.model_Jobs.filters.job_type')); //full time, part time
        $lists['experience_request'] = JHtml::_('select.genericlist',$levelnames,'experience_request','style="width:200px" ','value','text',$r->get('com_jobsfactory.model_Jobs.filters.experience_request'));
        $lists['keyword'] = $r->get('com_jobsfactory.model_Jobs.filters.keyword');
        $lists['in_description'] = $r->get('com_jobsfactory.model_Jobs.filters.in_description');
        $lists['in_archive'] = $r->get('com_jobsfactory.model_Jobs.filters.in_archive');
        //$lists['filter_archive'] = $r->get('com_jobsfactory.model_Jobs.filters.filter_archive');

        if ($cfg->enable_location_management) {
            $lists['states'] = JHtml::_('factorylocation.select', 'job_location_state', "onchange='jobsRefreshCities(this);'", '', false); //$city->country
            $lists['city'] = JHtml::_('factorycity.select', 'job_cityid', "", '', false);
        } else {
            $lists['city'] = $r->get('com_jobsfactory.model_Jobs.filters.city');
        }

        $lists['studies_request'] = JHtml::_('studieslevel.selectlist', 'studies_request', '', $r->get('com_jobsfactory.model_Jobs.filters.studies_request'));

        $fields = CustomFieldsFactory::getSearchableFieldsList('jobs');
        $fields_html = JHtml::_('customfields.displaysearchhtml', $fields);

        //$links = array();
        $Itemid = JobsHelperTools::getMenuItemByTaskName(array('task' => 'listjobs'));

        //$links['show_search']       = JRoute::_('index.php?option=com_jobsfactory&task=show_search&Itemid=' . $Itemid,true);
        //$links['searchonmap']       = JRoute::_('index.php?option=com_jobsfactory&task=googlemaps&controller=maps&search=1&Itemid=' . $Itemid,true);
       // $links['searchcompanies']   = JRoute::_('index.php?option=com_jobsfactory&task=searchcompanies&controller=user&Itemid=' . $Itemid,true);
        //$links['searchusers']       = JRoute::_('index.php?option=com_jobsfactory&task=searchusers&controller=user&Itemid=' . $Itemid,true);

        $view = $this->getView('Search', 'html');
        $view->lists = $lists;
        $view->custom_fields_html = $fields_html;
        $view->custom_fields = $fields;
        //$view->links = $links;

        if ($cfg->enable_location_management) {
            $view->job_location_state = $lists['states'];
        }

        JHtml::_('formbehavior.chosen', 'select');
        $view->display("t_search.tpl");
    }

    function listjobs()
    {
        $format = $this->input->get('format');
        $task = $this->input->getCmd('task');

        if ($format == 'feed')
            $view = $this->getView('Jobs', 'feed');
        else
            $view = $this->getView('Jobs', 'html');

        $view->task = $task;
        $view->display('t_listjobs.tpl');
    }

    function listcompanies()
    {
        $format = $this->input->get('format');
        $task = $this->input->getCmd('task');
        $user = JFactory::getUser();

        if ($format == 'feed')
            $view = $this->getView('Companies', 'feed');
        else {

            $view = $this->getView('Companies', 'html');

            require_once(JPATH_COMPONENT_SITE . DS . "helpers" . DS . "user.php");
            $cfg = JTheFactoryHelper::getConfig();
            $model = JModelLegacy::getInstance('Companies', 'jobsModel');

            JTheFactoryHelper::tableIncludePath('category');

            $items = $model->loadItems();
            $companies_list = array();

            for ($key=0; $key < count($items); $key++)
               {

                   $profile = JobsHelperTools::getUserProfileObject();
                   $profile->getUserProfile($items[$key]->userid);
                   //$profile->getUserProfile($items[$key]->companyId);

                   $tableName = $profile->getIntegrationTable();
                   $tableKey = $profile->getIntegrationKey();

                   if ($profile->profile_mode == 'cb') {
                       $company = clone $profile;
                   } else {
                       $company = JTable::getInstance('users','Table');
                   }

                   $modelCompany = JModelLegacy::getInstance('Company', 'jobsModel');
                   $assigned = $modelCompany->getAssignedCats($items[$key]);

                   if ($assigned) {
                       $company->field_of_activity = jobsHelperUser::getCompanyActivityFields(implode(',',$assigned));
                   }

                   if ($profile->profile_mode == 'cb') {
                       $my = JFactory::getUser();
                       if ($my->id && $my->id == $items[$key]->$tableKey) $company->is_my_company = 1;
                       else $company->is_my_company = 0;

                   } else {
                       $company->is_my_company = $company->isMyCompany();
                       $company->bind($items[$key]);
                   }

                   $company->no_available_jobs      = JobsHelperUser::getCompanyAvailableJobs($company->userid);

                   if ($cfg->jobs_allow_rating) {
                       //$company_ratings_info = JobsHelperUser::getCompanyRatings($company->userid);
                       //$company->user_rating = $company_ratings_info->user_rating;  //$items[$key]->user_rating
                       //$company->users_count = $company_ratings_info->users_count;  //$items[$key]->users_count
                       $company->user_rating = $items[$key]->user_rating;
                       $company->users_count = $items[$key]->users_count;
                       $company->my_rating_user = JobsHelperUser::getIsMyRating($user->id,$company->userid); //$items[$key]->my_rating_user
                   }

                   $last_applied = JobsHelperTools::getCandidateLastApplied($company->userid);
                   $company->last_applied = JHtml::date($last_applied, $cfg->date_format, false);

                   if (isset($items[$key]->watchListed_flag)) {
                       if ($items[$key]->watchListed_flag) {
                           $company->link_watchlist = JobsHelperRoute::getDelToCompanyWatchlist($items[$key]->$tableKey, false);
                           $company->watchListed_flag = $items[$key]->watchListed_flag;

                       } else {
                           $company->link_watchlist = JobsHelperRoute::getAddToCompanyWatchlist($items[$key]->$tableKey, false);
                           $company->watchListed_flag = 0;
                       }
                   }
                   $companies_list[]=$company;
       		}

            $view->companies_rows = $companies_list;
            $view->pagination = $model->get('pagination');
        }

        $view->task = $task;
        $view->user = $user;

        $view->jobs_allow_rating = $cfg->jobs_allow_rating;

        $view->display('t_listcompanies.tpl');
    }

    function listcandidates()
    {
        $format = $this->input->get('format');
        $task = $this->input->getCmd('task');

        if ($format == 'feed')
            $view = $this->getView('Candidates', 'feed');
        else
            $view = $this->getView('Candidates', 'html');

        JTheFactoryHelper::modelIncludePath('payments');
        $currency = JModelLegacy::getInstance('currency', 'JTheFactoryModel');
        $lists['default_currency'] = $currency->getDefault();

        $view->task = $task;
        $view->lists = $lists;
        $view->display('t_listcandidates.tpl');
    }

    function companylistcandidates()
    {
        $format = $this->input->get('format');
        $task = $this->input->getCmd('task');
        $job_id =  $this->input->getInt('id',0);

        $r = new JObject();
        $session = JFactory::getSession();
        $r = $session->get('registry');

        $lists['filter_candidates'] = $r->get('com_jobsfactory.model_Jobs.filters.filter_candidates');

        if ($format == 'feed')
            $view = $this->getView('Candidates', 'feed');
        else
            $view = $this->getView('Candidates', 'html');

        JTheFactoryHelper::modelIncludePath('payments');
        $currency = JModelLegacy::getInstance('currency', 'JTheFactoryModel');
        $lists['default_currency'] = $currency->getDefault();

        $view->task = $task;
        $view->job_id = $job_id;
        $view->lists = $lists;
        $view->display('t_complistcandidates.tpl');
    }

    function listlocations()
    {
        $format = $this->input->get('format');
        $task = $this->input->getCmd('task');
        $cfg = JTheFactoryHelper::getConfig();

        if ($cfg->enable_location_management) {
			

            $filter_location = $this->input->getInt("location", 0);
            $depth_level = 1;
            $user = JFactory::getUser();

			// This is hardcoded to the ADMIN model, and it breaks frontend model loading.
            $locationModel = JobsHelperTools::getLocationModel();
			JModelLegacy::addIncludePath(JPATH_COMPONENT.'/models/');
			
            if (!$locationModel->getLocationCount($filter_location)) {
                $this->setRedirect(JobsHelperRoute::getJobListRoute(array('location' => $filter_location)));
                return;
            }

            if ($format == 'feed')
                $view = $this->getView('Location', 'feed');
            else
                $view = $this->getView('Location', 'html');

            $current_location = null;
            if ($filter_location) {
                $current_location = JobsHelperTools::getLocationTable();
                $current_location->load($filter_location);
            }

            $db = JFactory::getDbo();
            $db->setQuery("SELECT c.`id` as countrid, c.`name` as country
                                FROM `#__" . APP_PREFIX . "_country` c
                                WHERE `active`=1 order by `name` ");

            $countries = $db->loadObjectList('countrid');

            $JobsLocationModel = JModelLegacy::getInstance('Location', 'jobsModel');
            $locations = $JobsLocationModel->getJobsLocationTree($filter_location, $depth_level,$user->id,true);

            $JobsCityModel = JModelLegacy::getInstance('City', 'jobsModel');
            $cities = $JobsCityModel->getJobsCityTree($filter_location, $depth_level,$user->id,true);

            $countryId_arr = array();
            JobsHelperView::prepareLocationTree($countries,$locations,$cities,$task,$countryId_arr);

            $view->task = $task;
            $view->locations = $locations;
            $view->cities = $cities;
            $view->countries = $countries;
            $view->countryId_arr = $countryId_arr;
            $view->current_location = $current_location;

            $view->display('t_listlocations.tpl');
        }
        else
            JJobsFactoryController::listcities();
    }

    function listcities()
    {
        $format = $this->input->get('format');
        $task = $this->input->getCmd('task');
        $cfg = JTheFactoryHelper::getConfig();
        $filter_city = $this->input->getWord("locationcity", '');
        $depth_level = 1;
        $user = JFactory::getUser();
        $filter_city = $this->input->getInt("city", 0);

        if ($format == 'feed')
            $view = $this->getView('Location', 'feed');
        else
            $view = $this->getView('Location', 'html');

        $current_city = null;

        if ($filter_city) {
            $current_city = JobsHelperTools::getCityTable();
            $current_city->load($filter_city);
        }


        $JobsCityModel = JModelLegacy::getInstance('City', 'jobsModel');
        $cities = $JobsCityModel->getJobsCityList($depth_level,$user->id,true);

        JobsHelperView::prepareCityTree($cities,$task);

        $view->task =$task;
        $view->cities = $cities;
        $view->current_city = $current_city;

        $view->display('t_listcities.tpl');
    }


    function saveactions() {

        $jobid =  $this->input->getInt('jobid',0);
        $candidate = JTable::getInstance('candidates', 'Table');

        foreach ($_REQUEST as $k=>$v){
            if (substr($k,0,6)=='action'){
                $candidateid = substr($k,6);
                $candidate->load($candidateid);
                $candidate->action = $v;
                $candidate->store();
            }
        }

        $this->setRedirect(JobsHelperRoute::getCompanyCandidatesRoute($jobid,'companylistcandidates'));
    }

    function myapplications()
    {
        $view = $this->getView('Jobs', 'html');
        $view->display('t_myapplications.tpl');
    }

    function jobapplications()
    {
        $view = $this->getView('User', 'html');
        $view->display('t_jobapplications.tpl');
    }

    function myjobs()
    {
        $view = $this->getView('Jobs', 'html');
        $cfg = JTheFactoryHelper::getConfig();

        $view->new_job_link = JobsHelperRoute::getNewJobRoute();
        $view->cfg = $cfg;

        $view->display('t_myjobs.tpl');
    }

    /**
     *          APPLICATIONS
     *
     */
    function SendJobApplication()
    {
        $job_id = $this->input->getInt('id');

        $my = JFactory::getUser();
        $app = JFactory::getApplication();
        $job = JTable::getInstance('jobs', 'Table');

        if (!$job->load($job_id)) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobListRoute(null, false));
            return;
        }
        if ($job->userid == $my->id) {
            $app->enqueueMessage(JText::_("COM_JOBS_NOT_ALLOWED_TO_APPLY_OWN_OFFERS"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($job_id, null, false));
            return;
        }
        if ($job->close_offer) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_IS_CLOSED"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($job_id, null, false));
            return;
        }
        if ($job->published != 1) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($job_id, null, false));
            return;
        }
        if (JobsHelperDateTime::dateDiff($job->start_date) < 0) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobListRoute(null, false));
            return;
        }
        if ($job->close_by_admin) {
            $this->setRedirect(JobsHelperRoute::getJobListRoute(null, false), JText::_("COM_JOBS_THIS_JOB_WAS_BANNED_BY_THE_SITE_ADMINISTRATOR"));
            return;
        }

        $candidate = JTable::getInstance('candidates', 'Table');
        $candidate->userid = $my->id;
        $candidate->job_id = $job_id;
        $candidate->modified = gmdate('Y-m-d H:i:s');
        $candidate->accept = 0;
        $candidate->cancel = 0;

        JTheFactoryEventsHelper::triggerEvent('onBeforeSaveApplication', array($job, $candidate));
        $candidate->store();

        JTheFactoryEventsHelper::triggerEvent('onAfterSaveApplication', array($job, $candidate));
        $id_msg = $this->input->getInt('idmsg', null);

        $this->setRedirect(JobsHelperRoute::getJobDetailRoute($job_id, null, false), JText::_("COM_JOBS_SUCCESS"));
    }

    function Accept()
    {
        $app = JFactory::getApplication();
        $job_id = $this->input->getInt('jobid', 0);

        $job = JTable::getInstance('jobs', 'Table');
        $candidate = JTable::getInstance('candidates', 'Table');

        if (!$candidate->load($job_id)) {
            $app->enqueueMessage(JText::_("COM_JOBS_SELECT_APPLICATION_TO_ACCEPT"),'warning');
            return;
        }
        if ($candidate->cancel) {
            $app->enqueueMessage(JText::_("COM_JOBS_APPLICATION_WAS_CANCELED"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($candidate->job_id, null, false));
            return;
        }

        if (!$job->load($candidate->job_id)) {
            echo JText::_("COM_JOBS_JOB_DOES_NOT_EXIST");
            $app->enqueueMessage(JText::_("COM_JOBS_THE_APPLICATION_IS_NOT_VALID"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($candidate->job_id, null, false));
            return;
        }

        if (!$job->isMyJob()) {
            $app->enqueueMessage(JText::_("COM_JOBS_THE_JOB_DOES_NOT_BELONG_TO_YOU"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($candidate->job_id, null, false));
            return;
        }
        if ($job->close_offer) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_IS_CLOSED"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($candidate->job_id, null, false));
            return;
        }
        if ($job->published != 1) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_IS_NOT_PUBLISHED"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($candidate->job_id, null, false));
            return;
        }
        if ($job->close_by_admin) {
            $this->setRedirect(JobsHelperRoute::getJobListRoute(), JText::_("COM_JOBS_THIS_JOB_WAS_BANNED_BY_THE_SITE_ADMINISTRATOR"));
            return;
        }


        $user1 = JTable::getInstance('user', 'Table');
        if (!$user1->load($candidate->userid) || $user1->block) {
            $app->enqueueMessage(JText::_("COM_JOBS_USER_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($candidate->job_id, null, false));
            return;
        }

        @ignore_user_abort(true);

        $job->close_offer = 1;

        JTheFactoryEventsHelper::triggerEvent('onBeforeAcceptApplication', array($job, $candidate));

        $job->store(true);

        $candidate->accept = 1;
        $candidate->store();

        JTheFactoryEventsHelper::triggerEvent('onAfterAcceptApplication', array($job, $candidate));

        $this->setRedirect(JobsHelperRoute::getJobDetailRoute($job->id, null, false), JText::_("COM_JOBS_APPLICATION_ACCEPTED"));
    }


    function Terms_and_Conditions()
    {
        $cfg = JTheFactoryHelper::getConfig();
        echo $cfg->terms_and_conditions;
    }

    function Report_Job()
    {
        $app = JFactory::getApplication();
        $job_id = $this->input->getInt('id');
        $job = JTable::getInstance('jobs', 'Table');

        if (!$job->load($job_id)) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobListRoute(null, false));
            return;
        }
        if ($job->published != 1) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_IS_NOT_PUBLISHED"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($job_id, null, false));
            return;
        }
        if ($job->close_by_admin) {
            $this->setRedirect(JobsHelperRoute::getJobListRoute(null, false), JText::_("COM_JOBS_THIS_JOB_WAS_BANNED_BY_THE_SITE_ADMINISTRATOR"));
            return;
        }

        $view = $this->getView('job', 'html');
        $view->job = $job;

        $view->display("t_reportjob.tpl");
    }

    function editExperience()
    {
        $experience_id = $this->input->getInt('id');
        $resume_id = $this->input->getInt('resume_id'); //resume_id = user->id
        $Itemid = $this->input->getInt('Itemid', 0);
        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile();

        $user_experience = JTable::getInstance('userexperience', 'Table');

        if (!$user_experience->load($experience_id)) {
            JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_EXPERIENCE_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getUserdetailsRoute(null, false));
            return;
        }

        if ($resume_id && $experience_id)
            //second part of the IF is for unpublished resumes due to lack of credits
            $lists['domain'] = JHtml::_('factorycategory.select', 'domain', "", $user_experience->domain);
        else
            $lists['domain'] = JHtml::_('factorycategory.select', 'domain', "", 1);

        $lists['exp_start_date_html'] = JHtml::_('jobdate.calendar', $user_experience->start_date, "start_date$user_experience->id", array('class' => 'cal-input', 'readonly' => 1), "start_date$user_experience->id");
        $lists['exp_end_date_html'] = JHtml::_('jobdate.calendar', $user_experience->end_date, "end_date$user_experience->id", array('class' => 'cal-input', 'readonly' => 1), "end_date$user_experience->id");
        $lists['cancel_form'] = JRoute::_( 'index.php?option=com_jobsfactory&controller=user&amp;task=userdetails&amp;Itemid='. $Itemid );

        $view  = $this->getView('resume','html' );

        JHtml::_('behavior.framework', true);
        JHtml::_('behavior.calendar');
        JHtml::_('bootstrap.tooltip');

        $view->user_experience = $user_experience;
        $view->resume_id = $resume_id;
        $view->lists = $lists;

        $view->display("elements/resumedetail/t_editexperience.tpl");
    }

    function editEducation()
    {
        $education_id = $this->input->getInt('id');
        $resume_id = $this->input->getInt('resume_id');//resume_id = user->id
        $Itemid = $this->input->getInt('Itemid', 0);
        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile();

        $user_education = JTable::getInstance('education', 'Table');

        if (!$user_education->load($education_id)) {
            JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_EDUCATION_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getUserdetailsRoute(null, false));
            return;
        }

        if ($resume_id && $education_id) {
            $lists['studies_level'] = JHtml::_('studieslevel.selectlist', 'study_level', '', $user_education->study_level); //full time, part time
        } else
            $lists['studies_level'] = JHtml::_('studieslevel.selectlist', 'study_level', '', null); //full time, part time

        $lists['country']               = JHtml::_('country.selectlist', 'country', 'id="country"', $userprofile->country, false);
        $lists['edu_start_date_html']   = JHtml::_('jobdate.calendar', $user_education->start_date, "start_date$user_education->id", array('class' => 'cal-input', 'readonly' => 1), "start_date$user_education->id");
        $lists['edu_end_date_html']     = JHtml::_('jobdate.calendar', $user_education->end_date, "end_date$user_education->id", array('class' => 'cal-input', 'readonly' => 1), "end_date$user_education->id" );
        $lists['cancel_form']           = JRoute::_( 'index.php?option=com_jobsfactory&controller=user&amp;task=userdetails&amp;Itemid='. $Itemid );

        $doc = JFactory::getDocument();
        $doc->addScript(JUri::root() . 'components/' . APP_EXTENSION . '/js/date.js');
        JHtml::_('behavior.framework', true);
        JHtml::_('behavior.calendar');
        JHtml::_('behavior.tooltip');

        $view  = $this->getView('resume', 'html');

        $view->user_education = $user_education;
        $view->resume_id = $resume_id;
        $view->lists = $lists;

        $view->display("elements/resumedetail/t_editeducation.tpl");
    }

    function do_Report()
    {
        $app = JFactory::getApplication();
        $my = JFactory::getUser();
        $database = JFactory::getDBO();

        $job_id = $this->input->getInt('id');
        $message = $database->escape($this->input->getString('message', ''));

        $job = JTable::getInstance('jobs', 'Table');
        if (!$job->load($job_id)) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobListRoute(null, false));
            return;
        }
        if ($job->published != 1) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_IS_NOT_PUBLISHED"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobDetailRoute($job_id, null, false));
            return;
        }
        if ($job->close_by_admin) {
            $this->setRedirect(JobsHelperRoute::getJobListRoute(null, false), JText::_("COM_JOBS_THIS_JOB_WAS_BANNED_BY_THE_SITE_ADMINISTRATOR"));
            return;
        }
        if (!$message) {
            $app->enqueueMessage(JText::_("COM_JOBS_MESSAGE_CAN_NOT_BE_EMPTY"),'warning');
            $this->setRedirect(JobsHelperRoute::getReportJobRoute($job_id, false));
            return;
        }

        $reported = JTable::getInstance('Report_Jobs', 'Table');
        $reported->job_id = $job_id;
        $reported->userid = $my->id;
        $reported->message = $message;
        $reported->modified = gmdate('Y-m-d H:i:s');
        $reported->store();

        JTheFactoryEventsHelper::triggerEvent('onJobReported', array($job, $message));

        $this->setRedirect(JobsHelperRoute::getJobDetailRoute($job_id, null, false), JText::_("COM_JOBS_JOB_REPORTED"));
    }

    function saveExperience()
    {
        $experience_id = $this->input->getInt('id');
        $resume_id = $this->input->getInt('resume_id');//resume_id = user->id

        $user_experience = JTable::getInstance('userexperience', 'Table');
        $model = JModelLegacy::getInstance('Userexperience', 'userexperienceModel');

        $model->bindUserexperience($user_experience);

        $err = $model->saveUserexperience($user_experience);

        if (count($err) || !$user_experience->id) {
            //Some errors!
            $err_message = implode('<br/>', $err);

            if ($experience_id)
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($user_experience->userid, false), $err_message);
            else
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($user_experience->userid, false), $err_message);
                JTheFactoryEventsHelper::triggerEvent('onAfterSaveUserExperienceError', array($experience_id, $err));
        } else {

            $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($user_experience->userid, false), JText::_("COM_JOBS_EXPERIENCE_SAVED"));
            JTheFactoryEventsHelper::triggerEvent('onAfterSaveUserExperienceSuccess', array($resume_id, $err));
        }
    }

    /**
     *
     */
    public function delete_experience()
    {
        $experience_id = $this->input->getInt('id');
        $resume_id = $this->input->getInt('resume_id');//resume_id = user->id

        $user_experience = JTable::getInstance('userexperience', 'Table');
        $model = JModelLegacy::getInstance('Userexperience', 'userexperienceModel');

        if (!$user_experience->load($experience_id)) {
            JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_EXPERIENCE_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($user_experience->userid, false));
            return false;
        }

        if ($model->deleteUserexperience($experience_id,$resume_id)) {
          $msg = JText::_('COM_JOBS_EXPERIENCE_DELETED_SUCCESSFULLY');
        }
        else {
          $msg = JText::_('COM_JOBS_EXPERIENCE_ERR_DELETING');
          JFactory::getApplication()->enqueueMessage($model->getError(), 'error');
        }

        $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($resume_id, false), $msg);
        //$this->setRedirect(JobsHelperRoute::getJobListRoute(null, false), $msg);
        return true;
    }


    /*function saveEducation()
    {
		//exit;
        $education_id = $this->input->getInt('id');
        $resume_id = $this->input->getInt('resume_id');//resume_id = user->id

        $user_studylevel = JTable::getInstance('education', 'Table');
        $model = JModelLegacy::getInstance('Usereducation', 'usereducationModel');

        $model->bindUsereducation($user_studylevel);
        $err = $model->saveUsereducation($user_studylevel);

        if (count($err) || !$user_studylevel->id) {
            //Some errors!
            $err_message = implode('<br/>', $err);

            if ($education_id)
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($user_studylevel->userid, false), $err_message);
            else
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($user_studylevel->userid, false), $err_message);
                JTheFactoryEventsHelper::triggerEvent('onAfterSaveUserExperienceError', array($education_id, $err));
        } else {

            $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($user_studylevel->userid, false), JText::_("COM_JOBS_EDUCATION_SAVED"));
            JTheFactoryEventsHelper::triggerEvent('onAfterSaveUserEducationSuccess', array($resume_id, $err));
        }
    }*/

    function profile(){

        $userid = $this->input->getInt('id');
        $view = $this->getView('Profile', 'html');
        $user = JFactory::getUser();

        $view->userid = $userid;
        $view->user = $user;

        $view->display('t_groupselect.tpl');
    }

    function displayprofile() {
        $userid = $this->input->getInt('id');
        $user = JFactory::getUser();
        $view = $this->getView('Profile', 'html');

        $group_type = $this->input->getInt('group_type', null);
        if (isset($group_type)) {
            if ($group_type) {
                $view->group_type = JText::_("COMPANY_GROUP" );
            } else {
                $view->group_type = JText::_("CANDIDATE_GROUP" );
            }
        }
        $view->user = $user;

        $view->display('t_groupcompleted.tpl');
    }

}
