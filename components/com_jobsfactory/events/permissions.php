<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Events
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JTheFactoryEventPermissions extends JTheFactoryEvents
{
    function onBeforeExecuteTask(&$stopexecution)
    {
        $app = JFactory::getApplication();

        $task = $app->input->getCmd('task');
        $controllerClass = $app->input->getWord('controller');
        require_once(JPATH_SITE . '\components\com_jobsfactory\helpers\tools.php');
        $RBAcl = JobsHelperTools::getJobsACL();

        $app = JFactory::getApplication();
        $cfg = JTheFactoryHelper::getConfig();
        $user = JFactory::getUser();

        if (strpos($task, '.') !== FALSE) {
            $task = explode('.', $task);
            $controllerClass = $task[0];
            $task = $task[1];
        }

        if (in_array($task, $RBAcl->publicTasks) || $controllerClass == 'crontask')
            return; //Anon Task ok

        if (!$user->id ) {
            //By default tasks need to be done by logged users
            $app->redirect(JobsHelperRoute::getJobListRoute(),JText::_("COM_JOBS_YOU_NEED_TO_LOGIN_IN_ORDER_TO_ACCESS_THIS_SECTION"));
            $stopexecution = true;
            return;
        }

        //Only Logged user from now on
        if (empty($cfg->candidate_groups) || empty($cfg->company_groups)) {
            $isCompany = false;
            $isCandidate = false;
        } else {
            $isCompany = JobsHelperTools::isCompany($user->id);
            $isCandidate = JobsHelperTools::isCandidate($user->id);
        }

        if (!$isCandidate && !$isCompany) {

			$usergrouptype = $app->input->get('usergrouptype', null);

			if ($usergrouptype != null)
			{
				$set_group = JobsHelperUser::setUserGroup();
				if ($set_group == true)
				{
					$app->redirect(JRoute::_('index.php?option=com_jobsfactory&controller=user&task=userdetails&id='.$user->id . '&Itemid=' . JFactory::getApplication()->input->getInt('Itemid')));
					$stopexecution = true;
					return;
				}
			}

            if ($RBAcl->taskmapping[$task] != 'registereduser') {
                $app->redirect(JobsHelperRoute::getProfileRoute($user->id, false));
                $stopexecution = true;
                return;
            } else {
                if ($RBAcl->profileTasks[$task] == 'registereduser' || in_array($task, $RBAcl->anonTasks)) {
                    $a = JobsHelperUser::setUserGroup();
                    if (!$a)  {
                        $app->redirect(JobsHelperRoute::getJobListRoute(),JText::_("COM_JOBS_YOU_NEED_TO_BE_A_COMPANY_OR_CANDIDATE_IN_ORDER_TO_ACCESS_THIS_SECTION"));
                    $stopexecution = true;
                    return;
                    }
                }
            }
        }

        if (!isset($RBAcl->taskmapping[$task]) && !in_array($task, $RBAcl->anonTasks))
           return; // no need to check other ACL Seller/Candidate taskmap

        $userprofile = null;
        if (!in_array($task, $RBAcl->anonTasks)) {
            //User must have his profile Filled for this task
            if ( ($isCandidate || $isCompany)) {
                 $userprofile = JobsHelperTools::getUserProfileObject();
            } else {
                if ($RBAcl->profileTasks[$task] != 'registereduser') {
                    $app->redirect(JobsHelperRoute::getProfileRoute($user->id, false));
                    $stopexecution = true;
                    return;

                } else {
                    $a = JobsHelperUser::setUserGroup();
                    if (!$a)  {
                        $app->redirect(JobsHelperRoute::getJobListRoute(),JText::_("COM_JOBS_YOU_NEED_TO_BE_A_COMPANY_OR_CANDIDATE_IN_ORDER_TO_ACCESS_THIS_SECTION"));
                    $stopexecution = true;
                    return;
                    }
                }
            }

            if (!$userprofile)
                $userprofile = JobsHelperTools::getUserProfileObject();

            if (!$userprofile->checkProfile($user->id)) {
                //Profile is not filled! we must redirect
                if (!$r = JobsHelperTools::redirectToProfile())
                    $r = JobsHelperRoute::getUserdetailsRoute(null, false);
                $app->redirect($r, JText::_("COM_JOBS_ERR_MORE_USER_DETAILS"));
                $stopexecution = true;
                return;
            }
        }

        if (!$userprofile)
            $userprofile = JobsHelperTools::getUserProfileObject();

        $userprofile->getUserProfile();

        if (isset($RBAcl->taskmapping[$task])) {
            if ($RBAcl->taskmapping[$task] == 'seller' && !$isCompany) {
                //Task allows only COMPANIES
                $app->redirect(JobsHelperRoute::getJobListRoute(),JText::_("COM_JOBS_YOU_NEED_TO_BE_A_COMPANY_IN_ORDER_TO_ACCESS_THIS_SECTION"));
                $stopexecution = true;
                return;
            }

            if ($RBAcl->taskmapping[$task] == 'candidate' && !$isCandidate) {
                //Task allows only CANDIDATES
                $app->redirect(JobsHelperRoute::getJobListRoute(),JText::_("COM_JOBS_YOU_NEED_TO_BE_A_CANDIDATE_IN_ORDER_TO_ACCESS_THIS_SECTION"));
                $stopexecution = true;
                $app->close();
                return;
            }
        }
    }

}
