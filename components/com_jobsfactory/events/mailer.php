<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Events
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JTheFactoryEventMailer extends JTheFactoryEvents
{
    function onAfterCancelJob($job)
    {
        $database = JFactory::getDbo();

        $q = "SELECT  u.* FROM  #__users u
              LEFT JOIN #__jobsfactory_candidates b on u.id=b.userid
			  WHERE
              b.userid is not null and
              b.job_id = '$job->id' AND
              b.cancel=0";
        $database->setQuery($q);
        $usermails = $database->loadObjectList();
        $query = "SELECT u.* from #__jobsfactory_watchlist w
            left join #__users u on w.job_id = u.id
            where w.job_id = '$job->id'";
        $database->setQuery($query);
        $watchlist_mails = $database->loadObjectList();

        $job->SendMails($watchlist_mails, 'job_watchlist_canceled');
        $job->SendMails($usermails, 'job_canceled');
    }

    function onAfterSaveApplication($job, $candidate)
    {

        if ($job->cancel)
		{
			return; //not published yet
		}

        $owner_user = JTable::getInstance("user");
        $owner_user->load($job->userid);
        $job->SendMails(array($owner_user), 'new_application');

        /*if ($job->employer_visibility == EMPLOYER_TYPE_PUBLIC) {

            $database = & JFactory::getDBO();
            $query = "SELECT u.* from #__jobsfactory_watchlist w " .
                " LEFT JOIN #__users u on w.userid=u.id " .
                " WHERE w.job_id='{$job->id}' ";

            $database->setQuery($query);
            $watches = $database->loadObjectList();
            $job->SendMails($watches, 'new_application');
        }*/
    }

    function onAfterSendMessage($job, $message)
    {
        $usr = JTable::getInstance("user");
        $usr->load($message->userid2);
        $job->SendMails(array($usr), "new_message");
    }

    function onAfterBroadcastMessage($job, $message)
    {
        $database = JFactory::getDBO();
        $database->setQuery("select distinct u.* from
                    #__users u
                    left join #__jobsfactory_messages m on m.userid1=u.id where job_id='{$job->id}'");
        $usr1 = $database->loadObjectList();

        $database->setQuery("select distinct u.* from
                    #__users u
                    left join #__jobsfactory_candidates b on b.userid=u.id where b.cancel=0 and job_id='{$job->id}'");
        $usr2 = $database->loadObjectList();

        $usr = array_merge($usr1, $usr2);
        $job->SendMails($usr, "new_broadcast_message");

    }

    function onAfterAcceptApplication($job, $candidate)
    {
        $database = JFactory::getDBO();
        $user1 = JFactory::getUser($candidate->userid);
        $job->SendMails(array($user1), 'application_accepted');

        $query = "SELECT DISTINCT u.*
                    FROM #__jobsfactory_candidates b
   		              LEFT JOIN #__users u ON b.userid=u.id
   		            WHERE b.cancel=0 AND b.accept=0 AND u.block=0
                      AND b.job_id='{$job->id}'
   		          ";
        $database->setQuery($query);
    }

    function onAfterSaveJobSuccess($job)
    {
        // Send emails only for new jobs
        $isNew = 1;
        if (isset($_POST['isNew'])) {
            $isNew = $_POST['isNew'] ? 1 : 0;
        }
        if (!$isNew) return null;

        $cfg        = JTheFactoryHelper::getConfig();
        $database   = JFactory::getDbo();
        $user       = JFactory::getUser();
        $job->SendMails(array($user), 'new_job_created');

        if ( $job->published  && (!$cfg->admin_approval || $job->approved) ) {
            $query = " SELECT u.* FROM #__users u
    				   LEFT JOIN #__jobsfactory_watchlist_cats f ON f.userid = u.id
    				   WHERE f.catid = '$job->cat' AND u.id <> '$user->id' ";
            $database->setQuery($query);
            $watches = $database->loadObjectList();
            $job->SendMails($watches, 'new_job_watch');

            $query = " SELECT u.* FROM #__users u
                       LEFT JOIN #__jobsfactory_watchlist_companies f ON f.userid = u.id
                       WHERE f.companyid = '$job->userid' AND u.id <> '$user->id' ";
            $database->setQuery($query);
            $watchesC = $database->loadObjectList();
            $job->SendMails($watchesC, 'new_job_watch_company');

            $query = " SELECT u.* FROM #__users u
                       LEFT JOIN #__jobsfactory_watchlist_loc f ON f.userid = u.id
                       WHERE f.locationid = '$job->job_location_state' AND u.id <> '$user->id' ";
            $database->setQuery($query);
            $watchesL = $database->loadObjectList();
            $job->SendMails($watchesL, 'new_job_watch_location');

            $query = " SELECT u.* FROM #__users u
                       LEFT JOIN #__jobsfactory_watchlist_city f ON f.userid = u.id
                       WHERE f.cityid = '$job->job_cityid' AND u.id <> '$user->id' ";
            $database->setQuery($query);
            $watchesL = $database->loadObjectList();
            $job->SendMails($watchesL, 'new_job_watch_city');

        }
        if ($cfg->admin_approval) {
            $query = " SELECT u.* FROM #__users u
    				   LEFT JOIN #__user_usergroup_map m ON m.user_id= u.id
    				   LEFT JOIN #__usergroups g ON m.group_id= g.id
 				   WHERE g.title='Super Users'
 				   OR g.title = 'Administrator' ";
            $database->setQuery($query);
            $admins = $database->loadObjectList();

            $job->SendMails($admins, 'job_admin_approval');
        }
    }

    function onAfterCloseJob($job)
    {
        $database = JFactory::getDbo();
        //Notify users who have jobs;
        $query = "SELECT DISTINCT u.*
                    FROM #__jobsfactory_candidates a
                    LEFT JOIN #__users u ON a.userid=u.id
                    WHERE a.job_id={$job->id} AND a.cancel=0";
        $database->setQuery($query);
        $users_with_apps = $database->loadObjectList();
        $job->SendMails($users_with_apps, 'job_closed');

        //Notify  Watchlist, Clean Watchlist
        $query = "select distinct u.*
                    from #__jobsfactory_watchlist a
                    left join #__users u on a.userid=u.id
                    where a.job_id={$job->id}";
        $database->setQuery($query);
        $users_with_watchlist = $database->loadObjectList();
        $job->SendMails($users_with_watchlist, 'job_watchlist_closed');

        $query = "delete from #__jobsfactory_watchlist where job_id={$job->id}";
        $database->setQuery($query);
        $database->execute();

    }

    function onAfterSaveResumeSuccess($resume)
    {
        $cfg        = JTheFactoryHelper::getConfig();
        $user       = JFactory::getUser();

        $resume->SendMails(array($user), 'new_resume_created');
    }
}
