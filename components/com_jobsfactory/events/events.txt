Available events for Jobs Factory:

onBeforeExecuteTask(&$stopexecution) - Called after framework init, ACL check, and before any Controller Execution
onAfterExecuteTask($controller) - Called after task execution with controller as param

onBeforeEditJob($job) - Called before edit view is loaded. for Edit/New/Republish
onBeforeCancelJob($job)
onAfterCancelJob($job)

onBeforeSaveJob($job)
onAfterSaveJobError($job,$error_array)
onAfterSaveJobSuccess($job)

onBeforeSaveApplication($job,$app)
onAfterSaveApplication($job,$app)

onBeforeSendMessage($job,$messageobject)
onAfterSendMessage($job,$messageobject)

onBeforeBroadcastMessage($job,$messageobject)
onAfterBroadcastMessage($job,$messageobject)

onAfterCloseJob($job)

onJobReported($job,$message)

onPaymentIPNError($paylog,$error_text)
onPaymentForOrder($paylog,$order) - called for status OK, MANUAL_CHECK and ERROR. Dispatcher must check if order is Completed!

onBeforeDisplay($task,$smarty)
onAfterDisplay()

onDefaultCurrencyChange() - called when default curreny changes. for now without parameters 
