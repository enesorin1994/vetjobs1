<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.4
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JobsfactoryConfig {
	public $employervisibility_enable = '1';
	public $employervisibility_val = '1';
	public $jobpublish_enable = '1';
	public $jobpublish_val = '0';
	public $date_format = 'Y-m-d';
	public $date_time_format = 'h:iA';
	public $nr_items_per_page = '10';
	public $inner_categories = '1';
	public $enable_countdown = '1';
	public $availability = '3';
	public $archive = '3';
	public $max_nr_tags = '5';
	public $allow_guest_messaging = '1';
	public $uploadresume_option = '1';
	public $enable_attach = '1';
	public $attach_compulsory = '0';
	public $attach_max_size = '1024';
	public $attach_extensions = 'zip,rar,txt';
	public $workflow = 'quick';
	public $max_picture_size = '2048';
	public $main_picture_require = '0';
	public $gallery = 'scrollgallery';
	public $thumb_width = '150';
	public $thumb_height = '150';
	public $medium_width = '500';
	public $medium_height = '500';
    public $candidate_groups = array();
    public $company_groups = array();
    public $google_key = '';
	public $googlemap_defx = '48.86471476180282';
	public $googlemap_defy = '2.28515625';
	public $googlemap_default_zoom = '7';
	public $googlemap_distance = '1';
	public $googlemap_unit_available = '5,25,60,100,150';
	public $googlemap_gx = '600';
	public $googlemap_gy = '450';
	public $googlemap_allowed_maps = array("0" => "Map", "1" => "Hybrid");
	public $terms_and_conditions = '<p>Place your terms and conditions here! <strong>Leave this field empty to remove the T&amp;C checkbox from the forms.</strong></p>';
	public $enable_hour = '1';
	public $admin_approval = '0';
	public $map_in_job_details = '0';
	public $profile_mode = 'component';
	public $theme = 'default';
	public $allow_messenger = '1';
	public $show_paypalemail = '1';
	public $allow_paypal = '1';
	public $cron_password = 'pass';
	public $googlemap_maptype = 'ROADMAP';
	public $candidatevisibility_val = '1';
	public $enable_location_management = '0';
	public $resume_extensions = 'pdf,txt';
	public $resumepublish_enable = '1';
	public $resumepublish_val = '1';
	public $resume_admin_approval = '0';
	public $choose_antispam_bot = 'joomla';
	public $mailcaptcha_public_key = '';
	public $mailcaptcha_private_key = '';
	public $enable_antispam_bot = '0';
	public $decimal_symbol = 'comma';
	public $decimals_number = '2';
	public $enable_images = '1';
}