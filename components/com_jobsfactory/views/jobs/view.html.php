<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');


class jobsViewjobs extends JobsSmartyView
{

	function display($template=null)
    {
        $app    = JFactory::getApplication();
		$model=JModelLegacy::getInstance('Jobs','jobsModel');

        $locationModel = JobsHelperTools::getLocationModel();
        $cityModel = JobsHelperTools::getCityModel();

        JTheFactoryHelper::tableIncludePath('category');

		$items = $model->loadItems();

        $jobs_list = array();
        
	    for ($key=0; $key < count($items); $key++)
        {
	    	$job = JTable::getInstance('Jobs','Table');
            $job->bind($items[$key]);

            $job->setCacheObject($items[$key]);
			$job->job_shortdescription = $items[$key]->job_shortdescription;
            $jobs_list[]=$job;
		}

        //$model->setState('filters.filter_archive', $app->getUserStateFromRequest('com_jobsfactory.filter_archive','archive', 'active'));

        $filter_order_Dir = $model->getState( 'filters.filter_order_Dir');
        $filter_order = $model->getState( 'filters.filter_order');
        $filter_archive = $model->getState('filters.filter_archive');
        $filter_myjobs = $model->getState('filters.filter_myjobs');
        $filter_watchlist = $model->getState('filters.filter_watchlist');
		$pagination = $model->get('pagination');
        $filters = $model->getFilters();

        $links = array();
        $links['filter_link_active'] = JobsHelperRoute::getJobListRoute(array('filter_archive' => ''));
        $links['filter_link_archive'] = JobsHelperRoute::getJobListRoute(array('filter_archive' => 'archive'));

        $links['filter_link_my_active'] = JobsHelperRoute::getMyJobListRoute(array('filter_myjobs' => 'active'));
        $links['filter_link_my_unpublished'] = JobsHelperRoute::getMyJobListRoute(array('filter_myjobs' => 'unpublished'));
        $links['filter_link_my_archive'] = JobsHelperRoute::getMyJobListRoute(array('filter_myjobs' => 'archive'));
        $links['filter_link_my_unapproved'] = JobsHelperRoute::getMyJobListRoute(array('filter_myjobs' => 'unapproved'));

		$this->job_rows         =  $jobs_list;
		$this->filter_order_Dir = $filter_order_Dir;
		$this->filter_order     = $filter_order;
		$this->filter_archive   = $filter_archive;
        $this->filter_myjobs    = ($filter_myjobs != null ) ? $filter_myjobs : 'active';
        $this->filter_watchlist = $filter_watchlist;
		$this->pagination       = $pagination;
        $this->inputsHiddenFilters = JHTML::_('listJobs.inputsHiddenFilters', $filters );
        $this->htmlLabelFilters = JHTML::_('listJobs.htmlLabelFilters', $filters, false );

        $this->links = $links;

        JHtml::_("behavior.modal");
        $document = JFactory::getDocument();
        $document->addScript(JUri::root().'components/com_jobsfactory/js/jobs.js',true);

		parent::display($template);
	}
}
