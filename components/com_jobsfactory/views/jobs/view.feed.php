<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class jobsViewjobs extends JViewLegacy
{

	function display($tpl=null) {
		
		$app	= JFactory::getApplication();
		// Get the page/component configuration

		$doc     = JFactory::getDocument();
		$siteEmail = $app->getCfg('mailfrom');

		$doc->link = JRoute::_("index.php?option=com_jobsfactory&view=jobs&format=feed");
		$model	= JModelLegacy::getInstance('Jobs','jobsModel');

		$items = $model->loadItems();

	    $catObj = JobsHelperTools::getCategoryTable();
	    
		if($items)
		
	    for ($key=0; $key<count($items); $key++){
            $job=$items[$key];
			
			$title = $this->escape( $job->title );
			$title = html_entity_decode( $title );
			
			$item = new JFeedItem();
			
			$item->title 		= $title;
			$item->link 		= JRoute::_(JURI::root()."index.php?option=com_jobsfactory&amp;&amp;task=viewapplications&amp;id=$job->id");
			$item->description 	= $job->description;
			$item->date			= $job->start_date;
			$catObj->load($job->category);
			$item->category   	= $catObj->catname;
			$item->author		= $job->user_id;
			$item->authorEmail = $siteEmail;

	    	$doc->addItem($item);
		}
		
	}

}
