<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class jobsViewApplications extends JobsSmartyView
{
    function display($tmpl=null)
    {
        JHtml::_("behavior.modal");
        $document = JFactory::getDocument();
        $document->addScript(JUri::root().'components/com_jobsfactory/js/jobs.js',true);

		$this->smarty->assign("page_title" , JText::_('COM_JOBS_APPLY_JOB'));

		parent::display($tmpl);
    }

}
