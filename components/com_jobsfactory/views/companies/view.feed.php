<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class jobsViewCompanies extends JViewLegacy
{

	function display($tpl=null) {
		
		$app	= JFactory::getApplication();
		// Get the page/component configuration

		$doc     = JFactory::getDocument();
		$siteEmail = $app->get('mailfrom');

		$doc->link = JRoute::_("index.php?option=com_jobsfactory&view=companies&format=feed");
		$model	= JModelLegacy::getInstance('Companies','jobsModel');

		$items = $model->loadItems();
	    $catObj = JobsHelperTools::getCategoryTable();
	    
		if($items)
		
	    for ($key=0; $key<count($items); $key++){
            $company=$items[$key];
			
			$title = $this->escape( $company->name );
			$title = html_entity_decode( $title );
			
			$item = new JFeedItem();
			
			$item->title 		        = $title;
			$item->link 		        = JRoute::_(JURI::root()."index.php?option=com_jobsfactory&amp;&amp;task=viewapplications&amp;id=$company->id");
			$item->description 	= $company->shortdescription;
			$item->date			        = $company->start_date;
			$item->author		= $company->user_id;
			$item->authorEmail = $siteEmail;

	    	$doc->addItem($item);
		}
		
	}

}
