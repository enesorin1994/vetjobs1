<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class jobsViewCompanies extends JobsSmartyView
{

	function display($template=null)
    {
        require_once(JPATH_COMPONENT_SITE . DS . "helpers" . DS . "user.php");
        $model = JModelLegacy::getInstance('Companies', 'jobsModel');

		JHtml::_("behavior.modal");
        $filter_order_Dir = $model->getState( 'filters.filter_order_Dir');
        $filter_order = $model->getState( 'filters.filter_order');
		$pagination = $model->get('pagination');
        $filters = $model->getFilters();

		$this->filter_order_Dir = $filter_order_Dir;
		$this->filter_order = $filter_order;

		$this->pagination = $pagination;
        $this->inputsHiddenFilters = JHTML::_('listJobs.inputsHiddenFilters', $filters );
        $this->htmlLabelFilters = JHTML::_('listJobs.htmlLabelFilters', $filters, false );

        $document = JFactory::getDocument();
        $document->addScript(JUri::root().'components/com_jobsfactory/js/jobs.js',true);

		parent::display($template);
	}
}
