<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class jobsViewProfile extends JobsSmartyView
{

	function display($template=null)
    {
        require_once(JPATH_COMPONENT_SITE . DS . "helpers" . DS . "user.php");
        $model = JModelLegacy::getInstance('Candidates', 'jobsModel');
        $pagination = $model->get('pagination');
		JHTML::_("behavior.modal");
        $doc = JFactory::getDocument();
        $doc->addScript(JUri::root() . 'components/' . APP_EXTENSION . '/js/jobs.js');



        $Itemid = $app = JFactory::getApplication()->input->getInt('Itemid');
        $attributes='';

        $opts = array();
        $opts[] = JHTML::_('select.option', 2, JText::_("COM_JOBS_CANDIDATE_GROUP"));
        $opts[] = JHTML::_('select.option', 1, JText::_("COM_JOBS_COMPANY_GROUP"));

        $radioUserGroup =  JHTML::_('select.radiolist',  $opts, 'usergrouptype', $attributes,  'value', 'text', USER_GROUP_CANDIDATES);

        $lists['cancel_form'] = JRoute::_( 'index.php?option=com_jobsfactory&amp;task=listjobs&Itemid='. $Itemid );

        $this->assign("radioUserGroup", $radioUserGroup);
        $this->assign("lists", $lists);

		parent::display($template);
	}
}
