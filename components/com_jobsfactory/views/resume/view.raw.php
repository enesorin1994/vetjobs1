<?php
/*------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class jobsViewResume extends JobsSmartyView
{
    function display($template)
    {
        JHTML::_("behavior.modal");
        JHtml::_('behavior.framework', true);
        JHtml::_('behavior.calendar');
        JHtml::script("jquery.noconflict.js",'components/com_jobsfactory/js/jquery/');

        parent::display($template);
    }

}