<?php
/*------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class jobsViewResume extends JobsSmartyView
{
    function display($template=null)
    {
        $model  = JModelLegacy::getInstance('Jobs','jobsModel');

        JHTML::_("behavior.modal");
        JHtml::_('behavior.framework', true);
        JTheFactoryHelper::tableIncludePath('category');
        JHtml::_('behavior.calendar');

        JHTML::script("jquery.noconflict.js",'components/com_jobsfactory/js/jquery/');

        $items = $model->loadItems();

        $jobs_list=array();

        for ($key=0; $key < count($items); $key++)
        {
            $job = JTable::getInstance('Jobs','Table');
            $job->bind($items[$key]);
            $job->setCacheObject($items[$key]);
            $jobs_list[] = $job;
        }

        $filter_order_Dir   = $model->getState( 'filters.filter_order_Dir');
        $filter_order       = $model->getState( 'filters.filter_order');
        $filter_archive     = $model->getState('filters.filter_archive');
        $filter_myjobs      = $model->getState('filters.filter_myjobs');
        $pagination         = $model->get('pagination');
        $filters            = $model->getFilters();

        $this->assign("job_rows", $jobs_list);
        $this->assign("filter_order_Dir", $filter_order_Dir);
        $this->assign("filter_order", $filter_order);
        $this->assign("filter_archive", $filter_archive);
        $this->assign("filter_myjobs", $filter_myjobs);
        $this->assign("pagination", $pagination);

        $this->assign('inputsHiddenFilters', JHTML::_('listJobs.inputsHiddenFilters', $filters ));
        $this->assign('htmlLabelFilters', JHTML::_('listJobs.htmlLabelFilters', $filters, false ));

        parent::display($template);
    }

}