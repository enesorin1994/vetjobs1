<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');


jimport( 'joomla.application.component.view');

class jobsViewMaps extends JobsSmartyView
{
	function display($tmpl=null){
		
        JHtml::_('behavior.framework', true);

        $cfg = JTheFactoryHelper::getConfig();

        if ($cfg->google_key!="") {
		    $jdoc = JFactory::getDocument();
            $js_declaration = " jQuery(document).ready( function(){load_gmaps(); } ); ";

            //$js_declaration = " window.addEvent('domready', function(){load_gmaps(); } ); ";
		    $jdoc->addScriptDeclaration( $js_declaration );
        }

		$this->page_title = JText::_("COM_JOBS_LIST_JOBS");

        parent::display($tmpl);

	}

}
