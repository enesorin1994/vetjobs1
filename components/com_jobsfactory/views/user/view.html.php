<?php 
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class jobsViewUser extends JobsSmartyView
{
    protected $page_title;
    protected $custom_fields;
    protected $lists;


    function display($tpl = null)
	{
        // Get the view data.
        $document	= JFactory::getDocument();
        JHtml::_('bootstrap.tooltip');

		switch($tpl){
			case "t_myuserdetails.tpl":
                JHtml::_('behavior.calendar');
                JHtml::_('dropdown.init');

                $document->addScript(JUri::root().'components/com_jobsfactory/js/job_edit.js',true);
                $document->addScript(JUri::root().'components/com_jobsfactory/js/date.js',true);
                $document->addScript(JUri::root().'components/com_jobsfactory/js/jobs.js',true);
                JHtml::_('behavior.framework',true);
                $document->addScript(JUri::root().'components/com_jobsfactory/js/Stickman.MultiUpload.js',true);
                break;
            case "t_myusereducation.tpl":
            case "t_myuserexperience.tpl":
            case "elements/resumedetail/t_editeducation.tpl":
                JTheFactoryHelper::tableIncludePath('category');
                $document->addScript(JUri::root().'components/com_jobsfactory/js/jobs.js',true);
                $document->addScript(JUri::root().'components/com_jobsfactory/js/job_edit.js',true);
                $document->addScript(JUri::root().'components/com_jobsfactory/js/date.js',true);
                break;
			case "t_search_users.tpl":
                JHtml::_("behavior.calendar");
                JHtml::_('behavior.framework', true);
                JHtml::script("joomla.javascript.js","includes/js/");
                break;
            case "t_mycompanydetails.tpl":
                JHtml::_('behavior.formvalidation');
                JHtml::_('behavior.framework', true);
                $document->addScript(JUri::root().'components/com_jobsfactory/js/job_edit.js',true);
                $document->addScript(JUri::root().'components/com_jobsfactory/js/Stickman.MultiUpload.js',true);
                break;
		}

        $document->addScriptDeclaration(' var root = "'.JUri::root().'";');

        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile();
        if ($userprofile->id) {
            $document->addScriptDeclaration(' var resumeid = "'.$userprofile->id.'";');
        }

        parent::display($tpl);
	}
	
}
