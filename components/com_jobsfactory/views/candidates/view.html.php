<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class jobsViewCandidates extends JobsSmartyView
{

	function display($template=null)
    {
        require_once(JPATH_COMPONENT_SITE . DS . "helpers" . DS . "user.php");
        $cfg = JTheFactoryHelper::getConfig();
        $model = JModelLegacy::getInstance('Candidates', 'jobsModel');

        JHtml::_("behavior.modal");
        $doc = JFactory::getDocument();
        $doc->addScript(JUri::root() . 'components/' . APP_EXTENSION . '/js/jobs.js');

        JTheFactoryHelper::tableIncludePath('category');

		$items = $model->loadItems();
        $profile = JobsHelperTools::getUserProfileObject();
        $tableKey = $profile->getIntegrationKey();

        $candidates_list=array();

	    for ($key=0; $key < count($items); $key++)
        {
            $profile->getUserProfile($items[$key]->candidateId);
            //$tableName = $profile->getIntegrationTable();

            if ($profile->profile_mode == 'cb') {

                $cbname         = $profile->getFilterField('name');
                $cbsurname      = $profile->getFilterField('surname');
                $cbusername     = $profile->getFilterField('username');
                $cbgender       = $profile->getFilterField('gender');
                $cbcity         = $profile->getFilterField('city');
                $cbtotal_experience = $profile->getFilterField('total_experience');

                $profile->fullname = $profile->$cbsurname. ' '. $profile->$cbname;

                if ($profile->fullname == ' ') {
                    $profile->fullname =  $profile->username;
                }
                $profile->function = JobsHelperUser::getLatestPosition($profile->id);
                $profile->age      = JobsHelperUser::getUserAge($profile->id);

                if (isset($profile->$cbgender)) {
                    $profile->gendername = ($profile->$cbgender == 1) ? JText::_('COM_JOBS_CANDIDATE_MALE') : JText::_('COM_JOBS_CANDIDATE_FEMALE');
                } else {
                    $profile->gendername = '';
                }
                if (isset($profile->$cbtotal_experience)) {
                    $profile->levelname  = JobsHelperUser::getResumeExperience($profile->$cbtotal_experience);
                } else {
                    $profile->levelname  = '';
                }

                if (isset($profile->$cbcity)) {
                    $profile->city = $profile->$cbcity;
                } else {
                    $profile->city = '';
                }

                $last_applied = JobsHelperTools::getCandidateLastApplied($profile->$tableKey);
                $profile->last_applied = (isset($last_applied)) ? JHtml::date($last_applied, $cfg->date_format, false) : JText::_('COM_JOBS_NEVER');

                $actionID = $items[$key]->id;
                $profile->action = JHtml::_('candidateactions.selectlist', "action$actionID", 'style="float: left; clear: left !important;"', $items[$key]->action);
                $profile->userlink = $profile->getProfileLink($profile->$tableKey);

                $candidates_list[]= clone $profile;

            } else {
                $candidate = JTable::getInstance('candidates','Table');
                $candidate->$tableKey = $items[$key]->candidateId;
                $candidate->bind($items[$key]);

                $candidate->fullname        = $candidate->getName();
                $candidate->function        = JobsHelperUser::getLatestPosition($candidate->$tableKey);
                $candidate->age             = JobsHelperUser::getUserAge($candidate->$tableKey);
                $candidate->gendername      = ($items[$key]->gender == 1) ? JText::_('COM_JOBS_CANDIDATE_MALE') : JText::_('COM_JOBS_CANDIDATE_FEMALE');
                $candidate->levelname       = JobsHelperUser::getResumeExperience($items[$key]->total_experience);
                $candidate->city            = $profile->city;
                $candidate->desired_salary  = $profile->desired_salary;

                $last_applied = JobsHelperTools::getCandidateLastApplied($candidate->$tableKey);
                $candidate->last_applied = JHtml::date($last_applied, $cfg->date_format, false);

                $actionID = $items[$key]->id;
                $candidate->action = JHtml::_('candidateactions.selectlist', "action$actionID", 'style="float: left; clear: left !important;"', $items[$key]->action);
                $candidate->userlink = JobsHelperRoute::getUserdetailsRoute($candidate->$tableKey);

                $candidate->setCacheObject($items[$key]);
                $candidates_list[]=$candidate;
           }
		}

        $filter_order_Dir = $model->getState( 'filters.filter_order_Dir');
        $filter_order = $model->getState( 'filters.filter_order');
        $filter_candidates = $model->getState('filters.filter_candidates');
		$pagination = $model->get('pagination');
        $filters = $model->getFilters();

		$this->candidates_rows = $candidates_list;
		$this->filter_order_Dir = $filter_order_Dir;
		$this->filter_order = $filter_order;
        $this->filter_candidates = $filter_candidates;
		$this->pagination = $pagination;

        $this->inputsHiddenFilters = JHTML::_('listJobs.inputsHiddenFilters', $filters );
        $this->htmlLabelFilters = JHTML::_('listJobs.htmlLabelFilters', $filters, false );

        $task = JFactory::getApplication()->input->getCmd('task');

		parent::display($template);
	}
}
