<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

//jimport( 'joomla.application.component.view');

class jobsViewJob extends JobsSmartyView
{
    function display($tmpl=null)
    {
        JHtml::_('behavior.calendar');
        JHtml::_('bootstrap.tooltip');

        $document	= JFactory::getDocument();
        $cfg=JTheFactoryHelper::getConfig();
        $document->addScript(JUri::root().'components/com_jobsfactory/js/Stickman.MultiUpload.js',true);
        $document->addScript(JUri::root().'components/com_jobsfactory/js/job_edit.js',true);
        $document->addScript(JUri::root().'components/com_jobsfactory/js/date.js',true);
        $document->addScript(JUri::root().'components/com_jobsfactory/js/jobs.js',true);
        // Get stylesheet for active template
        $document->addStyleSheet(JURI::root().'components/'.APP_EXTENSION.'/templates/'.$cfg->theme.'/'.APP_PREFIX.'_template.css');
        JHtml::_('behavior.framework');

        parent::display($tmpl);
    }

}

