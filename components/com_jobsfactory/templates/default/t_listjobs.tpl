{set_css}
{include file='js/t_javascript_language.tpl'}
{include file='js/t_javascript_countdown.tpl'}
{import_js_file url="jobs.js"}

<h2>
{"COM_JOBS_JOBS_LIST"|translate}
	<a href="index.php?option=com_jobsfactory&task={$task}&format=feed&limitstart=" target="_blank">
		<img src="{$IMAGE_ROOT}f_rss.jpg" width="14" border="0" alt="RSS" />
	</a>
</h2>

{literal}
<script type="text/javascript">
    jQuery(document).ready( function ($) {
        $(document).on("click", "#job_tabmenu li a", function(event){
           location.href = this.rel;
        });
    });
</script>
{/literal}


<div style="text-align:right;">
<ul class="nav nav-tabs" id="job_tabmenu">
    <li class="{if ($filter_archive=='active' || $filter_archive=='')}active{else}inactive{/if}">
        <a data-toggle="tab" href="#tab1" rel ="{$links.filter_link_active}">
            {"COM_JOBS_VIEW_ACTIVE"|translate}</a>
    </li>
    <li class="{if ($filter_archive=='archive')}active{else}inactive{/if}">
       	<a data-toggle="tab" href="#tab2" rel="{$links.filter_link_archive}">
       	    {"COM_JOBS_VIEW_ARCHIVED"|translate}</a>
    </li>
</ul>
</div>


<form action="{$ROOT_HOST}" method="get" name="jobsForm" id="jobsForm">
<input type="hidden" name="option" value="{$option}" />
<input type="hidden" name="task" value="{$task}" />
<input type="hidden" name="Itemid" value="{$Itemid}" />

{* Include filter selectboxes *}
{include file='elements/t_header_filter.tpl'}

<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="job_list_container">
<tr style="height: 30px;">
	<th style="width: auto; text-align: left;">
        {include file='elements/sort_field.tpl' label="COM_JOBS_TITLE"|translate key="title"} /
        {include file='elements/sort_field.tpl' label="COM_JOBS_CATEGORY"|translate key="catname"} /
        {include file='elements/sort_field.tpl' label="COM_JOBS_USER"|translate key="username"}
    </th>
	<th style="width: 190px; text-align: left;">
        {include file='elements/sort_field.tpl' label="COM_JOBS_DETAILS_ENDING_IN"|translate key="end_date"}
    </th>
	<th style="width: 120px; text-align: right;">
    	{include file='elements/sort_field.tpl' label="COM_JOBS_NR_APPLICATIONS"|translate key="nr_applicants"}
	</th>
</tr>
    <tr>
       <td class="v_spacer_15" colspan="4">&nbsp;</td>
    </tr>

  {if $job_rows|count}
	{section name=jobsloop loop=$job_rows}
		{include file='elements/lists/t_listjobs_cell.tpl' job=`$job_rows[jobsloop]` index=`$smarty.section.jobsloop.rownum`}
	{/section}
    {else}
    <tr>
        <td class="job_dbk" valign="top" colspan="3">
            {include file='t_nojobs.tpl'}
        </td>
    </tr>
  {/if}
</table>

{include file='elements/t_listfooter.tpl'}

</form>
<div class="v_spacer_15">&nbsp;</div>
<div style="clear: both;"></div>
