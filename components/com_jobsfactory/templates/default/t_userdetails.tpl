{set_css}
{include file='js/t_javascript_language.tpl'}

<h1>{'COM_JOBS_USER_PROFILE'|translate}: {$user->name} {$user->surname}</h1>

{createtabslist name="content-pane"}
    {additemtabs id="tab1" name="content-pane" active="1" text="COM_JOBS_USER_PROFILE"|translate}
  {if $user->userid}
    {additemtabs id="tab2" name="content-pane" active="0" text="COM_JOBS_TAB_EDUCATION"|translate}
    {additemtabs id="tab3" name="content-pane" active="0" text="COM_JOBS_TAB_EXPERIENCE"|translate}
    {additemtabs id="tab4" name="content-pane" active="0" text="COM_JOBS_TAB_OTHER"|translate}
  {/if}
{endtabslist}

{startpane id="content-pane" name="content-pane" active="tab1" usecookies=0}
    {starttab name="content-pane" id="tab1" text="COM_JOBS_USER_PROFILE"|translate}

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="user_detailstable">
<tr>
    <td colspan="2" class="job_user_label">
        {positions position="header" item=$user page="user_profile"}
    </td>
</tr>
<tr>
    <td colspan="2" class="job_user_label">
        {positions position="header" item=$user page="company_profile"}
    </td>
</tr>

{if $user->picture}
  <tr>
        <td width=150 class="job_user_label">{"COM_JOBS_PICTURE"|translate}</td>
        <td class="job_dbk_c">
            <img src="{$RESUME_PICTURES}/resize_{$user->picture}" />
        </td>
  </tr>
{/if}
<tr>
    <td width=150 class="job_user_label">{"COM_JOBS_NAME"|translate}</td>
    <td><span class="job_title">{$user->name} {$user->surname}</span></td>
</tr>
<tr>
    <td width=150 class="job_user_label">{"COM_JOBS_SURNAME"|translate}</td>
    <td>{$user->surname}</td>
</tr>
<tr>
   <td width=150 class="job_user_label">{"COM_JOBS_AGE"|translate}</td>
   <td>{$user->age}</td>
</tr>
<tr>
   <td width=150 class="job_user_label">{"COM_JOBS_GENDER"|translate}</td>
   <td>{if $user->gender == 1}{"COM_JOBS_CANDIDATE_MALE"|translate}{else}{"COM_JOBS_CANDIDATE_FEMALE"|translate}{/if}</td>
</tr>
<tr>
   <td width=150 class="job_user_label">{"COM_JOBS_MARITAL_STATUS"|translate}</td>
   <td>{$user->marital_status}</td>
</tr>
{if $cfg->show_contact == "1"}
    <tr>
          <td width=150 class="job_user_label">{"COM_JOBS_ADDRESS"|translate}</td>
          <td>{$user->address}</td>
      </tr>
      <tr>
          <td width=150 class="job_user_label">{"COM_JOBS_CITY"|translate}</td>
          <td>{$user->city}</td>
      </tr>
      <tr>
          <td width=150 class="job_user_label">{"COM_JOBS_COUNTRY"|translate}</td>
          <td>{$user->country}</td>
      </tr>
      <tr>
          <td width=150 class="job_user_label">{"COM_JOBS_PHONE"|translate}</td>
          <td>{$user->phone}</td>
      </tr>
      <tr>
         <td width=150 class="job_user_label">{"COM_JOBS_EMAIL"|translate}</td>
         <td>{$user->email}</td>
      </tr>
      {if $cfg->allow_messenger && $cfg->show_contact}
          <tr>
              <td width=150 class="job_user_label">{"COM_JOBS_YM"|translate}</td>
              <td>{$user->YM}</td>
          </tr>
          <tr>
              <td width=150 class="job_user_label">{"COM_JOBS_HOTMAIL"|translate}</td>
              <td>{$user->Hotmail}</td>
          </tr>
          <tr>
              <td width=150 class="job_user_label">{"COM_JOBS_SKYPE"|translate}</td>
              <td>{$user->Skype}</td>
          </tr>
          <tr>
              <td width=150 class="job_user_label">{"COM_JOBS_LINKEDIN"|translate}</td>
              <td><a href="{$user->linkedIN}">{$user->linkedIN}</a></td>
          </tr>
          <tr>
              <td width=150 class="job_user_label">{"COM_JOBS_FACEBOOK"|translate}</td>
              <td><a href="{$user->facebook}" target="_blank">{$user->facebook}</a></td>
          </tr>
      {/if}
      {if $cfg->allowpaypal && $cfg->show_contact}
          <tr>
              <td width=150 class="job_user_label">{"COM_JOBS_PAYPAL_EMAIL"|translate}</td>
              <td>{$user->paypalemail}</td>
          </tr>
      {/if}

    {if $user->file_name}
        <td width=150 class="job_user_label">{"COM_JOBS_ATTACHMENT"|translate}</td>
        <td>
            <strong><a title="userAttachment" href="{$userTable->get('links.download_file')}" target="_blank" name="show_att" id="show_att" >
                    <img src="{$IMAGE_ROOT}cv.png" border="0" title="{"COM_JOBS_PDF_RESUME"|translate}" />{$user->file_name}</a>
           </strong>
        </td>
    {/if}
{/if}
    <tr>
        <td colspan="2" class="job_user_label">
            {positions position="bottom" item=$user page="user_profile"}
        </td>
    </tr>
    <tr>
        <td colspan="2" class="job_user_label">
            {positions position="bottom" item=$user page="company_profile"}
        </td>
    </tr>
  {* @since 1.5.0 *}
  <tr><td height="10px" colspan="2">&nbsp;</td></tr>
  <tr>
  	<td colspan="2">
      <table width="100%">
        	  <tr>
            	<th class="list_ratings_header title_grey left"><span class="pull-left">{"COM_JOBS_APPLIED_FOR_JOB"|translate}</span></th>
            	<th class="list_ratings_header title_grey"><span class="pull-right">{"COM_JOBS_APPLIED_ON"|translate}</span></th>
        	  </tr>
        	 {foreach from=$lists.applications item=item}
        	 	 <tr class="form_bg{cycle values='0,1'}">
        	 		<td width="*%">
        	 			<a href='{$links->getJobDetailRoute($item->job_id)}'>{$item->title}</a>
        	 		</td>
        	 		<td width="*%" class="right">
        	 			{printdate date=$item->modified use_hour=0}
        	 		</td>
        	 	</tr>
        	 	<tr class="form_bg{cycle values='0,1'}">
        	 		<td colspan="3" >
        	 		      <div class="msg_text">{$item->message}</div>
        	 		</td>
        		 </tr>
            {/foreach}
		</table>
        	  </td>
		  </tr>
	</table>

{endtab}
<!-- NEW TAB EDUCATION -->

{starttab name="content-pane" id="tab2" text="COM_JOBS_TAB_EDUCATION"|translate}
{if $lists.countedu > 0 && ($task=='UserProfile')}
  {foreach from=$array_education item=user_education}
    <div id="row_edit_education{$user_education->id}" >
      <table class="no_border user_resume table-striped" width="100%" cellpadding="0" cellspacing="0" style="border:solid 1px #DDDDDD; border-top:none; " >
        <tr>
            <td class="job_dbk" align="right" width="150">
                <label class="job_lables">{"COM_JOBS_STUDY_LEVEL"|translate}</label>
            </td>
            <td class="job_dt job_values">
                {$user_education->levelname}
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right" width="150">
                <label class="job_lables">{"COM_JOBS_EDU_INSTITUTION"|translate}</label>
            </td>
            <td class="job_dt job_values">
                {$user_education->edu_institution}
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right" width="150">
                <label class="job_lables">{"COM_JOBS_COUNTRY"|translate}</label>
            </td>
            <td class="job_dt job_values">
                {$user_education->country}
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right" width="150">
                <label class="job_lables">{"COM_JOBS_CITY"|translate}</label>
            </td>
            <td class="job_dt job_values">
                {$user_education->city}
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right" width="150">
                <label class="job_lables">{"COM_JOBS_START_DATE"|translate}</label>
            </td>
            <td class="job_dt job_values">
                {if $task=='UserProfile'}
                    {printdate date=$user_education->start_date}
                {/if}
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right" width="150">
                <label class="job_lables">{"COM_JOBS_END_DATE"|translate}</label>
            </td>
            <td class="job_dt job_values">
                {if $task=='UserProfile'}
                    {printdate date=$user_education->end_date}
                {/if}
            </td>
        </tr>
        <tr><td colspan="2" class="job_dbk_c form_bg0">&nbsp;</td></tr>
      </table>
    </div>

    {/foreach}
{/if}
{endtab}
<!-- NEW TAB EXPERIENCE -->
{starttab name="content-pane" id="tab3" text="COM_JOBS_TAB_EXPERIENCE"|translate}
  {if $lists.countexp > 0 && $task=='UserProfile'}

      <div class="info_user">
         <span>{"COM_JOBS_TOTAL_EXPERIENCE"|translate} : {$lists.total_experience}</span><br />
          <span>{"COM_JOBS_SALARY"|translate}: {$user->desired_salary} {$lists.default_currency}</span>
      </div>
    <div style="clear: both;"></div>

    {foreach from=$array_experience item=user_experience}
        <div id="row_edit_experience{$user_experience->id}" >
        <table class="no_border user_resume table-striped" width="100%" cellpadding="0" cellspacing="0" >
            <tr>
               <td class="job_dbk" align="right" width="150">
                   <label class="job_lables">{"COM_JOBS_COMPANY"|translate}</label>
               </td>
               <td class="job_dt job_values">
                   {$user_experience->company}
               </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right" width="150">
                    <label class="job_lables">{"COM_JOBS_POSITION"|translate}</label>
                </td>
                <td class="job_dt job_values">
                    {$user_experience->position}
                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right" width="150">
                    <label class="job_lables">{"COM_JOBS_EXP_RESPONSIBILITIES"|translate}</label>
                </td>
                <td class="job_dt job_values">
                    {$user_experience->exp_description}
                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right" width="150">
                    <label class="job_lables">{"COM_JOBS_DOMAIN"|translate}</label>
                </td>
                <td class="job_dt job_values">
                    {assign var=domain value=$user_experience->domain}
                    {$user_experienceTable->getCatname($domain)}
                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right" width="150">
                    <label class="job_lables">{"COM_JOBS_START_DATE"|translate}</label>
                </td>
                <td class="job_dt job_values">
                    {if $task=='UserProfile'}
                        {printdate date=$user_experience->start_date}
                    {/if}
                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right" width="150">
                    <label class="job_lables">{"COM_JOBS_END_DATE"|translate}</label>
                </td>
                <td class="job_dt job_values">
                    {if $task=='UserProfile'}
                        {printdate date=$user_experience->end_date}
                    {/if}
                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right" width="150">
                    <label class="job_lables">{"COM_JOBS_CITY"|translate}</label>
                </td>
                <td class="job_dt job_values">
                    {$user_experience->city}
                </td>
            </tr>
            <tr><td colspan="2" class="job_dbk_c form_bg0">&nbsp;</td></tr>
            </table>
          </div>
    {/foreach}

{/if}
{endtab}

<!-- NEW TAB OTHER -->
{* education Tab *}
{starttab name="content-pane" id="tab4" text="COM_JOBS_TAB_OTHER"|translate}

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
           <td class="job_dbk" align="right">
               <label class="job_lables">{"COM_JOBS_USER_ACHIEVEMENTS"|translate}</label>
           </td>
           <td class="job_dbk_c">{$user->user_achievements}</td>
        </tr>
        <tr>
           <td class="job_dbk" align="right" width="150">
               <label class="job_lables">{"COM_JOBS_USER_GOALS"|translate}</label>
           </td>
           <td class="job_dbk">{$user->user_goals}</td>
       </tr>
       <tr>
           <td class="job_dbk" align="right">
               <label class="job_lables">{"COM_JOBS_USER_HOBBIES"|translate}</label>
           </td>
           <td class="job_dbk_c">{$user->user_hobbies}</td>
       </tr>

    </table>
{endtab}

{endpane name="content-pane" id="content-pane"}
