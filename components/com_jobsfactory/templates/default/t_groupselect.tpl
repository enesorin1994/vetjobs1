{include file='js/t_javascript_language.tpl'}
{set_css}
<h1>{'COM_JOBS_SELECT_PROFILE'|translate}&nbsp;{$user->name}</h1>

<div class="jobs_userprofile">
<form action="index.php?option=com_jobsfactory&controller=user&task=setusergroup" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" >
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
    <input type="hidden" name="id" value="{$userid}" />
	<input type="hidden" name="task" value="setUserGroup" />
	<input type="hidden" name="controller" value="user" />

    <div class="detail_job_info">
        <span style="float:left;">{"COM_JOBS__SELECT_USER_GROUP"|translate}</span>
        <br />
        <div class="v_spacer_15">&nbsp;</div>
        <div style="float:left;">{$radioUserGroup}</div>
    </div>

    <br />
    <div class="v_spacer_15">&nbsp;</div>
    <div style="clear: both;"></div>

    <div>
        <input name="save" value="{'COM_JOBS_SAVE'|translate}" class="btn btn-primary" type="submit" />
        <a href="{$lists.cancel_form}" class="back_button_cancel"><input name="cancel" value="{jtext text='COM_JOBS_CANCEL'}" class="btn btn-primary" type="button" /></a>
    </div>
</form>
</div>

<div style="clear: both;"></div>