{literal}
<script type="text/javascript">
    jQuery(document).ready( function ($) {
        $(document).on("click", "#job_tabmenu li a", function(event){
           location.href = this.rel;
        });
    });
</script>
{/literal}

<!-- Begin Content -->
<div>
    <ul class="nav nav-tabs" id="job_tabmenu">
        <li class="{if ('show_search' == $task)}active{else}inactive{/if}">
            <a data-toggle="tab" href="#tab1" rel="{$links->getSearchRoute()}" >
        {"COM_JOBS_SEARCH_JOBS"|translate}</a>
        </li>
        {if ($cfg->google_key!="") }
        <li class="{if ('googlemaps' == $task)}active{else}inactive{/if}">
            <a data-toggle="tab" href="#tab2" rel = "{$links->getSearchonMapRoute()}">
        {"COM_JOBS_RADIUS_SEARCH"|translate}</a>
        </li>
        {/if}
        <li class="{if ('searchcompanies' == $task)}active{else}inactive{/if}">
           <a data-toggle="tab" href="#tab3" rel = "{$links->getSearchonCompaniesRoute()}">
        {"COM_JOBS_SEARCH_COMPANIES"|translate}</a>
        </li>
        <li class="{if ('searchusers' == $task)}active{else}inactive{/if}">
            <a data-toggle="tab" href="#tab4" rel = "{$links->getSearchonUsersRoute()}">
        {"COM_JOBS_SEARCH_CANDIDATES"|translate}</a>
        </li>
    </ul>
</div>
<div id="config-document" class="tab-content">
    <div id="tab1" class="tab-pane {if ('show_search' == $task)}active{else}inactive{/if}">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    {if ($cfg->google_key!="") }
    <div id="tab2" class="tab-pane {if ('googlemaps' == $task)}active{else}inactive{/if}">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    {/if}
    <div id="tab3" class="tab-pane {if ('searchcompanies' == $task)}active{else}inactive{/if}">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    <div id="tab4" class="tab-pane {if ('searchusers' == $task)}active{else}inactive{/if}">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
</div>

<!-- End Content -->
<div style="clear: both;"></div>
