{include file='js/t_javascript_language.tpl'}
{set_css}
<h2><strong>{$page_title}</strong></h2>
<h3>{if $sfilters->keyword != ''} {"COM_JOBS_KEYWORD"|translate}: {$sfilters->keyword}{/if}</h3>
<form action="{$ROOT_HOST}/index.php" method="get" name="jobsForm">
<input type="hidden" name="option" value="{$option}">
<input type="hidden" name="task" value="{$task}">
<input type="hidden" name="controller" value="user">
<input type="hidden" name="Itemid" value="{$Itemid}">
{"COM_JOBS_TOTAL_FOUND"|translate}: {$users|@count} ({"COM_JOBS_PAGES"|translate}: {$pagination->pagesTotal})
{foreach from=$users item=user}
<div class="job_user">
	<div class="user_head">
		<a href="{$user->link}">
			{$user->candidateName} {$user->candidateSurname}
		</a>
        {positions position="header" item=$user page="user_profile"}
	</div>
	<div class="user_info">
		<table width="100%" class="job_search_sub">
			<tr>

				<td>
					{if $cfg->show_contact}
                        <div>
                        <label class="job_lables">{"COM_JOBS_CITY"|translate}</label>: <span>{$user->city|default:"--"}</span>
                        </div><br />
						{if $user->country_name}
							<div>
                                <label class="job_lables">{"COM_JOBS_COUNTRY"|translate}</label>: <span>{$user->country_name}</span>
                            </div><br />
						{/if}
						{if $cfg->allow_messenger== "1"}
                            <div class="pull=left">
						        <label class="job_lables">{"COM_JOBS_YM"|translate}</label>: <span>{$user->YM|default:"--"}</span>
                            </div><br />
					        <div>
                    <label class="job_lables">{"COM_JOBS_HOTMAIL"|translate}</label>: <span>{$user->Hotmail|default:"--"}</span>
                            </div><br />
                            <div>
							<label class="job_lables">{"COM_JOBS_SKYPE"|translate}</label>: <span>{$user->Skype|default:"--"}</span>
                            </div><br />
                            <div>
                            <label class="job_lables">{"COM_JOBS_LINKEDIN"|translate}</label>: <span>{$user->linkedIN|default:"--"}</span>
                            </div><br />
                            <div>
                    <label class="job_lables">{"COM_JOBS_FACEBOOK"|translate}</label>: <span>{$user->facebook|default:"--"}</span>
                            </div>
                            <br />
						{/if}
					{/if}
                    {positions position="details" item=$user page="user_profile"}
				</td>
                <td>
                    {if $cfg->google_key!="" && ( ($user->googleMaps_x!="" && $user->googleMaps_y!="") )}
                        <a class="modal" {literal} rel="{handler: 'iframe', size: { {/literal} x:{$cfg->googlemap_gx+25|default:'260'}, y:{$cfg->googlemap_gy+20|default:'100'}} {literal} } " {/literal} href="{$ROOT_HOST}index.php?option=com_jobsfactory&controller=maps&tmpl=component&task=googlemap&x={$user->googleMaps_x}&y={$user->googleMaps_y}"></a>
                    {/if}
                    {positions position="googlemaps" item=$user page="user_profile"}
                </td>
			</tr>
            <tr>
                <td>
                    <label class="job_lables">{"COM_JOBS_COMPANY_IDENTIFICATION_NO"|translate}</label>: <span>{$user->useruniqueid|default:"--"}</span>
                </td>
            </tr>
		</table>
	</div>
</div>
{/foreach}
{include file='elements/t_listfooter.tpl'}
</form>
<div style="clear: both;"></div>