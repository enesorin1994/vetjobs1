{set_css}
{include file='js/t_javascript_language.tpl'}

<h1>{'COM_JOBS_COMPANY_PROFILE'|translate}: {$user->name}</h1>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="user_detailstable">
<tr>
    <td colspan="2">
        {positions position="header" item=$user page="company_profile"}
    </td>
</tr>
{if $cfg->enable_images}
    <tr>
        <td width=150 class="job_user_label">{"COM_JOBS_LOGO"|translate}</td>
        <td>
            {if $user->picture}
                <img src="{$COMPANY_PICTURES}/resize_{$user->picture}" />
            {else}
                <img src="{$IMAGE_ROOT}/user/logo.png" />
            {/if}
        </td>
    </tr>
{/if}
<tr>
    <td width=150 class="job_user_label">{"COM_JOBS_NAME"|translate}</td>
    <td><span class="job_title">{$user->name}</span></td>
</tr>
<tr>
    <td width=150 class="job_user_label">{"COM_JOBS_COMPANY_WEBSITE"|translate}</td>
    <td><a href="{$user->website}">{$user->website}</a></td>
</tr>
<tr>
    <td class="job_user_label">{"COM_JOBS_COMPANY_SHORT_DESCRIPTION"|translate}</td>
    <td>{$user->shortdescription}</td>
</tr>
<tr>
    <td class="job_user_label">{"COM_JOBS_ACTIVITY_DOMAIN"|translate}</td>
    <td>{$activityDomains}</td>
</tr>

{if $cfg->show_contact == "1"}
      <tr>
          <td width=150 class="job_user_label">{"COM_JOBS_ADDRESS"|translate}</td>
          <td>{$user->address}</td>
      </tr>
      <tr>
          <td width=150 class="job_user_label">{"COM_JOBS_CITY"|translate}</td>
          <td>{$user->city}</td>
      </tr>
      <tr>
          <td width=150 class="job_user_label">{"COM_JOBS_COUNTRY"|translate}</td>
          <td>{$user->country}</td>
      </tr>
      <tr>
          <td class="job_user_label">{"COM_JOBS_CONTACT_USERNAME"|translate}</td>
          <td>{$user->contactname}</td>
      </tr>
      <tr>
          <td class="job_user_label">{"COM_JOBS_POSITION"|translate}</td>
          <td>{$user->contactposition}</td>
      </tr>
      <tr>
          <td class="job_user_label">{"COM_JOBS_CONTACT_PHONE"|translate}</td>
          <td>{$user->contactphone}</td>
      </tr>
      <tr>
          <td class="job_user_label">{"COM_JOBS_CONTACT_EMAIL"|translate}</td>
          <td>{$user->contactemail}</td>
      </tr>
      <tr>
        <td width=150 class="job_user_label">{"COM_JOBS_FACEBOOK"|translate}</td>
        <td><a href="{$user->facebook}" target="_blank">{$user->facebook}</a></td>
      </tr>
      <tr>
        <td width=150 class="job_user_label">{"COM_JOBS_TWITTER"|translate}</td>
        <td><a href="{$user->twitter}" target="_blank">{$user->twitter}</a></td>
      </tr>

      {if $cfg->allowpaypal && $cfg->show_contact}
          <tr>
              <td width=150 class="job_user_label">{"COM_JOBS_PAYPAL_EMAIL"|translate}</td>
              <td>{$user->paypalemail}</td>
          </tr>
      {/if}
{/if}
 <tr>
   <td width=150 class="job_user_label">{"COM_JOBS_ABOUT_US"|translate}</td>
   <td>{$user->about_us}</td>
 </tr>

 {if $cfg->jobs_allow_rating}
    <tr>
        <td width=150 class="job_user_label">{"COM_JOBS_RATING"|translate}</td>
        <td>
        {if $user->users_count >= 5}
            <i class="ads-icon-star"></i>{$user->user_rating} <span class="muted">({$user->users_count})</span>
        {else}
            <span class="muted small">{jtext text="COM_JOBS_NOT_ENOUGH_VOTES_RECEIVED"}</span>
        {/if}
        </td>
    </tr>
 {/if}
    <tr>
        <td colspan="2">
            {positions position="bottom" item=$user page="company_profile"}
        </td>
    </tr>
  {* @since 1.5.0 *}

  <tr><td height="10px" colspan="2">&nbsp;</td></tr>

{if $lists.jobslist|@count > 0}
  <tr><td colspan="2"><strong>{"COM_JOBS_LATEST__JOB_OPENINGS"|translate}</strong></td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr>
  	<td colspan="2">
      <table class="table-striped">
        	  <tr>
            	<th class="list_ratings_header">{"COM_JOBS_TITLE"|translate}</th>
            	<th class="list_ratings_header">{"COM_JOBS_START_DATE"|translate}</th>
            	<th class="list_ratings_header">{"COM_JOBS_END_DATE"|translate}</th>
                <th class="list_ratings_header">{"COM_JOBS_NR_CANDIDATES"|translate}</th>
        	  </tr>

        	 {foreach from=$lists.jobslist item=item}
                 <tr>
        	 		<td width="35%" >
                         <a href='{$links->getJobDetailRoute($item->id)}'>{$item->title}</a>
        	 		</td>
        	 		<td width="*%">{$item->start_date}</td>
                    <td width="*%">{$item->end_date}</td>
        	 		<td width="*%" class="right">{$item->nr_applicants}</td>
        	 	</tr>
            {/foreach}
		</table>
        	  </td>
		  </tr>
{/if}
	</table>
{if $lists.jobslist|@count > 0}
    <div class="user_details_header">
      <a href="{$ROOT_HOST}index.php?option=com_jobsfactory&task=listjobs&reset=all&userid={$user->userid}&Itemid={$Itemid}">{"COM_JOBS_MORE_JOBS"|translate}</a>
    </div>
{/if}
