{* Include Validation script *}
{include file='js/t_javascript_language.tpl'}
{import_js_file url="job_edit.js"}
{set_css}

<h1>{"COM_JOBS_SEARCH_JOBS"|translate}</h1>

{include file='t_search_tabs.tpl'}

{*{startpane name="job_tabmenu" active="tab1"}*}
{starttab name="job_tabmenu" id="tab1"}

<form action="{$ROOT_HOST}index.php" method="get" name="rJobForm" id="rJobForm" class="form-horizontal">
<input type="hidden" name="task" value="showSearchResults"/>
<input type="hidden" name="option" value="{$option}"/>
<input type="hidden" name="Itemid" value="{$Itemid}"/>

<div class="jobs_search" style="max-width: 700px;">

    <div class = "row-fluid">
        <div class = "span9">
            <input type="text" name="keyword" class="inputbox input-block-level " size="50" value="{$lists.keyword}" placeholder = "{"COM_JOBS_TITLE"|translate}" style="margin-bottom: 2px !important;" />
        </div>
        <div class="span3">
            <input type="submit" name="search" value="{'COM_JOBS_SEARCH'|translate}" class="btn btn-primary btn-block btn-medium"/>
        </div>
    </div>
    <div><span>&nbsp;</span></div>
    <div class = "jobs_search_inner">
        <div class = "row-fluid">

            <div class = "span5 offset2">
                <div class="job_search_sub">
                    <input type="checkbox" class="inputbox pull-left" name="in_description" value="1" {if $lists.in_description}checked=checked{/if} /> &nbsp;<label
                        class="job_lables">{"COM_JOBS_SEARCH_IN_DESCRIPTION"|translate}</label>&nbsp;&nbsp;
                </div>
            </div>

            <div class = "span5">
                <div class="job_search_sub">
                    <input type="checkbox" class="inputbox pull-left" name="in_archive" value="1" {if $lists.in_archive}checked=checked{/if} /> &nbsp;<label
                        class="job_lables">{"COM_JOBS_SEARCH_IN_ARCHIVE"|translate}</label>
                </div>
            </div>

        </div>
        <div>
            <div><span>&nbsp;</span></div>
        </div>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="control-label job_lables"><label class="job_lables">{"COM_JOBS_USERNAME"|translate}</label></div>
            </div>
            <div class="span10">
                {$lists.users}
            </div>
        </div>
        {if $lists.cats}
        <div class = "row-fluid">
            <div class = "span2">
                <div class="control-label job_lables"><label class="job_lables">{"COM_JOBS_CATEGORY"|translate}</label></div>
            </div>
            <div class="span10">
                {$lists.cats}
            </div>
        </div>
        {/if}
        {if $lists.country}
        <div class = "row-fluid">
            <div class = "span2">
                <div class="control-label job_lables"><label class="job_lables">{"COM_JOBS_COUNTRY"|translate}</label></div>
            </div>
            <div class="span10">
                {$lists.country}
            </div>
        </div>
        {/if}
        {if $job_location_state}
        <div class = "row-fluid">
            <div class = "span2">
                <div class="control-label job_lables"><label class="job_lables">{"COM_JOBS_LOCATION"|translate}</label></div>
            </div>
            <div class="span10">
                {$job_location_state}
            </div>
        </div>
        {/if}
        <div class = "row-fluid">
            <div class = "span2">
                <div class="control-label job_lables"><label class="job_lables">
                    <span class="pull-left">{"COM_JOBS_CITY"|translate}</span></label>
                </div>
            </div>
            <div class="span10">
                {if $cfg->enable_location_management && !empty($lists.city)}
                    <div id="cities" name="cities">
                        {$lists.city}
                    </div>
                {else}
                    <input class="inputbox" type="text" size="30" name="city" value="{$lists.city}" alt="city">
                {/if}
            </div>
        </div>
        <div class = "row-fluid" style="margin: 2px 0 2px 0;">
            <div class = "span2">
                <div class="job_lables"><label class="job_lables">{"COM_JOBS_AFTER_DATE"|translate}</label></div>
            </div>
            <div class="span10">
                {$lists.afterd}
            </div>
        </div>
        <div class = "row-fluid" style="margin: 1px 0 2px 0;">
            <div class = "span2">
                <div class="job_lables"><label class="job_lables">{"COM_JOBS_BEFORE_DATE"|translate}</label></div>
            </div>
            <div class="span10">
                {$lists.befored}
            </div>
        </div>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="job_lables"><label class="job_lables">{"COM_JOBS_JOB_TYPE"|translate}</label></div>
            </div>
            <div class="span10">
                {$lists.job_type}
            </div>
        </div>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="job_lables"><label class="job_lables">{"COM_JOBS_TAB_EXPERIENCE"|translate}</label></div>
            </div>
            <div class="span10">
                {$lists.experience_request}
            </div>
        </div>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="job_lables"><label class="job_lables">{"COM_JOBS_STUDIES_REQUEST"|translate}</label></div>
            </div>
            <div class="span10">
               {$lists.studies_request}
            </div>
        </div>

        {if $custom_fields_html|@count > 0}
        <div class = "row-fluid">
            <fieldset class="jobs_search">
                <legend>{jtext text="COM_JOBS_CUSTOM_FIELDS"}</legend>
                <div class="span12 custom_fields_box">
                  {$custom_fields_html}
                </div>
            </fieldset>
        </div>
        {/if}

        <div class = "row-fluid">
            <div class="span3">
                <input type="submit" name="search" value="{'COM_JOBS_SEARCH'|translate}" class="btn btn-primary btn-block btn-medium"/>
            </div>
        </div>
    </div>
</div>
</form>

{endtab}