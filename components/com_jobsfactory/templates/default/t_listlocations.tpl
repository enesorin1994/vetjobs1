{* revised 1.5.4 *}
{set_css}
<h2><strong>{$page_title}</strong></h2>

{include file='elements/category/t_categories_tabs.tpl'}
{starttab name="job_tabmenu" id="tab3"}

{if $current_location}
	<div class="job_cat_breadcrumb">
    	<a href="{$links->getLocationRoute()}">{"COM_JOBS_ALL"|translate}</a>
		<strong>{$current_location->locname}</strong>
	</div>
{/if}

<table class="job_categories" cellspacing="0" cellpadding="3">
{if $countryId_arr|@count==0}
	<tr>
		<td>{"COM_JOBS_THERE_ARE_NO_SUBCATEGORIES"|translate}</td>
	</tr>
{else}
    {foreach from=$countryId_arr key="mykey" item=countryId_arr_item}

      {if is_array($countryId_arr_item) && !empty($countryId_arr_item)}

        {if $countryId_arr|@count>1}
          {if $smarty.foreach.countryId_arr_item.rownum is odd}
            <tr>
            {/if}
                <td width="20%" valign="top"><div class="form_bg0">
                        {if $mykey != 0}
                            {$countries[$mykey]->country}
                        {else}
                            {"COM_JOBS_NO_COUNTRY_ASSIGNED"|translate}
                        {/if}
                    </div>
                </td>
            {if $smarty.foreach.countryId_arr_item.rownum is not odd}
            </tr>
          {/if}
        {/if}
        {foreach from=$countryId_arr_item item=countryId_arr_item2}
            {if $smarty.foreach.countryId_arr_item2.rownum is odd}
            <tr>
            {/if}
                <td width="40%" valign="top">
                    <div class="job_maincat">
                        <a href="{$countryId_arr_item2->link}">{$countryId_arr_item2->locname}</a><span style="font-weight: normal;font-size: 12px">({$countryId_arr_item2->nr_a})</span>
                        {if $countryId_arr_item2->nr_a>0}
                        <a href="{$countryId_arr_item2->view}">
                            <img src="{$IMAGE_ROOT}document-text-image.png" border="0" alt="" /></a>
                        {/if}
                        {if $is_logged_in}
                            <a href="{$countryId_arr_item2->link_watchlist}">
                                {if $countryId_arr_item2->watchListed_flag}
                                    <img src="{$IMAGE_ROOT}f_watchlist_0.png" border="0" alt="" width="16" title="{'COM_JOBS_REMOVE_FROM_WATCHLIST_LOC'|translate}"/>
                                {else}
                                    <img src="{$IMAGE_ROOT}f_watchlist_1.png" border="0" alt="" width="16" title="{'COM_JOBS_ADD_TO_WATCHLIST_LOC'|translate}"/>
                                {/if}
                            </a>
                        {/if}
                    </div>

                    <div class="job_subcat_container">
                      {if $countryId_arr_item2->cityids|@count}
                        {section name=city loop=$countryId_arr_item2->cityid_arr}
                            {assign var=cityID value=$countryId_arr_item2->cityid_arr[city]}

                            {if $countryId_arr_item2->cityname_arr != null}
                                <div>
                                    {if $is_logged_in}
                                        <a href="{$countryId_arr_item2->link_watchlist_city[$cityID]}">
                                            {if $cities[$cityID]->watchListed_city_flag}
                                                <img src="{$IMAGE_ROOT}f_watchlist_0.png" border="0" alt=""  width="16" title="{'COM_JOBS_REMOVE_FROM_WATCHLIST_CITY'|translate}"/>
                                            {else}
                                                <img src="{$IMAGE_ROOT}f_watchlist_1.png" border="0" alt="" width="16" title="{'COM_JOBS_ADD_TO_WATCHLIST_CITY'|translate}" />
                                            {/if}
                                        </a>
                                    {/if}
                                    <a href="{$countryId_arr_item2->view_arr[$cityID]}">{$countryId_arr_item2->cityname_arr[$cityID]} ({$countryId_arr_item2->jobs_no[$cityID]})</a>
                                    {if $countryId_arr_item2->nr_subloc>0}
                                        <a href="{$countryId_arr_item2->view_arr[$cityID]}">
                                            <img src="{$IMAGE_ROOT}product.gif" border="0" alt="" />
                                        </a>
                                    {/if}
                                </div>
                            {/if}
                        {/section}
                      {/if}
                    </div>

                </td>
            {if $smarty.foreach.countryId_arr_item2.rownum is not odd}
            </tr>
            {/if}

        {/foreach}
      {else}
        {if $countryId_arr|@count>1}
            {if $smarty.foreach.countryId_arr_item.rownum is odd}
            <tr>
            {/if}
              <td width="20%" valign="top"><div class="form_bg0">
                {$countries[$mykey]->country} (0)</div>
            {if $smarty.foreach.countryId_arr_item.rownum is not odd}
            </tr>
            {/if}
        {/if}
      {/if}
    {/foreach}

{/if}
</table>
{endtab}
