{set_css}
{include file='js/t_javascript_language.tpl'}
{import_js_file url="jobs.js"}

<h2>
{"COM_JOBS_COMPANIES_LIST"|translate}
	<a href="index.php?option=com_jobsfactory&task={$task}&format=feed&limitstart=" target="_blank">
		<img src="{$IMAGE_ROOT}f_rss.jpg" width="14" border="0" alt="RSS" />
	</a>
</h2>


<form action="{$ROOT_HOST}" method="get" name="jobsForm" id="jobsForm">
<input type="hidden" name="option" value="{$option}" />
<input type="hidden" name="task" value="{$task}" />
<input type="hidden" name="Itemid" value="{$Itemid}" />

{* Include filter selectboxes *}
{include file='elements/t_header_filter.tpl'}

<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="job_list_container table-striped">
  <tr style="height: 30px;">
    <th style="width: auto; text-align: left;" class="title_grey">
        {"COM_JOBS_LOGO"|translate}
    </th>
	<th style="width: *%; text-align: left;" class="title_grey">{include file='elements/sort_field.tpl' label="Name"|translate key="name"} /
        {"COM_JOBS_ACTIVITY_DOMAIN"|translate}
	</th>
    {if $jobs_allow_rating}
        <th style="width: auto; text-align: left;">
            <span class="title_grey"> {"COM_JOBS_RATINGS"|translate}</span>
        </th>
    {/if}
	<th style="width: 120px; text-align: left;">
	    {include file='elements/sort_field.tpl' label="Location"|translate key="city"}
        <span class="title_grey"> / {"COM_JOBS_AVAILABLE_POSTS"|translate}</span>
	</th>
  </tr>
	{section name=companiesloop loop=$companies_rows}
		{include file='elements/lists/t_listcompanies_cell.tpl' company=`$companies_rows[companiesloop]` index=`$smarty.section.companiesloop.rownum`}
	{/section}
</table>
{include file='elements/t_listfooter.tpl'}

</form>
<div style="clear: both;"></div>
