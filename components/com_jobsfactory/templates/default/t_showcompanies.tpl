{include file='js/t_javascript_language.tpl'}
{set_css}
<h2><strong>{$page_title}</strong></h2>
<h3>{"COM_JOBS_KEYWORD"|translate}: {$sfilters->keyword}</h3>
<form action="{$ROOT_HOST}/index.php" method="get" name="jobsForm">
<input type="hidden" name="option" value="{$option}">
<input type="hidden" name="task" value="{$task}">
<input type="hidden" name="controller" value="user">
<input type="hidden" name="Itemid" value="{$Itemid}">
{"COM_JOBS_TOTAL_FOUND"|translate}: {$users|@count} ({"COM_JOBS_PAGES"|translate}: {$pagination->pagesTotal})
{foreach from=$users item=user}
<div class="job_user">
	<div class="user_head">
        {if $cfg->show_contact}
            <a href="{$user->link}">
                {$user->companyName}
            </a>
        {/if}
        {positions position="header" item=$user page="company_profile"}
	</div>
	<div class="user_info">
		<table width="100%" class="job_search_sub">
			<tr>
				<td class="job_dbk_c">
					{if $cfg->show_contact}
                        <div>
                            <label class="job_lables">{"COM_JOBS_CITY"|translate}</label>:<span>{$user->city|default:"--"}</span>
                        </div>
                        <br />
						{if $user->country_name}
                        <div class="pull=left">
							<span class="job_lables pull-left" style="float: left;">{"COM_JOBS_COUNTRY"|translate}</span>: <span>{$user->country_name}</span>
                        </div>
                        <br />

						{/if}
                        <div class="pull=left">
    					    <label class="job_lables pull-left">{"COM_JOBS_COMPANY_EMAIL"|translate}</label>:<span>{$user->contactemail|default:"--"}</span>
                        </div>
                        <br />
                        <div class="pull=left">
							<label class="job_lables pull-left">{"COM_JOBS_COMPANY_WEBSITE"|translate}</label>:<span>{$user->website|default:"--"}</span>
                        </div>
                        <br />
					{/if}
                    <div style="clear: both;"></div>
					{if $user->companydescription}
                    <div>
					    <label class="job_lables pull-left">{"COM_JOBS_COMPANY_DESCRIPTION"|translate}</label>:
                        <div class="job_company_description">
					        {$user->companydescription}
                        </div>
                    </div>
					{/if}
                    <div>
                        <label class="job_lables">{"COM_JOBS_CONTACT_NAME"|translate}</label>: <span>{$user->contactname|default:"--"}</span>
                    </div>
                    <br />
                    <div class="pull=left">
                        <label class="job_lables">{"COM_JOBS_CONTACT_POSITION"|translate}</label>: <span>{$user->contactposition|default:"--"}</span>
                    </div>

                    {positions position="details" item=$user page="company_profile"}
				</td>
                <td class="job_dbk_c">
                    {if $cfg->google_key!="" && ( ($user->googleMaps_x!="" && $user->googleMaps_y!="") )}
                        <a href = "#" onclick = "window.open('index.php?option=com_jobsfactory&amp;controller=maps&amp;tmpl=component&amp;task=googlemap&amp;x={$user->googleMaps_x}&amp;y={$user->googleMaps_y}','GoogleMap','width=650,height=500');return false;">{"COM_JOBS_VIEW_MAP"|translate}</a>
                    {/if}
                    {positions position="googlemaps" item=$user page="company_profile"}
                </td>
			</tr>
            <tr>
               <td>
                   <label class="job_lables">{"COM_JOBS_COMPANY_IDENTIFICATION_NO"|translate}</label>: <span>{$user->useruniqueid|default:"--"}</span>
               </td>
           </tr>
		</table>
	</div>
</div>
{/foreach}
{include file='elements/t_listfooter.tpl'}
</form>
<div style="clear: both;"></div>