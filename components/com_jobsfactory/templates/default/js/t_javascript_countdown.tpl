{if $cfg->enable_countdown}
    {import_js_file url="countdown.js"}
    {import_js_block}
        {literal}
    	window.addEvent('domready', function(){
    		new job_countdown( '.timer', language["job_days"],language["job_expired"]);
    	});
        {/literal}
    {/import_js_block}
{/if}
