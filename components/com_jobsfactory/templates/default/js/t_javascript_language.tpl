{import_js_block}
var language=Array();
language["job_err_fields_compulsory"]="{'COM_JOBS_COMPULSORY_FIELDS_UNFILLED'|translate}";
language["job_err_empty_job"]="{'COM_JOBS_PRICE_CAN_NOT_BE_EMTPY'|translate}";
language["job_err_terms"]="{'COM_JOBS_YOU_MUST_CHECK_THE_TERMS_AND_CONDITIONS'|translate}";

language["job_confirm_close_job"]="{'COM_JOBS_ARE_YOU_SURE_YOU_WANT_TO_CLOSE_THIS_JOB'|translate}";
language["job_job_price"]="{'COM_JOBS_APPLICATION_PRICE'|translate}";
language["job_required"]="{'COM_JOBS_FIELD_REQUIRED'|translate}";
language["job_err_numeric"]="{'COM_JOBS_FIELD_MUST_BE_NUMERIC'|translate}";
language["job_err_email"]="{'COM_JOBS_EMAIL_IS_NOT_VALID'|translate}";
language["job_err_startdate"]="{'COM_JOBS_JOB_START_DATE_IS_NOT_VALID'|translate}";
language["job_err_enddate"]="{'COM_JOBS_JOB_END_DATE_IS_NOT_VALID'|translate}";
language["job_err_max_valability"]="{'COM_JOBS_JOB_END_DATE_EXCEEDS_MAXIMUM_ALLOWED_LENGTH'|translate}";
language["job_notfound"]="{'COM_JOBS_NOT_FOUND'|translate}";
language["job_noresults"]="{'COM_JOBS_NO_RESULTS_FOUND'|translate}";
language["job_post_in_cat"]="{'COM_JOBS_POST_JOB_IN_THIS_CATEGORY'|translate}";
language["job_days"]="{'COM_JOBS_DAYS'|translate}";
language["job_expired"]="{'COM_JOBS_EXPIRED'|translate}";
language["job_err_birthdate"]="{'COM_JOBS_JOB_BIRTH_DATE_IS_NOT_VALID'|translate}";

{if $terms_and_conditions}
    var must_accept_term= true;
{else}
    var must_accept_term= false;
{/if}

var job_currency='{$job->currency}';

var job_max_availability={$cfg->availability|default:0};
var job_date_format='{$cfg->date_format}';
{/import_js_block}
