{* Include Validation script *}
{include file='js/t_javascript_language.tpl'}
{import_js_file url="job_edit.js"}

{assign var="fieldRequired" value=$lists.fieldRequiredlink}
{*set_css*}
<span class="v_spacer_15"></span>

{if $paymentItems}
{include file='elements/userprofile/t_profile_tabs.tpl'}
{/if}

<span class="v_spacer_15"></span>

{import_js_file url="Stickman.MultiUpload.js"}
{import_js_file url="date.js"}
{import_js_file url="user_resume.js"}
{include file='elements/userprofile/t_profile_validate.tpl'}

{createtabslist name="content-pane"}
    {additemtabs id="usertab1" name="content-pane" active="1" text="COM_JOBS_TAB_GENERAL"|translate}
  {if $user->userid}
    {additemtabs id="usertab2" name="content-pane" active="0" text="COM_JOBS_TAB_EDUCATION"|translate}
    {additemtabs id="usertab3" name="content-pane" active="0" text="COM_JOBS_TAB_EXPERIENCE"|translate}
    {additemtabs id="usertab4" name="content-pane" active="0" text="COM_JOBS_TAB_OTHER"|translate}
  {/if}
{endtabslist}

{startpane id="content-pane" name="content-pane" active="usertab1" usecookies=0}
    {starttab name="content-pane" id="usertab1" text="COM_JOBS_TAB_GENERAL"|translate}

<div class="last_dates">
    <span>{if $user->last_modified!= NULL}{"COM_JOBS_LAST_MODIFIED_ON"|translate}{$user->last_modified}{/if}</span>
    <span style="float:right;">{if $user->last_applied!= NULL}{"COM_JOBS_LAST_APPLIED_ON"|translate}{$user->last_applied}{/if}</span>
</div>

<div class="jobs_userprofile">
<form action="{$ROOT_HOST}index.php" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" class="form-validate">
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
    <input type="hidden" name="resume_id" value="{$user->id}" />

    {if $userTable->id}
        <input type="hidden" name="id" value="{$userTable->id}" />
    {/if}
    <input type="hidden" name="useruniqueid" value="{$user->useruniqueid}" />
    <input type="hidden" name="isCompany" value="0" />
	<input type="hidden" name="task" value="saveUserDetails" />
	<input type="hidden" name="controller" value="user" />

    <div class="user_edit_header">
        <table width="100%" cellpadding="0" cellspacing="0">
        	<tr >
        		<td align="left">
        			{"COM_JOBS_EDIT_USER_DETAILS"|translate} ({$user->username})
        		</td>
        		<td align="right">
                    <input name="save" value="{'COM_JOBS_SAVE'|translate}" class="btn btn-primary validate save-button" type="submit" />
					<a class="btn btn-primary hasTooltip" data-toggle="tooltip" data-placement="top" title data-original-title="{"COM_JOBS_SAVE_BEFORE_SEND"|translate}" id="emailresume" href="index.php?option=com_jobsfactory&task=emailresume&controller=user&id={$user->id}">{"COM_JOBS_EMAIL_RESUME_BUTTON"|translate}<a/>
        		</td>
        	</tr>
        </table>
    </div>

    <div class="form-horizontal user_resume" id="generalprofile" style="border:solid 1px #DDDDDD; border-top:none; ">
        <div class="control-group">
            <div class="user_edit_section right">
                {"COM_JOBS_GENERAL_PROFILE"|translate}
			</div>
		</div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_NAME"|translate}{$fieldRequired}</label>
            <div class="controls">
				<input class="inputbox required" type="text" name="name" value="{$user->name}" size="40" />
			</div>
		</div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_SURNAME"|translate}</label>
            <div class="controls">
				<input class="inputbox" type="text" name="surname" value="{$user->surname}" size="40" />
			</div>
		</div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_BIRTH_DATE"|translate}{$fieldRequired}</label>
            <div class="controls">{$lists.birth_date_html} ({"COM_JOBS_AGE"|translate}{$user->age})
                &nbsp;{infobullet text="user_birthdate_help"|translate}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_GENDER"|translate}{$fieldRequired}</label>
            <div class="controls1">{$lists.gender}<span style="color: red; float: left; width: 15px; padding-left: 3px;" title="{'COM_JOBS_FIELD_REQUIRED'|translate}" class="required_span"></span></div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_MARITAL_STATUS"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="marital_status" value="{$user->marital_status}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_PHONE"|translate}</label>
            <div class="controls">
				<input class="inputbox" type="text" name="phone" value="{$user->phone}" size="40" />
			</div>
		</div>
        <div class="control-group">
			<label class = "control-label job_lables">{"COM_JOBS_EMAIL"|translate}</label>
            <div class="controls">
				<input class="inputbox" type="text" name="email" value="{$user->email}" size="40" />
			</div>
		</div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_ADRESS"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="address" value="{$user->address}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_CITY"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="city" value="{$user->city}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_COUNTRY"|translate}</label>
            <div class="controls">
                {$lists.country}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_COMPANY_IDENTIFICATION_NO"|translate}</label>
            <div class="controls">
                {$user->useruniqueid}
            </div>
        </div>

        <div class="job_dbk_c form_bg0">&nbsp;</div>
		{if $cfg->allow_messenger}
            <div class="control-group">
    			<label class = "control-label job_lables">{"COM_JOBS_YM"|translate}</label>
                <div class="controls">
    				<input class="inputbox" type="text" name="YM" value="{$user->YM}" size="40" />
    			</div>
    		</div>
            <div class="control-group">
    			<label class = "control-label job_lables">{"COM_JOBS_HOTMAIL"|translate}</label>
                <div class="controls">
    				<input class="inputbox" type="text" name="Hotmail" value="{$user->Hotmail}" size="40" />
    			</div>
    		</div>
            <div class="control-group">
    			<label class = "control-label job_lables">{"COM_JOBS_SKYPE"|translate}</label>
                <div class="controls">
    				<input class="inputbox" type="text" name="Skype" value="{$user->Skype}" size="40" />
    			</div>
    		</div>
		{/if}
        <div class="job_dbk_c form_bg0">&nbsp;</div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_LINKEDIN"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="linkedIN" value="{$user->linkedIN}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_FACEBOOK"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="facebook" value="{$user->facebook}" size="40" />
                &nbsp;{infobullet text="user_facebook_help"|translate}
            </div>
        </div>

        {if $cfg->enable_images}
            {* ===> if images are allowed they are showed/ upload fields are showed *}
            {if $user->picture}
                <div class="control-group">
                    <label class="control-label job_lables">{"COM_JOBS_PICTURE"|translate}{if $cfg->main_picture_require}{$fieldRequired}{/if}</label>
                    <div class="controls">
                        <img src="{$RESUME_PICTURES}/resize_{$user->picture}" />
                        <span style = "float: left;">
                            {if $cfg->main_picture_require}
                                <input type = "checkbox" name = "replace_my_file_element" value = "1" checked="checked" />
                                {"COM_JOBS_REPLACE"|translate}
                                <br />
                                <input class="inputbox" id="my_file_element" type="file" name="picture_0" />
                            {else}
                                <input type = "checkbox" name = "delete_main_picture" id = "my_file_element" value = "1"/>{"COM_JOBS_DELETE"|translate}
                            {/if}
                        </span>
                        {*<input type="checkbox" name="delete_main_picture" value="1" />&nbsp;{"COM_JOBS_DELETE"|translate}*}
                    </div>
                </div>
            {else}
                <div class="control-group">
                    <label class="control-label job_lables">{"COM_JOBS_ATTACH_PHOTO"|translate}{if $cfg->main_picture_require}{$fieldRequired}{/if}</label><br/>
                    <small style="color: Grey;">{"COM_JOBS_PICTURE_MAX_SIZE"|translate}:{$cfg->max_picture_size}k</small>
                    <div class="controls">
                        <input class="inputbox{if $cfg->main_picture_require} required{/if}" id="my_file_element" type="file" name="picture" />
                        {import_js_block}
                            {literal}
                            window.addEvent('domready', function(){
                                new MultiUpload( $( 'my_file_element' ), 1, '_{id}', true, true ,{/literal}'{$IMAGE_ROOT}'{literal});
                            });
                            {/literal}
                        {/import_js_block}
                    </div>
                </div>
            {/if}
        {/if}

        {if $cfg->uploadresume_option}
            {* attachments *}
            <div class="control-group">
                <div class="job_edit_section">
                    {"COM_JOBS_RESUMES_UPLOAD"|translate}
                </div>
            </div>
            <div class="control-group">
                 {if $user->file_name}
                    <label class="control-label job_lables">{"COM_JOBS_ATTACHMENT"|translate}</label>&nbsp;<br />
                    <div class="controls">
                        <strong><a title="userAttachment" href="{$userTable->get('links.download_file')}" target="_blank" name="show_att" id="show_att" >
                                    <img src="{$IMAGE_ROOT}cv.png" border="0" title="{"COM_JOBS_PDF_RESUME"|translate}" />{$user->file_name}</a>
                        </strong>&nbsp;&nbsp;&nbsp;
                       <a href="{$userTable->get('links.deletefile_file')}">{"COM_JOBS_DELETE"|translate}</a>
                    </div>
                 {else}
                    <label class="control-label job_lables">{"COM_JOBS_ATTACHMENT"|translate}</label>
                    <div class="controls">
                        <input name="attachment" class="inputbox{if $cfg->attach_compulsory} required{/if}"  type="file" /><br />
                        <small style="color: Grey;">{"COM_JOBS_MAXIMUM_FILE_SIZE"|translate|replace:"%s":$cfg->attach_max_size}</small>
                        {if $cfg->resume_extensions}
                            <br /><small style="color: Grey;">{"COM_JOBS_ALLOWED_ATTACHMENT_EXTENSIONS"|translate} {$cfg->resume_extensions}</small>
                        {/if}
                    </div>
                 {/if}
            </div>
        {/if}
        <div class="job_dbk_c form_bg0">&nbsp;</div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_TOTAL_EXPERIENCE"|translate}</label>
            <div class="controls">
                 {$lists.total_experience}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_SALARY"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="desired_salary" value="{$user->desired_salary}" size="20" />
                {$lists.default_currency}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_PROFILE_VISIBILITY"|translate}</label>
            <div class="controls">
                {$lists.isVisible}
            </div>
        </div>
        <div class="job_dbk_c form_bg0">&nbsp;</div>

        <div id="resume" class="custom_fields_box">
            <div class="pull-left">
                 {$custom_fields_html}
            </div>
        </div>
    </div>

    <div class="btn-toolbar form-horizontal user_edit_section">
        <div class="btn-group controls">
            <input type="submit" name="save" value="{'COM_JOBS_SAVE'|translate}" class="validate btn btn-primary save-button" />&nbsp;&nbsp;
        </div>
    </div>
</form>

</div>
{endtab}

<!-- NEW TAB EDUCATION -->
    {include file='t_myusereducation.tpl'}
<!-- NEW TAB EXPERIENCE -->
    {include file='t_myuserexperience.tpl'}

<!-- NEW TAB OTHER -->
{* education Tab *}
{starttab name="content-pane" id="usertab4" text="COM_JOBS_TAB_OTHER"|translate}
    <form action="{$ROOT_HOST}index.php" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" >
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
    <input type="hidden" name="resume_id" value="{$user->id}" />
	<input type="hidden" name="task" value="saveUserSkills" />
	<input type="hidden" name="controller" value="user" />

    <div class="user_edit_header">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr >
                <td align="left">
                    {"COM_JOBS_EDIT_USER_OTHER"|translate}
                </td>
            </tr>
        </table>
    </div>

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
           <td class="job_dbk" align="right">
               <label class="job_lables">{"COM_JOBS_USER_ACHIEVEMENTS"|translate}</label>
           </td>
           <td class="job_dbk_c">{*$user->id*}
              <textarea name="user_achievements" id="user_achievements" rows="2" cols="4"
                    style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" >{$user->user_achievements}</textarea>
           </td>
        </tr>
        <tr>
           <td class="job_dbk" align="right" width="150">
               <label class="job_lables">{"COM_JOBS_USER_GOALS"|translate}</label>
           </td>
           <td class="job_dbk">
               <textarea name="user_goals" id="user_goals" rows="2" cols="4"
                    style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" >{$user->user_goals}</textarea>
           </td>
       </tr>
       <tr>
           <td class="job_dbk" align="right">
               <label class="job_lables">{"COM_JOBS_USER_HOBBIES"|translate}</label>
           </td>
           <td class="job_dbk_c">
               <textarea name="user_hobbies" id="user_hobbies" rows="2" cols="4"
                   style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" >{$user->user_hobbies}</textarea>
           </td>
       </tr>
        <tr>
            <td align="right" class="user_edit_section"  colspan="2" >
                 <input name="save" value="{'COM_JOBS_SAVE'|translate}" class="btn btn-primary validate" type="submit" />
            </td>
        </tr>
    </table>
    </form>
{endtab}

{endpane name="content-pane" id="content-pane"}
