{* revised 1.5.4 *}
{set_css}
<h2><strong>{$page_title}</strong></h2>

{include file='elements/category/t_categories_tabs.tpl'}
{starttab name="job_tabmenu" id="tab3"}

{if $current_city}
	<div class="job_cat_breadcrumb">
    	<a href="{$links->getCityRoute()}">{"COM_JOBS_ALL"|translate}</a>
		<strong>{$current_city->cityname}</strong>
	</div>
{/if}

<table class="job_categories" cellspacing="0" cellpadding="3">
{if $cities|@count==0}
	<tr>
		<td>{"COM_JOBS_THERE_ARE_NO_SUBCATEGORIES"|translate}</td>
	</tr>
{else}
    {section name=state loop=$cities}
        {if $smarty.section.cities.rownum is odd}
        <tr>
        {/if}
            <td width="50%" valign="top">
                <div class="job_maincat">
                    <a href="{$cities[state]->link}">{$cities[state]->job_cityname}</a><span style="font-weight: normal;font-size: 12px">({$cities[state]->jobs_no})</span>
                    {if $cities[state]->jobs_no>0}
                    <a href="{$cities[state]->view}">
                        <img src="{$IMAGE_ROOT}document-text-image.png" border="0" alt="" /></a>
                    {/if}
                </div>
            </td>
        {if $smarty.section.cities.rownum is not odd}
        </tr>
        {/if}
    {/section}
{/if}
</table>
{endtab}
