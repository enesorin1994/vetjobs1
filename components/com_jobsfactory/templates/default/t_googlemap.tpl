{include file='js/t_javascript_language.tpl'}
{set_css}

{if ($googleMapX!="" && $googleMapY!="") || $cfg->google_key!="" }

	<div id="map_canvas" style="width: {$cfg->googlemap_gx|default:250}px; height: {$cfg->googlemap_gy|default:250}px"></div>
    {include file="js/t_javascript_maps.tpl" googleMapX=`$googleMapX` googleMapY=`$googleMapY`}

    {import_js_block}
        {literal}
        window.addEvent('domready', function () {
            gmap_showmap.delay(500);
        });
        {/literal}
    {/import_js_block}
{else}
	{"COM_JOBS_NO_GOOGLE_MAP_DEFINED"|translate}
{/if}
	
