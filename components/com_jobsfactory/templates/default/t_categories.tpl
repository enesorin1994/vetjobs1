{* revised 1.5.4 *}
{set_css}
<h2><strong>{$page_title}</strong></h2>

{include file='elements/category/t_categories_tabs.tpl'}

{*{startpane name="job_tabmenu" active="tab1"}*}
{starttab name="job_tabmenu" id="tab1"}

    {* Alphabet filter *}
    <div class="text-center" style = "width: 90%;">{$filter_letter}</div>

    {if $current_cat}
        <div class="job_cat_breadcrumb">
            <a href="{$links->getCategoryRoute()}">{"COM_JOBS_ALL"|translate}</a>
            <strong>{$current_cat->catname}</strong>
            {if $current_cat->description}
                <div class="job_cat_description">
                    {$current_cat->description}
                </div>
            {/if}
        </div>
    {/if}
<table class="job_categories" cellspacing="0" cellpadding="3">
{if $categories|@count==0}
	<tr>
		<td>{"COM_JOBS_THERE_ARE_NO_SUBCATEGORIES"|translate}</td>
	</tr>
{else}
    {section name=category loop=$categories}
      {if $smarty.section.category.rownum is odd}
        <tr>
      {/if}
	        <td width="50%" valign="top">
                <div class="job_maincat">
                    <a href="{$categories[category]->link}">{$categories[category]->catname}</a><span style="font-weight: normal;font-size: 12px">({$categories[category]->nr_a})</span>
                    {if $categories[category]->nr_a>0}
                    <a href="{$categories[category]->view}">
                        <img src="{$IMAGE_ROOT}document-text-image.png" border="0" alt="" /></a>
                    {/if}
                    {if $is_logged_in}
                        <a href="{$categories[category]->link_watchlist}">
                            {if $categories[category]->watchListed_flag}
                                <img src="{$IMAGE_ROOT}f_watchlist_0.png" border="0" alt="" width="16" title="{'COM_JOBS_REMOVE_FROM_WATCHLIST_CAT'|translate}"/>
                            {else}
                                <img src="{$IMAGE_ROOT}f_watchlist_1.png" border="0" alt="" width="16" title="{'COM_JOBS_ADD_TO_WATCHLIST_CAT'|translate}"/>
                            {/if}
                        </a>
                        {if $isCompany}
                        <a href="{$categories[category]->link_new_listing}"><img src="{$IMAGE_ROOT}new_listing.png" border="0" alt="{"COM_JOBS_NEW_LISTING_IN_CATEGORY"|translate}"
                            title = "{"COM_JOBS_NEW_LISTING_IN_CATEGORY"|translate}" class="new_listing" /></a>
                        {/if}
                    {/if}
                </div>
                <div class="job_subcat_container">
                  {if $categories[category]->subcategories|@count}
                    {section name=subcategory loop=$categories[category]->subcategories}
                        <div>
                            <a href="{$categories[category]->subcategories[subcategory]->link}">{$categories[category]->subcategories[subcategory]->catname} ({$categories[category]->subcategories[subcategory]->nr_a})</a>
                            {if $categories[category]->subcategories[subcategory]->nr_a>0}
                                <a href="{$categories[category]->subcategories[subcategory]->view}">
                                    <img src="{$IMAGE_ROOT}document-text-image.png" border="0" alt="" /></a>
                            {/if}

                            {if $is_logged_in}
                                <a href="{$categories[category]->subcategories[subcategory]->link_watchlist}">
                                    {if $categories[category]->subcategories[subcategory]->watchListed_flag}
                                        <img src="{$IMAGE_ROOT}f_watchlist_0.png" border="0" alt=""  width="16" title="{'COM_JOBS_REMOVE_FROM_WATCHLIST_CAT'|translate}"/>
                                    {else}
                                        <img src="{$IMAGE_ROOT}f_watchlist_1.png" border="0" alt="" width="16" title="{'COM_JOBS_ADD_TO_WATCHLIST_CAT'|translate}" />
                                    {/if}
                                </a>
                                {if $isCompany}
                                    <a href = "{$categories[category]->subcategories[subcategory]->link_new_listing}"><img src="{$IMAGE_ROOT}new_listing.png" border = "0"
                                        alt="{"COM_JOBS_NEW_LISTING_IN_CATEGORY"|translate}"
                                        title="{"COM_JOBS_NEW_LISTING_IN_CATEGORY"|translate}"
                                        class="new_listing" /></a>
                                {/if}
                            {/if}

                        </div>
                    {/section}
                  {/if}
                </div>
	</td>
{if $smarty.section.category.rownum is not odd}
</tr>
{/if}
{/section}
{/if}
</table>

<div style="clear: both;"></div>
{endtab}
