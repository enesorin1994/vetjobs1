{include file='elements/userprofile/t_profile_tabs.tpl'}

{import_js_file url="Stickman.MultiUpload.js"}
{include file='elements/userprofile/t_profile_validate.tpl'}
{assign var="fieldRequired" value=$lists.fieldRequiredlink}

<div class="user_edit_header">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr >
            <td align="left">
                {"COM_JOBS_EDIT_COMPANY_DETAILS"|translate}
            </td>
            <td align="right">
                <input name="save" value="{'COM_JOBS_SAVE'|translate}" class="btn btn-primary validate" type="submit" />
            </td>
        </tr>
    </table>
</div>

<span class="v_spacer_15"></span>
<div class="last_dates">
    <span>{"COM_JOBS_LAST_MODIFIED_ON"|translate}{if $user->last_modified!= NULL}{$user->last_modified}{else}
          {"COM_JOBS_NEW"|translate}{/if}</span>
    <span style="float:right;">{"COM_JOBS_LAST_LOGGED_ON"|translate}{if $user->last_loggedon!= NULL}{$user->last_loggedon}{else}
          {"COM_JOBS_NO_RESULTS_FOUND"|translate}{/if}</span>
</div>
    <div class="form-horizontal jobs_top_align" id="profesionalprofile" style="border:solid 1px #DDDDDD;">

        <div class="control-group">
			<label class="control-label job_lables" for="name">{"COM_JOBS_COMPANYNAME"|translate}{$fieldRequired}</label>
            <div class="controls">
                {if $user->name == '' && $user->username}
                    <input class="inputbox required" type="text" name="name" value="{$user->username}" size="40" />
                {else}
                    <input class="inputbox required" type="text" name="name" value="{$user->name}" size="40" />
                {/if}
			</div>
		</div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_COMPANY_WEBSITE"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="website" value="{$user->website}" size="40" />
                &nbsp;{infobullet text="user_website_help"|translate}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_COMPANY_SHORT_DESCRIPTION"|translate}</label>
            <div class="controls">
                <textarea name="shortdescription" cols="40" rows="5">{$user->shortdescription}</textarea>
            </div>
        </div>

        {if $cfg->enable_images}
            {* Picture Gallery *}
            {* ===> if images are allowed they are showed/ upload fields are showed *}
            {if $user->picture}
                <div class="control-group">
                    <label class="control-label job_lables">{"COM_JOBS_LOGO"|translate}{if $cfg->main_picture_require}{$fieldRequired}{/if}</label>
                    <div class="controls">

                        <img src="{$COMPANY_PICTURES}/resize_{$user->picture}"/>

                        <span style = "float: left;">
                            {*onchange = "deleteMainToggleRequire(this.checked)"*}
                            {if $cfg->main_picture_require}
                                <input type = "checkbox" name = "replace_my_file_element" value = "1" checked="checked" />
                                {"COM_JOBS_REPLACE"|translate}
                                <br />
                                <input class="inputbox" id="my_file_element" type="file" name="picture" />
                            {else}
                                <input type = "checkbox" name = "delete_main_picture" id = "my_file_element" value = "1"/>{"COM_JOBS_DELETE"|translate}
                            {/if}
                        </span>
                        {*<input type="checkbox" name="delete_main_picture" value="1" />{"COM_JOBS_DELETE"|translate}*}

                    </div>
                </div>
            {else}
                <div class="control-group">
                        <label class="control-label job_lables">{"COM_JOBS_ATTACH_LOGO"|translate}{if $cfg->main_picture_require}{$fieldRequired}{/if}</label>
                    <div class="controls">
                        <small style="color: Grey;">{"COM_JOBS_PICTURE_MAX_SIZE"|translate}:{$cfg->max_picture_size}k</small>
                        <input class="inputbox{if $cfg->main_picture_require} required{/if}" id="my_file_element" type="file" name="picture" />
{*
                        {import_js_block}
                            {literal}
                            window.addEvent('domready', function(){
                                new MultiUpload( $( 'my_file_element' ), 1, '_{id}', false, false ,{/literal}'{$IMAGE_ROOT}'{literal});
                            });
                            {/literal}
                        {/import_js_block}
*}
                    </div>
                </div>
            {/if}
        {/if}

        <div><span>&nbsp;</span></div>

        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_ACTIVITY_DOMAIN"|translate}</label>
            <div class="controls">
                {$lists.activityDomains}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_CONTACT_USERNAME"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="contactname" value="{$user->contactname}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_POSITION"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="contactposition" value="{$user->contactposition}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_CONTACT_PHONE"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="contactphone" value="{$user->contactphone}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_CONTACT_EMAIL"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="contactemail" value="{$user->contactemail}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <td class="job_dbk_c form_bg0" align="center" colspan="2">&nbsp;</td>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_ABOUT_US"|translate}</label>
            <div class="controls">
                {$lists.about_us}
            </div>
        </div>

        <div><span>&nbsp;</span></div>

        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_ADRESS"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="address" value="{$user->address}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_CITY"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="city" value="{$user->city}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_COUNTRY"|translate}</label>
            <div class="controls">
                {$lists.country}
            </div>
        </div>
        {if $cfg->google_key!=""}
            <div class="control-group">
                <div class="controls">
                    <a href="#" onclick="window.open('index.php?option=com_jobsfactory&amp;controller=maps&amp;tmpl=component&amp;task=googlemap_tool','SelectGoogleMap','width=650,height=500');return false;">{"COM_JOBS_SELECT_COORDINATES"|translate}</a>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label job_lables" for="googleX">{"COM_JOBS_COORDINATE_X"|translate}</label>
                <div class="controls">
                    <input class="inputbox" type="text" id="googleX" name="googleMaps_x" value="{$user->googleMaps_x}" size="20" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label job_lables" for="googleY">{"COM_JOBS_COORDINATE_Y"|translate}</label>
                <div class="controls">
                    <input class="inputbox" type="text" id="googleY" name="googleMaps_y" value="{$user->googleMaps_y}" size="20" />
                </div>
            </div>
            <div class="control-group">
                <div class="controls1">
                    <br />
                    {if $user->googleMaps_x!="" && $user->googleMaps_y!=""}
                        <iframe src="{$ROOT_HOST}index.php?option=com_jobsfactory&controller=maps&amp;tmpl=component&amp;task=googlemap&amp;x={$user->googleMaps_x}&amp;y={$user->googleMaps_y}"
                                style="margin: 0 5px; height: 100%; width: 98%; max-width: {$cfg->googlemap_gx+25|default:'600'}px; min-height: 350px !important; max-height: {$cfg->googlemap_gy+20|default:'350'}px;"></iframe>
                    {/if}
                </div>
            </div>
        {/if}

        <div><span>&nbsp;</span></div>

        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_COMPANY_IDENTIFICATION_NO"|translate}</label>
            <div class="controls">
                {$user->useruniqueid}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_FACEBOOK"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="facebook" value="{$user->facebook}" size="40" />
                &nbsp;{infobullet text="user_facebook_help"|translate}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_TWITTER"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="twitter" value="{$user->twitter}" size="40" />
                &nbsp;{infobullet text="user_twitter_help"|translate}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_EMPLOYER_VISIBILITY"|translate}</label>
            <div class="controls">
                {if $cfg->employervisibility_enable}
                        {$lists.isVisible}
                 &nbsp;{infobullet text="employer_visibility_help"|translate}
                {else}
                    <strong>{if $cfg->employervisibility_val == $EMPLOYER_VISIBILITIES.EMPLOYER_TYPE_CONFIDENTIAL}{"COM_JOBS_CONFIDENTIAL"|translate}{else}{"COM_JOBS_PUBLIC"|translate}{/if}</strong>
                {/if}
            </div>
        </div>
        <div><span>&nbsp;</span></div>

        <div>
            <div class="width100left">
                 {$custom_fields_html}
            </div>
        </div>
	</div>
    <div style="clear:both;" ></div>
    <br />
	<div class="btn-toolbar form-horizontal user_edit_section">
    	<div class="btn-group controls">
        	<input name="save" value="{jtext text='COM_JOBS_SAVE'}" class="validate btn btn-primary" type="submit" />&nbsp;&nbsp;
		</div>
		<div class="btn-group controls">
        	<input name = "cancel" value = "{'COM_JOBS_CANCEL'|translate}" class = "btn" type = "submit" />
    	</div>
	</div>

    <div style="clear:both;" ></div>
