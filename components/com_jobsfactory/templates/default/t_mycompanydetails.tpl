{* Include Validation script *}
{include file='js/t_javascript_language.tpl'}
{set_css}
<h1>{'COM_JOBS_COMPANY_PROFILE'|translate}&nbsp;{$user->name}</h1>

<div class="jobs_userprofile">
<form action="{$ROOT_HOST}index.php?option=com_jobsfactory&controller=user&task=savecompanydetails" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" >
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
    <input type="hidden" name="userid" value="{$companyId}" />
    {if $jobsUserId }
        <input type="hidden" name="id" value="{$jobsUserId}" />
    {/if}
    <input type="hidden" name="useruniqueid" value="{$user->useruniqueid}" />
    <input type="hidden" name="isCompany" value="1" />
	<input type="hidden" name="task" value="saveCompanyDetails" />
	<input type="hidden" name="controller" value="user" />

    <span class="v_spacer_15"></span>
    {include file='t_companyprofile.tpl'      label="COM_JOBS_COMPANY_PROFILE"|translate}

</form>
</div>
