<div class="pagination">
    {$pagination->getPagesLinks()}

    <div class="v_spacer_5">&nbsp;</div>
    <p class="counter">
        {$pagination->getPagesCounter()}&nbsp;
        <span class="form-limit">
            {"COM_JOBS_DISPLAY"|translate} {$pagination->getLimitBox()}
        </span>     
    </p>
</div>
