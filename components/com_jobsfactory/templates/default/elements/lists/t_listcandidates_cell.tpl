{if $index is odd}
	{assign var=class value="1"}
{else}
	{assign var=class value="2"}
{/if}
{if $candidate->featured && $candidate->featured!='none'}
	{assign var=class_featured value="listing-"|cat:$candidate->featured}
{else}
	{assign var=class_featured value=""}
{/if}

<tr class="{if $class_featured}{$class_featured}{else}job_row_{$class}{/if}">
	<td  class="job_dt candidate_title" valign="top">
        <div class="candidate_title">
            <span class="pull-left1">
            <label class="job_lables"> <a href="{$candidate->userlink}">{*$candidate->get('links.candidate_profile')*}
                {$candidate->fullname}</a></label>

            </span>
            <br />
            {if $candidate->function != ''}
                <span class="job_category pull-left">
                     {'COM_JOBS_POSITION'|translate}:
                     {$candidate->function}<br />
                </span>
            {/if}
            <br />

                <span class="job_category  pull-left">
                    {'COM_JOBS_SALARY'|translate}:
                    {$candidate->_cache->desired_salary} {$lists.default_currency} <br />
                </span>
        </div>
	    {positions position="cell-header" item=$candidate page="candidates"}
	</td>
    <td class="job_dbk" valign="top" width="100">
            {$candidate->age} <br />
            {$candidate->gendername}
    </td>
    <td class="job_dbk" valign="top" width="200">
           {'COM_JOBS_MY_EXPERIENCE'|translate}:
               {$candidate->levelname}<br />
           {'COM_JOBS_LOCATION'|translate}:
               {$candidate->_cache->city}<br />
           {"COM_JOBS_LAST_APPLIED_ON"|translate}{$candidate->last_applied}
    </td>
</tr>

{if $class_featured}
<tr>
   <td class="v_spacer_7" colspan="4">&nbsp;</td>
</tr>
{/if}