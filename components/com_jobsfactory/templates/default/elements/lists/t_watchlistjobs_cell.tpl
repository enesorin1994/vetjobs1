{if $index is odd}
	{assign var=class value="1"}
{else}
	{assign var=class value="2"}
{/if}
{if $job->featured && $job->featured!='none'}
	{assign var=class_featured value="listing-"|cat:$job->featured}
{else}
	{assign var=class_featured value=""}
{/if}

<tr class="{if $class_featured}{$class_featured}{else}job_row_{$class}{/if}">
	<td class="job_dt" valign="top" colspan="3">
        <div class="job_title">
            {if $class_featured}
                <img src="{$TEMPLATE_IMAGES}f_featured.png" title="{'COM_JOBS_NEW_MESSAGES'|translate}" alt="{'COM_JOBS_NEW_MESSAGES'|translate}" class="featured-image" />
            {/if}
            {if $job->employer_visibility == $EMPLOYER_VISIBILITIES.EMPLOYER_TYPE_CONFIDENTIAL}
                <img src="{$TEMPLATE_IMAGES}private.png" title="{'COM_JOBS_CONFIDENTIAL'|translate}" alt="{'COM_JOBS_CONFIDENTIAL'|translate}" />
            {/if}
            <a href="{$job->get('links.jobs')}">{$job->title}</a>

	        {positions position="cell-header" item=$job page="jobs"}
        </div>
	</td>
    <td class="job_dbk" valign="bottom" align="right">
        <span class="job_uniqueid">{'COM_JOBS_ID'|translate}: {$job->jobuniqueID}</span>
    </td>
</tr>
<tr class="{if $class_featured}{$class_featured}{else}job_row_{$class}{/if}">
	<td class="job_dbk" valign="top">


        <span class="job_list_category">
            <img src = "{$TEMPLATE_IMAGES}folder.png"
                 title = "{'COM_JOBS_CATEGORY'|translate}"
                 alt = "{'COM_JOBS_CATEGORY'|translate}"
                 style = "vertical-align: bottom;margin-right: -11px;"
                    />
            {if $job->get('catname')}
                <a href="{$job->get('links.filter_cat')}">{$job->get('catname')}</a>&nbsp;
            {else}
                &nbsp;-&nbsp;
            {/if}
		</span>
        {if $job->has_file}
            <img src = "{$TEMPLATE_IMAGES}attach.gif"
                 title = "{'COM_JOBS_HAS_ATTACHMENTS'|translate}"
                 alt = "{'COM_JOBS_HAS_ATTACHMENTS'|translate}"
                 style = "height:16px;vertical-align: middle;margin-left: 3px;"
                    />
            <span style = "color:grey;font-size: 10px;">{'COM_JOBS_FILES'|translate}</span>
        {/if}
        {if !$job->isMyJob() & $is_logged_in}
            {if $job->get('favorite')}
                <span class='add_to_watchlist'>
                        <a href='{$job->get('links.del_from_watchlist')}'>
                            <img src="{$IMAGE_ROOT}f_watchlist_0.png" style="height:16px;vertical-align: bottom;" title="{'COM_JOBS_REMOVE_FROM_FAVORITESLIST'|translate}" alt="{'COM_JOBS_REMOVE_FROM_FAVORITESLIST'|translate}"/></a>
                    </span>
            {else}
                <span class="add_to_watchlist">
                        <a href='{$job->get('links.add_to_watchlist')}'>
                            <img src="{$IMAGE_ROOT}f_watchlist_1.png" style="height:16px;vertical-align: bottom;" title="{'COM_JOBS_ADD_TO_WATCHLIST'|translate}" alt="{'COM_JOBS_ADD_TO_WATCHLIST'|translate}"/></a>
                    </span>
            {/if}
        {/if}
        {if $job->shortdescription}
            <span class="job_category1">
                &nbsp;<img src="{$TEMPLATE_IMAGES}f_expand_01.png" title="{'COM_JOBS_SHOW_DESCRIPTION'|translate}" class="job_link" width="16"
                        alt="{'COM_JOBS_SHOW_DESCRIPTION'|translate}" onclick="jobObject.toggleDescription('job_short_description_{$index}',this);"/>
            </span>
            <div id="job_short_description_{$index}" class="job_short_description">
                {$job->shortdescription}
            </div>
        {/if}
        {positions position="cell-left" item=$job page="jobs"}
	</td>
    <td class="job_dbk" valign="top">
    		<span class="job_date">{$job->_cache->companyname}</span><br/>
    </td>
    <td class="job_dbk" valign="top">
        {if $cfg->enable_location_management}
            <span class="job_date">{$job->_cache->cityname}</span>/
            <span class="job_date">{$job->_cache->locname}</span>
        {else}
            <span class="job_date">{$job->_cache->cityname}</span>
        {/if}
    </td>
    <td class="job_dbk right" valign="top">
		<!-- [+] Time Ending -->
		{if $job->close_offer}
			<span class='canceled_on'>
                {if $job->end_date gt $job->closed_date}
                    {'COM_JOBS_CANCELED_ON'|translate}
                {else}
                    {'COM_JOBS_CLOSED_ON'|translate}
                {/if}

                {printdate date=$job->closed_date}
            </span>
		{elseif  $job->get('expired')}
		   <span class='expired'>{"COM_JOBS_EXPIRED"|translate}</span>
		{else}
            {if $cfg->enable_countdown}
                 <span class="timer"> {$job->get('countdown')}</span>
            {else}
                 <span class=''> {$job->get('enddate_text')}</span>
            {/if}
		{/if}
		<!-- [-] Time Ending -->
	</td>
</tr>
<tr class="{if $class_featured}{$class_featured}{else}job_row_{$class}{/if}">
	<td colspan="4" class="job_foot right">
		{positions position="cell-footer" item=$job page="jobs"}
	</td>
</tr>
{if $class_featured}
<tr>
   <td class="v_spacer_7" colspan="4">&nbsp;</td>
</tr>
{/if}
