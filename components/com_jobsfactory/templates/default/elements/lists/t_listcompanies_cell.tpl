{if $index is odd}
	{assign var=class value="1"}
{else}
	{assign var=class value="2"}
{/if}
{if $company->featured && $company->featured!='none'}
	{assign var=class_featured value="listing-"|cat:$company->featured}
{else}
	{assign var=class_featured value=""}
{/if}

<tr class="{if $class_featured}{$class_featured}{else}job_row_{$class}{/if}">
	<td  class="job_dt company_title" valign="top" width="110">
        <div class="company_title1">
                <span class="job_category">
                    {if $company->picture}
                        <img src="{$COMPANY_PICTURES}/resize_{$company->picture}" width="100" />
                    {else}
                        <img src="{$IMAGE_ROOT}/user/logo.png" width="80" />
                    {/if}
                </span>
        </div>
	    {positions position="cell-header" item=$company page="companies"}
	</td>
    <td class="job_dbk" valign="top" width="*%">
        <a href="{$links->getUserdetailsRoute($company->userid)}" target="_blank" >{$company->name}</a>
        <br />

        {if !$company->is_my_company & $is_logged_in}
            <a href="{$company->link_watchlist}">
                {if $company->watchListed_flag}
                    <img src="{$IMAGE_ROOT}f_watchlist_0.png" border="0" alt="" width="16" title="{'COM_JOBS_REMOVE_FROM_WATCHLIST_COMPANY'|translate}"/>
                {else}
                    <img src="{$IMAGE_ROOT}f_watchlist_1.png" border="0" alt="" width="16" title="{'COM_JOBS_ADD_TO_WATCHLIST_COMPANY'|translate}"/>
                {/if}
            </a>
        {/if}
        <br />
        {$company->field_of_activity}

    </td>
    {if $jobs_allow_rating}
    <td class="job_dbk" valign="top" width="200">
        {if $company->users_count >= 5}
            <i class="ads-icon-star"></i>&nbsp;{$company->user_rating} <span class="muted">({$company->users_count})</span>
        {else}
            <span class="muted small">{jtext text="COM_JOBS_NOT_ENOUGH_VOTES_RECEIVED"}</span>
        {/if}
        <br />
        <div style="vertical-align: bottom" class="rate-block">
            {if $company->my_rating_user}
              <ul class="user-rating">
                {section name=full start=0 loop=$company->my_rating_user}
                  <li class="full"></li>
                {/section}
                {section name=empty start=$company->my_rating_user loop=5}
                  <li></li>
                {/section}
              </ul>
            {elseif !$company->is_my_company}
              <div class="actions">
                <a href="{generate_url _='CompanyVoteRoute' id=$company->userid}" class="btn btn-smalls btn-success button-rate-user">{jtext text="COM_JOBS_RATE_COMPANY"}</a>
              </div>
            {/if}
        </div>
    </td>
    {/if}
    <td class="job_dbk" valign="top" width="200">
           {'COM_JOBS_LOCATION'|translate}:
               {$company->city}<br />
           {if $company->no_available_jobs!==null}

               <a href="{$links->getCompanyJobsRoute($company->userid)}" target="_blank" >
                    <span class="number">{$company->no_available_jobs}</span>
                </a> {"COM_JOBS_AVAILABLE_JOBS"|translate}
           {/if}
    </td>
</tr>
{if $class_featured}
<tr>
   <td class="v_spacer_7" colspan="4">&nbsp;</td>
</tr>
{/if}
