{if $index is odd}
	{assign var=class value="1"}
{else}
	{assign var=class value="2"}
{/if}
{if $candidate->featured && $candidate->featured!='none'}
	{assign var=class_featured value="listing-"|cat:$candidate->featured}
{else}
	{assign var=class_featured value=""}
{/if}

<tr class="{if $class_featured}{$class_featured}{else}job_row_{$class}{/if}">
	<td  class="job_dt candidate_title" valign="top">
        <div class="candidate_title1">
            <label class="job_lables"> <a href="{$candidate->get('links.candidate_profile')}">{$candidate->fullname}</a></label>
            <br />
                <span class="job_category">
                     {'COM_JOBS_POSITION'|translate}:
                     {$candidate->function}<br />
                </span>
                <span class="job_category">
                    {'COM_JOBS_SALARY'|translate}:
                    {$candidate->_cache->desired_salary} {$lists.default_currency}<br />
                </span>
        </div>
	    {positions position="cell-header" item=$candidate page="candidates"}
	</td>
    <td class="job_dbk" valign="top" width="100">
            {$candidate->age} <br />
            {$candidate->gendername}
    </td>
    <td class="job_dbk" valign="top" width="200">
           {'COM_JOBS_MY_EXPERIENCE'|translate}:
               {$candidate->levelname}<br />
           {'COM_JOBS_LOCATION'|translate}:
               {$candidate->_cache->city}<br />
           {"COM_JOBS_LAST_APPLIED_ON"|translate}{$candidate->last_applied}
    </td>
    <td class="job_dbk" valign="top">
       <span class="job_category radio">
               {$candidate->action}<br />
       </span>
   </td>
</tr>
{if $class_featured}
<tr>
   <td class="v_spacer_7" colspan="4">&nbsp;</td>
</tr>
{/if}
