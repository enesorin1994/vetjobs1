{if $index is odd}
	{assign var=class value="1"}
{else}
	{assign var=class value="2"}
{/if}

{if $application->featured && $application->featured!='none'}
	{assign var=class_featured value="listing-"|cat:$application->featured}
{else}
	{assign var=class_featured value=""}
{/if}

<tr class="{if $class_featured}{$class_featured}{else}job_row_{$class}{/if}">
	<td  class="job_dt" valign="top">
        <div class="job_title">
            {if $class_featured}
               <img src="{$TEMPLATE_IMAGES}f_featured.png" title="{'COM_JOBS_NEW_MESSAGES'|translate}" alt="{'COM_JOBS_NEW_MESSAGES'|translate}" class="featured" />
            {/if}
            <a href="{$ROOT_HOST}index.php?option=com_jobsfactory&task=viewapplications&id={$application->job_id}&Itemid={$Itemid}">
                {$application->title}
            </a>
        </div>
        {positions position="cell-header" item=$application page="jobs"}
	</td>
    <td class="job_dbk" valign="bottom" align="right">
        <span class="job_uniqueid">{'COM_JOBS_ID'|translate}: {$application->jobuniqueID}</span>
    </td>
    {if $isCompany}
        <td align="center">{'COM_JOBS_NR_CANDIDATES'|translate}</td>
    {/if}
</tr>
<tr class="{if $class_featured}{$class_featured}{else}job_row_{$class}{/if}">
    <td class="job_dbk" valign="bottom">
        {if $application->shortdescription}
            <div style = "float: left;">
                &nbsp;<img src="{$TEMPLATE_IMAGES}f_expand_01.png" title="{'COM_JOBS_SHOW_DESCRIPTION'|translate}" class="job_link"
                        alt="{'COM_JOBS_SHOW_DESCRIPTION'|translate}" onclick="jobObject.toggleDescription('job_short_description_{$index}',this);"/>
            </div>
            <div id="job_short_description_{$index}" class="job_short_description">
                {$application->shortdescription}
            </div>
        {/if}
        <span class="job_category" style="{if !$application->shortdescription}margin-left: 0;{/if}">
            <img src = "{$TEMPLATE_IMAGES}folder.png"
             title = "{'COM_JOBS_CATEGORY'|translate}"
             alt = "{'COM_JOBS_CATEGORY'|translate}"
             style = "vertical-align: bottom;margin-right: -11px;"
                />
            {if $application->catname}
                <span style="margin-left: 10px; font-size: 95%;">{$application->catname}</span>
            {else}
                &nbsp;-&nbsp;
            {/if}
        </span>
        {positions position="cell-left" item=$application page="jobs"}
	</td>
    <td class="job_dbk" valign="top">
        <span class="job_date">{$application->modified}</span><br/>
        <!-- [+] Time Ending -->
        {if $application->close_offer}
            <span class='canceled_on'>
            {if $application->end_date gt $application->closed_date}
                {'COM_JOBS_CANCELED_ON'|translate}
            {else}
                {'COM_JOBS_CLOSED_ON'|translate}
            {/if}:
            </span>
            {printdate date=$application->closed_date}
        {/if}
        <!-- [-] Time Ending -->
        {positions position="cell-middle" item=$application page="jobs"}

    </td>

    {if $isCompany}
        <td class="job_dbk" valign="top" align="center">
            <div>
                {if $application->show_candidate_nr ==1}
                    <a href="{$links->getJobDetailRoute($application->job_id)}">
                        <span class="hasTip" title="{'COM_JOBS_NR_CANDIDATES'|translate}">
                            {$application->nr_applicants}
                        </span>
                    </a>
                {else}
                    -
                {/if}
                <br />

            </div>
        </td>
    {/if}
</tr>
<tr class="{if $class_featured}{$class_featured}{else}job_row_{$class}{/if}">
	<td colspan="3" align="right" class="job_foot">
		{positions position="cell-footer" item=$application page="jobs"}
	</td>
</tr>
