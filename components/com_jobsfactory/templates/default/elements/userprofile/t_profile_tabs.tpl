{literal}
<script type="text/javascript">
    jQuery(document).ready( function ($) {
        $(document).on("click", "#job_tabmenu li a", function(event){
           location.href = this.rel;
        });
    });
</script>
{/literal}

<!-- Begin Content -->
<div>
    <ul class="nav nav-tabs" id="job_tabmenu">
        <li class="{if ('userdetails' == $task)}active{else}inactive{/if}">
           <a data-toggle="tab" href="#tab1" rel="{$links->getUserdetailsRoute()}">{"COM_JOBS_MY_PROFILE"|translate}</a>
        <li>

        {if $paymentItems}
            <li class = "{if ('payments.history' == $task)}active{else}inactive{/if}">
                <a data-toggle="tab" href="#tab2" rel="{$links->getPaymentsHistoryRoute()}">{"COM_JOBS_PAYMENT_BALANCE"|translate}</a>
            </li>
        {/if}
    </ul>

    <div id="config-document" class="tab-content">
        <div id="tab1" class="tab-pane {if ('userdetails' == $task)}active{else}inactive{/if}">
            <div class="row-fluid">
                <div class="span12">&nbsp;</div>
            </div>
        </div>
        {if $paymentItems}
            <div id="tab2" class="tab-pane {if ('payments.history' == $task)}active{else}inactive{/if}">
                <div class="row-fluid">
                    <div class="span12">&nbsp;</div>
                </div>
            </div>
        {/if}
    </div>
    </div>
<!-- End Content -->
<div style="clear: both;"></div>
