<form id="add_experience" name="add_experience" enctype="multipart/form-data" action="{$ROOT_HOST}index.php" method="POST" >
	<input type="hidden" name="id" value="{$user_experience->id}" />
    <input type="hidden" name="resume_id" value="{$user->id}" />
	<input type="hidden" name="task" value="saveExperience" />
	<input type="hidden" name="option" value="{$option}" />

    <div class="form-horizontal jobs_top_align" style="border:solid 1px #DDDDDD; border-top:none; ">

        <div class="control-group">
            <div class="job_edit_section">
                {"COM_JOBS_RESUME_EXPERIENCE"|translate}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_COMPANY"|translate}</label>
            <div class="controls">
                <input class="inputbox input-xlarge" name="company" type="text" size="60" value="{$user_experience->company}" />
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_POSITION"|translate}</label>
            <div class="controls">
               <input class="inputbox input-xlarge" name="position" type="text" size="60" value="{$user_experience->position}" />
           </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_EXP_RESPONSIBILITIES"|translate}</label>
            <div class="controls">
                <textarea name="exp_description" id="exp_description" rows="2" cols="4"
                                style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" ></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_DOMAIN"|translate}</label>
            <div class="controls">
                {$lists.domain}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_START_DATE"|translate}</label>
            <div class="controls">
                {$lists.exp_start_date_html}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_END_DATE"|translate}</label>
            <div class="controls">
                {$lists.exp_end_date_html}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_CITY"|translate}</label>
            <div class="controls">
                <input class="inputbox input-xlarge" name="city" type="text" size="60" value="{$user_experience->city}" />
            </div>
        </div>
        <div class="control-group job_edit_section">
            <div class="pull-right">
                  <input type="submit" name="saveexperience" value="{'COM_JOBS_SAVE_EXPERIENCE'|translate}" class="btn btn-primary validate" />
                <a href="#" class="cancel_experience btn" rel="{$user->id}" >&nbsp;{"COM_JOBS_CANCEL"|translate}</a>
            </div>
        </div>
    </div>

</form>

