<script language="javascript" type="text/javascript" src="{$ROOT_HOST}components/com_jobsfactory/js/user_resume.js"></script>
{include file='js/t_javascript_language.tpl'}
{set_css}

<form id="edit_education" name="edit_education" enctype="multipart/form-data" action="{$ROOT_HOST}index.php" method="POST" >
	<input type="hidden" name="id" value="{$user_education->id}" />
    <input type="hidden" name="resume_id" value="{$resume_id}" />
	<input type="hidden" name="task" value="saveEducation" />
	<input type="hidden" name="controller" value="user" />
	<input type="hidden" name="option" value="{$option}" />

    <div class="job_edit_section">
        <h3 class="page-header item-title" style="border: none !important;">
            {"COM_JOBS_USER_EDUCATION"|translate}
        </h3>
    </div>
    <span class="v_spacer_15"></span>

    <div class="form-horizontal jobs_top_align" style="border:solid 1px #DDDDDD; padding: 4px 0;">
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_STUDY_LEVEL"|translate}</label>
            <div class="controls">
                {$lists.studies_level}
            </div>
        </div>

        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_EDU_INSTITUTION"|translate}</label>
            <div class="controls">
                <input class="inputbox" name="edu_institution" type="text" size="60" value="{$user_education->edu_institution}">
            </div>
        </div>

        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_COUNTRY"|translate}</label>
            <div class="controls">
                {$lists.country}
            </div>
        </div>

        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_START_DATE"|translate}</label>
            <div class="controls">
                {if $task=='FormUserEducation'}
                    {printdate date=$user_education->start_date}
                    <input class="text_area" type="hidden" name="start_date" id="start_date" size="12" maxlength="19" value="{$user_education->start_date|date_format:'%Y-%m-%d %H:%M:%S'}" alt="start_date"/>
                {else}
                    {$lists.edu_start_date_html}
                {/if}
            </div>
        </div>

        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_END_DATE"|translate}</label>
            <div class="controls">
                {$lists.edu_end_date_html}
            </div>
        </div>

        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_CITY"|translate}</label>
            <div class="controls">
                <input class="inputbox" name="city" type="text" size="60" value="{$user_education->city}">
            </div>
        </div>

        <div class="control-group">
           <div class="custom_fields_box">
               {$custom_fields_html}
           </div>
        </div>

        <div class="btn-toolbar form-horizontal job_edit_section">
           <div class="btn-group controls">
               <input type="submit" name="saveeducation" value="{'COM_JOBS_SAVE_EDUCATION'|translate}" class="btn btn-primary validate" />
           </div>
           <div class="btn-group controls">
               <a href="{$lists.cancel_form}" class="back_button_cancel"><input name="cancel" value="{jtext text='COM_JOBS_CANCEL'}" class="btn" type="button" /></a>
           </div>
        </div>

    </div>

</form>

