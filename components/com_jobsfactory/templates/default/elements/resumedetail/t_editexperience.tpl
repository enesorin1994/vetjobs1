<script language="javascript" type="text/javascript" src="{$ROOT_HOST}components/com_jobsfactory/js/user_resume.js"></script>
{include file='js/t_javascript_language.tpl'}
{set_css}

<form id="edit_experience" name="edit_experience" enctype="multipart/form-data" action="{$ROOT_HOST}index.php" method="POST" >
	<input type="hidden" name="id" value="{$user_experience->id}" />
    <input type="hidden" name="resume_id" value="{$user->id}" />
	<input type="hidden" name="task" value="saveExperience" />
	<input type="hidden" name="option" value="{$option}" />

    <div class="job_edit_section">
        <h3 class="page-header item-title" style="border: none !important;">
            {"COM_JOBS_RESUME_EXPERIENCE"|translate}
        </h3>
    </div>
    <span class="v_spacer_15"></span>
    <div class="form-horizontal jobs_top_align" style="border:solid 1px #DDDDDD; padding: 4px 0;">
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_POSITION"|translate}</label>
            <div class="controls">
                <input class="inputbox" name="position" type="text" size="60" value="{$user_experience->position}">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_DOMAIN"|translate}</label>
            <div class="controls">
                {$lists.domain}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_COMPANY"|translate}</label>
            <div class="controls">
                <input class="inputbox" name="company" type="text" size="60" value="{$user_experience->company}">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_START_DATE"|translate}</label>
            <div class="controls">
                {$lists.exp_start_date_html}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_END_DATE"|translate}</label>
            <div class="controls">
                {$lists.exp_end_date_html}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_CITY"|translate}</label>
            <div class="controls">
                <input class="inputbox" name="city" type="text" size="60" value="{$user_experience->city}">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables">{"COM_JOBS_EXP_RESPONSIBILITIES"|translate}</label>
            <div class="controls">
                <textarea name="exp_description{$user->id}" id="exp_description{$user->id}" rows="3" cols="4"
                				style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;">{$user_experience->exp_description}</textarea>
            </div>
        </div>
        <div class="control-group">
           <div class="custom_fields_box">
               {$custom_fields_html}
           </div>
        </div>

        <div class="btn-toolbar form-horizontal job_edit_section">
           <div class="btn-group controls">
               <input type="submit" name="saveexperience" value="{'COM_JOBS_SAVE_EXPERIENCE'|translate}" class="btn btn-primary validate" />&nbsp;&nbsp;
           </div>
           <div class="btn-group controls">
               <a href="{$lists.cancel_form}" class="back_button_cancel"><input name="cancel" value="{jtext text='COM_JOBS_CANCEL'}" class="btn" type="button" /></a>
           </div>
        </div>
    </div>

</form>

