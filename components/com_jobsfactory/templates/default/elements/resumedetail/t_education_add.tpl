<form id="add_education" name="add_education" enctype="multipart/form-data" action="{$ROOT_HOST}index.php" method="POST" >
	<input type="hidden" name="id" value="{$user_education->id}" />
    <input type="hidden" name="resume_id" value="{$user->id}" />
	<input type="hidden" name="task" value="saveEducation" />
	<input type="hidden" name="option" value="{$option}" />

	<div class="form-horizontal jobs_top_align" style="border:solid 1px #DDDDDD; border-top:none; ">    
        <div class="control-group">
             <div class="job_edit_section">
                {"COM_JOBS_USER_EDUCATION"|translate}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_STUDY_LEVEL"|translate}</label>
            <div class="controls">
                {$lists.studies_level}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_EDU_INSTITUTION"|translate}</label>
            <div class="controls">
                <input class="inputbox input-xlarge" name="edu_institution" type="text" size="60" value="{$user_education->edu_institution}">
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_COUNTRY"|translate}</label>
            <div class="controls">
                {$lists.country}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_CITY"|translate}</label>
            <div class="controls">
                <input class="inputbox input-xlarge" name="city" type="text" size="60" value="{$user_education->city}">
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_START_DATE"|translate}</label>
            <div class="controls">
                {$lists.new_edu_start_date_html}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_END_DATE"|translate}</label>
            <div class="controls">
                {$lists.new_edu_end_date_html}
            </div>
        </div>
        <div class="control-group job_edit_section">
            <div class="pull-right">
                <input type="submit" name="saveeducation" value="{'COM_JOBS_SAVE_EDUCATION'|translate}" class="btn btn-primary validate" />
                <a href="#" class="cancel_education btn" rel="{$user->id}" >&nbsp;{"COM_JOBS_CANCEL"|translate}</a>
            </div>
        </div>
    </div>

</form>

