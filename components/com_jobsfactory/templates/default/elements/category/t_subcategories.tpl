{foreach from=$subcategories item=subcategory}
	<div class="job_treecatsub_head span12">
		<div class="cat_link">
            <span class="pull-left"><a href="{$subcategory->link}">{$subcategory->catname}</a></span>

            <span class="job_treecat_icons pull-left">
                <a href="{$subcategory->link}">
                    <img src="{$IMAGE_ROOT}folder-horizontal-open.png" border="0" alt="" title="{'COM_JOBS_FILTER_CATEGORY'|translate}" /></a>
                <a href="{$subcategory->view}">
                    <img src="{$IMAGE_ROOT}document-text-image.png" border="0" alt="" title="{'COM_JOBS_FILTER_CATEGORY'|translate}" /></a>
                {if $is_logged_in}
                    <a href="{$subcategory->link_watchlist}">
                    {if $subcategory->watchListed_flag}
                        <img src="{$IMAGE_ROOT}f_watchlist_0.png" border="0" alt="" title="{'COM_JOBS_REMOVE_FROM_FAVORITESLIST'|translate}"/>
                    {else}
                        <img src="{$IMAGE_ROOT}f_watchlist_1.png" border="0" alt=""  title="{'COM_JOBS_ADD_TO_WATCHLIST'|translate}"/>
                    {/if}
                    </a>
                    {if $isCompany}
                        <a href = "{$subcategory->link_new_listing}"><img src = "{$IMAGE_ROOT}new_listing.png" border = "0" alt = "{"COM_JOBS_NEW_LISTING_IN_CATEGORY"|translate}"
                            title = "{"COM_JOBS_NEW_LISTING_IN_CATEGORY"|translate}" class="new_listing" /></a>
                    {/if}
                {/if}
            </span>

            <span style = "font-weight: normal;font-size: 10px;" class="pull-left1">({$subcategory->nr_a}
                {if $subcategory->nr_a > 1 || !$subcategory->nr_a}
                    {"COM_JOBS_NR_JOBS"|translate}
                    {else}
                    {"COM_JOBS_NR_JOB"|translate}
                {/if}
                    |
                {$subcategory->subcategories|@count}
                {if $subcategory->subcategories|@count > 1 || !$subcategory->subcategories|@count}
                    {"COM_JOBS_NR_SUBCATEGORIES"|translate})
                    {else}
                    {"COM_JOBS_NR_SUBCATEGORY"|translate})
                {/if}
            </span>
		</div>
		<div class="cat_legend">
		
		</div>
	</div>
	<div id="job_cats_{$subcategory->id}" class="job_treecatsub">
		{if $subcategory->subcategories|@count>0}
			{include file="elements/category/t_subcategories.tpl" subcategories=$subcategory->subcategories}
		{/if}
	</div>
{/foreach}
