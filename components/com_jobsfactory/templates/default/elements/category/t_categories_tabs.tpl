{literal}
<script type="text/javascript">
    jQuery(document).ready( function ($) {
        $(document).on("click", "#job_tabmenu li a", function(event){
           location.href = this.rel;
        });
    });
</script>
{/literal}

<!-- Begin Content -->
<div>
    <ul class="nav nav-tabs" id="job_tabmenu">

        <li class = "{if ('categories' == $task)}active{else}inactive{/if}">
            <a data-toggle="tab" href="#tab1" rel = "{$ROOT_HOST}index.php?option=com_jobsfactory&task=categories&Itemid={$Itemid}">
        {"COM_JOBS_VIEW_CATEGORIES_CLASICAL"|translate}</a>
        </li>
        <li class = "{if ('tree' == $task)}active{else}inactive{/if}">
            <a data-toggle="tab" href="#tab2" rel = "{$ROOT_HOST}index.php?option=com_jobsfactory&task=tree&Itemid={$Itemid}">
        {"COM_JOBS_VIEW_CATEGORIES_TREE"|translate}</a>
        </li>
        <li class = "{if ('listlocations' == $task)}active{else}inactive{/if}">
            <a  data-toggle="tab" href="#tab3" rel = "{$ROOT_HOST}index.php?option=com_jobsfactory&task=listlocations&Itemid={$Itemid}">
        {"COM_JOBS_VIEW_LOCATIONS"|translate}</a>
        </li>
        <li class = "{if ('googlemaps' == $task)}active{else}inactive{/if}">
            <a data-toggle="tab" href="#tab4" rel = "{$ROOT_HOST}index.php?option=com_jobsfactory&task=googlemaps&controller=maps&Itemid={$Itemid}">
        {"COM_JOBS_GOOGLE_MAPS"|translate}</a>
        </li>
    </ul>
</div>
<div id="config-document" class="tab-content">
    <div id="tab1" class="tab-pane {if ('categories' == $task)}active{else}inactive{/if}">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    <div id="tab2" class="tab-pane {if ('tree' == $task)}active{else}inactive{/if}">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    <div id="tab3" class="tab-pane {if ('listlocations' == $task)}active{else}inactive{/if}">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    <div id="tab4" class="tab-pane {if ('googlemaps' == $task)}active{else}inactive{/if}">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
</div>

<!-- End Content -->
<div style="clear: both;"></div>
