<span class="job_detail_socialtoolbar pull-right">
    <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">{$job->get('links.tweeter')}</a>
    {literal}
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    {/literal}
    {*<a href="http://twitter.com/home?status=Currently reading {$ad->get('links.tweeter')}" title="Click to send this page to Twitter!" target="_blank"><img src="{$ROOT_HOST}components/com_adsfactory/images/tweet.png" alt="Tweet This!" class="ads_noborder detail_img_social" /></a>*}
</span>

<span class="job_detail_socialtoolbar pull-right">
    <a href="http://delicious.com/save" title="{jtext text='COM_JOBS_DELICIOUS'}" onclick="window.open('http://delicious.com/save?v=5&amp;noui&amp;jump=close&amp;url='+encodeURIComponent(location.href)+'&amp;title='+encodeURIComponent(document.title), 'delicious','toolbar=no,width=550,height=550'); return false;">
        <img src="{$ROOT_HOST}components/com_jobsfactory/images/Social_Delicious.png" class="detail_img_social" alt="{jtext text='COM_JOBS_DELICIOUS'}" /></a>
</span>

<div id="fb-root" class="pull-right"></div>
{literal}
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
{/literal}

<div style="float: right;" class="fb-share-button job_detail_socialtoolbar" data-href="{$lists.location_url}" data-type="button"></div>
{*<div class="fb-share-button" data-href="http://developers.facebook.com/docs/plugins/" data-type="icon_link"></div>*}


<div class="job_detail_socialtoolbar pull-right">
{literal}
  <script type="text/javascript" src="http://platform.linkedin.com/in.js"></script>
  <script type="in/share" data-counter="right"></script>
{/literal}
</div>

