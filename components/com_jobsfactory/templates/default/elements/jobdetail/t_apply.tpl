{include file='js/t_javascript_language.tpl'}
{set_css}
{import_js_file url="jobs.js"}
{* $lists.isCompany ||*}
{if $hasApplied > 0}
   <span class="job_app"> {"COM_JOBS_YOU_ALREADY_APPLIED"|translate} </span>
{else}
    {if $job->published}
        {if $lists.isCandidate}
            <form action="{$ROOT_HOST}index.php?option=com_jobsfactory&task=sendjobapplication" method="post" name="jobForm" onsubmit="return jobObject.ApplicationFormValidate(this);">
                <input type="hidden" name="option" value="{$option}"/>
                <input type="hidden" name="task" value="sendjobapplication"/>
                <input type="hidden" name="id" value="{$job->id}"/>
                <input type="hidden" name="Itemid" value="{$Itemid}"/>
                <table class="table-striped table grey_line_top">
                    <tr>
                        <td class="job_app">
                            <div class="center">
                                {* <span class="span1">&nbsp;</span> *}
                                <span class="span12">
                                    <input type="submit" name="send" value="{'COM_JOBS_SEND_APPLICATION'|translate}" class="btn btn-primary btn-block" {*$disable_apps*}/>
                                </span>

                                <span class="span6">
                                    {if $terms_and_conditions}
                                        &nbsp; <input type="checkbox" class="inputbox" name="agreement" value="1" {$disable_apps} />
                                        {'COM_JOBS_AGREE'|translate}
                                        <a href="javascript: void(0);" onclick="window.open('{$links->getTermsAndConditionsRoute()}','messwindow','location=1,status=1,scrollbars=1,width=500,height=500')" id="job_terms">
                                        {'COM_JOBS_TERMS_AND_CONDITIONS'|translate}</a>
                                    {/if}
                                </span>
                            </div>
                        </td>
                    </tr>
                 </table>
            </form>
        {else}
            {'COM_JOBS_YOU_ARE_NOT_A_CANDIDATE'|translate}
        {/if}
    {/if}
{/if}
