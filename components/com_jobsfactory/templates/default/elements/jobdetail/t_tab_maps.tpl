{if $cfg->google_key && $cfg->map_in_job_details &&(($job->googlex && $job->googley)||($user->googleMaps_x && $user->googleMaps_y)) }
    {if ($job->googlex && $job->googley)}
        {include file='js/t_javascript_maps.tpl' googleMapX=`$job->googlex` googleMapY=`$job->googley`}
    {elseif ($user->googleMaps_x && $user->googleMaps_y)}
        {include file='js/t_javascript_maps.tpl' googleMapX=`$user->googleMaps_x` googleMapY=`$user->googleMaps_x`}
    {/if}

    {import_js_block}
     {literal}
        window.addEvent('domready', function () {
            var loaded = false;

            $$('ul#jobdetail-pane.nav.nav-tabs li a').addEvent('click',function(){
                if (loaded == false) {
                    load_gmaps();
                }
                gmap_showmap.delay(500);
                loaded = true;
            });
        });
     {/literal}
    {/import_js_block}

   {starttab  name="jobdetail-pane" id="tab2" text="COM_JOBS_LOCATION_ON_MAP"|translate}
        {*<div id="map_canvas" style="width: {$cfg->googlemap_gx|default:250}px; height: {$cfg->googlemap_gy|default:250}px"></div>*}
    <div id = "map_canvas" class="maps_detail_jobs"></div>
   {endtab}
{/if}