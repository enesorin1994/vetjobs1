<table width="100%" cellpadding="0" cellspacing="0">
	 <tr>
	  <td align="right">
	    <div style="padding:5px;">
          {if !$job->isMyJob() && $job->close_offer == 0}
            {if $is_logged_in}
                {if $job->get('favorite')}
                        <span class = 'add_to_watchlist'><a href = '{$job->get('links.del_from_watchlist')}'>
                            <img src = "{$IMAGE_ROOT}f_watchlist_0.png" title = "{'COM_JOBS_REMOVE_FROM_WATCHLIST'|translate}" alt = "{'COM_JOBS_REMOVE_FROM_WATCHLIST'|translate}" /></a>
                        </span>
                {else}
                        <span class = 'add_to_watchlist'>
                            <a href = '{$job->get('links.add_to_watchlist')}'>
                                <img src = "{$IMAGE_ROOT}f_watchlist_1.png" title = "{'COM_JOBS_ADD_TO_WATCHLIST'|translate}"
                                     alt = "{'COM_JOBS_ADD_TO_WATCHLIST'|translate}" />
                            </a>
                        </span>
                {/if}
            {/if}
          {/if}
          {if $job->isMyJob()}
    	    {if $job->close_offer == 1}
			    <input type="button" onclick="window.location = '{$job->get('links.republish')}';" class="btn btn-primary" value="{'COM_JOBS_REPUBLISH'|translate}" alt="{'COM_JOBS_REPUBLISH'|translate}" />
		    {else}
                <input type="button" value="{'COM_JOBS_EDIT'|translate}" onclick="window.location = '{$job->get('links.edit')}';" class="btn btn-primary" alt="{'COM_JOBS_EDIT'|translate}" />
                <input type="button" value="{'COM_JOBS_CLOSE_AND_ARCHIVE'|translate}" onclick="document.getElementById('close_job_div').style.display='';" class="btn" alt="{'COM_JOBS_CLOSE_AND_ARCHIVE'|translate}" />
            {/if}
            <!-- [+] Cancel Form Hidden -->
            <div id="close_job_div" style="display:none;">
                <form name="cancel_job_form" id="cancel_job_form" method="POST" action="{$job->get('links.cancel')}">
                    <input type="hidden" name="id" value="{$job->id}"/>
                    <input type="hidden" name="option" value="{$option}"/>
                    <input type="hidden" name="task" value="canceljob"/>
                    <input type="hidden" name="Itemid" value="{$Itemid}"/>
                    <table>
                        <tr>
                            <td align="right" valign="top">{"COM_JOBS_CANCEL_REASON"|translate}:</td>
                            <td>
                                <textarea id="cancel_reason" name="cancel_reason" rows="5"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><input type="button" value="{'COM_JOBS_CANCEL_JOB'|translate}"  onclick="if(confirm('{"COM_JOBS_ARE_YOU_SURE_YOU_WANT_TO_CANCEL"|translate}')) document.cancel_job_form.submit();" class="btn btn-primary" />
                            <input type="button" value="{'COM_JOBS_CLOSE'|translate}"  onclick="document.getElementById('close_job_div').style.display='none';" class="btn" /></td>
                        </tr>
                    </table>
                </form>
            </div>
            <!-- [-] Cancel Form Hidden -->
        {* IF not my job*}
		{else}
            {if $job->close_offer == 0}
                <a href = '{$job->get('links.report')}'>
                            <img src = "{$IMAGE_ROOT}report_job.png"
                                 title = "{'COM_JOBS_REPORT_JOB_AS_ABUSIVE'|translate}"
                                 alt = "{'COM_JOBS_REPORT_JOB_AS_ABUSIVE'|translate}"
                                    />
                        </a>
            {/if}
		{/if}
		</div>
	  </td>
	 </tr>
   {if $job->isMyJob()}
     {if $cfg->admin_approval && !$job->approved}
     <tr>
        <td>
            <div class="jobsfactory_pending_notify">{"COM_JOBS_JOB_IS_PENDING_ADMINISTRATOR_APPROVAL"|translate}</div>
        </td>
     </tr>
     {/if}
   {/if}
</table>

<!-- [-] Buttons Bar -->
