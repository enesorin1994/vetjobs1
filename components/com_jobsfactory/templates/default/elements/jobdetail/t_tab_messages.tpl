{starttab  name="jobdetail-pane" id="tab3" text="COM_JOBS_TAB_SYSTEM_MESSAGES"|translate}
<div class="media system_message">

	<div class="media-body">
		{foreach from=$job->messages key=key item=item}
		<div class="media">
			{$item->modified|date_format}: &nbsp;
			{$item->message}
			<br>
		</div>
		{/foreach}
	</div>
</div>
{endtab}