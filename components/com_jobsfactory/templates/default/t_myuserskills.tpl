{* Include Validation script *}
{include file='js/t_javascript_language.tpl'}
{set_css}

{literal}
<script>
  jQuery(document).ready( function ($)
  {

      $('.edit_skills').click( function (event)
     {
         event.preventDefault();
         var id = $(this).attr("rel");

       //$('#experience_form' + id).show("normal");
       $.post(root + "index.php?option=com_jobsfactory&task=editexperience&format=raw", {
           id:    id,
           resume_id:     resumeid },
           function(data){
           //$('#photobattle-report-loading').hide();
           $('#row_edit_skills' + id).html(data);
       });

       return false;
     });
  });
</script>
{/literal}

<h1>{$user->username} </h1>


<div class="jobs_userprofile">
<form action="{$ROOT_HOST}index.php" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" >
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
    <input type="hidden" name="resume_id" value="{$user->id}" />
	<input type="hidden" name="task" value="saveSkills" />

    <div class="user_edit_header">
        <table width="100%" cellpadding="0" cellspacing="0">
        	<tr >
        		<td align="left">
        			{"COM_JOBS_SKILLS_ACHIEVEMENTS"|translate}
        		</td>
        		<td align="right">
                    <input name="save" value="{'COM_JOBS_SAVE'|translate}" class="btn btn-primary validate" type="submit" />
        		</td>
        	</tr>

        </table>
    </div>

    <table width="100%" cellpadding="0" cellspacing="0" style="border:solid 1px #DDDDDD; border-top:none; " >
        <tr>
            <td align="right" class="resume_edit_section" colspan="2" >
                {"COM_JOBS_RESUME_EXPERIENCE"|translate}
            </td>
        </tr>
    </table>

    <div id="row_edit_skills{$user->id}" >
      <table width="100%" >
        <tr>
            <td class="job_dbk" align="right" width="150">
                <label class="job_lables">{"COM_JOBS_EXP_ACHIEVEMENTS"|translate}</label>
            </td>
            <td class="job_dbk_c">
                <textarea name="user_achievements" id="user_achievements" rows="3" cols="4"
        				style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" >{$user->user_achievements}</textarea>
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right" width="150">
                <label class="job_lables">{"COM_JOBS_GOALS"|translate}</label>
            </td>
            <td class="job_dbk_c">
                <textarea name="user_goals" id="user_goals" rows="3" cols="4"
                        style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" >{$user->user_goals}</textarea>
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right" width="150">
                <label class="job_lables">{"COM_JOBS_HOBBIES"|translate}</label>
            </td>
            <td class="job_dbk_c">
                <textarea name="user_hobbies" id="user_hobbies" rows="3" cols="4"
                        style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" >{$user->user_hobbies}</textarea>
            </td>
        </tr>
    <tr><td colspan="2" style="background-color: #DDD;">&nbsp;</td></tr>

    </table>
    </div>
  </table>


	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td align="right" class="user_edit_section"  colspan="2" >
			     <input name="save" value="{'COM_JOBS_SAVE'|translate}" class="btn btn-primary validate" type="submit" />
			</td>
		</tr>
	</table>
	
</form>
</div>
