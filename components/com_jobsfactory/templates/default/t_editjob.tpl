{* Include Validation script *}
{include file='js/t_javascript_language.tpl'}
{set_css}
{import_js_file url="jobs.js"}

{import_js_file url="Stickman.MultiUpload.js"}
{import_js_file url="date.js"}
{import_js_file url="job_edit.js"}

{assign var="fieldRequired" value=$lists.fieldRequiredlink}

<div>
{if $payment_items_header}
    <div class="jobs_headerinfo">{$payment_items_header}</div>
{/if}
</div>

<div>
    <form action="{$ROOT_HOST}index.php?option={$option}&task=save&id={$job->id}" method="post" name="rJobForm" id="rJobForm"  onsubmit="return jobObject.ApplicationFormValidate(this);" enctype="multipart/form-data" >
    <input type="hidden" name="Itemid" value="{$Itemid}" />
    <input type="hidden" name="option" value="{$option}" />
    <input type="hidden" name="id" value="{$job->id}" />
    <input type="hidden" name="task" value="save" />
    <input type="hidden" name="oldid" value="{$oldid}" />
    <input type="hidden" name="has_custom_fields_with_cat" value="{$custom_fields_with_cat}" />
    {$form_token}

    {if ($job->id && $job->featured != 'none') || $listing_enabled}
        <div class="list_ratings_header"><span style="padding-left: 30px;">{$edit_24hours_info}</span><br /></div>
    {/if}

    <div>
        <div class="pull-left">
            <h3 class="page-header item-title" style="border: none !important;">
            {"COM_JOBS_JOB_OPENING"|translate}: <small><i>{if $job->id}{"COM_JOBS_EDIT"|translate}{else}{"COM_JOBS_NEW"|translate}{/if}</i></small>
            {if $job->title}<small>[{$job->title}]</small>{/if}
            </h3>
        </div>

        <div class="btn-toolbar form-horizontal right">
            <div class="btn-group controls">
                <input type="submit" name="save" value="{'COM_JOBS_SAVE'|translate}" class="validate btn btn-primary" />&nbsp;&nbsp;
            </div>
            <div class="btn-group controls">
                <input type = "submit" name = "cancel" id="cancel_edit" value = "{'COM_JOBS_CANCEL'|translate}" class = "btn" />
            </div>
        </div>
    </div>
    <span class="v_spacer_15"></span>

    <div class="form-horizontal jobs_top_align" style="border:solid 1px #DDDDDD; border-top:none; ">

        <div class="job_dbk_c form_bg0">&nbsp;</div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_POSITION"|translate}{$fieldRequired}</label>
            <div class="controls">
                <div class="job_values">
                    {if $allow_edit==1}
                        <input class="inputbox required input-large" type="text" size="75" name="title" value="{$job->title}" />
                    {else}
                       {if ($task=='editjob' && $job->published)}
                            <span class="job_edit_value">{$job->title}</span>
                            <input type="hidden" name="title" value="{$job->title}">
                       {else}
                            <input class="inputbox required input-large" type="text" size="75" name="title" value="{$job->title}" />
                       {/if}
                    {/if}
                </div>
            </div>
        </div>
        {* Project descriptions *}
        <div class="control-group">
            <label class = "control-label job_lables" for="shortdescription">{"COM_JOBS_SHORT_DESCRIPTION"|translate}</label>
            <div class="controls">
                <input class="inputbox" name="shortdescription" type="text" size="60" value="{$job->shortdescription}">
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_CATEGORY"|translate}</label>
            <div class="controls">
                <div class="job_values">
                {if $task=='editjob' && $job->published}
                    {$lists.catname}
                    <input type="hidden" name="cat" value="{$job->cat}">
                {else}
                    {$lists.cats}
                {/if}
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_JOB_TYPE"|translate}</label>
            <div class="controls">
                {$lists.jobtypes}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_NO_JOBS_AVAILABLE"|translate}</label>
            <div class="controls">
                <div class="job_values">
                {if ($task=='editjob' && $job->published)}
                    {$job->jobs_available}
                    <input type="hidden" name="jobs_available" value="{$job->jobs_available}">
                {else}
                    <input class="inputbox validate-numeric required" type="text" size="7" name="jobs_available" value="{if !$job->jobs_available}1{else}{$job->jobs_available}{/if}" alt="jobs_available">{/if}
                </div>
            </div>
        </div>

        {if $cfg->enable_location_management && $lists.country != '' && $lists.use_location != ''}
            <div class="control-group">
                <label class = "control-label job_lables">
                    {if $cfg->enable_location_management}
                        {"COM_JOBS_JOB_USE_COUNTRY_LOCATION"|translate}
                    {else}
                        {"COM_JOBS_JOB_USE_COUNTRY"|translate}
                    {/if}
                </label>
                <div class="controls1">
                    {$lists.use_location}
                </div>
            </div>

            <div class="control-group">
                <div id="countrylist">
                    <label class = "control-label job_lables">{"COM_JOBS_JOB_COUNTRY_LOCATION"|translate}<span class="jobs_required">&nbsp;(*)</span></label>
                    <div class="controls">
                        {$lists.country}
                    </div>
                </div>
            </div>
            <div>
                <div class="job_dbk_c" style="text-align: center;">
                    <span id='countryError' style='color:red'></span>
                </div>
            </div>
        {/if}

        {if $cfg->enable_location_management && $lists.states != '' && $lists.use_location != ''}
            <div class="control-group">
                <div id="statelist">
                    <label class = "control-label job_lables">{"COM_JOBS_JOB_STATE_LOCATION"|translate}</label>
                    <div class="controls">
                        {if $job->id}
                            <div id="states" name="states">
                                {$lists.states}
                            </div>
                        {else}
                            <div id="states" name="states">
                                {$lists.states}
                            </div>
                        {/if}
                    </div>
                </div>
            </div>
        {/if}

        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_JOB_CITY_LOCATION"|translate}{if $cfg->enable_location_management == '0' || $lists.cities == ''}{$fieldRequired}{/if}</label>
            <div class="controls">
                {if $cfg->enable_location_management && $lists.cities != ''}
                    <div id="cities" name="cities">
                        {$lists.cities}
                    </div>
                {else}
                    <input class="inputbox required" type="text" size="60" name="job_cityname" value="{$job->job_cityname}" alt="job_cityname">
                {/if}
            </div>
        </div>
        <div class="job_dbk_c form_bg0">&nbsp;</div>

        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_EMPLOYER_VISIBILITY"|translate}</label>
            <div class="controls">
                <div class="job_values">
                    {if $cfg->employervisibility_enable || $job->id}
                        {if $job->id && $allow_edit==0}
                            <strong>{if $job->employer_visibility == $EMPLOYER_VISIBILITIES.EMPLOYER_TYPE_CONFIDENTIAL}{"COM_JOBS_CONFIDENTIAL"|translate}{else}{"COM_JOBS_PUBLIC"|translate}{/if}</strong>
                        {else}
                            {$lists.employervisibility}
                        {/if}
                        &nbsp;{infobullet text="employer_visibility_help"|translate}
                    {else}
                        <strong>{if $cfg->employervisibility_val == $EMPLOYER_VISIBILITIES.EMPLOYER_TYPE_CONFIDENTIAL}{"COM_JOBS_CONFIDENTIAL"|translate}{else}{"COM_JOBS_PUBLIC"|translate}{/if}</strong>
                    {/if}
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_ABOUT_US_VISIBILITY"|translate}</label>
            <div class="controls">
                <div class="job_values">
               {if $job->id && $allow_edit==0}
                    <strong>{if $job->about_visibility == 0}{"COM_JOBS_CONFIDENTIAL"|translate}{else}{"COM_JOBS_PUBLIC"|translate}{/if}</strong>
               {else}
                    {$lists.aboutusvisibility}
               {/if}

               <span class="float:left;"> &nbsp;{infobullet text="about_visibility_help"|translate}</span>
               </div>
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_SHOW_CANDIDATES_NUMBER"|translate}</label>
            <div class="controls">
                <div class="job_values">
                    <input type="radio" name="show_candidate_nr" value="1" {if $job->show_candidate_nr!=='0'}checked{/if}>{"COM_JOBS_YES"|translate}
                    <input type="radio" name="show_candidate_nr" value="0" {if $job->show_candidate_nr==='0'}checked{/if}>{"COM_JOBS_NO"|translate}
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_DATE_POSTED"|translate}</label>
            <div class="controls">
                <div class="job_values">
                {if $task=='editjob' && $allow_edit==0}
                    {printdate date=$job->start_date}
                    <input class="text_area" type="hidden" name="start_date" id="start_date" value="{$job->start_date}"/>
                {else}
                    {$lists.start_date_html}
                {/if}
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_VALID_UNTIL"|translate}</label>
            <div class="controls">
                <div class="job_values">
                {if $task=='editjob' && $allow_edit==0}
                        {printdate date=$job->end_date use_hour=1}
                        <input class="text_area" type="hidden" name="end_date" id="end_date" value="{$job->end_date}" />
                {else}
                    {$lists.end_date_html}&nbsp;{$lists.tip_max_availability}
                    {if $cfg->enable_hour}
                        <input name="end_hour" size="1" value="{$lists.end_hour}" alt="" class="inputbox new_listing" /> :
                        <input name="end_minutes" size="1" value="{$lists.end_minute}" alt="" class="inputbox new_listing" />
                    {/if}
                {/if}
                </div>
            </div>
        </div>
        <div class="job_dbk_c form_bg0">&nbsp;</div>

        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_EXPERIENCE_REQUEST"|translate}</label>
            <div class="controls">
                {$lists.experience_request}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_STUDIES_REQUEST"|translate}</label>
            <div class="controls">
                {if $task=='editjob' || $task=='form'}
                    {$lists.studies_request}
                {else}
                    {$job->studies_request}
                {/if}
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_LANGUAGES_REQUEST"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" name="languages_request" value="{$job->languages_request}" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_PUBLISHED"|translate}</label>
            {if $cfg->jobpublish_enable}
                {if $task=='editjob' && $job->published}
                    <div class="controls">
                        <div class="job_values">
                            {"COM_JOBS_YES"|translate}
                        </div>
                    </div>
                {else}
                        {$lists.published}
                {/if}
            {else}
                <div class="job_values">
                    {if ($job->id && $job->published)||(!$job->id && $cfg->jobpublish_val)}{"COM_JOBS_YES"|translate}{else}{"COM_JOBS_NO"|translate}{/if}
                </div>
            {/if}
        </div>
        <div class="job_dbk_c form_bg0">&nbsp;</div>
    </div>


    <a title="job_description" name="job_description" id="job_description"></a>
    <div class="form-horizontal jobs_top_align" style="border:solid 1px #DDDDDD; border-top:none; ">

        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_DESCRIPTION"|translate}<span class="jobs_required">&nbsp;(*)</span></label>
            <div class="controls">
                <div class="job_values">
                    {$lists.description}
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_BENEFITS"|translate}</label>
            <div class="controls">
                <div class="job_values">
                    {$lists.benefits}
                </div>
            </div>
        </div>

        {if $cfg->google_key!=""}
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_GOOGLE_MAP"|translate}</label>
            <div class="controls">
                <div class="job_values">
                    {if $user->googleMaps_x !="" && $user->googleMaps_y !="" }
                    <a href="#" onclick="document.getElementById('googleX').value='{$user->googleMaps_x}';document.getElementById('googleY').value='{$user->googleMaps_y}'; return false;">{"COM_JOBS_KEEP_PROFILE_COORDINATES"|translate}</a> |
                    {/if}
                    <a href="#" onclick="window.open('index.php?option=com_jobsfactory&controller=maps&tmpl=component&task=googlemap_tool','SelectGoogleMap','width=650,height=500');return false;">{"COM_JOBS_SELECT_COORDINATES"|translate}</a>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_COORDINATE_X"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" id="googleX" name="googlex" value="{$job->googlex}" size="20" />
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_COORDINATE_Y"|translate}</label>
            <div class="controls">
                <input class="inputbox" type="text" id="googleY" name="googley" value="{$job->googley}" size="20"/>
            </div>
        </div>
        {/if}

        <div>
            {$custom_fields_html}
        </div>
    </div>

    <div class="form-horizontal jobs_top_align" style="border:solid 1px #DDDDDD; border-top:none; ">

        {if $cfg->enable_attach}
            <div class="job_dbk_c form_bg0">&nbsp;</div>
            {* attachments *}
            <div class="control-group">
                {if $job->file_name}
                    <label class = "control-label job_lables">{"COM_JOBS_ATTACHMENT"|translate}</label>&nbsp;<br />
                    <div class="controls">
                        <!--$job->get('links.download_file')//$job->get('links.deletefile_file')-->
                        <strong><a href="{$links->getDownloadFileRoute($job->id ,'attach')}" target="_blank" >{$job->file_name}</a></strong>&nbsp;&nbsp;&nbsp;
                        <a href="{$links->getDeleteFileRoute($job->id ,'attach')}">{"COM_JOBS_DELETE"|translate}</a>
                    </div>
                {else}
                    <label class = "control-label job_lables">{"COM_JOBS_ATTACHMENT"|translate}</label>
                    <div class="controls">
                        <input name="attachment" class="inputbox{if $cfg->attach_compulsory} required{/if}" size="50"  type="file" /><br />
                        <small style="color: Grey;">{"COM_JOBS_MAXIMUM_FILE_SIZE"|translate|replace:"%s":$cfg->attach_max_size}</small>
                        {if $cfg->attach_extensions}
                            <br /><small style="color: Grey;">{"COM_JOBS_ALLOWED_ATTACHMENT_EXTENSIONS"|translate} {$cfg->attach_extensions}</small>
                        {/if}
                    </div>
                {/if}
            </div>
        {/if}

        <div class="job_dbk_c form_bg0">&nbsp;</div>

        <div class="control-group">
            <label class = "control-label job_lables">{"COM_JOBS_TAGS"|translate}</label>
            <div class="controls">
                <input name="tags" class="inputbox" value="{if $job->id}{$job->get('tags')}{/if}" size="50" type="text"/> {infobullet text="Insert tags comma separated"|translate}
            </div>
        </div>
        <span class="v_spacer_5"></span>

        <div class="control-group">
            {if $terms_and_conditions}
                <label class = "control-label job_lables">&nbsp;</label>
                <div class="controls">
                    <small>
                        <input type="checkbox" class="inputbox" name="agreement" value="1" style="margin-bottom: 5px;" />
                    {'COM_JOBS_AGREE'|translate}
                        <a href="javascript: void(0);" onclick="window.open('{$links->getTermsAndConditionsRoute()}','messwindow','location=1,status=1,scrollbars=1,width=500,height=500')"
                           id="job_terms">
                    {'COM_JOBS_TERMS_AND_CONDITIONS'|translate}</a>
                    </small>
                </div>
            {/if}
        </div>
    </div>

    <br />
    <div class="btn-toolbar form-horizontal job_edit_section">
        <div class="btn-group controls">
            <input type="submit" name="save" value="{'COM_JOBS_SAVE'|translate}" class="validate btn btn-primary"  />&nbsp;&nbsp;
        </div>
        <div class="btn-group controls">
            <input type = "submit" name = "cancel" id="cancel_edit1" value = "{'COM_JOBS_CANCEL'|translate}" class = "btn"  />
        </div>
    </div>

    </form>
</div>
<!-- next row -->
<div style="clear:both;" ></div>

{php}
    {echo JHtml::_('behavior.keepalive');}
{/php}