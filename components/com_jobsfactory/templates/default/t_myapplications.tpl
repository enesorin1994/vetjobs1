{set_css}
{include file='js/t_javascript_language.tpl'}
{include file='js/t_javascript_countdown.tpl'}
{import_js_file url="jobs.js"}

{if $isCandidate}
<h2>{"COM_JOBS_MY_APPLICATIONS"|translate} ({$user->name}&nbsp;{$user->surname})</h2>
{else}
<h2>{"COM_JOBS_APPLICATIONS_TO_MY_JOB_OPENINGS"|translate}</h2>
{/if}


<form action="{$ROOT_HOST}index.php" method="get" name="jobsForm">
<input type="hidden" name="option" value="{$option}">
<input type="hidden" name="task" value="{$task}">
<input type="hidden" name="Itemid" value="{$Itemid}">

{* Include filter selectboxes *}

<table align="center" cellpadding="0" cellspacing="0" width="100%" id="job_list_container">
    <tr style="height: 30px;">
    	<th style="width: auto; text-align: left;">
            {'COM_JOBS_POSITION'|translate} / {'COM_JOBS_CATEGORY'|translate}
       </th>
    	<th style="width: 190px; text-align: left;">{'COM_JOBS_APPLIED_DATE'|translate}
        </th>
    </tr>

{section name=jobsloop loop=$job_rows}
		{include file='elements/lists/t_listapplications_cell.tpl' application=`$job_rows[jobsloop]` index=`$smarty.section.jobsloop.rownum`}
	{/section}
</table>
    {include file='elements/t_listfooter.tpl'}

</form>
<div style="clear: both;"></div>