{set_css}
{include file='js/t_javascript_language.tpl'}
{include file='js/t_javascript_countdown.tpl'}
{import_js_file url="jobs.js"}
<h2>{"COM_JOBS_MY_JOBS"|translate}</h2>

<div style="padding:5px"></div>
<div align="right" style="text-align:right;">

{literal}
    <script type="text/javascript">
        jQuery(document).ready( function ($) {
            $(document).on("click", "#job_tabmenu li a", function(event){
               location.href = this.rel;
            });
        });
    </script>
{/literal}

<ul class="nav nav-tabs" id="job_tabmenu">
	{include file='elements/filter_button.tpl' filter='active'     label="COM_JOBS_VIEW_PUBLISHED"|translate}
	{include file='elements/filter_button.tpl' filter='unpublished' label="COM_JOBS_VIEW_DRAFT"|translate}
    {if $cfg->admin_approval}
    {include file='elements/filter_button.tpl' filter='unapproved'  label="COM_JOBS_VIEW_UNAPPROVED"|translate}
    {/if}
	{include file='elements/filter_button.tpl' filter='archive'     label="COM_JOBS_VIEW_ARCHIVED"|translate}
 </ul>
</div>


<form action="{$ROOT_HOST}index.php" method="get" name="jobsForm" id="jobsForm">
<input type="hidden" name="option" value="{$option}">
<input type="hidden" name="task" value="{$task}">
<input type="hidden" name="Itemid" value="{$Itemid}">
<input type="hidden" name="filter_myjobs" value="{$filter_myjobs}">

{* Include filter selectboxes *}
{include file='elements/t_header_filter.tpl'}

<table align="center" cellpadding="0" cellspacing="0" width="100%" id="job_list_container" class="job_list_container">
    <tr style="height: 30px;">
    	<th style="width: auto; text-align: left;">
            {include file='elements/sort_field.tpl' label="COM_JOBS_TITLE"|translate key="title"} /
            {include file='elements/sort_field.tpl' label="COM_JOBS_CATEGORY"|translate key="catname"} /
            {include file='elements/sort_field.tpl' label="COM_JOBS_USER"|translate key="username"}
        </th>
    	<th style="width: 190px; text-align: left;">
            {include file='elements/sort_field.tpl' label="COM_JOBS_DETAILS_ENDING_IN"|translate key="end_date"}
        </th>
    	<th style="width: 120px; text-align: right;">
        	{include file='elements/sort_field.tpl' label="COM_JOBS_NR_APPLICATIONS"|translate key="nr_applicants"}
    	</th>
    </tr>
        <tr>
           <td class="v_spacer_15" colspan="4">&nbsp;</td>
        </tr>

        {if $job_rows|count}
            {section name=jobsloop loop=$job_rows}
                {include file='elements/lists/t_listjobs_cell.tpl' job=`$job_rows[jobsloop]` index=`$smarty.section.jobsloop.rownum`}
            {/section}
        {else}
        <tr>
            <td class="job_dbk" valign="top" colspan="3">
                {include file='t_nojobs.tpl'}
            </td>
        </tr>
    {/if}
</table>
{include file='elements/t_listfooter.tpl'}

</form>
<div class="v_spacer_15">&nbsp;</div>
<div style="clear: both;"></div>