{set_css}
{include file='js/t_javascript_language.tpl'}

<h1>{"COM_JOBS_PAYMENT_BALANCE"|translate}</h1>

<div>
  <table id="creditsprofile" width="85%" cellpadding="0" cellspacing="0" style="border:solid 1px #DDDDDD; " >
    <tr>
        <td class="job_dt" colspan="2">
            <div class="job_credits1">
                <h2>{"COM_JOBS_YOUR_CURRENT_BALANCE_IS"|translate}:
                {$lists.balance->balance}&nbsp;{$lists.balance->currency}
                    </h2>
            </div>
            <div>
                <a href="{$lists.links.upload_funds}">
                 {"COM_JOBS_ADD_FUNDS_TO_YOUR_BALANCE"|translate}
                 </a>
            </div>
             <div>
                 <a href="{$lists.links.payment_history}">
                  {"COM_JOBS_SEE_MY_PAYMENTS_HISTORY"|translate}
                  </a>
             </div>
        </td>
    </tr>
  </table>
</div>