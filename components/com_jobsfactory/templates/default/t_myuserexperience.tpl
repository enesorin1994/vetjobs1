{* Include Validation script *}
{include file='js/t_javascript_language.tpl'}
{set_css}

{literal}
<script>
  jQuery(document).ready( function ($)
  {
      $('.edit_experience').click( function (event)
     {
         event.preventDefault();
         var id = $(this).attr("rel");

       //$('#experience_form' + id).show("normal");
       $.post(root + "index.php?option=com_jobsfactory&task=editexperience&format=raw", {
           id:    id,
           resume_id:     resumeid },
           function(data){
           //$('#photobattle-report-loading').hide();
           $('#row_edit_experience' + id).html(data);
       });

       return false;
     });
  });
</script>
{/literal}
    {* experience Tab *}
    {starttab name="content-pane" id="usertab3" text="COM_JOBS_TAB_EXPERIENCE"|translate}

<div class="jobs_userprofile">
<form action="{$ROOT_HOST}index.php" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" >
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
    <input type="hidden" name="resume_id" value="{$user->id}" />
	<input type="hidden" name="task" value="saveExperience" />

    <div class="user_edit_header">
        <table width="100%" cellpadding="0" cellspacing="0">
        	<tr >
        		<td align="left">
        			{"COM_JOBS_EDIT_USER_EXPERIENCE"|translate}
        		</td>
        	</tr>
        </table>
    </div>

    <!--resume_experience-->
    <a title="resume_experience" name="resume_experience" id="resume_experience"></a>

    {if $task=='userdetails'}
        <div class="jobsfactory_icon jobsfactory_experience_add" style="padding: 2px;">
             <a href="#" class="show_experience_form" id="show_experience_form{$user->id}" rel="{$user->id}">
                 {"COM_JOBS_EXPERIENCE_ADD"|translate}</a>
        </div>
    {/if}
    <span style="display: none; padding: 5px;" class="jobsfactory_experience_form" id="experience_form{$user->id}">
    	<div>
    		<table cellspacing="0" cellpadding="0" width="80%">
    		  <tr>
    			<td class="experience_column">
                    {include file="elements/resumedetail/t_experience_add.tpl"}
                </td>
    		  </tr>
    		</table>
    	</div>
    </span>

{if $lists.countexp > 0 && $task=='userdetails'}

  <table width="100%" cellpadding="0" cellspacing="0" style="border:solid 1px #DDDDDD; border-top:none; " >
	<tr>
		<td align="right" class="resume_edit_section" colspan="2" >
            {"COM_JOBS_RESUME_EXPERIENCE"|translate}
		</td>
	</tr>
  </table>

    {foreach from=$array_experience item=user_experience}
    <div id="row_edit_experience{$user_experience->id}" >
    <table class="no_border user_resume table-striped" width="100%" cellpadding="0" cellspacing="0" >
        <tr>
           <td class="job_dbk" align="right">
               <label class="job_lables">{"COM_JOBS_COMPANY"|translate}</label>
           </td>
           <td class="job_dt job_values1">
               {$user_experience->company}
           </td>
           <td>
               <span style="float: right">
                    <a class="edit_experience1" href="{$ROOT_HOST}index.php?option=com_jobsfactory&task=editexperience&id={$user_experience->id}&resume_id={$user->id}">
                    <input name="edit" value='{"COM_JOBS_EDIT"|translate}' class="btn btn-success" type="button" /></a>
                    <a onclick="return confirm('{jtext text='COM_JOBS_CONFIRM_DELETE'}')" href="{$ROOT_HOST}index.php?option=com_jobsfactory&task=delete_experience&id={$user_experience->id}&resume_id={$user->id}" class="delete_experience" rel="{$user_experience->id}" >
                        <input name="del" value='{"COM_JOBS_DELETE"|translate}' class="btn btn-danger" type="button" /></a>
               </span>
           </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables">{"COM_JOBS_POSITION"|translate}</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                {$user_experience->position}
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables">{"COM_JOBS_EXP_RESPONSIBILITIES"|translate}</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                {$user_experience->exp_description}
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables">{"COM_JOBS_DOMAIN"|translate}</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                {assign var=domain value=$user_experience->domain}
                {$user_experienceTable->getCatname($domain)}
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables">{"COM_JOBS_START_DATE"|translate}</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                {if $task=='userdetails'}
                    {printdate date=$user_experience->start_date}
                    <input type="hidden" name="start_date" id="start_date" size="15" maxlength="19" value="{$user_experience->start_date|date_format:'%Y-%m-%d %H:%M:%S'}" />
                {else}
                    {$lists.exp_start_date_html}
                {/if}
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables">{"COM_JOBS_END_DATE"|translate}</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                {if $task=='userdetails'}
                    {printdate date=$user_experience->end_date}
                    <input type="hidden" name="end_date" id="end_date" value="{$user_experience->end_date|date_format:'%Y-%m-%d %H:%M:%S'}" />
                {else}
                    {$lists.exp_end_date_html}
                {/if}
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables">{"COM_JOBS_CITY"|translate}</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                {$user_experience->city}
            </td>
        </tr>
        <tr><td colspan="3" class="job_dbk_c form_bg0">&nbsp;</td></tr>
        </table>
      </div>
    {/foreach}
{/if}

</form>
</div>

{endtab}