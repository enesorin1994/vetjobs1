{set_css}
{include file='js/t_javascript_language.tpl'}
{include file='js/t_javascript_countdown.tpl'}
{import_js_file url="jobs.js"}

<h2>{"COM_JOBS_MY_WATCHLIST"|translate}</h2>
{literal}
    <script type="text/javascript">
        jQuery(document).ready( function ($) {
            $(document).on("click", "#job_tabmenu li a", function(event){
               location.href = this.rel;
            });
        });
    </script>
{/literal}

<div align="right" style="text-align:right;">
<ul class="nav nav-tabs" id="job_tabmenu">
    <li class="{if (!$filter_watchlist)}active{else}inactive{/if}">
       	<a data-toggle="tab" href="#tab1" rel="{$ROOT_HOST}index.php?option=com_jobsfactory&Itemid={$Itemid}&controller=watchlist&task={$task}&filter_watchlist=">
       	    {"COM_JOBS_WATCHLIST_JOBS"|translate}</a>
    </li>
    <li class="{if ($filter_watchlist == 'categories')}active{else}inactive{/if}">
    	<a data-toggle="tab" href="#tab2" rel="{$ROOT_HOST}index.php?option=com_jobsfactory&Itemid={$Itemid}&controller=watchlist&task={$task}&filter_watchlist=categories">
    	    {"COM_JOBS_MY_WATCHLIST_CATEGORIES"|translate}</a>
    </li>
    {if $cfg->enable_location_management}
        <li class="{if ($filter_watchlist == 'cities')}active{else}inactive{/if}">
            <a data-toggle="tab" href="#tab3" rel="{$ROOT_HOST}index.php?option=com_jobsfactory&Itemid={$Itemid}&controller=watchlist&task=watchlist&filter_watchlist=cities">
                {"COM_JOBS_MY_WATCHLIST_CITIES"|translate}</a>
        </li>
        <li class="{if ($filter_watchlist == 'locations')}active{else}inactive{/if}">
            <a data-toggle="tab" href="#tab4" rel="{$ROOT_HOST}index.php?option=com_jobsfactory&Itemid={$Itemid}&controller=watchlist&task=watchlist&filter_watchlist=locations">
                {"COM_JOBS_MY_WATCHLIST_LOCATIONS"|translate}</a>
        </li>
    {/if}
    <li class="{if ($filter_watchlist == 'companies')}active{else}inactive{/if}">
    	<a data-toggle="tab" href="#tab5" rel="{$ROOT_HOST}index.php?option=com_jobsfactory&Itemid={$Itemid}&controller=watchlist&task=watchlist&filter_watchlist=companies">
    	    {"COM_JOBS_MY_WATCHLIST_COMPANIES"|translate}</a>
    </li>
</ul>
</div>

<form action="{$ROOT_HOST}index.php?option=com_jobsfactory" method="post" name="jobsForm" id="jobsForm">
<input type="hidden" name="option" value="{$option}" />
<input type="hidden" name="controller" value="watchlist" />
<input type="hidden" name="task" value="{$task}" />
<input type="hidden" name="Itemid" value="{$Itemid}" />

{* Include filter selectboxes *}
{include file='elements/t_header_filter.tpl'}

<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="job_list_container">
<tr>
    <th style="width: auto; text-align: left;">
        {include file='elements/sort_field.tpl' label="Title"|translate key="title"} /
        {include file='elements/sort_field.tpl' label="Category"|translate key="catname"}
    </th>
	<th style="width: 120px; text-align: left;">{include file='elements/sort_field.tpl' label="Company"|translate key="username"}</th>
    <th style="width: 90px; text-align: left;">
        {if $cfg->enable_location_management}
            {include file='elements/sort_field.tpl' label="City"|translate key="cityname"} /
            {include file='elements/sort_field.tpl' label="Location"|translate key="locname"}
        {else}
            {include file='elements/sort_field.tpl' label="City"|translate key="job_cityname"}
        {/if}
    </th>
	<th style="width: 110px; text-align: right;">{include file='elements/sort_field.tpl' label="Ending in"|translate key="end_date"}</th>
</tr>
<tr>
   <td class="v_spacer_15" colspan="4">&nbsp;</td>
</tr>
	{section name=jobsloop loop=$job_rows}
		{include file='elements/lists/t_watchlistjobs_cell.tpl' job=`$job_rows[jobsloop]` index=`$smarty.section.jobsloop.rownum`}
	{/section}
</table>
{include file='elements/t_listfooter.tpl'}

</form>
<div style="clear: both;"></div>