{* Include Validation script *}
{include file='js/t_javascript_language.tpl'}
{set_css}

{literal}
<script>
  jQuery(document).ready( function ($)
  {

    $('.edit_education').click( function (event)
      {
          event.preventDefault();
          var id = $(this).attr("rel");

          $.post(root + "index.php?option=com_jobsfactory&task=editeducation&format=raw", {
            id:    id,
            resume_id:     resumeid },
            function(data){
            $('#row_edit_education' + id).html(data);
          });

        return false;
      });
  });
</script>
{/literal}
{* education Tab *}
{starttab name="content-pane" id="usertab2" text="COM_JOBS_TAB_EDUCATION"|translate}

<div class="jobs_userprofile">
    <div class="user_edit_header">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr >
                <td align="left">
                    {"COM_JOBS_EDIT_USER_EDUCATION"|translate}
                </td>
            </tr>
        </table>
    </div>
<form action="{$ROOT_HOST}index.php" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" >
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
    <input type="hidden" name="resume_id" value="{$user->id}" />
	<input type="hidden" name="controller" value="user" />
	<input type="hidden" name="task" value="saveEducation" />


    <!--resume_user_education-->
    <a title="resume_education" name="resume_education" id="resume_education"></a>

    {if $task=='userdetails'}
        <div class="jobsfactory_icon jobsfactory_education_add" style="padding: 2px;">
             <a href="#" class="show_education_form" id="show_education_form{$user->id}" rel="{$user->id}">
                 {"COM_JOBS_EDUCATION_ADD"|translate}</a>
        </div>
    {/if}
    <span style="display: none; padding: 5px;" class="jobsfactory_education_form" id="education_form{$user->id}">
    	<div>
    		<table cellspacing="0" cellpadding="0" width="80%">
    		  <tr>
    			<td class="education_column">
                    {include file="elements/resumedetail/t_education_add.tpl"}
                </td>

    		  </tr>
    		</table>
    	</div>
    </span>

    {if $lists.countedu > 0 && ($task=='userdetails')}
        <table width="100%" cellpadding="0" cellspacing="0" class="user_resume" >
            <tr>
                <td align="right" class="resume_edit_section" colspan="2" >
                    {"COM_JOBS_CANDIDATE_EDUCATION"|translate}
                </td>
            </tr>
        </table>

        {foreach from=$array_education item=user_education}
        <div id="row_edit_education{$user_education->id}" >
          <table class="no_border user_resume table-striped" width="100%" cellpadding="0" cellspacing="0" style="border:solid 1px #DDDDDD; border-top:none; " >
            <tr>
                <td class="job_dbk" align="right">
                    <label class="job_lables">{"COM_JOBS_STUDY_LEVEL"|translate}</label>
                </td>
                <td class="job_dt job_values1">
                    {$user_education->levelname}
                </td>
                <td>
                    <span style="float: right">
                        <a class="edit_education1" href="{$ROOT_HOST}index.php?option=com_jobsfactory&task=editeducation&id={$user_education->id}&resume_id={$user->id}&Itemid={$Itemid}"><input name="edit" value='{"COM_JOBS_EDIT"|translate}' class="btn btn-success" type="button" /></a>
                        <a onclick="return confirm('{jtext text='COM_JOBS_CONFIRM_DELETE'}')" href="{$ROOT_HOST}index.php?option=com_jobsfactory&controller=user&task=delete_education&id={$user_education->id}&resume_id={$user->id}&Itemid={$Itemid}" class="delete_education" rel="{$user_education->id}" >
                            <input name="del" value='{"COM_JOBS_DELETE"|translate}' class="btn btn-danger" type="button" /></a>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right">
                    <label class="job_lables">{"COM_JOBS_EDU_INSTITUTION"|translate}</label>
                </td>
                <td class="job_dt job_values" colspan="2">
                    {$user_education->edu_institution}
                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right"
                    <label class="job_lables">{"COM_JOBS_COUNTRY"|translate}</label>
                </td>
                <td class="job_dt job_values1" colspan="2">
                    {$user_education->country}
                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right">
                    <label class="job_lables">{"COM_JOBS_CITY"|translate}</label>
                </td>
                <td class="job_dt job_values1" colspan="2">
                    {$user_education->city}
                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right">
                    <label class="job_lables">{"COM_JOBS_START_DATE"|translate}</label>
                </td>
                <td class="job_dt job_values1" colspan="2">
                    {if $task=='userdetails'}
                        {printdate date=$user_education->start_date}
                        <input class="text_area" type="hidden" name="start_date{$user_education->id}" id="start_date{$user_education->id}" size="15" maxlength="19" value="{$user_education->start_date|date_format:'%Y-%m-%d %H:%M:%S'}" alt="start_date"/>
                    {else}
                        {$lists.edu_start_date_html}
                    {/if}

                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right">
                    <label class="job_lables">{"COM_JOBS_END_DATE"|translate}</label>
                </td>
                <td class="job_dt job_values1" colspan="2">
                    {if $task=='userdetails'}
                        {printdate date=$user_education->end_date}
                        <input class="text_area" type="hidden" name="end_date" id="end_date" size="15" maxlength="19" value="{$user_education->end_date|date_format:'%Y-%m-%d %H:%M:%S'}" alt="end_date"/>
                    {else}
                        {$lists.edu_end_date_html}
                    {/if}
                </td>
            </tr>
            <tr><td colspan="3" class="job_dbk_c form_bg0">&nbsp;</td></tr>
          </table>
        </div>

        {/foreach}
    {/if}

</form>
</div>

{endtab}
