{* Include Validation script *}
{include file='js/t_javascript_language.tpl'}

{set_css}

<h2><strong>{$page_title}</strong></h2>
{include file='t_search_tabs.tpl'}
{starttab name="job_tabmenu" id="tab3"}

<form action="{$ROOT_HOST}index.php" method="post" name="jobForm">
    <input type="hidden" name="task" value="showCompanies"/>
    <input type="hidden" name="controller" value="user"/>
    <input type="hidden" name="option" value="{$option}"/>
    <input type="hidden" name="reset" value="all"/>
    <input type="hidden" name="Itemid" value="{$Itemid}"/>

    <div class="jobs_search" style="max-width: 700px;">

        <div class = "jobs_search_inner">
            <div class = "row-fluid">
                <div class = "span2">
			        <label class="job_lables">{"COM_JOBS_KEYWORD"|translate}</label>
                </div>
                <div class="span10">
        			<input type="text" name="keyword" class="inputbox input-xxlarge" size="30" />
                </div>
		    </div>

            <div class = "row-fluid">
                <div class = "span2">
                    <label class="job_lables">{"COM_JOBS_COUNTRY"|translate}</label>
                </div>
                <div class="span10">
                    {$lists.country}
                </div>
            </div>
            <div class = "row-fluid">
                <div class = "span2">
                    <label class="job_lables">{"COM_JOBS_ACTIVITY_DOMAIN"|translate}</label>
                </div>
                <div class="span10">
                    {$lists.activityDomains}
                </div>
            </div>

            {if $lists.custom_fields != ''}
            <div class = "row-fluid form-horizontal">
                <fieldset class="jobs_search">
                    <legend>{jtext text="COM_JOBS_CUSTOM_FIELDS"}</legend>
                    <div class="span12 custom_fields_box">
                      {$lists.custom_fields}
                    </div>
                </fieldset>
            </div>
            {/if}
            <div class = "row-fluid">
                <div class="span3">
                    <input type="submit" name="search" value="{'COM_JOBS_SEARCH'|translate}" class="btn btn-primary btn-block btn-medium"/>
                </div>
            </div>
        </div>
    </div>
</form>
{endtab}