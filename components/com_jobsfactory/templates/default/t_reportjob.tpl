{set_css}

<form action="{$ROOT_HOST}index.php" name="jobForm" method="POST" class="report_form">
<input type="hidden" name="option" value="{$option}">
<input type="hidden" name="Itemid" value="{$Itemid}">
<input type="hidden" name="id" value="{$job->id}">
<input type="hidden" name="task" value="do_report">
<div class="job_edit_header" >
     <table width="100%" cellpadding="0" cellspacing="0">
         <tr>
            <td>{"COM_JOBS_REPORT_JOB_AS_ABUSIVE"|translate}</td>
         </tr>
     </table>
</div>
<table width="100%" cellpadding="0" cellspacing="0" class="user_detailstable">
  <tr>
   <td><label class="job_lables">{"COM_JOBS_JOB_TITLE"|translate}:</label> <span class="job_title">{$job->title}</span></td>
  </tr>
  <tr>
   <td>
	<textarea name="message" rows="10" cols="50"></textarea>
   </td>
  </tr>
  <tr>
   <td>
	<a href='javascript:history.go(-1);'><input type="button" class="btn btn-primary" value="{'COM_JOBS_BACK'|translate}"></a>
	<input type="submit" name="send" value="{'COM_JOBS_SEND'|translate}" class="btn btn-primary" />
   </td>
  </tr>
</table>
</form>
