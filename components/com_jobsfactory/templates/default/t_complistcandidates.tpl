{set_css}
{include file='js/t_javascript_language.tpl'}
{import_js_file url="jobs.js"}

<h2>
{"COM_JOBS_CANDIDATES_LIST"|translate}
	<a href="index.php?option=com_jobsfactory&task={$task}&format=feed&limitstart=" target="_blank">
		<img src="{$IMAGE_ROOT}f_rss.jpg" width="14" border="0" alt="RSS" />
	</a>
</h2>

<div align="right" style="text-align:right;">
<ul id="job_tabmenu">
    <li>
    	<a class="{if (!$filter_candidates)}active{else}inactive{/if}" href="{$ROOT_HOST}index.php?option=com_jobsfactory&Itemid={$Itemid}&task={$task}&id={$job_id}&filter_candidates=">
    	    {"COM_JOBS_VIEW_ACTIVE"|translate}</a>
    </li>
    <li>
        <a class="{if ($filter_candidates == 'call' )}active{else}inactive{/if}" href="{$ROOT_HOST}index.php?option=com_jobsfactory&Itemid={$Itemid}&task={$task}&id={$job_id}&filter_candidates=call">
            {"COM_JOBS_VIEW_CALL"|translate}</a>
    </li>
    <li>
        <a class="{if ($filter_candidates == 'analyze')}active{else}inactive{/if}" href="{$ROOT_HOST}index.php?option=com_jobsfactory&Itemid={$Itemid}&task={$task}&id={$job_id}&filter_candidates=analyze">
            {"COM_JOBS_VIEW_ANALYZE"|translate}</a>
    </li>
    <li>
        <a class="{if ($filter_candidates == 'rejected')}active{else}inactive{/if}" href="{$ROOT_HOST}index.php?option=com_jobsfactory&Itemid={$Itemid}&task={$task}&id={$job_id}&filter_candidates=rejected">
            {"COM_JOBS_VIEW_REJECTED"|translate}</a>
    </li>
</ul>
</div>


<form action="{$ROOT_HOST}" method="post" name="jobsForm" id="jobsForm">
<input type="hidden" name="option" value="{$option}" />
<input type="hidden" name="task" value="saveactions" />
<input type="hidden" name="jobid" value="{$job_id}" />
<input type="hidden" name="Itemid" value="{$Itemid}" />

{* Include filter selectboxes *}
{include file='elements/t_header_filter.tpl'}

<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="job_list_container">
<tr>
	<th style="width: auto; text-align: left;"><span class="title_grey">{"COM_JOBS_POSITION"|translate} /{"COM_JOBS_SALARY"|translate}</span></th>
	<th style="width: 80px; text-align: left;"><span class="title_grey">{"COM_JOBS_AGE"|translate} / {"COM_JOBS_GENDER"|translate}</span></th>
	<th style="width: 120px; text-align: left;"><span class="title_grey">{"COM_JOBS_DETAILS"|translate}</span></th>
    <th style="width: 170px; text-align: left;"><span class="title_grey">{"COM_JOBS_VIEW_ACTIONS"|translate}</span>
        <input name="save" value="{'COM_JOBS_SAVE'|translate}" class="buttons" type="submit" />
    </th>
</tr>
	{section name=candidatesloop loop=$candidates_rows}
		{include file='elements/lists/t_complistcandidates_cell.tpl' candidate=`$candidates_rows[candidatesloop]` index=`$smarty.section.candidatesloop.rownum`}
	{/section}
</table>
{include file='elements/t_listfooter.tpl'}

</form>

<div style="clear: both;"></div>