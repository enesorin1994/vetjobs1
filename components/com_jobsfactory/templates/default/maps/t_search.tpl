{set_css}
{include file='js/t_javascript_language.tpl'}
<h1>{"COM_JOBS_RADIUS_SEARCH"|translate}</h1>

{include file='t_search_tabs.tpl'}
{starttab name="job_tabmenu" id="tab2"}

{if ($cfg->google_key!="") }
    {include file="js/t_javascript_maps.tpl"}
    {import_js_block}
        {literal}
        window.addEvent('domready', function () {
            load_gmaps();
        });
        {/literal}
    {/import_js_block}

    <div class="jobs_search_map_filters">
        <div class="left_detail">
            <span class = "label">{"COM_JOBS_ADDRESS"|translate}</span>
            <input type = "text" id = "addressInput" />

            <span class = "label">{"COM_JOBS_RADIUS"|translate}</span>
            <select id = "radiusSelect" style="width: 70px;">
                {foreach from=$lists.radius_units item=item}
                        <option value = "{$item}">{$item}</option>
                {/foreach}
            </select>
            {if $cfg->googlemap_distance==1}
                {"COM_JOBS_KM"|translate}
            {else}
                {"COM_JOBS_MILES"|translate}
            {/if}
            &nbsp;
            <span class="pull-right">
                <input type = "button" class = "btn btn-primary" onclick = "gmap_refreshjobs_search()" value = "{'COM_JOBS_FIND'|translate}" />
            </span>
        </div>
    </div>

    <br/>    
    <br/>

    <div class="map_search left_detail">
        <div id = "map_canvas" class="maps_t_search"></div>
            <span id = "wait_loader" class="text-center" style = "width:auto; display:none;">
                <img src = "{$IMAGE_ROOT}ajax-loader2.gif" style = "width:170px !important;" />
            </span>

        <div id = "search_sidebar"></div>
    </div>
    <br/>
{else}
	{"COM_JOBS_NO_GOOGLE_MAP_DEFINED"|translate}
{/if}

<div style="clear:both;"></div>
{endtab}
