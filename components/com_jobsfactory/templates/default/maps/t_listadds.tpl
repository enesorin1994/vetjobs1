{set_css}
{include file='js/t_javascript_language.tpl'}
<h1>{$page_title}</h1>

{include file='elements/category/t_categories_tabs.tpl'}
{starttab name="job_tabmenu" id="tab4"}

{if $cfg->google_key}
    {include file="js/t_javascript_maps.tpl"}
    {import_js_block}
        {literal}
        window.addEvent('domready', function () {
            load_gmaps();
            gmap_refreshjobs();
        });
        {/literal}
    {/import_js_block}

    <span class="v_spacer_15"></span>
    <div class = "jobs_list_map_filters">
        <div>
            <span class = "label">{"COM_JOBS_CATEGORY"|translate}</span>
            {$lists.category}
            <label class="control-label" for="ads_limitbox">{"COM_JOBS_SHOW"|translate}</label>
            <span>
                <select id = "ads_limitbox" onchange = "gmap_refreshjobs();" style="max-width:80px;">
                    <option value = "10">10</option>
                    <option value = "20" selected = "">20</option>
                    <option value = "50">50</option>
                    <option value = "0">{"COM_JOBS_ALL"|translate}</option>
                </select>
            </span>
        </div>
    </div>

    <br />
    <div id = "wait_loader" class="text-center" style = "width:auto; display:none;"><img src = "{$IMAGE_ROOT}ajax-loader2.gif" /></div>
    <br />
    <div class="map_search">
        <div id = "map_canvas" class="maps_t_listjobs"></div>
    </div>
    	{else}
    		{"COM_JOBS_NO_GOOGLE_MAP_DEFINED"|translate}
    {/if}
    <div class="pull-left" style="clear:both;"></div>

{endtab}