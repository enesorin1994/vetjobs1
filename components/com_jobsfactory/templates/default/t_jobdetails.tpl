{* Include Tabbing Scripts *}
{startpane id="jobdetail-pane" name="jobdetail-pane" active="tab1" usecookies=0}

{createtabslist name="jobdetail-pane"}
    {additemtabs id="tab1" name="jobdetail-pane" active="1" text="COM_JOBS_TAB_JOB_DETAIL"|translate}
    {if ($cfg->google_key!="") && $cfg->map_in_job_details && (($job->googlex && $job->googley)||($user->googleMaps_x && $user->googleMaps_y)) }
        {additemtabs id="tab2" name="jobdetail-pane" active="0" text="Location on Map"|translate}
    {/if}
	{if $job->isMyJob() && $job->messages}
	{additemtabs id="tab3" name="jobdetail-pane" active="0" text="COM_JOBS_TAB_SYSTEM_MESSAGES"|translate}
	{/if}
{endtabslist}

  {starttab name="jobdetail-pane" id="tab1" text="COM_JOBS_TAB_JOB_DETAIL"|translate}

    {include file='js/t_javascript_language.tpl'}
    {include file='js/t_javascript_countdown.tpl'}

    {if $payment_items_header}
        <div class="jobs_headerinfo" xmlns = "http://www.w3.org/1999/html">{$payment_items_header}</div>
    {/if}
                            {*<div class="pull-left span12">
                              {literal}
                                  <script type="text/javascript" src="http://platform.linkedin.com/in.js"></script>
                                  <script type="in/share" data-counter="right"></script>
                              {/literal}
                              </div>*}
    <!-- [+]Job Container -->
    <div class="jobContainer">

        <div class="job_details span12 detail_job_header grey_line1">
            {* Top line where is included job category and back to listing link *}
            <div class="grey_line1">
                <div class="job_details span8">
                    {if $job->get('catname')}
                        <a href="{$job->get('links.filter_cat')}">
                            <img src="{$TEMPLATE_IMAGES}folder.png" style="border:0; margin:1px; vertical-align:top;"/> {$job->get('catname')}</a>
                    {else} &nbsp;-&nbsp;
                    {/if}
                </div>
                <div class="job_details span4">
                    <span style="display:inline-block;float:right;"><a class="" href="{$job->get('links.jobs_listing')}">{'COM_JOBS_BACK_TO_LIST'|translate}</a></span>
                </div>
            </div>

            <div class="job_detail_text1">
                <div>
                    <div class="job_title_details">
                        <div class="job_details span6">
                            {if $job->isMyJob() && !$job->published}
                              <span class="job_title_details"><i>{$job->title}</i> ({"COM_JOBS_UNPUBLISHED"|translate})</span>
                            {else}
                              <span class="job_title_details">{$job->title}</span>
                            {/if}
                        </div>
                        <div class="job_details span6">{include file="elements/jobdetail/t_action_buttons.tpl"}</div>
                    </div>
                    <div>
                        <div class="detail_job_description span5">
                            <div class="box_content current">
                                <label class="job_lables">{"COM_JOBS_JOB_VALID_UP_TO"|translate}</label>
                                &nbsp;{$job->get('enddate_text')}
                                {if $job->get('expired') }
                                  <span class="job_expired">{"COM_JOBS_JOB_EXPIRED"|translate}</span>
                                {/if}
                            </div>
                        </div>
                        <div class="job_details span2">
                            <div class="box_content current">
                                {if $job->featured != 'none'}
                                    <span class="featured_job_detail"><label>{$job->featured}</label></span>{*alert-success  .alert-info*}
                                {else}
                                    &nbsp;
                                {/if}
                           </div>
                        </div>
                        <div class="detail_job_description span5" align="right1">
                            <div class="box_content current pull-right">
                                <label class="job_lables">{"COM_JOBS_JOB_UPDATED"|translate}</label>
                                &nbsp;{$job->get('modifieddate_text')}
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div class="jobs_position_header"  style="border-bottom: none">
                   {positions position="header" item=$job page="jobs"}
                </div>
            </div>
        </div>

        <div class="job_details span12">
            {*<div style="width: 100%;">*}
                {if $job->isMyJob()}
                    <div class="job_details span12" style="padding-right: 13px;">
                        <div class="box_bottom" style = "text-align: center;">
                            {if $job->show_candidate_nr!=='0'}
                                <span class="jobs_heading_status">
                                    {"COM_JOBS_NR_CANDIDATES"|translate}:
                                </span>
                                <label class="job_lables" style="font-size: 18px !important; float: none;">
                                    <a href="{$ROOT_HOST}index.php?option=com_jobsfactory&task=companylistcandidates&id={$job->id}&Itemid={$Itemid}">
                                        {$job->get('nr_applicants')}
                                    </a>
                                </label>
                            {/if}
                        </div>

                        <span class="v_spacer_5"></span>
                        </div>
                {else}
                    <div class="job_details span12" style="padding-right: 5px;">
                        {if $job->close_offer==0 && $job->get('expired') != 1}
                           {if $is_logged_in && $lists.hasApplied == 0}
                                <div class="box_content" style="text-align: left;">
                                   {include file="elements/jobdetail/t_apply.tpl"}
                                </div>
                           {elseif $is_logged_in && $lists.hasApplied > 0}
                                <div class="box_content applied-msg">
                                   <h3>{"COM_JOBS_YOU_ALREADY_APPLIED"|translate}</h3>
                                   {if $jobs|@count}
                                       <h4>{"COM_JOBS_HAVE_APPLIED"|translate} {$jobs|@count - 1}</h4>
                                   {/if}
                                </div>
                            {else}
                                <div class="box_app_details">{include file="elements/jobdetail/t_app_guest.tpl"}</div>
                            {/if}
                        {/if}
                    </div>
                {/if}

            {*</div>*}
        </div>

        <div style="clear: both;"></div>
        <div class="add_detail_socialtoolbar span12" style="padding-right: 3px;">
           {include file='elements/jobdetail/t_details_job_social.tpl'}
        </div>
        <div class="job_details span12 grey_line1">
            <div>
                {* Left column split all row for right side   class="job_details" *}
                <div class="detail_job_description span7">
                    <div class="box_content">
                        <div style="text-align: justify;" class="job_details_info">

                            {$job->description}

                            <span class="v_spacer_5"></span>

                            {if $job->file_name !="" }
                                <a href="{$job->get('links.download_file')}" target="_blank" >
                                    <img src="{$TEMPLATE_IMAGES}attach.gif" style="border:0; margin:1px; vertical-align:middle;"/>
                                    {$job->file_name}
                                </a>
                            {/if}
                            <span class="v_spacer_5"></span>
                        </div>
                        <div class="box_bottom"></div>
                    </div>
                    <span class="v_spacer_5"></span>

                    <div class="job_details jobs_position">
                        {positions position="detail-left" item=$job page="jobs"}
                    </div>
                    <div style="clear: both;"></div>
                </div>

                {* Right side include company details and job details *}
                <div class="detail_job_info job_details span5">
                    <div class="box_title">{'COM_JOBS_COMPANY_DETAILS'|translate}</div>

                    <div width="100%" class="box_right_details">
                        <div class="span12">
                            <label class="job_lables pull-left">
                            {if $cfg->employervisibility_enable || $job->id}
                                 {if $job->employer_visibility == 1}
                                      <a href = "{$job->get('links.employer_profile')}">
                                        {if $employer->name != ''}{$employer->name}
                                        {else}
                                            {$employer->username}
                                        {/if}
                                          {if $employer->picture}
                                            <img src="{$COMPANY_PICTURES}/resize_{$employer->picture}" width="100" style="float: right;" />
                                          {else}
                                            <img src="{$IMAGE_ROOT}/user/logo.png" width="100" style="float: right;" />
                                          {/if}
                                      </a>
                                 {else}
                                     {if $job->employer_visibility == $EMPLOYER_VISIBILITIES.EMPLOYER_TYPE_CONFIDENTIAL}{"COM_JOBS_CONFIDENTIAL"|translate}{else}{"COM_JOBS_PUBLIC"|translate}{/if}
                                 {/if}
                            {else}
                                 <strong>{if $cfg->employervisibility_val == $EMPLOYER_VISIBILITIES.EMPLOYER_TYPE_CONFIDENTIAL}{"COM_JOBS_CONFIDENTIAL"|translate}{else}{"COM_JOBS_PUBLIC"|translate}{/if}</strong>
                            {/if}
                            </label>
                            <span class="left_spacer_10"></span>
                            <a href = "{$job->get('links.employer_profile')}" title = "{'COM_JOBS_DETAILS'|translate}"></a>
                        </div>

                        <div style="clear: both;"></div>
                        <div class="span12">
                            <label class="job_lables pull-left">{"COM_JOBS_JOINED_SINCE"|translate}:</label> &nbsp;{printdate date=$employer->registerDate use_hour=0}
                        </div>

                        <div class="span12">
                            {if $employer->city || $employer->country}
                                <label class="job_lables pull-left">{"COM_JOBS_USER_LOCATION"|translate}:</label>&nbsp;

                                    {$employer->city}{if $employer->city},{/if}
                                    {$employer->country}
                                    {if !$employer->country}-{/if}

                            {/if}
                        </div>
                        <div class="span12">

                            <a href = "{$job->get('links.otherjobs')}">{"COM_JOBS_OTHER_JOBS"|translate}</a>
                                    {"COM_JOBS_FROM_THIS_USER"|translate}
                        </div>
                        <span style="line-height:1px; padding: 0;">&nbsp;</span>
                    </div>

                    <span class="v_spacer_5"></span>
                    <div style="clear: both;"></div>

                    <div class="box_title">{"COM_JOBS_DETAILS"|translate}:</div>
                    <div width="100%" class="box_right_details">
                        <div>
                            <div class="span12">
                                <label class="job_lables pull-left">{"COM_JOBS_JOB_TYPE"|translate}:</label> &nbsp;{$lists.jobType}
                            </div>
                            <div class="span12">
                                <label class="job_lables pull-left">{"COM_JOBS_AVAILABLE_POSTS"|translate}:</label> &nbsp;{$job->jobs_available}
                            </div>
                            <div class="span12">
                                <label class="job_lables pull-left">{"COM_JOBS_JOB_LOCATION"|translate}:</label>&nbsp;
                                    {if $cfg->enable_location_management}
                                        {if $lists.job_location_state || $lists.job_location_city}
                                            {$lists.job_location_city}{if $lists.job_location_city},{/if}
                                            {$lists.job_location_state}{if $lists.job_location_state},{/if}
                                            {$job->get('job_country')}
                                        {/if}
                                    {else}
                                        {$lists.job_location_city}, {$lists.job_location_country}
                                    {/if}
                            </div>
                            <div class="span12">
                                <label class="job_lables pull-left">{"COM_JOBS_EXPERIENCE_REQUEST"|translate}:</label> &nbsp;{$lists.jobExperienceRequested}
                            </div>
                            <div class="span12">
                                <label class="job_lables pull-left">{"COM_JOBS_STUDIES_REQUEST"|translate}:</label> &nbsp;{$lists.jobStudiesRequested}
                            </div>
                            {if $job->languages_request}
                                <div class="span12">
                                    <label class="job_lables pull-left">{"COM_JOBS_LANGUAGES_REQUEST"|translate}:</label> &nbsp;{$job->languages_request}
                                </div>
                            {/if}
                            <div class="span12">
                                <label class="job_lables pull-left">{"COM_JOBS_START_DATE"|translate}:</label> &nbsp;{$job->get('startdate_text')}
                            </div>

                            <div class="span12">
                                {if $job->close_offer}
                                    <h4><span class="closed" style="float: left;">{jtext text="COM_JOBS_CLOSED_ON"}: {if $job->get('closed_date_text')} {$job->get('closed_date_text')} {/if}</span></h4>
                                {elseif $job->get('expired')}
                                    <span style="float: right;">
                                         <span class='expired'>{jtext text="COM_JOBS_JOB_EXPIRED"}: </span>({$job->get('enddate_text')})
                                    </span>
                                {else}
                                    {if $cfg->enable_countdown}
                                         <label class="job_lables pull-left">{jtext text="COM_JOBS_DETAILS_ENDING_IN"}: </label><span class="timer"> {$job->get('countdown')}</span>
                                    {else}
                                         <label class="job_lables pull-left">{jtext text="COM_JOBS_END_DATE"}: </label> <span class=''> {$job->get('enddate_text')}</span>
                                    {/if}
                                {/if}
                            </div>
                            <div class="span12">
                                <label class="job_lables">{$job->hits}&nbsp;{"COM_JOBS_HITS"|translate}</label>
                            </div>
                            <span style="line-height:1px; padding: 0;">&nbsp;</span>
                        </div>
                    </div>
                    <div class="detail_right_position">
                        {positions position="detail-right" item=$job page="jobs"}
                    </div>
                </div>
            </div>

        </div>

        <div class="job_details span12 grey_line1">
            <div class="detail_job_info span12">
                <div class="job_detail_info">
                    <div>
                        <div class="detail_job_description">
                            <br />
                            <div class="box_title">{"COM_JOBS_BENEFITS"|translate}</div>
                            <div class="box_content">
                                <div><span class="job_details_info">{$job->benefits}</span></div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="detail_job_description">
                            {positions position="middle" item=$job page="jobs"}
                        </div>
                    </div>

                    <div class="grey_line">
                        <div class="detail_job_description">
                            <br /><div class="box_title">{"COM_JOBS_ABOUT_US"|translate}</div>
                            <div class="box_content">
                                {if $job->about_visibility == 1}
                                    <div><span class="job_details_info">{$employer->about_us}</span></div>
                                {else}
                                    <div><strong>{"COM_JOBS_CONFIDENTIAL"|translate}</strong></div>
                                {/if}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="job_details job_details_tag">
                            {positions position="footer" item=$job page="jobs"}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="span12">
            <div class="detail_job_info">
                <div class="job_details_tag">
                    <span class = "v_spacer_5"></span>
                    {$job->get('links.tags')}
                </div>
            </div>
        </div>

    </div>

    <div style="clear:both;"></div>
  {endtab}
  {if $cfg->google_key && $cfg->map_in_job_details && (($job->googlex && $job->googley)||($user->googleMaps_x && $user->googleMaps_y)) }
      {include file="elements/jobdetail/t_tab_maps.tpl"}
  {/if}
	{if $job->isMyJob() && $job->messages}
		{include file="elements/jobdetail/t_tab_messages.tpl"}
	{/if}
{endpane name="jobdetail-pane" id="jobdetail-pane"}
