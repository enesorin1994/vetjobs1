{set_css}
{include file='js/t_javascript_language.tpl'}
{import_js_file url="jobs.js"}

<h2>
{"COM_JOBS_CANDIDATES_LIST"|translate}
	<a href="index.php?option=com_jobsfactory&task={$task}&format=feed&limitstart=" target="_blank">
		<img src="{$IMAGE_ROOT}f_rss.jpg" width="14" border="0" alt="RSS" />
	</a>
</h2>

<form action="{$ROOT_HOST}" method="get" name="jobsForm" id="jobsForm">
<input type="hidden" name="option" value="{$option}" />
<input type="hidden" name="task" value="{$task}" />
<input type="hidden" name="Itemid" value="{$Itemid}" />

{* Include filter selectboxes *}
{include file='elements/t_header_filter.tpl'}

<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="job_list_container table-striped">
  <tr style="height: 30px;">
  	<th style="width: auto; text-align: left;">
        {include file='elements/sort_field.tpl' label="Position"|translate key="function"}<span class="title_grey">/</span>
    {include file='elements/sort_field.tpl' label="Salary"|translate key="desired_salary"}</th>

	<th  style="width: 100px; text-align: left;"><span class="title_grey">{"COM_JOBS_AGE"|translate}/ </span>{include file='elements/sort_field.tpl' label="Gender"|translate key="gender"}</th>
	<th style="width: 120px; text-align: left;" class="title_grey">{"COM_JOBS_DETAILS"|translate}</th>
    {if $task=='companylistcandidates'}
        <th style="width: 100px; text-align: left;">{"COM_JOBS_VIEW_ACTIONS"|translate}
           <input name="save" value="{'COM_JOBS_SAVE'|translate}" class="buttons" type="submit" />
        </th>
    {/if}
  </tr>
  {section name=candidatesloop loop=$candidates_rows}
  	  {include file='elements/lists/t_listcandidates_cell.tpl' candidate=`$candidates_rows[candidatesloop]` index=`$smarty.section.candidatesloop.rownum`}
  {/section}
</table>
{include file='elements/t_listfooter.tpl'}

</form>
<div style="clear: both;"></div>