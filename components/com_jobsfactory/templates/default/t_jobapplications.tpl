{set_css}
{include file='js/t_javascript_language.tpl'}
{include file='js/t_javascript_countdown.tpl'}

<h2>{"COM_JOBS_JOBS_APPLICATIONS"|translate}</h2>

<form action="{$ROOT_HOST}index.php" method="get" name="jobsForm">
<input type="hidden" name="option" value="{$option}">
<input type="hidden" name="task" value="{$task}">
<input type="hidden" name="Itemid" value="{$Itemid}">


{* Include filter selectboxes *}
{include file='elements/t_header_filter.tpl'}

<table align="center" cellpadding="0" cellspacing="0" width="100%" id="job_list_container">
	{section name=jobsloop loop=$job_rows}
		{include file='elements/lists/t_listjobs_cell.tpl' job=`$job_rows[jobsloop]` index=`$smarty.section.jobsloop.rownum`}
	{/section}
</table>
{include file='elements/t_listfooter.tpl'}

</form>
<div style="clear: both;"></div>