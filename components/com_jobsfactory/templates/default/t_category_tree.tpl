{set_css}
{import_js_file url="jobs.js"}

<h2><strong>{$page_title}</strong></h2>

{include file='elements/category/t_categories_tabs.tpl'}
{starttab name="job_tabmenu" id="tab2"}
  {foreach from=$categories item=category}
	<div class="job_treecat">
		<div class="cat_link span12">
			<span class="pull-left"><a href="{$category->link}"> {$category->catname}</a></span>
			<span class="job_treecat_icons pull-left">
                <a href="{$category->link}">
                    <img src="{$IMAGE_ROOT}folder-horizontal-open.png" border="0" alt="" title="{'COM_JOBS_FILTER_CATEGORY'|translate}" /></a>
                <a href="{$category->view}">
                    <img src="{$IMAGE_ROOT}document-text-image.png" border="0" alt="" title="{'COM_JOBS_FILTER_CATEGORY'|translate}" /></a>
                {if $is_logged_in}
                    <a href="{$category->link_watchlist}">
                    {if $category->watchListed_flag}
                        <img src="{$IMAGE_ROOT}f_watchlist_0.png" border="0" alt="" title="{'COM_JOBS_REMOVE_FROM_FAVORITESLIST'|translate}"/>
                    {else}
                        <img src="{$IMAGE_ROOT}f_watchlist_1.png" border="0" alt="" title="{'COM_JOBS_add_to_watchlist'|translate}"/>
                    {/if}</a>
                    {if $isCompany}
                        <a href = "{$category->link_new_listing}"><img src = "{$IMAGE_ROOT}new_listing.png" border = "0" alt = "{"COM_JOBS_NEW_LISTING_IN_CATEGORY"|translate}"
                            title = "{"COM_JOBS_NEW_LISTING_IN_CATEGORY"|translate}" class="new_listing" /></a>
                    {/if}
                {/if}
            </span>
            <span style="font-size: 10px;font-weight: normal;" class="pull-left1">
                    ({$category->nr_a}
            	    {if $category->nr_a > 1 || !$category->nr_a}
            		    {"COM_JOBS_NR_JOBS"|translate}
            		    {else}
            		    {"COM_JOBS_NR_JOB"|translate}
            	    {/if}

                    | {$category->subcategories|@count}
            	    {if $category->subcategories|@count > 1 || !$category->subcategories|@count}
            		    {"COM_JOBS_NR_SUBCATS"|translate})
            		    {else}
            		    {"COM_JOBS_NR_SUBCATEGORY"|translate})
            	    {/if}
            </span>

            {if $category->subcategories|@count>0}
            <span class="pull-left1">
                {assign var="categoryid" value=$category->id}
            	<a style="outline: none;" href="#" onclick='jQuery("#job_cats_{$categoryid}").slideToggle("slow");return false;'><img src="{$TEMPLATE_IMAGES}f_expand_01.png" title="{'COM_JOBS_HIDE_SUBCATEGORIES'|translate}" class="job_link" style="margin-left:8px;" /></a>
            </span>
            {/if}

		</div>
	</div>

    {if $category->subcategories|@count>0}
        <br />

        <div id="job_cats_{$category->id}" class="job_treecatsub">
            {include file="elements/category/t_subcategories.tpl" subcategories=$category->subcategories}
        </div>
    {/if}
  {/foreach}

<div style="clear: both;"></div>
{endtab}