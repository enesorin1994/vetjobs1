jQuery(document).ready( function ($) {

    $(document).on("click", ".show_experience_form", function(event){
    	event.preventDefault();
    	var id = $(this).attr("rel");
    	$('#experience_form' + id).show("normal");
      });

    $(document).on("click", ".cancel_experience", function(event){
        event.preventDefault();
        var id = $(this).attr("rel");
        $('.jobsfactory_experience_form').hide('fold');
      });

    $(document).on("click", ".show_education_form", function(event){
        event.preventDefault();
        var id = $(this).attr("rel");
        $('#education_form' + id).show("normal");
      });

    $(document).on("click", ".cancel_education", function(event){
        event.preventDefault();
        var id = $(this).attr("rel");
        $('.jobsfactory_education_form').hide('fold');
      });


     // Save button
    $(document).on("click", ".save-button", function(event){
        event.preventDefault();

        // Validate mail and birthdate
        var $email = $('form input[name="email"]'); //change form to id or containment selector
        var regex=/^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
        if ($email.val() != '' && !regex.test($email.val()))
        {
            alert(language["job_err_email"]);
            //alert('Please enter a valid email address.');
            return false;
        }

        // Validate birthdate
        var currentTime = new Date();
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear() - 15;

    	var nowDate = year + '-' + month + '-' + day;

        var joomlaformat = job_date_format;
        joomlaformat = joomlaformat.replace('m', 'M');
        joomlaformat = joomlaformat.replace('Y', 'y');
        //'yyyy-MM-dd'

        var $birthdate = $('form input[name="birth_date"]');

       // jQuery('form input[name="birth_date"]').val()
        if (!isDate($birthdate.val(), joomlaformat))
        {
            alert(language["job_err_birthdate"]);
            return false;
        }

        if ( compareDates(nowDate, 'yyyy-M-d', $birthdate.val(), joomlaformat) < 0 || dateGt( nowDate, 'yyyy-M-d', $birthdate.val(), joomlaformat) < 0 )
        {
            alert(language["job_err_birthdate"]);
            return false;
        }

        form = document.getElementById('rJobForm');
        if(!form){
            form = document.adminForm;
        }

        form.submit();

    });

});
