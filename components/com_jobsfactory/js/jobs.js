var jobObject = {
        toggleDescription: function (id,image)
        {
            $$('#'+id).each(function(el){
                if(el.getStyle('display')=='none'){
                    el.setStyle('display','block');
                    image.src=image.src.replace('_01.png','_02.png');
                }
                else{
                    el.setStyle('display','none');
                    image.src=image.src.replace('_02.png','_01.png');
                }
            })
        },
        ApplicationFormValidate: function(form)
        {
           	terms =  form.elements['agreement'];
            //var edittask = form.elements['task'].value;

        	// verify if Terms and conditions are checked for Save
        	if( must_accept_term && !terms.checked && btnClicked != 'Cancel'){
        		alert(language["job_err_terms"]);
                $(terms).addClass('invalid');
                $(terms).getParent().addClass('invalid');

                return false;
        	}

        	return true;
        },
        submitListForm:function (order_by,order_direction)
        {
            if (order_direction=='!ASC') 
                order_direction='DESC';
            if (order_direction=='!DESC') 
                order_direction='ASC';
            if (order_direction=='') 
                order_direction='ASC';

            form = document.getElementById('jobsForm');
            if(!form){
                form = document.adminForm;
            }

            if (typeof(task) !== 'undefined') {
                form.task.value = task;
            }

            form.filter_order.value=order_by;
            form.filter_order_Dir.value=order_direction;
            form.submit();
        },        
        SendMessage:function (link,message_id,candidate_id,username)
        {
            if (!candidate_id) candidate_id=0;
            if (!message_id) message_id=0;
        
            //if (link) link.style.display='none';
            $$("dt.tab2").fireEvent("click");
            document.getElementById('candidate_id').value=candidate_id;
        	document.getElementById('idmsg').value=message_id;
            document.getElementById('job_message_box').style.display='block';
            document.getElementById('message_to').innerHTML=username;
            document.getElementById('message').focus();
        },
        SendBroadcastMessage:function (link)
        {
        	if (link) link.style.display='none';
        
        	document.getElementById('broadcast_msg').style.display='block';
        	document.getElementById('broadcast_message').focus();
        }
}

 String.prototype.trim = function () {
   return this.replace(/^\s+|\s+$/gm, '');
 };

  String.prototype.fulltrim=function(){
      return this.replace(/(?:(?:^|\n)\s+<p>|\s+<\/p>(?:$|\n))/g,'').replace(/\s+/g,' ');
  };


var btnClicked = '';
window.onload = function() {

    var buttonC = document.getElementById("cancel_edit");
    var formC = document.getElementById('rJobForm');

    var handlerC = function() {
      btnClicked = document.getElementById('cancel_edit').value;
    };

    if ( buttonC && formC) {
        if (buttonC.addEventListener) {
            buttonC.addEventListener("click", handlerC, false);
        } else if (buttonC.attachEvent) {
            // NB Takes two parameters
            buttonC.attachEvent("onclick", handlerC);
        }
    }
};

jQuery(document).ready(function ($) {
  // Rate company
  $('.button-rate-user').click(function (event) {
    event.preventDefault();

    var $element = $(this),
        parent = $element.parents('.actions:first'),
        block = parent.parents('.rate-block:first'),
        url = $element.attr('href'),
        stars = '<ul class="user-rating active"><li></li><li></li><li></li><li></li><li></li></ul>';

    parent.hide();
    block
      .append(stars)
      .find('.user-rating li').click(function (event) {
        event.preventDefault();

        var $star = $(this),
            rating = $star.index() + 1;

        $.post(url, { rating: rating, format: 'json' }, function (response) {
          $star.parent().removeClass('active').find('li:lt(' + rating + ')').addClass('full');
        }, 'json');
      }).end();
  });

});