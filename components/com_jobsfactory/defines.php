<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
if (!defined('APP_EXTENSION')) define('APP_EXTENSION', 'com_jobsfactory');

define('JOB_PICTURES', JURI::root().'/media/com_jobsfactory/images');
define('JOB_PICTURES_PATH',JPATH_ROOT.DS.'media'.DS.'com_jobsfactory'.DS.'images'.DS);
define('JOB_UPLOAD_FOLDER',JPATH_ROOT.DS.'media'.DS.'com_jobsfactory'.DS.'files'.DS);
define('JOB_TEMPLATE_CACHE', JPATH_ROOT.DS.'cache'.DS.'com_jobsfactory'.DS.'templates');

define('EMPLOYER_TYPE_PUBLIC', 1);
define('EMPLOYER_TYPE_CONFIDENTIAL', 0);
define('EMPLOYER_TYPE_ONE_ON_ONE', 3);
define('EMPLOYER_TYPE_LIMITED', 4);

define('RESUME_PICTURES', JURI::root().'/media/com_jobsfactory/resumeimages');
define('RESUME_PICTURES_PATH',JPATH_ROOT.DS.'media'.DS.'com_jobsfactory'.DS.'resumeimages'.DS);
define('RESUME_UPLOAD_FOLDER',JPATH_ROOT.DS.'media'.DS.'com_jobsfactory'.DS.'resumefiles'.DS);

define('COMPANY_PICTURES', JURI::root().'/media/com_jobsfactory/companyimages');
define('COMPANY_PICTURES_PATH',JPATH_ROOT.DS.'media'.DS.'com_jobsfactory'.DS.'companyimages'.DS);
define('COMPANY_UPLOAD_FOLDER',JPATH_ROOT.DS.'media'.DS.'com_jobsfactory'.DS.'companyimages'.DS);

define('USER_GROUP_CANDIDATES',0);
define('USER_GROUP_COMPANIES',1);
