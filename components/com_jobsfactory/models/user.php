<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class jobsModelUser extends jobsModelGeneric
{
    var $_name = 'User';
    var $name = 'User';
    var $page = 'user_profile';
    var $context = 'userModel';

    var $knownFilters = array(
        'keyword' => array('type' => 'string'),
        'country' => array('type' => 'string'),
        'desired_salary' => array('type' => 'string'),
        'job_type' => array('type' => 'int'),
        'total_experience' => array('type' => 'int'),
        'study_level' => array('type' => 'int'),
        'experience_request' => array('type' => 'int'),
        'gender' => array('type' => 'string'),
        'city' => array('type' => 'string')
    );

    var $attachment = null;

    function getUserData($id)
    {
        $user = JobsHelperTools::getUserProfileObject();
        $user->getUserProfile($id);
        return clone $user;
    }


    function bindUserDetails(&$user){

        $cfg = JTheFactoryHelper::getConfig();
        $app = JFactory::getApplication();
        $my = JFactory::getUser();

        //$user->id = $app->input->getInt('id', null);
        //$user->load($user->id);
        $user->userid = $app->input->getInt('resume_id', null);
        $user->load(array('userid'=>$user->userid));

        $ignore = array('user_achievements','user_goals','user_hobbies');
        $user->bind($app->input->getArray($_POST),$ignore);

        $user->userid = $my->id;
        $user->birth_date = JobsHelperDateTime::DateToIso($user->birth_date);
        $user->birth_date = JobsHelperDateTime::isoDateToUTC($user->birth_date); //store all in UTC

        $date = JFactory::getDate();
        $user->modified = $date->toSql();

        if ($user->id) {
            if($user->file_name!="")
                $this->file_name = $user->file_name;

            $user->modified = gmdate('Y-m-d H:i:s');
            if ($user->useruniqueid == '') {
                $user->useruniqueid = uniqid(); //microtime().
            }
        } else {
            $user->userid = $my->id;
            $user->modified = gmdate('Y-m-d H:i:s');
            $user->useruniqueid = uniqid(); //microtime().
        }
    }

    function saveUserDetails($userdetails)
    {
        $my = JFactory::getUser();
        $db = JFactory::getDbo();
        $user = JTable::getInstance('users', 'Table');

        //if (!$user->load($my->id)) {
        if (!$user->load(array('userid'=>$my->id))) {
            $db->setQuery("INSERT INTO " . $user->getTableName() . "(`userid`) values ({$my->id})");
            $db->execute();
        }

        $userdetails->user_achievements = $user->user_achievements;
        $userdetails->user_goals = $user->user_goals;
        $userdetails->user_hobbies = $user->user_hobbies;

        $error = $this->validateSave($userdetails);
        if (count($error) > 0) return $error;

        if (!$e = $userdetails->store()) {
            $error[] = $e;
            return $error;
        }

        $userdetails->load(array('userid'=>$my->id));

        // UPLOAD FILES
        $msg = $this->uploadFiles($userdetails);

        if (strlen($msg)) {
            $error[] = $msg;
        }

        // Upload file zip,rar,txt
        $this->uploadAtachment($userdetails);
        $this->deleteFiles($userdetails);

        return $error;
    }

    function bindUserSkills(&$user){

        $app = JFactory::getApplication();
        $my = JFactory::getUser();
        $user->id = $app->input->get('resume_id', null);

        $user->load($user->id);

        $data['user_achievements'] = $app->input->getHtml('user_achievements','');
        $data['user_goals'] = $app->input->getHtml('user_goals');
        $data['user_hobbies'] = $app->input->getHtml('user_hobbies');

        if (!$user->bind($data)) {
            $this->setError($user->getError());
            return false;
        }

        $user->userid = $my->id;
        $date = JFactory::getDate();
        $user->modified = $date->toSql();

        if ($user->id) {
            if ($user->useruniqueid == '') {
                $user->useruniqueid = uniqid(); //microtime().
            }

        } else {
            $user->userid = $my->id;
            $user->useruniqueid = uniqid(); //microtime().
        }
    }


    function saveUserSkills($userdetails){

        $my = JFactory::getUser();
        $db = JFactory::getDbo();
        $user = JTable::getInstance('users', 'Table');

        if (!$user->load($my->id)) {
            $db->setQuery("INSERT INTO " . $user->getTableName() . "(`userid`) values ({$my->id})");
            $db->execute();
        }

        $error = array();
        if (!$e = $userdetails->store()) {
            $error[] = $e;
            return $error;
        }

        return $error;
    }


    function getUserCountries()
    {
        $db = JFactory::getDbo();
        $query = JTheFactoryDatabase::getQuery();
        $profile = JobsHelperTools::getUserProfileObject();
        $field = $profile->getFilterField('country');
        $table = $profile->getFilterTable('country');

        $query->select("distinct `{$field}` country");
        $query->select("c.`name` as text");
        $query->from($table, 'u');
        $query->join('left','#__jobsfactory_country','c','u.country=`c`.`id`');
        $query->where("`{$field}`<>'' and `{$field}` is not null");
        $db->setQuery((string)$query);

        return $db->loadObjectList();

    }

    function buildQuery()
    {
        $db = JFactory::getDbo();

        $task = JFactory::getApplication()->input->getCmd('task','showUsers');
        $profile    = JobsHelperTools::getUserProfileObject();
        $tableName  = $profile->getIntegrationTable();
        $tableKey   = $profile->getIntegrationKey();
        $integration = $profile->getIntegrationArray(); //fieldmap

        $fieldName = $profile->getFilterField('name');
        $query = JTheFactoryDatabase::getQuery();
        $query->select('`u`.*,`u`.`id` AS userid');
        $query->select('null AS password');

        $query->select('COUNT(DISTINCT j.id) AS nr_jobs');
        $query->select('COUNT(DISTINCT bi.id) AS nr_applications');

        $query->from('#__users', 'u');
        $query->join('left', '#__jobsfactory_jobs', 'j', '`u`.`id`=`j`.`userid` and j.`published`=1');

        if ($task == 'showUsers') {
            $query->join('left', '#__jobsfactory_candidates', 'bi', '`u`.`id`=`bi`.`userid`');
            $query->join('left', '#__jobsfactory_education', 'ed', '`u`.`id`=`ed`.`userid`');
            $query->join('left', '#__jobsfactory_userexperience', 'uexp', '`u`.`id`=`uexp`.`userid`');
        }

        //$this->buildCustomQuery($query, $profile, null, $profile->getProfileFields());
        if ($profile && $profile->profile_mode == 'cb') {
            $this->setCustomFiltersCB($profile);
            $this->buildCustomQueryCB($query, $profile, null, $profile->getProfileFields());
        } else {
            parent::buildCustomQuery($query, $profile, null, $profile->getProfileFields());
        }
        //parent::buildCustomQuery($query, $profile, null, $profile->getProfileFields());
        $queriedTables = $query->getQueriedTables();

        if ($profile) {

            $table = $profile->getFilterTable('name');
            $integration_name = $profile->getFilterField('name');
            $alias = array_search($table, $queriedTables);
            //$alias = array_search($tableName, $queriedTables);
            $integration_username = $profile->getFilterField('username');

            $integration_surname = $profile->getFilterField('surname');
            $surnametable = $profile->getFilterTable('surname');
            $surnamealias = array_search($surnametable, $queriedTables);

            $query->select(' `'.$alias.'`.`'.$integration_name.'` AS candidateName');
            $query->select(' `'.$surnamealias.'`.`'.$integration_surname.'` AS candidateSurname');
        } else {
            $query->select('u.name AS candidateName');
            $query->select(' ""  AS candidateSurname');
        }

        //all the filters are profile related; in order to work, we have to append them to the query AFTER the join has been made with the profile tables, in parent::buildCustomQuery()
        $keyword = $this->getState('filters.keyword');
        $name = $this->getState('filters.name');
        $k = $keyword ? $keyword : ($name ? $name : null);

        if ($k) {
            $s = array();
            $searchTerms = explode(" ", $k); // Split the words

            if (!empty($searchTerms)) {
                foreach ($searchTerms as $searchTerm) {

                    $table = $profile->getFilterTable('name');
                    $field = $profile->getFilterField('name');

                    if ($profile) {

                        if ($profile->profile_mode == 'cb') {

                            $alias = array_search($table, $queriedTables);
                            $s[] = ' (`' . $alias . '`.`' . $field . '` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                        } else {
                            $alias = array_search($table, $queriedTables);
                            $s[] = ' (`' . $alias . '`.`' . $field . '` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    //else {
                            //$s[] = ' (`u`.`name` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    //}
                        }
                    }
                    else {
                            $s[] = ' (`u`.`name` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    }

                    $surnametable = $profile->getFilterTable('surname');
                    $integration_surname = $profile->getFilterField('surname');
                    $surnamealias = array_search($surnametable, $queriedTables);

                    if ($profile) {
                        $s[] = ' (`' . $surnamealias . '`.`' . $integration_surname . '` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    }

                    $firstnametable = $profile->getFilterTable('contactname');
                    $integration_firstname = $profile->getFilterField('contactname');
                    $firstnamealias = array_search($firstnametable, $queriedTables);

                    if ($profile) {
                        $s[] = ' (`' . $firstnamealias . '`.`' . $integration_firstname . '` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    }

                    $lastnametable = $profile->getFilterTable('lastname');
                    $integration_lastname = $profile->getFilterField('lastname');
                    $lastnamealias = array_search($lastnametable, $queriedTables);

                    if ($profile && $profile->profile_mode == 'cb') {
                        $s[] = ' (`' . $lastnamealias . '`.`' . $integration_lastname . '` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    }

                    $middlenametable = $profile->getFilterTable('middlename');
                    $integration_middlename = $profile->getFilterField('middlename');
                    $middlenamealias = array_search($middlenametable, $queriedTables);

                    if ($profile && $profile->profile_mode == 'cb') {
                        $s[] = ' (`' . $middlenamealias . '`.`' . $integration_middlename . '` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    }

                    $table = '#__jobsfactory_userexperience';
                    $field = 'position';
                    $position = array_search($table, $queriedTables);

                    if ($position != '') {
                        //$aliasposition = ($position != '') ? $position : 'ui';
                        $aliasposition = $position;
                        $s[] = ' (' . $aliasposition . '.' . $field . ' LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    }

                    $field2 = 'exp_description';
                    $exp_description = array_search($table, $queriedTables);

                    if ($exp_description != '') {
                        $aliasexp_description = $exp_description;
                        $s[] = ' (' . $aliasexp_description . '.' . $field2 . ' LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    }
                }

                $query->where($s, 'OR');
            }
        }

        if ($this->getState('filters.country')) {
            $table = $profile->getFilterTable('country');
            $field = $profile->getFilterField('country');
            $alias = array_search($table, $queriedTables);

            if ($profile->profile_mode == 'cb') {
                $countrytable = JTable::getInstance('country', 'Table');
                $country      = $countrytable->getCountryName($this->getState('filters.country'));
                $query->where(' (`' . $alias . '`.`' . $field . '` LIKE  \'%' . $db->escape($country) . '%\') ');
            } else {
                $country = $this->getState('filters.country');
                $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($country) . '\') ');
            }
        }

        if ($this->getState('filters.city')) {
            $table = $profile->getFilterTable('city');
            $field = $profile->getFilterField('city');
            $alias = array_search($table, $queriedTables);
            $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.city')) . '\') ');
        }

        if ($this->getState('filters.desired_salary')) {
            $table = $profile->getFilterTable('desired_salary');
            $field = $profile->getFilterField('desired_salary');
            $alias = array_search($table, $queriedTables);
            $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.desired_salary')) . '\') ');
            //$query->where(" ui.desired_salary = '" . $db->escape($this->getState('filters.desired_salary')) . "' ");
        }

        if ($task == 'showUsers') {
           $genderFilter = JFactory::getApplication()->input->getCmd('gender');
            if ($genderFilter != 'all') {
                $table = $profile->getFilterTable('gender');
                $field = $profile->getFilterField('gender');
                $alias = array_search($table, $queriedTables);
                $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape((int)$genderFilter) . '\') ');
            }
        }

        if ($this->getState('filters.study_level')) {//#__jobsfactory_education
            //cb_levelsecondaryeducation
            $table = $profile->getFilterTable('study_level');
            $field = $profile->getFilterField('study_level');
            $alias = array_search($table, $queriedTables);
            $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.study_level')) . '\') ');
        }

        if ($this->getState('filters.total_experience')) {

           $total_experience = $db->escape($this->getState('filters.total_experience'));
            $table = $profile->getFilterTable('total_experience');
            $field = $profile->getFilterField('total_experience');
            $alias = array_search($table, $queriedTables);
            $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $total_experience . '\') ');
        }

        $filter_order = $this->getState('filters.filter_order');
        if ($filter_order) {
            $filter_order_Dir = $this->getState('filters.filter_order_Dir');
            $query->order($db->escape($filter_order . ' ' . $filter_order_Dir));
        }

        $cfg = JTheFactoryHelper::getConfig();
        $tableKey = $profile->getIntegrationKey();
        $alias = array_search($tableName, $queriedTables);
        $candidatesGroup = implode(',',$cfg->candidate_groups);

        if ($task == 'showUsers' && $profile) {
            $query->join('left', '#__user_usergroup_map', 'm', "`$alias`.`$tableKey` =`m`.`user_id` ");
            $query->where('m.group_id IN ('."$candidatesGroup".')');

            $table = $profile->getFilterTable('isVisible');
            $field = $profile->getFilterField('isVisible');

            $alias = array_search($table, $queriedTables);
            $query->where(' (`' . $alias . '`.`' . $field . '` = 1) ');
             //$query->where(' `ui`.`isVisible` = 1 ');
        }

        $query->group('`u`.`id`');

        return $query;
    }

    function setCustomFiltersCB($profile=null)
    {
      $app = JFactory::getApplication();
      $db = JFactory::getDbo();

      $queryCB		=	'SELECT `name`, `table` '
                       .	"\n FROM `#__comprofiler_fields` "
                       .	"\n WHERE profile=1 AND published=1 AND searchable = 1 ORDER BY `ordering` ";
      $db->setQuery( $queryCB );
      $searchableFields=$db->loadObjectList();

      foreach ($searchableFields as $field)
      {
          //TO DO: add 3rd and 4th params: default, type
          $requestKey='user_profile'.'%'.$field->name;
          $value=$app->getUserStateFromRequest($this->context.'.filters.'.$requestKey, $requestKey);

          if (!empty($value))
              $this->setState('filters.'.$requestKey, $value);
      }
    }

    function buildCustomQueryCB(&$query, $profileObject=null, $useridField=null, $selectIntegrationFields=array())
        {
            $queriedTables=$query->getQueriedTables();
            $cfg = JTheFactoryHelper::getConfig();
            $db = JFactory::getDbo();

            $queryCB		=	'SELECT `name`, `table` '
                                .	"\n FROM `#__comprofiler_fields` "
                                .	"\n WHERE profile=1 AND published=1 AND searchable = 1 ORDER BY `ordering` ";
            $db->setQuery( $queryCB );
            $searchableFields=$db->loadObjectList();

            /*38 =>
                object(stdClass)[440]
                  public 'name' => string 'cb_county' (length=9)
                  public 'table' => string '#__comprofiler' (length=14)*/

            if ($profileObject)
            {
                //first look for Joomla's users table; join it only if not queried
                $usersAlias=array_search('#__users', $queriedTables);
                if (false===$usersAlias)
                {
                    if (!$useridField)
                    {
                        JFactory::getApplication()->enqueueMessage('When using profile mode, the USERID foreign key is required!','error');
                        return;
                    }
                    $query->join('left', '`#__users`', '`u`', $useridField.'=`u`.`id`');
                    $usersAlias='u';
                }

                //join the extended profile table
                $tableName=$profileObject->getIntegrationTable();

                $tableKey=$profileObject->getIntegrationKey();
                $query->join('left', ''.$tableName.'', 'ui', '`'.$usersAlias.'`.`id`=`ui`.`'.$tableKey.'`');

                //we need to "refresh" this because of the previous joins
                $queriedTables=$query->getQueriedTables();
            }

            foreach ($searchableFields as $field)
            {
                $requestKey='user_profile'.'%'.$field->name;

                $searchValue=$this->getState('filters.'.$requestKey);

                $fieldName=$field->name;
                //$fieldName=$profileObject->getFilterField($field->db_name);
                $tableName=$field->table;
                $tableAlias=array_search($tableName, $queriedTables);

                if (false===$tableAlias)
                {
                    //this IS a searchable field, but its parent table is not loaded in this context
                    //i.e.: we use CB and this is a searchable CF for "component profile"
                    continue;
                }

                //SELECT the field no matter what
                //$query->select('`'.$tableAlias.'`.`'.$fieldName.'`');

                //no value from request => no SQL filter
                if (empty($searchValue))
                {
                    continue;
                }

                if ($tableAlias) $table_alias=$tableAlias.".";
                else
                    $table_alias=isset($cfg['aliases'][$field->table])?($cfg['aliases'][$field->table]."."):"";

                //$sql=" ".$table_alias.$field->name."=".$db->quote($searchValue);
                $sql=" ".$table_alias.$field->name." LIKE ".$db->quote('%'.$searchValue.'%');
                $query->where($sql);
             }

            foreach ($selectIntegrationFields as $f)
            {
                $fieldName=$profileObject->getFilterField($f);
                $fieldTable=$profileObject->getFilterTable($f);

                $tableAlias=array_search($fieldTable, $queriedTables);

                if (!$fieldTable || !$tableAlias)
                {
                    $query->select('NULL AS '.$f);
                }
                else
                {
                    $query->select('`'.$tableAlias.'`.`'.$fieldName.'` AS '.$f);
                }
            }
        }


    function getTotal()
    {
        if (empty($this->total)) {
            $db = JFactory::getDbo();
            $query = $this->buildQuery();
            $query->set('select', array("count(distinct `u`.id)"));
            $query->set('order', null);
            $query->set('group', null);

            $db->setQuery((string)$query);
            $this->total = $db->loadResult();
        }
        return $this->total;
    }

    function hasApplied($jobid, $userid)
    {

        $db = $this->getDbo();
        $query = JTheFactoryDatabase::getQuery();
        $query->set('select', "count(`a`.id)");
        $query->from('#__jobsfactory_candidates', 'a');
        $query->where("`a`.job_id = $jobid and `a`.userid=$userid ");

        $db->setQuery((string)$query);
        $has_applied = $db->loadResult();

        return $has_applied;
    }

    function uploadFiles(&$user)
    {
        jimport('joomla.filesystem.file');
        $msg = '';

        $db = JFactory::getDBO();
        $cfg = JTheFactoryHelper::getConfig();
        if (!$cfg->enable_images) return; //No image processing

        require_once(JPATH_COMPONENT_SITE . DS . 'thefactory' . DS . 'front.images.php');
        $imgTrans = new JTheFactoryImages();

        $my = JFactory::getUser();
        $delete_pictures = JArrayHelper::getValue($_POST, 'delete_pictures', null);

        if (!count($delete_pictures))
            $delete_pictures = array();

        $delete_main_picture = JFactory::getApplication()->input->getString('delete_main_picture', '');
        $delete_atachment = JFactory::getApplication()->input->getString('delete_atachment', '');

        if (!is_dir(RESUME_PICTURES_PATH))
            @mkdir(RESUME_PICTURES_PATH, 0755);

        $nrfiles = 0;
        foreach ($_FILES as $k => $file) {

            if (substr($k, 0, 7) != "picture")
                continue;

            if (!isset($file['name']) || $file['name'] == "")
                continue;

            if (!is_uploaded_file(@$file['tmp_name'])) {
                continue;
            }

            if (filesize(@$file['tmp_name']) > $cfg->max_picture_size * 1024) {
                continue;
            }

            $fname = $file['name'];

            $ext = JFile::getExt($fname);

            if (!$user->isAllowedImage($ext)) {
                $msg .= JText::_("COM_JOBS_EXTENSION_NOT_ALLOWED") . ': ' . $file['name'];
                continue;
            }

            if ($k == "picture_0") {
                $file_name = "main_{$user->userid}.{$ext}";

                $user->picture = $file_name;
                $user->store();
            }

            $path = RESUME_PICTURES_PATH . "/$file_name";
            $res = move_uploaded_file($file['tmp_name'], $path);

            if ($res) {
                @chmod($path, 0755);

                $s = $imgTrans->resize_image(RESUME_PICTURES_PATH . $file_name, $cfg->thumb_width, $cfg->thumb_height, 'resize');
                if (!$s) {
                    $msg .= $file['name'] . " thumb - " . JText::_('COM_JOBS_ERR_LOADING_PICTURE') . "<br><br>";
                }
                $s = $imgTrans->resize_image(RESUME_PICTURES_PATH . $file_name, $cfg->medium_width, $cfg->medium_height, 'middle');
                if (!$s) {
                    $msg .= $file['name'] . " middle - " . JText::_('COM_JOBS_ERR_LOADING_PICTURE') . "<br><br>";
                }
            } else {
                $msg .= $file['name'] . "- " . JText::_('COM_JOBS_ERR_UPLOAD_FAIL') . "<br><br>";
            }

        }
        return $msg;

    }

    function uploadAtachment($user)
    {

        jimport('joomla.filesystem.file');
        $msg = '';

        if (isset($this->attachment)) {
            $file_name = "{$user->userid}.attach";
            //$file_name = "{$user->id}.attach";
            $path = JOB_UPLOAD_FOLDER . "$file_name";

            if (move_uploaded_file($this->attachment['tmp_name'], $path)) {
                $user->file_name = $this->attachment["name"];
                $user->store();
            }
        }
    }

    function deleteFiles($userdetails)
    {
        $my = JFactory::getUser();
        $database = JFactory::getDBO();
        $app = JFactory::getApplication();

        $delete_main_picture = $app->input->get('delete_main_picture', null);
        $id = (int)$userdetails->id;

        if ($delete_main_picture) {
            $query = "SELECT picture FROM `#__jobsfactory_users` WHERE id='$id'";
            $database->setQuery($query);
            $main_pic = $database->loadResult();

            $query = "UPDATE `#__jobsfactory_users` SET picture = '' where id='$id' ";
            $database->setQuery($query);
            $database->execute();

            @unlink(RESUME_PICTURES . $main_pic);
            @unlink(RESUME_PICTURES . "resize_" . $main_pic);
            @unlink(RESUME_PICTURES . "middle_" . $main_pic);
        }
    }

    function validateSave($item)
    {

        $cfg = JTheFactoryHelper::getConfig();
        $error = array();
        // Title validation
        if ($item->name == "" || !isset($item->name))
            $error[] = JText::_("COM_JOBS_NAME_CAN_NOT_BE_EMPTY");
        // Dates validations
            if (!$item->id) {

                $birth_date = $item->birth_date;

                if (!$birth_date) {
                    $error[] = JText::_("COM_JOBS_ERR_BIRTH_DATE_VALID");
                }

                $year = date("Y", strtotime($birth_date));
                $month = date("m", strtotime($birth_date));
                $day = date("d", strtotime($birth_date));

                if ($year < 1900 && $year > 2200) {
                    $error[] = JText::_("COM_JOBS_ERR_BIRTH_DATE_VALID") . "<br />";
                }
                if ($month < 1 && $month > 12) {
                    $error[] = JText::_("COM_JOBS_ERR_BIRTH_DATE_VALID") . "<br />";
                }
                if ($day < 1 && $day > 31) {
                    $error[] = JText::_("COM_JOBS_ERR_BIRTH_DATE_VALID") . "<br />";
                }

            }

            //ATTACHMENT validations
            if ($cfg->enable_attach || $cfg->uploadresume_option) {
                if (isset($_FILES['attachment']) && $_FILES['attachment']['size'] != 0) {
                     $this->attachment = $_FILES['attachment'];
                }

                if ($this->attachment) {

                    if (is_array($this->attachment)) {
                        $ext = strtolower(JFile::getExt($this->attachment['name']));
                    } else {
                        $ext = strtolower(JFile::getExt($this->attachment));
                    }

                    //if ($cfg->attach_extensions) {
                    if ($cfg->resume_extensions) {
                        $allowed = explode(",", $cfg->resume_extensions);//attach_extensions

                        if (!in_array($ext, $allowed)) {
                            $error [] = JText::_('COM_JOBS_ATTACHED_FILE_EXTENSION_NOT_ALLOWED_USE') . $cfg->resume_extensions;
                            unset($this->attachment);
                        }
                    }

                    if ($cfg->attach_max_size) {
                        if (filesize($this->attachment['tmp_name']) > 1024 * $cfg->attach_max_size) {
                            $error [] = JText::_('COM_JOBS_ATTACHED_FILE_IS_TOO_LARGE_MAXIMUM_ALLOWED') . $cfg->attach_max_size . ' kB';
                            unset($this->attachment);
                        }
                    }
                } elseif ($cfg->attach_compulsory)
                    $error[] = JText::_("COM_JOBS_ERR_ATTACHMENT_COMPULSORY") . "<br />";
            }

            if ($cfg->enable_images && $cfg->main_picture_require) {
                $has_main_picture = false;

                $app = JFactory::getApplication();
                $oldid = $app->input->getInt('resume_id', 0);
                $delete_main_picture = $app->input->get('delete_main_picture',0);

                if ($oldid || $item->id) {
                    //repost or edit existing
                    $oldUser = JTable::getInstance('users', 'Table');
                    $has_main_picture = isset($_FILES['picture_0']) && (is_uploaded_file(@$_FILES['picture_0']['tmp_name']));

                    if (!$delete_main_picture && $oldUser->load($oldid ? $oldid : $item->id)) {
                        $has_main_picture = $has_main_picture || ($oldUser->picture !== '');
                    }
                } else {

                    if (isset($_FILES['picture_0'])) {
                        $file = $_FILES['picture_0'];
                        $has_main_picture = true;

                        if (!is_uploaded_file(@$file['tmp_name'])) {
                            $error[] = $file['name'] . "- " . JText::_("COM_JOBS_ERR_UPLOAD_FAIL");
                            $has_main_picture = false;
                        }

                        if (filesize($file['tmp_name']) > $cfg->max_picture_size * 1024) {
                            $error[] = $file['name'] . "- " . JText::_("COM_JOBS_ERR_IMAGESIZE_TOO_BIG");
                            $has_main_picture = false;
                        }
                        if (isset($file['name']) && $file['name']) {
                            $ext = strtolower(JFile::getExt($file['name']));
                            if (!$item->isAllowedImage($ext)) {
                                $error[] = JText::_("COM_JOBS_EXTENSION_NOT_ALLOWED") . ': ' . $ext;
                                $has_main_picture = false;
                            }
                        } else {
                            $has_main_picture = false;
                        }
                    }
                }

                if (!$has_main_picture)
                    $error[] = JText::_("COM_JOBS_ERR_PICTURE_IS_REQUIRED");
            }

            //Custom Validations
            if (!$item->check())
                $error = array_merge($error, $item->_validation_errors);

            return $error;
        }


  function delete()
  {
    $app = JFactory::getApplication();
    $group_id = $app->input->getInt('id', 0);
    $cid      = $app->get('cid', array(), 'array');
    $member   = $this->getTable('GroupMember');

    JArrayHelper::toInteger($cid);
    $this->group_id = $group_id;

    foreach ($cid as $member_id)
    {
        try
        {
            $member->delete($member_id);
        }
        catch (RuntimeException $e)
        {
        	$app->enqueueMessage($e->getMessage(),'error');
        	return false;
        }
    }

    return true;
  }

}
