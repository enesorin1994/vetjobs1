<?php

/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die;

JLoader::register('BlogFactoryDataProviderInterface', JPATH_SITE . '/components/com_blogfactory/helpers/dataprovider.php');
JLoader::register('BlogFactoryDataProvider', JPATH_SITE . '/components/com_blogfactory/helpers/dataprovider.php');

class JobsFactoryBlogFactoryDataProvider extends BlogFactoryDataProvider implements BlogFactoryDataProviderInterface
{
  public function getItems($userId, $limit = 0, $limitstart = 0, $search = null)
  {
    $dbo = $this->getDbo();
    $items = array();

    $queryForOwnJobs = $this->getQueryForOwnJobs($userId, $search);
    $queryForAppliedJobs = $this->getQueryForAppliedJobs($userId, $search);

    $query = '(' . $queryForOwnJobs . ') UNION (' . $queryForAppliedJobs . ')';

    $results = $dbo->setQuery($query, $limitstart, $limit)
      ->loadObjectList();

    foreach ($results as $result) {
      $items[] = (object)array(
        'title'       => $result->title,
        'description' => $result->description,
        'link'        => JRoute::_('index.php?option=com_jobsfactory&task=viewapplications&id=' . $result->id, false),
      );
    }

    return $items;
  }

  public function getTotal($userId, $search = null)
  {
    $dbo = $this->getDbo();

    $queryForOwnJobs = $this->getQueryForOwnJobs($userId, $search, true);
    $queryForAppliedJobs = $this->getQueryForAppliedJobs($userId, $search, true);

    $totalForOwnJobs = $dbo->setQuery($queryForOwnJobs)
      ->loadResult();

    $totalForAppliedJobs = $dbo->setQuery($queryForAppliedJobs)
      ->loadResult();

    return $totalForOwnJobs + $totalForAppliedJobs;
  }

  protected function getQueryForOwnJobs($userId, $search = null, $total = false)
  {
    $dbo = $this->getDbo();

    $query = $dbo->getQuery(true)
      ->from('#__jobsfactory_jobs j')
      ->where('j.userid = ' . $dbo->quote($userId));

    if ($total) {
      $query->select('COUNT(j.id) AS total');
    }
    else {
      $query->select('j.id, j.title, j.shortdescription, j.description');
    }

    if (!is_null($search)) {
      $query->where('j.title LIKE ' . $dbo->quote('%' . $search . '%'));
    }

    return (string)$query;
  }

  protected function getQueryForAppliedJobs($userId, $search = null, $total = false)
  {
    $dbo = $this->getDbo();

    $query = $this->getDbo()->getQuery(true)
      ->from('#__jobsfactory_candidates c')
      ->leftJoin('#__jobsfactory_jobs j ON j.id = c.job_id')
      ->where('c.userid = ' . $dbo->quote($userId));

    if ($total) {
      $query->select('COUNT(j.id) AS total');
    }
    else {
      $query->select('j.id, j.title, j.shortdescription, j.description');
    }

    if (!is_null($search)) {
      $query->where('j.title LIKE ' . $dbo->quote('%' . $search . '%'));
    }

    return (string)$query;
  }
}
