<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

class userexperienceModelUserexperience extends JModelLegacy
{

    var $_name = 'userexperience';
    var $name = 'userexperience';

    function saveUserexperience($userexperience)
    {
        $isNew = !(bool)$userexperience->id;

        $error = $this->validateSave($userexperience);
        if (count($error) > 0) return $error;

        if (!$e = $userexperience->store()) {
            $error[] = $e;
            return $error;
        }

        return $error;
    }

    function bindUserexperience(&$userexperience)
    {
        $cfg = JTheFactoryHelper::getConfig();
        $date = new JDate();
        $app = JFactory::getApplication();

        $userexperience->id = $app->input->getInt('id', null);

        if ($userexperience->id) {
            $userexperience->load($userexperience->id);
        }
        $posted_val = $app->input->getArray($_POST);;
        $userexperience->bind($posted_val);

        if (isset($posted_val['exp_start_date']) && isset($posted_val['exp_end_date']) ) {
            $userexperience->start_date = JobsHelperDateTime::DateToIso($posted_val['exp_start_date']);
            $userexperience->end_date = JobsHelperDateTime::DateToIso($posted_val['exp_end_date']);

            $userexperience->start_date = JobsHelperDateTime::isoDateToUTC($userexperience->start_date); //store all in UTC
            $userexperience->end_date = JobsHelperDateTime::isoDateToUTC($userexperience->end_date); //store all in UTC
        }

        $user = JFactory::getUser();
        $userexperience->userid = $user->id;
        $userexperience->exp_description = $app->input->getHtml('exp_description', '');//??

        if ($userexperience->id) {
            // edit custom field prep
            $old_exp = JTable::getInstance('userexperience', 'Table');
            $old_exp->load($userexperience->id);
            // do not edit these fields
            $userexperience->start_date = JobsHelperDateTime::DateToIso($posted_val['start_date'.$userexperience->id]); //start_date22
            $userexperience->end_date = JobsHelperDateTime::DateToIso($posted_val['end_date'.$userexperience->id]);

            $userexperience->start_date = JobsHelperDateTime::isoDateToUTC($userexperience->start_date); //store all in UTC
            $userexperience->end_date = JobsHelperDateTime::isoDateToUTC($userexperience->end_date); //store all in UTC
        }
    }

    public function deleteUserexperience($experience_id,$userid)
    {
        $database = JFactory::getDbo();
        $user = JFactory::getUser();

        if (($user->id == $userid) && $experience_id != "") {
            $database->setQuery("DELETE FROM `#__jobsfactory_userexperience` WHERE `userid` = '" . $user->id .
                "' AND `id` = '" . (int)$experience_id . "'");
            $database->execute();
        }
        return $database->getAffectedRows();
    }

    function validateSave($item)
    {

        $cfg = JTheFactoryHelper::getConfig();
        $error = array();
        // Title validation
        if ($item->position == "" || !isset($item->position))
            $error[] = JText::_("COM_JOBS_POSITION_CAN_NOT_BE_EMPTY");

        // Dates validations
        if (!$item->id) {

            $start_date = $item->start_date;
            $end_date = $item->end_date;

            if (!$start_date) {
                $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID");
            }
            if (!$end_date) {
                $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID");
            }

            $year       = date("Y", strtotime($start_date));
            $month      = date("m", strtotime($start_date));
            $day        = date("d", strtotime($start_date));
            $year_end   = date("Y", strtotime($end_date));
            $month_end  = date("m", strtotime($end_date));
            $day_end    = date("d", strtotime($end_date));

            if ($year < 1900 && $year > date('Y')) {
                $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID") . "<br />";
            }
            if ($month < 1 && $month > 12) {
                $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID") . "<br />";
            }
            if ($day < 1 && $day > 31) {
                $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID") . "<br />";
            }

            if ($year_end < 1900 && $year_end > date('Y')) {
                $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID") . "<br />";
            }
            if ($month_end < 1 && $month_end > 12) {
                $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID") . "<br />";
            }
            if ($day_end < 1 && $day_end > 31) {
                $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID") . "<br />";
            }

            $datedif = mktime(0, 0, 0, $month_end, $day_end, $year_end) - mktime(0, 0, 0, $month, $day, $year);
            if ($datedif < 0) {
                $error[] = JText::_("COM_JOBS_ERR_END_BIGGER_THAN_START") . "<br />";
            }

            $datedif_Start = mktime(0, 0, 0, date("m"), date("d"), date("Y")) - mktime(0, 0, 0, $month, $day, $year);
            if ($datedif_Start < 0) {
                $error[] = JText::_("COM_JOBS_ERR_START_BIGGER_THAN_NOW") . "<br />";
            }

            $datedif_End = mktime(0, 0, 0, date("m"), date("d"), date("Y")) - mktime(0, 0, 0, $month_end, $day_end, $year_end);
            if ($datedif_End < 0) {
                $error[] = JText::_("COM_JOBS_ERR_END_BIGGER_THAN_NOW") . "<br />";
            }
        }

        //Custom Validations
        if (!$item->check())
            $error = array_merge($error, $item->_validation_errors);


        return $error;
    }

    function getNrFieldsWithFilters()
    {
        $database = JFactory::getDbo();
        $database->setQuery("select count(*) from #__jobsfactory_fields where page='resumes' and categoryfilter=1");
        return $database->loadResult();
    }


}
