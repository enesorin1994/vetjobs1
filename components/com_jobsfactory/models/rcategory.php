<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

/**
 * @package        Jobs
 */
class jobsModelRCategory extends JModelLegacy
{
    var $_name = 'category';
    var $name = 'category';

    function getJobsCategoryTree($root_category = 0, $max_depth = null, $watchlist_user = null, $filter_letter = 'all')
    {
        $db = JFactory::getDbo();
        $config = JFactory::getConfig();
        $datenow = JFactory::getDate('now', $config->get('config.offset'));
        $watchlist_fields = "";
        $watchlist_join = "";

        if ($watchlist_user) {
            $watchlist_fields = ",count(w.id) as watchListed_flag ";
            $watchlist_join = "left join `#__jobsfactory_watchlist_cats` w on w.catid=c.id and w.userid='{$watchlist_user} '";
        }
        if ('all' != $filter_letter) {
            $filter_letter = " AND `c`.`catname` LIKE '" . trim($filter_letter) . "%' ";
        } else {
            $filter_letter = ' ';
        }
        $query = "SELECT c.*,
                    count(distinct a.id) as nr_a,
                    count(c2.id) as nr_subcats,
                    null as `subcategories`
                    {$watchlist_fields}
                  FROM `#__jobsfactory_categories` c
                        LEFT JOIN `#__jobsfactory_categories` c2 on c.id=c2.parent
                        LEFT JOIN `#__jobsfactory_jobs` a on c.id=a.cat
                                                        AND `a`.`close_by_admin` <> 1
                                                        AND `a`.`close_offer` = 0
                                                        AND `a`.`published` = 1
                                                        AND `a`.`start_date` <= '" . $datenow->toSql(false) . "'
                        {$watchlist_join}
                  WHERE c.`status`=1 and c.`parent`='{$root_category}'
                  {$filter_letter}
                  GROUP BY c.id
                  ORDER BY c.ordering
            ";
        $db->setQuery($query);
        $items = $db->loadObjectList();

        if ($max_depth === NULL || $max_depth)
            for ($i = 0; $i < count($items); $i++)
                if ($items[$i]->nr_subcats)
                    $items[$i]->subcategories = $this->getJobsCategoryTree($items[$i]->id, ($max_depth === NULL) ? $max_depth : ($max_depth - 1), $watchlist_user);

        return $items;
    }


    // CATEGORIES WATCHLIST (favoriteS)
    function addWatch($id)
    {
        $database = JFactory::getDBO();
        $user = JFactory::getUser();
        if ($user->id && $id != "") {
            $database->setQuery("INSERT INTO #__jobsfactory_watchlist_cats SET userid = '" . $user->id .
                "',catid='" . (int)$id . "'");
            $database->execute();
        }
        return $database->getAffectedRows();
        //return $database->affected_rows;
    }

    function delWatch($id)
    {
        $database = JFactory::getDBO();
        $user = JFactory::getUser();
        if ($user->id && $id != "") {
            $database->setQuery("DELETE FROM #__jobsfactory_watchlist_cats WHERE userid = '" . $user->id .
                "' AND catid='" . (int)$id . "'");
            $database->execute();
        }
        return $database->getAffectedRows();
    }

}
