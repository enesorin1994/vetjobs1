<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

/**
 * @package        Jobs
 */
class jobsModelCity extends JModelLegacy
{
    var $_name = 'city';
    var $name = 'city';

    function getJobsCityTree($root_location = 1, $max_depth = null, $watchlist_user = null,$main=true)
    {
        $db         = JFactory::getDbo();
        $config     = JFactory::getConfig();
        $datenow    = JFactory::getDate('now', $config->get('config.offset'));
        $watchlist_fields = "";
        $watchlist_join = "";
        $rootcondition = ($root_location != 0) ? " and c2.`location`='{$root_location}' " : '';

        if ($main)
            $idcondition = ($root_location != 0) ? "and c.`id`='{$root_location}' " : '';
        else
            $idcondition = ($root_location != 0) ? "and c2.`id`='{$root_location}' " : '';

        if ($watchlist_user) {
            $watchlist_fields_city = ",count(cw.id) as watchListed_city_flag ";
            $watchlist_join_city = "left join `#__jobsfactory_watchlist_city` cw on cw.cityid=c2.id and cw.userid='{$watchlist_user}'";
        }

        $query = "SELECT c2.id, c2.cityname, c2.location
                    {$watchlist_fields_city}
                    FROM  `#__jobsfactory_cities` c2
                        left join `#__jobsfactory_locations` c on c.id=c2.location
                        {$watchlist_join_city}
                        where c2.`status`=1
                        group by c2.id
                        order by c2.ordering, c2.cityname ";

        $db->setQuery($query);
        $items = $db->loadObjectList('id');

        return $items;
    }

    function getJobsCityList($max_depth = null, $watchlist_user = null,$main=true) {
        $db         = JFactory::getDbo();
        $config     = JFactory::getConfig();
        $datenow    = JFactory::getDate('now', $config->get('config.offset'));
        $watchlist_fields = "";
        $watchlist_join = "";

        $query = "SELECT j.id, j.job_cityname, count(j.id) as jobs_no
                    FROM  `#__jobsfactory_jobs` j
                        where j.`published`=1
                        group by j.job_cityname
                        order by j.id ";

        $db->setQuery($query);
        $items = $db->loadObjectList();

        return $items;
    }


    // CITIES WATCHLIST (favoriteS)
    function addWatch($id)
    {
        $database = JFactory::getDBO();
        $user = JFactory::getUser();

        if ($user->id && $id != "") {
            $database->setQuery("INSERT INTO #__jobsfactory_watchlist_city SET userid = '" . $user->id .
                "',cityid='" . (int)$id . "'");
            $database->execute();
        }

        return $database->getAffectedRows();
    }

    function delWatch($id)
    {
        $database = JFactory::getDBO();
        $user = JFactory::getUser();

        if ($user->id && $id != "") {
            $database->setQuery("DELETE FROM #__jobsfactory_watchlist_city WHERE userid = '" . $user->id .
                "' AND cityid='" . (int)$id . "'");
            $database->execute();
        }
        return $database->getAffectedRows();
    }


}
