<?php
    /**------------------------------------------------------------------------
    com_jobsfactory - Jobs Factory 1.6.5
    ------------------------------------------------------------------------
     * @author thePHPfactory
     * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
     * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     * Websites: http://www.thePHPfactory.com
     * Technical Support: Forum - http://www.thePHPfactory.com/forum/
     * @package: Jobs
    -------------------------------------------------------------------------*/

    defined('_JEXEC') or die('Restricted access');

    class jobsModelCandidates extends jobsModelGeneric
    {
        var $_name = 'Candidates';
        var $name = 'Candidates';
        var $page = 'candidates';
        var $context = 'candidatesModel';

        var $order_fields = array(
            'start_date'      => "j.start_date",
            'title'           => "j.title",
            'username'        => "u.username",
            'end_date'        => "j.end_date",
            'function'		  => "j.title",
            'desired_salary'  => "prof.desired_salary",
            'gender'          => "prof.gender",
            'birth_date'      => "prof.birth_date",
            'hits'            => "j.hits",
            'catname'         => "cat.catname",
            'id'              => "c.id"

        );
        var $knownFilters = array(
            'keyword'          => array('type' => 'string'),
            'in_description'   => array('type' => 'int'),
            'filter_candidates'=> array('type' => 'string'),
            'filter_myjobs'    => array('type' => 'string'),
            'userid'           => array('type' => 'int'),
            'cat'              => array('type' => 'int'),
            'tag'              => array('type' => 'string'),
            'city'             => array('type' => 'string'),
            'country'          => array('type' => 'string'),
            'afterd'           => array('type' => 'string'),
            'befored'          => array('type' => 'string'),
            'function'         => array('type' => 'string'),
            'desired_salary'    => array('type' => 'string'),
            'birth_date'        => array('type' => 'string'),
            'gender'            => array('type' => 'string'),
            'nr_applicants'     => array('type' => 'int'),
            'filter_order'     => array('type' => 'string', 'default' => 'start_date'),
            'filter_order_Dir' => array('type' => 'string', 'default' => 'DESC')
        );

        function buildQuery()
        {
            $user   = JFactory::getUser();
            $db     = JFactory::getDBO();
            $cfg    = JTheFactoryHelper::getConfig();
            $config = JFactory::getConfig();
            $app    = JFactory::getApplication();

            $datenow = JFactory::getDate('now', $config->get('config.offset'));
            $task   = $app->input->getCmd('task');
            $job_id = $app->input->getInt('id',0);

             //this binds to the query object everything that is related to custom fields
            $profile = JobsHelperTools::getUserProfileObject();
            $tableName = $profile->getIntegrationTable();
            $tableKey = $profile->getIntegrationKey();
            $integration = $profile->getIntegrationArray(); //fieldmap
            $fieldName = $profile->getFilterField('name');

            $query = JTheFactoryDatabase::getQuery();
            $query->from($tableName, 'prof');  //user profile table

            //$query->from('#__jobsfactory_candidates', 'c');

            /**
             *
             *  Fields to select
             *
             * */
            $query->select('`prof`.*');
            $query->select('prof.`'.$tableKey.'` as candidateId ');
            $query->select('`c`.*');
            $query->select('`cat`.`id` as cati, `cat`.`catname`');
            $query->select('`j`.`title`');
            $query->select('`u`.`username`');

            /**
             *
             *  Where conditions
             *
             * */

            switch ($task) {
                //live jobs
                default:
                case 'listcandidates':

                    $fieldisVisible = $profile->getFilterField('isVisible');
                    $fieldisCompany = $profile->getFilterField('isCompany');

                    $query->where("`prof`.$fieldisVisible = 1");
                    $query->where("`prof`.$fieldisCompany = 0");
                    //$query->where("`fu`.`isVisible` = 1");
                    break;
                case 'companylistcandidates':
                    $query->where(array("`cat`.`status`=1", "`cat`.`status` IS NULL"), "OR");
                    if ($job_id) {
                        $query->where("`c`.`job_id` =  $job_id ");
                    }

                    if ($this->getState('filters.filter_candidates')) {
                        //Rejected candidates
                        if ($this->getState('filters.filter_candidates') == 'rejected') {
                            $query->where("`c`.`action` = 3");
                        }
                        if ($this->getState('filters.filter_candidates') == 'call') {
                            $query->where("`c`.`action` = 1");
                        }
                        if ($this->getState('filters.filter_candidates') == 'analyze') {
                            $query->where("`c`.`action` = 2");
                        }
                    }
                    else
                        $query->where(array("`c`.`action` = 1", "`c`.`action` = 2",  "`c`.`action`= '' "), "OR");
                    break;

                case 'watchlist':
                    $query->where(array("`cat`.`status`=1", "`cat`.`status` IS NULL"), "OR");
                    $query->where("`fav_table`.userid ='" . $user->id . "'");
                    $query->where("`j`.`start_date` <= '" . $datenow->toSql(false) . "'");
                    break;

                case 'myapplications':
                    $query->where("`c`.`userid` = '" . $user->id . "'");
                    break;
            }

            if ($this->getState('filters.keyword')) {

                $keyword = $db->escape($this->getState('filters.keyword'));

                $w = array();
                $w[] = 'j.title LIKE \'%' . $keyword . '%\'';
                if ($this->getState('filters.indesc')) {
                    $w[] = 'j.shortdescription LIKE \'%' . $keyword . '%\'';
                    $w[] = 'j.description LIKE \'%' . $keyword . '%\'';
                }
                $query->where($w, 'OR');
            }

            if ($this->getState('filters.userid')) {
                $query->where(" prof.$tableKey = '" . $db->escape($this->getState('filters.userid')) . "' ");
            }

            if ($this->getState('filters.cat')) {
                if (!$cfg->inner_categories) {
                    $query->where(" j.cat= '" . $db->escape($this->getState('filters.cat')) . "' ");
                } else {
                    JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'thefactory' . DS . 'category' . DS . 'models');
                    $catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');
                    $cat_ids = $catModel->getCategoryChildren((int)$this->getState('filters.cat'));
                    $cat_ids [] = (int)$this->getState('filters.cat');

                    if (count($cat_ids)) {
                        $cat_ids = implode(",", $cat_ids);
                        $query->where(' j.cat  IN (' . $cat_ids . ') ');
                    }
                }
            }

            if ($this->getState('filters.afterd')) {
                $start_date = JobsHelperDateTime::DateToIso($this->getState('filters.afterd'));
                $start_date = JFactory::getDate($start_date, $config->get('config.offset'))->toSql(false);
                $query->where(' j.start_date>=\'' . $start_date . '\'');
            }

            if ($this->getState('filters.befored')) {
                $before_date = JobsHelperDateTime::DateToIso($this->getState('filters.befored'));
                $before_date = JFactory::getDate($start_date, $config->get('config.offset'))->toSql(false);
                $query->where('j.end_date>=\'' . $before_date . '\'');
            }

            /**
             *  Joins
             * */
            $query->join('left', '#__jobsfactory_candidates', 'c', '`prof`.`'.$tableKey.'`=`c`.`userid`');
            $query->join('left', '#__jobsfactory_jobs', 'j', '`j`.`id`=`c`.`job_id` and `c`.`cancel`=0 ' . (($task == 'myapplications') ? "AND `c`.`userid` = '{$user->id}'" : ""));
            $query->join('left','#__users','u', '`prof`.`'.$tableKey.'` = `u`.`id`');

            $query->join('left', '#__jobsfactory_categories', 'cat', '`j`.`cat`=`cat`.`id`');

            // Required ordering filter
            $filter_order = $this->getState('filters.filter_order');
            if ($filter_order) {
                $filter_order_Dir = $this->getState('filters.filter_order_Dir');
                $query->order($db->escape($this->order_fields[$filter_order] . ' ' . $filter_order_Dir));
            }

            //this binds to the query object everything that is related to custom fields
            $queriedTables = $query->getQueriedTables();
            if ($this->getState('filters.country')) {
                $table = $profile->getFilterTable('country');
                $field = $profile->getFilterField('country');

                $alias = array_search($table, $queriedTables);
                $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.country')) . '\') ');
            }

            if ($this->getState('filters.city')) {
                $table = $profile->getFilterTable('city');
                $field = $profile->getFilterField('city');
                $alias = array_search($table, $queriedTables);
                $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.city')) . '\') ');
            }

            if ($task != 'companylistcandidates') {
                $query->group('`prof`.`'.$tableKey.'`');
            }

            return $query;
        }

        function getTotal()
        {

            if (empty($this->total)) {
                $db = JFactory::getDbo();
                $query = $this->buildQuery();

                $query->set('select', "count(distinct `c`.userid)");
                $query->set('order', null);
                $query->set('group', null);

                $db->setQuery((string)$query);
                $this->total = $db->loadResult();

            }
            return $this->total;
        }

        function getCompanyJobsList($id)
        {
            $db = JFactory::getDbo();
            $query = "SELECT j.*, a.userid, a.comments, c.catname "
                . " ,IF ( j.show_candidate_nr = 1 , COUNT(DISTINCT a.userid) ,0) as nr_applicants "
                . " FROM #__jobsfactory_jobs j "
                . " LEFT JOIN #__jobsfactory_candidates a ON j.id = a.job_id  "
                . " LEFT JOIN #__jobsfactory_categories c ON j.cat = c.id "
                . " WHERE j.userid = '{$id}' "
                . " LIMIT 0,10 ";

            $db->setQuery($query);
            return $db->loadObjectList();
        }


    }

?>
