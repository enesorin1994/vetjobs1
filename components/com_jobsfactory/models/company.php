<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class jobsModelCompany extends jobsModelGeneric
{
    var $_name = 'Company';
    var $name = 'Company';
    var $page = 'company_profile';
    var $context = 'userModel';
    var $knownFilters = array(
        'keyword' => array('type' => 'string'),
        'country' => array('type' => 'string'),
        'activitydomain' => array('int' => 'activitydomain'),
        'city' => array('type' => 'string')
    );


    function getCompanyData($id)
    {
        $company = JobsHelperTools::getUserProfileObject();
        $company->getUserProfile($id);
        return clone $company;
    }

    function bindCompanyDetails( &$company ){
        $cfg = JTheFactoryHelper::getConfig();
        $my = JFactory::getUser();
        $app = JFactory::getApplication();

        $company->userid = $app->input->getInt('userid', null); //ID from the #__jobsfactory_users
        $companyId = (isset($company->userid)) ? $company->userid : $my->id;
        //$company->load($companyId);

        $company->load(array('userid'=>$companyId));

        $f = new JFilterInput(array(),array(),1,1);

        $company->bind(JTheFactoryHelper::getRequestArray('post')); //,array('id')

        //$company->userid = $my->id;
        $company->userid = $companyId;
        $company->name = $app->input->getString('name');
        $company->website = $app->input->getHtml("website");
        $company->shortdescription = $app->input->get('shortdescription', '', 'string');
        $company->about_us = $f->clean($_REQUEST['about_us'],"html");

        if ($company->userid) {
        // edit custom field prep
            if ($company->useruniqueid == '') {
                $company->useruniqueid = uniqid(); //microtime().
            }

            if (!$cfg->employervisibility_enable)
                $company->isVisible=$cfg->employervisibility_val;

            $company->modified = gmdate('Y-m-d H:i:s');
        } else {
            // new custom field prep
            $company->modified = gmdate('Y-m-d H:i:s');
            $company->useruniqueid = uniqid(); //microtime().

            if (!$cfg->employervisibility_enable)
                $company->isVisible=$cfg->employervisibility_val;
        }
    }

    function saveCompanyDetails($companydetails)
    {
        $my = JFactory::getUser();
        $db = JFactory::getDbo();
        $app = JFactory::getApplication();
        $cat_filter = $app->input->get('activitydomain', array(), 'array');

        $company = JTable::getInstance('users', 'Table');

        if (!$company->load(array('userid'=>$my->id))) {
            $db->setQuery("INSERT INTO " . $company->getTableName() . "(`userid`) VALUES ({$my->id})");
            $db->execute();
        }

        $error = 0;
        $error = $this->validateSave($companydetails);

        if (count($error) > 0) return $error;

        if (!$e = $companydetails->store()) {
            $error[] = $e;
            return $error;
        }

        //$company->userid = $my->id;
        $this->setAssignedCats($companydetails, $cat_filter);

        // UPLOAD FILES
        $msg = $this->uploadFiles($companydetails);

        if(strlen($msg)){
            $error[] = $msg;
        }

        return $error;
    }

    function setActivityDomain(&$company,$activity_domain)
    {
        if (!count($activity_domain)) return; //no experiences assigned
        // add category assignemtns
        $inserts = array();
        foreach ($activity_domain as $expid){
            $inserts[] = "{$expid}";
        }

        if (count($inserts))
        {
            return implode(",",$inserts);
        } else {
            return '(1)';
        }
    }

    function getCompanyCountries()
    {
        $db = JFactory::getDbo();
        $query = JTheFactoryDatabase::getQuery();

        $profile = JobsHelperTools::getUserProfileObject();
        $field = $profile->getFilterField('country');
        $table = $profile->getFilterTable('country');

        $query->select("distinct `{$field}` country");
        $query->from($table);
        $query->where("`{$field}`<>'' and `{$field}` is not null");
        $db->setQuery((string)$query);

        return $db->loadObjectList();
    }

    function buildQuery()
    {
        $db = JFactory::getDbo();
        $task = JFactory::getApplication()->input->getCmd('task','showCompanies');

        $profile = JobsHelperTools::getUserProfileObject();
        $tableName  = $profile->getIntegrationTable();

        $query = JTheFactoryDatabase::getQuery();
        $query->select('`u`.*,`u`.`id` AS userid');
        $query->select('null AS password');

        $query->select('COUNT(DISTINCT j.id) AS nr_jobs');
        $query->from('#__users', 'u');
        $query->join('left', '#__jobsfactory_jobs', 'j', '`u`.`id`=`j`.`userid` and j.`published`=1');

        if ($profile && $profile->profile_mode == 'cb') {
            $this->setCustomFiltersCB($profile);
            $this->buildCustomQueryCB($query, $profile, null, $profile->getProfileFields());
        } else {
            parent::buildCustomQuery($query, $profile, null, $profile->getProfileFields());
        }

        $queriedTables = $query->getQueriedTables();

        if ($profile) {

            $table = $profile->getFilterTable('name');
            $integration_name = $profile->getFilterField('name');
            $alias = array_search($table, $queriedTables);

            $query->select(' `'.$alias.'`.`'.$integration_name.'` AS companyName');
        } else {
            $query->select('u.name AS companyName');
        }

        //all the filters are profile related; in order to work, we have to append them to the query AFTER the join has been made with the profile tables, in parent::buildCustomQuery()
        $keyword = $db->escape($this->getState('filters.keyword'));
        $name = $db->escape($this->getState('filters.name'));

        $k = $keyword ? $keyword : ($name ? $name : null);
        if ($k) {
            $s = array();
            $searchTerms = explode(" ", $k); // Split the words

            if (!empty($searchTerms)) {
                foreach ($searchTerms as $searchTerm) {

                    $table = $profile->getFilterTable('name');
                    $field = $profile->getFilterField('name');

                    if ($profile) {

                        if ($profile->profile_mode == 'cb') {

                            $aliasName = array_search($table, $queriedTables); //u
                            $aliasName = ($aliasName != '') ? $aliasName : 'u';
                            $s[] = ' (`' . $aliasName . '`.`' . $field . '` LIKE \'%' . $db->escape($searchTerm) . '%\') ';

                        } else {
                            //$tabletest = $profile->getFilterTable('name');
                            $aliasName = array_search($table, $queriedTables); //u
                            $aliasName = ($aliasName != '') ? $aliasName : 'u';
                            $s[] = ' (`' . $aliasName . '`.`' . $field . '` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    //else {
                            //$s[] = ' (`u`.`name` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    //}
                        }
                    }
                    else {
                            $s[] = ' (`u`.`name` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                    }

                    $table = $profile->getFilterTable('username');
                    $field = $profile->getFilterField('username');

                    $aliasUsername = array_search($table, $queriedTables); //u
                    $aliasUsername = ($aliasUsername != '') ? $aliasUsername : 'u';
                    $s[] = ' (`' . $aliasUsername . '`.`' . $field . '` LIKE \'%' . $db->escape($searchTerm) . '%\') ';
                }

                $query->where($s, 'OR');
            }
        }

        if ($this->getState('filters.country')) {
            $table = $profile->getFilterTable('country');
            $field = $profile->getFilterField('country');
            $alias = array_search($table, $queriedTables);

            if ($profile->profile_mode == 'cb') {
                $countrytable = JTable::getInstance('country', 'Table');
                $country      = $countrytable->getCountryName($this->getState('filters.country'));
                $query->where(' (`' . $alias . '`.`' . $field . '` LIKE  \'%' . $db->escape($country) . '%\') ');
               // $this->getState('filters.country')
            } else {
                $country = $this->getState('filters.country');
                $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($country) . '\') ');
            }
        }

        if ($this->getState('filters.city')) {
            $table = $profile->getFilterTable('city');
            $field = $profile->getFilterField('city');

            $alias = array_search($table, $queriedTables);
            $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.city')) . '\') ');
        }

        if ($this->getState('filters.activitydomain')) {

            if ($profile->profile_mode == 'cb') {
                $table = $profile->getFilterTable('activitydomain');
                $field = $profile->getFilterField('activitydomain');

                $alias = array_search($table, $queriedTables);
                $query->where(' (`' . $alias . '`.`' . $field . '` LIKE  \'%' . $db->escape($this->getState('filters.activitydomain')) . '%\') ');
                //$query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.activitydomain')) . '\') ');
            } else {
                $domain_id = $db->escape($this->getState('filters.activitydomain'));

                $db->setQuery("SELECT company_id FROM `#__jobsfactory_companies_categories` WHERE `cid`='{$domain_id}' AND `company_id`!= 0 ");
                $company_ids = $db->loadColumn();

                if (!empty($company_ids)) {
                    $company_ids = implode(',', $company_ids);
                    $query->where(' ui.id  IN (' . $company_ids . ') ');
                }
            }
        }

        $cfg = JTheFactoryHelper::getConfig();
        $tableKey = $profile->getIntegrationKey();
        $alias = array_search($tableName, $queriedTables);
        $companiesGroup = implode(',',$cfg->company_groups);

        if ($task == 'showCompanies' && $profile) {

            $query->join('left', '#__user_usergroup_map', 'm', "`$alias`.`$tableKey` =`m`.`user_id` ");
            $query->where('m.group_id IN ('."$companiesGroup".')');

            $table = $profile->getFilterTable('isVisible');
            $field = $profile->getFilterField('isVisible');
            $alias = array_search($table, $queriedTables);

            $query->where(' (`' . $alias . '`.`' . $field . '` = 1) ');
        }

        $filter_order = $this->getState('filters.filter_order');
        if ($filter_order) {
            $filter_order_Dir = $this->getState('filters.filter_order_Dir');
            $query->order($db->escape($filter_order . ' ' . $filter_order_Dir));
        }

        $query->group('`u`.`id`');

        return $query;
    }

    function setCustomFiltersCB($profile=null)
    {
       $app=JFactory::getApplication();
       $db=JFactory::getDbo();

       $queryCB		=	'SELECT `name`, `table` '
                        .	"\n FROM `#__comprofiler_fields` "
                        .	"\n WHERE profile=1 AND published=1 AND searchable = 1 ORDER BY `ordering` ";
       $db->setQuery( $queryCB );
       $searchableFields=$db->loadObjectList();

       foreach ($searchableFields as $field)
       {
           //TO DO: add 3rd and 4th params: default, type
           $requestKey='company_profile'.'%'.$field->name;
           $value=$app->getUserStateFromRequest($this->context.'.filters.'.$requestKey, $requestKey);

           if (!empty($value))
               $this->setState('filters.'.$requestKey, $value);
       }
    }

    function buildCustomQueryCB(&$query, $profileObject=null, $useridField=null, $selectIntegrationFields=array())
    {
        $queriedTables=$query->getQueriedTables();
        $cfg = JTheFactoryHelper::getConfig();
        $db = JFactory::getDbo();

        $queryCB		=	'SELECT `name`, `table` '
        						.	"\n FROM `#__comprofiler_fields` "
                                .	"\n WHERE profile=1 AND published=1 AND searchable = 1 ORDER BY `ordering` ";
        $db->setQuery( $queryCB );
        $searchableFields=$db->loadObjectList();

        /*38 =>
            object(stdClass)[440]
              public 'name' => string 'cb_county' (length=9)
              public 'table' => string '#__comprofiler' (length=14)*/

        if ($profileObject)
        {
            //first look for Joomla's users table; join it only if not queried
            $usersAlias=array_search('#__users', $queriedTables);
            if (false===$usersAlias)
            {
                if (!$useridField)
                {
                    JFactory::getApplication()->enqueueMessage('When using profile mode, the USERID foreign key is required!','error');
                    return;
                }
                $query->join('left', '`#__users`', '`u`', $useridField.'=`u`.`id`');
                $usersAlias='u';
            }

            //join the extended profile table
            $tableName = $profileObject->getIntegrationTable();
            $tableKey = $profileObject->getIntegrationKey();
            $query->join('left', ''.$tableName.'', 'ui', '`'.$usersAlias.'`.`id`=`ui`.`'.$tableKey.'`');

            //the listing includes some profile filters
            //$this->pushProfileIntegrationFields($searchableFields, $profileObject);

            //we need to "refresh" this because of the previous joins
            $queriedTables=$query->getQueriedTables();
        }

        foreach ($searchableFields as $field)
        {

            $requestKey='company_profile'.'%'.$field->name;

            $searchValue=$this->getState('filters.'.$requestKey);

            $fieldName=$field->name;
            $tableName=$field->table;
            $tableAlias=array_search($tableName, $queriedTables);

            if (false===$tableAlias)
            {
                //this IS a searchable field, but its parent table is not loaded in this context
                //i.e.: we use CB and this is a searchable CF for "component profile"
                continue;
            }

            //SELECT the field no matter what
            //$query->select('`'.$tableAlias.'`.`'.$fieldName.'`');

            //no value from request => no SQL filter
            if (empty($searchValue))
            {
                continue;
            }

            //$ftype=CustomFieldsFactory::getFieldType($field->ftype?$field->ftype:'inputbox');

            if ($tableAlias) $table_alias=$tableAlias.".";
            else
                $table_alias=isset($cfg['aliases'][$field->table])?($cfg['aliases'][$field->table]."."):"";

            //$sql=" ".$table_alias.$field->name."=".$db->quote($searchValue);
            $sql=" ".$table_alias.$field->name." LIKE ".$db->quote('%'.$searchValue.'%');

            $query->where($sql);
           // $query->where($ftype->getSQLFilter($field, $searchValue, $tableAlias));
        }

        foreach ($selectIntegrationFields as $f)
        {
            $fieldName=$profileObject->getFilterField($f);
            $fieldTable=$profileObject->getFilterTable($f);

            $tableAlias=array_search($fieldTable, $queriedTables);

            if (!$fieldTable || !$tableAlias)
            {
                $query->select('NULL AS '.$f);
            }
            else
            {
                $query->select('`'.$tableAlias.'`.`'.$fieldName.'` AS '.$f);
            }
        }

    }

    function getTotal()
    {
        if (empty($this->total)) {
            $db = JFactory::getDbo();
            $query = $this->buildQuery();
            $query->set('select', array("count(distinct `u`.id)"));
            $query->set('order', null);
            $query->set('group', null);

            $db->setQuery((string)$query);
            $this->total = $db->loadResult();
        }
        return $this->total;
    }

    function hasApplied($jobid, $userid)
    {

        $db = JFactory::getDbo();
        $query = JTheFactoryDatabase::getQuery();
        $query->set('select', "count(`a`.id)");
        $query->from('#__jobsfactory_candidates', 'a');

        $query->where("`a`.job_id = $jobid and `a`.userid=$userid ");
        $db->setQuery((string)$query);

        $has_applied = $db->loadResult();

        return $has_applied;
    }

    function getAssignedCats(&$company)
    {
        $db = JFactory::getDbo();
        $companyId = (isset($company->profileId)) ? $company->profileId : $company->id;

        $db->setQuery("SELECT cid FROM `#__jobsfactory_companies_categories` where `company_id`='{$companyId}'");
        return $db->loadColumn();
    }

    function setAssignedCats(&$company,$catids)
    {
        $db = JFactory::getDbo();;
        // delete category assignments
        $sql = "DELETE FROM `#__jobsfactory_companies_categories` WHERE company_id = '{$company->id}'";
        $db->setQuery($sql);
        $db->execute();

        if (!count($catids)) return; //no categories assigned
        // add category assignemtns
        $inserts = array();
        foreach ($catids as $catid){
            $inserts[] = " ('{$company->id}', '{$catid}')";
        }

        if (count($inserts))
        {
            $sql = "INSERT INTO `#__jobsfactory_companies_categories` (`company_id`,`cid`) VALUES ".implode(",",$inserts);
            $db->setQuery($sql);
            $db->execute();
        }
    }

     // COMPANIES WATCHLIST (favoriteS)
    function addWatch($id)
    {
        $database = JFactory::getDBO();
        $user = JFactory::getUser();

        if ($user->id && $id != "") {
            $database->setQuery("INSERT INTO #__jobsfactory_watchlist_companies SET userid = '" . $user->id .
                "',companyid='" . (int)$id . "'");
            $database->execute();
        }
        return $database->getAffectedRows();

    }

    function delWatch($id)
    {
        $database = JFactory::getDbo();
        $user = JFactory::getUser();

        if ($user->id && $id != "") {
            $database->setQuery("DELETE FROM #__jobsfactory_watchlist_companies WHERE userid = '" . $user->id .
                "' AND companyid='" . (int)$id . "'");
            $database->execute();
        }
        return $database->getAffectedRows();
    }

    function uploadFiles(&$company)
    {
        jimport('joomla.filesystem.file');
        $msg='';

        $db = JFactory::getDbo();
        $cfg= JTheFactoryHelper::getConfig();
        if (!$cfg->enable_images) return; //No image processing

        require_once(JPATH_COMPONENT_SITE.DS.'thefactory'.DS.'front.images.php');
		$imgTrans = new JTheFactoryImages();

        $app = JFactory::getApplication();
        //$jobsUserid = $app->input->getInt('id', 0);

        $oldid = $company->id;
        $jobsUserid = $app->input->getInt('userid', 0);
        $delete_main_picture = $app->input->getCmd('delete_main_picture', '');

        //replace logo if image mandatory
        $replace_my_file_element = $app->input->getCmd('replace_my_file_element', '');

        if (!is_dir(COMPANY_PICTURES_PATH))
            @mkdir(COMPANY_PICTURES_PATH, 0755);

        if ($oldid){
            //Repost
        	$this->moveOldFiles( $company, $oldid, $jobsUserid, $delete_main_picture,$replace_my_file_element);
        }
        $nrfiles=0;

        foreach ($_FILES as $k => $file) {
        	if (substr($k, 0, 7) != "picture")
                continue;

			if ( !isset($file['name']) || $file['name']=="")
            	continue;

            if ( !is_uploaded_file(@$file['tmp_name']) ){
                continue;
            }

            if ( filesize(@$file['tmp_name'])>$cfg->max_picture_size*1024 ){
                continue;
            }

            $fname = $file['name'];

            $ext = JFile::getExt($fname);

			if (!$company->isAllowedImage($ext)) {
				$msg .= JText::_("COM_JOBS_EXTENSION_NOT_ALLOWED") . ': ' . $file['name'];
				continue;
            }

            //if ($k == "picture" && (!$company->picture || $delete_main_picture)) {
            if ($k == "picture" && (!$company->picture || $delete_main_picture || $replace_my_file_element)) {

				$file_name = "logo_{$company->userid}.{$ext}";

                $company->picture = $file_name;
                $company->store();
            }

            $path = COMPANY_PICTURES_PATH . "$file_name";
			$res = move_uploaded_file($file['tmp_name'], $path);

            if ($res) {
				@chmod($path, 0755);

				$s = $imgTrans->resize_image(COMPANY_PICTURES_PATH.$file_name, $cfg->thumb_width, $cfg->thumb_height, 'resize');
				if(!$s){
					$msg .= $file['name'] . " thumb - " . JText::_('COM_JOBS_ERR_LOADING_PICTURE') . "<br><br>";
				}
				$s = $imgTrans->resize_image(COMPANY_PICTURES_PATH.$file_name, $cfg->medium_width, $cfg->medium_height, 'middle');
				if(!$s){
					$msg .= $file['name'] . " middle - " . JText::_('COM_JOBS_ERR_LOADING_PICTURE') . "<br><br>";
				}
            } else {
				$msg .= $file['name'] . "- " . JText::_('COM_JOBS_ERR_UPLOAD_FAIL') . "<br><br>";
            }

        }
        return $msg;

    }

    function moveOldFiles( $company, $oldid , $jobsUserid,$delete_main_picture, $replace_my_file_element){

        jimport( 'joomla.filesystem.file' );
        $my = JFactory::getUser();
       // $oldid = $company->id;
       // $jobsUserid = $app->input->getInt('userid', 0);
        $database = JFactory::getDbo();
        $database->setQuery("SELECT count(*) FROM #__jobsfactory_users WHERE id='$oldid' and userid='$my->id'");

        if (!$database->loadResult()) {
            echo JText::_('COM_JOBS_ALERTNOTAUTH');
            exit;
        }

        //replace_my_file_element
        $database->setQuery("SELECT picture FROM #__jobsfactory_users WHERE id=$oldid");
        $oldlogo = $database->loadResult();

        $new_id = $company->userid;

        if ( (!empty($oldlogo) && !$delete_main_picture) || (!empty($oldlogo) && $replace_my_file_element)) {
            if (file_exists(COMPANY_PICTURES_PATH . $oldlogo)) {

                $new_logo_name = "logo_{$new_id}." . JFile::getExt($oldlogo);
                copy(COMPANY_PICTURES_PATH . $oldlogo, COMPANY_PICTURES_PATH . $new_logo_name);
                copy(COMPANY_PICTURES_PATH . "middle_$oldlogo", COMPANY_PICTURES_PATH . "middle_$new_logo_name");
                copy(COMPANY_PICTURES_PATH . "resize_$oldlogo", COMPANY_PICTURES_PATH . "resize_$new_logo_name");
                $company->picture = $new_logo_name;
                $company->store();
            }
        }
        if ($delete_main_picture) {
            $company->picture = '';
            $company->store();

            if (file_exists(COMPANY_PICTURES_PATH . $oldlogo)) {
                @unlink(COMPANY_PICTURES_PATH . $oldlogo);
                @unlink(COMPANY_PICTURES_PATH . "resize_$oldlogo");
                @unlink(COMPANY_PICTURES_PATH . "middle_$oldlogo");
            }

        }
    }

    function validateSave($item)
    {

        $cfg = JTheFactoryHelper::getConfig();
        $error = array();

        // Title validation
        if ($item->name == "" || !isset($item->name))
            $error[] = JText::_("COM_JOBS_NAME_CAN_NOT_BE_EMPTY");

        if ($cfg->enable_images && $cfg->main_picture_require) {
            $has_main_picture = false;

            $app = JFactory::getApplication();

            //$jobsUserid = $app->input->getInt('id', 0);
            $oldid = $app->input->getInt('userid', 0);
            $delete_main_picture = $app->input->getWord('delete_main_picture', '');

            if ($oldid || $item->id) {
                //repost or edit existing
                $oldUser = JTable::getInstance('users', 'Table');
                $has_main_picture = isset($_FILES['picture']) && (is_uploaded_file(@$_FILES['picture']['tmp_name']));

                if (!$delete_main_picture && $oldUser->load($oldid ? $oldid : $item->id)) {
                    $has_main_picture = $has_main_picture || ($oldUser->picture !== '');
                }
            } else {

                if (isset($_FILES['picture'])) {
                    $file = $_FILES['picture'];
                    $has_main_picture = true;

                    if (!is_uploaded_file(@$file['tmp_name'])) {
                        $error[] = $file['name'] . "- " . JText::_("COM_JOBS_ERR_UPLOAD_FAIL");
                        $has_main_picture = false;
                    }

                    if (filesize($file['tmp_name']) > $cfg->max_picture_size * 1024) {
                        $error[] = $file['name'] . "- " . JText::_("COM_JOBS_ERR_IMAGESIZE_TOO_BIG");
                        $has_main_picture = false;
                    }
                    if (isset($file['name']) && $file['name']) {
                        $ext = strtolower(JFile::getExt($file['name']));
                        if (!$item->isAllowedImage($ext)) {
                            $error[] = JText::_("COM_JOBS_EXTENSION_NOT_ALLOWED") . ': ' . $ext;
                            $has_main_picture = false;
                        }
                    } else {
                        $has_main_picture = false;
                    }
                }
            }

            if (!$has_main_picture)
                $error[] = JText::_("COM_JOBS_ERR_PICTURE_IS_REQUIRED");
        }
        foreach ($_FILES as $k => $file) {
            if (substr($k, 0, 7) != "picture")
                continue;

            if (!isset($file['name']) || $file['name'] == "")
                continue;

            if (!is_uploaded_file(@$file['tmp_name'])) {
                continue;
            }

            if (filesize(@$file['tmp_name']) > $cfg->max_picture_size * 1024) {
                $error[] = $file['name'] . " - " . JText::_("COM_JOBS_ERR_IMAGESIZE_TOO_BIG");
            }
        }

        //Custom Validations
        if (!$item->check())
            $error = array_merge($error, $item->_validation_errors);


        return $error;
    }

    public function vote($id, $rating)
    {
        // Initialise variables.
        $user = JFactory::getUser();
        $rating = intval($rating);
        $table = $this->getTable('ratings', 'Table');
        $dbo = $this->getDbo();

        // Check if rating is valid.
        if (0 > $rating || 5 < $rating) {
          return false;
        }

        // Get company/user rated
        if (!$id) {
          return false;
        }

        if ( !$table->load(array('rated_userid'=>$id,'userid'=>$user->id)) ) {
          $dbo->setQuery("INSERT INTO `#__jobsfactory_ratings`(`userid`,`rated_userid`,`rating_user`) values ({$user->id},{$id},0)");
          $dbo->execute();
        }

       /* if (!$table->load(array('userid'=>$user->id))) {
          $dbo->setQuery("INSERT INTO `#__jobsfactory_ratings`(`userid`,`rated_userid`,`rating_user`) values ({$user->id},{$id},0)");
          $dbo->execute();
        }*/

        // Check if user has already voted for the company/user
        if (($table->userid == $user->id && $table->rated_userid == $id && $table->rating_user)) {
            JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_ALREADY_VOTED"),'notice');
            return false;
        }

        // Register rating
        $table->load(array('userid'=>$user->id,'rated_userid'=>$id));
        //$table->is_offered  = $user->id == $table->userid;
        $table->rating_user = $rating;

        if (!$table->store()) {
          return false;
        }

        // Update user average rating
        $jobsUserRating = $this->getTable('usersRating', 'Table');

        if (!$jobsUserRating->load(array('userid' => $id))) {
            $dbo->setQuery("INSERT INTO `#__jobsfactory_users_rating`(`userid`,`user_rating`,`users_count`) values ({$id},0.00,0)");
            $dbo->execute();
        }

        $jobsUserRating->load(array('userid' => $id));
        $jobsUserRating->user_rating = ($jobsUserRating->users_count * $jobsUserRating->user_rating + $rating) / ++$jobsUserRating->users_count;
        $jobsUserRating->store();

        return true;
    }

}

