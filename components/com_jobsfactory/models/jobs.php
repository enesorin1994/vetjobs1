<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class jobsModelJobs extends jobsModelGeneric
{
    var $_name = 'Jobs';
    var $name = 'Jobs';
    var $page = 'jobs';
    var $context = 'jobsModel';

    var $order_fields = array(
        'start_date' => "j.start_date",
        'title' => "j.title",
        'username' => "u.username",
        'end_date' => "j.end_date",
        'hits' => "j.hits",
        'catname' => "cat.catname",
        'job_cityname' => "job_cityname",
        'cityname' => "cityname",
        'locname' => "loc.locname",
        'job_type' => "j.job_type",
        'studies_request' => "j.studies_request",
        'experience_request' => "j.experience_request",
        'id' => "j.id",
        'nr_applicants' => "nr_applicants",
        'nr_applications' => "nr_applications"
    );
    var $knownFilters = array(
        'keyword' => array('type' => 'string'),
        'in_description' => array('type' => 'int'),
        'in_archive' => array('type' => 'int'),
        'filter_archive' => array('type' => 'string'),
        'filter_myjobs' => array('type' => 'string'),
        'filter_watchlist' => array('type' => 'string'),
        'userid' => array('type' => 'int'),
        'cat' => array('type' => 'int'),
        'location' => array('type' => 'int'),
        'job_location_state' => array('type' => 'int'),
        'locationcity' => array('type' => 'int'),
        'tag' => array('type' => 'string'),
        'city' => array('type' => 'string'),
        'job_cityid' => array('type' => 'int'),
        'country' => array('type' => 'string'),
        'job_type' => array('type' => 'int'),
        'studies_request' => array('type' => 'int'),
        'experience_request' => array('type' => 'int'),
        'afterd' => array('type' => 'string'),
        'befored' => array('type' => 'string'),
        'jinterval' => array('type' => 'int'),

        'filter_order' => array('type' => 'string', 'default' => 'title'),
        'filter_order_Dir' => array('type' => 'string', 'default' => 'ASC')
    );

    function buildQuery()
    {

        $user   = JFactory::getUser();
        $db     = JFactory::getDBO();
        $app    = JFactory::getApplication();
        $cfg    = JTheFactoryHelper::getConfig();
        $config = JFactory::getConfig();
        $datenow = JFactory::getDate('now', $config->get('config.offset'));

       /* $filter_archive=$this->getState('filters.filter_archive','active');
        $filter_myjobs=$this->getState('filters.filter_myjobs','active');
        $filter_watchlist=$this->getState('filters.filter_watchlist');
        $filter_catid=$this->getState('filters.cat',0);*/

        $task = $app->input->getCmd('task');
        $company_id = $app->input->get('company_id', null);

        $query = JTheFactoryDatabase::getQuery();
        $query->from('#__jobsfactory_jobs', 'j');

        /**
         *
         *  Fields to select
         *
         * */
        $query->select('`j`.*');
        $query->select('`j`.id AS jobId');
        $query->select('`cat`.`id` as cati, `cat`.`catname`');
        $query->select('`u`.`username`');
		$query->select('`j`.shortdescription AS job_shortdescription');

        $query->select('`co`.`name` AS companyname');

        if ($cfg->enable_location_management) {
            $query->select('`loc`.`locname` ');
            $query->select('IF ( j.job_cityid = 0 , j.job_cityname, (SELECT cityname FROM `#__jobsfactory_cities` as cit WHERE cit.id=j.job_cityid) ) as cityname');
        } else {
            //$query->select('`j`.`job_cityid` ');
            //make sure that not error if use location settings change
            if ($this->getState('filters.filter_order') == 'locname' || $this->getState('filters.filter_order') == 'cityname' ) {
                $this->setState('filters.filter_order','job_cityname');
            }

            $query->select('`j`.`job_cityname` ');
        }

        $query->select('GROUP_CONCAT(DISTINCT `t`.`tagname`) AS tags');
        $query->select('IF ( j.show_candidate_nr = 1 , COUNT(DISTINCT candidates.userid) ,0  ) as nr_applicants');
        $query->select('IF ( j.show_candidate_nr = 1 , COUNT(distinct candidates.id), 0 ) as nr_applications');

        if ($user->id) {
            $query->select("IF ( `j`.`userid` = '" . (int)$user->id . "', 1, 0 ) AS `is_my_job`");
            $query->select("`fav_table`.`id` AS favorite");
        } else {
            $query->select("0  AS `is_my_job`");
            $query->select("null AS favorite");
        }
        /**
         *
         *  Where conditions
         *
         * */
        $query->where("`j`.`close_by_admin` <> 1");

        switch ($task)
        {
            //live jobs
            default:
            case 'listjobs':
                $query->where(array("`cat`.`status`=1", "`cat`.`status` IS NULL"), "OR");

                if ($this->getState('filters.filter_archive')) {
                    if ($this->getState('filters.filter_archive') == 'archive') {
                        $query->where(array("`j`.`close_offer` = 1", "`j`.`published` = 1"), "AND");
                        //$query->where( " '" . $datenow->toSQL(false)."'" .(($this->getState('filters.filter_archive'=='active'))?'<=':'>=')."`j`.end_date",'AND');
                    }
                }
                else
                    $query->where(array("`j`.`close_offer` = 0", "`j`.`published` = 1"), "AND");

                if ($cfg->admin_approval) {
                    $query->where("`j`.`approved` = 1");
                }
                $query->where("`j`.`start_date` <= '" . $datenow->toSql(false) . "'");

                break;

            case 'companyjobs':
                $query->where("`j`.`userid` ='" . $company_id . "'");
                $query->where(array("`cat`.`status`=1", "`cat`.`status` IS NULL"), "OR");
                if ($this->getState('filters.filter_archive')) {
                    if ($this->getState('filters.filter_archive') == 'draft')
                        $query->where(array("`j`.`close_offer` = 0", "`j`.`published` = 0"), "AND");
                    else
                        $query->where(array("`j`.`close_offer` = 1", "`j`.`published` = 1"), "AND");
                }
                else
                    $query->where(array("`j`.`close_offer` = 0", "`j`.`published` = 1"), "AND");

                if ($cfg->admin_approval) {
                    $query->where("`j`.`approved` = 1");
                }
                $query->where("`j`.`start_date` <= '" . $datenow->toSql(false) . "'");
                $query->where("`j`.`end_date` >= '" . $datenow->toSql(false) . "'");

                break;
            case 'myjobs':
                if ($user->id) {
                    switch ($this->getState('filters.filter_myjobs'))
                    {
                        default:
                            $query->where("`j`.`userid` ='" . $user->id . "'");
                            if ($cfg->admin_approval) {
                                $query->where("`j`.`published` = 1");
                                $query->where(array("`j`.`close_offer` = 0", "`j`.`approved` = 1"), "AND");
                            } else
                                $query->where(array("`j`.`close_offer` = 0", "`j`.`published` = 1"), "AND");
                            break;

                        case 'unpublished':
                            $query->where("`j`.`userid` ='" . $user->id . "'");
                            $query->where(array("`j`.`close_offer` = 0", "`j`.`published` = 0"), "AND");
                            break;
                        case 'unapproved':
                            if ($cfg->admin_approval) {
                                $query->where("`j`.`userid` ='" . $user->id . "'");
                                $query->where(array("`j`.`close_offer` = 0", "`j`.`approved` = 0"), "AND");
                            }
                            break;
                        case 'archive':
                            $query->where("`j`.`userid` ='" . $user->id . "'");
                            $query->where("`j`.`close_offer` = 1");
                            break;
                    }

                }
                break;
            case 'watchlist':
                if ($user->id) {
                    switch ($this->getState('filters.filter_watchlist'))
                    {
                        default:
                            $query->where(array("`cat`.`status`=1", "`cat`.`status` IS NULL"), "OR");
                            $query->where("`fav_table`.userid ='" . $user->id . "'");
                            $query->where("`j`.`start_date` <= '" . $datenow->toSql(false) . "'");
                            break;
                        case 'categories':
                            $query->where(array("`cat`.`status`=1", "`cat`.`status` IS NULL"), "OR");
                            $query->where("`watch_cats`.userid ='" . $user->id . "'");
                            $query->where("`j`.`start_date` <= '" . $datenow->toSql(false) . "'");
                            break;
                        case 'cities':
                            $query->where("`watch_city`.userid ='" . $user->id . "'");
                            $query->where("`j`.`start_date` <= '" . $datenow->toSql(false) . "'");
                            break;
                        case 'locations':
                            $query->where("`watch_loc`.userid ='" . $user->id . "'");
                            $query->where("`j`.`start_date` <= '" . $datenow->toSql(false) . "'");
                            break;
                        case 'companies':
                            $query->where("`watch_comp`.userid ='" . $user->id . "'");
                            $query->where("`j`.`start_date` <= '" . $datenow->toSql(false) . "'");
                            break;
                    }

                }
                break;
            case 'myapplications':
                if ($user->id) {
                    $query->where("`candidates`.`userid` = '" . $user->id . "'");
                }
                break;
        }

        if ($this->getState('filters.keyword')) {

            $keyword = $db->escape($this->getState('filters.keyword'));

            $w = array();
            $w[] = 'j.title LIKE \'%' . $keyword . '%\'';
            $w[] = 'j.jobuniqueID LIKE \'%' . $keyword . '%\'';
            if ($this->getState('filters.in_description')) {
                $w[] = 'j.shortdescription LIKE \'%' . $keyword . '%\'';
                $w[] = 'j.description LIKE \'%' . $keyword . '%\'';
            }
            $query->where($w, 'OR');
        }

        if ($this->getState('filters.job_type')) {
            $query->where(" j.job_type = '" . $db->escape($this->getState('filters.job_type')) . "' ");
        }

        if ($this->getState('filters.studies_request')) {
            $query->where(" j.studies_request = '" . $db->escape($this->getState('filters.studies_request')) . "' ");
        }

        if ($this->getState('filters.experience_request')) {
            $experience_request = $db->escape($this->getState('filters.experience_request'));
            $query->where(" j.experience_request LIKE '%" . $db->escape($experience_request) . "%' ");
        }

        if ($this->getState('filters.jinterval')) {
            $jinterval = $db->escape($this->getState('filters.jinterval'));
            $query->where(" j.start_date >=   DATE_SUB('" .$datenow->toSql(false)."',INTERVAL $jinterval DAY) ");
        }

        if ($this->getState('filters.userid')) {
            $query->where(" j.userid = '" . $db->escape($this->getState('filters.userid')) . "' ");
        }

        if ($this->getState('filters.tag')) {
            $query->where(" t.tagname LIKE '%" . $db->escape($this->getState('filters.tag')) . "%' ");
        }

        if ($this->getState('filters.cat')) {
            if (!$cfg->inner_categories) {
                $query->where(" j.cat= '" . $db->escape($this->getState('filters.cat')) . "' ");
            } else {
                $myApp=JTheFactoryApplication::getInstance();
                $legacy=$myApp->getIniValue('use_legacy_model', 'categories');
                JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'thefactory' . DS . 'category' . DS . 'models');

                if ($legacy){
                    require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'thefactory' . DS . 'category' . DS . 'models'.DS.'category.legacy.php');
                    $catModel=JModelLegacy::getInstance('CategoryLegacy','JThefactoryModel');
                }else{
                    require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'thefactory' . DS . 'category' . DS . 'models'.DS.'category.joomla.php');
                    $catModel=JModelLegacy::getInstance('CategoryJoomla','JThefactoryModel');
                }

                $cat_ids = $catModel->getCategoryTree((int)$this->getState('filters.cat'));
                if (count($cat_ids)) {
                    if(1 < count($cat_ids)){
                        $cat_ids_obj = array();

                        foreach ($cat_ids as $cat_obj) {
                            $cat_ids_obj[] = $cat_obj->id;
                        }

                        $cat_ids = implode(",", $cat_ids_obj);
                    } else {
                        $cat_ids = $cat_ids[0]->id;
                    }
                }

                $query->where(' j.cat  IN (' . $cat_ids . ') ');
            }
        }

        if ($this->getState('filters.location')) {
            JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models');
            $locModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');
            $location_cities = $locModel->getLocationChildren((int)$this->getState('filters.location'));
            $location_city = (int)$this->getState('filters.location');

            if (count($location_cities)) {
                $location_ids = implode(",", $location_cities);
                $query->where(array("j.job_cityid  IN (" . $location_ids . ")", "j.job_location_state = $location_city"), "OR");
            } else {
                $query->where("j.job_location_state = $location_city");
            }
        }

        if ($this->getState('filters.job_location_state')) {
            $locationfilter = $this->getState('filters.job_location_state');
            $query->where(' j.job_location_state = \'' . $locationfilter . '\'' );
        }

        if ($this->getState('filters.afterd')) {
            $start_date = JobsHelperDateTime::DateToIso($this->getState('filters.afterd'));
            $start_date = JFactory::getDate($start_date, $config->get('config.offset'))->toSql(false);
            $query->where(' j.start_date>=\'' . $start_date . '\'');
        }

        if ($this->getState('filters.befored')) {
            $before_date = JobsHelperDateTime::DateToIso($this->getState('filters.befored'));
            $before_date = JFactory::getDate($before_date, $config->get('config.offset'))->toSql(false);
            $query->where(' j.end_date<=\'' . $before_date . '\'');
        }

        /**
         *  Joins
         * */
        $query->join('left','#__jobsfactory_candidates', 'candidates', '`candidates`.`job_id`=`j`.`id` AND `candidates`.`cancel`=0 ' . (($task == 'myapplications') ? "AND `candidates`.`userid` = '{$user->id}'" : ""));
        $query->join('left','#__jobsfactory_users','co','co.userid=`j`.`userid`');
        $query->join('left', '#__jobsfactory_categories', 'cat', '`j`.`cat`=`cat`.`id`');

        if ($cfg->enable_location_management) {
            $query->join('left', '#__jobsfactory_locations', 'loc', '`j`.`job_location_state`=`loc`.`id`');
        }
        $query->join('left', '#__jobsfactory_tags', 't', '`j`.`id`=`t`.`job_id`');

        if ($user->id) {
            $query->join('left', '#__jobsfactory_watchlist', 'fav_table', "`fav_table`.`job_id`=`j`.`id` AND `fav_table`.`userid` = '{$user->id}'");

            switch ($this->getState('filters.filter_watchlist'))
            {
                case 'categories':
                    $query->join('left', '#__jobsfactory_watchlist_cats', 'watch_cats', "`j`.`cat`=`watch_cats`.`catid` AND `watch_cats`.`userid` = '{$user->id}' "); //AND `watch_cats`.`userid` = '{$user->id}'
                    break;
                case 'cities':
                    $query->join('left', '#__jobsfactory_watchlist_city', 'watch_city', "`j`.`job_cityid`=`watch_city`.`cityid` "); //AND `watch_city`.`userid` = '{$user->id}'
                    $query->join('left', '#__jobsfactory_cities', 'city', '`watch_city`.`cityid`=`city`.`id`');
                    break;
                case 'locations':
                    $query->join('left', '#__jobsfactory_watchlist_loc', 'watch_loc',  "`j`.`job_location_state`=`watch_loc`.`locationid` "); //AND `watch_loc`.`userid` = '{$user->id}'
                    break;
                case 'companies':
                    $query->join('left', '#__jobsfactory_watchlist_companies', 'watch_comp', "`j`.`userid`=`watch_comp`.`companyid` "); //AND `watch_comp`.`userid` = '{$user->id}'
                    break;
            }
        }

        // Featurings first
        $query->order('`j`.featured=\'featured\' DESC');

        // Required ordering filter
        $filter_order = $this->getState('filters.filter_order');

        if ($filter_order) {
            $filter_order_Dir = $this->getState('filters.filter_order_Dir');
            $query->order($db->escape($this->order_fields[$filter_order] . ' ' . $filter_order_Dir));
        }

        $query->group('`j`.`id`');

        $profile = JobsHelperTools::getUserProfileObject();
        //this binds to the query object everything that is related to custom fields
        parent::buildCustomQuery($query, $profile, '`j`.`userid`');

        $queriedTables = $query->getQueriedTables();
        if ($this->getState('filters.country')) {
            $table = $profile->getFilterTable('country');
            $field = $profile->getFilterField('country');

            $alias = array_search($table, $queriedTables);
            $alias = ($alias != '') ? $alias : 'ui';

            $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.country')) . '\') ');
        }

        if ($this->getState('filters.city')) {
            $query->where(" j.job_cityname LIKE '%" . $db->escape($this->getState('filters.city')) . "%' ");
        }

        if ($this->getState('filters.job_cityid')) {
            $cityfilter = $this->getState('filters.job_cityid');
            $query->where(' j.job_cityid = \'' . $cityfilter . '\'' );
        }

        if ($this->getState('filters.locationcity')) {

           if ($cfg->enable_location_management) {
               $city = (int)$this->getState('filters.locationcity');
               $query->where(' j.job_cityid  IN (' . $city . ') ');
           } else {
               $city_name = JobsHelperLocation::getCityName($db->escape($this->getState('filters.locationcity')));
               $query->where(" j.job_cityname LIKE '%" . $db->escape($city_name) . "%' ");
           }
       }
		//var_dump((string)$query); exit;
        return $query;
    }

    function getTotal()
    {
        if (empty($this->total)) {
            $db = JFactory::getDbo();
            $query = $this->buildQuery();
            $query->set('select', "count(distinct `j`.id)");
            $query->set('order', null);
            $query->set('group', null);

            $db->setQuery((string)$query);
            $this->total = $db->loadResult();
        }

        return $this->total;
    }

    function getCompanyJobsList($id) {

        $db = JFactory::getDbo();

        $query = "SELECT j.*, a.userid as candidateId, a.comments, c.catname "
            . " ,IF ( j.show_candidate_nr = 1 , COUNT(DISTINCT a.userid) ,0) as nr_applicants "
            . " FROM #__jobsfactory_jobs j "
            . " LEFT JOIN #__jobsfactory_candidates a ON j.id = a.job_id  "
            . " LEFT JOIN #__jobsfactory_categories c ON j.cat = c.id "
            . " WHERE j.userid = '{$id}' "
            . " GROUP BY j.id "
            . " LIMIT 0,10 ";

        $db->setQuery($query);
        return $db->loadObjectList();
    }

}
