<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

/**
 * @package        Jobs
 */
class jobsModelLocation extends JModelLegacy
{
    var $_name = 'location';
    var $name = 'location';

    function getJobsLocationTree($root_location = 1, $max_depth = null, $watchlist_user = null,$main=true)
    {
        $db = JFactory::getDbo();
        $config = JFactory::getConfig();
        $datenow = JFactory::getDate('now', $config->get('config.offset'));
        $watchlist_fields = "";
        $watchlist_join = "";

        if ($main) {
            $idcondition = ($root_location != 0) ? "and l.`id`='{$root_location}'" : '';
            $city_status = '';
        }
        else {
            $idcondition = ($root_location != 0) ? "and c2.`id`='{$root_location}'" : '';
            $city_status = ($root_location != 0) ? "AND c2.`status`=1" : '';
        }

        if ($watchlist_user) {
            $watchlist_fields = ",count(w.id) as watchListed_flag ";
            $watchlist_join = "left join `#__jobsfactory_watchlist_loc` w on w.locationid=l.id and w.userid='{$watchlist_user}'";
        }

        $query = "SELECT l.*, count(distinct a.id) as nr_a, count(c2.id) as nr_subloc,
            GROUP_CONCAT( DISTINCT c2.id SEPARATOR  ', ' ) AS 'cityids',
            GROUP_CONCAT( DISTINCT c2.cityname SEPARATOR  ', ' ) AS 'cities'
             {$watchlist_fields}
                FROM `#__jobsfactory_locations` l
                    left join `#__jobsfactory_cities` c2 on l.id=c2.location
                    left join `#__jobsfactory_jobs` a on l.id=a.job_location_state and `a`.`close_by_admin` <> 1
                        and `a`.`close_offer` = 0 and `a`.`published` = 1 and `a`.`start_date` <= '" . $datenow->toSql(false) . "'
                    {$watchlist_join}
                    where l.`status`=1 {$city_status} {$idcondition}
            group by l.id
            order by l.ordering
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();

        return $items;
    }

    // LOCATIONS WATCHLIST (favoriteS)
    function addWatch($id)
    {
        $database = JFactory::getDBO();
        $user = JFactory::getUser();

        if ($user->id && $id != "") {
            $database->setQuery("INSERT INTO #__jobsfactory_watchlist_loc SET userid = '" . $user->id .
                "',locationid='" . (int)$id . "'");
            $database->execute();
        }

        return $database->getAffectedRows();
    }

    function delWatch($id)
    {
        $database = JFactory::getDBO();
        $user = JFactory::getUser();

        if ($user->id && $id != "") {
            $database->setQuery("DELETE FROM #__jobsfactory_watchlist_loc WHERE userid = '" . $user->id .
                "' AND locationid='" . (int)$id . "'");
            $database->execute();
        }

        return $database->getAffectedRows();
    }

}
