<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class attachmentsModelAttachments extends jobsModelGeneric
{
    var $_name = 'Attachments';
    var $name = 'Attachments';
    var $page = 'resumes';
    var $context = 'attachmentsModel';

    var $order_fields = array(
        'title' => "a.title",
        'username' => "u.username",
        'hits' => "a.hits",
        'id' => "a.id"
    );
    var $knownFilters = array(
        'keyword' => array('type' => 'string'),
        'in_description' => array('type' => 'int'),
        'filter_archive' => array('type' => 'string'),
        'PDFResume' => array('type' => 'string'),
        'userid' => array('type' => 'int'),
        'cat' => array('type' => 'int'),
        'tag' => array('type' => 'string'),
        'city' => array('type' => 'string'),
        'country' => array('type' => 'string'),
        'afterd' => array('type' => 'string'),
        'befored' => array('type' => 'string')
    );

    function buildQuery()
    {
        $user   = JFactory::getUser();
        $db     = JFactory::getDBO();
        $task   = JFactory::getApplication()->input->getCmd('task');
        $cfg    = JTheFactoryHelper::getConfig();
        $config = JFactory::getConfig();
        $datenow = JFactory::getDate('now', $config->get('config.offset'));

        $query = JTheFactoryDatabase::getQuery();
        $query->from('#__jobsfactory_attachments', 'a');

        /**
         *  Fields to select
         * */
        $query->select('`a`.*');
        $query->select('`a`.id AS resumeId');
        $query->select('`u`.`username`');
        $query->select('GROUP_CONCAT(DISTINCT `t`.`tagname`) AS tags');
        $query->select('IF ( COUNT(`pics`.`id`) >0 , 1, 0 ) AS more_pictures');

        if ($user->id) {
            $query->select("IF ( `a`.`userid` = '" . (int)$user->id . "', 1, 0 ) AS `is_my_resume`");
            $query->select("`fav_table`.`id` AS favorite");
        } else {
            $query->select("0  AS `is_my_resume`");
            $query->select("null AS favorite");
        }

        /**
         *
         *  Where conditions
         *
         * */

        switch ($task)
        {
            //live resumes
            default:
            case 'watchlist':  //could be cvs seen by companies
                $query->where("`fav_table`.userid ='" . $user->id . "'");
                break;
        }

        if ($this->getState('filters.keyword')) {

            $keyword = $db->escape($this->getState('filters.keyword'));

            $w = array();
            $w[] = 'a.title LIKE \'%' . $keyword . '%\'';
            if ($this->getState('filters.indesc')) {
                $w[] = 'a.shortdescription LIKE \'%' . $keyword . '%\'';
                $w[] = 'a.description LIKE \'%' . $keyword . '%\'';
            }
            $query->where($w, 'OR');
        }

        if ($this->getState('filters.userid')) {
            $query->where(" a.userid = '" . $db->escape($this->getState('filters.userid')) . "' ");
        }

        if ($this->getState('filters.tag')) {
            $query->where(" t.tagname LIKE '%" . $db->escape($this->getState('filters.tag')) . "%' ");
        }

        /**
         *  Joins
         * */
        $query->join('left', '#__jobsfactory_candidates', 'candidates', '`candidates`.`job_id`=`a`.`id` and `candidates`.`cancel`=0 ' . (($task == 'myapplications') ? "AND `candidates`.`userid` = '{$user->id}'" : ""));
        $query->join('left', '#__jobsfactory_tags', 't', '`a`.`id`=`t`.`job_id`');
        $query->join('left', '#__jobsfactory_pictures', 'pics', '`a`.`id`=`pics`.`job_id`');
        if ($user->id) {
            $query->join('left', '#__jobsfactory_watchlist', 'fav_table', "`fav_table`.`job_id`=`a`.`id` AND `fav_table`.`userid` = '{$user->id}'");
        }
        // Featurings first
        $query->order('`a`.featured=\'featured\' DESC');

        // Required ordering filter
        $filter_order = $this->getState('filters.filter_order');
        if ($filter_order) {
            $filter_order_Dir = $this->getState('filters.filter_order_Dir');
            $query->order($db->escape($this->order_fields[$filter_order] . ' ' . $filter_order_Dir));
        }

        $query->group('`a`.`id`');

        $profile = JobsHelperTools::getUserProfileObject();
        //this binds to the query object everything that is related to custom fields
        parent::buildCustomQuery($query, $profile, '`a`.`userid`');

        $queriedTables = $query->getQueriedTables();
        if ($this->getState('filters.country')) {
            $table = $profile->getFilterTable('country');
            $field = $profile->getFilterField('country');
            ;
            $alias = array_search($table, $queriedTables);
            $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.country')) . '\') ');
        }

        if ($this->getState('filters.city')) {
            $table = $profile->getFilterTable('city');
            $field = $profile->getFilterField('city');
            $alias = array_search($table, $queriedTables);
            $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.city')) . '\') ');
        }

        return $query;
    }

    function getTotal()
    {

        if (empty($this->total)) {
            $db = JFactory::getDbo();
            $query = $this->buildQuery();
            $query->set('select', "count(distinct `a`.id)");
            $query->set('order', null);
            $query->set('group', null);
            $db->setQuery((string)$query);
            $this->total = $db->loadResult();
        }
        return $this->total;
    }

}

