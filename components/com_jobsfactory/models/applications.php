<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

/**
 * @package		Jobs
 */
class jobsModelApplications extends JModelLegacy
{
	var $_name='applications';
	var $name='applications';
    var $context='applications';

    protected $userpagination=null;
    protected $usertotal=null;
    protected $pagination=null;
    protected $total=null;
    protected $items=null;

    function getUserApplications($company_userid)
    {
		
        $db = JFactory::getDbo();
		$db->setQuery("SELECT *, ".
            "count(id) as count FROM `#__jobsfactory_candidates` WHERE userid ='{$company_userid}'  ");
        $r1 = $db->loadObject();

        $result["candidate"]= $r1;
        $result["count_candidate"]= $r1->count;

        return $result;   
    }

    function loadCompanyItems($company_userid)
    {
        $this->getLimits();

        $query = $this->getApplicationsList($company_userid);
        $this->items = $this->_getList((string) $query, $this->getState('limitstart'), $this->getState('limit'));
        $this->setCompanyPagination($company_userid);

        return $this->items;
    }

    function getApplicationsList($company_userid)
    {
        $this->getLimits();

        $db = JFactory::getDbo();
        $query = JTheFactoryDatabase::getQuery();

        $query->from('#__jobsfactory_candidates', 'a');

        $query->select('`a`.*');
        $query->select('`j`.`title`');
        $query->select('`j`.`show_candidate_nr`, `j`.`shortdescription`');
        $query->select('`c`.`catname`');
        $query->select('IF ( j.show_candidate_nr = 1 , COUNT(DISTINCT a.userid) ,0) as nr_applicants');

        $query->join('left','#__jobsfactory_jobs','j','a.job_id=`j`.`id`');
        $query->join('left','#__jobsfactory_categories','c','j.cat=`c`.`id`');

        $query->where("j.userid = '{$company_userid}' ");
        $query->group('`a`.`job_id`');

        return $query;
    }

    function loadUserItems($userid)
    {
        $this->getLimits();

        $query = $this->getUserApplicationsList($userid);
        $this->items = $this->_getList((string) $query, $this->getState('limitstart'), $this->getState('limit'));
        $this->setUserPagination($userid);

        return $this->items;
    }

    function getUserApplicationsList($userid) {

        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $task = JFactory::getApplication()->input->getCmd('task','myapplications');
        $this->getLimits();

        $query = JTheFactoryDatabase::getQuery();

        $query->from('#__jobsfactory_candidates', 'a');

        $query->select('`a`.*');
        $query->select('`j`.`title`');
        $query->select('`j`.`show_candidate_nr`, `j`.`jobuniqueID`, `j`.`shortdescription`');
        $query->select('`j`.`start_date`, `j`.`end_date` ');
        $query->select('`u`.`name`, `u`.`username`');
        $query->select('`c`.`catname`');

        $query->join('left','#__users','u','a.userid=`u`.`id`');
        $query->join('left','#__jobsfactory_jobs','j','a.job_id=`j`.`id`');
        $query->join('left','#__jobsfactory_categories','c','`j`.`cat`=`c`.`id`');

        $query->where("a.userid = '{$userid}' ");
        $query->where("a.cancel = 0 ");

        return $query;
    }

    function getCatname()
    {
        $cattable = JTable::getInstance('category', 'JTheFactoryTable');
        $cattable->load($this->cat);
        return $cattable->catname;
    }

    function getLimits() {
        $app = JFactory::getApplication();
        $list_limit= JobsHelperTools::getItemsPerPage();

        // Get the pagination request variables
        $this->setState('limit', $app->getUserStateFromRequest($this->context.'.limit','limit', $list_limit, 'int'));
        $this->setState('limitstart', $app->input->getInt('limitstart', 0));
        // In case limit has been changed, adjust limitstart accordingly
        $this->setState('limitstart', ($this->getState('limit') != 0 ? (floor($this->getState('limitstart') / $this->getState('limit')) * $this->getState('limit')) :0));

    }

    public function setState($property, $value = null)
    {
        return $this->state->set($property, $value);
    }

    public function getState($property = null, $default = null)
    {
        if (!$this->__state_set)
        {
            // Protected method to auto-populate the model state.
            $this->populateState();

            // Set the model state set flag to true.
            $this->__state_set = true;
        }

        return $property === null ? $this->state : $this->state->get($property, $default);
    }

    function setUserPagination($userid){

        // Lets load the content if it doesn't already exist
        if (empty($this->userpagination)) {
            jimport('joomla.html.pagination');

            $this->userpagination= new JPagination($this->getUserTotal($userid), $this->getState('limitstart'),$this->getState('limit'));
        }

        return $this->userpagination;
    }

    function getUserPagination()
    {
        if($this->userpagination) return $this->userpagination;
        jimport('joomla.html.pagination');
        $this->userpagination = new JPagination($this->usertotal, $this->getState('limitstart'),$this->getState('limit'));
        return $this->userpagination;
    }

    function getCompanyPagination()
    {
        if($this->pagination) return $this->pagination;
        jimport('joomla.html.pagination');
        $this->pagination = new JPagination($this->total, $this->getState('limitstart'),$this->getState('limit'));
        return $this->pagination;
    }

    function setCompanyPagination($company_userid){

        // Lets load the content if it doesn't already exist
        if (empty($this->pagination)) {
            jimport('joomla.html.pagination');

            $this->pagination= new JPagination($this->getCompanyTotal($company_userid), $this->getState('limitstart'),$this->getState('limit'));
        }

        return $this->pagination;
    }

    function getUserTotal($userid)
    {
        if (empty($this->usertotal)) {
            $user = JFactory::getUser();
            $db = JFactory::getDbo();

            $query = $this->getUserApplicationsList($userid);

            $query->set('select', "count(distinct `j`.id)");
            $query->set('order', null);
            $query->set('group', null);

            $db->setQuery((string)$query);
            $this->usertotal = $db->loadResult();
        }
        return $this->usertotal;
    }

    function getCompanyTotal($company_userid)
    {

        if (empty($this->total)) {
            $db = JFactory::getDbo();
            $query = $this->getApplicationsList($company_userid);
            $query->set('select', "count(distinct `j`.id)");
            $query->set('order', null);
            $query->set('group', null);

            $db->setQuery((string)$query);
            $this->total = $db->loadResult();
        }
        return $this->total;
    }

}
