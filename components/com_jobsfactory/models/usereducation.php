<?php
    /**------------------------------------------------------------------------
    com_jobsfactory - Jobs Factory 1.6.5
    ------------------------------------------------------------------------
     * @author thePHPfactory
     * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
     * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     * Websites: http://www.thePHPfactory.com
     * Technical Support: Forum - http://www.thePHPfactory.com/forum/
     * @package: Jobs
    -------------------------------------------------------------------------*/

    defined('_JEXEC') or die('Restricted access');

    jimport('joomla.application.component.model');
    jimport('joomla.application.component.helper');

    class usereducationModelUsereducation extends JModelLegacy
    {

        var $_name = 'usereducation';
        var $name = 'usereducation';

        function saveUsereducation($usereducation)
        {
            $isNew = !(bool)$usereducation->id;

            $error = $this->validateSave($usereducation);
            if (count($error) > 0) return $error;

            if (!$e = $usereducation->store()) {
                $error[] = $e;
                return $error;
            }

            return $error;
        }

        function bindUsereducation(&$usereducation)
        {
            $cfg = JTheFactoryHelper::getConfig();
            $date = new JDate();
            $app = JFactory::getApplication();
            $usereducation->id = $app->input->getInt('id', null);

            $usereducation->load($usereducation->id);
            $posted_val = $app->input->getArray($_POST);

            $usereducation->bind($posted_val);

            if (isset($posted_val['edu_start_date']) && isset($posted_val['edu_end_date']) ) {
                $usereducation->start_date = JobsHelperDateTime::DateToIso($posted_val['edu_start_date']);
                $usereducation->end_date = JobsHelperDateTime::DateToIso($posted_val['edu_end_date']);

                $usereducation->start_date = JobsHelperDateTime::isoDateToUTC($usereducation->start_date); //store all in UTC
                $usereducation->end_date = JobsHelperDateTime::isoDateToUTC($usereducation->end_date); //store all in UTC
            }

            $user = JFactory::getUser();
            $usereducation->userid = $user->id;

           if ($usereducation->id) {
                // edit custom field prep
                $usereducation->start_date = JobsHelperDateTime::DateToIso($posted_val['start_date'.$usereducation->id]);
                $usereducation->end_date = JobsHelperDateTime::DateToIso($posted_val['end_date'.$usereducation->id]);

                $usereducation->start_date = JobsHelperDateTime::isoDateToUTC($usereducation->start_date); //store all in UTC
                $usereducation->end_date = JobsHelperDateTime::isoDateToUTC($usereducation->end_date); //store all in UTC
            }
        }

        public function deleteUserducation($education_id,$userid)
        {
            $database = JFactory::getDbo();
            $user = JFactory::getUser();

            if (($user->id == $userid) && $education_id != "") {
                $database->setQuery("DELETE FROM `#__jobsfactory_education` WHERE `userid` = '" . $user->id .
                    "' AND `id` = '" . (int)$education_id . "'");
                $database->execute();
            }
            return $database->getAffectedRows();
        }

        function validateSave($item)
        {

            $cfg = JTheFactoryHelper::getConfig();
            $error = array();
            // Title validation
            if ($item->edu_institution == "" || !isset($item->edu_institution))
                $error[] = JText::_("COM_JOBS_EDU_INSTITUTION_CAN_NOT_BE_EMPTY");

            // Dates validations
            if (!$item->id) {

                $start_date = $item->start_date;
                $end_date = $item->end_date;

                if (!$start_date) {
                    $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID");
                }
                if (!$end_date) {
                    $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID");
                }

                $year = date("Y", strtotime($start_date));
                $month = date("m", strtotime($start_date));
                $day = date("d", strtotime($start_date));
                $year_end = date("Y", strtotime($end_date));
                $month_end = date("m", strtotime($end_date));
                $day_end = date("d", strtotime($end_date));

                if ($year < 1900 && $year > date('Y')) {
                    $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID") . "<br />";
                }
                if ($month < 1 && $month > 12) {
                    $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID") . "<br />";
                }
                if ($day < 1 && $day > 31) {
                    $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID") . "<br />";
                }


                if ($year_end < 1900 && $year_end > date('Y')) {
                    $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID") . "<br />";
                }
                if ($month_end < 1 && $month_end > 12) {
                    $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID") . "<br />";
                }
                if ($day_end < 1 && $day_end > 31) {
                    $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID") . "<br />";
                }

                $datedif = mktime(0, 0, 0, $month_end, $day_end, $year_end) - mktime(0, 0, 0, $month, $day, $year);
                if ($datedif < 0) {
                    $error[] = JText::_("COM_JOBS_ERR_END_BIGGER_THAN_START") . "<br />";
                }

                $datedif_Start = mktime(0, 0, 0, date("m"), date("d"), date("Y")) - mktime(0, 0, 0, $month, $day, $year);
                if ($datedif_Start < 0) {
                    $error[] = JText::_("COM_JOBS_ERR_START_BIGGER_THAN_NOW") . "<br />";
                }

                $datedif_End = mktime(0, 0, 0, date("m"), date("d"), date("Y")) - mktime(0, 0, 0, $month_end, $day_end, $year_end);
                if ($datedif_End < 0) {
                    $error[] = JText::_("COM_JOBS_ERR_END_BIGGER_THAN_NOW") . "<br />";
                }

            }

            //Custom Validations
            if (!$item->check())
                $error = array_merge($error, $item->_validation_errors);


            return $error;
        }

        function getNrFieldsWithFilters()
        {
            $database = $this->getDBO();
            $database->setQuery("select count(*) from #__jobsfactory_fields where page='resumes' and categoryfilter=1");
            return $database->loadResult();
        }


    }
