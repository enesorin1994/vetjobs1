<?php
    /**------------------------------------------------------------------------
    com_jobsfactory - Jobs Factory 1.6.5
    ------------------------------------------------------------------------
     * @author thePHPfactory
     * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
     * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     * Websites: http://www.thePHPfactory.com
     * Technical Support: Forum - http://www.thePHPfactory.com/forum/
     * @package: Jobs
    -------------------------------------------------------------------------*/

    defined('_JEXEC') or die('Restricted access');

    class jobsModelCompanies extends jobsModelGeneric
    {
        var $_name = 'Companies';
        var $name = 'Companies';
        var $page = 'companies';
        var $context = 'companiesModel';

        var $order_fields = array(
            'name'            => "c.name",
            'username'        => "u.username",
            'city'            => "c.city",
            'jobs_available'  => "no_available_jobs",
            'id'              => "c.id"

        );
        var $knownFilters = array(
            'keyword'          => array('type' => 'string'),
            'filter_archive'   => array('type' => 'string'),
            'userid'           => array('type' => 'int'),
            'city'             => array('type' => 'string'),
            'filter_order'     => array('type' => 'string', 'default' => 'name'),
            'filter_order_Dir' => array('type' => 'string', 'default' => 'DESC')
        );

        function buildQuery()
        {
            $user   = JFactory::getUser();
            $db     = JFactory::getDBO();
            $task   = JFactory::getApplication()->input->getCmd('task');
            $cfg    = JTheFactoryHelper::getConfig();
            $config = JFactory::getConfig();

            $datenow = JFactory::getDate('now', $config->get('config.offset'));

            //this binds to the query object everything that is related to custom fields
            $profile = JobsHelperTools::getUserProfileObject();
            $tableName = $profile->getIntegrationTable();
            $tableKey = $profile->getIntegrationKey();
            $integration = $profile->getIntegrationArray(); //fieldmap
            $fieldName = $profile->getFilterField('name');

            $query = JTheFactoryDatabase::getQuery();
            $query->from($tableName, 'c');
            //$query->from('#__jobsfactory_users', 'c');
           // LEFT JOIN `$tableName` prof ON a.userid= prof.`$tableKey`  \r\n

            /**
             *
             *  Fields to select
             *
             * */
            $query->select('`c`.*');
            $query->select('`c`.id AS companyId');
            $query->select('`c`.`'.$fieldName.'`');
            $query->select('`u`.`username`');
            if ($user->id) {
               $query->select("count( DISTINCT w.id) as watchListed_flag ");
            }
            if ($user->id) {
                $query->select("IF ( `c`.$tableKey = '" . (int)$user->id . "', 1, 0 ) AS `is_my_company`");
                //$query->select("IF ( `c`.`userid` = '" . (int)$user->id . "', 1, 0 ) AS `is_my_company`");
            } else {
                $query->select("0  AS `is_my_company`");
            }
            if ($cfg->jobs_allow_rating) {
                $query->select('`ur`.user_rating, `ur`.users_count');
               // $query->select('r.rating_user as my_rating_user');
            }

            /**
             *
             *  Where conditions
             *
             * */

            switch ($task) {
                //live jobs
                default:
                case 'listcompanies':
                    $fieldisVisible = $profile->getFilterField('isVisible');
                    $fieldisCompany = $profile->getFilterField('isCompany');

                    //$cfg->company_groups
                    //$fieldmap=$this->integrationObject->getIntegrationArray();

                    $query->where("`c`.$fieldisVisible = 1");
                    $query->where("`c`.$fieldisCompany = 1");

                    break;
                case 'watchlist':
                    $query->where(array("`cat`.`status`=1", "`cat`.`status` IS NULL"), "OR");
                    $query->where("`fav_table`.userid ='" . $user->id . "'");
                    $query->where("`j`.`start_date` <= '" . $datenow->toSql(false) . "'");
                    break;
            }

            if ($this->getState('filters.keyword')) {
                $keyword = $db->escape($this->getState('filters.keyword'));

                $w = array();
                $w[] = 'c.title LIKE \'%' . $keyword . '%\'';
                if ($this->getState('filters.indesc')) {
                    $w[] = 'c.shortdescription LIKE \'%' . $keyword . '%\'';
                    $w[] = 'c.about_us LIKE \'%' . $keyword . '%\'';
                }
                $query->where($w, 'OR');
            }

            if ($this->getState('filters.userid')) {
                $query->where(" c.$tableKey = '" . $db->escape($this->getState('filters.userid')) . "' ");
            }

            if ($user->id) {
                $query->join('left', '#__jobsfactory_watchlist_companies', 'w', '`w`.`companyid`=`c`.`'.$tableKey.'` ' . (($task == 'listcompanies') ? "AND `w`.`userid` = '{$user->id}'" : ""));
            }

            /**
             *  Joins
             * */
            $query->join('left', '#__jobsfactory_jobs', 'j', '`j`.`userid`=`c`.`'.$tableKey.'`' );
            $query->join('inner','#__users','u', '`c`.`'.$tableKey.'` = `u`.`id`');

            if ($cfg->jobs_allow_rating) {
                $query->join('left', '#__jobsfactory_users_rating', 'ur', '`c`.`' . $tableKey . '` = `ur`.`userid`');
               // $query->join('left', '#__jobsfactory_ratings', 'r', '`c`.`' . $tableKey . '` = `r`.`userid`');
            }

            $queriedTables = $query->getQueriedTables();
            // Required ordering filter
            $filter_order = $this->getState('filters.filter_order');

            if ($filter_order == 'name'){
                //$table = $profile->getFilterTable('name');
                $alias = array_search($tableName, $queriedTables);
                $filter = $alias.'.'.$fieldName;
            } elseif ($filter_order == 'city') {
                $alias = array_search($tableName, $queriedTables);
                $filter = $alias.'.'.$profile->getFilterField('city');
            } else {
                $filter = $this->order_fields[$filter_order];
            }

            if ($filter_order) {
                $filter_order_Dir = $this->getState('filters.filter_order_Dir');
                $query->order($db->escape($filter . ' ' . $filter_order_Dir));
            }

            $query->group('`c`.`id`');

            //$profile = JobsHelperTools::getUserProfileObject();
            //this binds to the query object everything that is related to custom fields

            if ($this->getState('filters.city')) {
                $table = $profile->getFilterTable('city');
                $field = $profile->getFilterField('city');
                $alias = array_search($table, $queriedTables);
                $query->where(' (`' . $alias . '`.`' . $field . '` =\'' . $db->escape($this->getState('filters.city')) . '\') ');
            }

            return $query;
        }

        function getTotal()
        {

            if (empty($this->total)) {
                $db = JFactory::getDbo();
                $query = $this->buildQuery();

                $query->set('select', "count(distinct `c`.id)");
                $query->set('order', null);
                $query->set('group', null);

                $db->setQuery((string)$query);
                $this->total = $db->loadResult();
            }
            return $this->total;
        }

        function getCompanyJobsList($id)
        {
            $db = JFactory::getDbo();
            $query = "SELECT j.*, a.userid, a.comments, c.catname "
                . " ,IF ( j.show_candidate_nr = 1 , COUNT(DISTINCT a.userid) ,0) as nr_applicants "
                . " FROM #__jobsfactory_jobs j "
                . " LEFT JOIN #__jobsfactory_candidates a ON j.id = a.job_id  "
                . " LEFT JOIN #__jobsfactory_categories c ON j.cat = c.id "
                . " WHERE j.userid = '{$id}' "
                . " LIMIT 0,10 ";

            $db->setQuery($query);
            return $db->loadObjectList();
        }

    }

