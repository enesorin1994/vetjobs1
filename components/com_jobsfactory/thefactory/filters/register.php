<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: events
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	$pathSegments = explode(DIRECTORY_SEPARATOR, __DIR__);
	$compName     = '';
	foreach ($pathSegments as $k => $dir) {
		if (preg_match('/^com_(\w+)$/', $dir)) {
			$compName = $dir;
		}
	}

	require_once(JPATH_ADMINISTRATOR . '/components/' . $compName . '/thefactory/' . basename(dirname(__FILE__)) . '/register.php');

