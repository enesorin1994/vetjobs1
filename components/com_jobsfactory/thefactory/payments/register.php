<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryPaymentsRegister
	 */
	class JTheFactoryPaymentsRegister
	{
		/**
		 * registerModule
		 *
		 * @static
		 *
		 * @param null $app
		 */
		public static function registerModule($app = NULL)
		{

			if (!$app) $app = JTheFactoryApplication::getInstance();
			if ($app->getIniValue('use_payment_gateways')) {
				JFactory::getDocument()->addStyleSheet(JUri::base() . '/components/' . APP_EXTENSION . '/thefactory/payments/assets/css/payments.front.css');
				JHtml::addIncludePath($app->app_path_admin . 'payments/html');
				JLoader::register('JTheFactoryPricingHelper', $app->app_path_admin . 'payments/helper/pricing.php');
				JLoader::register('JTheFactoryBalanceController', $app->app_path_front . 'payments/controllers/balance.php');
				JLoader::register('JTheFactoryOrderProcessorController', $app->app_path_front . 'payments/controllers/orderprocessor.php');
				JLoader::register('JTheFactoryCartController', $app->app_path_front . 'payments/controllers/cart.php');
				JLoader::register('JTheFactoryOrder', $app->app_path_admin . 'payments/classes/orders.php');
				if ($app->getIniValue('use_events')) {
					JTheFactoryPricingHelper::registerEvents();
					JTheFactoryEventsHelper::registerEvents($app->app_path_admin . 'payments/events');
				}

				if($app->getIniValue('use_shopping_cart')){
					JLoader::register('JTheFactoryCartHelper', $app->app_path_admin . 'payments/helper/cart.php');
				}

				JTheFactoryHelper::loadModuleLanguage('payments');
			}
		}
	} // End Class
