<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.application.component.view');

	/**
	 * Class JTheFactoryViewCart
	 */
	class JTheFactoryViewCartCart extends JViewLegacy
	{
		/**
		 * display
		 *
		 * @param null $tpl
		 *
		 * @return mixed|void
		 */
		public function display($tpl = NULL)
		{
			$doc = JFactory::getDocument();
			$doc->addScript(JUri::root() . 'components/' . APP_EXTENSION . '/thefactory/payments/assets/js/cart.js');

			parent::display($tpl);
		}

	}

