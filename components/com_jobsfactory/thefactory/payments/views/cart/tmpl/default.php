<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
	use Joomla\Registry\Registry as JRegistry;

	$doc = JFactory::getDocument();
	$doc->addStyleDeclaration("
		.balance-more-info{
			display:block;
			font-size:11px;
			font-style:italic;
		}
	");

?>
<h1 class = "page-title"><?php echo JText::_('FACTORY_CART_PAGE_TITLE'); ?></h1>

<form action = "index.php?option=<?php echo APP_EXTENSION; ?>&task=cart.initcheckout" class = "" method = "post">

	<table width = "100%" class = "table_cart_box" data-cart-extension = "<?php echo APP_EXTENSION; ?>">
		<thead>
		<tr>
			<th>#</th>
			<th><?php echo JText::_('FACTORY_CART_ITEM_NAME'); ?></th>
			<th><?php echo JText::_('FACTORY_CART_ITEM_PRICE'); ?></th>
			<th><?php echo JText::_('FACTORY_CART_ITEM_QUANTITY'); ?></th>
			<th><?php echo JText::_('FACTORY_CART_ITEM_TOTAL'); ?></th>
			<th></th>
		</tr>
		</thead>

		<tbody>

		<?php if (count($this->items)):
			foreach ($this->items as $k => $item): ?>

				<tr class = "item-row row_item_<?php echo $k; ?>" data-item-index = "<?php echo $k; ?>">
					<td>
						<?php
							static $i = 1;
							echo $i;
							$i++;
						?>.
					</td>

					<td style = "text-align: left">
						<?php echo JText::_(strtoupper(APP_EXTENSION . '_PAY_FOR_' . str_replace(' ', '_', $item->itemname))); ?>
						<span class = "cart-item-description"><?php echo isset($item->itemdetails) ? '<br />' . $item->itemdetails : '' ?></span>
					</td>

					<td><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $item->price) . ' ' . $item->currency; ?></td>
					<td style = "width:75px;height: 37px;">
						<input type = "hidden" name = "items[<?php echo $k; ?>][itemname]" value = "<?php echo $item->itemname; ?>" />
						<input type = "hidden" name = "items[<?php echo $k; ?>][price]" value = "<?php echo $item->price; ?>" />
						<input type = "hidden" name = "items[<?php echo $k; ?>][currency]" value = "<?php echo $item->currency; ?>" />
						<input type = "hidden" name = "items[<?php echo $k; ?>][item_total]" value = "<?php echo $item->price * $item->quantity; ?>" />

						<?php $params = new JRegistry($item->params); ?>
						<?php if ($params->get('isUnit')) { ?>
							<input type = "hidden" name = "items[<?php echo $k; ?>][quantity]" value = "<?php echo $item->quantity; ?>" />
							<?php echo $item->quantity; ?>
						<?php } else { ?>
							<input type = "text" name = "items[<?php echo $k; ?>][quantity]" class = "item-quantity input-mini" value = "<?php echo $item->quantity; ?>" />
						<?php } ?>

					</td>
					<td class = "item-total" style = "width: 130px;">
						<span class = "item-total-price"><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $item->price * $item->quantity); ?></span>
						<span class = "item-total-currency"><?php echo $item->currency; ?></span>
					</td>
					<td style = "width: 18px;">
						<?php echo JHtml::_('link', 'javascript:void(0);',
							JHtml::_('image', JUri::root() . 'components/' . APP_EXTENSION . '/images/delete.png', ''),
							array('class' => 'delete-cart-item')) ?>
					</td>
				</tr>
				<?php $this->cartTotal += $item->price * $item->quantity; ?>
			<?php endforeach; ?>
			<!-- For js trigger -->
			<tr class = "cart-no-items hide-js-no-items-info">
				<td colspan = "6"><?php echo JText::_('FACTORY_CART_NO_ITEMS'); ?></td>
			</tr>
		<?php else: ?>
			<tr>
				<td colspan = "6" class = "cart-no-items"><?php echo JText::_('FACTORY_CART_NO_ITEMS'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>

		<tfoot>
		<tr>
			<td colspan = "6">&nbsp;</td>
		</tr>

		<?php if ((float)$this->balance['balance'] && count($this->items)): ?>
			<tr class = "balance-row">
				<td colspan = "2">
					<span class = "balance-info"><?php echo JText::sprintf('FACTORY_CART_BALANCE_INFO', JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $this->balance['balance']), $this->balance['currency']); ?></span>
					<span class = "balance-more-info"><?php echo JText::_('FACTORY_CART_BALANCE_MORE_INFO'); ?></span>
				</td>
				<td colspan = "4">
					<input type = "text" name = "balance_amount" class = "balance-amount input-mini" value = "<?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', 0.00); ?>" />
					<?php echo $this->balance['currency']; ?>
				</td>
			</tr>
		<?php endif; ?>

		<tr>
			<td colspan = "3" align = "right"><span
					class = "cart-total-label"><?php echo JText::_('FACTORY_CART_TOTAL'); ?></span></td>
			<td colspan = "3" class = "cart-total-box">
				<span class = "cart-total cart-total-value">
					<?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $this->cartTotal); ?>
				</span>
				<span class = "cart-total cart-total-currency"><?php echo $this->currency; ?></span>
			</td>
		</tr>
		</tfoot>
	</table>

	<!-- Display checkout button only if we have items in cart -->
	<?php if (count($this->items)): ?>
		<div class = "form-actions">
			<input type = "submit" name = "cart_to_checkout" class = "btn btn-wide btn-block btn-cart-checkout" value = "<?php echo JText::_('FACTORY_CART_BUTTON_CHECKOUT'); ?>" />
		</div>
	<?php endif; ?>
</form>



