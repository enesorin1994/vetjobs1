<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<h1><?php echo JText::_("FACTORY_ADD_FUNDS_TO_YOUR_ACCOUNT"); ?></h1>

<h2><?php echo JText::_("FACTORY_YOUR_CURRENT_BALANCE_IS"), ": ", number_format($this->balance->balance, 2), ' ', $this->balance->currency; ?></h2>

<div>
	<form action = "<?php echo JUri::root(); ?>index.php" method = "post" name = "paymentform">
		<input name = "option" type = "hidden" value = "<?php echo APP_EXTENSION; ?>" />
		<input name = "task" type = "hidden" value = "balance.checkout" />
		<input name = "Itemid" type = "hidden" value = "<?php echo $this->Itemid; ?>" />
		<table width = "55%">
			<tr>
				<td><?php echo JText::_("FACTORY_ADD"); ?></td>
				<td><input name = "amount" value = "" size = "6" type = "text" class = "inputbox" /> <?php echo $this->currency; ?></td>
				<td align = "center"><input type = "submit" class = "button btn" name = "submit" value = "<?php echo JText::_("FACTORY_SUBMIT"); ?>" /></td>
			</tr>
		</table>
	</form>
</div>
