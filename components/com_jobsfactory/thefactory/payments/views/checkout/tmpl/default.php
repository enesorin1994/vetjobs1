<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<h1 class = "order-title"><?php echo JText::_('FACTORY_CHECKOUT_YOUR_ORDER'), $this->order->id; ?> </h1>
<h3 class = "order-top-info"><?php echo JText::_('FACTORY_ORDER_DETAILS'); ?></h3>

<table width = "100%" class = "table_order_details_box">
	<thead>
	<tr>
		<th>#</th>
		<th><?php echo JText::_('FACTORY_ORDER_ITEM'); ?></th>
		<th><?php echo JText::_('FACTORY_QUANTITY'); ?></th>
		<th><?php echo JText::_('FACTORY_UNIT_PRICE'); ?></th>
		<th><?php echo JText::_('FACTORY_TOTAL_PRICE'); ?></th>
	</tr>
	</thead>
	<?php for ($i = 0; $i < count($this->order_items); $i++): ?>
		<tr>
			<td><?php echo $i + 1; ?>.</td>
			<td style = "text-align: left">
				<?php echo JText::_(strtoupper(APP_EXTENSION . '_PAY_FOR_' . str_replace(' ', '_', $this->order_items[$i]->itemname))); ?>
				<span class = "checkout-item-description"><?php echo isset($this->order_items[$i]->itemdetails) ? '<br />' . $this->order_items[$i]->itemdetails : ''; ?></span>
			</td>
			<td><?php echo $this->order_items[$i]->quantity; ?></td>
			<td><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $this->order_items[$i]->price), " ", $this->order_items[$i]->currency; ?></td>
			<td><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', ($this->order_items[$i]->price * $this->order_items[$i]->quantity)), " ", $this->order_items[$i]->currency; ?></td>

		</tr>
	<?php endfor; ?>
	<td colspan = "5">&nbsp;</td>
	<tfoot>
	<tr>
		<td colspan = "4" align = "right"><span
				class = "order_total_label"><?php echo JText::_('FACTORY_ORDER_TOTAL'); ?></span></td>
		<td><span
				class = "order_total_value"><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $this->order->order_total), " ", $this->order->order_currency; ?></span>
		</td>
	</tr>
	</tfoot>
</table>
<br />
<br />
<br />

<table width = "100%" class = "payment_gateways" cellpadding = "0" cellspacing = "0" border = "0">
	<thead>
	<tr>
		<th><?php echo JText::_('FACTORY_CHOOSE_A_PAYMENT_GATEWAY'); ?></th>
	</tr>
	</thead>
</table>

<div class = "payment_gateways">

	<?php if (count($this->payment_gateways)): ?>
	<div class = "row-fluid">
		<?php for ($i = 0;
			$i < count($this->payment_gateways);
			$i++): ?>
		<!-- Start new row at each 4 columns -->
		<?php if ($i !== 0 && $i % 3 == 0): ?>
	</div>
	<div class = "row-fluid">
		<?php endif; ?>

		<div class = "span4">
			<div class = "gateway_name"><?php echo JText::_(strtoupper(APP_EXTENSION . '_' . str_replace(' ', '_', $this->payment_gateways[$i]->fullname))); ?></div>
			<div class = "gateway_form"><?php echo $this->payment_forms[$i] ?></div>
		</div>
		<?php endfor; ?>

		<?php else: ?>
			<div class = "span12"><?php echo JText::_('FACTORY_NO_PAYMENTS_GATEWAYS_DEFINED'); ?></div>
		<?php
			endif;
		?>
	</div>


</div>
