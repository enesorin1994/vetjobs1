<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<span class="v_spacer_15"></span>
<!-- User Profile Tabs -->
<div align="left" style = "text-align:left;" xmlns = "http://www.w3.org/1999/html">
    <ul id="job_tabmenu" class="nav nav-tabs">
        <li class="inactive">
            <a class = "inactive1" href = "<?php echo JobsHelperRoute::getUserdetailsRoute(); ?>">
                <?php echo JText::_("COM_JOBS_MY_PROFILE");?></a>
        </li>
        <li class = "active">
            <a href = "<?php echo JobsHelperRoute::getPaymentsHistoryRoute();?>">
                <?php echo JText::_("COM_JOBS_PAYMENT_BALANCE");?></a>
        </li>
    </ul>
</div>

<span class="v_spacer_15"></span>

<div class = "box_balance_info">

	<table id = "table_box_balance_info_box">
		<!-- Balance status -->
		<tr>
			<td><?php echo JText::_("FACTORY_YOUR_CURRENT_BALANCE_IS"); ?>:</td>
			<td align = "right"><span class = "label"><span
						class = "my_balance_amount"><?php echo number_format($this->balance->balance, 2) . '</span> ' . $this->balance->currency; ?></span>
			</td>
		</tr>
		<?php if ($this->withdraw_enabled) : ?>
			<!-- Withdraw until now -->
			<tr>
				<td><span class = "grey"><?php echo JText::_('FACTORY_LEGEND_REQ_WITHDRAWAL'); ?>:</span></td>
				<td align = "right"><span class = "label"><span
							class = "grey"><?php echo number_format($this->balance->req_withdraw, 2) . ' ' . $this->balance->currency; ?></span></span>
				</td>
			</tr>
			<!-- Withdraw pending -->
			<tr>
				<td><?php echo JText::_('FACTORY_LEGEND_WITHDRAWN'); ?>:</td>
				<td align = "right"><span
						class = "label"><?php echo number_format($this->balance->withdrawn_until_now, 2) . ' ' . $this->balance->currency; ?></span>
				</td>
			</tr>
		<?php endif; ?>
	</table>

</div>

<!-- Add funds to site balance -->
<div class = "box_balance_actions">
	<form action = "<?php echo JUri::root(); ?>index.php" method = "post" name = "paymentform">
		<input name = "option" type = "hidden" value = "<?php echo APP_EXTENSION; ?>" />
		<input name = "task" type = "hidden" value = "balance.checkout" />
		<input name = "Itemid" type = "hidden" value = "<?php echo $this->Itemid; ?>" />
		<table width = "65%" id = "table_add_funds_box">
			<tr>
				<td colspan = "2"><?php echo JText::_("FACTORY_ADD_FUNDS_TO_YOUR_SITE_BALANCE"); ?></td>
			</tr>
			<tr>
				<td><input name = "amount" value = "" size = "6" type = "text" class = "inputbox" />
					<!-- 1 space --><?php echo $this->currency; ?></td>
				<td align = "center">
					<input type = "submit"
					       name = "submit"
					       class = "button"
					       value = "<?php echo JText::_("FACTORY_BUTTON_PURCHASE"); ?>"
						/>
				</td>
			</tr>
		</table>
	</form>
</div>

<span class = "v_spacer_15"></span>
<?php if ($this->withdraw_enabled) : ?>

	<!-- Withdraw funds from site balance -->
	<div class = "box_balance_actions" style = "margin-top: 13px;">

		<form action = "<?php echo JUri::root(); ?>index.php" method = "post" name = "paymentform">
			<input name = "option" type = "hidden" value = "<?php echo APP_EXTENSION; ?>" />
			<input name = "task" type = "hidden" value = "balance.reqWithdraw" />
			<input name = "Itemid" type = "hidden" value = "<?php echo $this->Itemid; ?>" />
			<input name = "currency" type = "hidden" value = "<?php echo $this->currency ?>" />

			<table width = "auto" id = "table_withdraw_funds_box">
				<tr>
					<td colspan = "2"><?php echo JText::_("FACTORY_WITHDRAW_FUNDS_FROM_YOUR_SITE_BALANCE"); ?></td>
				</tr>
				<tr>
					<td style = "">
						<input name = "amount" id = "withdraw_amount" value = "" size = "6" type = "text" class = "inputbox"
							<?php if ($this->balance->req_withdraw >= $this->balance->balance): ?>
								title = "<?php echo JText::_('FACTORY_TITLE_REQ_EXCEED_BALANCE'); ?>"
								disabled = "disabled"
							<?php endif; ?>
							/> <!-- 1 space --><?php echo $this->currency ?>
					</td>
					<td align = "center">
						<input type = "submit" name = "submit" class = "button"
						       value = "<?php echo JText::_("FACTORY_BUTTON_WITHDRAW"); ?>"
							<?php if ($this->balance->req_withdraw >= $this->balance->balance): ?>
								title = "<?php echo JText::_('FACTORY_TITLE_REQ_EXCEED_BALANCE'); ?>"
								disabled = "disabled"
							<?php else: ?>
								onclick = "if(document.getElementById('withdraw_amount').value) {
									return confirm('<?php echo JText::_('FACTORY_CONFIRM_REQUEST'); ?> ' +
									document.getElementById('withdraw_amount').value +
									' <?php echo $this->currency; ?>') ?  true : false;
									} else {
									alert('<?php echo JText::_('FACTORY_ALERT_NO_AMOUNT_REQ'); ?> ');
									document.getElementById('withdraw_amount').focus();
									return false;
									}
									"
							<?php endif; ?>
							/>
					</td>
				</tr>
			</table>
		</form>

	</div>
<?php endif; ?>

<span class = "v_spacer_15"></span>
<span class = "v_spacer_15"></span>

<!-- My Payment History -->
<div style = "float: left;width: 100%;">
	<h3 style = "text-transform: uppercase;"><?php echo JText::_("FACTORY_PAYMENTS_HISTORY"); ?></h3>

	<form action = "<?php echo JUri::root(); ?>index.php?option=<?php echo APP_EXTENSION; ?>&task=payments.history&Itemid=<?php echo $this->Itemid; ?>"
	      method = "post" name = "paymentform">

		<div style = "text-align: right;margin-bottom: 5px;">
	        <span class = "label"><?php echo JText::_('FACTORY_FILTER_TYPE') . '</span>: ' . $this->lists['type']; ?>
		        <span class = "label"><?php echo JText::_('FACTORY_FILTER_STATUS') . '</span>: ' . $this->lists['status']; ?>
		</div>
		<table id = "table_payments_history_box" style = "width: 100%;">
			<thead>
			<tr>
				<th><?php echo JText::_("FACTORY_ORDER"); ?></th>
				<th width = "180px"><?php echo JText::_("FACTORY_DATE"); ?></th>
				<th><?php echo JText::_('FACTORY_ORDER_DETAILS'); ?></th>
				<th style = "width: 100px;"><?php echo JText::_("FACTORY_AMOUNT"); ?></th>
				<th style = "width: 70px;"><?php echo JText::_("FACTORY_STATUS"); ?></th>
			</tr>
			</thead>

			<?php foreach ($this->orders as $order): ?>
				<tr class = "payment_order_<?php echo $order->status; ?>">
					<td class = "integer">
						<?php echo JHtml::_('link', 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $order->id . '&Itemid=' . $this->Itemid, str_pad($order->id, 5, '0', STR_PAD_LEFT)); ?>
					</td>
					<td><?php echo JHtml::_('date', $order->orderdate, $this->cfg->date_format . ' ' . $this->cfg->date_time_format); ?></td>
					<td><?php echo JHtml::_('payments.orderdetails', $order->id); ?></td>
					<td class = "integer"
					    style = "white-space: nowrap;"><?php echo number_format($order->order_total, 2), " ", $order->order_currency; ?></td>

					<td>
						<?php echo JHtml::_('link', "index.php?option=" . APP_EXTENSION . "&task=orderprocessor.details&orderid=" . $order->id . "&Itemid=" . $this->Itemid, JHtml::_('payments.statustext', $order->status)); ?>
					</td>

				</tr>
			<?php endforeach; ?>
		</table>


		<table width = "100%" class = "table_pagination_box pagination">
			<tr>
				<td style = "text-align: left;"><span
						class = "label"><?php echo $this->pagination->getPagesCounter(); ?></span></td>
				<td style = "text-align: right;">
					<span class = "label"><?php echo JText::_('JGLOBAL_DISPLAY_NUM'); ?>&#160;</span>
					<?php echo $this->pagination->getLimitBox(); ?></td>
			</tr>
			<tr class = "pagination_links">
				<td colspan = "2"><?php echo $this->pagination->getPagesLinks(); ?></td>
			</tr>
		</table>

	</form>
</div>
