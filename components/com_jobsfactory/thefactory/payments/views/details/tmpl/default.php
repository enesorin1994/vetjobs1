<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<h1 class = "order-title">
	<?php echo JText::_('FACTORY_ORDER_DETAILS'); ?>
	<span style = 'float: right;'>
	    <?php echo JHtml::_('link', 'index.php?option=' . APP_EXTENSION . '&task=payments.history&Itemid=' . $this->Itemid, JText::_('FACTORY_BACK')); ?>
    </span>
</h1>
<h3 class = "order-top-info"><?php echo JText::_('FACTORY_ORDER_NUMBER') . ': <span style="font-size:18px;font-weight:bold;">' . $this->order->id . '</span>'; ?></h3>
<h3 class = "order-top-info"><?php echo JText::_('FACTORY_ORDER_DATE'), ': ', JHtml::_('date', $this->order->orderdate, $this->cfg->date_format . ' ' . $this->cfg->date_time_format); ?></h3>
<h3 class = "order-top-info"><?php echo JText::_('FACTORY_ORDER_STATUS'), ': '; ?>
	<?php
		if ($this->order->status == 'P') {
			echo JText::_("FACTORY_PENDING");
			// For commission, cancel operation cannot be possible
			if (!in_array('comission', JArrayHelper::getColumn($this->order_items, 'itemname'))) {
				echo '<span style="float:right;">' . JHtml::link('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.cancel&orderid=' . $this->order->id . '&Itemid=' . $this->Itemid, JText::_('FACTORY_CANCEL'), array('class' => 'cancel_link_button')) . '</span>';
			}
		}
	?>
	<?php if ($this->order->status == 'C') echo JText::_("FACTORY_COMPLETED"); ?>
	<?php if ($this->order->status == 'X'): ?>
			<?php echo JText::_("FACTORY_CANCEL") . ' � ' . JHtml::_('link', 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.onpending&orderid=' . $this->order->id . '&Itemid=' . $this->Itemid, JText::_('FACTORY_ORDER_PAY_CANCELED')); ?>
		<?php endif; ?>
</h3>
<?php if ($this->order_gateway_name): ?>
	<h3 class = "order-top-info"><?php echo JText::_('FACTORY_ORDER_GATEWAY_USED'), ': '; ?><?php echo $this->order_gateway_name; ?></h3>
<?php endif; ?>

<table width = "100%" class = "table_order_details_box">
	<thead>
	<tr>
		<th>#</th>
		<th><?php echo JText::_('FACTORY_ORDER_ITEM'); ?></th>
		<th><?php echo JText::_('FACTORY_QUANTITY'); ?></th>
		<th><?php echo JText::_('FACTORY_UNIT_PRICE'); ?></th>
		<th><?php echo JText::_('FACTORY_TOTAL_PRICE'); ?></th>
	</tr>
	</thead>
	<?php for ($i = 0; $i < count($this->order_items); $i++): ?>
		<tr>
			<td><?php echo $i + 1; ?>.</td>
			<td style = "text-align: left">
				<?php echo JText::_(strtoupper(APP_EXTENSION . '_PAY_FOR_' . str_replace(' ', '_', $this->order_items[$i]->itemname))); ?>
				<span class = "checkout-item-description"><?php echo isset($this->order_items[$i]->itemdetails) ? '<br />' . $this->order_items[$i]->itemdetails : ''; ?></span>
			</td>
			<td><?php echo $this->order_items[$i]->quantity; ?></td>
			<td><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $this->order_items[$i]->price), " ", $this->order_items[$i]->currency; ?></td>
			<td><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', ($this->order_items[$i]->price * $this->order_items[$i]->quantity)), " ", $this->order_items[$i]->currency; ?></td>

		</tr>
	<?php endfor; ?>
	<td colspan = "5">&nbsp;</td>
	<tfoot>
	<tr>
		<td colspan = "4" align = "right"><span
				class = "order_total_label"><?php echo JText::_('FACTORY_ORDER_TOTAL'); ?></span></td>
		<td><span
				class = "order_total_value"><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $this->order->order_total), " ", $this->order->order_currency; ?></span>
		</td>
	</tr>
	</tfoot>
</table>
<?php if ($this->order->status == 'P' && $this->user->id == $this->order->userid) : ?>
	<br />
	<br />
	<br />

	<table width = "100%" class = "payment_gateways" cellpadding = "0" cellspacing = "0" border = "0">
		<thead>
		<tr>
			<th>
				<?php echo JText::_('FACTORY_CHOOSE_A_PAYMENT_GATEWAY'); ?>
				<span style = "float: right">
    <span class = "hidden-phone" style = "color: #AAAAAA;font-size: 11px;">
        <span style = "color:#999999;font-weight:bold;">
	        <?php echo JText::_('FACTORY_NOTE'); ?>:</span>
	    <?php echo JText::_('FACTORY_YOU_CAN_RESUME_THE_PAYMENT_FOR_THIS_ORDER'); ?>
    </span>
        </span>
			</th>
		</tr>
		</thead>
	</table>

	<div class = "payment_gateways">

		<?php if (count($this->payment_gateways)): ?>
		<div class = "row-fluid">
			<?php for ($i = 0;
				$i < count($this->payment_gateways);
				$i++): ?>
			<!-- Start new row at each 4 columns -->
			<?php if ($i !== 0 && $i % 3 == 0): ?>
		</div>
		<div class = "row-fluid">
			<?php endif; ?>

			<div class = "span4">
				<div class = "gateway_name"><?php echo JText::_(strtoupper(APP_EXTENSION . '_' . str_replace(' ', '_', $this->payment_gateways[$i]->fullname))); ?></div>
				<div class = "gateway_form"><?php echo $this->payment_forms[$i]; ?></div>
			</div>
			<?php endfor; ?>

			<?php else: ?>
				<div class = "span12"><?php echo JText::_('FACTORY_NO_PAYMENTS_GATEWAYS_DEFINED'); ?></div>
			<?php
				endif;
			?>
		</div>
	</div>

<?php endif; ?>
