<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryCartController
	 */
	class JTheFactoryCartController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		public $name = 'Cart';
		/**
		 * _name
		 *
		 * @var string
		 */
		public $_name = 'Cart';
		/**
		 * modulename
		 *
		 * @var string
		 */
		public $modulename = 'Payments';

		/**
		 * details
		 *
		 * View and manage Cart details
		 *
		 * @throws Exception
		 */
		public function details()
		{
			$cfg         = JTheFactoryHelper::getConfig();
			$doc         = JFactory::getDocument();
			$input       = JFactory::getApplication()->input;
			$my          = JFactory::getUser();
			$session     = JFactory::getSession();
			$namespace   = APP_PREFIX . '_shopping_cart';
			$Itemid      = $input->getInt('Itemid', 0);
			$view        = $this->getView('cart');
			$cartSession = array();

			$doc->addStyleSheet(JURI::root() . 'components/' . APP_EXTENSION . '/templates/' . $cfg->theme . '/' . APP_PREFIX . '_template.css');

			JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . APP_EXTENSION . DS . 'thefactory' . DS . 'payments' . DS . 'models');
			$modelCurrency = JModelLegacy::getInstance('Currency', 'JTheFactoryModel');

			$modelbalance = JTheFactoryPricingHelper::getModel('balance');
			$balance      = $modelbalance->getUserBalance($my->id);

			if ($session->has('cart_info', $namespace)) {
				$cartSession = $session->get('cart_info', '', $namespace);
			}

			$view->items     = isset($cartSession->items) ? $cartSession->items : array();
			$view->nrItems   = isset($cartSession->quantity) ? $cartSession->quantity : 0;
			$view->currency  = $modelCurrency->getDefault();
			$view->cartTotal = 0;

			$view->cfg     = $cfg;
			$view->balance = get_object_vars($balance);
			$view->Itemid  = $Itemid;

			$view->display();

		}

		/**
		 * initCheckout
		 *
		 * Fire onBeforeCheckout event where a pending order is created with order items as session Cart content.
		 *
		 * Clear Cart Session and go to checkout.
		 */
		public function initCheckout()
		{
			$session   = JFactory::getSession();
			$namespace = APP_PREFIX . '_shopping_cart';

			$cartSession = $session->get('cart_info', NULL, $namespace);
			$my          = JFactory::getUser();

			if (!$cartSession) {
				return FALSE;
			}

			// If user paid from balance then add to order the balance order item
			if (isset($cartSession->balanceAmount) && 0 < $cartSession->balanceAmount) {
				$modelbalance = JTheFactoryPricingHelper::getModel('balance');
				$balance      = get_object_vars($modelbalance->getUserBalance($my->id));

				$item                 = new stdClass();
				$item->itemname       = 'debitbalance';
				$item->itemdetails    = JText::_(APP_EXTENSION . '_PAY_FOR_' . $item->itemname . '_DESC');
				$item->iteminfo       = 0;
				$item->price          = -$cartSession->balanceAmount;
				$item->currency       = $balance['currency'];
				$item->quantity       = 1;
				$item->params         = '';
				$cartSession->items[] = $item;

				$modelbalance->decreaseBalance($cartSession->balanceAmount, $my->id);
			}

			// Get cart size and new total
			$cartSession->quantity  = count($cartSession->items);
			$cartSession->cartTotal = 0;

			foreach ($cartSession->items as $item) {
				$cartSession->cartTotal += $item->price * $item->quantity;
			}

			// Go to checkout after create an order for all cart items
			JTheFactoryEventsHelper::triggerEvent('onBeforeCheckout', array($cartSession->items, JTheFactoryFiltersHelper::trigger('filterPriceToSql', $cartSession->cartTotal)));
		}

		/**
		 * addToCartAjax
		 *
		 */
		public function addToCartAjax()
		{
			//JTheFactoryEventsHelper::triggerEvent('onAddToCart', array($item));

			echo json_encode(array('session' => JFactory::getSession()));
			exit;
		}

		/**
		 * changeItemQuantity
		 *
		 * Ajax handler responsible for updating session Cart item quantity
		 *
		 */
		public function changeItemQuantity()
		{
			$session         = JFactory::getSession();
			$namespace       = APP_PREFIX . '_shopping_cart';
			$itemIndex       = $this->input->getInt('itemIndex');
			$itemNewQuantity = $this->input->getInt('itemNewQuantity');
			$sessCart        = NULL;

			if ($session->has('cart_info', $namespace) && isset($itemIndex)) {
				$sessCart = $session->get('cart_info', '', $namespace);

				// Update item quantity and item price accordingly to new quantity
				$sessCart->items[$itemIndex]->quantity = $itemNewQuantity;

				// Get cart size and new total
				$sessCart->quantity  = count($sessCart->items);
				$sessCart->cartTotal = 0;

				foreach ($sessCart->items as $item) {
					$sessCart->cartTotal += $item->price * $item->quantity;
				}

				//Prepare to display through javascript the item total price
				$sessCart->itemTotalPrice = JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $sessCart->items[$itemIndex]->price * $itemNewQuantity);
				// Prepare to display through javascript the recalculated cartTotal price
				$sessCart->cartTotal = JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $sessCart->cartTotal);
				$session->set('cart_info', $sessCart, $namespace);
			}

			echo json_encode(array('sessionCart' => $sessCart));
			exit;
		}

		/**
		 * removeItem
		 *
		 * Ajax handler responsible for removing an item from session Cart
		 *
		 */
		public function removeItem()
		{
			$session   = JFactory::getSession();
			$namespace = APP_PREFIX . '_shopping_cart';
			$itemIndex = $this->input->getInt('itemIndex');
			$sessCart  = NULL;

			if ($session->has('cart_info', $namespace) && isset($itemIndex)) {
				$sessCart = $session->get('cart_info', '', $namespace);
				unset($sessCart->items[$itemIndex]);

				// Update cart size
				$sessCart->quantity  = count($sessCart->items);
				$sessCart->cartTotal = 0;

				foreach ($sessCart->items as $item) {
					$sessCart->cartTotal += $item->price * $item->quantity;
				}

				// Prepare to display through javascript the recalculated cartTotal price
				$sessCart->cartTotal = JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $sessCart->cartTotal);
				$session->set('cart_info', $sessCart, $namespace);
			}

			echo json_encode(array('sessionCart' => $sessCart));
			exit;
		}

		public function payFromBalance()
		{
			$session       = JFactory::getSession();
			$my            = JFactory::getUser();
			$namespace     = APP_PREFIX . '_shopping_cart';
			$balanceAmount = $this->input->getString('balanceAmount');
			$sessCart      = NULL;

			if ($session->has('cart_info', $namespace)) {
				$sessCart = $session->get('cart_info', '', $namespace);

				$sessCart->cartTotal = 0;

				foreach ($sessCart->items as $item) {
					$sessCart->cartTotal += $item->price * $item->quantity;
				}

				$balanceAmount = JTheFactoryFiltersHelper::trigger('filterPriceToSql', $balanceAmount);

				$modelbalance = JTheFactoryPricingHelper::getModel('balance');
				$balance      = $modelbalance->getUserBalance($my->id);

				if ($balanceAmount > $balance->balance) {

					echo json_encode(array('sessionCart' => $sessCart, 'exceedBalance' => TRUE));
					exit;
				}


				if ($balanceAmount > $sessCart->cartTotal) {

					echo json_encode(array('sessionCart' => $sessCart, 'negativeCart' => TRUE));
					exit;

				}

				$sessCart->cartTotal = JTheFactoryFiltersHelper::trigger('filterPriceToSql', $sessCart->cartTotal) - $balanceAmount;
				// Store in session the balance amount
				// in order to change accordingly the user balance after checkout.
				$sessCart->balanceAmount = $balanceAmount;

				// Prepare to display through javascript the recalculated cartTotal price
				$sessCart->cartTotal = JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $sessCart->cartTotal);
				$session->set('cart_info', $sessCart, $namespace);
			}

			echo json_encode(array('sessionCart' => $sessCart));
			exit;
		}
	}
