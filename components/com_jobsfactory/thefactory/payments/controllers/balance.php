<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryBalanceController
	 */
	class JTheFactoryBalanceController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		public $name = 'Balance';
		/**
		 * _name
		 *
		 * @var string
		 */
		public $_name = 'Balance';
		/**
		 * modulename
		 *
		 * @var string
		 */
		public $modulename = 'Payments';
		/**
		 * description
		 *
		 * @var string
		 */
		public $description = 'Add funds to your balance';

		/**
		 * addFunds
		 */
		public function addFunds()
		{
			$Itemid   = JFactory::getApplication()->input->getInt('Itemid', 0);
			$balance  = JModelLegacy::getInstance('balance', 'JTheFactoryModel');
			$currency = JModelLegacy::getInstance('currency', 'JTheFactoryModel');

			$user_balance     = $balance->getUserBalance();
			$default_currency = $currency->getDefault();

			$view           = $this->getView('balance');
			$view->Itemid   = $Itemid;
			$view->balance  = $user_balance;
			$view->currency = $default_currency;

			$view->display('addfunds');

		}

		/**
		 * checkout
		 *
		 * @return mixed
		 */
		public function checkout()
		{
			$input  = JFactory::getApplication()->input;
			$price  = $input->getFloat('amount', 0);
			$Itemid = $input->getInt('Itemid', 0);
			$app = JTheFactoryApplication::getInstance();

			if ($price <= 0) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=payments.history&Itemid=" . $Itemid, JText::_("FACTORY_AMOUNT_MUST_BE"), 'error');

				return;
			}

			$currency         = JModelLegacy::getInstance('currency', 'JTheFactoryModel');
			$default_currency = $currency->getDefault();

			$modelOrder = JTheFactoryPricingHelper::getModel('orders');

			// Create item with add balance
			$item              = new stdClass();
			$item->itemname    = $this->name;
			$item->itemdetails = JText::_($this->description);
			$item->iteminfo    = NULL;
			$item->price       = $price;
			$item->currency    = $default_currency;
			$item->quantity    = 1;
			$item->params      = '';

			if($app->getIniValue('use_shopping_cart')) {
				// Use CART and redirect
				JTheFactoryCartHelper::addItem($item, "index.php?option=" . APP_EXTENSION . "&task=" . $input->getString('returnToTask'));
			}

			$order = $modelOrder->createNewOrder($item, $price, $default_currency, NULL, 'P');

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=orderprocessor.checkout&orderid=$order->id&Itemid=" . $Itemid);

		}

		/**
		 * reqWithdraw
		 *
		 */
		public function reqWithdraw()
		{
			$user  = JFactory::getUser();
			$input = JFactory::getApplication()->input;

			$Itemid    = $input->getInt('Itemid', 0);
			$reqAmount = $input->getFloat('amount', 0);

			$modelBalance = JModelLegacy::getInstance('balance', 'JTheFactoryModel');
			$userBalance  = $modelBalance->getUserBalance();

			// Amount requested must be positive
			if ($reqAmount <= 0) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=payments.history&Itemid=" . $Itemid, JText::_("FACTORY_AMOUNT_MUST_BE"), 'error');

				return;
			}

			// Amount requested must be lower that balance
			if ($reqAmount + $userBalance->req_withdraw > $userBalance->balance) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=payments.history&Itemid=" . $Itemid, JText::_("FACTORY_REQ_AMOUNT_EXCEED_YOUR_BALANCE"), 'error');

				return;
			}

			// Everything is ok we can set now user request
			if ($modelBalance->setReqWithdraw($reqAmount)) {
				// Create email notifications event
				JTheFactoryEventsHelper::triggerEvent('onAfterReqWithdraw', array($user->id));

				$msg = JText::_('FACTORY_REQUESTED_WITHDRAW_OK');
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=payments.history&Itemid=" . $Itemid, $msg);

				return;

			}

		}
	}
