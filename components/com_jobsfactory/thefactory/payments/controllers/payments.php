<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryPaymentsController
	 */
	class JTheFactoryPaymentsController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		public $name = 'Payments';
		/**
		 * _name
		 *
		 * @var string
		 */
		public $_name = 'Payments';
		/**
		 * modulename
		 *
		 * @var string
		 */
		public $modulename = 'Payments';

		/**
		 * History
		 */
		public function History()
		{
			$user   = JFactory::getUser();
			$cfg    = JTheFactoryHelper::getConfig();
			$app    = JFactory::getApplication();
			$doc    = JFactory::getDocument();
			$Itemid = $app->input->getInt('Itemid', 0);
			// Get stylesheet for active template
			$doc->addStyleSheet(JURI::root() . 'components/' . APP_EXTENSION . '/templates/' . $cfg->theme . '/' . APP_PREFIX . '_template.css');

			$status = $app->getUserStateFromRequest(APP_EXTENSION . 'status', 'status', 0);
			$type   = $app->getUserStateFromRequest(APP_EXTENSION . 'type', 'type', 0);

			$lists = array();
			// Define Status drop down list
			$statusOptions   = array();
			$statusOptions[] = JHtml::_('select.option', '', JText::_('FACTORY_FILTER_ALL'));
			$statusOptions[] = JHtml::_('select.option', 'C', JText::_('FACTORY_FILTER_COMPLETED'));
			$statusOptions[] = JHtml::_('select.option', 'P', JText::_('FACTORY_FILTER_PENDING'));
			$statusOptions[] = JHtml::_('select.option', 'X', JText::_('FACTORY_FILTER_CANCELED'));
			$lists['status'] = JHtml::_('select.genericlist', $statusOptions, 'status', "onchange=this.form.submit();", 'value', 'text', $status);

			// Define Type drop down list
			$typeOptions   = array();
			$typeOptions[] = JHtml::_('select.option', '', JText::_('FACTORY_FILTER_ALL'));
			$typeOptions[] = JHtml::_('select.option', 'to_me', JText::_('FACTORY_FILTER_WITHDRAW'));
			$typeOptions[] = JHtml::_('select.option', 'by_me', JText::_('FACTORY_FILTER_PAY'));
			$lists['type'] = JHtml::_('select.genericlist', $typeOptions, 'type', "onchange=this.form.submit();", 'value', 'text', $type);

			$modelOrders = JTheFactoryPricingHelper::getModel('orders');
			$orders      = $modelOrders->getOrdersList($user->username);

			// Add funds to site balance
			$balance          = JModelLegacy::getInstance('balance', 'JTheFactoryModel');
			$currency         = JModelLegacy::getInstance('currency', 'JTheFactoryModel');
			$user_balance     = $balance->getUserBalance();
			$default_currency = $currency->getDefault();

			$view = $this->getView('payments');

			// Assign modelOrders to template
			// because we need iterate order items for each order
			$view->cfg              = $cfg;
			$view->type             = $type;
			$view->lists            = $lists;
			$view->orders           = $orders;
			$view->status           = $status;
			$view->balance          = $user_balance;
			$view->currency         = $default_currency;
			$view->pagination       = $modelOrders->pagination;
			$view->modelOrders      = $modelOrders;
			$view->withdraw_enabled = $balance->isWithdrawEnabled();
			$view->Itemid           = $Itemid;

			$view->display('history');

		}

	}
