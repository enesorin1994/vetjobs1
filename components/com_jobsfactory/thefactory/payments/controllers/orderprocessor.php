<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryOrderProcessorController
	 */
	class JTheFactoryOrderProcessorController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		public $name = 'OrderProcessor';
		/**
		 * _name
		 *
		 * @var string
		 */
		public $_name = 'OrderProcessor';
		/**
		 * modulename
		 *
		 * @var string
		 */
		public $modulename = 'Payments';

		/**
		 * First step for payment process
		 *
		 * @return mixed
		 */
		public function checkout()
		{
			$cfg   = JTheFactoryHelper::getConfig();
			$doc   = JFactory::getDocument();
			$user  = JFactory::getUser();
			$input = JFactory::getApplication()->input;

			$orderid = $input->getInt('orderid', 0);
			$Itemid  = $input->getInt('Itemid', 0);

			$doc->addStyleSheet(JURI::root() . 'components/' . APP_EXTENSION . '/templates/' . $cfg->theme . '/' . APP_PREFIX . '_template.css');

			if (!$orderid) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}

			$order       = JTable::getInstance('OrdersTable', 'JTheFactory');
			$ordermodel  = JModelLegacy::getInstance('Orders', 'JTheFactoryModel');
			$order_items = $ordermodel->getOrderItems($orderid);

			if (!$order->load($orderid)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}

			// Current user must be order user
			if ($user->id !== $order->userid) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}
			if ($order->status != 'P') {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $orderid . '&Itemid=' . $Itemid, JText::_("FACTORY_ORDER_IS_NO_LONGER_PENDING"));

				return;
			}


			$model = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$items = $model->getGatewayList(TRUE);


			$urls['return_url'] = JURI::root() . 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $orderid . '&Itemid=' . $Itemid;
			$urls['cancel_url'] = JURI::root() . 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.cancel&orderid=' . $orderid . '&Itemid=' . $Itemid;

			$payment_forms    = array();
			$payment_gateways = array();

			foreach ($items as $item) {
				// Notify URL base
				$urls['notify_url'] = JURI::root() . 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.ipn&orderid=' . $orderid;

				$gw = $model->getGatewayObject($item->classname);
				// Append gateway name
				$urls['notify_url'] .= '&gateway=' . $gw->name;
				$payment_forms[]    = $gw->getPaymentForm($order, $order_items, $urls);
				$payment_gateways[] = $gw;
			}

			$view                   = $this->getView('checkout');
			$view->order            = $order;
			$view->order_items      = $order_items;
			$view->payment_forms    = $payment_forms;
			$view->payment_gateways = $payment_gateways;
			$view->Itemid           = $Itemid;

			$view->display();

		}


		/**
		 * Order details with gateways listing
		 *
		 * @return mixed
		 */
		public function details()
		{
			$cfg   = JTheFactoryHelper::getConfig();
			$doc   = JFactory::getDocument();
			$user  = JFactory::getUser();
			$input = JFactory::getApplication()->input;

			$orderid = $input->getInt('orderid', 0);
			$Itemid  = $input->getInt('Itemid', 0);

			$doc->addStyleSheet(JURI::root() . 'components/' . APP_EXTENSION . '/templates/' . $cfg->theme . '/' . APP_PREFIX . '_template.css');

			if (!$orderid) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&Itemid=' . $Itemid, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}

			$order      = JTable::getInstance('OrdersTable', 'JTheFactory');
			$paymentLog = JTable::getInstance('PaymentLogTable', 'JTheFactory');
			$gateway    = JTable::getInstance('GatewaysTable', 'JTheFactory');

			if (!$order->load($orderid)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&Itemid=' . $Itemid, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}

			$model        = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$ordermodel   = JModelLegacy::getInstance('Orders', 'JTheFactoryModel');
			$balancemodel = JModelLegacy::getInstance('Balance', 'JTheFactoryModel');
			$order_items  = $ordermodel->getOrderItems($orderid);

			// An admin can view order details for any users and
			// each users can view only they orders
			if ($user->authorise('core.admin')) {
				// continue
			} elseif ($user->id !== $order->userid) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&Itemid=' . $Itemid, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}

			$orderForPaymentUsedGateway = '';
			if ($paymentLog->load($order->paylogid)) {
				$gateway->load($paymentLog->gatewayid);
				$orderForPaymentUsedGateway = $gateway->paysystem;
			}

			$items            = $model->getGatewayList(TRUE);
			$payment_forms    = array();
			$payment_gateways = array();

			// Only user that is receiver for this pending order
			// can do or resume a payment
			if ($user->id == $order->userid) {
				foreach ($items as $item) {
					$gw                 = $model->getGatewayObject($item->classname);
					$urls               = array();
					$urls['return_url'] = JURI::root() . 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.returning&orderid=' . $orderid . '&Itemid=' . $Itemid . '&gateway=' . $gw->name;
					$urls['notify_url'] = JURI::root() . 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.ipn&orderid=' . $orderid . '&gateway=' . $gw->name;
					$urls['cancel_url'] = JURI::root() . 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.cancel&orderid=' . $orderid . '&Itemid=' . $Itemid . '&gateway=' . $gw->name;
					$payment_forms[]    = $gw->getPaymentForm($order, $order_items, $urls);
					$payment_gateways[] = $gw;
				}
			}

			$view                     = $this->getView('details');
			$view->cfg                = $cfg;
			$view->user               = $user;
			$view->order              = $order;
			$view->order_items        = $order_items;
			$view->payment_forms      = $payment_forms;
			$view->withdraw_enabled   = $balancemodel->isWithdrawEnabled();
			$view->payment_gateways   = $payment_gateways;
			$view->order_gateway_name = $orderForPaymentUsedGateway;
			$view->Itemid             = $Itemid;

			$view->display();
		}

		/**
		 *
		 */
		public function ipn()
		{

			ob_clean();
			$input        = JFactory::getApplication()->input;
			$gateway_name = $input->getString('gateway', '');
			$orderid      = $input->getInt('orderid', 0);

			$model = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$gw    = $model->getGatewayObject($gateway_name);
			$order = JTable::getInstance('OrdersTable', 'JTheFactory');
			if (!is_object($gw)) {
				//error
				exit;
			}
			$paylog = $gw->processIPN();
			if (!$paylog->orderid) {
				$paylog->orderid = $orderid;
				$paylog->store();
			}


			if (!$paylog->orderid) {
				//Still no order attached to this payment?
				$error = JText::_('FACTORY_PAYMENT_DID_NOT_MATCH_AN_ORDER');
				JTheFactoryEventsHelper::triggerEvent('onPaymentIPNError', array($paylog, $error));
				exit;
			}

			if (!$order->load($paylog->orderid)) {
				//Still no order attached to this payment?
				$error = JText::_('FACTORY_PAYMENT_DID_NOT_MATCH_AN_ORDER');
				JTheFactoryEventsHelper::triggerEvent('onPaymentIPNError', array($paylog, $error));
				exit;
			}
			if (floatval($order->order_total) <> floatval($paylog->amount) || strtoupper($order->order_currency) <> strtoupper($paylog->currency)) {
				$paylog->status = 'error';
			}
			$paylog->userid = $order->userid;

			if (property_exists($paylog, 'gatewayid')) {
				$paylog->gatewayid = $gw->id;
			}

			$paylog->store();

			$date              = new JDate();
			$order->modifydate = $date->toSQL();
			if ($paylog->status == 'ok') $order->status = 'C';
			$order->paylogid = $paylog->id;
			$order->store();

			JTheFactoryEventsHelper::triggerEvent('onPaymentForOrder', array($paylog, $order));
			exit;
		}

		/**
		 * Cancel
		 *
		 * @return mixed
		 */
		public function Cancel()
		{
			$input   = JFactory::getApplication()->input;
			$orderid = $input->getInt('orderid', 0);
			$itemId  = $input->getInt('Itemid', 0);

			if (is_array($orderid)) $orderid = $orderid[0];

			$order = JTable::getInstance('OrdersTable', 'JTheFactory');

			if (!$orderid || !$order->load($orderid)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=payments.history&Itemid=' . $itemId, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}
			if ($order->status != 'P') {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $orderid . '&Itemid=' . $itemId, JText::_("FACTORY_ORDER_IS_ALREADY_CANCELLED"));

				return;
			}
			$order->modifydate = date('Y-m-d H:i:s');
			$order->status     = 'X';

			JTheFactoryEventsHelper::triggerEvent('onBeforeCancelOrder', array($order));
			$order->store();

			JTheFactoryEventsHelper::triggerEvent('onAfterCancelOrder', array($order));
			if ($order->status == 'X') $txt = JText::_("FACTORY_ORDER_WAS_CANCELLED");
			else
				$txt = '';
			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $orderid . '&Itemid=' . $itemId, $txt);
		}

		public function OnPending()
		{
			$input   = JFactory::getApplication()->input;
			$orderid = $input->getInt('orderid', 0);
			$itemId  = $input->getInt('Itemid', 0);

			if (is_array($orderid)) $orderid = $orderid[0];

			$order = JTable::getInstance('OrdersTable', 'JTheFactory');

			if (!$orderid || !$order->load($orderid)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=payments.history&Itemid=' . $itemId, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}

			if ($order->status == 'P') {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $orderid . '&Itemid=' . $itemId, JText::_("FACTORY_ORDER_IS_ALREADY_IN_PENDING"));

				return;
			}

			if ($order->status == 'C') {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $orderid . '&Itemid=' . $itemId, JText::_("FACTORY_ORDER_IS_ALREADY_PAID"));

				return;
			}

			$order->modifydate = date('Y-m-d H:i:s');
			$order->status     = 'P';
			$order->store();

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $orderid . '&Itemid=' . $itemId);
		}

		/**
		 * Cancel
		 *
		 * @return mixed
		 */
		public function Returning()
		{
			$input = JFactory::getApplication()->input;
			$order = JTable::getInstance('OrdersTable', 'JTheFactory');

			$orderid = $input->getInt('orderid', 0);
			$Itemid  = $input->getInt('Itemid', 0);

			if (!$order->load($orderid)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&Itemid=' . $Itemid, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $orderid . '&Itemid=' . $Itemid, JText::_("FACTORY_PAYMENT_WILL_BE_PROCESSED_SHORTLY"));
		}

		/**
		 * Called by payment gateway like bank transfer
		 *
		 * @return mixed
		 */
		public function Gateway()
		{
			$input        = JFactory::getApplication()->input;
			$gateway_name = $input->getString('gateway', '');
			$Itemid       = $input->getInt('Itemid', 0);
			$model        = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$gw           = $model->getGatewayObject($gateway_name);

			if (!is_object($gw)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&Itemid=' . $Itemid, JText::_("FACTORY_PAYMENT_GATEWAY_DOES_NOT_EXIST"));

				return;
			}

			$gw->processTask();
		}

	}
