<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryController
	 */
	class JTheFactoryController extends JControllerLegacy
	{
		/**
		 * modulename
		 *
		 * @var array|string
		 */
		public $modulename = '';
		/**
		 * basepath
		 *
		 * @var string
		 */
		public $basepath;

		/**
		 * @param array $config
		 */
		public function __construct($config = array())
		{
			$MyApp            = JTheFactoryApplication::getInstance();
			$this->modulename = $this->getModuleName();
			$this->basepath   = $MyApp->app_path_front . strtolower($this->modulename);

			$lang = JFactory::getLanguage();
			$lang->load('thefactory.' . strtolower($this->modulename));

			parent::__construct($config);

			//You can override views in the component frontend
			//View Name must be JTheFactoryView[Name]
			self::addViewPath($this->basepath . "/views");
			self::addViewPath(JPATH_SITE . '/components/' . APP_EXTENSION . '/views');

			JTheFactoryHelper::modelIncludePath($this->modulename);
			JTheFactoryHelper::tableIncludePath($this->modulename);

		}

		/**
		 * getView
		 *
		 * @param string $name
		 * @param string $type
		 * @param string $prefix
		 * @param array  $config
		 *
		 * @return JViewLegacy
		 */
		public function getView($name = '', $type = 'html', $prefix = '', $config = array())
		{
			$MyApp                   = JTheFactoryApplication::getInstance();
			$config['template_path'] = $MyApp->app_path_front . strtolower($this->modulename) . "/views/" . strtolower($name) . "/tmpl";
			$viewprefix              = ($prefix) ? ($prefix) : ('JTheFactoryView' . ucfirst($this->getName()));

			return parent::getView($name, $type, $viewprefix, $config);
		}

		/**
		 * getModuleName
		 *
		 * @return array|string
		 */
		public function getModuleName()
		{
			if (!$this->modulename) {
				$reflection = new ReflectionClass(get_class($this));
				$classFile  = $reflection->getFileName();

				return strtolower(basename(dirname(dirname($classFile))));
			} else {
				return $this->modulename;
			}
		}

	}
