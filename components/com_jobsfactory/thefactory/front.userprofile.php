<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryUserProfile
	 */
	class JTheFactoryUserProfile extends JObject
	{

		/**
		 * profile_mode
		 *
		 * @var null|string
		 */
		public $profile_mode = NULL;
		/**
		 * integrationObject
		 *
		 * @var JTheFactoryIntegrationComponent|null
		 */
		public $integrationObject = NULL;
		/**
		 * profileId
		 *
		 * @var null
		 */
		public $profileId = NULL;
		/**
		 * instances
		 *
		 * @var
		 */
		protected static $instances;

		/**
		 * @param string $profile_mode
		 */
		public function __construct($profile_mode = 'component')
		{
			$MyApp            = JTheFactoryApplication::getInstance();
			$integrationClass = 'JTheFactoryIntegration' . ucfirst($profile_mode);
			JLoader::register($integrationClass, $MyApp->app_path_admin . 'integration/' . $profile_mode . '.php');
			$this->integrationObject = new $integrationClass();
			$this->profile_mode      = $profile_mode;

			// Switch off integration with other components in case they are not installed.
			if (!$this->integrationObject->detectIntegration()) {
				JLoader::register('JTheFactoryIntegrationComponent', $MyApp->app_path_admin . 'integration' . DS . 'component.php');
				$this->integrationObject = new JTheFactoryIntegrationComponent();
				$this->profile_mode      = 'component';
			}
		}

		/**
		 * getInstance
		 *
		 * @param string $profile_mode
		 * @param int    $userid
		 *
		 * @return mixed
		 */
		public static function getInstance($profile_mode = 'component', $userid = 0)
		{
			if (!$userid) {
				$userid = JFactory::getUser()->id;
			}

			if (!isset (self::$instances[$userid])) {
				self::$instances[$userid] = new self($profile_mode);
				self::$instances[$userid]->getUserProfile($userid);
			}

			return self::$instances[$userid];
		}

		/**
		 * getUserProfile
		 *
		 * @param null $userid
		 */
		public function getUserProfile($userid = NULL)
		{

			$juser           = JFactory::getUser($userid);
			$extendedProfile = $this->integrationObject->getUserProfile($juser->id);

			foreach ($juser as $k => $v) {
				$this->$k = $v;
			}

			$fieldmap = $this->integrationObject->getIntegrationArray();

			foreach ($extendedProfile as $k => $v) {
				if ('id' == $k) {
					$this->profileId = $v;
					continue;
				}
				$this->$k = $v;

				//fill the integration fields
				$integrationFields = array_keys($fieldmap, $k);

				if (count($integrationFields)) {
					foreach ($integrationFields as $f) {
						$this->$f = $v;
					}
				}
			}
		}

		/**
		 * getIntegrationTable
		 *
		 * @return null|string
		 */
		public function getIntegrationTable()
		{
			return $this->integrationObject->table;
		}

		/**
		 * getIntegrationKey
		 *
		 * @return string
		 */
		public function getIntegrationKey()
		{
			return $this->integrationObject->keyfield;
		}

		/**
		 * getIntegrationArray
		 *
		 * @return array
		 */
		public function getIntegrationArray()
		{
			return $this->integrationObject->getIntegrationArray();
		}

		/**
		 * getProfileLink
		 *
		 * @param $userid
		 *
		 * @return null
		 */
		public function getProfileLink($userid)
		{
			return $this->integrationObject->getProfileLink($userid);
		}

		/**
		 * getProfileFields
		 *
		 * @return array
		 */
		public function getProfileFields()
		{
			return $this->integrationObject->getIntegrationFields();
		}

		/**
		 * getUserList
		 *
		 * @param int  $limitstart
		 * @param int  $limit
		 * @param null $filters
		 * @param null $ordering
		 *
		 * @return mixed
		 */
		public function getUserList($limitstart = 0, $limit = 30, $filters = NULL, $ordering = NULL)
		{
			return $this->integrationObject->getUserList($limitstart, $limit, $filters, $ordering);
		}

		/**
		 * getCountUserList
		 *
		 * @param int  $limitstart
		 * @param int  $limit
		 * @param null $filters
		 * @param null $ordering
		 *
		 * @return mixed
		 */
		public function getCountUserList($limitstart = 0, $limit = 30, $filters = NULL, $ordering = NULL)
		{
			return $this->integrationObject->getUserList($limitstart, $limit, $filters, $ordering);
		}

		/**
		 * checkProfile
		 *
		 * @param null $userid
		 *
		 * @return mixed
		 */
		public function checkProfile($userid = NULL)
		{
			return $this->integrationObject->checkProfile($userid);

		}

		/**
		 * getFilterField
		 *
		 * @param $filterName
		 *
		 * @return mixed
		 */
		public function getFilterField($filterName)
		{

			$integrationArray = $this->getIntegrationArray();

			return isset($integrationArray[$filterName]) ? $integrationArray[$filterName] : $filterName;
		}

		/**
		 * getFilterTable
		 *
		 * @param $filterName
		 *
		 * @return bool|null|string
		 */
		public function getFilterTable($filterName)
		{

			$db = JFactory::getDBO();

			$fieldName = $this->getFilterField($filterName);
			if (!$fieldName) {
				return NULL;
			}

			static $standardFields = NULL;
			if (!$standardFields) {
				$db->setQuery('SHOW FIELDS FROM #__users');
				$standardFields = $db->loadColumn();
			}

			if (in_array($fieldName, $standardFields)) {
				return '#__users';
			}

			static $extendedFields = NULL;
			if (!$extendedFields) {
				$db->setQuery('SHOW FIELDS FROM ' . $this->integrationObject->table);
				$extendedFields = $db->loadColumn();

				if (strstr($this->integrationObject->table, 'love')) {
					foreach ($extendedFields as $v) {
						// Ignore other fields than custom fields
						$pos_1 = strpos($v, 'field_');
						$pos_2 = strpos($v, '_visibility');
						if (FALSE === $pos_1 || FALSE !== $pos_2) {
							continue;
						}
						$fieldId = filter_var($v, FILTER_SANITIZE_NUMBER_INT);

						// Get correspondent field name for this user data profile field from #__lovefactory_field
						$query = "SELECT `title` FROM `#__lovefactory_fields` WHERE `id` = {$fieldId}";
						$db->setQuery($query);

						$getCF            = str_replace(' ', '_', $db->loadResult());
						$extendedFields[] = $getCF;

					}
				}
			}

			if (in_array($fieldName, $extendedFields)) {
				return $this->integrationObject->table;
			}

			return FALSE;
		}

		/**
		 * setFieldValue
		 *
		 * @param     $filterName
		 * @param     $value
		 * @param int $userid
		 *
		 * @return mixed|null
		 */
		public function setFieldValue($filterName, $value, $userid = 0)
		{

			$db = JFactory::getDBO();

			$fieldName = $this->getFilterField($filterName);
			if (!$fieldName) {
				JFactory::getApplication()->enqueueMessage('Field "' . strtoupper($filterName) . '" is not mapped in profile integration!', 'warning');

				return NULL;
			}
			$fieldTable = $this->getFilterTable($filterName);

			$newValue = preg_replace('/#fieldName/', $fieldName, $value);
			$value    = ($newValue == $value) ? $db->Quote($newValue) : $db->escape($newValue);

			$keyField = ('#__users' == $fieldTable) ? 'id' : $this->integrationObject->keyfield;
			$userid   = (array)$userid;

			$db->setQuery('UPDATE `' . $fieldTable . '` SET `' . $fieldName . '`= IF(`' . $fieldName . '` IS NULL, 1, ' . $value . ') WHERE `' . $keyField . '` IN (' . implode(',', $userid) . ')');

			return $db->execute();
		}
	} // End Class
