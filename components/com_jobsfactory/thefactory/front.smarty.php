<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: smarty
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	/**
	 * Class JTheFactorySmarty
	 */
	class JTheFactorySmarty extends Smarty
	{
		/**
		 *
		 */
		public function __construct()
		{
			$my     = JFactory::getUser();
			$input  = JFactory::getApplication()->input;
			$Itemid = $input->getInt('Itemid', 0);

//        if (JDEBUG) $this->debugging= true;

			$this->assign('ROOT_HOST', JURI::root());
			$this->assign('Itemid', $Itemid);
			$this->assign('option', $input->getWord('option', ''));
			$this->assign('task', $input->getCmd('task', ''));
			$this->assign('controller', $input->getCmd('controller', ''));
			$this->assign('is_logged_in', ($my->id) ? "1" : "0");
			$this->assign('joomlauser', $my);
			$cfg = new stdClass();

			if (class_exists('JTheFactoryHelper')) {
				$cfg = JTheFactoryHelper::getConfig();
				$this->assign('cfg', $cfg);
			}

			$this->register_modifier('translate', array($this, 'smarty_translate'));
			$this->register_modifier('t', array($this, 'smarty_translate'));
			$this->register_function('jtext', array($this, 'smarty_function_jtext'));
			$this->register_function('infobullet', array($this, 'smarty_infobullet'));
			$this->register_function('positions', array($this, 'smarty_positions'));

			$this->register_function('init_behavior', array($this, 'smarty_init_behavior'));
			$this->register_function('import_js_file', array($this, 'smarty_import_js_file'));
			$this->register_function('import_css_file', array($this, 'smarty_import_css_file'));
			$this->register_block('import_js_block', array($this, 'smarty_import_js_block'));
			$this->register_block('import_css_block', array($this, 'smarty_import_css_block'));

			$this->register_function('createtab', array($this, 'smarty_createtab'));
			$this->register_function('createtabslist', array($this, 'smarty_createtabslist'));
			$this->register_function('additemtabs', array($this, 'smarty_additemtabs'));
			$this->register_function('endtabslist', array($this, 'smarty_endtabslist'));
			$this->register_function('startpane', array($this, 'smarty_startpane'));
			$this->register_function('endpane', array($this, 'smarty_endpane'));
			$this->register_function('starttab', array($this, 'smarty_starttab'));
			$this->register_function('endtab', array($this, 'smarty_endtab'));
			$this->register_function('jhtml', array($this, 'smarty_jhtml'));
			$this->register_function('jroute', array($this, 'smarty_jroute'));

			$this->template_dir = JPATH_SITE . '/components/' . APP_EXTENSION . '/templates/' . $cfg->theme . '/';
			$this->compile_dir  = JPATH_ROOT . '/cache/' . APP_EXTENSION . '/templates';

			parent::__construct();
		}

		/**
		 * display
		 *
		 * @param null|string $tpl_name
		 * @param null        $cache_id
		 * @param null        $compile_id
		 */
		public function display($tpl_name, $cache_id = NULL, $compile_id = NULL)
		{
			$task = JFactory::getApplication()->input->getCmd('task', '');
			JTheFactoryEventsHelper::triggerEvent('onBeforeDisplay', array($task, $this));

			if (!file_exists($this->template_dir . $tpl_name)) {
				$tpl_name = JPATH_SITE . '/components/' . APP_EXTENSION . '/templates/default/' . $tpl_name;
			}

			if ($title = $this->get_template_vars('page_title')) {
				$doc = JFactory::getDocument();
				$doc->setTitle($title);
			}

			if (!$this->get_template_vars('template_file')) {
				$template_file = $tpl_name;
				if (substr($template_file, 0, 2) == 't_') $template_file = substr($template_file, 2);
				$template_file = preg_replace('#\.[^.]*$#', '', $template_file);
				self::assign('template_file', $template_file);
			}

			//parent::clear_compiled_tpl(); in admin, just when theme is changed; themes controller
			parent::display($tpl_name, $cache_id, $compile_id);

			JTheFactoryEventsHelper::triggerEvent('onAfterDisplay');
		}

		/**
		 * _smarty_include
		 *
		 * @param $params
		 */
		public function _smarty_include($params)
		{
			$tpl_inc = $params['smarty_include_tpl_file'];

			if (!file_exists($this->template_dir . $tpl_inc)) {
				$tpl_name                          = JPATH_SITE . '/components/' . APP_EXTENSION . '/templates/default/' . $tpl_inc;
				$params['smarty_include_tpl_file'] = $tpl_name;
			}

			parent::_smarty_include($params);
		}

		/**
		 * smarty_infobullet
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return mixed|string
		 */
		public function smarty_infobullet($params, &$smarty)
		{
			$customIcon = NULL;
			$res        = "";

			if (!empty($params['text'])) {
				if (!empty($params['icon'])) {
					$customIcon = $params['icon'];
				}

				JHTML::_('behavior.tooltip'); //load the tooltip behavior
				$res = JHTML::_('tooltip', JText::_($params['text']), '', $customIcon);
			}

			return $res;
		}

		/**
		 * smarty_translate
		 *
		 * @return mixed|string
		 */
		public function smarty_translate()
		{
			$args = func_get_args();
			if (count($args) == 1) {
				return JText::_($args[0]);
			}

			return call_user_func_array(array('JText', 'sprintf'), $args);

		}

		/**
		 * smarty_function_jtext
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return mixed|null|string
		 */
		public function smarty_function_jtext($params, &$smarty)
		{
			$text = isset($params['text']) ? strtoupper($params['text']) : NULL;
			if (!$text) {
				return NULL;
			}

			$params['text'] = JText::_($text);
			if (count($params) > 1) {
				return call_user_func_array('sprintf', $params);
			}

			return $params['text'];
		}

		/**
		 * @param array() $params
		 * @param Smarty  $smarty
		 *
		 * @return null|string
		 */
		public function smarty_positions($params, &$smarty)
		{
			/**
			 * @var FactoryFieldsTbl $item
			 */

			if (!isset($params['position']) || empty($params['position'])) return NULL;
			if (!isset($params['item']) || empty($params['item'])) return NULL;

			$item     = $params['item'];
			$page     = (isset($params['page'])) ? $params['page'] : "";
			$catfield = NULL;
			if (is_callable(array($item, 'getCategoryField'))) $catfield = $item->getCategoryField(); //make sure proper object is passed
			if (is_object($item)) $item = get_object_vars($item);

			$position      = $params['position'];
			$template_file = $smarty->get_template_vars('template_file');
			JTheFactoryHelper::modelIncludePath('positions');
			$model  = JModelLegacy::getInstance('Positions', 'JTheFactoryModel');
			$fields = $model->getFieldsForPosition($template_file, $position);
			$result = "";

			$fieldObj = JTable::getInstance('FieldsTable', 'JTheFactory');
			JTheFactoryHelper::modelIncludePath('fields');
			$fieldsmodel = JModelLegacy::getInstance('Fields', 'JTheFactoryModel');

			foreach ($fields as $field) if (isset($item[$field->db_name]) && $page == $field->page) {
				$fieldObj->bind($field);
				if ($catfield && $fieldObj->categoryfilter && !$fieldsmodel->hasAssignedCat($fieldObj, $item[$catfield])) continue;

				$ftype       = CustomFieldsFactory::getFieldType($field->ftype);
				$field_label = JText::_($field->name);

				$field_html = $ftype->getTemplateHTML($fieldObj, $item[$field->db_name]);
				// Display field only if its value is not empty
				if (!empty($field_html)) {
					$result .= "<div id='{$field->db_name}'><label class='custom_field'>{$field_label}: </label>&nbsp;{$field_html}</div>";
				}
			}

			return $result;
		}

		/**
		 * smarty_init_behavior
		 *
		 * @param $params
		 * @param $smarty
		 */
		public function smarty_init_behavior($params, &$smarty)
		{
			if (!empty($params['type'])) {
				$type = $params['type'];
			} else
				$type = 'modal';
			JHTML::_('behavior.' . $type);
		}

		/**
		 * smarty_import_js_file
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return null|string
		 */
		public function smarty_import_js_file($params, &$smarty)
		{
			if (empty($params['url'])) return NULL;
			$url = $params['url'];
			if (stripos($url, 'http://') === FALSE && stripos($url, 'https://') === FALSE) $url = JURI::root() . 'components/' . APP_EXTENSION . '/js/' . $url;
			$doc = JFactory::getDocument();
			$doc->addScript($url);
			if ($doc->getType() == 'raw') return "<script type=\"text/javascript\" src=\"" . $url . "\"></script>";
			else return NULL;
		}

		/**
		 * smarty_import_css_file
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return null|string
		 */
		public function smarty_import_css_file($params, &$smarty)
		{
			if (empty($params['url'])) return NULL;
			$url = $params['url'];
			if (stripos('http://', $url) === FALSE && stripos('https://', $url) === FALSE) $url = JURI::root() . 'components/' . APP_EXTENSION . '/css/' . $url;
			$doc = JFactory::getDocument();
			$doc->addStyleSheet($url);
			if ($doc->getType() == 'raw') return "<link href=\"" . $url . "\" rel=\"stylesheet\" type=\"text/css\">";
			else return NULL;
		}

		/**
		 * @param array() $params
		 * @param string  $content
		 * @param Smarty  $smarty
		 * @param bool    $repeat
		 *
		 * @return null|string
		 */
		public function smarty_import_js_block($params, $content, &$smarty, &$repeat)
		{
			if ($repeat) return NULL; //ignore opening tag
			$doc = JFactory::getDocument();
			$doc->addScriptDeclaration($content);
			if ($doc->getType() == 'raw') return "<script type='text/javascript'>{$content}</script>";
			else return NULL;
		}

		/**
		 * smarty_import_css_block
		 *
		 * @param $params
		 * @param $content
		 * @param $smarty
		 * @param $repeat
		 *
		 * @return null|string
		 */
		public function smarty_import_css_block($params, $content, &$smarty, &$repeat)
		{
			if ($repeat) return NULL; //ignore opening tag
			$doc = JFactory::getDocument();
			$doc->addStyleDeclaration($content);
			if ($doc->getType() == 'raw') return "<style type=\"text/css\">{$content}</style>";
			else
				return NULL;
		}

		/**
		 * smarty_createtab
		 *
		 * @param $params
		 * @param $smarty
		 */
		public function smarty_createtab($params, &$smarty)
		{
			//TODO: de testat new tabs in reverse
			return;
		}

		/**
		 * smarty_createtabslist
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return null|string
		 */
		public function smarty_createtabslist($params, &$smarty)
		{
			$jVersion = new JVersion();
			if ($jVersion->isCompatible('3.0')) {
				return '<ul class="nav nav-tabs" id="' . $params['name'] . '">';
			} else {
				return NULL;
			}
		}

		/**
		 * smarty_additemtabs
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return null|string
		 */
		public function smarty_additemtabs($params, &$smarty)
		{
			$jVersion = new JVersion();
			if ($jVersion->isCompatible('3.0')) {
				$isActive = '';
				if (isset($params['active']) && 1 == $params['active']) {
					$isActive = 'active';
				}

				return '<li class="' . $isActive . '" id="tab-for-' . $params['id'] . '"><a data-toggle="tab" href="#' . $params['id'] . '"> ' . $params['text'] . '</a></li>';
			} else {
				return NULL;
			}
		}

		/**
		 * smarty_endtabslist
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return null|string
		 */
		public function smarty_endtabslist($params, &$smarty)
		{
			$jVersion = new JVersion();

			return $jVersion->isCompatible('3.0') ? '</ul>' : NULL;
		}

		/**
		 * smarty_startpane
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return mixed
		 */
		public function smarty_startpane($params, &$smarty)
		{
			$jVersion = new JVersion();
			if ($jVersion->isCompatible('3.0')) {
				return JHtml::_('bootstrap.startPane', $params['name'], array('active' => $params['active']));
			} else {
				JHtml::_('behavior.framework');

				return JHtml::_('tabs.start', $params['id']);
			}
		}

		/**
		 * smarty_endpane
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return mixed
		 */
		public function smarty_endpane($params, &$smarty)
		{
			$jVersion = new JVersion();
			if ($jVersion->isCompatible('3.0')) {
				return JHtml::_('bootstrap.endPane', $params['name']);
			} else {
				return JHtml::_('tabs.end');
			}
		}

		/**
		 * smarty_starttab
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return mixed
		 */
		public function smarty_starttab($params, &$smarty)
		{
			$jVersion = new JVersion();
			if ($jVersion->isCompatible('3.0')) {
				return JHtml::_('bootstrap.addPanel', $params['name'], $params['id']);
			} else {
				return JHtml::_('tabs.panel', JText::_($params['text']), $params['paneid']);
			}
		}

		/**
		 * smarty_endtab
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return mixed|null
		 */
		public function smarty_endtab($params, &$smarty)
		{
			$jVersion = new JVersion();
			if ($jVersion->isCompatible('3.0')) {
				return JHtml::_('bootstrap.endPanel');
			} else {
				return NULL;
			}
		}

		/**
		 * smarty_jhtml
		 *
		 * @param $params
		 * @param $smarty
		 *
		 * @return mixed
		 */
		public function smarty_jhtml($params, &$smarty)
		{
			$args = array();
			foreach ($params as $k => $value) $args[] = $value;

			return call_user_func_array(array('JHtml', '_'), $args);
		}

		/**
		 * smarty_jroute
		 *
		 * @param $params
		 *
		 * @return null|The
		 */
		public function smarty_jroute($params)
		{
			$isXHTML = isset($params['xhtml']) ? $params['xhtml'] : TRUE;
			$isSSL   = isset($params['ssl']) ? $params['ssl'] : NULL;

			if (isset($params['link'])) {
				return JRoute::_($params['link'], $isXHTML, $isSSL);
			}

			return NULL;
		}
	}
