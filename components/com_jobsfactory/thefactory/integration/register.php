<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegrationRegister
	 */
	class JTheFactoryIntegrationRegister
	{
		/**
		 * registerModule
		 *
		 * @param null $app
		 */
		static function registerModule($app = NULL)
		{
			if (!$app) $app = JTheFactoryApplication::getInstance();
			if ($app->getIniValue('use_integration')) {
				JLoader::register('JTheFactoryIntegration', $app->app_path_admin . 'integration/integration.php');
				JTheFactoryHelper::loadModuleLanguage('integration');
			}
		}
	}
