<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JJobsfactoryControllerApplications extends JControllerLegacy
{
    var $_name = 'jobs';
    var $name = 'jobs';

    function UserApplications()
    {
        $app = JFactory::getApplication();

        $userid = $app->input->getInt('id');
        $company_userid = $app->input->getInt('userid');
        if ($company_userid == '')
            $company_userid = $userid;

        if (!JFactory::getUser($userid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_USER_DOES_NOT_EXIST"),'error');
            return;
        }

        $cfg = JTheFactoryHelper::getConfig();

        if ($userid) {
            $user_groups = JAccess::getGroupsByUser($userid);
        } else {
            $user_groups = JAccess::getGroupsByUser($company_userid);
        }
        $isCandidate = count(array_intersect($user_groups, $cfg->candidate_groups)) > 0;
        $isCompany = count(array_intersect($user_groups, $cfg->company_groups)) > 0;

        $userprofile = JobsHelperTools::getUserProfileObject();
        $applicationsmodel = JModelLegacy::getInstance('Applications', 'jobsModel');

        if ($isCandidate) {
            $userprofile->getUserProfile($userid);
            $job_rows = $applicationsmodel->loadUserItems($userid);
            $pagination = $applicationsmodel->getUserPagination();
        } else {
            if ($isCompany) {
                $userprofile->getUserProfile($company_userid);
                $job_rows = $applicationsmodel->loadCompanyItems($company_userid);
                $pagination = $applicationsmodel->getCompanyPagination();
            }
        }

        if ($userprofile && $userprofile->name == '') {
            $user = JFactory::getUser($userid);
            $userprofile->name = $user->name;
        }

        $view = $this->getView('applications', $app->input->getWord('format', 'html'));

        $view->user = clone $userprofile;
        $view->job_rows = $job_rows;
        $view->pagination = $pagination;
        $view->isCandidate = $isCandidate;
        $view->isCompany = $isCompany;

        $view->display("t_myapplications.tpl");
     }

    function showApplyJob()
    {
        $app = JFactory::getApplication();

        $userid = $app->input->getInt('user_rated');
        $jobid = $app->input->getInt('id');

        if (!JFactory::getUser($userid)) {
            $app->enqueueMessage(JText::_("COM_JOBS_USER_DOES_NOT_EXIST"),'error');
            return;
        }
        $job = JTable::getInstance('Jobs', 'Table');
        if (!$job->load($jobid)) {
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_DOES_NOT_EXIST"),'error');
            return;
        }

        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile($userid);

        $view = $this->getView('applications', $app->input->getWord('format', 'html'));
        $view->user = clone $userprofile;
        $view->job = $job;

        $view->display("t_ratejob.tpl");
    }

    function MyApplications()
    {
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $app->input->set('id', $user->id);

        return $this->UserApplications();
    }

}
