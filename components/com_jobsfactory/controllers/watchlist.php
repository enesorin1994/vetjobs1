<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JJobsfactoryControllerWatchlist extends JControllerLegacy
{
	var $_name='jobs';
	var $name='jobs';

    function AddWatchCat(){
   	    $cat = $this->input->getInt('cat',0);
        if (!$cat) {
            JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_SPECIFY_A_CATEGORY"),'warning');
            return;
        }
        $categoryModel = JModelLegacy::getInstance('RCategory','jobsModel');
        $categoryModel->addWatch($cat);

   		$refferer = $this->input->getString('HTTP_REFERER');
   		if(!$refferer) $refferer=JobsHelperRoute::getCategoryRoute();
   		$this->setRedirect($refferer,JText::_("COM_JOBS_ADDED_TO_WACHLIST"));
   	}

   	function DelWatchCat(){
   		$cat = JFactory::getApplication()->input->getInt('cat',0);
        if (!$cat) {
            JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_SPECIFY_A_CATEGORY"),'warning');
            return;
        }
        $categoryModel = JModelLegacy::getInstance('RCategory','jobsModel');
        $categoryModel->delWatch($cat);

        $refferer = $this->input->getString('HTTP_REFERER');
   	    if(!$refferer) $refferer=JobsHelperRoute::getCategoryRoute();

        $this->setRedirect($refferer,JText::_("COM_JOBS_REMOVED_FROM_WACHLIST"));
   	}

    function AddWatchLocation(){
       $location = JFactory::getApplication()->input->getInt('location',0);

       if (!$location) {
           JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_SPECIFY_A_LOCATION"),'warning');
           return;
       }

       $locationModel = JModelLegacy::getInstance('Location','jobsModel');
       $locationModel->addWatch($location);

       $refferer = JFactory::getApplication()->input->getString('HTTP_REFERER');
       if(!$refferer) $refferer = JobsHelperRoute::getLocationRoute();

        $this->setRedirect($refferer,JText::_("COM_JOBS_ADDED_TO_WACHLIST"));
    }


    function DelWatchLocation(){
        $location = JFactory::getApplication()->input->getInt('location',0);
        if (!$location) {
           JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_SPECIFY_A_LOCATION"),'warning');
           return;
        }

        $locationModel = JModelLegacy::getInstance('Location','jobsModel');
        $locationModel->delWatch($location);

        $refferer = JFactory::getApplication()->input->getString('HTTP_REFERER');
        if(!$refferer) $refferer=JobsHelperRoute::getLocationRoute();

        $this->setRedirect($refferer,JText::_("COM_JOBS_REMOVED_FROM_WACHLIST"));
    }

    function AddWatchCity(){
       $cityid = JFactory::getApplication()->input->getInt('cityid',0);
       if (!$cityid) {
           JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_SPECIFY_A_CITY"),'warning');
           return;
       }

       $cityModel = JModelLegacy::getInstance('City','jobsModel');
       $cityModel->addWatch($cityid);

       $refferer = $this->input->getString('HTTP_REFERER');
       if(!$refferer) $refferer = JobsHelperRoute::getCityRoute();

       $this->setRedirect($refferer,JText::_("COM_JOBS_ADDED_TO_WACHLIST"));
    }

    function DelWatchCity(){
        $cityid = JFactory::getApplication()->input->getInt('cityid',0);
        if (!$cityid) {
           JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_SPECIFY_A_CITY"),'warning');
           return;
        }
        $cityModel = JModelLegacy::getInstance('City','jobsModel');
        $cityModel->delWatch($cityid);

        $refferer = $this->input->getString('HTTP_REFERER');
        if(!$refferer) $refferer=JobsHelperRoute::getCityRoute();

        $this->setRedirect($refferer,JText::_("COM_JOBS_REMOVED_FROM_WACHLIST"));
    }

    function AddWatchCompany(){
       $companyid = JFactory::getApplication()->input->getInt('companyid',0);
       if (!$companyid) {
           JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_SPECIFY_A_COMPANY"),'warning');
           return;
       }

       $companyModel = JModelLegacy::getInstance('Company','jobsModel');
       $companyModel->addWatch($companyid);

       $refferer = $this->input->getString('HTTP_REFERER');
       if(!$refferer) $refferer = JobsHelperRoute::getUserdetailsRoute($companyid,false);

        $this->setRedirect($refferer,JText::_("COM_JOBS_ADDED_TO_WACHLIST"));
    }

    function DelWatchCompany(){
        $companyid = JFactory::getApplication()->input->getInt('companyid',0);
        if (!$companyid) {
           JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_SPECIFY_A_COMPANY"),'warning');
           return;
        }
        $companyModel = JModelLegacy::getInstance('Company','jobsModel');
        $companyModel->delWatch($companyid);

        $refferer = $this->input->getString('HTTP_REFERER');

        if(!$refferer) $refferer=JobsHelperRoute::getUserdetailsRoute($companyid, false);

        $this->setRedirect($refferer,JText::_("COM_JOBS_REMOVED_FROM_WACHLIST"));
    }


    function DelWatch(){

   		$id = JFactory::getApplication()->input->getInt('id');
   		$my = JFactory::getUser();

        $database = JFactory::getDbo();
        $database->setQuery("DELETE FROM  #__jobsfactory_watchlist WHERE userid=$my->id AND job_id='$id'");
   		$database->execute();

        $refferer = $this->input->getString('HTTP_REFERER');
   		if(!$refferer) $refferer = JobsHelperRoute::getJobListRoute();

   		$this->setRedirect($refferer,JText::_("COM_JOBS_JOB_REMOVED_FROM_WATCHLIST"));
   	}

   	function AddWatchList(){

   		$database = JFactory::getDBO();
   		$my = JFactory::getUser();
   		$id = JFactory::getApplication()->input->getInt('id');

        $database->setQuery("DELETE FROM  #__jobsfactory_watchlist WHERE userid=$my->id AND job_id='$id'");
   		$database->execute();//make sure you do not add duplicates

        $watchList = JTable::getInstance('watchlist', 'Table');
   		$watchList->userid = $my->id;
   		$watchList->job_id = $id;
   		$watchList->store();

        $refferer = $this->input->getString('HTTP_REFERER');
   		if(!$refferer) $refferer = JobsHelperRoute::getJobListRoute();

   		$this->setRedirect($refferer,JText::_("COM_JOBS_JOB_ADDED_IN_WATCHLIST"));
   	}

    function watchlist()
    {
        $view=$this->getView('Jobs','html');
        $view->display('t_mywatchlist.tpl');
    }

}
