<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JJobsfactoryControllerCrontask extends JControllerLegacy
{
	var $_name = 'cronTask';
	var $name = 'cronTask';

	public function __construct($config=array())
	{
		$config['default_task'] = 'cron';
		parent::__construct($config);
	}

	public function cron()
	{
		/* cron script Pass and authentication*/
		$app = JFactory::getApplication();
		$cfg = JTheFactoryHelper::getConfig();
		$config = JFactory::getConfig();
		$pass = $app->input->get('pass');
		$debug= $app->input->get('debug',0);

		$date = JFactory::getDate();
		$nowMysql = $date->toSQL(false);

		$database = JFactory::getDbo();

		JTheFactoryHelper::modelIncludePath('payments');
		JTheFactoryHelper::tableIncludePath('payments');

		$log = JTable::getInstance('Log','Table');
		$log->priority = 'log';
		$log->event = 'cron';
		$log->logtime = $nowMysql;
		$logtext = "";

		if ($cfg->cron_password !== $pass)
		{
			//Bad Password, log and exit;
			$log->log = JText::_("COM_JOBS_BAD_PASSWORD_USED") . " > $pass";
			$log->store();
			die(JText::_("COM_JOBS_ACCESS_DENIED"));
		}

		@set_time_limit(0);
		@ignore_user_abort(true);

		// Send CVs
		if ($cfg->enable_resume_email == '1')
		{
			$helper = new JobsHelperEmailResume;
			$helper->sendCV();
		}

		/* Close all expired jobs */

		$query = "SELECT a.* FROM `#__jobsfactory_jobs` a
    			  WHERE '$nowMysql' > a.end_date
                  AND a.close_offer != 1
                  AND a.published = 1
                  AND a.close_by_admin != 1";

		$database->setQuery($query);
		$rows = $database->loadObjectList();
		$job = JTable::getInstance('jobs', 'Table');

		if(count($rows))
		{
			$logtext .= sprintf("Closed %d jobs\r\n",count($rows));

			foreach ($rows as $r)
			{
				$job->bind($r);
				$job->close_offer = 1; //close expired
				$job->closed_date = $nowMysql;

				if($job->store(true)) //store with quicksave
				{
					JTheFactoryEventsHelper::triggerEvent('onAfterCloseJob',array($job));
				}

				/* Cancel all applications for expired jobs */
				$query = "UPDATE #__jobsfactory_candidates c
                          SET c.cancel = 1
                          WHERE c.job_id ={$job->id}";

				$database->setQuery($query);
				$database->execute();
			}
		}

		/* End Close all expired jobs */

		//Daily Jobs (things that should run once a day )
		$daily = $app->input->get('daily','');

		if($daily)
		{
			//Notify upcoming expirations
			$query = "SELECT a.* from `#__jobsfactory_jobs` a
        		 	  WHERE '$nowMysql' >=DATE_ADD(end_date,INTERVAL -1 DAY) and a.close_offer != 1 and published = 1";
			$database->setQuery($query);
			$rows = $database->loadObjectList();

			if(count($rows) > 0)
			{
				$logtext .= sprintf("Soon to expire: %d jobs\r\n",count($rows));

				foreach ($rows as $row)
				{
					$job->bind($row);
					$user = JFactory::getUser($job->userid);
					$job->SendMails(array($user),'job_your_will_expire');

					$query = "SELECT u.* from `#__users` u
        						LEFT JOIN #__jobsfactory_watchlist w ON u.id = w.userid
        						WHERE w.job_id ={$job->id}";

					$database->setQuery($query);
					$users_with_watchlist = $database->loadObjectList();

					$job->SendMails($users_with_watchlist,'job_watchlist_will_expire');
				}
			}

			$model = JModelLegacy::getInstance('Currency','JTheFactoryModel');
			$currtable = JTable::getInstance('CurrencyTable','JTheFactory');

			$currencies = $model->getCurrencyList();
			$default_currency = $model->getDefault();
			$results = array();

			foreach($currencies as $currency)
			{
				if ($currency->name==$default_currency)
				{
					$currtable->load($currency->id);
					$currtable->convert = 1;
					$currtable->store();
					$results[] = $currency->name . " ---> " . $default_currency . " = 1";
					continue;
				}

				$conversion=$model->getYahooCurrency($currency->name, $default_currency);

				if($conversion === false)
				{
					$results[] = JText::_("COM_JOBS_ERROR_CONVERTING") . " {$currency->name} --> $default_currency";
					continue;
				}

				$currtable->load($currency->id);
				$currtable->convert = $conversion;
				$currtable->store();
				$results[] = $currency->name . " ---> " . $default_currency . " = $conversion ";
			}

			$logtext .= implode("\r\n",$results);
			$logtext .= "\r\n";

			//some cleanup
			//Prune old closed jobs
			$interval = intval($cfg->archive);

			if($interval > 0)
			{
				$query = "SELECT id
            				FROM `#__jobsfactory_jobs`
            				WHERE '$nowMysql' > DATE_ADD( closed_date, INTERVAL $interval MONTH )
            				AND close_offer =1
            				AND published =0
            				LIMIT 0 , 50";

				$database->setQuery($query);
				$ids = $database->loadColumn();

				if(count($ids))
				{
					foreach($ids as $id)
					{
						$job->delete($id);

						/* delete all applications for deleted jobs */
						$query = "DELETE FROM `#__jobsfactory_candidates`
                                  WHERE job_id ={$id}";

						$database->setQuery($query);
						$database->execute();
					}
				}

				$logtext .= sprintf("Flushed %d jobs\r\n",count($rows));
			}

			//Close all jobs without a user
			$query = "UPDATE `#__jobsfactory_jobs` AS `j`
            			LEFT JOIN `#__users` AS `b` ON `j`.`userid` = `b`.`id`
            			SET
            				`close_by_admin` = '1',
            				`closed_date` = UTC_TIMESTAMP()
            			WHERE `b`.`id` IS NULL
            		";

			$database->setQuery($query);
			$database->execute();
		}

		$log->log = $logtext;
		$log->store();

		if($debug)
		{
			return;
		}

		ob_clean();
		exit();
	}
}
