<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JJobsfactoryControllerAttachements extends JControllerLegacy
{
	var $_name='jobs';
	var $name='jobs';
    
	function downloadfile() {

        $app = JFactory::getApplication();
        $id = $app->input->getInt('id');

		@set_time_limit(0);

		$job = JTable::getInstance('jobs', 'Table');
		if (!$job->load($id)){
            $app->enqueueMessage(JText::_("COM_JOBS_JOB_DOES_NOT_EXIST"),'warning');
 		    $this->setRedirect( JobsHelperRoute::getJobListRoute());
		    return;
		}

		$filetype = $app->input->get('file','attach');
        $name = $job->file_name;
	    $filepath =JOB_UPLOAD_FOLDER.$job->id.".attach";


		if (!file_exists($filepath)){
            $app->enqueueMessage(JText::_("COM_JOBS_FILE_DOES_NOT_EXIST"),'warning');
 		    $this->setRedirect( JobsHelperRoute::getJobDetailRoute($id));
		    return;
		}

		ob_clean();
		@ignore_user_abort(true);
		@set_time_limit(0);

		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=\"".$name."\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-size: ".filesize($filepath));
		header("Content-Length: ".filesize($filepath));

		if ($stream = fopen($filepath, 'rb'))
		{
			while(!feof($stream) && connection_status() == 0)
				echo(fread($stream,1024*4));
			fclose($stream);
		    exit;
		}else {
            $app->enqueueMessage(JText::_("COM_JOBS_ERROR_READING_FILE"),'warning');
 		    $this->setRedirect( JobsHelperRoute::getJobDetailRoute($id));
		    return;
		}
	}

    function downloadUserResume()
    {
        jimport('joomla.filesystem.file');

        $app = JFactory::getApplication();
        $id = $app->input->getInt('id'); //resumeID

        @set_time_limit(0);
        $user = JTable::getInstance('users', 'Table');

        if (!$user->load($id)){
            $app->enqueueMessage(JText::_("COM_JOBS_RESUME_DOES_NOT_EXIST"),'warning');
            $this->setRedirect( JobsHelperRoute::getJobListRoute());
            return;
        }

        $filetype=$app->input->get('file','attach');
        $name=$user->file_name;
        //$filepath =JOB_UPLOAD_FOLDER.$user->id.".$filetype";
        $filepath =JOB_UPLOAD_FOLDER.$user->userid.".$filetype";

        if (!file_exists($filepath)){
            $app->enqueueMessage(JText::_("COM_JOBS_FILE_DOES_NOT_EXIST"),'warning');
            $this->setRedirect( JobsHelperRoute::getUserdetailsRoute($id));
            return;
        }

        ob_clean();
        @ignore_user_abort(true);
        @set_time_limit(0);

        header("Pragma: public");
        header("Expires: 0"); // set expiration time
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment; filename=\"".$name."\"");
        header("Content-Transfer-Encoding: binary");
        header("Content-size: ".filesize($filepath));
        header("Content-Length: ".filesize($filepath));

        if ($stream = fopen($filepath, 'rb'))
        {
            while(!feof($stream) && connection_status() == 0)
                echo(fread($stream,1024*4));
            fclose($stream);
            exit;
        } else {
            $app->enqueueMessage(JText::_("COM_JOBS_ERROR_READING_FILE"),'warning');
            $this->setRedirect( JobsHelperRoute::getJobDetailRoute($id));
            return;
        }
    }

    function delete()
    {
        $id   = JFactory::getApplication()->input->get('id',null,'default','int');
        $resume = JTable::getInstance('attachment', 'Table');

        $resume->load($id);
        $resume->delete($id);

        $this->setRedirect("index.php?option=com_jobsfactory&task=myuserdetails", JText::_("COM_JOBS_REMOVED"));
    }

    function deletefile()
    {
        $id = $this->input->getInt('id', 0);
        $type = $this->input->getCmd('file', '');
        jimport('joomla.filesystem.file');

        $job = JTable::getInstance('jobs', 'table');
        $job->load($id);

        $fileName = '';
        switch ($type) {
            case 'attach':
                $job->file_name = '';
                $fileName = JOB_UPLOAD_FOLDER . DS . JFile::makeSafe($id . '.' . $type);
                break;
        }

        $job->store();

        if (JFile::exists($fileName)) {
            JFile::delete($fileName);
        }

        $this->setRedirect(JRoute::_('index.php?option=' . APP_EXTENSION . '&task=editjob&id=' . $id, false));
    }

}
