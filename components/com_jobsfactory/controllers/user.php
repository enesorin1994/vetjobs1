<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JJobsfactoryControllerUser extends JControllerLegacy
{
    var $_name = 'jobs';
    var $name = 'jobs';

    function UserDetails()
    {
        $cfg = JTheFactoryHelper::getConfig();
        $user = JFactory::getUser();
        $app = JFactory::getApplication();
        $Itemid = $app->input->getInt('Itemid', 0);

        $isCompany = JobsHelperTools::isCompany($user->id);
        $isCandidate = JobsHelperTools::isCandidate($user->id);

        JHtml::_('behavior.calendar');
        JHtml::_('bootstrap.tooltip');

        if ($isCandidate) {

            if ($r = JobsHelperTools::redirectToProfile()) {
                //3rd Party Profile integration
                $this->setRedirect($r);
                return;
            }

            $user = JobsHelperTools::getUserProfileObject();
            $user->getUserProfile($user->id);
            JFilterOutput::objectHTMLSafe($user, ENT_QUOTES);

            $lists = array();
            $lists["country"] = JHtml::_('country.selectlist', 'country', 'id="country"', $user->country);

            JTheFactoryHelper::modelIncludePath('payments');

            $balance = JModelLegacy::getInstance('balance', 'JTheFactoryModel');
            $lists["balance"] = $balance->getUserBalance();
            $lists["links"] = array(
                "upload_funds" => JobsHelperRoute::getAddFundsRoute(),
                "payment_history" => JobsHelperRoute::getPaymentsHistoryRoute()
            );

            $currency= JModelLegacy::getInstance('currency', 'JTheFactoryModel');
            $lists['default_currency'] = $currency->getDefault();

            $editorName = JFactory::getConfig()->get('editor');
            $editor = JEditor::getInstance($editorName);

            $config = JFactory::getConfig();
            $datenow  = JFactory::getDate('now', $config->get('config.offset'));
            $substract_years = JobsHelperDateTime::substractYears($datenow, 15); //substract 15 years

            $birth_date = (strlen($user->birth_date) == 0) ? $substract_years : $user->birth_date;
            $lists['birth_date_html'] = JHtml::_('jobdate.calendar', $birth_date, 'birth_date', array('class' => 'cal-input', 'readonly' => 1), 'birth_date');

            if ($user->id && !$user->gender)
                $lists['gender'] = JHtml::_('jobgender.selectlist', 'gender', '', 1);
            else
                $lists['gender'] = JHtml::_('jobgender.selectlist', 'gender', '', $user->gender);


            $lists['total_experience'] = JHtml::_('experiencelevel.selectlist', 'total_experience', '', $user->total_experience); //full time, part time
            $lists['studies_level'] = JHtml::_('studieslevel.selectlist', 'study_level', '', ''); //full time, part time

            if (!$user->userid && isset($cfg->candidatevisibility_val))
                $lists['isVisible'] = JHtml::_('employervisibility.selectlist', 'isVisible', '', $cfg->candidatevisibility_val);
            else
                $lists['isVisible'] = JHtml::_('employervisibility.selectlist', 'isVisible', '', $user->isVisible);

            if ($user->userid) {
                $user->last_modified    = JHtml::date($user->modified, $cfg->date_format, false);
                $last_applied           = JobsHelperTools::getCandidateLastApplied($user->userid);

				// ::date defaults to now, we need this to be null.
                if($last_applied)
				{
					$user->last_applied     = JHtml::date($last_applied, $cfg->date_format, false);
				}

                $user->age              = JobsHelperUser::getUserAge($user->userid);
            }

            $lists['countexp'] = 0;
            $lists['countedu'] = 0;
            $user_education = JTable::getInstance('education', 'Table');
            $array_education = $user_education->getUserEducation($user->userid,$user->id);

            $countrytable = JTable::getInstance('country', 'Table');
            //$user_education->country      = $countrytable->getCountryName($user_education->country);

            $lists['countedu'] = count($array_education);

            $user_experienceTable = JTable::getInstance('userexperience', 'Table');
            $array_experience = $user_experienceTable->getUserExperience($user->userid,$user->id); //resume_id replace with ID from jobsfactory_users
            $lists['countexp'] = count($array_experience);

            $lists['domain'] = JHtml::_('factorycategory.select', 'domain', "", 1);

            $date = new JDate();
            $lists['new_edu_start_date_html'] = JHtml::_('jobdate.calendar', $date->toSql(), 'edu_start_date', array('class' => 'cal-input', 'readonly' => 1), 'edu_start_date');
            $lists['new_edu_end_date_html'] = JHtml::_('jobdate.calendar', $date->toSql(), 'edu_end_date', array('class' => 'cal-input', 'readonly' => 1), 'edu_end_date');
            $lists['exp_start_date_html'] = JHtml::_('jobdate.calendar', $date->toSql(), 'exp_start_date', array('class' => 'cal-input', 'readonly' => 1), 'exp_start_date');
            $lists['exp_end_date_html'] = JHtml::_('jobdate.calendar', $date->toSql(), 'exp_end_date', array('class' => 'cal-input', 'readonly' => 1), 'exp_end_date');

            $lists['cancel_form'] = JRoute::_( 'index.php?option=com_jobsfactory&amp;task=userdetails&controller=user&amp;Itemid='. $Itemid );

            $fields = CustomFieldsFactory::getFieldsList("user_profile");
            $fields_html = JHtml::_('customfields.displayfieldshtml', $user, $fields);

            $db = JFactory::getDbo();
            $userTable = JTable::getInstance('users', 'Table');
            if ($user->id) $userTable->load($user->id);

            $db->setQuery("SELECT count(id) FROM #__jobsfactory_pricing WHERE enabled=1 AND itemname = 'contact' ");
            $paymentItems = $db->loadResult();

            JHtml::_('behavior.formvalidation');
            $lists['fieldRequiredlink'] = JHtml::image(JUri::root() . 'components/com_jobsfactory/images/requiredField.png', JText::_('COM_JOBS_REQUIRED'),'class="jobs_required"');

            $view = $this->getView('user', 'html');

            $view->lists            = $lists;
            $view->user             = $user;
            $view->cfg              = $cfg;
            $view->userTable        = $userTable;
            $view->custom_fields_html = $fields_html;
            $view->array_education  = $array_education;
            $view->array_experience = $array_experience;
            $view->user_experienceTable = $user_experienceTable;
            $view->paymentItems     = $paymentItems;
            $view->page_title       = JText::_("COM_JOBS_USER_DETAILS");

            $view->display("t_myuserdetails.tpl");

        } else {
            // isCompany
            $my = JFactory::getUser();
            if ($r = JobsHelperTools::redirectToProfile()) {
                //3rd Party Profile integration
                $this->setRedirect($r);
                return;
            }

            $company = JobsHelperTools::getUserProfileObject();
            $company->getUserProfile($user->id);

            $lists = array();
            $lists["country"] = JHtml::_('country.selectlist', 'country', 'id="country"', $company->country);

            JTheFactoryHelper::modelIncludePath('payments');
            $balance = JModelLegacy::getInstance('balance', 'JTheFactoryModel');
            $lists["balance"] = $balance->getUserBalance();
            $lists["links"] = array(
                "upload_funds" => JobsHelperRoute::getAddFundsRoute(),
                "payment_history" => JobsHelperRoute::getPaymentsHistoryRoute()
            );

            $editorAboutUsName = JFactory::getConfig()->get('editor');
            $editorAboutUs = JEditor::getInstance($editorAboutUsName);
            $lists["about_us"] = $editorAboutUs->display('about_us', $company->about_us, '100%', '400', '70', '15', false);

            if ($company->id) {
                $company->last_modified = JHtml::date($company->modified, $cfg->date_format, false);
                $last_loggedon = JobsHelperTools::getCandidateLastLoggedOn($company->userid);
                $company->last_loggedon = JHtml::date($last_loggedon, $cfg->date_format, false);
            }
            $fields = CustomFieldsFactory::getFieldsList("company_profile");
            $fields_html = JHtml::_('customfields.displayfieldshtml', $company, $fields);

            if (!$company->userid && !$cfg->employervisibility_enable && $cfg->employervisibility_val)
                $lists['isVisible'] = "<input type='hidden' name='isVisible' value='" . $cfg->employervisibility_val . "' >";
            else
                $lists['isVisible'] = JHtml::_('employervisibility.selectlist', 'isVisible', '', $company->isVisible);

            $model = JModelLegacy::getInstance('Company', 'jobsModel');
            $assigned = $model->getAssignedCats($company);
            $lists['activityDomains'] 	 = JHtml::_('factorycategory.select','activitydomain[]','style="width:200px" multiple="multiple" size=7', $assigned,true);

            $db = JFactory::getDbo();
            $db->setQuery("SELECT count(id) FROM #__jobsfactory_pricing WHERE enabled=1");
            $paymentItems = $db->loadResult();

            $task = $app->input->getCmd('task');
            $selected=array();
            $selected[$task]='font-weight:bolder;';

            $companyId = (!empty($company->userid)) ? $company->userid : $user->id;
            $jobsUserId = (isset($company->profileId)) ? $company->profileId : $user->id;

            $lists['fieldRequiredlink'] = JHtml::image(JUri::root() . 'components/com_jobsfactory/images/requiredField.png', JText::_('COM_JOBS_REQUIRED'),'class="jobs_required"');

            $view = $this->getView('user', 'html');
            $view->lists        = $lists;
            $view->task         = $task;
            $view->selectedtask = $selected[$task];
            $view->user         = $company;
            $view->paymentItems = $paymentItems;
            $view->companyId    = $companyId;
            $view->jobsUserId   = $jobsUserId;
            $view->custom_fields_html = $fields_html;
            $view->page_title   = JText::_("COM_JOBS_COMPANY_DETAILS");

            $view->display("t_mycompanydetails.tpl");
        }
    }

    function saveUserDetails()
    {
        $my = JFactory::getUser();
        $id = JFactory::getApplication()->input->getInt('resume_id');
        $user = JTable::getInstance('users', 'Table');

        $model = JModelLegacy::getInstance('User', 'jobsModel');
        $model->bindUserDetails($user);
        $err = $model->saveUserDetails($user);

        if (count($err) || !$user->id) {
            //Some errors!
            $err_message = implode('<br/>', $err);
            if ($user->id)
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute(null, false), $err_message);
            else
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute(null, false), $err_message);
                JTheFactoryEventsHelper::triggerEvent('onAfterSaveResumeError', array($user, $err));
        } else {
            $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($my->id, false), JText::_("COM_JOBS_DETAILS_SAVED"));
            JTheFactoryEventsHelper::triggerEvent('onAfterSaveResumeSuccess', array($user, $err));
        }

    }

    function saveCompanyDetails()
    {
        $my = JFactory::getUser();
        $company = JTable::getInstance('users', 'Table');
        //$company->id = JFactory::getApplication()->input->get('id');//ID from the #__users
        $company->userid = JFactory::getApplication()->input->get('userid'); //ID from the #__jobsfactory_users
		$itemid = JFactory::getApplication()->input->get('Itemid');
        $model = $this->getModel('Company', 'jobsModel');
        $model->bindCompanyDetails($company);
        $err = $model->saveCompanyDetails($company);

        if (count($err) || !$company->id) {

            $err_message = implode('<br/>', $err);
            if ($company->id)
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($my->id, false, $itemid), $err_message);
            else
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute(null, false, $itemid), $err_message);
        } else {
            $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($my->id, false, $itemid), JText::_("COM_JOBS_DETAILS_SAVED"));
        }
    }

    function profile(){

        $userid = $this->input->getInt('id');
        $view = $this->getView('Profile', 'html');
        $user = JFactory::getUser();

        $view->userid = $userid;
        $view->user = $user;

        $view->display('t_groupselect.tpl');
    }

    function UserProfile()
    {
        $app = JFactory::getApplication();
        $id = $app->input->getInt('id');

        if (!$id) {
            $app->enqueueMessage(JText::_("COM_JOBS_SELECT_AN_USER"),'warning');
            $this->setRedirect(JobsHelperRoute::getJobListRoute());
        }

        if ($r = JobsHelperTools::redirectToProfile($id)) {
            //3rd Party Profile integration
            $this->setRedirect($r);
            return;
        }

        $cfg = JTheFactoryHelper::getConfig();
        $user = JobsHelperTools::getUserProfileObject();
        $user->getUserProfile($id);

        $userTable = JTable::getInstance('users', 'Table');
        if ($user->id) $userTable->load($user->id);
        //JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_jobsfactory' . DS . 'tables');
        $countrytable = JTable::getInstance('country', 'Table');

        $user_groups = JAccess::getGroupsByUser($id, false);
        $isCandidate = count(array_intersect($user_groups, $cfg->candidate_groups)) > 0;
        $isCompany = count(array_intersect($user_groups, $cfg->company_groups)) > 0;

        if ($isCompany) {

            $model = JModelLegacy::getInstance('Company', 'jobsModel');
            $assigned = $model->getAssignedCats($user);

            $activityDomains = ''; // to do select activity domains
            if ($assigned) {
                $activityDomains = jobsHelperUser::getCompanyActivityFields(implode(',',$assigned));
            }
        }

        if ($isCandidate) {
            $applicationModel = $this->getModel('Applications', 'jobsModel');
            $lists = array();

            $lists['applications'] = $applicationModel->loadUserItems($id);

            $user_education = JTable::getInstance('education', 'Table');
            $array_education = $user_education->getUserEducation($user->userid,$user->id);

            $lists['countedu'] = count($array_education);

            $user_experienceTable = JTable::getInstance('userexperience', 'Table');
            $array_experience = $user_experienceTable->getUserExperience($user->userid,$user->id); //resume_id replace with ID from jobsfactory_users
            $lists['countexp'] = count($array_experience);

            $fields = CustomFieldsFactory::getFieldsList("user_profile");
            $fields_html = JHtml::_('customfields.displayfieldshtml', $user, $fields);

            $user->age  = JobsHelperUser::getUserAge($user->userid);
            $lists['total_experience']  = JobsHelperUser::getResumeExperience($user->total_experience);

            $user->country      = $countrytable->getCountryName($user->country);

            JTheFactoryHelper::modelIncludePath('payments');
            $currency= JModelLegacy::getInstance('currency', 'JTheFactoryModel');
            $lists['default_currency'] = $currency->getDefault();

            if ($user->email != '') {
                $user->email = JobsHelperTools::cloack_email($user->email);
            }
            if ($user->Hotmail != '') {
                $user->Hotmail = JobsHelperTools::cloack_email($user->Hotmail);
            }

        } else {
            $jobModel = $this->getModel('Jobs', 'jobsModel');
            $lists = array();
            $lists['jobslist'] = $jobModel->getCompanyJobsList($id);

            $user->country      = $countrytable->getCountryName($user->country);
            if ($user->contactemail != '') {
                $user->contactemail = JobsHelperTools::cloack_email($user->contactemail);
            }

            if ($cfg->jobs_allow_rating) {

               $company_ratings_info = JobsHelperUser::getCompanyRatings($user->userid);

               $user->user_rating = ($company_ratings_info != null) ? $company_ratings_info->user_rating : '';  //$items[$key]->user_rating
               $user->users_count = ($company_ratings_info != null) ? $company_ratings_info->users_count : '';  //$items[$key]->users_count
               $user->my_rating_user = JobsHelperUser::getIsMyRating($user->id,$user->userid); //$items[$key]->my_rating_user
           }

        }

         JHtml::_('behavior.multiselect');
         JHtml::_('dropdown.init');
         JHtml::_('formbehavior.chosen', 'select');

        $view = $this->getView('user', 'html');
        $view->lists = $lists;
        $view->user = clone $user; //using CLONE since the profile object might be reset in onBeforeDisplay
        $view->userTable = $userTable;
        $view->page_title = JText::_("COM_JOBS_USER_PROFILE") . ": " . $user->username;

        if ($isCandidate) {

            $view->custom_fields_html   = $fields_html;
            $view->array_education      = $array_education;
            $view->array_experience     = $array_experience;
            $view->user_experienceTable = $user_experienceTable;

            $view->display("t_userdetails.tpl");
        } else {
            $view->activityDomains = $activityDomains;
            $view->display("t_companydetails.tpl");
        }
    }


    function FormUserEducation()
    {

        $document	= JFactory::getDocument();
        $cfg = JTheFactoryHelper::getConfig();
        $lists['countedu'] = 0;

        $view = $this->getView('user', 'html');
        $user_education = JTable::getInstance('education', 'Table');
        $educationModel = JModelLegacy::getInstance('Usereducation', 'usereducationModel');

        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile();
        $date = new JDate();

        if ($userprofile->id && !$userprofile->gender)
            $lists['gender'] = JHtml::_('jobgender.selectlist', 'gender', '', $userprofile->gender);
        else
            $lists['gender'] = JHtml::_('jobgender.selectlist', 'gender', '', 1);

        $lists['domain']            = JHtml::_('factorycategory.select', 'domain', "", 1);
        $lists['country']           = JHtml::_('country.selectlist', 'country', 'id="country"', $userprofile->country, true);
        $lists['total_experience']  = JHtml::_('experiencelevel.selectlist', 'total_experience', '', $userprofile->total_experience); //full time, part time
        $lists['studies_level']     = JHtml::_('studieslevel.selectlist', 'study_level', '', ''); //full time, part time
        $lists['edu_start_date_html'] = JHtml::_('jobdate.calendar', $user_education->start_date, "start_date$user_education->id", array('class' => 'cal-input', 'readonly' => 1), "start_date$user_education->id");
        $lists['edu_end_date_html'] = JHtml::_('jobdate.calendar', $user_education->end_date, "end_date$user_education->id", array('class' => 'cal-input', 'readonly' => 1), "end_date$user_education->id");

        if ($userprofile->id) {
            $user_education = JTable::getInstance('education', 'Table');
            $array_education = $user_education->getUserEducation($userprofile->userid,$userprofile->id);
            $lists['countedu'] = count($array_education);

            $view->array_education = $array_education;
        }

        $root = JUri::root();
        $document->addScriptDeclaration(' var root = "'.$root.'";');

        if ($userprofile->id) {
            $document->addScriptDeclaration(' var resumeid = "'.$userprofile->id.'";');
        }

        $view->resume = $userprofile;
        $view->user = $userprofile;
        $view->lists = $lists;

        $view->display("t_myusereducation.tpl");
    }

    function saveEducation()
    {
        $app = JFactory::getApplication();
        $education_id = $app->input->getInt('id');
        //$resume_id = $app->input->getInt('resume_id');//resume_id = user->id
		$my = JFactory::getUser();

        $user_studylevel = JTable::getInstance('education', 'Table');
        $model = JModelLegacy::getInstance('Usereducation', 'usereducationModel');

        $model->bindUsereducation($user_studylevel);
        $err = $model->saveUsereducation($user_studylevel);

        if (count($err) || !$user_studylevel->id) {
            //Some errors!
            $err_message = implode('<br/>', $err);

            if ($education_id)
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($resume_id, false), $err_message);
            else
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($resume_id, false), $err_message);
                JTheFactoryEventsHelper::triggerEvent('onAfterSaveUserExperienceError', array($education_id, $err));
        } else {

            $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($my->id, false), JText::_("COM_JOBS_EDUCATION_SAVED"));
            JTheFactoryEventsHelper::triggerEvent('onAfterSaveUserEducationSuccess', array($resume_id, $err));
        }
    }

    /**
     *
     */
    public function delete_education()
    {
        $app = JFactory::getApplication();
        $education_id = $app->input->getInt('id');
        $resume_id = $app->input->getInt('resume_id');//resume_id = user->id

        $user_studylevel = JTable::getInstance('education', 'Table');
        $model = JModelLegacy::getInstance('Usereducation', 'usereducationModel');

        if (!$user_studylevel->load($education_id)) {
            $app->enqueueMessage(JText::_("COM_JOBS_EDUCATION_DOES_NOT_EXIST"),'warning');
            $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($resume_id, false));
            return false;
        }

        if ($model->deleteUserducation($education_id,$resume_id)) {
          $msg = JText::_('COM_JOBS_EDUCATION_DELETED_SUCCESSFULLY');
        }
        else {
          $msg = JText::_('COM_JOBS_EDUCATION_ERR_DELETING');
          JFactory::getApplication()->enqueueMessage($model->getError(), 'error');
        }

        $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($resume_id, false), $msg);
        //$this->setRedirect(JobsHelperRoute::getJobListRoute(null, false), $msg);
        return true;
    }

    function FormUserExperience()
    {

        $document	= JFactory::getDocument();
        $app        = JFactory::getApplication();
        $resume_id  = $app->input->getInt('id');//resume_id = user->id
        $lists['countexp'] = 0;

        $view = $this->getView('user', 'html');
        $educationModel = JModelLegacy::getInstance('Usereducation', 'usereducationModel');

        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile();
        $date = new JDate();

        if ($userprofile->id && !$userprofile->gender)
            $lists['gender'] = JHtml::_('jobgender.selectlist', 'gender', '', $userprofile->gender);
        else
            $lists['gender'] = JHtml::_('jobgender.selectlist', 'gender', '', 1);

        $lists['domain']            = JHtml::_('factorycategory.select', 'domain', "", 1);
        $lists['country']           = JHtml::_('country.selectlist', 'country', 'id="country"', $userprofile->country, true);
        $lists['total_experience']  = JHtml::_('experiencelevel.selectlist', 'total_experience', '', $userprofile->total_experience); //full time, part time
        $lists['studies_level']     = JHtml::_('studieslevel.selectlist', 'study_level', '', ''); //full time, part time
        $lists['exp_start_date_html'] = JHtml::_('jobdate.calendar', $date->toSql(), 'start_date', array('class' => 'cal-input', 'readonly' => 1), 'start_date');
        $lists['exp_end_date_html'] = JHtml::_('jobdate.calendar', $date->toSql(), 'end_date', array('class' => 'cal-input', 'readonly' => 1), 'end_date');

        if ($userprofile->id) {
            $user_experience = JTable::getInstance('userexperience', 'Table');
            $array_experience = $user_experience->getUserExperience($userprofile->userid,$userprofile->id);
            $lists['countexp'] = count($array_experience);

            $view->array_experience = $array_experience;
        }

        $root = JUri::root();
        $document->addScriptDeclaration(' var root = "'.$root.'";');
        if ($userprofile->id) {
            $document->addScriptDeclaration(' var resumeid = "'.$userprofile->id.'";');
        }

        $view->resume = $userprofile;
        $view->user = $userprofile;
        $view->lists = $lists;

        $view->display("t_myuserexperience.tpl");
    }

    function FormUserSkills()
    {
        $document	= JFactory::getDocument();
        $app = JFactory::getApplication();

        $view = $this->getView('user', 'html');
        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile();
        $date = new JDate();

        $root = JUri::root();
        $document->addScriptDeclaration(' var root = "'.$root.'";');
        if ($userprofile->id) {
            $document->addScriptDeclaration(' var resumeid = "'.$userprofile->id.'";');
        }

        $view->user = $userprofile;

        $view->display("t_myuserskills.tpl");
    }

    function saveUserSkills()
    {
        $my = JFactory::getUser();
        $id = JFactory::getApplication()->input->getInt('id');
        $user = JTable::getInstance('users', 'Table');

        $model = JModelLegacy::getInstance('User', 'jobsModel');
        $model->bindUserSkills($user);
        $err = $model->saveUserSkills($user);

        if (count($err) || !$user->id) {
            //Some errors!
            $err_message = implode('<br/>', $err);
            if ($id)
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($my->id, false), $err_message);
            else
                $this->setRedirect(JobsHelperRoute::getUserdetailsRoute(null, false), $err_message);
                JTheFactoryEventsHelper::triggerEvent('onAfterSaveResumeError', array($user, $err));
        } else {
            $this->setRedirect(JobsHelperRoute::getUserdetailsRoute($my->id, false), JText::_("COM_JOBS_SKILLS_SAVED"));
            JTheFactoryEventsHelper::triggerEvent('onAfterSaveResumeSuccess', array($user, $err));
        }
    }

    public function setUserGroup() {
	var_dump('unused function?');exit;
       $cfg = JTheFactoryHelper::getConfig();
       $app = JFactory::getApplication();
       $userid = $app->input->get('id', null);

       $user_groups = JAccess::getGroupsByUser($userid, false);
       $isCandidate = count(array_intersect($user_groups, $cfg->candidate_groups)) > 0;
       $isCompany = count(array_intersect($user_groups, $cfg->company_groups)) > 0;

        if ($isCandidate) {
            $this->setRedirect(JobsHelperRoute::getUserGroupRoute($userid), JText::_("COM_JOBS_SET_CANDIDATE_GROUP"));
        }

        if ($isCompany) {
            $this->setRedirect(JobsHelperRoute::getUserGroupRoute($userid), JText::_("COM_JOBS_SET_COMPANY_GROUP"));
        }

    }

    function searchUsers()
    {
        $params = JComponentHelper::getParams('com_jobsfactory');
        $fields = CustomFieldsFactory::getSearchableFieldsList('user_profile');

        $lists = array();
        $lists['country'] = JHtml::_('country.selectlist', 'country', 'id="country"', '');

        $opts = array();
        $opts[] = JHtml::_('select.option', 'all', JText::_("JALL"));
        $opts[] = JHtml::_('select.option', 1,JText::_("COM_JOBS_MALE"));
        $opts[] = JHtml::_('select.option', 0,JText::_("COM_JOBS_FEMALE"));
        $lists['gender'] = JHtml::_('select.radiolist',  $opts, 'gender', '' , 'value', 'text', 'all');

        //$lists['gender'] = JHtml::_('jobgender.selectlist', 'gender', '', 1);

        $levelnames = JobsHelperUser::getExperienceLevels();
        $emptyopt[] = JHtml::_('select.option', '', JText::_("COM_JOBS__ANY_LEVEL"));
        $levelnames = array_merge($emptyopt, $levelnames);
        $lists['total_experience']  = JHtml::_('select.genericlist',$levelnames,'total_experience','style="width:200px" ','value','text','');
        $lists['studies_level']     = JHtml::_('studieslevel.selectlist', 'study_level', '', ''); //full time, part time
        $lists['custom_fields']     = JHtml::_('customfields.displaysearchhtml', $fields);

        $view = $this->getView('user', 'html');
        $view->page_title = JText::_("COM_JOBS_SEARCH_USERS");
        $view->custom_fields = $fields;
        $view->lists = $lists;

        JHtml::_('formbehavior.chosen', 'select');

        $view->display("t_search_users.tpl");
    }

    function showUsers()
    {
        $uri = JURI::getInstance();
        $model = JModelLegacy::getInstance('User', 'jobsModel');
        $applicationmodel = JModelLegacy::getInstance('Applications', 'jobsModel');

        $items = $model->loadItems();

        for ($key = 0; $key < count($items); $key++) {
            $user = &$items[$key];
            $user->link = JobsHelperRoute::getUserdetailsRoute($user->userid);
            if ($user->email != '') {
                $user->email =  JobsHelperTools::cloack_email($user->email);
            }
            if ($user->Hotmail != '') {
                $user->Hotmail = JobsHelperTools::cloack_email($user->Hotmail);
            }
            $user->applications = $applicationmodel->getUserApplications($user->userid);
        }
        $filters = $model->getFilters();
        $pagination = $model->get('pagination');

        $view = $this->getView('user', 'html');
        $view->action = JRoute::_(JFilterOutput::ampReplace($uri->toString()));
        $view->users = $items;
        $view->sfilters = $filters;
        $view->pagination = $pagination;
        $view->page_title = JText::_("COM_JOBS_USER_SEARCH_RESULT");

        $view->display("t_showusers.tpl");
    }

    function searchCompanies()
    {
        $fields = CustomFieldsFactory::getSearchableFieldsList('company_profile');//from #__jobsfactory_fields

        $lists = array();
        $lists['country']           = JHtml::_('country.selectlist', 'country', 'id="country"', '');
        $lists['activityDomains']   = JHtml::_('factorycategory.select', 'activitydomain', 'style="width:200px"', '', false, false, true);
        $lists['custom_fields']     = JHtml::_('customfields.displaysearchhtml', $fields);
        $view = $this->getView('user', 'html');

        $view->page_title = JText::_("COM_JOBS_SEARCH_COMPANIES");
        $view->custom_fields = $fields;
        $view->lists = $lists;

        JHtml::_('formbehavior.chosen', 'select');

        $view->display("t_search_companies.tpl");
    }


    public function showCompanies()
    {
        $uri = JURI::getInstance();
        $model = JModelLegacy::getInstance('Company', 'jobsModel');
        $items = $model->loadItems();
		//var_dump($items); exit;
        for ($key = 0; $key < count($items); $key++) {
            $user =& $items[$key];

            $user->link = JobsHelperRoute::getUserdetailsRoute($user->userid);
            if (isset($user->contactemail)) {
                $user->contactemail = JobsHelperTools::cloack_email($user->contactemail);
            }
            if ($user->email != '') {
                $user->email =  JobsHelperTools::cloack_email($user->email);
            }
        }
        $filters = $model->getFilters();

        $pagination = $model->get('pagination');

        $view = $this->getView('user', 'html');
        $view->action = JRoute::_(JFilterOutput::ampReplace($uri->toString()));
        $view->users = $items;
        $view->sfilters = $filters;
        $view->pagination = $pagination;
        $view->page_title = JText::_("COM_JOBS_USER_SEARCH_RESULT");

        $view->display("t_showcompanies.tpl");
    }

    function paymentbalance()
    {

        $cfg = JTheFactoryHelper::getConfig();
        $user = JFactory::getUser();
        $user_groups = JAccess::getGroupsByUser($user->id, false);
        $isCandidate = count(array_intersect($user_groups, $cfg->candidate_groups)) > 0;
        $isCompany = count(array_intersect($user_groups, $cfg->company_groups)) > 0;
        JTheFactoryHelper::modelIncludePath('payments');

        $lists = array();
        $balance = JModelLegacy::getInstance('balance', 'JTheFactoryModel');
        $lists["balance"] = $balance->getUserBalance();
        $lists["links"] = array(
              "upload_funds" => JobsHelperRoute::getAddFundsRoute(),
              "payment_history" => JobsHelperRoute::getPaymentsHistoryRoute()
        );

        $view = $this->getView('User', 'html');
        $view->lists = $lists;

        $view->display('t_mypaymentbalance.tpl');
    }

    function deletefile()
    {

        $app = JFactory::getApplication();
        $id = $app->input->getInt('id'); //resumeID
        $type = $app->input->getCmd('file','attach');
        jimport('joomla.filesystem.file');

        $user = JTable::getInstance('users', 'table');
        $user->load($id);

        $fileName = '';
        $user->file_name = '';
        $fileName = JOB_UPLOAD_FOLDER . JFile::makeSafe($id . '.' . $type);

        $user->store();

        if (JFile::exists($fileName)) {
            JFile::delete($fileName);
        }

        $this->setRedirect(JRoute::_('index.php?option=com_jobsfactory&controller=user&task=UserProfile&id=' . $id, false));
    }

    public function vote()
    {
        $app = JFactory::getApplication();
        $id = $app->input->getInt('id', 0);
        $rating = $app->input->getInt('rating', 0);
        $model = $this->getModel('Company', 'jobsModel');
        $response = array();

        if ($model->vote($id, $rating)) {
          $response['status'] = 1;
        }
        else {
          $response['status'] = 0;
        }

        echo json_encode($response);

        return true;
    }

    function listjobs() {
        $this->setRedirect(JobsHelperRoute::getJobListRoute());
        return;
    }

	public function emailResume($userid = null, $touser = true)
	{
		if (!$userid)
		{
			$userid = JFactory::getApplication()->input->get('id');
			if (!JFactory::getUser($userid))
			{
				return;
			}
		}

		$helper = new JobsHelperEmailResume;
		$helper->sendCV($userid);

		JFactory::getApplication()->enqueueMessage(JText::_('COM_JOBS_RESUME_SENT'));

		$this->setRedirect(JobsHelperRoute::getUserdetailsRoute(null, false));

		return;
	}
}