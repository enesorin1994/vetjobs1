<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Smarty
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JobsSmartyView extends JTheFactorySmartyView
{
    function __construct()
    {
        JViewLegacy::__construct();
        $this->smarty = new JobsSmarty();
        
    }
    
}
