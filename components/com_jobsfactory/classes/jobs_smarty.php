<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Smarty
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JobsSmarty extends JTheFactorySmarty
{
    function __construct()
    {
        parent::__construct();

		$this->register_function('printdate',array($this,'smarty_printdate'));
		$this->register_function('set_css',array($this,'smarty_set_css'));
        $this->register_function('generate_url', array($this, 'smarty_generate_url'));
		$this->register_function('set_title', array($this, 'smarty_set_title'));

        $cfg = JTheFactoryHelper::getConfig();
		if($cfg->theme)
			$this->template_dir=JPATH_COMPONENT_SITE.DS.'templates'.DS.$cfg->theme.DS;

        $this->assign('cfg',$cfg);
		$this->assign('IMAGE_ROOT',JURI::root().'components/com_jobsfactory/images/');
        $this->assign('JOB_PICTURES',JOB_PICTURES);
        $this->assign('COMPANY_PICTURES',COMPANY_PICTURES);
        $this->assign('RESUME_PICTURES',RESUME_PICTURES);
        $this->assign('EMPLOYER_VISIBILITIES',array(
            'EMPLOYER_TYPE_PUBLIC'=> EMPLOYER_TYPE_PUBLIC,
            'EMPLOYER_TYPE_CONFIDENTIAL'=>EMPLOYER_TYPE_CONFIDENTIAL,
            'EMPLOYER_TYPE_ONE_ON_ONE'=> EMPLOYER_TYPE_ONE_ON_ONE,
            'EMPLOYER_TYPE_LIMITED'=> EMPLOYER_TYPE_LIMITED
        ));
        $this->assign('TEMPLATE_IMAGES',JURI::root().'components/com_jobsfactory/templates/'.$cfg->theme.'/images/');
        $this->assign('links',new JobsHelperRoute() );
    }

    function display($tpl_name, $cache_id=null, $compile_id=null){
//    var image_link_dir='/components/com_jobsfactory/images/';
        $doc = JFactory::getDocument();
        $doc->addScriptDeclaration("var image_link_dir='".
            JURI::root().'components/com_jobsfactory/images/'
        ."';");
                
        parent::display($tpl_name);
    }

	public function smarty_set_title($title)
	{
		if ($title['title'])
		{
			$app = JFactory::getApplication();
			$site_title = $app->get('sitename');

			JFactory::getDocument()->setTitle($site_title . ' - ' . $title['title']);
		}
	}

    function smarty_printdate($params, &$smarty) {
        $res = "";
        if (!empty($params['date'])) {
            $cfg        = JTheFactoryHelper::getConfig();
            $dateformat = $cfg->date_format;
            if ( $params['use_hour'] )
                $dateformat.=" H:i";
            $res    = JHtml::date($params['date'],$dateformat,false);
        }
        return $res;
    }

    //Used for cloaking emails
    function smarty_jobs_print_encoded($params, &$smarty){
        $extra = '';
    
        if (empty($params['address'])) {
            return "";
        } else {
            $address = $params['address'];
        }
    
        $text = $address;
        $encode = (empty($params['encode'])) ? 'none' : $params['encode'];
        
        if (!in_array($encode,array('javascript','javascript_charcode','hex','none')) ) {
            $smarty->trigger_error("print_encoded: 'encode' parameter must be none, javascript or hex");
            return;
        }
    
        if ($encode == 'javascript' ) {
            $string = 'document.write(\''.$text.'\');';
    
            $js_encode = '';
            for ($x=0; $x < strlen($string); $x++) {
                $js_encode .= '%' . bin2hex($string[$x]);
            }
    
            return '<script type="text/javascript">eval(unescape(\''.$js_encode.'\'))</script>';
    
        } elseif ($encode == 'javascript_charcode' ) {
            $string = $text;
    
            for($x = 0, $y = strlen($string); $x < $y; $x++ ) {
                $ord[] = ord($string[$x]);   
            }
    
            $_ret = "<script type=\"text/javascript\" language=\"javascript\">\n";
            $_ret .= "<!--\n";
            $_ret .= "{document.write(String.fromCharCode(";
            $_ret .= implode(',',$ord);
            $_ret .= "))";
            $_ret .= "}\n";
            $_ret .= "//-->\n";
            $_ret .= "</script>\n";
            
            return $_ret;
            
            
        } elseif ($encode == 'hex') {
    
            preg_match('!^(.*)(\?.*)$!',$address,$match);
            if(!empty($match[2])) {
                $smarty->trigger_error("mailto: hex encoding does not work with extra attributes. Try javascript.");
                return;
            }
            $text_encode = '';
            for ($x=0; $x < strlen($text); $x++) {
                $text_encode .= '&#x' . bin2hex($text[$x]).';';
            }
    
            $mailto = "&#109;&#97;&#105;&#108;&#116;&#111;&#58;";
            return $text_encode;
    
        } else {
            // no encoding
            return $text;
    
        }
    
    	
    }
    
    function smarty_set_css($params, &$smarty) {
        $cfg = JTheFactoryHelper::getConfig();
    	$doc = JFactory::getDocument();

    	if ($cfg->theme!="" && file_exists(JPATH_ROOT."/components/com_jobsfactory/templates/".strtolower($cfg->theme)."/".APP_PREFIX."_template.css") )
            $doc->addStyleSheet(JURI::root().'components/'.APP_EXTENSION.'/templates/'.strtolower($cfg->theme).'/'.APP_PREFIX.'_template.css');
    	else
            $doc->addStyleSheet(JURI::root().'components/'.APP_EXTENSION.'/templates/default/'.APP_PREFIX.'_template.css');
    }

    /**
     * Set custom tooltip icon
     *
     * @param $params
     * @param $smarty
     *
     * @return mixed|string
     */
    function smarty_infobullet($params, &$smarty)
    {
        $params['icon'] = JURI::root() . 'components/' . APP_EXTENSION . '/images/tooltip.png';

        return parent::smarty_infobullet($params, $smarty);

    }

    public function smarty_generate_url($params)
    {
        $callable = array('JobsHelperRoute', 'get' . $params['_']);

        if (!is_callable($callable)) {
          return '#';
        }

        unset($params['_']);

        return call_user_func_array($callable, $params);
    }
        
}
