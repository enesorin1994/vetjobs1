<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author ThePHPfactory
 * @copyright Copyright (C) 2016 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thephpfactory.com
 * Technical Support: Forum - http://www.thephpfactory.com/forum/
 * Since : 1/14/16 emailresume.php
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access.');

class JobsHelperEmailResume
{
	private function format_date($date)
	{
		$cfg        = JTheFactoryHelper::getConfig();
		$dateformat = $cfg->date_format;
		//$dateformat .= " H:i";
		$res    = JHtml::date($date, $dateformat, false);

		return $res;
	}

	private function getUsers($userid = null)
	{
		$query = "SELECT *,ju.name AS name, u.email AS joomla_email, ux.levelname AS experience_level, ju.email AS jobs_email,c.name AS country
					FROM `#__jobsfactory_users` AS ju
					LEFT JOIN `#__users` AS u ON ju.userid = u.id
					LEFT JOIN `#__jobsfactory_country` AS c ON ju.country =  c.id
					LEFT JOIN `#__jobsfactory_experiencelevel` AS ux ON ju.total_experience = ux.id";

		if ($userid == null)
		{
			$query .= " WHERE ju.isCompany = '0' AND ju.cv_notified = '0'
						AND (ju.cv_date_notified IS NULL OR ju.cv_date_notified < '" . new JDate('now -24 hour') . "')
						AND ju.modified < '". new JDate('now -1 hour')."'
						AND ju.modified > '" . new JDate('now -24 hour') ."'";
		}
		else
		{
			$query .= " WHERE u.id = " . $userid;
		}

		$db = JFactory::getDbo();
		$db->setQuery($query);
		$users = $db->loadObjectList();

		return $users;
	}

	public function sendCV($userid = null)
	{
		$users = $this->getUsers($userid);

		if (!$users)
		{
			return false;
		}

		foreach ($users as $k => $v)
		{

			$db = JFactory::getDbo();
			// load additional profile data.

			//get Experience
			$query = "SELECT *, c.catname AS domain FROM `#__jobsfactory_userexperience` AS ux
						LEFT JOIN `#__jobsfactory_categories` AS c ON ux.domain = c.id
						WHERE `userid` = ". $v->userid;
			$db->setQuery($query);
			$user_experience = $db->loadObjectList();
			$v->user_experiences = $user_experience;

			// get Education
			$query = "SELECT *, sl.levelname AS study_level FROM `#__jobsfactory_education` AS e
						LEFT JOIN `#__jobsfactory_studieslevel` AS sl ON e.study_level = sl.id
						WHERE `userid` = ".$v->userid;
			$db->setQuery($query);
			$user_education = $db->loadObjectList();
			$v->user_educations = $user_education;


			// get Custom Fields
			$fields = CustomFieldsFactory::getFieldsList("user_profile");

			$cf = new stdClass();
			foreach ($fields as $x => $y)
			{
				$cf->{$y->db_name} = new stdClass();
				$cf->{$y->db_name}->name = $y->name;
				$cf->{$y->db_name}->value = $v->{$y->db_name};
			}

			if(isset($cf))
			{
				$v->custom_fields = $cf;
			}

			$mailer = JFactory::getMailer();
			$config = JFactory::getConfig();

			$mailer->setSender($config->get('mailfrom'));

			$cfg = JTheFactoryHelper::getConfig();


			if(!$userid)
			{
				if($cfg->resume_email_address != null)
				{
					$mailer->addRecipient($cfg->resume_email_address);
				}
				else
				{
					$mailer->addRecipient($config->get('mailfrom'));
				}
			}
			else
			{
				$mailer->addRecipient($v->email);
			}

			if($v->cv_date_notified == null)
			{
				$mailer->setSubject(JText::sprintf('COM_JOBSFACTORY_CV_EMAIL_SUBJECT_NEW', $v->name));
			}
			else
			{
				$mailer->setSubject(JText::sprintf('COM_JOBSFACTORY_CV_EMAIL_SUBJECT', $v->name));
			}

			$mailer->isHtml(true);
			$mailer->Encoding = 'base64';

			$html_body = $this->cvMailBody($v);
			$mailer->setBody($html_body);

			$config = JFactory::getConfig();
			// Don't completely break the email if attachments don't work.
			if ($config->get('mailer') == 'smtp'){
				if ($v->file_name != null)
				{
					jimport('joomla.filesystem.file');
					$filetype = 'attach';
					$filepath = JOB_UPLOAD_FOLDER . $v->userid . ".$filetype";

					if (file_exists($filepath)){
						$mailer->addAttachment($filepath, $v->file_name);
					}
				}
			}

			$sent = $mailer->Send();

			if($sent)
			{
				$query = "UPDATE #__jobsfactory_users c
                          SET c.cv_date_notified = '". new JDate('now') . "'
                          WHERE c.userid ={$v->userid}";
				$db->setQuery($query);
				$db->execute();

				return true;
			}
			else
			{
				return false;
				//TODO log the failure
			}
		}

		return false;
	}

	private function cvMailBody($data)
	{
		JLoader::import( 'currency', JPATH_ADMINISTRATOR . '/components/com_jobsfactory/thefactory/payments/models' );
		$modelCurrency = JModelLegacy::getInstance('Currency','JTheFactoryModel');
		$currency = $modelCurrency->getDefault();

		$html = "";

		if($data->cv_date_notified == null)
		{
			$html .= "<h1>" . JText::sprintf('COM_JOBSFACTORY_CV_EMAIL_SUBJECT_NEW', $data->name). "</h1>";
		}
		else
		{
			$html .= "<h1>" . JText::sprintf('COM_JOBSFACTORY_CV_EMAIL_SUBJECT', $data->name). "</h1>";
		}

		$html .= "<br>";
		$html .= "<h2>Personal Information</h2>";
		$html .= "<ul style='list-style-type: none;'>";
		$html .= "<li>" . JText::_('COM_JOBS_NAME') . ": " . $data->name . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_SURNAME') . ": " . $data->surname . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_BIRTHDATE') . ": " . $this->format_date($data->birth_date) . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_CANDIDATE_GENDER') . ": " . (is_null($data->gender) ? 'Female' : 'Male')  . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_MARITAL_STATUS') . ": " . $data->marital_status . "</li>";
		$html .= "<li></li>";
		$html .= "<li>" . JText::_('COM_JOBS_PHONE') . ": " . $data->phone . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_EMAIL') . ": " . $data->jobs_email . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_ADDRESS') . ": " . $data->address . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_CITY') . ": " . $data->city . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_COUNTRY') . ": " . $data->country . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_COMPANY_IDENTIFICATION_NO') . ": " . $data->useruniqueid . "</li>";
		$html .= "<li></li>";
		$html .= "<li>" . JText::_('COM_JOBS_YM') . ": " . $data->YM . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_HOTMAIL') . ": " . $data->Hotmail . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_LINKEDIN') . ": " . $data->linkedIN . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_SKYPE') . ": " . $data->Skype . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_FACEBOOK') . ": " . $data->facebook . "</li>";
		$html .= "<li></li>";
		$html .= "<li>" . JText::_('COM_JOBS_TOTAL_EXPERIENCE') . ": " . $data->experience_level . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_SALARY') . ": " . $data->desired_salary . " " .  $currency . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_PROFILE_VISIBILITY') . ": " . (is_null($data->isVisible) ? 'No' : 'Yes') . "</li>";
		$html .= "</ul>";

		$html .= "<ul style='list-style-type: none;'>";
		$html .= "<li>" . JText::_('COM_JOBS_USER_ACHIEVEMENTS') . ": " . $data->user_achievements . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_USER_GOALS') . ": " . $data->user_goals . "</li>";
		$html .= "<li>" . JText::_('COM_JOBS_USER_HOBBIES') . ": " . $data->user_hobbies . "</li>";
		$html .= "</ul>";

		if($data->user_experiences)
		{
			$html .= "<h2>" . JText::_('COM_JOBS_TAB_EXPERIENCE') . "</h2>";

			foreach ($data->user_experiences as $k => $v)
			{
				$html .= "<ul style='list-style-type: none;'>";
				$html .= "<li>" . JText::_('COM_JOBS_COMPANY') . ": " . $v->company . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_POSITION') . ": " . $v->position . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_EXP_RESPONSIBILITIES') . ": " . $v->exp_description . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_DOMAIN') . ": " . $v->domain . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_START_DATE') . ": " . $this->format_date($v->start_date) . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_END_DATE') . ": " . $this->format_date($v->end_date) . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_CITY') . ": " . $v->city . "</li>";
				$html .= "</ul>";
			}
		}
		else
		{
			$html .= "<br>";
			$html .= JText::_('COM_JOBS_NO_EXPERIENCE_ADDED');
		}

		if($data->user_educations)
		{
			$html .= "<h2>" . JText::_('COM_JOBS_TAB_EDUCATION') . "</h2>";

			foreach ($data->user_educations as $k => $v)
			{
				$html .= "<ul style='list-style-type: none;'>";
				$html .= "<li>" . JText::_('COM_JOBS_STUDY_LEVEL') . ": " . $v->study_level . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_EDU_INSTITUTION') . ": " . $v->edu_institution . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_COUNTRY') . ": " . $v->country . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_CITY') . ": " . $v->city . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_START_DATE') . ": " . $this->format_date($v->start_date) . "</li>";
				$html .= "<li>" . JText::_('COM_JOBS_END_DATE') . ": " . $this->format_date($v->end_date) . "</li>";
				$html .= "</ul>";
			}


		}
		else
		{
			$html .= "<br>";
			$html .= JText::_('COM_JOBS_NO_EDUCATION_ADDED');
		}

		if($data->custom_fields)
		{
			$html .= "<h2>" . JText::_('COM_JOBSFACTORY_CUSTOM_FIELDS') . "</h2>";
			$html .= "<ul style='list-style-type: none;'>";
			foreach($data->custom_fields as $k => $v)
			{
				$html .= "<li>". $v->name .": " . $v->value ."</li>";
			}
			$html .= "</ul>";

		}

		return $html;
	}
}