<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JobsHelperLinksCandidate{

    public static function getLinks($candidate)
    {
        static $links;
        if (!$candidate->id) return array();
        if (isset($links[$candidate->id])) return $links[$candidate->id];

        $links[$candidate->id]=array();
        $links[$candidate->id]['candidate_profile'] = JobsHelperRoute::getUserdetailsRoute($candidate->userid);

        return $links[$candidate->id];
    }

    public static function get($candidate,$params)
    {
        $linktype = $params;

        if (is_array($params)) $linktype=$params[0];

        if (!$linktype)
        {
            //JError::raiseNotice(113,JText::sprintf("Link Type '%s' is unknown!",$linktype));
            if(JDEBUG)
                JFactory::getApplication()->enqueueMessage("Link Type $linktype is unknown!", 'notice');

            return "";
            
        }
        $links = self::getLinks($candidate);

        if (isset($links[$linktype]))
            return $links[$linktype];
        else
        {
            //JError::raiseNotice(113,JText::sprintf("Link Type '%s' is unknown!",$linktype));
            if(JDEBUG)
                JFactory::getApplication()->enqueueMessage("Link Type $linktype is unknown!", 'notice');

            return "";
            
        }
    }
    
}
