<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

//class JobsHelper
class JobsfactoryHelper
{
    static function FileName2ClassName($prefix,$filename,$suffix)
    {
        jimport('joomla.filesystem.file');
        $class_name= $prefix.ucfirst(strtolower(preg_replace('/\s/','_', JFile::stripExt($filename)))).$suffix;
        return $class_name;   
    }

    static function LoadHelperClasses()
    {
        $helperfolder = JPATH_COMPONENT_SITE.DS.'helpers';
        $files = JFolder::files($helperfolder,'\.php$');

        if (count($files))
            foreach($files as $helperfile)
            {
                if ($helperfile==basename(__FILE__))
                    continue;
                $class_name = self::FileName2ClassName('JobsHelper',$helperfile,'');
                JLoader::register($class_name,$helperfolder.DS.$helperfile);
            }
    }

}
