<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JobsHelperLinksUser
{

    public static function getLinks($user)
    {
        static $links;
        if (!$user->id) return array();
        if (isset($links[$user->id])) return $links[$user->id];

        $links[$user->id] = array();
        $links[$user->id]['candidate_profile'] = JobsHelperRoute::getUserdetailsRoute($user->userid);
        $links[$user->id]['download_file'] = JobsHelperRoute::getDownloadFileUserRoute($user->userid, 'attach');
        $links[$user->id]['deletefile_file'] = JobsHelperRoute::getDeleteFileUserRoute($user->userid, 'attach');
        $links[$user->id]['add_to_watchlist'] = JobsHelperRoute::getAddToCompanyWatchlist($user->id);
        $links[$user->id]['del_from_watchlist'] = JobsHelperRoute::getDelToCompanyWatchlist($user->id);

        return $links[$user->id];
    }

    public static function get($user, $params)
    {
        $linktype = $params;
        if (is_array($params)) $linktype = $params[0];
        if (!$linktype) {
            //JError::raiseNotice(113, JText::sprintf("Link Type '%s' is unknown!", $linktype));
            if (JDEBUG)
                JFactory::getApplication()->enqueueMessage("Link Type $linktype is unknown!", 'notice');

            return "";

        }
        $links = self::getLinks($user);
        if (isset($links[$linktype]))
            return $links[$linktype];
        else
        {
            //JError::raiseNotice(113, JText::sprintf("Link Type '%s' is unknown!", $linktype));
            if (JDEBUG)
                JFactory::getApplication()->enqueueMessage("Link Type $linktype is unknown!", 'notice');

            return "";

        }
    }

}
