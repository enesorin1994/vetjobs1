<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JobsHelperLinksJob{

    public static function getLinks($job)
    {
        static $links;
        if (!$job->id) return array();
        if (isset($links[$job->id])) return $links[$job->id];

        $links[$job->id]=array();
        $links[$job->id]['otherjobs'] = JobsHelperRoute::getJobListRoute(array("userid"=>$job->userid ));
        $links[$job->id]['jobs_listing'] = JobsHelperRoute::getJobListRoute();
        $links[$job->id]['jobs'] = JobsHelperRoute::getJobDetailRoute($job->id);
		$links[$job->id]['edit'] =  JobsHelperRoute::getJobEditRoute( $job->id);
	  	$links[$job->id]['cancel'] = JobsHelperRoute::getJobCancelRoute($job->id);
	  	$links[$job->id]['filter_cat'] = JobsHelperRoute::getJobListRoute(array("cat"=>$job->cat ));
		$links[$job->id]['republish'] = JobsHelperRoute::getJobRepublishRoute($job->id);
		$links[$job->id]['add_to_watchlist'] = JobsHelperRoute::getAddToWatchlistRoute($job->id);
		$links[$job->id]['del_from_watchlist'] = JobsHelperRoute::getDelFromWatchlistRoute($job->id);
        $links[$job->id]['employer_profile'] = JobsHelperRoute::getUserdetailsRoute($job->userid);
        $links[$job->id]['report'] = JobsHelperRoute::getReportJobRoute($job->id);
        $links[$job->id]['new_job'] = JobsHelperRoute::getNewJobRoute();
		$links[$job->id]['bulkimport'] = JobsHelperRoute::getBulkImportRoute();
		$links[$job->id]['terms'] = JobsHelperRoute::getTermsAndConditionsRoute() ;
		$links[$job->id]['download_file'] = JobsHelperRoute::getDownloadFileRoute($job->id ,'attach');
		$links[$job->id]['deletefile_file'] = JobsHelperRoute::getDeleteFileRoute($job->id ,'attach');
		$links[$job->id]['tags'] = '';
		$tags=$job->get('tags');

        $uri = JUri::getInstance();
        $root = $uri->root();
        $pathonly = $uri->base(true);

        $replace = '#'.$pathonly.'/#';
        $basepath = preg_replace($replace,'',$root);
        $links[$job->id]['tweeter'] = urlencode($basepath.$links[$job->id]['details']);

		if (!is_array($tags)) $tags=explode(',',$tags);
		for($i=0;$i<count($tags);$i++)
		    if ($tags[$i]){
    		    $href=JobsHelperRoute::getTagsRoute($tags[$i]);
    		    $links[$job->id]['tags'] .= "<span class='job_tag'><a href='$href'>".$tags[$i]."</a>".(($i+1<count($tags))?",":"")."</span>";
		    }
        return $links[$job->id];
    }

    public static function get($job,$params)
    {
        $linktype = $params;

        if (is_array($params)) $linktype=$params[0];

        if (!$linktype)
        {
            //JError::raiseNotice(113,JText::sprintf("Link Type '%s' is unknown!",$linktype));
            if(JDEBUG)
                JFactory::getApplication()->enqueueMessage("Link Type $linktype is unknown!", 'notice');

            return "";
            
        }
        $links = self::getLinks($job);

        if (isset($links[$linktype]))
            return $links[$linktype];
        else
        {
            //JError::raiseNotice(113,JText::sprintf("Link Type '%s' is unknown!",$linktype));
            if(JDEBUG)
                JFactory::getApplication()->enqueueMessage("Link Type $linktype is unknown!", 'notice');

            return "";
            
        }
    }
    
}
