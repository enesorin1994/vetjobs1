<?php
    /**------------------------------------------------------------------------
    com_jobsfactory - Jobs Factory 1.6.5
    ------------------------------------------------------------------------
     * @author thePHPfactory
     * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
     * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     * Websites: http://www.thePHPfactory.com
     * Technical Support: Forum - http://www.thePHPfactory.com/forum/
     * @package: Jobs
    -------------------------------------------------------------------------*/

    defined('_JEXEC') or die('Restricted access');

    class JobsHelperJob
    {
        static function getOrderItemsForJob($jobid, $itemname = null)
        {
            $db = JFactory::getDbo();
            $db->setQuery("select * from `#__jobsfactory_payment_orderitems` oi
                        left join `#__jobsfactory_payment_orders` o on oi.orderid=o.id
                        where iteminfo='{$jobid}'
                        " . ($itemname ? " and itemname='$itemname'" : ""));
            return $db->loadObjectList();
        }

        static function getCSVData($fieldname, $data, $header)
        {
            $fieldname = strtolower($fieldname);
            $header = array_change_key_case($header, CASE_LOWER);

            if (isset($header[$fieldname]) && isset($data[$header[$fieldname]]))
                return trim($data[$header[$fieldname]]);
            else
            {}
            exit;  //return "";
        }

        static function ImportFromCSV()
        {
            jimport('joomla.filesystem.path');

            $database = JFactory::getDBO();
            $my = JFactory::getUser();
            $cfg = JTheFactoryHelper::getConfig();
            $app = JFactory::getApplication();
            $config = JFactory::getConfig();
            @set_time_limit(0);

            jimport('joomla.filesystem.file');
            require_once(JPATH_COMPONENT_SITE . DS . 'thefactory' . DS . 'front.images.php');

            $job = JTable::getInstance('jobs', 'Table');
            $tags = JTable::getInstance('tags', 'Table');

            $errors = array();

            if (!isset($_FILES['csv']['tmp_name']) || !is_uploaded_file($_FILES['csv']['tmp_name'])) {
                $errors[] = JText::_("COM_JOBS_UPLOAD_A_CSV_FILE");
                return $errors;
            }

            $csv_fname = $_FILES['csv']['name'];
            $ext = JFile::getExt($csv_fname);
            if (!in_array(strtolower(JFile::getExt($csv_fname)), array("csv", "txt"))) {
                $errors[] = JText::_("COM_JOBS_EXTENSION_NOT_ALLOWED");
                return $errors;
            }

            $csv_fname = $config->get('tmp_path') . DS . $csv_fname;

            if (!JFile::upload($_FILES['csv']['tmp_name'], $csv_fname)) {
                $errors[] = JText::_("COM_JOBS_ERROR_UPLOADING_CSV_FILE");
                return $errors;
            }

            $handle = fopen($csv_fname, "r");

            //Read CSV Header
            $header = fgetcsv($handle, 30000, "\t");

            if (is_array($header)) $header = array_flip($header);

            while (($data = fgetcsv($handle, 30000, "\t")) !== FALSE) {

                $job->id = null;
                $job->title = JFilterOutput::cleanText(self::getCSVData('title', $data, $header));
                $job->userid = self::getCSVData("userid", $data, $header);
                if (!$job->userid) $job->userid = $my->id;
                $job->shortdescription = JFilterOutput::cleanText(self::getCSVData("shortdescription", $data, $header));
                $job->description = preg_replace('/<script[^>]*?>.*?<\/script>/si', '', self::getCSVData("description", $data, $header));
                $job->end_date = self::getCSVData("end_date", $data, $header);
                $job->start_date = self::getCSVData("start_date", $data, $header);
                $job->published = 1;
                $job->job_type = self::getCSVData("job_type", $data, $header);
                $job->languages_request = self::getCSVData("languages_request", $data, $header);
                $job->experience_request = self::getCSVData("experience_request", $data, $header);
                $job->studies_request = self::getCSVData("studies_request", $data, $header);
                $job->benefits = JFilterOutput::cleanText(self::getCSVData("benefits", $data, $header));
                //$job->job_cityname = JFilterOutput::cleanText(self::getCSVData('job_cityname', $data, $header));
                $job->job_cityid = self::getCSVData("job_cityid", $data, $header);
                $job->job_location_state = self::getCSVData("job_location_state", $data, $header);
                $job->job_country = self::getCSVData("job_country", $data, $header);
                $job->about_visibility = self::getCSVData("about_visibility", $data, $header);
                $job->employer_visibility = self::getCSVData("employer_visibility", $data, $header);
                $job->jobuniqueID = uniqid(); //microtime().
                $job->jobs_available = self::getCSVData("jobs_available", $data, $header);
                $job->cat = self::getCSVData("category", $data, $header);
                $job->show_candidate_nr = self::getCSVData("show_candidate_nr", $data, $header);
                $job->featured = self::getCSVData("featured", $data, $header);

                $job->store(true);

                $tag = self::getCSVData("tags", $data, $header);
                if ($tag)
                    $tags->setTags($job->id, $tag);

                $job->store();
            }
            fclose($handle);

            if (file_exists($csv_fname))
                JFile::delete($csv_fname);

            return $errors;
        }


        static function ImportFromCSVLoc()
        {
            jimport('joomla.filesystem.path');

            $database   = JFactory::getDBO();
            $my         = JFactory::getUser();
            $cfg        = JTheFactoryHelper::getConfig();
            $app        = JFactory::getApplication();
            $config     = JFactory::getConfig();
            @set_time_limit(0);

            jimport('joomla.filesystem.path');
            jimport('joomla.filesystem.file');
            require_once(JPATH_COMPONENT_SITE . DS . 'thefactory' . DS . 'front.images.php');

            $city           = JTable::getInstance('city', 'Table');
            $location       = JTable::getInstance('location', 'Table');
            $countrytable   = JTable::getInstance('country', 'Table');

            $errors = array();

            if ( (!isset($_FILES['locations']['tmp_name']) || !is_uploaded_file($_FILES['locations']['tmp_name'])) &&
                (!isset($_FILES['cities']['tmp_name']) || !is_uploaded_file($_FILES['cities']['tmp_name'])) )
            {
                $errors[] = JText::_("COM_JOBS_UPLOAD_A_CSV_FILE");
                return $errors;
            }

            // import locations/area/county
            if (isset($_FILES['locations']['tmp_name']) && is_uploaded_file($_FILES['locations']['tmp_name']) ) {

                $csv_fname = $_FILES['locations']['name'];
                $ext = JFile::getExt($csv_fname);

                if (!in_array(strtolower(JFile::getExt($csv_fname)), array("csv", "txt"))) {
                    $errors[] = JText::_("COM_JOBS_EXTENSION_NOT_ALLOWED");
                    return $errors;
                }

                $csv_fname = $config->get('tmp_path') . DS . $csv_fname;

                if (!JFile::upload($_FILES['locations']['tmp_name'], $csv_fname)) {
                    $errors[] = JText::_("COM_JOBS_ERROR_UPLOADING_CSV_FILE");
                    return $errors;
                }

                $handle = fopen($csv_fname, "r");

                //Read CSV Header
                $header = fgetcsv($handle, 30000, "\t");

                if (is_array($header)) $header = array_flip($header);

                while (($data = fgetcsv($handle, 30000, "\t")) !== FALSE) {

                    $location->id = null;
                    $location->locname = JFilterOutput::cleanText(self::getCSVData('locname', $data, $header));

                    $country = self::getCSVData('country', $data, $header);
                    $countryid = $countrytable->getCountryId($country);
                    $location->country = JFilterOutput::cleanText($countryid);

                    $location->status = self::getCSVData("status", $data, $header);
                    $lang = JFactory::getLanguage();
                    $location->hash = $lang->transliterate($location->locname);
                    $location->hash = md5(strtolower(JFilterOutput::stringURLSafe($location->hash)));

                    $location->store(true);

                }
                fclose($handle);

                if (file_exists($csv_fname))
                    JFile::delete($csv_fname);

            }
            // import cities
            if (isset($_FILES['cities']['tmp_name']) && is_uploaded_file($_FILES['cities']['tmp_name']) ) {

                $csv_fname2 = $_FILES['cities']['name'];
                $ext = JFile::getExt($csv_fname2);

                if (!in_array(strtolower(JFile::getExt($csv_fname2)), array("csv", "txt"))) {
                    $errors[] = JText::_("COM_JOBS_EXTENSION_NOT_ALLOWED");
                    return $errors;
                }

                $csv_fname2 = $config->get('tmp_path') . DS . $csv_fname2;

                if (!JFile::upload($_FILES['cities']['tmp_name'], $csv_fname2)) {
                    $errors[] = JText::_("COM_JOBS_ERROR_UPLOADING_CSV_FILE");
                    return $errors;
                }

                $handle = fopen($csv_fname2, "r");

                //Read CSV Header
                $header = fgetcsv($handle, 30000, "\t");

                if (is_array($header)) $header = array_flip($header);

                while (($data = fgetcsv($handle, 30000, "\t")) !== FALSE) {
                    $city->id = null;
                    $city->cityname = JFilterOutput::cleanText(self::getCSVData('cityname', $data, $header));
                    $loc = self::getCSVData('location', $data, $header);
                    $locationId = $location->getLocationId($loc);
                    $city->location = JFilterOutput::cleanText($locationId);
                    //$city->location = self::getCSVData("location", $data, $header);
                    $city->status = self::getCSVData("status", $data, $header);

                    $lang = JFactory::getLanguage();
                    $city->hash = $lang->transliterate($city->cityname);
                    $city->hash = md5(strtolower(JFilterOutput::stringURLSafe($city->hash)));

                    $city->store(true);

                }
                fclose($handle);

                if (file_exists($csv_fname2))
                    JFile::delete($csv_fname2);

            }
            return $errors;

        }

    }
