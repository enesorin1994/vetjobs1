<?php
    /**------------------------------------------------------------------------
    com_jobsfactory - Jobs Factory 1.6.5
    ------------------------------------------------------------------------
     * @author thePHPfactory
     * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
     * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     * Websites: http://www.thePHPfactory.com
     * Technical Support: Forum - http://www.thePHPfactory.com/forum/
     * @package: Jobs
    -------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');
class JobsHelperLocation
{

        static function getLocationName($locationid) {
            $db = JFactory::getDbo();
            $query = "SELECT loc.locname "
                . " FROM #__jobsfactory_locations loc "
                . " WHERE loc.id = '{$locationid}' ";

            $db->setQuery($query);
            return $db->loadResult();
        }

        static function getCityName($cityid) {

            $cfg = JTheFactoryHelper::getConfig();
            $db = JFactory::getDbo();

            if ($cfg->enable_location_management) {
                $query = "SELECT cy.cityname "
                    . " FROM #__jobsfactory_cities cy "
                    . " WHERE cy.id = '{$cityid}' ";

                $db->setQuery($query);
                return $db->loadResult();
            } else {
                return '';
            }
        }

        static function getCityJobs($cityid) {
            $db = JFactory::getDbo();
            $query = "SELECT COUNT(DISTINCT j.id)"
                . " FROM #__jobsfactory_jobs j "
                . " WHERE j.job_cityid = '{$cityid}' ";

            $db->setQuery($query);
            return $db->loadResult();
        }

        function getCSVData($fieldname, $data, $header)
        {
            $fieldname = strtolower($fieldname);
            $header = array_change_key_case($header, CASE_LOWER);
            if (isset($header[$fieldname]) && isset($data[$header[$fieldname]]))
                return trim($data[$header[$fieldname]]);
            else
                return "";
        }

        function moveTempImage($imagename, $new_imagename)
        {
            $errors = array();
            if (!file_exists($imagename)) {
                $errors[] = JText::_("COM_JOBS_IMAGE_MISSING_FROM_ARCHIVE") . ": " . JFile::getName($imagename);
                return $errors;
            }
            $ext = JFile::getExt($imagename);
            if (!TableLocations::isAllowedImage($ext)) {
                $errors[] = JText::_("COM_JOBS_USUPPORTED_IMAGE_EXTENSION") . ": " . JFile::getName($imagename);
                return $errors;
            }
            $cfg = JTheFactoryHelper::getConfig();
            if (filesize($imagename) > $cfg->max_picture_size * 1024) {
                $errors[] = JText::_("COM_JOBS_IMAGE_IS_TOO_LARGE") . ": " . JFile::getName($imagename);
                return $errors;
            }
            $imgTrans = new JTheFactoryImages();
            if (file_exists($new_imagename))
                JFile::delete($new_imagename);
            if (JFile::move($imagename, $new_imagename)) {
                $imgTrans->resize_image($new_imagename, $cfg->thumb_width, $cfg->thumb_height, 'resize');
                $imgTrans->resize_image($new_imagename, $cfg->medium_width, $cfg->medium_height, 'middle');
            } else {
                $errors[] = JText::_("COM_JOBS_CAN_NOT_MOVE") . ": " . JFile::getName($imagename);
            }
            return $errors;
        }

        function ImportFromCSV()
        {
            jimport('joomla.filesystem.path');

            $database   = JFactory::getDBO();
            $my         = JFactory::getUser();
            $cfg        = JTheFactoryHelper::getConfig();
            $app        = JFactory::getApplication();
            $config     = JFactory::getConfig();
            @set_time_limit(0);

            $appAdm = JTheFactoryApplication::getInstance();

            jimport('joomla.filesystem.path');
            jimport('joomla.filesystem.file');
            require_once(JPATH_COMPONENT_SITE . DS . 'thefactory' . DS . 'front.images.php');
            require_once($appAdm->app_path_admin .'location' .DS.'tables'. DS . 'location.php');
            require_once($appAdm->app_path_admin .'city' .DS.'tables'. DS . 'city.php');

            $location = JTable::getInstance('Location', 'JTheFactoryTable');
            $citytable = JTable::getInstance('City', 'JTheFactoryTable');

            $errors = array();

            if (!isset($_FILES['csv']['tmp_name']) || !is_uploaded_file($_FILES['csv']['tmp_name'])) {
                $errors[] = JText::_("COM_JOBS_UPLOAD_A_CSV_FILE");
                return $errors;
            }

            $csv_fname = $_FILES['csv']['name'];
            $ext = JFile::getExt($csv_fname);
            if (!in_array(strtolower(JFile::getExt($csv_fname)), array("csv", "txt"))) {
                $errors[] = JText::_("COM_JOBS_EXTENSION_NOT_ALLOWED");
                return $errors;
            }

            $csv_fname = $config->get('tmp_path') . DS . $csv_fname;

            if (!JFile::upload($_FILES['csv']['tmp_name'], $csv_fname)) {
                $errors[] = JText::_("COM_JOBS_ERROR_UPLOADING_CSV_FILE");
                return $errors;
            }

            if (isset($_FILES['arch']['tmp_name']) && $_FILES['arch']['tmp_name']) {
                $tmp_imgpath = JPath::clean($config->get('tmp_path') . DS . md5(microtime() . rand(0, 100)));
                JFolder::create($tmp_imgpath);
                jimport('joomla.filesystem.archive');
                $zip = JArchive::getAdapter('zip');

                if (($res = $zip->extract($_FILES['arch']['tmp_name'], $tmp_imgpath)) !== TRUE) {
                    $errors[] = JText::_("COM_JOBS_ERROR_EXTRACTING_ZIP") . ": " . $res;
                }
            }

            $handle = fopen($csv_fname, "r");

            //Read CSV Header
            $header = fgetcsv($handle, 30000, "\t");
            if (is_array($header)) $header = array_flip($header);

            while (($data = fgetcsv($handle, 30000, "\t")) !== FALSE) {

                $location->id = null;
                $location->locname       = JFilterOutput::cleanText(self::getCSVData("locname", $data, $header));
                $location->country       = JFilterOutput::cleanText(self::getCSVData("country", $data, $header));
                $location->status = 1;
                $location->store(true);

                $nrcities = 0;
                $cities = self::getCSVData("cities", $data, $header);
                $arr_cities = explode(",", $cities);
                foreach ($arr_cities as $city) {
                    $citytable->id = null;
                    $citytable->cityname = $city;
                    $citytable->location = $location->id;
                    $citytable->status = 1;
                    $citytable->store();
                }

            }
            fclose($handle);

            if (file_exists($csv_fname))
                JFile::delete($csv_fname);

            return $errors;
        }

}


?>
