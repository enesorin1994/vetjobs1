<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');
class JobsHelperDateTime
{

    /**
     * @static
     *
     * @param $isodate
     *
     * @return string
     */
    public static function isoDateToUTC($isodate)
    {
        //$config     = JFactory::getConfig();
        //$DateTime   = JFactory::getDate($isodate , $config->get('config.offset'));
        $DateTime = JFactory::getDate($isodate, self::getTimeZone());
        return $DateTime->toSql();
        //return $DateTime->toSql(true);
    }

    /**
     * Returns the userTime zone if the user has set one,
     * or the global config one
     *
     * @static
     * @return mixed
     */
    public static function getTimeZone()
    {
        $config = JFactory::getConfig();
        $userTz = JFactory::getUser()->getParam('timezone');
        $timeZone = $config->get('offset');

        if ($userTz) {
            $timeZone = $userTz;
        }
        return $timeZone;
    }

    public static function getNow(){

        $config = JFactory::getConfig();
        $offset = $config->get('offset');
        $timestamp = strtotime(gmdate("M d Y H:i:s", time()));

        return $timestamp + 3600*$offset;
    }

    public static function apendDays($time, $nrOfdays) {

       return $time + 86400*$nrOfdays;

    }

    public static function substractYears($date, $nrOfyears) {

        $newdate = strtotime ( "-$nrOfyears year" , strtotime ( $date ) ) ;
        $newdate = date ( 'Y-m-d' , $newdate );

        return $newdate;
    }

    /**
     * @static
     *
     * @param $isodate
     *
     * @return int
     */
    public static function dateDiff($isodate)
    {
        $config = JFactory::getConfig();
        $d1 = JFactory::getDate($isodate);
        $d2 = JFactory::getDate('now' , $config->get('config.offset'));
        $diff = $d2->toUnix()-$d1->toUnix();

        return $diff;
    }

    /**
     * @static
     *
     * @param $isodate
     *
     * @return string
     */
    public static function dateToCountdown($isodate)
    {
        $diff =-self::dateDiff($isodate);

    	if ($diff > 0){
    		$s = sprintf("%02d",$diff%60); $diff=intval($diff/60);
    		$m = sprintf("%02d",$diff%60); $diff=intval($diff/60);
    		$h = sprintf("%02d",$diff%24); $d=intval($diff/24);
    		if ($d > 0)
                return "$d ".JText::_("COM_JOBS_DAYS").", $h:$m:$s";
    		else 
                return "$h:$m:$s";
    	} else
            return JText::_("COM_JOBS_EXPIRED");
        
        
    }

    public static function process_countdown( &$job ){

      	$time = JobsHelperDateTime::getNow();
        $cfg = JTheFactoryHelper::getConfig();

   		if ( $cfg->enable_countdown ){

       		$config = JFactory::getConfig();
   			$offset = $config->get("offset");

   			$expiredate = JobsHelperDateTime::getTimeStamp($job->end_date);
       		$wtf1 = date($cfg->date_format." ".$cfg->date_time_format,$time);
       		$wtf2 = date($cfg->date_format." ".$cfg->date_time_format, $expiredate );

           	$diff = $expiredate - $time;

           	if ( $diff > 0 ) {
           		$s	= sprintf("%02d",$diff%60);
           		$diff	= intval($diff/60);

           		$m	= sprintf("%02d",$diff%60);
           		$diff	= intval($diff/60);

           		$h	= sprintf("%02d",$diff%24);

           		$d		= intval($diff/24);

           		if ($d > 0)
                    $job->countdown	= "$d ".JText::_("COM_JOBS_DAYS").", $h:$m:$s";
           		else
                    $job->countdown	= "$h:$m:$s";

           	} else {
                $job->countdown	= JText::_("COM_JOBS_EXPIRED");
           	}
   		}

           if ( $job->close_offer == '1' && ($job->closed_date > $job->end_date) && $job->published == '1' && $job->close_by_admin == '0') {
               $job->archived=true;
           } else {
               $job->archived=false;
           }

           if ( (JobsHelperDateTime::getTimeStamp($job->end_date) <= $time) && $job->close_offer == 0 && $job->published == 1 && $job->close_by_admin == 0 ) {
               $job->expired=true;
           }
           else {
               $job->expired=false;
           }

        $job->startdate_text = date($cfg->date_format, strtotime($job->start_date) );

   	    if ($job->end_date && $job->end_date!='0000-00-00 00:00:00' ) {
   			if ($cfg->enable_hour) {
   				$dateformat=$cfg->date_format." H:i";
   			} else
   				$dateformat=$cfg->date_format;

            $job->enddate_text=date($dateformat, strtotime($job->end_date) );
   		}

   	}

    public static function getTimeStamp($date){

       $result = null;

       if (preg_match('~(\\d{4})-(\\d{2})-(\\d{2})[T\s](\\d{2}):(\\d{2}):(\\d{2})(.*)~', $date, $matches))
       {
           $result = gmmktime(
               $matches[4], $matches[5], $matches[6],
               $matches[2], $matches[3], $matches[1]
           );
       }

       return 	$result;
    }

    /**
     * @static
     *
     * @param $dateformat
     *
     * @return mixed
     */
    public static function dateFormatConversion($dateformat)
    {
        $strftime_format=$dateformat;
        $strftime_format=str_replace('%','%%',$strftime_format);
        $strftime_format=str_replace('Y','%Y',$strftime_format);
        $strftime_format=str_replace('y','%y',$strftime_format);
        $strftime_format=str_replace('d','%d',$strftime_format);
        $strftime_format=str_replace('D','%A',$strftime_format);
        $strftime_format=str_replace('m','%m',$strftime_format);
        $strftime_format=str_replace('F','%B',$strftime_format);

    //echo JHtml::_('calendar', JHtml::date($this->row->end_date, 'Y-m-d H:i:s'), 'end_date', 'end_date', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19'));
        $strftime_format=str_replace('H','%H',$strftime_format);
        $strftime_format=str_replace('i','%M',$strftime_format);
        $strftime_format=str_replace('s','%S',$strftime_format);

        return $strftime_format;
    }

    /**
     * @static
     *
     * @param $date
     *
     * @return string
     */
    public static function DateToIso($date)
    {
        $cfg = JTheFactoryHelper::getConfig();
        if (!$date)
            return $date; //empty date
        if ($cfg->date_format == 'Y-m-d') {
            return $date;
        }

        if ($cfg->date_format == 'm/d/Y') {
            if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[1] . "-" . $matches[2];
        }
        if ($cfg->date_format == 'd/m/Y') {
            if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }
        if ($cfg->date_format == 'd/m/Y') {
            if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }
        if ($cfg->date_format == 'd.m.Y') {
            if (preg_match("/([0-9]+)\.([0-9]+)\.([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }
        if ($cfg->date_format == 'D, F d Y') {
            $d=strtotime($date);
            return date("Y-m-d",$d);
        }
        return $date;
    }

}
