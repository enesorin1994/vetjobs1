<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JobsHelperTools
{

    /**
     * @param $mail_address
     *
     * @return string
     */
    public static function cloack_email($mail_address)
    {
        $cfg = JTheFactoryHelper::getConfig();

        if ($cfg->enable_antispam_bot == 1 && $cfg->choose_antispam_bot) {
            if ($cfg->choose_antispam_bot == "recaptcha") {
                require_once(JPATH_COMPONENT_SITE . DS . 'helpers' . DS . 'recaptcha' . DS . 'recaptchalib.php');
                $mail = recaptcha_mailhide_url("01WxCXdKklKdG2JpOlMY15jw==", "2198178B23BFFB00CBAEA6370CE7A0B2", $mail_address);
                return "<a href=\"$mail.\" onclick=\"window.open('$mail', '', 'toolbar=0,scrollbars=0,location=0,statusbar=0, menubar=0,resizable=0,width=500,height=300');	return false;\" title=\"Reveal this e-mail address\">" . JText::_("COM_JOBS_SHOW_EMAIL") . "</a>";
            } elseif ($cfg->choose_antispam_bot == "joomla") {

                // Discutable if use Content Mail Plugin or ... just JHTML_('email.cloak' it does the same .. just global configuration is in question
                //		global $mainframe;
                //		$plugin = JPluginHelper::getPlugin('content', 'emailcloak');
                //		$pluginParams = new JRegistry($plugin->params);
                //		require_once (JPATH_SITE . DS . 'plugins' . DS . 'content' . DS . 'emailcloak' . DS . 'emailcloak.php');

                return JHtml::_('email.cloak', $mail_address);
                //  	return $mail_address;
            } elseif ($cfg->choose_antispam_bot == "smarty") {
                $smarty = new JTheFactorySmarty();
                require_once(SMARTY_DIR . "plugins/function.mailto.php");
                return JobsSmarty::smarty_jobs_print_encoded(array("address" => $mail_address, "encode" => "hex"), $smarty);
            }
        } else
            return $mail_address;
    }


    /**
     * Finds The Menu Item Of the Component
     *  by the needles as params
     *
     * needle example: 'view' => 'category'
     *
     *
     * @param array $needle
     * @since 1.5.0
     */
    public static function getMenuItemId($needles)
    {

        if (!is_array($needles)) {
            if ($needles)
                $needles = array($needles);
            else
                $needles = array();
        }
        $needles['option'] = 'com_jobsfactory';

        $menus = JApplication::getMenu('site', array());
        $items = $menus->getItems('query', $needles);

        if (!count($items))
            return null; //no extension menu items

        $match = reset($items); //fallback first encountered Menuitem

        foreach ($items as $item) {
            if ($match->access != 0 && $item->access == 0) {
                $match = $item; //even better fallback is one that has public access
                continue;
            }

            $xssmatch1 = array_intersect_assoc($item->query, $needles);
            $xssmatch2 = array_intersect_assoc($match->query, $needles);

            if (count($xssmatch1) > count($xssmatch2)) { //better needlematch
                $match = $item; //even better fallback is one that has public access
                continue;
            }
        }

        return $match->id;
    }

    /**
     * Get the Parameter Task ItemID
     *  	if more than one menu items of same task, returns the first
     *
     * @param  $task_name
     * @return int
     */
    static function getMenuItemByTaskName($task_name = NULL, $option = 'com_jobsfactory'){

		if (!$task_name) {
            return 0;
        }

        if (is_array($task_name) && array_key_exists('task', $task_name)) {
            $task = $task_name['task'];
        }

        if (is_array($task_name) && array_key_exists('option', $task_name)) {
            $option = $task_name['option'];
        }

        $db    = JFactory::getDbo();
        $query = "SELECT `id` FROM `#__menu` WHERE `link` LIKE '%{$task}%' AND `link` LIKE '%option={$option}%' ORDER BY id DESC LIMIT 0 , 1 ";
        $db->setQuery($query);

        if ($db->loadResult()) {
            return $db->loadResult();
        }

        return 0;
    }

    /**
     * @return string
     */
    public static function getProfileMode()
    {
        $cfg = JTheFactoryHelper::getConfig();
        return ($cfg->profile_mode) ? $cfg->profile_mode : "component";
    }

    public static function getUserProfileObject()
    {
        $profile_mode = self::getProfileMode();

        return JTheFactoryUserProfile::getInstance($profile_mode);
    }

    public static function getCompanyProfileObject()
    {
        $profile_mode = self::getProfileMode();

        return JTheFactoryUserProfile::getInstance($profile_mode);
    }

    /**
     * @param null $id
     *
     * @return mixed
     */
    public static function redirectToProfile($id = null)
    {
        $userprofile = self::getUserProfileObject();
        return $userprofile->getProfileLink($id);
    }

    public static function isCompany($userid = null)
    {
        if (!$userid) {
            $user = JFactory::getUser();
            $userid = $user->id;
        }
        if (!$userid) return false;
        $cfg = JTheFactoryHelper::getConfig();
        //if (!$cfg->enable_acl) return true;
        $user_groups = JAccess::getGroupsByUser($userid,false);

        return count(array_intersect($user_groups, $cfg->company_groups)) > 0;
    }

    public static function isCandidate($userid = null)
    {
        if (!$userid) {
            $user = JFactory::getUser();
            $userid = $user->id;
        }
        if (!$userid) return false;

        $cfg = JTheFactoryHelper::getConfig();
        //if (!$cfg->enable_acl) return true;
        $user_groups = JAccess::getGroupsByUser($userid,false);

        return count(array_intersect($user_groups, $cfg->candidate_groups)) > 0;
    }

    public static function isVerified($userid = null)
    {
        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile($userid);
        return $userprofile->verified;
    }

    /**
     * @return JobsACL|object
     */
    public static function &getJobsACL()
    {
        static $acl = null;
        if (is_object($acl)) return $acl;
        require_once(JPATH_COMPONENT_SITE . DS . 'jobsfactory.acl.php');
        $acl = new JobsACL();

        return $acl;
    }

    public static function getItemsPerPage()
    {
        $cfg    = JTheFactoryHelper::getConfig();
        $config = JFactory::getConfig();
        if (intval($cfg->nr_items_per_page) > 0)
            return $cfg->nr_items_per_page;
        else
            return $config->get('config.list_limit');
    }

    public static function getCategoryModel()
    {
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'thefactory' . DS . 'category' . DS . 'models');
        $catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');

        return $catModel;
    }

    public static function getCategoryTable()
    {
        JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'thefactory' . DS . 'category' . DS . 'tables');
        $catTable = JTable::getInstance('Category', 'JTheFactoryTable');

        return $catTable;
    }

    public static function getLocationModel()
    {
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models');
        $locModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');

        return $locModel;
    }

    public static function getLocationTable()
    {
       JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'tables');
       $locTable = JTable::getInstance('Location', 'Table');

       return $locTable;
    }

    public static function getCityModel()
    {
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models');
        $cityModel = JModelLegacy::getInstance('City', 'JobsAdminModel');

        return $cityModel;
    }

    public static function getCityTable()
    {
       JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS .  'tables');
       $cityTable = JTable::getInstance('City', 'Table');

       return $cityTable;
    }

    public static function getCandidateLastApplied($userid)
    {
        $db=JFactory::getDbo();
        $db->setQuery("SELECT modified FROM `#__jobsfactory_candidates` a
                        where userid='{$userid}'
                        ORDER BY modified DESC LIMIT 0,1" );
        return $db->loadResult();
    }

    public static function getCandidateLastLoggedOn($userid)
    {
        $db=JFactory::getDbo();
        $db->setQuery("SELECT lastvisitDate FROM `#__users`
                        where id='{$userid}'
                        " );
        return $db->loadResult();

    }

    /**
     * Display payment manager icons
     *
     * @static
     *
     */
    public static function getPaymentManagerIcons()
    {
        echo "<div id = 'cpanel' class='payment_manager_icons'>";

        $link = 'index.php?option=com_adsfactory&amp;task=orders.listing';
        JTheFactoryAdminHelper::quickiconButton($link, ADMIN_COMPONENT_IMAGES_PATH. 'paymentitems.png', JText::_("COM_ADS_VIEW_ORDERS"));

        $link = 'index.php?option=com_adsfactory&amp;task=payments.listing';
        JTheFactoryAdminHelper::quickiconButton($link, ADMIN_COMPONENT_IMAGES_PATH . 'payments.png', JText::_("COM_ADS_VIEW_PAYMENTS"));

        $link = 'index.php?option=com_adsfactory&amp;task=balances.listing';
        JTheFactoryAdminHelper::quickiconButton($link, ADMIN_COMPONENT_IMAGES_PATH . 'payments.png', JText::_("COM_ADS_USER_PAYMENT_BALANCES"));


        echo "</div>
             <div style = 'height:100px'>&nbsp;</div>
             <div style = 'clear:both;'></div>
             ";


    } // End Method

    /**
     * Get active theme
     *
     * @return mixed
     */
    public static function getActiveTheme()
    {
        $cfg = JTheFactoryHelper::getConfig();
        return $cfg->theme;
    }


    public static function formatNumberToDisplay($price){
        $cfg = JTheFactoryHelper::getConfig();

        switch ($cfg->decimal_symbol) {
            case 'comma':
                $decimal_separator = ',';
                $thousands_separator = '.';
                break;
            case 'dot':
                $decimal_separator = '.';
                $thousands_separator = ',';
                break;
            default:
                $decimal_separator = ',';
                $thousands_separator = '';
                break;
        }

        return number_format(floatval($price), intval($cfg->decimals_number), $decimal_separator, $thousands_separator);
    }

    /**
    * Remove thousand separator and replace decimals separator with point sign
    * from a number
    *
    * @param $number
    *
    * @return mixed
    */
   public static function formatNumberToSql($number)
   {
       $cfg = JTheFactoryHelper::getConfig();

       switch ($cfg->decimal_symbol) {
           case 'comma':
               $decimal = ',';
               $thousands = '.';
               break;
           case 'dot':
               $decimal = '.';
               $thousands = ',';
               break;
       }

       $no_thousands   = str_replace($thousands, '', $number);
       $formatedNumber = str_replace($decimal, '.', $no_thousands);

       return floatVal($formatedNumber);

   }

}
