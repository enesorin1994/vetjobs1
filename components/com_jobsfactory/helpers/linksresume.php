<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JobsHelperLinksResume
{

    function getLinks($resume)
    {
        static $links;
        if (!$resume->id) return array();
        if (isset($links[$resume->id])) return $links[$resume->id];

        $links[$resume->id] = array();
        $links[$resume->id]['resumes'] = JobsHelperRoute::getResumeDetailRoute($resume->id);
        $links[$resume->id]['edit'] = JobsHelperRoute::getResumeEditRoute($resume->id);
        $links[$resume->id]['cancel'] = JobsHelperRoute::getResumeCancelRoute($resume->id);
        $links[$resume->id]['add_to_watchlist'] = JobsHelperRoute::getAddToWatchlistRoute($resume->id);
        $links[$resume->id]['del_from_watchlist'] = JobsHelperRoute::getDelFromWatchlistRoute($resume->id);
        $links[$resume->id]['employer_profile'] = JobsHelperRoute::getUserdetailsRoute($resume->userid);
        $links[$resume->id]['candidate_profile'] = JobsHelperRoute::getUserdetailsRoute($resume->userid);
        $links[$resume->id]['new_resume'] = JobsHelperRoute::getNewResumeRoute();
        $links[$resume->id]['bulkimport'] = JobsHelperRoute::getBulkImportRoute();
        $links[$resume->id]['terms'] = JobsHelperRoute::getTermsAndConditionsRoute();
        $links[$resume->id]['download_file'] = JobsHelperRoute::getDownloadFileRoute($resume->id, 'attach');
        $links[$resume->id]['deletefile_file'] = JobsHelperRoute::getDeleteFileRoute($resume->id, 'attach');
        $links[$resume->id]['tags'] = '';
        $tags = $resume->get('tags');

        if (!is_array($tags)) $tags = explode(',', $tags);
        for ($i = 0; $i < count($tags); $i++)
            if ($tags[$i]) {
                $href = JobsHelperRoute::getTagsRoute($tags[$i]);
                $links[$resume->id]['tags'] .= "<span class='job_tag'><a href='$href'>" . $tags[$i] . "</a>" . (($i + 1 < count($tags)) ? "," : "") . "</span>";
            }
        return $links[$resume->id];
    }

    function get($resume, $params)
    {
        $linktype = $params;
        if (is_array($params)) $linktype = $params[0];
        if (!$linktype) {
            //JError::raiseNotice(113, JText::sprintf("Link Type '%s' is unknown!", $linktype));
            if (JDEBUG)
                JFactory::getApplication()->enqueueMessage("Link Type $linktype is unknown!", 'notice');

            return "";

        }
        $links = self::getLinks($resume);
        if (isset($links[$linktype]))
            return $links[$linktype];
        else
        {
            //JError::raiseNotice(113, JText::sprintf("Link Type '%s' is unknown!", $linktype));
            if (JDEBUG)
                JFactory::getApplication()->enqueueMessage("Link Type $linktype is unknown!", 'notice');
            return "";

        }
    }

}
