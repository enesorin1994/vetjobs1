<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JobsHelperGallery
{

    static function getGalleryPlugin()
    {
        static $gallery;
        if (isset($gallery) && is_object($gallery))
            return $gallery;

        $cfg = JTheFactoryHelper::getConfig();
        $gallery_name = "gl_" . $cfg->gallery;
        require_once(JPATH_COMPONENT_SITE . DS . "gallery" . DS . "$gallery_name.php");

        $gallery = new $gallery_name(JOB_PICTURES,
            $cfg->medium_width,
            $cfg->medium_height,
            $cfg->thumb_width,
            $cfg->thumb_height
        );
        return $gallery;
    }

}

