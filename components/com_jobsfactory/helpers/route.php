<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.helper');

class JobsHelperRoute
{
	/**
	 * @param null $needles
	 *
	 * @return string
	 */
    static function getItemid($needles = null)
    {
        $Itemid = $app = JFactory::getApplication()->input->getInt('Itemid');

        if (!$Itemid) $Itemid = JobsHelperTools::getMenuItemByTaskName($needles);
        if ($Itemid) return "&Itemid=" . $Itemid;

        return "";
    }

	/**
	 * @param      $jobid
	 * @param null $anchor
	 * @param bool $xhtml
	 *
	 * @return string
	 */
    static function getJobDetailRoute($jobid, $anchor = null, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=viewapplications&id={$jobid}" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link, $xhtml) . $anchor;
    }

    static function getEducationDetailRoute($resumeid, $anchor = null, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=user&task=FormUserEducation" . self::getItemid(array('task' => 'FormUserEducation'));
        return JRoute::_($link, $xhtml) . $anchor;
    }

    static function getExperienceDetailRoute($resumeid, $anchor = null, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=user&task=FormUserExperience" . self::getItemid(array('task' => 'FormUserExperience'));
        return JRoute::_($link, $xhtml) . $anchor;
    }

    static function getUserResumeEditRoute($resumeid, $education_id, $anchor = null, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=editresume&id={$resumeid}" . self::getItemid(array('task' => 'formresume'));
        return JRoute::_($link, $xhtml) . $anchor;
    }

    static function getshowApplyJobRoute($jobid, $userid, $xhtml = true )
    {
        $link = "index.php?option=com_jobsfactory&controller=applications&task=showapplyjob&user_rated={$userid}&id={$jobid}" . self::getItemid(array('task' => 'UserProfile'));
        return JRoute::_($link, $xhtml);
    }

    static function getUserResumesRoute($userid = null)
    {
        if ($userid)
            $link = "index.php?option=com_jobsfactory&controller=resumes&task=userresumes&id={$userid}" . self::getItemid(array('task' => 'UserProfile'));
        else
            $link = "index.php?option=com_jobsfactory&controller=resumes&task=myresumes" . self::getItemid(array('task' => 'userdetails'));
        return JRoute::_($link);
    }

    static function getUserApplicationsRoute($userid = null, $xhtml = true)
    {
        if ($userid)
            $link = "index.php?option=com_jobsfactory&controller=applications&task=userapplications&id={$userid}" . self::getItemid(array('task' => 'UserProfile'));
        else
            $link = "index.php?option=com_jobsfactory&controller=applications&task=myapplications" . self::getItemid(array('task' => 'userdetails'));
        return JRoute::_($link, $xhtml);
    }

    static function getCategoryRoute($catid = null, $filter_letter = 'all', $task = 'categories', $catslug = null, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task={$task}" . (($catid) ? ("&cat={$catid}") : ("")) . "&filter_letter={$filter_letter}" . $catslug . self::getItemid(array('task' => 'categories'));
        return JRoute::_($link, $xhtml);
    }

    static function getLocationRoute($locationid = null, $task = 'listlocations', $catslug = null, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task={$task}" . (($locationid) ? ("&location={$locationid}") : ("")) . $catslug . self::getItemid(array('task' => 'listlocation'));
        return JRoute::_($link, $xhtml);
    }

    static function getCityRoute($cityid = null, $task = 'listjobs', $catslug = null, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task={$task}" . (($cityid) ? ("&cityid={$cityid}") : ("")) . $catslug . self::getItemid(array('task' => 'listcities'));
        return JRoute::_($link, $xhtml);
    }

    static function getCompanyJobsRoute($companyid = null, $task = 'companylistcandidates', $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=companyjobs&company_id={$companyid}" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link, $xhtml);
    }

    static function getCompanyCandidatesRoute($jobid = null, $task = 'companylistcandidates', $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task={$task}" . "&id={$jobid}" . self::getItemid(array('task' => 'companylistcandidates'));
        return JRoute::_($link, $xhtml);
    }

    static function getJobListRoute($filters = null, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=listjobs";

        if (is_array($filters)) {
            foreach ($filters as $k => $v)
                $link .= "&$k=$v";
        } elseif ($filters) $link .= $filters;
        $link .= self::getItemid(array('task' => 'listjobs'));

        return JRoute::_($link, $xhtml);
    }

    static function getMyJobListRoute($filters = null, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=myjobs";

        if (is_array($filters)) {
            foreach ($filters as $k => $v)
                $link .= "&$k=$v";
        } elseif ($filters) $link .= $filters;
        $link .= self::getItemid(array('task' => 'myjobs'));

        return JRoute::_($link, $xhtml);
    }


    static function getProfileRoute($id, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=user&task=profile&id={$id}" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link,$xhtml);
    }

    static function setProfileRoute($id, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=user&task=setUserGroup&id={$id}" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link, $xhtml);
    }

    static function getTagsRoute($tag, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=tags&tag={$tag}";
        $link .= self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link, $xhtml);

    }

    static function getAddToCatWatchlist($catid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=watchlist&task=addwatchcat&cat={$catid}" . self::getItemid(array('task' => 'categories'));
        return JRoute::_($link, $xhtml);
    }

    static function getDelToCatWatchlist($catid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=watchlist&task=delwatchcat&cat={$catid}" . self::getItemid(array('task' => 'categories'));
        return JRoute::_($link, $xhtml);
    }

    static function getAddToLocationWatchlist($locationid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=watchlist&task=addwatchlocation&location={$locationid}" . self::getItemid(array('task' => 'locations'));
        return JRoute::_($link, $xhtml);
    }

    static function getDelToLocationWatchlist($locationid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=watchlist&task=delwatchlocation&location={$locationid}" . self::getItemid(array('task' => 'locations'));
        return JRoute::_($link, $xhtml);
    }

    static function getAddToCityWatchlist($cityid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=watchlist&task=addwatchcity&cityid={$cityid}" . self::getItemid(array('task' => 'listcities'));
        return JRoute::_($link, $xhtml);
    }

    static function getDelToCityWatchlist($cityid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=watchlist&task=delwatchcity&cityid={$cityid}" . self::getItemid(array('task' => 'listcities'));
        return JRoute::_($link, $xhtml);
    }

    static function getAddToCompanyWatchlist($companyid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=watchlist&task=addwatchcompany&companyid={$companyid}" . self::getItemid(array('task' => 'listcompanies'));
        return JRoute::_($link, $xhtml);
    }

    static function getDelToCompanyWatchlist($companyid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=watchlist&task=delwatchcompany&companyid={$companyid}" . self::getItemid(array('task' => 'listcompanies'));
        return JRoute::_($link, $xhtml);
    }

    static function getJobEditRoute($id, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=editjob&id=$id" . self::getItemid(array('task' => 'editjob', 'task' => 'newjob', 'task' => 'form'));
        return JRoute::_($link, $xhtml);
    }

    static function getJobCancelRoute($id, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=canceljob&id=$id" . self::getItemid(array('task' => 'editjob', 'task' => 'newjob', 'task' => 'form'));
        return JRoute::_($link, $xhtml);
    }

    static function getJobRepublishRoute($id, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=republish&id=$id" . self::getItemid(array('task' => 'editjob', 'task' => 'newjob', 'task' => 'form'));
        return JRoute::_($link, $xhtml);
    }

    static function getNewJobRoute($xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=form" . self::getItemid(array('task' => 'editjob', 'task' => 'newjob', 'task' => 'form'));
        return JRoute::_($link, $xhtml);
    }

    static function getNewJobInCategoryRoute($catid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=form&category=" . $catid . self::getItemid(array('task' => 'editjob', 'task' => 'newjob', 'task' => 'form'));
        return JRoute::_($link, $xhtml);
    }

    static function getUserGroupRoute($userid = null,$group_type = 0, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=displayprofile&id=$userid&group_type=$group_type" . self::getItemid(array('task' => 'UserProfile', 'task' => 'userdetails'));
        return JRoute::_($link, $xhtml);
    }

	static function getUserdetailsRoute($userid = null, $xhtml = true, $itemid = null)
	{
		if (!$userid)
		{
			if (!$itemid)
			{
				$itemid = self::getItemid(array('task' => 'userdetails'));
			}
			$link = "index.php?option=com_jobsfactory&controller=user&task=userdetails&Itemid=" . $itemid;
		}
		else
		{
			if (!$itemid)
			{
				$itemid = JobsHelperTools::getMenuItemByTaskName(array('task' => 'userdetails', 'task' => 'UserProfile'));
			}
			$link = "index.php?option=com_jobsfactory&controller=user&task=UserProfile&id=$userid" . "Itemid=" . $itemid;
		}
		return JRoute::_($link, $xhtml);
	}

    static function getTermsAndConditionsRoute($xhtml = true)
    {
        $link = 'index.php?option=com_jobsfactory&task=terms_and_conditions&format=raw';
        return JRoute::_($link, $xhtml);
    }

    static function getAddToWatchlistRoute($jobid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=watchlist&task=addwatchlist&id={$jobid}" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link, $xhtml);
    }

    static function getDelFromWatchlistRoute($jobid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=watchlist&task=delwatch&id={$jobid}" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link, $xhtml);
    }

    static function getReportJobRoute($jobid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=report_job&id={$jobid}" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link, $xhtml);
    }

    static function getBulkImportRoute($xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=bulkimport" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link, $xhtml);
    }

    static function getDownloadFileRoute($jobid, $filetype = 'attach', $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=attachements&task=downloadfile&id={$jobid}&file={$filetype}" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link, $xhtml);
    }

    static function getDownloadFileUserRoute($userid, $filetype = 'attach')
    {
        $link = "index.php?option=com_jobsfactory&format=raw&controller=attachements&task=downloadUserResume&id={$userid}&file={$filetype}" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link);
    }

    static function getDeleteFileRoute($jobid, $filetype = 'delete', $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&controller=attachements&task=deletefile&id={$jobid}&file={$filetype}" . self::getItemid(array('task' => 'listjobs'));
        return JRoute::_($link, $xhtml);
    }

    static function getDeleteFileUserRoute($userid, $filetype = 'delete')
    {
        $link = "index.php?option=com_jobsfactory&controller=user&task=deletefile&id={$userid}&file={$filetype}" . self::getItemid(array('task' => 'UserProfile'));
        return JRoute::_($link);
    }

    static function getSelectCategoryRoute($xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=selectcat" . self::getItemid(array('task' => 'form', 'task' => 'editjob', 'task' => 'new'));
        return JRoute::_($link, $xhtml);
    }

    static function getCheckoutRoute($orderid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=orderprocessor.checkout&orderid=$orderid" . self::getItemid(array('task' => 'form', 'task' => 'editjob', 'task' => 'new'));
        return JRoute::_($link, $xhtml);
    }

    static function getAddFundsRoute($xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=balance.addfunds" . self::getItemid(array('task' => 'userdetails', 'controller' => 'user'));
        return JRoute::_($link, $xhtml);
    }

    static function getPaymentsHistoryRoute($xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=payments.history" . self::getItemid(array('task' => 'userdetails', 'controller' => 'user'));
        return JRoute::_($link, $xhtml);
    }

    static function getFeaturedRoute($jobid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=setfeatured&id=" . $jobid . self::getItemid(array('task' => 'viewapplications', 'task' => 'details'));
        return JRoute::_($link, $xhtml);
    }

    static function getAcceptApplication($jobid, $xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=accept&jobid={$jobid}" . self::getItemid(array('task' => 'viewapplications', 'task' => 'details', 'task' => 'listjobs'));
        return JRoute::_($link, $xhtml);
    }

    /**
    * @param bool   $xhtml
    *
    * @return string
    */
    static function getSearchRoute($xhtml = true)
    {
       $link = "index.php?option=com_jobsfactory&task=show_search" . self::getItemid(array('task' => 'show_search'));
       return JRoute::_($link, $xhtml);
    }

    /**
     * @param bool   $xhtml
     *
     * @return string
     */
    static function getSearchonMapRoute($xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=googlemaps&controller=maps&search=1" . self::getItemid(array('task' => 'show_search'));
        return JRoute::_($link, $xhtml);
    }

    /**
    * @param bool   $xhtml
    *
    * @return string
    */
    static function getSearchonCompaniesRoute($xhtml = true)
    {

        $link  = "index.php?option=com_jobsfactory&task=searchcompanies&controller=user". self::getItemid(array('task' => 'show_search'));
        return JRoute::_($link, $xhtml);
    }

    /**
    * @param bool   $xhtml
    *
    * @return string
    */
    static function getSearchonUsersRoute($xhtml = true)
    {
        $link = "index.php?option=com_jobsfactory&task=searchusers&controller=user". self::getItemid(array('task' => 'show_search'));
        return JRoute::_($link, $xhtml);
    }

    static function getCompanyVoteRoute($company_id, $xhtml = true)
    {
        $link = 'index.php?option=com_jobsfactory&task=user.vote&id=' . $company_id;
        return JRoute::_($link, $xhtml);
    }

}



