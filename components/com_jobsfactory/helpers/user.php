<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

//class JobsHelperUser {
class JobsHelperUser{

    public static function getLatestPosition($userid) {
        $db = JFactory::getDbo();

        if ($userid) {
            $db->setQuery("SELECT `ue`.`position` FROM `#__jobsfactory_userexperience` ue
                    WHERE `ue`.`userid`='{$userid}'
                    ORDER BY `ue`.`end_date` DESC, `ue`.`id` DESC
                    LIMIT 0,1 "
                 ) ;
            return $db->loadResult();
        } else {
            return JText::_('COM_JOBS_NO_POSITION_FILLED');
        }
    }

    function getResumeId($userid) {
        $db = JFactory::getDbo();
        $db->setQuery("SELECT id FROM `#__jobsfactory_users`
                         WHERE userid='{$userid}'
                      ") ;
        return $db->loadResult();
    }

    public static function getResumeExperience($resumeExp){

        $database = JFactory::getDbo();
        $database->setQuery("SELECT levelname FROM #__jobsfactory_experiencelevel WHERE id='$resumeExp' ");
        $experiece_name = $database->LoadResult();

        return $experiece_name;
    }

    public static function getExperienceLevels(){

        $db = JFactory::getDbo();
        $db->setQuery("select id as `value`, `levelname` as text from `#__".APP_PREFIX."_experiencelevel` where `published`=1 order by `ordering`,`id`");
        $levelnames = $db->loadObjectList();

        return $levelnames;
    }

    public static function getStudyLevels(){

        $db = JFactory::getDbo();
        $db->setQuery("select id as `value`, `levelname` as text from `#__".APP_PREFIX."_studieslevel` where `published`=1 order by `ordering`,`id`");
        $levelnames = $db->loadObjectList();

        return $levelnames;
    }

    public static function getUserAge($userid) {

        $db = JFactory::getDbo();

        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile($userid);

        if ($userprofile->profile_mode == 'cb') {
            $cb_birth_date = $userprofile->getFilterField('birth_date');
			if ($cb_birth_date)
			{
            	$birth_date = $userprofile->$cb_birth_date;
			}
            //$birth_date = JobsHelperDateTime::DateToIso($userprofile->$cb_birth_date);
            //$birth_date = JobsHelperDateTime::isoDateToUTC($birth_date); //store all in UTC
        } else {
            $db->setQuery("SELECT birth_date FROM `#__jobsfactory_users`
                         WHERE userid='{$userid}'
                      ") ;
            $birth_date = $db->loadResult();
        }

        if (isset($birth_date) && $birth_date != '0000-00-00 00:00:00' && $birth_date != '0000-00-00') {
            $birth_date = new DateTime($birth_date);
            $today = new DateTime('00:00:00');
            //$today = new DateTime('2010-08-01 00:00:00'); // for testing purposes

            $diff = $today->diff($birth_date);
            //printf('%d years, %d month, %d days', $diff->y, $diff->m, $diff->d);
            return $diff->y . JText::_('COM_JOBS_YEARS');

        } else {
            return JText::_('COM_JOBS_NO_BIRTH_DATE');
        }

    }

	static function getCompanyAvailableJobs($companyid) {
        $db         = JFactory::getDbo();
        $config     = JFactory::getConfig();
        $datenow    = JFactory::getDate('now', $config->get('config.offset'));
        $cfg        = JTheFactoryHelper::getConfig();
        $condition  = '';

        if ($cfg->admin_approval) {
            $condition = "AND `j`.`approved` = 1";
        }

        $db->setQuery("SELECT COUNT(j.id) as no_available_jobs
                        FROM `#__jobsfactory_jobs` j
                            WHERE `j`.`userid`='{$companyid}'
                            AND `j`.`published` = 1
                            AND `j`.`close_offer` = 0
                            AND (`j`.`close_by_admin` <> 1)
                            $condition
                            AND `j`.`start_date` <= '".$datenow->toSql(false) ."'
                            AND `j`.`end_date` >= '".$datenow->toSql(false) ."'
                      ") ;

        return $db->loadResult();
    }

    static function getCompanyActivityFields($categIds) {
        $db = JFactory::getDbo();
        $ActivityFields = '';

        $db->setQuery("SELECT GROUP_CONCAT( DISTINCT c.catname SEPARATOR  ', ' ) AS 'activityfields'
                        FROM `#__jobsfactory_categories` c
                        WHERE `c`.`id` IN ({$categIds})
                      ") ;
        $ActivityFields = $db->loadResult();

        return $ActivityFields;
    }

    static function getCompanyRatings($companyid)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
                    ->select('ur.user_rating, ur.users_count')
                    ->from('#__jobsfactory_users_rating ur')
                    ->where('ur.userid = ' . $db->quote($companyid));
        $jobsRatedUser = $db->setQuery($query, 0, 1)
                   ->loadObject();

        return $jobsRatedUser;
    }

    static function getIsMyRating($userid,$companyid)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
                    ->select('r.rating_user')
                    ->from('#__jobsfactory_ratings r')
                    ->where('r.userid = ' . $db->quote($userid) .' AND r.rated_userid = ' . $db->quote($companyid));
        $jobsRatingUser = $db->setQuery($query, 0, 1)
                   ->loadResult();

        return $jobsRatingUser;
    }

	/*
	 * @param userid = the user id
	 * @param $group_type 2 for candidate, 1 for company
	 */
    public static function setUserGroup($userid = null, $group_type = null) {


		$cfg = new JobsfactoryConfig();
		$app = JFactory::getApplication();

		if($userid == null)
		{
			if($app->input->get('id'))
			{
				$userid = $app->input->get('id', null);
			}
			else
			{
				$userid = JFactory::getUser()->id;
			}
		}

		if(!$group_type)
		{
			$group_type = $app->input->get('usergrouptype', 0, 'POST', 'string'); //2 for candidate, 1 for company
		}

        //if company
        if ($group_type === '1') {
           foreach ($cfg->company_groups as $cgroup) {
             $setCompany = JUserHelper::addUserToGroup($userid, $cgroup);
             return $setCompany;
           }
        }
		if($group_type === '2'){
        //if candidate
           foreach ($cfg->candidate_groups as $ugroup) {
              $setCandidate = JUserHelper::addUserToGroup($userid, $ugroup);
              return $setCandidate;
           }
        }
    }

	public static function emailResume($userid, $resume)
	{


	}


}
