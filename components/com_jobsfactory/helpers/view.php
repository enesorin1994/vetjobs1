<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JobsHelperView
{
    public static function prepareCategoryTree(&$categories,$task='categories')
    {
        $categoryItemid=JobsHelperRoute::getItemid(array('task'=>'categories'));
        $jobsItemid=JobsHelperRoute::getItemid(array('task'=>'listjobs'));
        
        for($i=0;$i<count($categories);$i++)
        {
            $row=&$categories[$i];

    		$catslug = "";
    		if(isset($row->catslug)){
    			$separator = PHP_EOL;
    			$catslug = str_replace($separator,"/",str_replace("/","-",$row->catslug));
    			$catslug = "&amp;catslug=$catslug";
    		}
            if (isset($row->watchListed_flag))
            {
                if ( $row->watchListed_flag )
                    $row->link_watchlist = JobsHelperRoute::getDelToCatWatchlist($row->id);
                else
                    $row->link_watchlist = JobsHelperRoute::getAddToCatWatchlist($row->id);
            }
            
            $row->link = JobsHelperRoute::getCategoryRoute($row->id, 'all', $task,$catslug);
            $row->link_new_listing = JobsHelperRoute::getNewJobInCategoryRoute($row->id);
            $row->view = JobsHelperRoute::getJobListRoute(array('cat'=>$row->id));
            $row->descr = $row->description; //backwards compatibility. will be removed ASAP
    
            if (count($row->subcategories))
                self::prepareCategoryTree($row->subcategories); //recursion 
        }
        
        
    }

    static function buildLetterFilter($filter_cat)
    {

        $letters = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $filter_letter = JFactory::getApplication()->input->getString('filter_letter', 'all');

        $letters_filter = "<div id='box_letters_filter'>";
        foreach ($letters as $letter) {

            $active = strtolower($filter_letter) == strtolower($letter) ? 'active' : '';
            $letters_filter .= "<a href='" . JobsHelperRoute::getCategoryRoute($filter_cat, $letter) . "' class='" . $active . "'>" . $letter . "</a>";
        }
        // List all categories
        $letters_filter .= "<a href='" . JobsHelperRoute::getCategoryRoute($filter_cat) . "'>" . JText::_('COM_JOBS_ALL') . "</a>";
        $letters_filter .= "</div>";

        return $letters_filter;
    }

    static function prepareLocationTree(&$countries,&$locations,$watch_cities,$task='listlocations',&$countryId_arr)
    {
        $categoryItemid = JobsHelperRoute::getItemid(array('task'=>'listlocations'));
        $jobsItemid = JobsHelperRoute::getItemid(array('task'=>'listjobs'));

        $countryId_arr = array();
        foreach ($countries as $country) {
            $countryId_arr[$country->countrid] = array();
        }

        for($i=0;$i<count($locations);$i++)
        {
            $row = $locations[$i];

            $catslug = "";
            if(isset($row->catslug)){
                $separator = PHP_EOL;
                $catslug = str_replace($separator,"/",str_replace("/","-",$row->catslug));
                $catslug = "&amp;catslug=$catslug";
            }

            if (isset($row->watchListed_flag))
            {
                if ( $row->watchListed_flag )
                    $row->link_watchlist = JobsHelperRoute::getDelToLocationWatchlist($row->id);
                else
                    $row->link_watchlist = JobsHelperRoute::getAddToLocationWatchlist($row->id);
            }

            //location filter displays all jobs from all cities in a state
            $row->link = JobsHelperRoute::getLocationRoute($row->id,$task,$catslug);
            $row->view = JobsHelperRoute::getJobListRoute(array('location'=>$row->id));

            if ($row->cities != null) {
                $row->cityname_arr1 = explode(', ', $row->cities);
                $row->cityid_arr = explode(', ', $row->cityids);
            } else {
                $row->cityname_arr = null;
                $row->cityid_arr = null;
            }

            if ( $row->cities != null && $row->cityids != null) {
                for ($k=0; $k < count($row->cityid_arr); $k++) {

                  if ($row->cityname_arr1[$k] != null || $row->cityname_arr1[$k] != '') {
                    $catslug1 = "";
                    if (isset($row->cityname_arr1[$k])){
                        $separator = PHP_EOL;
                        $catslug1 = str_replace($separator,"/",str_replace("/","-",$row->cityname_arr1[$k]));
                        $catslug1 = "&amp;catslug=$catslug1";
                    }

                      //$row->cityname_arr[$row->cityid_arr[$k]] = $row->cityname_arr1[$k];
                      $row->cityname_arr[$row->cityid_arr[$k]] = $row->cityname_arr1[$k];
                      $row->jobs_no[$row->cityid_arr[$k]] = JobsHelperLocation::getCityJobs($row->cityid_arr[$k]);

                      if ( isset($watch_cities[$row->cityid_arr[$k]]) && $watch_cities[$row->cityid_arr[$k]]->watchListed_city_flag )
                          $row->link_watchlist_city[$row->cityid_arr[$k]] = JobsHelperRoute::getDelToCityWatchlist($row->cityid_arr[$k]);
                      else
                          $row->link_watchlist_city[$row->cityid_arr[$k]] = JobsHelperRoute::getAddToCityWatchlist($row->cityid_arr[$k]);

                      //city filter displays all jobs from selected city
                      $row->view_arr[$row->cityid_arr[$k]] = JobsHelperRoute::getJobListRoute(array('locationcity'=>$row->cityid_arr[$k]));
                  }
                }
            }

            // each country id[location id] stores location object
            $countryId_arr[$row->country][$row->id] = clone $row;
        }
    }

    static function prepareCityTree($cities,$task) {
        $categoryItemid = JobsHelperRoute::getItemid(array('task'=>'listcities'));
        $jobsItemid = JobsHelperRoute::getItemid(array('task'=>'listjobs'));

        for($i=0;$i<count($cities);$i++)
        {
            $row = $cities[$i];
            //city filter displays all jobs from selected city
            $row->link = JobsHelperRoute::getJobListRoute(array('city'=>$row->job_cityname ));
            $row->view = JobsHelperRoute::getJobListRoute(array('city'=>$row->job_cityname));
        }
    }


    static function loadJQuery()
    {
		$jdoc = JFactory::getDocument();
		$j_loaded = false;
		foreach ($jdoc->_scripts as $j => $jj)
			if (strstr($j, "jquery.js") !== false)
                return;
		$jdoc->addScript('components/com_jobsfactory/js/jquery/jquery.js');
		$jdoc->addScript('components/com_jobsfactory/js/jquery/jquery.noconflict.js');
    }

}
