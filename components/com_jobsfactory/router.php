<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

    if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

	// TODO remove completely if nothing breaks.
	//require_once( JPATH_ROOT.'/components/com_jobsfactory/defines.php' );
    require_once( JPATH_ROOT.'/components/com_jobsfactory/helpers/route.php' );

function jobsFactoryBuildRoute(&$query)
{

	$segments = array();
    $task=JArrayHelper::getValue($query,'task','');

    $controller=JArrayHelper::getValue($query,'controller','');
    if (!$controller && strpos($task,'.')!==FALSE) //task=controller.task?
    {
        $task		= explode('.',$task);
        $controller = $task[0];
        $task       = $task[1];
    }
    $func	= ($controller) ? $func = $controller.'Controller':'defaultController';
    
    if (is_callable(array('jobsRouteCreate',$func)))
        $segments=jobsRouteCreate::$func($task,$query);

	return $segments;
}


function jobsFactoryParseRoute($segments){

    $task = '';

	// get a menu item based on Itemid or currently active
	$app = JFactory::getApplication();	
	$menu = $app->getMenu();
	$menuItem = $menu->getActive();
	
	if (!isset($menuItem->query['task']))
	   $menuItem->query['task'] = "listjobs";
	
	if(isset($menuItem->query['task']))
		$task = $menuItem->query['task'];
	
	$vars = array();

	if( in_array($segments[0], array(
			'details',
			'userdetails',
			'userprofile',
            'profile',
			'listjobs',
			'myjobs',
			'report_job',
			'editjob',
			'tags',
            'categories',
			'show_search',
			'form',
			'searchusers',
			'searchcompanies',
            'jobsonmap', // Mapping for googlemaps
			'searchonmap', // Mapping for googlemaps
			'searchusersonmap' )));
    {
       $task = $segments[0];
	   $vars['task'] = $task;
    }

	//Set the active menu item
	switch($task){
		
		case "details":
			$vars['task'] = 'viewapplications';
			$vars['id']=$segments[1];
			break;
		case "report_job":
			$vars['id']=$segments[1];
			break;
        case "profile":
            $vars['task'] = 'profile';
            $vars['controller']='user';
            $vars['id']=$segments[1];
            break;
		case "userprofile":
			$vars['task'] = 'UserProfile';
            $vars['controller']='user';
			$vars['id']=$segments[1];
			break;
		
		case "listjobs":
			$vars['filter_archive'] = '';
            if (count($segments) > 2 && $segments[1] == 'category') {
                $vars['cat'] = $segments[2];
            } else if (count($segments) > 2 && $segments[1] == 'user') {
                $vars['userid'] = $segments[2];
            } else if (count($segments) > 2 && $segments[2] == 'archive') {
                $vars['filter_archive'] = 'archive';
            } else if (count($segments) > 2 && $segments[2] == 'draft') {
                $vars['filter_archive'] = 'draft';
            } else if (count($segments) > 2 && $segments[2] == 'active') {
                $vars['filter_archive'] = 'active';
            }
            break;
        case "myjobs":
            $vars['myjobs'] = $segments[0];
            $vars['filter_myjobs'] = '';
            if (isset($segments[2]) && $segments[2] != 'filter_myjobs') {
                $vars['filter_myjobs'] = $segments[2];
            }
            break;
        case "categories":
            if (count($segments) > 1 && $segments[1] != 'filter_letter') {
                $vars['cat'] = $segments[1];
            } else {
                $vars['filter_letter'] = $segments[2];
            }
            if (count($segments) > 4) {
                $vars['filter_letter'] = $segments[4];
            }
			break;
        case "tags":
            $vars['tag'] = $segments[1];
			break;
        case "form":
            if (count($segments) > 3 && $segments[1] == 'category') {
                $vars['category'] = $segments[2];
            }
            break;
        case "userdetails":
            $vars['task']       = 'userdetails';
            $vars['controller'] = 'user';
            break;
        case "searchusers":
            $vars['task']       = 'searchusers';
            $vars['controller'] = 'user';

            break;
        case "searchcompanies":
            $vars['task']       = 'searchcompanies';
            $vars['controller'] = 'user';
            break;
        case "jobsonmap":
            if (count($segments) && $segments[0] = 'jobsonmap') {
                $vars['search'] = '';
            }
            $vars['task']       = 'googlemaps';
            $vars['controller'] = 'maps';
            break;
        case "searchonmap":
            if (count($segments) && $segments[0] = 'searchonmap') {
                $vars['search'] = 1;
            }
            $vars['task']       = 'googlemaps';
            $vars['controller'] = 'maps';
            break;
        case "searchusersonmap":
            if (count($segments) && $segments[0] = 'searchusersonmap') {
                $vars['searchusers'] = 1;
            }
            $vars['task']       = 'googlemaps';
            $vars['controller'] = 'maps';
            break;
        case "googlemap_tool":
            $vars['controller'] = 'maps';
            $vars['tmpl']       = 'component';
            break;
	}

	return $vars;

}
class jobsRouteCreate
{
    static function getCategoryName($catid)
    {
        $cache=JFactory::getCache('com_jobsfactory.category','');
        $cache->setCaching(true); //Force caching
        $res=$cache->get('categoryname_'.$catid);
        if (!$res){
            $db   = JFactory::getDBO();
            $db->setQuery("select catname from #__jobsfactory_categories where id='".(int)$catid."'");
            $res=$db->loadResult();
            $cache->store($res,'categoryname_'.$catid);
        }
        return $res;
    }

    static function getJobTitle($jobid)
    {
        $cache=JFactory::getCache('com_jobsfactory.jobs','');
        $cache->setCaching(true); //Force caching
        $res=$cache->get('jobtitle_'.$jobid);
        if (!$res){
            $db   = JFactory::getDBO();
            $db->setQuery("SELECT `j`.`title` FROM `#__jobsfactory_jobs` AS `j` WHERE `id`='".(int)$jobid."'");
            $res=$db->loadResult();
            $cache->store($res,'jobtitle_'.$jobid);
        }
        return $res;
    }

    static function defaultController($task,&$query)
    {
        $segments=array();
    	switch ($task){
            case "form":
                $segments[] = "form";
                $catid      = JArrayHelper::getValue($query, 'category', NULL);

                if ($catid) {
                    $catname    = JFilterOutput::stringURLSafe(self::getCategoryName($catid));
                    $segments[] = 'category';
                    $segments[] = $catid;
                    $segments[] = $catname;
                    unset($query['category']);
                }
                unset($query["task"]);
                break;
    		case "report_job":
    			$segments[]="report_job";
    			$segments[]=$query['id'];
                $segments[] = JFilterOutput::stringURLSafe(self::getJobTitle($query['id']));

    			unset($query['id']);
    			unset($query["task"]);
    			break;
            case "userdetails":
                $segments[] = "userdetails";
                unset($query["task"]);
                unset($query["controller"]);
                break;
            case "profile":
                $segments[] = "profile";
                unset($query["task"]);
                unset($query["controller"]);
                break;

    		//job details
            case "viewapplications":
    			
    			$segments[]="details";
    			$segments[]=$query['id'];
                $segments[] = JFilterOutput::stringURLSafe(self::getJobTitle($query['id']));

    			unset($query['id']);
    			unset($query["task"]);
                break;
    		case "listjobs":
                $segments[]='listjobs';
                $catid=JArrayHelper::getValue($query,'cat',null);

    			if($catid){
                	//$database = JFactory::getDBO();
    				//$database->setQuery("select catname from #__jobsfactory_categories where id='{$catid}'");
			        //$catname=JFilterOutput::stringURLSafe($database->loadResult());
					$catname    = JFilterOutput::stringURLSafe(self::getCategoryName($catid));
                    $segments[]='category';
                    $segments[]=$catid;
                    $segments[]=$catname;
    				unset($query['cat']);
    			}

                $userid = JArrayHelper::getValue($query, 'userid', 0);
                if ($userid) {
                    $segments[] = 'user';
                    $segments[] = $userid;
                    $user       = JFactory::getUser($userid);
                    $segments[] = $user->username;
                    unset($query['userid']);
                }

                $filter_archive = JArrayHelper::getValue($query, 'filter_archive', null);
                if ($filter_archive) {
                    $segments[] = 'filter_archive';

                    switch ($filter_archive) {
                        case 'draft':
                            $segments[] = 'draft';
                            break;
                        case 'archive':
                            $segments[] = 'archive';
                            break;
                        case 'active':
                            $segments[] = 'active';
                            break;
                    }
                    unset($query['filter_archive']);
                }

    			unset($query["task"]);
    			unset($query["alias"]);
    			break;
            case "myjobs":
                $segments[]='myjobs';

                $filter_myjobs = JArrayHelper::getValue($query, 'filter_myjobs', null);

                if ($filter_myjobs) {
                    $segments[] = 'filter_myjobs';
                    $segments[] = $filter_myjobs;
                    unset($query["filter_myjobs"]);
                }

                unset($query["task"]);
                unset($query["alias"]);
                break;

    		case "categories":
                $segments[]='categories';
                $catid=JArrayHelper::getValue($query,'cat',null);
    			if($catid){
                	//$database = JFactory::getDBO();
    				//$database->setQuery("select catname from #__jobsfactory_categories where id='{$catid}'");
			        //$catname=JFilterOutput::stringURLSafe($database->loadResult());
					$catname    = JFilterOutput::stringURLSafe(self::getCategoryName($catid));                    
					$segments[]=$catid;
                    $segments[]=$catname;
    				unset($query['cat']);
    			}
                $filter_letter = JArrayHelper::getValue($query, 'filter_letter', null);
                if ($filter_letter) {
                    $segments[] = 'filter_letter';
                    $segments[] = $filter_letter;
                    unset($query["filter_letter"]);
                }

    			unset($query["task"]);
    			unset($query["alias"]);
    			break;
            case "tags":
                $segments[]='tags';
                $tag=JArrayHelper::getValue($query,'tag',null);
                $segments[]=$tag;
    			unset($query["task"]);
    			unset($query["tag"]);
                break;
            case 'show_search':
                $segments[] = 'show_search';
                $reload     = JArrayHelper::getValue($query, 'reload', NULL);
                if ($reload) {
                    $segments[] = 'reload';
                    $segments[] = 1;
                    unset($query["reload"]);
                }
                unset($query["task"]);
                break;
    	}
    	
        return $segments;
    }


    static function JJobsfactoryControllerUser($task,&$query)
    {
    		
        $segments=array();
    	switch ($task){
    		case "UserProfile":
    			
    			$segments[]="userprofile";
    			$userid=JArrayHelper::getValue($query,'id',0);;
   				$segments[]=$userid;
                $user=JFactory::getUser($userid);
   				$segments[]=$user->username;
    				
    			unset($query['Itemid']);
    			unset($query['id']);
    			unset($query["task"]);
    			unset($query["controller"]);
    			break;
            case "profile":
                $segments[] = "profile";
                unset($query["task"]);
                unset($query["controller"]);
                break;
            case "userdetails":
                $segments[] = "userdetails";
                unset($query["task"]);
                unset($query["controller"]);
                break;
            case 'searchusers':
                $segments[] = 'searchusers';

                unset($query["task"]);
                unset($query["controller"]);

                break;
            case 'searchcompanies':
               $segments[] = 'searchcompanies';

               unset($query["task"]);
               unset($query["controller"]);
               break;
    	}
    	
        return $segments;
        
    }

    /**
     * @param $task
     * @param $query
     *
     * @return array
     */
    static function JJobsfactoryControllerMaps($task, &$query)
    {
        $segments = array();
        switch ($task) {

            case "googlemaps":

                $searh      = JArrayHelper::getValue($query, 'search', '');
                $searhUsers = JArrayHelper::getValue($query, 'searchusers', '');

                if ($searh) {
                    $segments[] = "searchonmap";
                    unset($query["search"]);

                } else if ($searhUsers) {
                    $segments[] = "searchusersonmap";
                    unset($query["searchusers"]);
                } else {
                    $segments[] = "jobsonmap";
                }
                unset($query["task"]);
                unset($query["controller"]);
                break;
            case "googlemap_tool":
                $segments[] = "googlemap_tool";
                unset($query["task"]);
                unset($query["controller"]);
                unset($query["tmpl"]);
        }

        return $segments;
    }
}
