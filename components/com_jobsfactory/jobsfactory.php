<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
require_once('defines.php');

//Load Framework
require_once (JPATH_COMPONENT_ADMINISTRATOR . DS . 'thefactory' . DS . 'application' . DS . 'application.class.php');
$MyApp = JTheFactoryApplication::getInstance(null, true);
$MyApp->Initialize();

require_once(JPATH_COMPONENT_SITE . DS . 'options.php');
//Load Classes
require_once JPATH_COMPONENT_SITE . DS . 'classes' . DS . 'jobs_smarty.php';
require_once JPATH_COMPONENT_SITE . DS . 'classes' . DS . 'jobs_smartyview.php';
require_once JPATH_COMPONENT_SITE . DS . 'classes' . DS . 'jobs_model.php';

// Load optional rtl Bootstrap css and Bootstrap bugfixes
JHtmlBootstrap::loadCss(true,JFactory::getDocument()->direction);
    if (!JFolder::exists(JOB_TEMPLATE_CACHE)) JFolder::create(JOB_TEMPLATE_CACHE);

$MyApp->dispatch();
