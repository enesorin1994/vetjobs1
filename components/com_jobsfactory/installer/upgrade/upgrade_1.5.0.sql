CREATE TABLE IF NOT EXISTS `#__jobsfactory_users_rating` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `userid` int(11) NOT NULL COMMENT 'rated user',
 `user_rating` decimal(5,2) NOT NULL,
 `users_count` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT 'gives rating',
  `rated_userid` int(11) NOT NULL,
  `rating_user` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `rated_userid` (`rated_userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;