<?php
/**------------------------------------------------------------------------
thefactory - The Factory Class Library - v 2.5.0
------------------------------------------------------------------------
 * @author    thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: thefactory
 * @subpackage: installer
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

class JTheFactoryUpgrade
{
    function upgrade($oldversion)
    {
        $jnewversion = '1.4.2';
        $x = strcmp($oldversion,$jnewversion);
        $db = JFactory::getDbo();

        if ($oldversion == '1.0.0' ) {
        //if ($x < 0 ) {
            JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_jobsfactory' . DS . 'tables');

            $struct = $db->getTableColumns('#__jobsfactory_jobs');
            if (!array_key_exists('job_country',$struct))
            {
                $db->setQuery("ALTER TABLE `#__jobsfactory_jobs` ADD COLUMN `job_country` INT(11) NOT NULL AFTER `job_location_state` ");
                $db->execute();
            }

            $countrytable = JTable::getInstance('country', 'Table');

            $db->setQuery("SELECT l.`id` as locid, l.country FROM `#__jobsfactory_locations` l ");
            $locations_country = $db->loadObjectList('locid');

            $db->setQuery("ALTER TABLE `#__jobsfactory_locations` MODIFY `country` int(11) NOT NULL");
            $db->execute();

            foreach($locations_country as $k=>$country){

                $countryid = $countrytable->getCountryId($country->country); //getCountryId(name) full match
                $countryidCondition = (isset($countryid)) ? $countryid : 0;

                if ( isset($countryidCondition) ) {
                    $db->setQuery("UPDATE `#__jobsfactory_locations` SET country = $countryidCondition  WHERE id = $country->locid");
                    $db->execute();
                }
            }

            //mail notification columns added from 1.3.1
            //JTheFactoryHelper::tableIncludePath('mailman');
            //$mail_body  = JTable::getInstance('MailmanTable', 'JTheFactory');
            //$mail_struct = $db->getTableColumns('#__'.APP_PREFIX.'_mails');
            $db->setQuery("SHOW COLUMNS FROM ".'#__'.APP_PREFIX.'_mails'." ");
            $emailTableColumns = $db->loadObjectList('Field');

            if(!array_key_exists('group', $emailTableColumns)){
                $db->setQuery("ALTER TABLE `#__jobsfactory_mails` ADD COLUMN `group` enum('candidate','company','admin','watchlist','job') DEFAULT NULL AFTER `mail_type` ");
                $db->execute();

                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `group` = 'company'  WHERE id IN (2,9,10,16,17) ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `group` = 'candidate'  WHERE id IN (1,3,6,8) ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `group` = 'watchlist'  WHERE id IN (5,7,11,12,13,14,15) ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `group` = 'admin'  WHERE id IN (18,19,20) ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `group` = 'job'  WHERE id IN (4) ");
                $db->execute();
            }

            if(!array_key_exists('ordering', $emailTableColumns)){
                $db->setQuery("ALTER TABLE `#__jobsfactory_mails` ADD COLUMN `ordering` INT(11) DEFAULT NULL AFTER `enabled` ");
                $db->execute();

                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 1 WHERE id = 16 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 2 WHERE id = 2 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 3 WHERE id = 3 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 4 WHERE id = 1 ");
                $db->execute();

                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 5 WHERE id = 9 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 6 WHERE id = 12 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 7 WHERE id = 13 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 8 WHERE id = 14 ");
                $db->execute();

                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 9 WHERE id = 15 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 10 WHERE id = 10 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 11 WHERE id = 11 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 12 WHERE id = 18 ");
                $db->execute();

                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 13 WHERE id = 19 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 14 WHERE id = 20 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 15 WHERE id = 17 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 16 WHERE id = 8 ");
                $db->execute();

                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 17 WHERE id = 6 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 18 WHERE id = 7 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 19 WHERE id = 5 ");
                $db->setQuery("UPDATE `#__jobsfactory_mails` SET `ordering` = 20 WHERE id = 4 ");
                $db->execute();
            }

        }

        $struct = $db->getTableColumns('#__jobsfactory_payment_log');

        if (!array_key_exists('gatewayid',$struct))
        {
            $db->setQuery("ALTER TABLE `#__jobsfactory_payment_log` ADD `gatewayid` int(11) DEFAULT NULL AFTER `orderid` ");
            $db->execute();
        }

        if ($oldversion <= '1.4.2' ) {

            /*
             * CREATE TABLE IF NOT EXISTS `#__jobsfactory_users_rating` (
               `id` int(11) NOT NULL AUTO_INCREMENT,
               `userid` int(11) NOT NULL COMMENT 'rated user',
               `user_rating` decimal(5,2) NOT NULL,
               `users_count` int(11) NOT NULL,
               PRIMARY KEY (`id`),
               KEY `userid` (`userid`)
             ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
             */

        }

    }

}

