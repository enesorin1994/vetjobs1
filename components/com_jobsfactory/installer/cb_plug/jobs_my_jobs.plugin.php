<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.2.2
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
 * @subpackage: CBPlugins
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class getmyjobsTab extends cbTabHandler {

	function getmyjobsTab() {
		$this->cbTabHandler();
	}

	function getDisplayTab($tab,$user,$ui){
		
        $Itemid = JFactory::getApplication()->input->getInt('Itemid');
        $context = 'getmyjobstab';
		
		$LO = JFactory::getLanguage();
		$LO->load('com_jobsfactory');
		
		$database = JFactory::getDbo();
		$my = JFactory::getUser();
        $app = JFactory::getApplication();

		if(!file_exists(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."jobsfactory.php")){
			  return "<div>You must First install <a href='http://thephpfactory.com/jobs-factory-for-joomla'> Jobs Factory </a></div>";
		}
		
		require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."options.php");
		require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."helpers".DS."tools.php");
		require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."helpers".DS."route.php");

        $cfg=new JobsfactoryConfig();
		$limitstart = $app->getUserStateFromRequest($context.'.limitstart','limitstart',0,'INT');

        $w="";        
        if ($cfg->admin_approval)
            $w="AND b.approved='1'"; 

		$query = "SELECT count(*)
			FROM #__jobsfactory_jobs b
			left join #__users u on b.userid=u.id
			where userid = '$user->user_id' $w";

		$database->setQuery($query);
		$total = $database->loadResult();
		
		$query = "SELECT u.username,b.*
			FROM #__jobsfactory_jobs b
			left join #__users u on b.userid=u.id
			where userid = '$user->user_id' $w order by `start_date` desc";

		$pagingParams = $this->_getPaging( array(), array( "myjobs_" ) );
		if (isset($pagingParams["myjobs_limitstart"])) {
			$limitstart=$pagingParams["myjobs_limitstart"];
		}

		$database->setQuery($query,$limitstart, $cfg->nr_items_per_page);
		$myjobs = $database->loadObjectList();
		
		$pagingParams["limitstart"] = $limitstart;
		$pagingParams["limit"] = $cfg->nr_items_per_page;

		$return ="";
		$return .= "\t\t<div>\n";
		$return .='<form name="topForm'.$tab->tabid.'" action="index.php" method="post">';
		$return .="<input type='hidden' name='option' value='com_comprofiler' />";
		$return .="<input type='hidden' name='task' value='userProfile' />";
		$return .="<input type='hidden' name='user' value='".$user->user_id."' />";
		$return .="<input type='hidden' name='tab' value='".$tab->tabid."' />";
		$return .="<table width='100%'>";
		if($myjobs) {
			$return	.= '<tr>';
			$return .= '<th class="list_ratings_header">'.JText::_("COM_JOBS_TITLE").'</th>';
			$return .= '<th class="list_ratings_header" width="100">'.JText::_("COM_JOBS_START_DATE").'</th>';
			$return .= '<th class="list_ratings_header" width="100">'.JText::_("COM_JOBS_END_DATE").'</th>';

		    if($my->id==$user->user_id){
				$return .= '<th class="list_ratings_header" width="100">'.JText::_("COM_JOBS_LAST_APPLICATION").'</th>';
			}

			$return .= '</tr>';
			$k=0;
			foreach ($myjobs as $ma)
            {
                if($my->id == $user->user_id){
                    $query = "SELECT modified from #__jobsfactory_candidates where cancel=0 and job_id='$ma->id' order by id desc limit 1";
                    $database->setQuery($query);
                    $last_application = $database->loadResult();

                }
                $return .='<tr class="mywatch'.$k.'">';
                $return .='<td>';
                $return .= '<a href="'.JobsHelperRoute::getJobDetailRoute($ma->id).'">'.$ma->title.'</a>';
                $return .='</td>';
                $return .='<td>';
                $return .=JHtml::date($ma->start_date,$cfg->date_format,false);
                $return .='</td>';
                $return .='<td>';
                $return .=JHtml::date($ma->end_date,$cfg->date_format,false);
                $return .='</td>';
                if($my->id==$user->user_id){
                    $return .= '<td>'.($last_application?$last_application:"-").'</td>';
                }
                $return .= "</tr>";
                $k=1-$k;
			}
		} else {
			$return .=	JText::_("COM_JOBS_NO_JOBS_POSTED");
		}
		if($total>=$cfg->nr_items_per_page)
			$pageslinks = "index.php?option=com_comprofiler&task=userProfile&user=$user->user_id&limitstart=$limitstart&tab=$tab->tabid&Itemid=$Itemid";

		$return .= "<tr height='20px'>";
		$return .= "<td colspan='3' align='center'>";
		$return .= "</td>";
		$return .= "</tr>";
		$return .= "<tr>";
		$return .= "<td colspan='2' align='center'>";
		$return .= $this->_writePaging($pagingParams,"myjobs_", $cfg->nr_items_per_page, $total);
		$return .= "</td>";
		$return .= "</tr>";
		$return .= "</table>";
		$return .= "</form>";
		$return .= "</div>";

		return $return;
	}
}
?>
