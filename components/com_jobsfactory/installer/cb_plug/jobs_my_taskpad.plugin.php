<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.2.2
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
 * @subpackage: CBPlugins
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class myjobsTaskPad extends cbTabHandler {

	function getmyjobstaskpadTab() {
		$this->cbTabHandler();
	}

	function getDisplayTab($tab,$user,$ui){
		
        $Itemid = JFactory::getApplication()->input->getInt('Itemid');

		$database = JFactory::getDbo();
		$my = JFactory::getUser();
		$LO = JFactory::getLanguage();
		$LO->load('com_jobsfactory');
		
		if($my->id!=$user->user_id || !$my->id){
			return null;
		}

		if(!file_exists(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."jobsfactory.php")){
			  return "<div>You must First install <a href='http://thephpfactory.com/jobs-factory-for-joomla'> Jobs Factory </a></div>";
		}
		
		require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."options.php");
		require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."helpers".DS."tools.php");
		require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."helpers".DS."route.php");
        
        $cfg=new JobsfactoryConfig();

		$tasklist=array(
		  'newjob'=>'f_newjob.png',
		  'myjobs'=>'f_myjobs.png ',
		  'myapplications'=>'f_myapplications.png',
		  'watchlist&controller=watchlist'=>'f_mywatchlist.png',
		  'listcats'=>'f_listcats.png',
		  'listjobs'=>'f_listjobs.png',
		  'search'=>'f_search.png'
		);
		$isCompany=true;
		$isCandidate=true;
        $user_groups=JAccess::getGroupsByUser($user->user_id,false);
        $isCandidate=count(array_intersect($user_groups,$cfg->candidate_groups))>0;
        $isCompany=count(array_intersect($user_groups,$cfg->company_groups))>0;

		$isVerified=false;
		$isPowerseller=false;
		
		$isVerified_field = "";
		$isPowerseller_field = "";
		
		$database->setQuery("SELECT * FROM #__jobsfactory_fields_assoc");
		$qq = $database->loadObjectList("field");
		
        if(isset($qq["verified"]) && $qq["verified"]!="") $isVerified_field = $qq["verified"]->assoc_field;		
		if(isset($qq["powerseller"]) && $qq["powerseller"]!="")	$isPowerseller_field = $qq["powerseller"]->assoc_field;
		
		if($isVerified_field)
			$isVerified = $user->$isVerified_field;
		if($isPowerseller_field)
			$isPowerseller = $user->$isPowerseller_field;

		$database->setQuery("SELECT * FROM #__jobsfactory_payment_balance b  WHERE b.userid = '$my->id'");
		$balance = $database->loadObject();

		$database->setQuery("SELECT * FROM #__jobsfactory_currency c  WHERE `default`=1");
		$default_currency = $database->loadObject();

		$return = "\t\t<div>\n";
   		$return .="<table width='100%'>";
		$return .= "\t\t<tr><td colspan=4><div style='padding-left:120px;'>\n";
		if ($isCompany) {
    		$return .= "\t\t<img style='margin-right:70px;' src='".JUri::root()."/components/com_jobsfactory/images/user/isCompany.png' border=0 width=25>\n";
		}else {
    		$return .= "\t\t<img style='margin-right:70px;' src='".JUri::root()."/components/com_jobsfactory/images/user/isCompany2.png' border=0 width=25>\n";
		}
		if ($isCandidate) {
    		$return .= "\t\t<img style='margin-right:70px;' src='".JUri::root()."/components/com_jobsfactory/images/user/isCandidate.png' border=0 width=25>\n";
		}else {
    		$return .= "\t\t<img style='margin-right:70px;' src='".JUri::root()."/components/com_jobsfactory/images/user/isCandidate2.png' border=0 width=25>\n";
		}
		if ($isVerified) {
    		$return .= "\t\t<img style='margin-right:70px;' src='".JUri::root()."/components/com_jobsfactory/images/user/verified_1.gif' border=0 width=25>\n";
		}else {
    		$return .= "\t\t<img style='margin-right:70px;' src='".JUri::root()."/components/com_jobsfactory/images/user/verified_0.gif' border=0 width=25>\n";
		}
		if ($isPowerseller) {
    		$return .= "\t\t<img style='margin-right:70px;' src='".JUri::root()."/components/com_jobsfactory/images/user/powerseller1.png' border=0 width=25>\n";
		}else {
    		$return .= "\t\t<img style='margin-right:70px;' src='".JUri::root()."/components/com_jobsfactory/images/user/powerseller0.png' border=0 width=25 >\n";
		}
		$return .= "\t\t</div></td></tr>\n";
		$return .= "\t\t<tr><td colspan=4><div style='padding-left:80px;'>\n";
   		$return .= "\t\t<div style='width:100px;float:left;text-align:center;'>".JText::_("COM_JOBS_COMPANY_GROUP").":<br />".($isCompany?JText::_("COM_JOBS_YES"):JText::_("COM_JOBS_NO"))."</div>\n";
   		$return .= "\t\t<div style='width:100px;float:left;text-align:center;'>".JText::_("COM_JOBS_CANDIDATE_GROUP").":<br />".($isCandidate?JText::_("COM_JOBS_YES"):JText::_("COM_JOBS_NO"))."</div>\n";
   		$return .= "\t\t<div style='width:100px;float:left;text-align:center;'>".JText::_("COM_JOBS_VERIFIED").":<br />".($isVerified?JText::_("COM_JOBS_YES"):JText::_("COM_JOBS_NO"))."</div>\n";
   		$return .= "\t\t<div style='width:100px;float:left;text-align:center;'>".JText::_("COM_JOBS_POWERSELLER").":<br />".($isPowerseller?JText::_("COM_JOBS_YES"):JText::_("COM_JOBS_NO"))."</div>\n";
		$return .= "\t\t</div></td></tr>\n";
		$return .= "\t\t<tr><td colspan=4>\n";

		$keys=array_keys($tasklist);

   		$return .= "<table width='100%'><tr>";
   		for ($i=0;$i<count($keys)/2;$i++){
   		    $f_task =  JRoute::_("index.php?option=com_jobsfactory&task=".$keys[$i]."&Itemid=".JobsHelperRoute::getItemid(array('task'=>$keys[$i])));
   		    $return .= "<td width='100' align='center'><a href='$f_task'><img src='".JUri::root()."/components/com_jobsfactory/images/menu/".$tasklist[$keys[$i]]."' border=0></a></td>";
   		}
   		$return .= "</tr><tr>";
   		for ($i=count($keys)/2+1;$i<count($keys);$i++){
   		    $f_task = JRoute::_("index.php?option=com_jobsfactory&task=".$keys[$i]."&Itemid=".JobsHelperRoute::getItemid(array('task'=>$keys[$i])));
   		    $return .= "<td width='100' align='center'><a href='$f_task'><img src='".JUri::root()."/components/com_jobsfactory/images/menu/".$tasklist[$keys[$i]]."' border=0></a></td>";
   		}
		$return .= "\t\t</tr></table></td></tr>\n";

	
        $return	.= '<tr>';
        $return .= '<th class="list_ratings_header" colspan=4><hr></th>';
        $return .= '</tr>';
        $return	.= '<tr>';
        $return .= '<th class="list_ratings_header" colspan=4>'.
        JText::_('COM_JOBS_CURRENT_BALANCE').': '.($balance?($balance->balance." ".$balance->currency):"0 ".$default_currency->name);
        $return .= '&nbsp <a href="'.JobsHelperRoute::getAddFundsRoute().'">'.JText::_('COM_JOBS_ADD_FUNDS_TO_YOUR_BALANCE').'</a></th>';
        $return .= '</tr>';
        $return	.= '<tr>';
        $return .= '<th class="list_ratings_header" colspan=4><a href="'.JobsHelperRoute::getPaymentsHistoryRoute().'">'.JText::_("COM_JOBS_SEE_MY_PAYMENTS_HISTORY").'</a></th>';
        $return .= '</tr>';
        
		$return .= "</table>";
		$return .= "</div>";

		return $return;
	}
}

