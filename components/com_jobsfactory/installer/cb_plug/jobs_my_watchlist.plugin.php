<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.2.2
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: Jobs
 * @subpackage: CBPlugins
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class getmyjobswatchlistTab extends cbTabHandler {

	function getmyjobswatchlistTab() {
		$this->cbTabHandler();
	}

	function getDisplayTab($tab,$user,$ui){
		
        $Itemid = JFactory::getApplication()->input->getInt('Itemid');
		
		$database = JFactory::getDbo();
		$my = JFactory::getUser();
        $app = JFactory::getApplication();

		$LO = JFactory::getLanguage();
		$LO->load('com_jobsfactory');
        $context='getmyjobswatchlistTab';

		if($my->id!=$user->user_id){
			return null;
		}

		if(!file_exists(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."jobsfactory.php")){
			  return "<div>You must First install <a href='http://thephpfactory.com/jobs-factory-for-joomla'> Jobs Factory </a></div>";
		}
		
		require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."options.php");
		require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."helpers".DS."tools.php");
		require_once(JPATH_ROOT.DS."components".DS."com_jobsfactory".DS."helpers".DS."route.php");

        $cfg=new JobsfactoryConfig();
		$limitstart = $app->getUserStateFromRequest($context.'.limitstart','limitstart',0,'INT');

        $w="";        
        if ($cfg->admin_approval)
            $w="AND b.approved='1'"; 

		$query = "SELECT count(*)
			FROM #__jobsfactory_jobs b
			left join #__jobsfactory_watchlist w on '$user->user_id'=w.userid
			left join #__users u on b.userid = u.id
			where b.id=w.job_id $w ";

		$database->setQuery($query);
		$total = $database->loadResult();

		$pagingParams = $this->_getPaging( array(), array( "rmywatchlist_" ) );
		if (isset($pagingParams["rmywatchlist_limitstart"])) {
			$limitstart=$pagingParams["rmywatchlist_limitstart"];
		}

		$query = "SELECT u.username,b.*
			FROM #__jobsfactory_jobs b
			left join #__jobsfactory_watchlist w on '$user->user_id'=w.userid
			left join #__users u on b.userid = u.id
			where b.id=w.job_id $w order by id desc";

		$database->setQuery($query,$limitstart, $cfg->nr_items_per_page);
		$mywatches = $database->loadObjectList();
		
		$pagingParams["limitstart"] = $limitstart;
		$pagingParams["limit"] = $cfg->nr_items_per_page;

		$return = "\t\t<div>\n";
		$return .='<form name="topForm'.$tab->tabid.'" action="index.php" method="post">';
		$return .="<input type='hidden' name='option' value='com_comprofiler' />";
		$return .="<input type='hidden' name='task' value='userProfile' />";
		$return .="<input type='hidden' name='user' value='".$user->user_id."' />";
		$return .="<input type='hidden' name='tab' value='".$tab->tabid."' />";
		$return .="<input type='hidden' name='act' value='' />";
		$return .="<table width='100%'>";


		if($mywatches) {
			$return	.= '<tr>';
			$return .= '<th class="list_ratings_header">'.JText::_("COM_JOBS_TITLE").'</th>';
			$return .= '<th class="list_ratings_header" width="80">'.JText::_("COM_JOBS_EMPLOYER").'</th>';
			$return .= '<th class="list_ratings_header" width="80">'.JText::_("COM_JOBS_START_DATE").'</th>';
			$return .= '<th class="list_ratings_header" width="80">'.JText::_("COM_JOBS_END_DATE").'</th>';
			$return .= '<th class="list_ratings_header" width="80">'.JText::_("COM_JOBS_MAX_PRICE").'</th>';
			$return .= '</tr>';
			$k=0;
			foreach ($mywatches as $mw){
    			 $return .='<tr class="mywatch'.$k.'">';
    			 $return .='<td>';
    			 $return .= '<a href="'.JobsHelperRoute::getJobDetailRoute($mw->id).'">'.$mw->title.'</a>';
    			 $return .='</td>';
    			 $return .='<td>';
    			 $return .= '<a href="'.JobsHelperRoute::getUserdetailsRoute($mw->userid).'">'.$mw->username.'</a>';
    			 $return .='</td>';
    			 $return .='<td>';
    			 $return .= JHtml::date($mw->start_date,$cfg->date_format,false);
    			 $return .='</td>';
    			 $return .='<td>';
    			 $return .= JHtml::date($mw->end_date,$cfg->date_format,false);
    			 $return .='</td>';
    			 $return .='<td>';
    			 $return .= $mw->max_price." ".$mw->currency;
    			 $return .='</td>';
    			 $return .= "</tr>";
    			 $k=1-$k;

			 }
		} else {
			$return .=	"".JText::_("COM_JOBS_NO_ITEMS")."";
		}
		if($total>=$cfg->nr_items_per_page)
			$pageslinks = "index.php?option=com_comprofiler&task=userProfile&user=$user->user_id&limitstart=$limitstart&tab=$tab->tabid";
		$return .= "<tr height='20px'>";
		$return .= "<td colspan='3' align='center'>";
		$return .= "</td>";
		$return .= "</tr>";
		$return .= "<tr>";
		$return .= "<td colspan='2' align='center'>";
		$return .= $this->_writePaging($pagingParams,"rmywatchlist_", $cfg->nr_items_per_page, $total);
		$return .= "</td>";
		$return .= "</tr>";
		$return .= "</table>";
		$return .= "</form>";
		$return .= "</div>";

		return $return;
	}
}
?>
