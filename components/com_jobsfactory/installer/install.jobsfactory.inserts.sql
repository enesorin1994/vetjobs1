
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('1', 'name', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('2', 'surname', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('3', 'address', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('4', 'city', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('5', 'country', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('6', 'phone', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('7', 'paypalemail', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('8', 'birth_date', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('9', 'candidate_email', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('10', 'linkedIN', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('11', 'facebook', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('12', 'isVisible', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('13', 'about_us', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('14', 'modified', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('15', 'website', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('16', 'email', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('17', 'twitter', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('18', 'googleMaps_x', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('19', 'googleMaps_y', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('20', 'YM', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('21', 'Hotmail', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('22', 'Skype', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('23', 'verified', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('24', 'powerseller', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('25', 'useruniqueid', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('26', 'contactname', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('27', 'shortdescription', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('28', 'contactposition', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('29', 'contactphone', '');
INSERT INTO `#__jobsfactory_fields_assoc` VALUES ('30', 'contactemail', '');


INSERT INTO `#__jobsfactory_country` VALUES (4,'AFGHANISTAN','AF',1);
INSERT INTO `#__jobsfactory_country` VALUES (5,'ALAND ISLANDS','AX',0);
INSERT INTO `#__jobsfactory_country` VALUES (6,'ALBANIA','AL',1);
INSERT INTO `#__jobsfactory_country` VALUES (7,'ALGERIA','DZ',0);
INSERT INTO `#__jobsfactory_country` VALUES (8,'AMERICAN SAMOA','AS',0);
INSERT INTO `#__jobsfactory_country` VALUES (9,'ANDORRA','AD',0);
INSERT INTO `#__jobsfactory_country` VALUES (10,'ANGOLA','AO',0);
INSERT INTO `#__jobsfactory_country` VALUES (11,'ANGUILLA','AI',0);
INSERT INTO `#__jobsfactory_country` VALUES (12,'ANTARCTICA','AQ',0);
INSERT INTO `#__jobsfactory_country` VALUES (13,'ANTIGUA AND BARBUDA','AG',0);
INSERT INTO `#__jobsfactory_country` VALUES (14,'ARGENTINA','AR',0);
INSERT INTO `#__jobsfactory_country` VALUES (15,'ARMENIA','AM',0);
INSERT INTO `#__jobsfactory_country` VALUES (16,'ARUBA','AW',0);
INSERT INTO `#__jobsfactory_country` VALUES (17,'AUSTRALIA','AU',1);
INSERT INTO `#__jobsfactory_country` VALUES (18,'AUSTRIA','AT',1);
INSERT INTO `#__jobsfactory_country` VALUES (19,'AZERBAIJAN','AZ',0);
INSERT INTO `#__jobsfactory_country` VALUES (20,'BAHAMAS','BS',0);
INSERT INTO `#__jobsfactory_country` VALUES (21,'BAHRAIN','BH',0);
INSERT INTO `#__jobsfactory_country` VALUES (22,'BANGLADESH','BD',0);
INSERT INTO `#__jobsfactory_country` VALUES (23,'BARBADOS','BB',0);
INSERT INTO `#__jobsfactory_country` VALUES (24,'BELARUS','BY',0);
INSERT INTO `#__jobsfactory_country` VALUES (25,'BELGIUM','BE',0);
INSERT INTO `#__jobsfactory_country` VALUES (26,'BELIZE','BZ',0);
INSERT INTO `#__jobsfactory_country` VALUES (27,'BENIN','BJ',0);
INSERT INTO `#__jobsfactory_country` VALUES (28,'BERMUDA','BM',0);
INSERT INTO `#__jobsfactory_country` VALUES (29,'BHUTAN','BT',0);
INSERT INTO `#__jobsfactory_country` VALUES (30,'BOLIVIA','BO',0);
INSERT INTO `#__jobsfactory_country` VALUES (31,'BOSNIA AND HERZEGOVINA','BA',0);
INSERT INTO `#__jobsfactory_country` VALUES (32,'BOTSWANA','BW',0);
INSERT INTO `#__jobsfactory_country` VALUES (33,'BOUVET ISLAND','BV',0);
INSERT INTO `#__jobsfactory_country` VALUES (34,'BRAZIL','BR',0);
INSERT INTO `#__jobsfactory_country` VALUES (35,'BRITISH INDIAN OCEAN TERRITORY','IO',0);
INSERT INTO `#__jobsfactory_country` VALUES (36,'BRUNEI DARUSSALAM','BN',0);
INSERT INTO `#__jobsfactory_country` VALUES (37,'BULGARIA','BG',0);
INSERT INTO `#__jobsfactory_country` VALUES (38,'BURKINA FASO','BF',0);
INSERT INTO `#__jobsfactory_country` VALUES (39,'BURUNDI','BI',0);
INSERT INTO `#__jobsfactory_country` VALUES (40,'CAMBODIA','KH',0);
INSERT INTO `#__jobsfactory_country` VALUES (41,'CAMEROON','CM',0);
INSERT INTO `#__jobsfactory_country` VALUES (42,'CANADA','CA',0);
INSERT INTO `#__jobsfactory_country` VALUES (43,'CAPE VERDE','CV',0);
INSERT INTO `#__jobsfactory_country` VALUES (44,'CAYMAN ISLANDS','KY',0);
INSERT INTO `#__jobsfactory_country` VALUES (45,'CENTRAL AFRICAN REPUBLIC','CF',0);
INSERT INTO `#__jobsfactory_country` VALUES (46,'CHAD','TD',0);
INSERT INTO `#__jobsfactory_country` VALUES (47,'CHILE','CL',0);
INSERT INTO `#__jobsfactory_country` VALUES (48,'CHINA','CN',0);
INSERT INTO `#__jobsfactory_country` VALUES (49,'CHRISTMAS ISLAND','CX',0);
INSERT INTO `#__jobsfactory_country` VALUES (50,'COCOS (KEELING) ISLANDS','CC',0);
INSERT INTO `#__jobsfactory_country` VALUES (51,'COLOMBIA','CO',0);
INSERT INTO `#__jobsfactory_country` VALUES (52,'COMOROS','KM',0);
INSERT INTO `#__jobsfactory_country` VALUES (53,'CONGO','CG',0);
INSERT INTO `#__jobsfactory_country` VALUES (54,'CONGO,THE DEMOCRATIC REPUBLIC OF THE','CD',0);
INSERT INTO `#__jobsfactory_country` VALUES (55,'COOK ISLANDS','CK',0);
INSERT INTO `#__jobsfactory_country` VALUES (56,'COSTA RICA','CR',0);
INSERT INTO `#__jobsfactory_country` VALUES (57,'CROATIA','HR',0);
INSERT INTO `#__jobsfactory_country` VALUES (58,'CUBA','CU',0);
INSERT INTO `#__jobsfactory_country` VALUES (59,'CYPRUS','CY',0);
INSERT INTO `#__jobsfactory_country` VALUES (60,'CZECH REPUBLIC','CZ',0);
INSERT INTO `#__jobsfactory_country` VALUES (61,'DENMARK','DK',0);
INSERT INTO `#__jobsfactory_country` VALUES (62,'DJIBOUTI','DJ',0);
INSERT INTO `#__jobsfactory_country` VALUES (63,'DOMINICA','DM',0);
INSERT INTO `#__jobsfactory_country` VALUES (64,'DOMINICAN REPUBLIC','DO',0);
INSERT INTO `#__jobsfactory_country` VALUES (65,'ECUADOR','EC',0);
INSERT INTO `#__jobsfactory_country` VALUES (66,'EGYPT','EG',0);
INSERT INTO `#__jobsfactory_country` VALUES (67,'EL SALVADOR','SV',0);
INSERT INTO `#__jobsfactory_country` VALUES (68,'EQUATORIAL GUINEA','GQ',0);
INSERT INTO `#__jobsfactory_country` VALUES (69,'ERITREA','ER',0);
INSERT INTO `#__jobsfactory_country` VALUES (70,'ESTONIA','EE',0);
INSERT INTO `#__jobsfactory_country` VALUES (71,'ETHIOPIA','ET',0);
INSERT INTO `#__jobsfactory_country` VALUES (72,'FALKLAND ISLANDS (MALVINAS)','FK',0);
INSERT INTO `#__jobsfactory_country` VALUES (73,'FAROE ISLANDS','FO',0);
INSERT INTO `#__jobsfactory_country` VALUES (74,'FIJI','FJ',0);
INSERT INTO `#__jobsfactory_country` VALUES (75,'FINLAND','FI',0);
INSERT INTO `#__jobsfactory_country` VALUES (76,'FRANCE','FR',1);
INSERT INTO `#__jobsfactory_country` VALUES (77,'FRENCH GUIANA','GF',1);
INSERT INTO `#__jobsfactory_country` VALUES (78,'FRENCH POLYNESIA','PF',0);
INSERT INTO `#__jobsfactory_country` VALUES (79,'FRENCH SOUTHERN TERRITORIES','TF',0);
INSERT INTO `#__jobsfactory_country` VALUES (80,'GABON','GA',0);
INSERT INTO `#__jobsfactory_country` VALUES (81,'GAMBIA','GM',0);
INSERT INTO `#__jobsfactory_country` VALUES (82,'GEORGIA','GE',0);
INSERT INTO `#__jobsfactory_country` VALUES (83,'GERMANY','DE',1);
INSERT INTO `#__jobsfactory_country` VALUES (84,'GHANA','GH',0);
INSERT INTO `#__jobsfactory_country` VALUES (85,'GIBRALTAR','GI',0);
INSERT INTO `#__jobsfactory_country` VALUES (86,'GREECE','GR',0);
INSERT INTO `#__jobsfactory_country` VALUES (87,'GREENLAND','GL',0);
INSERT INTO `#__jobsfactory_country` VALUES (88,'GRENADA','GD',0);
INSERT INTO `#__jobsfactory_country` VALUES (89,'GUADELOUPE','GP',0);
INSERT INTO `#__jobsfactory_country` VALUES (90,'GUAM','GU',0);
INSERT INTO `#__jobsfactory_country` VALUES (91,'GUATEMALA','GT',0);
INSERT INTO `#__jobsfactory_country` VALUES (92,'GUERNSEY','GG',0);
INSERT INTO `#__jobsfactory_country` VALUES (93,'GUINEA','GN',0);
INSERT INTO `#__jobsfactory_country` VALUES (94,'GUINEA-BISSAU','GW',0);
INSERT INTO `#__jobsfactory_country` VALUES (95,'GUYANA','GY',0);
INSERT INTO `#__jobsfactory_country` VALUES (96,'HAITI','HT',0);
INSERT INTO `#__jobsfactory_country` VALUES (97,'HEARD ISLAND AND MCDONALD ISLANDS','HM',0);
INSERT INTO `#__jobsfactory_country` VALUES (98,'HONDURAS','HN',0);
INSERT INTO `#__jobsfactory_country` VALUES (99,'HONG KONG','HK',0);
INSERT INTO `#__jobsfactory_country` VALUES (100,'HUNGARY','HU',0);
INSERT INTO `#__jobsfactory_country` VALUES (101,'ICELAND','IS',0);
INSERT INTO `#__jobsfactory_country` VALUES (102,'INDIA','IN',0);
INSERT INTO `#__jobsfactory_country` VALUES (103,'INDONESIA','ID',0);
INSERT INTO `#__jobsfactory_country` VALUES (104,'IRAN, ISLAMIC REPUBLIC OF','IR',0);
INSERT INTO `#__jobsfactory_country` VALUES (105,'IRAQ','IQ',0);
INSERT INTO `#__jobsfactory_country` VALUES (106,'IRELAND','IE',0);
INSERT INTO `#__jobsfactory_country` VALUES (107,'ISLE OF MAN','IM',0);
INSERT INTO `#__jobsfactory_country` VALUES (108,'ISRAEL','IL',0);
INSERT INTO `#__jobsfactory_country` VALUES (109,'ITALY','IT',0);
INSERT INTO `#__jobsfactory_country` VALUES (110,'JAMAICA','JM',0);
INSERT INTO `#__jobsfactory_country` VALUES (111,'JAPAN','JP',0);
INSERT INTO `#__jobsfactory_country` VALUES (112,'JERSEY','JE',0);
INSERT INTO `#__jobsfactory_country` VALUES (113,'JORDAN','JO',0);
INSERT INTO `#__jobsfactory_country` VALUES (114,'KAZAKHSTAN','KZ',0);
INSERT INTO `#__jobsfactory_country` VALUES (115,'KENYA','KE',0);
INSERT INTO `#__jobsfactory_country` VALUES (116,'KIRIBATI','KI',0);
INSERT INTO `#__jobsfactory_country` VALUES (117,'KOREA, REPUBLIC OF','KR',0);
INSERT INTO `#__jobsfactory_country` VALUES (118,'KUWAIT','KW',0);
INSERT INTO `#__jobsfactory_country` VALUES (119,'KYRGYZSTAN','KG',0);
INSERT INTO `#__jobsfactory_country` VALUES (120,'LATVIA','LV',0);
INSERT INTO `#__jobsfactory_country` VALUES (121,'LEBANON','LB',0);
INSERT INTO `#__jobsfactory_country` VALUES (122,'LESOTHO','LS',0);
INSERT INTO `#__jobsfactory_country` VALUES (123,'LIBERIA','LR',0);
INSERT INTO `#__jobsfactory_country` VALUES (124,'LIBYAN ARAB JAMAHIRIYA','LY',0);
INSERT INTO `#__jobsfactory_country` VALUES (125,'LIECHTENSTEIN','LI',0);
INSERT INTO `#__jobsfactory_country` VALUES (126,'LITHUANIA','LT',0);
INSERT INTO `#__jobsfactory_country` VALUES (127,'LUXEMBOURG','LU',0);
INSERT INTO `#__jobsfactory_country` VALUES (128,'MACAO','MO',0);
INSERT INTO `#__jobsfactory_country` VALUES (129,'MACEDONI, THE FORMER YUGOSLAV REPUBLIC OF','MK',0);
INSERT INTO `#__jobsfactory_country` VALUES (130,'MADAGASCAR','MG',0);
INSERT INTO `#__jobsfactory_country` VALUES (131,'MALAWI','MW',0);
INSERT INTO `#__jobsfactory_country` VALUES (132,'MALAYSIA','MY',0);
INSERT INTO `#__jobsfactory_country` VALUES (133,'MALDIVES','MV',0);
INSERT INTO `#__jobsfactory_country` VALUES (134,'MALI','ML',0);
INSERT INTO `#__jobsfactory_country` VALUES (135,'MALTA','MT',0);
INSERT INTO `#__jobsfactory_country` VALUES (136,'MARSHALL ISLANDS','MH',0);
INSERT INTO `#__jobsfactory_country` VALUES (137,'MARTINIQUE','MQ',0);
INSERT INTO `#__jobsfactory_country` VALUES (138,'MAURITANIA','MR',0);
INSERT INTO `#__jobsfactory_country` VALUES (139,'MAURITIUS','MU',0);
INSERT INTO `#__jobsfactory_country` VALUES (140,'MAYOTTE','YT',0);
INSERT INTO `#__jobsfactory_country` VALUES (141,'MEXICO','MX',0);
INSERT INTO `#__jobsfactory_country` VALUES (142,'MICRONESIA, FEDERATED STATES OF','FM',0);
INSERT INTO `#__jobsfactory_country` VALUES (143,'MOLDOVA, REPUBLIC OF','MD',0);
INSERT INTO `#__jobsfactory_country` VALUES (144,'MONACO','MC',0);
INSERT INTO `#__jobsfactory_country` VALUES (145,'MONGOLIA','MN',0);
INSERT INTO `#__jobsfactory_country` VALUES (146,'MONTENEGRO','ME',0);
INSERT INTO `#__jobsfactory_country` VALUES (147,'MONTSERRAT','MS',0);
INSERT INTO `#__jobsfactory_country` VALUES (148,'MOROCCO','MA',0);
INSERT INTO `#__jobsfactory_country` VALUES (149,'MOZAMBIQUE','MZ',0);
INSERT INTO `#__jobsfactory_country` VALUES (150,'MYANMAR','MM',0);
INSERT INTO `#__jobsfactory_country` VALUES (151,'NAMIBIA','NA',0);
INSERT INTO `#__jobsfactory_country` VALUES (152,'NAURU','NR',0);
INSERT INTO `#__jobsfactory_country` VALUES (153,'NEPAL','NP',0);
INSERT INTO `#__jobsfactory_country` VALUES (154,'NETHERLANDS','NL',0);
INSERT INTO `#__jobsfactory_country` VALUES (155,'NETHERLANDS ANTILLES','AN',0);
INSERT INTO `#__jobsfactory_country` VALUES (156,'NEW CALEDONIA','NC',0);
INSERT INTO `#__jobsfactory_country` VALUES (157,'NEW ZEALAND','NZ',0);
INSERT INTO `#__jobsfactory_country` VALUES (158,'NICARAGUA','NI',0);
INSERT INTO `#__jobsfactory_country` VALUES (159,'NIGER','NE',0);
INSERT INTO `#__jobsfactory_country` VALUES (160,'NIGERIA','NG',0);
INSERT INTO `#__jobsfactory_country` VALUES (161,'NIUE','NU',0);
INSERT INTO `#__jobsfactory_country` VALUES (162,'NORFOLK ISLAND','NF',0);
INSERT INTO `#__jobsfactory_country` VALUES (163,'NORTHERN MARIANA ISLANDS','MP',0);
INSERT INTO `#__jobsfactory_country` VALUES (164,'NORWAY','NO',0);
INSERT INTO `#__jobsfactory_country` VALUES (165,'OMAN','OM',0);
INSERT INTO `#__jobsfactory_country` VALUES (166,'PAKISTAN','PK',0);
INSERT INTO `#__jobsfactory_country` VALUES (167,'PALAU','PW',0);
INSERT INTO `#__jobsfactory_country` VALUES (168,'PALESTINIAN TERRITORY, OCCUPIED','PS',0);
INSERT INTO `#__jobsfactory_country` VALUES (169,'PANAMA','PA',0);
INSERT INTO `#__jobsfactory_country` VALUES (170,'PAPUA NEW GUINEA','PG',0);
INSERT INTO `#__jobsfactory_country` VALUES (171,'PARAGUAY','PY',0);
INSERT INTO `#__jobsfactory_country` VALUES (172,'PERU','PE',0);
INSERT INTO `#__jobsfactory_country` VALUES (173,'PHILIPPINES','PH',0);
INSERT INTO `#__jobsfactory_country` VALUES (174,'PITCAIRN','PN',0);
INSERT INTO `#__jobsfactory_country` VALUES (175,'POLAND','PL',0);
INSERT INTO `#__jobsfactory_country` VALUES (176,'PORTUGAL','PT',0);
INSERT INTO `#__jobsfactory_country` VALUES (177,'PUERTO RICO','PR',0);
INSERT INTO `#__jobsfactory_country` VALUES (178,'QATAR','QA',0);
INSERT INTO `#__jobsfactory_country` VALUES (179,'R�UNION','RE',0);
INSERT INTO `#__jobsfactory_country` VALUES (180,'ROMANIA','RO',1);
INSERT INTO `#__jobsfactory_country` VALUES (181,'RUSSIAN FEDERATION','RU',0);
INSERT INTO `#__jobsfactory_country` VALUES (182,'RWANDA','RW',0);
INSERT INTO `#__jobsfactory_country` VALUES (183,'SAINT HELENA','SH',0);
INSERT INTO `#__jobsfactory_country` VALUES (184,'SAINT KITTS AND NEVIS','KN',0);
INSERT INTO `#__jobsfactory_country` VALUES (185,'SAINT LUCIA','LC',0);
INSERT INTO `#__jobsfactory_country` VALUES (186,'SAINT PIERRE AND MIQUELON','PM',0);
INSERT INTO `#__jobsfactory_country` VALUES (187,'SAINT VINCENT AND THE GRENADINES','VC',0);
INSERT INTO `#__jobsfactory_country` VALUES (188,'SAMOA','WS',0);
INSERT INTO `#__jobsfactory_country` VALUES (189,'SAN MARINO','SM',0);
INSERT INTO `#__jobsfactory_country` VALUES (190,'SAO TOME AND PRINCIPE','ST',0);
INSERT INTO `#__jobsfactory_country` VALUES (191,'SAUDI ARABIA','SA',0);
INSERT INTO `#__jobsfactory_country` VALUES (192,'SENEGAL','SN',0);
INSERT INTO `#__jobsfactory_country` VALUES (193,'SERBIA','RS',0);
INSERT INTO `#__jobsfactory_country` VALUES (194,'SEYCHELLES','SC',0);
INSERT INTO `#__jobsfactory_country` VALUES (195,'SIERRA LEONE','SL',0);
INSERT INTO `#__jobsfactory_country` VALUES (196,'SINGAPORE','SG',0);
INSERT INTO `#__jobsfactory_country` VALUES (197,'SLOVAKIA','SK',0);
INSERT INTO `#__jobsfactory_country` VALUES (198,'SLOVENIA','SI',0);
INSERT INTO `#__jobsfactory_country` VALUES (199,'SOLOMON ISLANDS','SB',0);
INSERT INTO `#__jobsfactory_country` VALUES (200,'SOMALIA','SO',0);
INSERT INTO `#__jobsfactory_country` VALUES (201,'SOUTH AFRICA','ZA',0);
INSERT INTO `#__jobsfactory_country` VALUES (202,'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','GS',0);
INSERT INTO `#__jobsfactory_country` VALUES (203,'SPAIN','ES',0);
INSERT INTO `#__jobsfactory_country` VALUES (204,'SRI LANKA','LK',0);
INSERT INTO `#__jobsfactory_country` VALUES (205,'SUDAN','SD',0);
INSERT INTO `#__jobsfactory_country` VALUES (206,'SURINAME','SR',0);
INSERT INTO `#__jobsfactory_country` VALUES (207,'SVALBARD AND JAN MAYEN','SJ',0);
INSERT INTO `#__jobsfactory_country` VALUES (208,'SWAZILAND','SZ',0);
INSERT INTO `#__jobsfactory_country` VALUES (209,'SWEDEN','SE',0);
INSERT INTO `#__jobsfactory_country` VALUES (210,'SWITZERLAND','CH',1);
INSERT INTO `#__jobsfactory_country` VALUES (211,'SYRIAN ARAB REPUBLIC','SY',0);
INSERT INTO `#__jobsfactory_country` VALUES (212,'TAIWAN, PROVINCE OF CHINA','TW',0);
INSERT INTO `#__jobsfactory_country` VALUES (213,'TAJIKISTAN','TJ',0);
INSERT INTO `#__jobsfactory_country` VALUES (214,'TANZANIA, UNITED REPUBLIC OF','TZ',0);
INSERT INTO `#__jobsfactory_country` VALUES (215,'THAILAND','TH',0);
INSERT INTO `#__jobsfactory_country` VALUES (216,'TIMOR-LESTE','TL',0);
INSERT INTO `#__jobsfactory_country` VALUES (217,'TOGO','TG',0);
INSERT INTO `#__jobsfactory_country` VALUES (218,'TOKELAU','TK',0);
INSERT INTO `#__jobsfactory_country` VALUES (219,'TONGA','TO',0);
INSERT INTO `#__jobsfactory_country` VALUES (220,'TRINIDAD AND TOBAGO','TT',0);
INSERT INTO `#__jobsfactory_country` VALUES (221,'TUNISIA','TN',0);
INSERT INTO `#__jobsfactory_country` VALUES (222,'TURKEY','TR',0);
INSERT INTO `#__jobsfactory_country` VALUES (223,'TURKMENISTAN','TM',0);
INSERT INTO `#__jobsfactory_country` VALUES (224,'TURKS AND CAICOS ISLANDS','TC',0);
INSERT INTO `#__jobsfactory_country` VALUES (225,'TUVALU','TV',0);
INSERT INTO `#__jobsfactory_country` VALUES (226,'UGANDA','UG',0);
INSERT INTO `#__jobsfactory_country` VALUES (227,'UKRAINE','UA',0);
INSERT INTO `#__jobsfactory_country` VALUES (228,'UNITED ARAB EMIRATES','AE',0);
INSERT INTO `#__jobsfactory_country` VALUES (229,'UNITED KINGDOM','GB',1);
INSERT INTO `#__jobsfactory_country` VALUES (230,'UNITED STATES','US',1);
INSERT INTO `#__jobsfactory_country` VALUES (231,'UNITED STATES MINOR OUTLYING ISLANDS','UM',0);
INSERT INTO `#__jobsfactory_country` VALUES (232,'URUGUAY','UY',0);
INSERT INTO `#__jobsfactory_country` VALUES (233,'UZBEKISTAN','UZ',0);
INSERT INTO `#__jobsfactory_country` VALUES (234,'VANUATU','VU',0);
INSERT INTO `#__jobsfactory_country` VALUES (235,'VATICAN CITY STATE (HOLY SEE)','VA',0);
INSERT INTO `#__jobsfactory_country` VALUES (236,'VENEZUELA','VE',0);
INSERT INTO `#__jobsfactory_country` VALUES (237,'VIET NAM','VN',0);
INSERT INTO `#__jobsfactory_country` VALUES (238,'VIRGIN ISLANDS, BRITISH','VG',0);
INSERT INTO `#__jobsfactory_country` VALUES (239,'VIRGIN ISLANDS, U.S.','VI',0);
INSERT INTO `#__jobsfactory_country` VALUES (240,'WALLIS AND FUTUNA','WF',0);
INSERT INTO `#__jobsfactory_country` VALUES (241,'WESTERN SAHARA','EH',0);
INSERT INTO `#__jobsfactory_country` VALUES (242,'YEMEN','YE',0);
INSERT INTO `#__jobsfactory_country` VALUES (243,'ZAMBIA','ZM',0);
INSERT INTO `#__jobsfactory_country` VALUES (244,'ZIMBABWE','ZW',0);


INSERT INTO `#__jobsfactory_currency` VALUES (1, 'USD', '4', '0.75529', null);
INSERT INTO `#__jobsfactory_currency` VALUES (2, 'EUR', '5', '1.00000', 1);
INSERT INTO `#__jobsfactory_currency` VALUES (5, 'CHF', '2', '0.81830', null);
INSERT INTO `#__jobsfactory_currency` VALUES (8, 'AUD', '3', '0.65090', null);


INSERT INTO `#__jobsfactory_mails` VALUES (1,'new_message', 'candidate', 'Dear %NAME%  %SURNAME%,  You received a new reply to your application regarding the Job:  %JOBTITLE%  You can see the message here  %JOBLINK%  You must login to read the message.','New message regarding Job %JOBTITLE%',1,4);
INSERT INTO `#__jobsfactory_mails` VALUES (2,'new_application', 'company', 'Dear %NAME% %SURNAME%,  You received a new Application for your job %JOBTITLE%  You can get more details on the webpage  %JOBLINK%','New application on your Job %JOBTITLE%',1,2);
INSERT INTO `#__jobsfactory_mails` VALUES (3,'new_resume_created','candidate','Dear %NAME% %SURNAME%,  Your resume has been created successfully.','Your resume has been created successfully',0,3);
INSERT INTO `#__jobsfactory_mails` VALUES (4,'job_application_canceled','job','One application was cancelled','One application was canceled',0,20);
INSERT INTO `#__jobsfactory_mails` VALUES (5,'job_watchlist_canceled','watchlist', 'Dear %NAME% %SURNAME%,  The job %JOBTITLE% from your watchlist was canceled!  You can get more details on the webpage  %JOBLINK%','Informations about your Watched Job  %JOBTITLE%',0,19);
INSERT INTO `#__jobsfactory_mails` VALUES (6,'job_canceled','candidate','Dear %NAME% %SURNAME%,  The job %JOBTITLE%  was canceled!  You can get more details on the webpage  %JOBLINK%','Informations about job on  %JOBTITLE%',1,17);
INSERT INTO `#__jobsfactory_mails` VALUES (7,'job_watchlist_closed','watchlist','Dear %NAME% %SURNAME%,  The job %JOBTITLE% from your watchlist was closed !  You can get more details on the webpage  %JOBLINK%','Informations about your Watched Job %JOBTITLE%',0,18);
INSERT INTO `#__jobsfactory_mails` VALUES (8,'job_closed','candidate','Dear %NAME% %SURNAME%,  The job %JOBTITLE% was closed !  You can get more details on the webpage  %JOBLINK%','Informations about job on %JOBTITLE%',1,16);
INSERT INTO `#__jobsfactory_mails` VALUES (9,'job_admin_message','company','Dear %NAME% %SURNAME%,  You received a message from one of the Administrators regarding your job %JOBTITLE%  !  You can get more details on the webpage  %JOBLINK%','New Message from an Admin regarding %JOBTITLE%',1,5);
INSERT INTO `#__jobsfactory_mails` VALUES (10,'job_your_will_expire','company','Dear %NAME% %SURNAME%,  Your Job %JOBTITLE% will soon expire !  You can get more details on the webpage  %JOBLINK%','Your Job %JOBTITLE% will soon expire',1,10);
INSERT INTO `#__jobsfactory_mails` VALUES (11,'job_watchlist_will_expire','watchlist', 'Dear %NAME% %SURNAME%,  The Job you watched: %JOBTITLE% will soon expire !  You can get more details on the webpage  %JOBLINK%','Informations about your watched Job  %JOBTITLE%',1,11);
INSERT INTO `#__jobsfactory_mails` VALUES (12,'new_job_watch','watchlist','Dear %NAME% %SURNAME%,  There is a new Job in the category you watched: %JOBTITLE%  You can get more details on the webpage  %JOBLINK%','Informations about your watched Category  %CATTITLE%',0,6);
INSERT INTO `#__jobsfactory_mails` VALUES (13,'new_job_watch_company','watchlist','Dear %NAME% %SURNAME%,  There is a new Job from the company you watched: %JOBTITLE%  You can get more details on the webpage  %JOBLINK%','Informations about your watched company  %NAME%',0,7);
INSERT INTO `#__jobsfactory_mails` VALUES (14,'new_job_watch_location','watchlist','Dear %NAME% %SURNAME%,  There is a new Job in the location you watched: %JOBTITLE%  You can get more details on the webpage  %JOBLINK%','Informations about your watched location',0,8);
INSERT INTO `#__jobsfactory_mails` VALUES (15,'new_job_watch_city','watchlist','Dear %NAME% %SURNAME%,  There is a new Job in the city you watched: %JOBTITLE%  You can get more details on the webpage  %JOBLINK%','Informations about your watched city',0,9);
INSERT INTO `#__jobsfactory_mails` VALUES (16,'new_job_created','company','Dear %NAME% %SURNAME%,  Your Job has been created successfully: %JOBTITLE%  You can get more details on the webpage  %JOBLINK%','Your Job: %JOBTITLE% has been created successfully',1,1);
INSERT INTO `#__jobsfactory_mails` VALUES (17,'job_admin_close_job','company','Dear %NAME% %SURNAME%,  Your Job has been closed by admin - %JOBTITLE%  You can get more details on the webpage  %JOBLINK%','Your Job: %JOBTITLE% has been closed by Admin',0,15);
INSERT INTO `#__jobsfactory_mails` VALUES (18,'job_admin_approval','admin','Dear %NAME% %SURNAME%,  A new job was placed  - %JOBTITLE%  and it needs Admin Approval.','A new job has been placed: %JOBTITLE%',1,12);
INSERT INTO `#__jobsfactory_mails` VALUES (19,'admin_job_approved','admin','Dear %NAME% %SURNAME%,  Your job was approved by Admin - %JOBTITLE%.','Approved job link: %JOBTITLE%',1,13);
INSERT INTO `#__jobsfactory_mails` VALUES (20,'admin_job_unapproved','admin','Dear %NAME% %SURNAME%,  Your job was unApproved by Admin - %JOBTITLE% .','UnApproved job link: %JOBTITLE%',1,14);


INSERT INTO `#__jobsfactory_paysystems` VALUES ('1', 'Paypal', 'pay_paypal', '1', 'paypalemail=\"change@me.com1\"\nuse_sandbox=\"1\"\nauto_accept=\"1\"', '1', '1');
INSERT INTO `#__jobsfactory_paysystems` VALUES ('2', 'Moneybookers', 'pay_moneybookers', '1', 'email=\"change@me.com1\"', '2', '0');
INSERT INTO `#__jobsfactory_paysystems` VALUES ('3', '2Checkout', 'pay_2checkout', '0', 'x_login=\"change@me.com2\"\ntest_mode=\"1\"', '3', '0');
INSERT INTO `#__jobsfactory_paysystems` VALUES ('4', 'BankTransfer', 'pay_banktransfer', '1', 'bank_info=\"<p>Info</p>\"', '4', '0');
INSERT INTO `#__jobsfactory_paysystems` VALUES ('5', 'Sagepayments.net', 'pay_sagepayments', '0', 'Terminal_id=\"2131231\"\nLogo_url=\"\"\nB_color=\"\"\nBF_color=\"\"\nM_color=\"\"\nF_color=\"\"', '6', '0');
INSERT INTO `#__jobsfactory_paysystems` VALUES ('6', 'Authorize.net', 'pay_authnet', '1', 'loginID=""\r\ntransactionKey=""\r\nsandboxMode=""\r\nheader_html_payment_form=""\r\nfooter_html_payment_form=""\r\nheader2_html_payment_form=""\r\nfooter2_html_payment_form=""\r\ncolor_background=""\r\ncolor_link=""\r\ncolor_text=""\r\nlogo_url=""\r\nbackground_url=""\r\ncancel_url_text=""', NULL, 0);
INSERT INTO `#__jobsfactory_paysystems` (`paysystem`, `classname`, `enabled`, `ordering`) VALUES ('Mollie', 'pay_mollie', 0, 7);

INSERT INTO `#__jobsfactory_pricing` VALUES ('1', 'featured', 'fixed', 'Featured Job', '33.00', 'USD', '0', 'price_powerseller=\"22\"\nprice_verified=\"11\"\ncategory_pricing_enabled=', '2');
INSERT INTO `#__jobsfactory_pricing` VALUES ('2', 'listing', 'fixed', 'Pay per Listing', '10.00', 'USD', '0', 'price_powerseller=\"\"\nprice_verified=\"\"\ncategory_pricing_enabled=\nemail_text=\"Hello %SURNAME%,\nWe kindly remind you that you have an outstanding payment of %BALANCE% for the services provided by our site.\nLog in using your username \"%USERNAME%\" and follow this link in order to pay the fees:\n%LINK%\"', '1');
INSERT INTO `#__jobsfactory_pricing` VALUES ('3', 'comission', 'percent', 'Earn Commission', '10.00', 'USD', '0', 'price_powerseller=\"5\"\nprice_verified=\"1\"\ncategory_pricing_enabled=\nemail_text=\"PHA+SGVsbG8gJVNVUk5BTUUlLGE8L3A+DQo8cD7CoDwvcD4NCjxwPmFkIC0gJUJBTEFOQ0UlPC9wPg0KPHA+YXMgLSAlTElOSyU8L3A+DQo8cD5kYTwvcD4NCjxwPnNkPC9wPg0KPHA+YXM8L3A+\"', '2');
INSERT INTO `#__jobsfactory_pricing` VALUES ('4', 'contact', 'fixed', 'Pay per Contact', '44.00', 'USD', '0', 'price_powerseller=\"4\"\nprice_verified=\"2\"\ncategory_pricing_enabled=', '2');


INSERT INTO `#__jobsfactory_studieslevel` (`id`, `levelname`, `published`, `ordering`) VALUES
(1, 'School', 1, NULL),
(2, 'Highschool', 1, NULL),
(3, 'College being', 1, NULL),
(4, 'College graduate', 1, NULL),
(5, 'Vocational school', 1, NULL),
(6, 'University', 0, NULL),
(7, 'Master', 1, NULL);

INSERT INTO `#__jobsfactory_jobtype` (`id`, `typename`, `published`, `ordering`) VALUES
(1, 'Full time', 1, 1),
(2, 'Part time', 1, 2),
(3, 'Project-based', 0, 3),
(4, 'Temporary', 1, NULL),
(5, 'Practice', 1, NULL);


INSERT INTO `#__jobsfactory_experiencelevel` (`id`, `levelname`, `published`, `ordering`) VALUES
(1, 'No experience', 1, NULL),
(2, '0-6 months', 1, NULL),
(3, '6-12 months', 1, NULL),
(4, '1-2 years', 1, NULL),
(5, '2-5 years', 1, NULL),
(6, '5-10 years', 1, NULL),
(7, 'over 10 years', 1, NULL);
