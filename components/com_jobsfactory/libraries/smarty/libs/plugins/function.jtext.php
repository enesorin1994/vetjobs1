<?php
defined('_JEXEC') or die('Restricted access');
    /** @license - http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html GNU/LGPL */
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {jtext} function plugin
 *
 * Type:     function<br>
 * Name:     jtext<br>
 * Purpose:  displays ini contants<br>
 *
 * @param array
 * @param Smarty
 */
function smarty_function_jtext($params, &$smarty)
{
  $text = isset($params['text']) ? $params['text'] : $params['_'];

  return JText::_($text);
}

/* {jtext text="abc"} */

?>
