<?php
    defined('_JEXEC') or die('Restricted access');
    /** @license - http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html GNU/LGPL */

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Get seconds and microseconds
 * @return double
 */
function smarty_core_get_microtime($params, &$smarty)
{
    $mtime = microtime();
    $mtime = explode(" ", $mtime);
    $mtime = (double)($mtime[1]) + (double)($mtime[0]);
    return ($mtime);
}


/* vim: set expandtab: */

?>
