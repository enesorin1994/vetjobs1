<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

	// Access the file from Joomla environment
	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryFilterPrices extends JTheFactoryFilters
	{

		public function filterPriceToDisplay($price)
		{
			if (!$price) {
				return $price;
			}

			return JobsHelperTools::formatNumberToDisplay($price);
		}
	}
