<?php
/* Smarty version 3.1.29, created on 2017-03-22 13:19:34
  from "C:\xampp\htdocs\Vet\components\com_eventsfactory\src\Views\Event\edit.html.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_58d26bd68106b6_33843843',
  'file_dependency' => 
  array (
    '820ae936aa5520c62ca790554bf4ccf5ceed6702' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\src\\Views\\Event\\edit.html.tpl',
      1 => 1488964492,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:layout.html.tpl' => 1,
    'file:Event/_warnings.html.tpl' => 1,
    'file:Event/_manage.html.tpl' => 1,
    'file:Event/_form.html.tpl' => 1,
  ),
),false)) {
function content_58d26bd68106b6_33843843 ($_smarty_tpl) {
if (!is_callable('smarty_function_assets')) require_once 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\framework\\Smarty\\Plugins\\function.assets.php';
if (!is_callable('smarty_function_trans')) require_once 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\framework\\Smarty\\Plugins\\function.trans.php';
if (!is_callable('smarty_function_url')) require_once 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\framework\\Smarty\\Plugins\\function.url.php';
if (!is_callable('smarty_function_event_approved')) require_once 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\src\\Smarty\\Plugins\\function.event_approved.php';
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "main", array (
  0 => 'block_1113158d26bd64f7670_82930120',
  1 => false,
  3 => 0,
  2 => 0,
));
?>

<?php $_smarty_tpl->ext->_inheritance->endChild($_smarty_tpl);
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:layout.html.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'main'}  file:Event/edit.html.tpl */
function block_1113158d26bd64f7670_82930120($_smarty_tpl, $_blockParentStack) {
?>

    <?php echo smarty_function_assets(array('html'=>array("formbehavior.chosen"),'script'=>array("assets/js/views/event/edit",("https://maps.googleapis.com/maps/api/js?key=").($_smarty_tpl->tpl_vars['googleMapsApiKey']->value))),$_smarty_tpl);?>

    <div class="eventsfactory-event-edit">
        <?php if ($_smarty_tpl->tpl_vars['event']->value->id) {?>
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:Event/_warnings.html.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php }?>

        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:Event/_manage.html.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('event_id'=>$_smarty_tpl->tpl_vars['event']->value->id), 0, false);
?>


        <h1>
            <?php if ($_smarty_tpl->tpl_vars['event']->value->id) {?>
                <?php echo smarty_function_trans(array('text'=>"event_edit_page_heading",'tokens'=>array($_smarty_tpl->tpl_vars['event']->value->title)),$_smarty_tpl);?>

            <?php } else { ?>
                <?php echo smarty_function_trans(array('text'=>"event_organize_page_heading"),$_smarty_tpl);?>

            <?php }?>
        </h1>

        <?php if ($_smarty_tpl->tpl_vars['event']->value->id) {?>
        <form action="<?php echo smarty_function_url(array('task'=>"event.edit",'id'=>$_smarty_tpl->tpl_vars['event']->value->id),$_smarty_tpl);?>
" method="post" enctype="multipart/form-data">
            <?php } else { ?>
            <form action="<?php echo smarty_function_url(array('task'=>"event.organize"),$_smarty_tpl);?>
" method="post" enctype="multipart/form-data">
                <?php }?>
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:Event/_form.html.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


                <div class="actions">
                    <?php if (!$_smarty_tpl->tpl_vars['approveEvents']->value || !$_smarty_tpl->tpl_vars['event']->value->isSubmitted()) {?>
                        <button type="submit" class="btn btn-small btn-success">
                            <?php if ($_smarty_tpl->tpl_vars['event']->value->id) {?>
                                <?php echo smarty_function_trans(array('text'=>"event_edit_button_update"),$_smarty_tpl);?>

                            <?php } else { ?>
                                <?php echo smarty_function_trans(array('text'=>"event_edit_button_organize"),$_smarty_tpl);?>

                            <?php }?>
                        </button>
                    <?php }?>

                    <a href="<?php echo smarty_function_url(array('task'=>"event.details",'id'=>$_smarty_tpl->tpl_vars['event']->value->id),$_smarty_tpl);?>
" class="btn btn-small btn-danger">
                        <?php echo smarty_function_trans(array('text'=>"event_edit_button_cancel"),$_smarty_tpl);?>

                    </a>

                    <?php if ($_smarty_tpl->tpl_vars['event']->value->id && $_smarty_tpl->tpl_vars['approveEvents']->value) {?>
                        <div class="pull-right" style="display: none;">
                            <?php echo smarty_function_trans(array('text'=>"event_edit_approved_status_label"),$_smarty_tpl);?>

                            <?php echo smarty_function_event_approved(array('event'=>$_smarty_tpl->tpl_vars['event']->value),$_smarty_tpl);?>


                            <?php if ($_smarty_tpl->tpl_vars['event']->value->isPending() || $_smarty_tpl->tpl_vars['event']->value->isRejected()) {?>
                                <a href="<?php echo smarty_function_url(array('task'=>"approve.submit",'event_id'=>$_smarty_tpl->tpl_vars['event']->value->id),$_smarty_tpl);?>
"
                                   class="btn btn-small btn-primary">
                                    <?php echo smarty_function_trans(array('text'=>"event_edit_button_submit_approval"),$_smarty_tpl);?>

                                </a>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['event']->value->isSubmitted()) {?>
                                <a href="<?php echo smarty_function_url(array('task'=>"approve.cancel",'event_id'=>$_smarty_tpl->tpl_vars['event']->value->id),$_smarty_tpl);?>
"
                                   class="btn btn-small btn-danger">
                                    <?php echo smarty_function_trans(array('text'=>"event_edit_button_cancel_approval_submission"),$_smarty_tpl);?>

                                </a>
                            <?php }?>
                        </div>
                    <?php }?>
                </div>
            </form>

    </div>
<?php
}
/* {/block 'main'} */
}
