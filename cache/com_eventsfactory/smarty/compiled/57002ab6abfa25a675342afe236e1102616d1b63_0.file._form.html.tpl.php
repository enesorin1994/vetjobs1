<?php
/* Smarty version 3.1.29, created on 2017-03-22 13:19:34
  from "C:\xampp\htdocs\Vet\components\com_eventsfactory\src\Views\Event\_form.html.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_58d26bd6ad7655_72791404',
  'file_dependency' => 
  array (
    '57002ab6abfa25a675342afe236e1102616d1b63' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\src\\Views\\Event\\_form.html.tpl',
      1 => 1488964492,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58d26bd6ad7655_72791404 ($_smarty_tpl) {
if (!is_callable('smarty_function_trans')) require_once 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\framework\\Smarty\\Plugins\\function.trans.php';
if (!is_callable('smarty_function_poster')) require_once 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\src\\Smarty\\Plugins\\function.poster.php';
if ($_smarty_tpl->tpl_vars['form']->value->getErrors()) {?>
    <div class="alert alert-danger">
        <button class="close" data-dismiss="alert">&times;</button>
        <h4><?php echo smarty_function_trans(array('text'=>"form_heading_error"),$_smarty_tpl);?>
</h4>

        <?php
$_from = $_smarty_tpl->tpl_vars['form']->value->getErrors();
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_error_0_saved_item = isset($_smarty_tpl->tpl_vars['error']) ? $_smarty_tpl->tpl_vars['error'] : false;
$_smarty_tpl->tpl_vars['error'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['error']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->_loop = true;
$__foreach_error_0_saved_local_item = $_smarty_tpl->tpl_vars['error'];
?>
            <div>
                <?php echo $_smarty_tpl->tpl_vars['error']->value->getMessage();?>

            </div>
        <?php
$_smarty_tpl->tpl_vars['error'] = $__foreach_error_0_saved_local_item;
}
if ($__foreach_error_0_saved_item) {
$_smarty_tpl->tpl_vars['error'] = $__foreach_error_0_saved_item;
}
?>
    </div>
<?php }?>

<h3>Details</h3>
<?php
$_from = $_smarty_tpl->tpl_vars['form']->value->getFieldset('details');
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_field_1_saved_item = isset($_smarty_tpl->tpl_vars['field']) ? $_smarty_tpl->tpl_vars['field'] : false;
$_smarty_tpl->tpl_vars['field'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
$__foreach_field_1_saved_local_item = $_smarty_tpl->tpl_vars['field'];
?>
    <?php echo $_smarty_tpl->tpl_vars['field']->value->renderField();?>

<?php
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_1_saved_local_item;
}
if ($__foreach_field_1_saved_item) {
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_1_saved_item;
}
?>

<div class="row-fluid">
    <div class="span6">
        <h3>Media</h3>

        <div style="margin-bottom: 10px;">
            <?php echo smarty_function_poster(array('path'=>$_smarty_tpl->tpl_vars['poster']->value->path,'extension'=>$_smarty_tpl->tpl_vars['poster']->value->extension),$_smarty_tpl);?>

        </div>

        <?php
$_from = $_smarty_tpl->tpl_vars['form']->value->getFieldset('poster');
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_field_2_saved_item = isset($_smarty_tpl->tpl_vars['field']) ? $_smarty_tpl->tpl_vars['field'] : false;
$_smarty_tpl->tpl_vars['field'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
$__foreach_field_2_saved_local_item = $_smarty_tpl->tpl_vars['field'];
?>
            <?php echo $_smarty_tpl->tpl_vars['field']->value->renderField();?>

        <?php
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_2_saved_local_item;
}
if ($__foreach_field_2_saved_item) {
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_2_saved_item;
}
?>
    </div>

    <div class="span6">
        <h3>Dates</h3>
        <?php
$_from = $_smarty_tpl->tpl_vars['form']->value->getFieldset('dates');
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_field_3_saved_item = isset($_smarty_tpl->tpl_vars['field']) ? $_smarty_tpl->tpl_vars['field'] : false;
$_smarty_tpl->tpl_vars['field'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
$__foreach_field_3_saved_local_item = $_smarty_tpl->tpl_vars['field'];
?>
            <?php echo $_smarty_tpl->tpl_vars['field']->value->renderField();?>

        <?php
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_3_saved_local_item;
}
if ($__foreach_field_3_saved_item) {
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_3_saved_item;
}
?>

        <h3>Registration</h3>
        <?php
$_from = $_smarty_tpl->tpl_vars['form']->value->getFieldset('registration');
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_field_4_saved_item = isset($_smarty_tpl->tpl_vars['field']) ? $_smarty_tpl->tpl_vars['field'] : false;
$_smarty_tpl->tpl_vars['field'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
$__foreach_field_4_saved_local_item = $_smarty_tpl->tpl_vars['field'];
?>
            <?php echo $_smarty_tpl->tpl_vars['field']->value->renderField();?>

        <?php
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_4_saved_local_item;
}
if ($__foreach_field_4_saved_item) {
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_4_saved_item;
}
?>
    </div>
</div>

<h3 id="location" data-defaults="<?php echo htmlentities(json_encode($_smarty_tpl->tpl_vars['defaultLocation']->value));?>
">Location</h3>
<?php
$_from = $_smarty_tpl->tpl_vars['form']->value->getFieldset('location');
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_field_5_saved_item = isset($_smarty_tpl->tpl_vars['field']) ? $_smarty_tpl->tpl_vars['field'] : false;
$_smarty_tpl->tpl_vars['field'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
$__foreach_field_5_saved_local_item = $_smarty_tpl->tpl_vars['field'];
?>
    <?php echo $_smarty_tpl->tpl_vars['field']->value->renderField();?>

<?php
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_5_saved_local_item;
}
if ($__foreach_field_5_saved_item) {
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_5_saved_item;
}
}
}
