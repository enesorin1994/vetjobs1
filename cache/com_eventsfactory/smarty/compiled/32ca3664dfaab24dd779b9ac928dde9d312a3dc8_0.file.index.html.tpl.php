<?php
/* Smarty version 3.1.29, created on 2017-03-21 13:37:31
  from "C:\xampp\htdocs\Vet\components\com_eventsfactory\src\Views\Event\index.html.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_58d11e8bf3c017_91992501',
  'file_dependency' => 
  array (
    '32ca3664dfaab24dd779b9ac928dde9d312a3dc8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\src\\Views\\Event\\index.html.tpl',
      1 => 1488964492,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:layout.html.tpl' => 1,
    'file:Event/_list_event.html.tpl' => 1,
  ),
),false)) {
function content_58d11e8bf3c017_91992501 ($_smarty_tpl) {
if (!is_callable('smarty_function_assets')) require_once 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\framework\\Smarty\\Plugins\\function.assets.php';
if (!is_callable('smarty_function_trans')) require_once 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\framework\\Smarty\\Plugins\\function.trans.php';
if (!is_callable('smarty_function_url')) require_once 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\framework\\Smarty\\Plugins\\function.url.php';
if (!is_callable('smarty_function_tablesort')) require_once 'C:\\xampp\\htdocs\\Vet\\components\\com_eventsfactory\\framework\\Smarty\\Plugins\\function.tablesort.php';
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "main", array (
  0 => 'block_2756058d11e8bec2e71_11050436',
  1 => false,
  3 => 0,
  2 => 0,
));
?>

<?php $_smarty_tpl->ext->_inheritance->endChild($_smarty_tpl);
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:layout.html.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'main'}  file:Event/index.html.tpl */
function block_2756058d11e8bec2e71_11050436($_smarty_tpl, $_blockParentStack) {
?>

    <?php echo smarty_function_assets(array('html'=>array("bootstrap.loadCss","jquery.framework"),'script'=>array("media/com_eventsfactory/assets/js/views/event/index")),$_smarty_tpl);?>

    <div class="eventsfactory-event-index">
        <h1><?php echo smarty_function_trans(array('text'=>"event_index_page_title"),$_smarty_tpl);?>
</h1>

        <?php if ($_smarty_tpl->tpl_vars['events']->value) {?>
            <div class="events-filters"><?php ob_start();
echo smarty_function_url(array('task'=>"event.index"),$_smarty_tpl);
$_tmp1=ob_get_clean();
ob_start();
echo smarty_function_trans(array('text'=>"event_index_title"),$_smarty_tpl);
$_tmp2=ob_get_clean();
echo smarty_function_tablesort(array('filter'=>$_smarty_tpl->tpl_vars['filter']->value,'column'=>"title",'url'=>$_tmp1,'label'=>$_tmp2),$_smarty_tpl);
ob_start();
echo smarty_function_url(array('task'=>"event.index"),$_smarty_tpl);
$_tmp3=ob_get_clean();
ob_start();
echo smarty_function_trans(array('text'=>"event_index_date"),$_smarty_tpl);
$_tmp4=ob_get_clean();
echo smarty_function_tablesort(array('filter'=>$_smarty_tpl->tpl_vars['filter']->value,'column'=>"starts_at",'url'=>$_tmp3,'label'=>$_tmp4),$_smarty_tpl);
ob_start();
echo smarty_function_url(array('task'=>"event.index"),$_smarty_tpl);
$_tmp5=ob_get_clean();
ob_start();
echo smarty_function_trans(array('text'=>"event_index_likes"),$_smarty_tpl);
$_tmp6=ob_get_clean();
echo smarty_function_tablesort(array('filter'=>$_smarty_tpl->tpl_vars['filter']->value,'column'=>"likes",'url'=>$_tmp5,'label'=>$_tmp6),$_smarty_tpl);?>
</div>
            <div class="eventsfactory-event-list">
                <?php
$_from = $_smarty_tpl->tpl_vars['events']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_event_0_saved_item = isset($_smarty_tpl->tpl_vars['event']) ? $_smarty_tpl->tpl_vars['event'] : false;
$_smarty_tpl->tpl_vars['event'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['event']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['event']->value) {
$_smarty_tpl->tpl_vars['event']->_loop = true;
$__foreach_event_0_saved_local_item = $_smarty_tpl->tpl_vars['event'];
?>
                    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:Event/_list_event.html.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                <?php
$_smarty_tpl->tpl_vars['event'] = $__foreach_event_0_saved_local_item;
}
if ($__foreach_event_0_saved_item) {
$_smarty_tpl->tpl_vars['event'] = $__foreach_event_0_saved_item;
}
?>
            </div>
            <div class="pagination">
                <?php echo $_smarty_tpl->tpl_vars['pagination']->value->getPagesLinks();?>

            </div>
        <?php } else { ?>
            <p><?php echo smarty_function_trans(array('text'=>"event_index_no_events"),$_smarty_tpl);?>
</p>
        <?php }?>
    </div>
<?php
}
/* {/block 'main'} */
}
