<?php /* Smarty version 2.6.28, created on 2017-03-15 13:04:58
         compiled from t_listads.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_listads.tpl', 3, false),array('function', 'set_js', 't_listads.tpl', 4, false),array('function', 'jtext', 't_listads.tpl', 27, false),array('function', 'cycle', 't_listads.tpl', 39, false),array('function', 'positions', 't_listads.tpl', 63, false),array('function', 'printprice', 't_listads.tpl', 72, false),array('function', 'jhtml', 't_listads.tpl', 154, false),array('modifier', 'cat', 't_listads.tpl', 34, false),array('modifier', 'truncate', 't_listads.tpl', 51, false),array('modifier', 'translate', 't_listads.tpl', 93, false),array('modifier', 'default', 't_listads.tpl', 123, false),array('modifier', 'count', 't_listads.tpl', 181, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_countdown.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php echo $this->_plugins['function']['set_js'][0][0]->smarty_set_js(array(), $this);?>


<form action="<?php echo $this->_tpl_vars['action']; ?>
" method="post" name="adsForm" id="adsForm">
	<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />
	<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
	<input type="hidden" name="task" value="<?php echo $this->_tpl_vars['task']; ?>
" />
    <?php if ($this->_tpl_vars['task'] == 'watchlist'): ?>
        <input type="hidden" name="controller" value="watchlist" />
    <?php endif; ?>
    <?php if ($this->_tpl_vars['list_view'] == ''): ?>
        <input type="hidden" name="list_view" value="<?php echo $this->_tpl_vars['list_view']; ?>
" />
    <?php endif; ?>

    <div>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "t_listadds_header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </div>

</form>

<div class="adds_man_list">

  <?php if ($this->_tpl_vars['link_new_listing']): ?>
    <a href = "<?php echo $this->_tpl_vars['link_new_listing']; ?>
"><img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
new_listing.png" class="ads_noborder"
       alt = "<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_NEW_LISTING_IN_CATEGORY'), $this);?>
"
       title = "<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_NEW_LISTING_IN_CATEGORY'), $this);?>
" /></a>
<?php endif; ?>

    <div class="ads_noborder width100">
        <?php $_from = $this->_tpl_vars['ad_rows']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
            <?php if ($this->_tpl_vars['item']->featured && $this->_tpl_vars['item']->featured != 'none'): ?>
                <?php $this->assign('class_featured', ((is_array($_tmp="listing-")) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['item']->featured) : smarty_modifier_cat($_tmp, $this->_tpl_vars['item']->featured))); ?>
            <?php else: ?>
                <?php $this->assign('class_featured', ""); ?>
            <?php endif; ?>

            <?php echo smarty_function_cycle(array('assign' => 'class_suffix','values' => "1,2"), $this);?>

            <div>

            <div class="adds_man_item<?php echo $this->_tpl_vars['class_suffix']; ?>
_title <?php echo $this->_tpl_vars['class_featured']; ?>
">
                <div class="pull-left ads_top_align" style="width: 15%; min-width:75px; max-width: <?php echo $this->_tpl_vars['thumb_width']; ?>
px; max-height: <?php echo $this->_tpl_vars['thumb_width']; ?>
px; padding: 2px;">
                    <a href="<?php echo $this->_tpl_vars['item']->get('links.details'); ?>
"><?php echo $this->_tpl_vars['item']->get('thumbnail'); ?>
</a>
                </div>
                <div class="pull-left listads_middle">
                    <a href="<?php echo $this->_tpl_vars['item']->get('links.details'); ?>
"><span class="title"><?php echo $this->_tpl_vars['item']->title; ?>
</span></a>

                    <div class="adsfactory_shortdescription hidden-phone">
                        <?php if ($this->_tpl_vars['cfg']->ads_short_desc_long != "" && $this->_tpl_vars['item']->short_description != ""): ?>
                            <?php echo ((is_array($_tmp=$this->_tpl_vars['item']->short_description)) ? $this->_run_mod_handler('truncate', true, $_tmp, $this->_tpl_vars['cfg']->ads_short_desc_long) : smarty_modifier_truncate($_tmp, $this->_tpl_vars['cfg']->ads_short_desc_long)); ?>

                        <?php elseif ($this->_tpl_vars['item']->short_description != ""): ?>
                           <?php echo $this->_tpl_vars['item']->short_description; ?>

                        <?php else: ?>
                           &nbsp;
                        <?php endif; ?>
                   </div>

                   <div class="category_adslist">
                        <a href='<?php echo $this->_tpl_vars['item']->get('links.filter_cat'); ?>
' class="category_link"><?php echo $this->_tpl_vars['item']->get('catname'); ?>
</a>&nbsp;
                   </div>
                   <div class="ad_details ads_position_list">
                   <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "cell-middle",'item' => $this->_tpl_vars['item'],'page' => 'ads'), $this);?>

                   </div>
                </div>

                <div class="box_price_listads" style="max-height: <?php echo $this->_tpl_vars['thumb_width']; ?>
px;">
                    <div class="askprice_listads">
                        <?php if ($this->_tpl_vars['item']->askprice): ?>
                            <span class="price_listads">
                                <?php if (is_numeric ( $this->_tpl_vars['item']->askprice )): ?>
                                    <span class="askprice_listads_numeric"><?php echo $this->_plugins['function']['printprice'][0][0]->smarty_print_price(array('price' => $this->_tpl_vars['item']->askprice,'currency' => $this->_tpl_vars['item']->currency), $this);?>
</span>
                                <?php else: ?>
                                    <?php echo $this->_tpl_vars['item']->askprice; ?>

                                <?php endif; ?>
                            </span>
                        <?php endif; ?>
                    </div>
                    <div class="box_ads_position_list">
                        <span class="pull-right ad_details ads_position_list" style="font-size: 11px !important;"><?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "cell-footer",'item' => $this->_tpl_vars['item'],'page' => 'ads'), $this);?>
</span>
                    </div>
                </div>
            </div>

            <div style="padding: 2px 0 0 0;" class="adds_man_item<?php echo $this->_tpl_vars['class_suffix']; ?>
_footer <?php echo $this->_tpl_vars['class_featured']; ?>
">
                            <?php if (! $this->_tpl_vars['item']->isMyAd()): ?>
                   <span class="pull-left">
                    <?php if ($this->_tpl_vars['item']->get('favorite')): ?>
                        <a href = '<?php echo $this->_tpl_vars['item']->get('links.del_from_watchlist'); ?>
'>
                            <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_0.png" title = "<?php echo ((is_array($_tmp='COM_ADS_REMOVE_FROM_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt = "<?php echo ((is_array($_tmp='COM_ADS_REMOVE_FROM_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" height="16"  class="ads_noborder ads_list_icon" />
                        </a>
                    <?php else: ?>
                        <a href = '<?php echo $this->_tpl_vars['item']->get('links.add_to_watchlist'); ?>
'>
                            <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_1.png" title = "<?php echo ((is_array($_tmp='COM_ADS_ADD_TO_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                                 alt = "<?php echo ((is_array($_tmp='COM_ADS_ADD_TO_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" height="16"  class="ads_noborder ads_list_icon" />
                        </a>
                    <?php endif; ?>
                   </span>
                <?php else: ?>
                    <a href='<?php echo $this->_tpl_vars['item']->get('links.edit'); ?>
'>
                        <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
edit.png" title="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_EDIT'), $this);?>
" alt="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_EDIT'), $this);?>
" height="16"  class="pull-left ads_noborder ads_list_icon" />
                    </a>
                <?php endif; ?>

                <span class="ads_hits listing_footer_text <?php echo $this->_tpl_vars['class_featured']; ?>
"><?php echo $this->_tpl_vars['item']->hits; ?>
 <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_HITS'), $this);?>
 &nbsp;</span>

                <?php if ($this->_tpl_vars['item']->get('imagecount') > 1): ?>                     <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
morepics_on.png" title="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_MORE_PICTURES'), $this);?>
" alt="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_MORE_PICTURES'), $this);?>
" height="16"  class="pull-left ads_noborder ads_list_icon" />
                <?php endif; ?>

                <?php if ($this->_tpl_vars['cfg']->enable_attach): ?>
                   <?php if ($this->_tpl_vars['item']->atachment): ?>
                     <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
downloads_on.png" title="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_HAS_DOWNLOADS'), $this);?>
" alt="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_HAS_DOWNLOADS'), $this);?>
" height="16" class="pull-left ads_noborder ads_list_icon" />
                   <?php endif; ?>
                <?php endif; ?>

                <?php if ($this->_tpl_vars['cfg']->google_key != "" && $this->_tpl_vars['cfg']->map_in_ad_details == '1'): ?>
                   <?php if ($this->_tpl_vars['item']->googlex != "" && $this->_tpl_vars['item']->googley != ""): ?>
                       <a class="modal" href="<?php echo $this->_tpl_vars['item']->get('links.googlemap'); ?>
"<?php echo '
                          rel="{handler: \'iframe\', size: { '; ?>
 x:<?php echo ((is_array($_tmp=@$this->_tpl_vars['cfg']->googlemap_gx+25)) ? $this->_run_mod_handler('default', true, $_tmp, '260') : smarty_modifier_default($_tmp, '260')); ?>
, y:<?php echo ((is_array($_tmp=@$this->_tpl_vars['cfg']->googlemap_gy+20)) ? $this->_run_mod_handler('default', true, $_tmp, '100') : smarty_modifier_default($_tmp, '100')); ?>
} <?php echo ' } " '; ?>
>
                           <img style="height: 16px;" src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
map.png" title="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_GOOGLEMAP'), $this);?>
" alt="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_GOOGLEMAP'), $this);?>
" class="pull-left ads_noborder ads_list_icon" />
                       </a>
                   <?php endif; ?>
                <?php endif; ?>
                <span class="listads_bottom_user"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/list/t_listadds_user.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></span>

                <span class="listads_bottom_date listing_footer_text <?php echo $this->_tpl_vars['class_featured']; ?>
">                    <?php if ($this->_tpl_vars['item']->closed): ?>
                       <div>
                           <span class='expired'>
                               <?php if ($this->_tpl_vars['item']->end_date < $this->_tpl_vars['item']->closed_date && ! $this->_tpl_vars['item']->close_by_admin): ?>
                                   <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_AD_CLOSED_ON'), $this);?>
:
                               <?php else: ?>
                                   <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_AD_CLOSED_ON'), $this);?>
:
                               <?php endif; ?>
                           </span>
                           <?php if ($this->_tpl_vars['item']->get('closed_date_text')): ?> <?php echo $this->_tpl_vars['item']->get('closed_date_text'); ?>
 <?php endif; ?>
                       </div>
                   <?php elseif ($this->_tpl_vars['item']->get('expired')): ?>
                       <div>
                           <span class='expired'><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_EXPIRED'), $this);?>
</span>
                       </div>
                   <?php elseif ($this->_tpl_vars['item']->close_by_admin): ?>
                       <div>
                           <span class='expired'><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_AD_CLOSED_BY_ADMIN'), $this);?>
</span>
                       </div>
                   <?php else: ?>                        <div>
                           <?php if ($this->_tpl_vars['cfg']->ads_sort_date == 'start'): ?>
                                <?php if ($this->_tpl_vars['cfg']->enable_countdown): ?>
                                    <i class="icon-clock"></i>&nbsp;<?php echo $this->_plugins['function']['jhtml'][0][0]->smarty_jhtml(array('_' => "date.relative",'date' => $this->_tpl_vars['item']->start_date), $this);?>

                                <?php else: ?>
                                    <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_POSTED_ON'), $this);?>
: <?php echo $this->_tpl_vars['item']->get('startdate_text'); ?>

                                <?php endif; ?>
                           <?php else: ?>
                               <?php if ($this->_tpl_vars['cfg']->enable_countdown): ?>
                                  <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ENDING_IN'), $this);?>
 <span class="timer" id="time<?php echo $this->_tpl_vars['key']+1; ?>
"><?php echo $this->_tpl_vars['item']->get('countdown'); ?>
</span>
                               <?php else: ?>
                                   <?php echo $this->_tpl_vars['item']->get('enddate_text'); ?>

                               <?php endif; ?>
                           <?php endif; ?>
                       </div>
                   <?php endif; ?>
                   <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "cell-right",'item' => $this->_tpl_vars['item'],'page' => 'ads'), $this);?>

               </span>
            </div>
        </div>
        <?php endforeach; endif; unset($_from); ?>
    </div>

    <a target="_blank" href="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_adsfactory&amp;format=feed<?php $_from = $this->_tpl_vars['filters']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?><?php if ($this->_tpl_vars['item']): ?>&<?php echo $this->_tpl_vars['key']; ?>
=<?php echo $this->_tpl_vars['item']; ?>
<?php endif; ?><?php endforeach; endif; unset($_from); ?>">
  		<img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsfactory/images/livemarks.png" class="pull-right ads_noborder" alt="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_RSS'), $this);?>
" /></a>

    <div class="pagination">
        <?php echo $this->_tpl_vars['pagination']->getPagesLinks(); ?>

    </div>

<?php if (! ( count($this->_tpl_vars['ad_rows']) > 0 )): ?>
	<div class="adds_man_item1">
	 <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_NO_ADS'), $this);?>

	</div>
<?php endif; ?>
</div>

<div class="v_spacer_15">&nbsp;</div>
<div style="clear: both;"></div>