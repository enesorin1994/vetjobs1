<?php /* Smarty version 2.6.28, created on 2017-03-15 13:04:58
         compiled from t_listadds_header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'jtext', 't_listadds_header.tpl', 7, false),array('function', 'jroute', 't_listadds_header.tpl', 53, false),array('function', 'html_options', 't_listadds_header.tpl', 97, false),array('modifier', 'count', 't_listadds_header.tpl', 14, false),array('modifier', 'translate', 't_listadds_header.tpl', 40, false),)), $this); ?>
<div>
    <?php echo $this->_tpl_vars['inputsHiddenFilters']; ?>


    <?php if ($this->_tpl_vars['category_filter']): ?>
        <div class="ads_category_navigation">
            <?php echo $this->_tpl_vars['category_filter']->navigation; ?>
<br />
            <h3 class="ads_category_header"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_CATEGORY'), $this);?>
: <?php echo $this->_tpl_vars['category_filter']->catname; ?>
</h3>
            <?php if ($this->_tpl_vars['category_filter']->description): ?>
                <span class="ads_category_description"><?php echo $this->_tpl_vars['category_filter']->description; ?>
</span><br/>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (count($this->_tpl_vars['htmlLabelFilters']) > 0): ?>
        <div>
    		<?php $_from = $this->_tpl_vars['htmlLabelFilters']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['filters'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['filters']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
        $this->_foreach['filters']['iteration']++;
?>
    		  <?php echo $this->_tpl_vars['key']; ?>
 - <span class="ad_filter"><?php echo $this->_tpl_vars['item']; ?>
</span><?php if (! ($this->_foreach['filters']['iteration'] == $this->_foreach['filters']['total'])): ?>,<?php endif; ?>
        	<?php endforeach; endif; unset($_from); ?>
            	<a href="index.php?option=com_adsfactory&task=<?php if ($this->_tpl_vars['task'] == 'myads'): ?>myads<?php else: ?>listads<?php endif; ?>&reset=all&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
">
                    <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
remove_filter1.png" border="0" title="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_REMOVE_FILTER'), $this);?>
" alt="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_REMOVE_FILTER'), $this);?>
"
                        onmouseover="this.src='<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
remove_filter2.png';"
                        onmouseout="this.src='<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
remove_filter1.png';" /></a>
        	<br /><br />
        </div>
    <?php endif; ?>

    <div class="width100">
        <div style="height: auto;" class="add_list_header header_title_sort padding0">
            <nav class="wrap mainnav adsnavbar-collapse-fixed-top" id="ads-mainnav">
                <div class="adsnavbar">
                    <div class="adsnavbar-inner">
            	                          <div class="adsnav-collapse collapse always-show">
                        <ul class="adsnav">

                        <?php if ($this->_tpl_vars['task'] == 'myads'): ?>
                            <li class="<?php if (( $this->_tpl_vars['filter_myads'] == 'active' || $this->_tpl_vars['filter_myads'] == '' )): ?>active<?php else: ?>inactive<?php endif; ?>">
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/filter_button.tpl', 'smarty_include_vars' => array('filter' => 'active','label' => ((is_array($_tmp='COM_ADS_ACTIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            </li>
                            <li class="<?php if (( $this->_tpl_vars['filter_myads'] == 'expired' )): ?>active<?php else: ?>inactive<?php endif; ?>">
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/filter_button.tpl', 'smarty_include_vars' => array('filter' => 'expired','label' => ((is_array($_tmp='COM_ADS_EXPIRED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            </li>
                            <li class="<?php if (( $this->_tpl_vars['filter_myads'] == 'archive' )): ?>active<?php else: ?>inactive<?php endif; ?>">
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/filter_button.tpl', 'smarty_include_vars' => array('filter' => 'archive','label' => ((is_array($_tmp='COM_ADS_ARCHIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            </li>
                            <li class="<?php if (( $this->_tpl_vars['filter_myads'] == 'unpublish' )): ?>active<?php else: ?>inactive<?php endif; ?>">
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/filter_button.tpl', 'smarty_include_vars' => array('filter' => 'unpublish','label' => ((is_array($_tmp='COM_ADS_UNPUBLISHED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            </li>
                        <?php elseif ($this->_tpl_vars['task'] == 'watchlist'): ?>
                            <li class="<?php if (( $this->_tpl_vars['filter_archive'] == 'active' || $this->_tpl_vars['filter_archive'] == '' )): ?>active<?php else: ?>inactive<?php endif; ?>">
                                <a class = "<?php if (( $this->_tpl_vars['filter_archive'] == 'active' )): ?>active<?php else: ?>inactive<?php endif; ?>" href = '<?php echo $this->_plugins['function']['jroute'][0][0]->smarty_jroute(array('link' => "index.php?option=com_adsfactory&Itemid=".($this->_tpl_vars['Itemid'])."&task=".($this->_tpl_vars['task'])."&filter_archive=active"), $this);?>
'>
                                    <span class="text_header_small <?php if ($this->_tpl_vars['filter_archive'] == 'active'): ?>active<?php endif; ?>"><?php echo ((is_array($_tmp='COM_ADS_ACTIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span></a>
                            </li>
                            <li class="<?php if (( $this->_tpl_vars['filter_archive'] == 'expired' )): ?>active<?php else: ?>inactive<?php endif; ?>">
                                <a class = "<?php if (( $this->_tpl_vars['filter_archive'] == 'expired' )): ?>active<?php else: ?>inactive<?php endif; ?>" href = '<?php echo $this->_plugins['function']['jroute'][0][0]->smarty_jroute(array('link' => "index.php?option=com_adsfactory&Itemid=".($this->_tpl_vars['Itemid'])."&task=".($this->_tpl_vars['task'])."&filter_archive=expired"), $this);?>
'>
                                    <span class="text_header_small <?php if ($this->_tpl_vars['filter_archive'] == 'expired'): ?>active<?php endif; ?>"><?php echo ((is_array($_tmp='COM_ADS_EXPIRED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span></a>
                            </li>
                            <li class="<?php if (( $this->_tpl_vars['filter_archive'] == 'archive' )): ?>active<?php else: ?>inactive<?php endif; ?>">
                                <a class = "<?php if (( $this->_tpl_vars['filter_archive'] == 'archive' )): ?>active<?php else: ?>inactive<?php endif; ?>" href = '<?php echo $this->_plugins['function']['jroute'][0][0]->smarty_jroute(array('link' => "index.php?option=com_adsfactory&Itemid=".($this->_tpl_vars['Itemid'])."&task=".($this->_tpl_vars['task'])."&filter_archive=archive"), $this);?>
'>
                                    <span class="text_header_small <?php if ($this->_tpl_vars['filter_archive'] == 'archive'): ?>active<?php endif; ?>"><?php echo ((is_array($_tmp='COM_ADS_ARCHIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span></a>
                            </li>
                        <?php else: ?>
                            <li class="<?php if (( $this->_tpl_vars['filter_archive'] == 'active' || $this->_tpl_vars['filter_archive'] == '' )): ?>active<?php else: ?>inactive<?php endif; ?>">
                                <a class="<?php if (( $this->_tpl_vars['filter_archive'] == 'active' )): ?>active<?php else: ?>inactive<?php endif; ?>" href='<?php echo $this->_plugins['function']['jroute'][0][0]->smarty_jroute(array('link' => "index.php?option=com_adsfactory&Itemid=".($this->_tpl_vars['Itemid'])."&task=".($this->_tpl_vars['task'])."&filter_archive=active"), $this);?>
'>
                               <span class="text_header_small <?php if ($this->_tpl_vars['filter_archive'] == 'active'): ?>active<?php endif; ?>"><?php echo ((is_array($_tmp='COM_ADS_ACTIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span></a>
                            </li>
                            <li class="<?php if (( $this->_tpl_vars['filter_archive'] == 'expired' )): ?>active<?php else: ?>inactive<?php endif; ?>">
                                <a class="<?php if (( $this->_tpl_vars['filter_archive'] == 'expired' )): ?>active<?php else: ?>inactive<?php endif; ?>" href='<?php echo $this->_plugins['function']['jroute'][0][0]->smarty_jroute(array('link' => "index.php?option=com_adsfactory&Itemid=".($this->_tpl_vars['Itemid'])."&task=".($this->_tpl_vars['task'])."&filter_archive=expired"), $this);?>
'>
                               <span class="text_header_small <?php if ($this->_tpl_vars['filter_archive'] == 'expired'): ?>active<?php endif; ?>"><?php echo ((is_array($_tmp='COM_ADS_EXPIRED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span></a>
                            </li>
                            <li class="<?php if (( $this->_tpl_vars['filter_archive'] == 'archive' )): ?>active<?php else: ?>inactive<?php endif; ?>">
                                <a class="<?php if (( $this->_tpl_vars['filter_archive'] == 'archive' )): ?>active<?php else: ?>inactive<?php endif; ?>" href='<?php echo $this->_plugins['function']['jroute'][0][0]->smarty_jroute(array('link' => "index.php?option=com_adsfactory&Itemid=".($this->_tpl_vars['Itemid'])."&task=".($this->_tpl_vars['task'])."&filter_archive=archive"), $this);?>
'>
                               <span class="text_header_small <?php if ($this->_tpl_vars['filter_archive'] == 'archive'): ?>active<?php endif; ?>"><?php echo ((is_array($_tmp='COM_ADS_ARCHIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span></a>
                            </li>
                        <?php endif; ?>
                        </ul>
                      </div>
                    </div>
            	</div>
            </nav>
        </div>
    </div>
    <div style="clear: both;"></div>

    <div class="header_filters">
        <div class="header_filters_left">
          <a href="javascript:void(0);" style="cursor:pointer;" onclick="adObject.viewStyleAds('list');">
                  <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
list.png" title = "<?php echo ((is_array($_tmp='COM_ADS_VIEW_LISTVIEW')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt = "<?php echo ((is_array($_tmp='COM_ADS_VIEW_LISTVIEW')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" height="32" class="ads_noborder text_header_small" /></a>
          <a href="javascript:void(0);" style="cursor:pointer;" onclick="adObject.viewStyleAds('grid');">
                  <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
grid.png" title = "<?php echo ((is_array($_tmp='COM_ADS_VIEW_GRIDVIEW')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt = "<?php echo ((is_array($_tmp='COM_ADS_VIEW_GRIDVIEW')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" height="32" class="ads_noborder text_header_small" /></a>
        </div>

        <div class="header_filters_right text_header_small">
			<select name="filter_order" onchange="adObject.submitListForm(this.value,'<?php echo $this->_tpl_vars['reverseorder_Dir']; ?>
');" style="width: 115px;" class="pull-left input-medium">
                <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['filter_ids'],'output' => $this->_tpl_vars['filter_names'],'selected' => $this->_tpl_vars['selected_id']), $this);?>

            </select>
            <?php if ($this->_tpl_vars['selected_id'] != ""): ?>
                 <?php if ($this->_tpl_vars['filter_order_Dir'] == 'ASC'): ?>
                    <a href="javascript:void(0);" style="cursor:pointer;" onclick="adObject.submitListForm('<?php echo $this->_tpl_vars['selected_id']; ?>
','DESC');"><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
arrow-up_br.png" height="24" class="ads_noborder text_header_small" alt="<?php echo ((is_array($_tmp='COM_ADS_ASCENDING')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" /></a>
                 <?php else: ?>
                    <a href="javascript:void(0);" style="cursor:pointer;" onclick="adObject.submitListForm('<?php echo $this->_tpl_vars['selected_id']; ?>
','ASC');"><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
arrow-down_br.png" height="24" class="ads_noborder text_header_small" alt="<?php echo ((is_array($_tmp='COM_ADS_DESCENDING')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" /></a>
                 <?php endif; ?>
             <?php else: ?>
                    <a href="javascript:void(0);" style="cursor:pointer;" onclick="adObject.submitListForm('<?php echo $this->_tpl_vars['selected_id']; ?>
','ASC');"><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
arrow-up_br.png" height="24" class="ads_noborder text_header_small" alt="<?php echo ((is_array($_tmp='COM_ADS_ASCENDING')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" /></a>
             <?php endif; ?>

            <span class="text_header_small"><?php echo $this->_tpl_vars['pagination']->getLimitBox(); ?>
</span>
        </div>
    </div>
    <div style="clear: both;"></div>

</div>