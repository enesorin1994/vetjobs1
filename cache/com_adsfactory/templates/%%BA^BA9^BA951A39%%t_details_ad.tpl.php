<?php /* Smarty version 2.6.28, created on 2017-03-16 14:48:15
         compiled from t_details_ad.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_details_ad.tpl', 3, false),array('function', 'positions', 't_details_ad.tpl', 39, false),array('function', 'printprice', 't_details_ad.tpl', 64, false),array('function', 'jtext', 't_details_ad.tpl', 77, false),array('modifier', 'translate', 't_details_ad.tpl', 16, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_countdown.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>


    <div class="width100">
        <?php if ($this->_tpl_vars['payment_items_header']): ?>
            <div class = "ads_headerinfo" xmlns = "http://www.w3.org/1999/html"><?php echo $this->_tpl_vars['payment_items_header']; ?>
</div>
        <?php endif; ?>
    </div>
<!-- [+]Add Container -->

    <div class="addContainer">
        <div class="ad_details span12 grey_line">
                        <span class="ads_inline">
                <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
folder.png" title = "<?php echo ((is_array($_tmp='COM_ADS_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt = "<?php echo ((is_array($_tmp='COM_ADS_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" style = "vertical-align: middle;" />
                <?php if ($this->_tpl_vars['ad']->get('catname')): ?>
                	<?php echo $this->_tpl_vars['ad']->get('CategoryBreadcrumb'); ?>

                <?php else: ?>&nbsp;-&nbsp;<?php endif; ?>
            </span>
            <span class="ads_inline pull-right"><a href = "<?php echo $this->_tpl_vars['ad']->get('links.ads_listing'); ?>
"><?php echo ((is_array($_tmp='COM_ADS_BACK_TO_LIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a></span>
		</div>
        <div style="clear: both;"></div>
        <div class="ad_details span7 ads_right10">
                        <div class = "box_content">
                <div class = "ad_title_details box_title">
                    <?php if ($this->_tpl_vars['ad']->isMyAd() && ! $this->_tpl_vars['ad']->published): ?>
                        <span class = "padding0 ad_title_details"><i><?php echo $this->_tpl_vars['ad']->title; ?>
</i> (<?php echo ((is_array($_tmp='COM_ADS_UNPUBLISHED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
)</span>
                    <?php else: ?>
                        <span class = "padding0 ad_title_details"><?php echo $this->_tpl_vars['ad']->title; ?>
</span>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['ad']->ad_type == @ADS_TYPE_PRIVATE): ?>
                            (<?php echo ((is_array($_tmp='COM_ADS_PRIVATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
)
                    <?php endif; ?>
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/details/t_action_buttons.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                </div>
                <div class="ad_details ads_position">
            	<?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => 'header','item' => $this->_tpl_vars['ad'],'page' => 'ads'), $this);?>

                </div>
        	</div>

            <div class="box_title">
                                <div class="adsfactory_description">
                    <div class="add_detail_fields">
                       <?php echo $this->_tpl_vars['ad']->description; ?>

                    </div>
                    <span class = "v_spacer_5"></span>
                </div>
            </div>
            <div class="ad_details ads_position">
            <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "detail-left",'item' => $this->_tpl_vars['ad'],'page' => 'ads'), $this);?>

            </div>
        </div>

            	<div class="ad_details span4">
            <div class = "box_content ad_details_price">
				<span class="pull-right right">
					<?php if ($this->_tpl_vars['ad']->askprice): ?>
                        <span class="padding0">
                            <?php if (is_numeric ( $this->_tpl_vars['ad']->askprice )): ?>
                                <span class="askprice"><?php echo $this->_plugins['function']['printprice'][0][0]->smarty_print_price(array('price' => $this->_tpl_vars['ad']->askprice,'currency' => $this->_tpl_vars['ad']->currency), $this);?>
</span>
                            <?php else: ?>
                                <span class="askprice"><?php echo $this->_tpl_vars['ad']->askprice; ?>
</span>
                            <?php endif; ?>
                        </span>
                    <?php else: ?>
                        &nbsp;
                    <?php endif; ?>
				</span>
            </div>

			<div class="box_content">
                <div class="width100left">
                    <legend><span class="page-header"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_AD_DETAILS'), $this);?>
</span></legend>
                    <span>
                        <label><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_START_DATE'), $this);?>
: </label> <?php echo $this->_tpl_vars['ad']->get('startdate_text'); ?>

                    </span>
                    <div>
	                    <?php if ($this->_tpl_vars['ad']->closed): ?>
	                        <span class="pull-right">
	                            <h4><span class="closed"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_AD_CLOSED_ON'), $this);?>
: <?php if ($this->_tpl_vars['ad']->get('closed_date_text')): ?> <?php echo $this->_tpl_vars['ad']->get('closed_date_text'); ?>
 <?php endif; ?></span></h4>
	                        </span>
	                    <?php elseif ($this->_tpl_vars['ad']->get('expired')): ?>
	                        <span class="pull-right">
	                            <span class='expired'><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_EXPIRED'), $this);?>
: </span>(<?php echo $this->_tpl_vars['ad']->get('enddate_text'); ?>
)
	                        </span>
	                    <?php else: ?>
                        	<span>
								<?php if ($this->_tpl_vars['cfg']->enable_countdown): ?>
                            		<label><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_EXPIRES_IN'), $this);?>
: </label><span class="timer" id="time1"> <?php echo $this->_tpl_vars['ad']->get('countdown'); ?>
</span>
		                        <?php else: ?>
                            		<label><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_END_DATE'), $this);?>
: </label> <span class=''> <?php echo $this->_tpl_vars['ad']->get('enddate_text'); ?>
</span>
                        		<?php endif; ?>
							</span>
                    	<?php endif; ?>
					</div>
					<div>
                    	<?php if ($this->_tpl_vars['ad']->ad_city): ?>
                        	<span class="pull-left">
								<label><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_AD_CITY'), $this);?>
: </label><span class="padding0"> <?php echo $this->_tpl_vars['ad']->ad_city; ?>
</span>
                    		</span>	
						<?php endif; ?>
					</div>
                    <div>
                    	<?php if ($this->_tpl_vars['ad']->ad_postcode): ?>
							<span class="pull-left">
								<label><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_AD_POSTCODE'), $this);?>
: </label><span class="padding0"> <?php echo $this->_tpl_vars['ad']->ad_postcode; ?>
</span>
							</span>                    		
						<?php endif; ?>
					</div>
                </div>
            </div>
			        	<div class="box_content ads_top_align">
            	<div class="width100left">
            		<legend><span class="page-header"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_POSTED_BY'), $this);?>
</span></legend>
					<div class="pull-left" style="min-width: 200px;">
                    	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/details/t_details_add_user.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
				</div>
			</div>
			<div style="clear: both;"></div>
            <div class="detail_right_position">
                <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "detail-right",'item' => $this->_tpl_vars['ad'],'page' => 'ads'), $this);?>

            </div>
		</div>
        <div style="clear: both;"></div>
		
		<div class = "ad_details span11">
			<div class="add_detail_socialtoolbar add_newadd_header_selected">
              	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/details/t_details_add_social.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
              	<span class="add_socialbutton">
                 	<?php if ($this->_tpl_vars['ad']->get('imagecount') > 1): ?>
                     	<img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
morepics_on.png" class="ads_noborder detail_img_social" title="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_MORE_PICTURES'), $this);?>
" alt="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_MORE_PICTURES'), $this);?>
" />
                 	<?php endif; ?>
              	</span>
             	<?php if ($this->_tpl_vars['cfg']->enable_attach == '1' && $this->_tpl_vars['ad']->atachment != ""): ?>
	                 <span class="add_socialbutton">
	                     <a href = "<?php echo $this->_tpl_vars['ad']->get('links.download_file'); ?>
" target = "_blank">
	                         <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
downloads_on_24.png" title="<?php echo $this->_tpl_vars['ad']->atachment; ?>
" alt="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_HAS_DOWNLOADS'), $this);?>
" class="ads_noborder detail_img_social" />
	                     </a>
	                 </span>
	             <?php endif; ?>
                 <span class="add_socialbutton">
                     <a class="modal" href="index.php?option=com_adsfactory&amp;task=tellFriend_show&id=<?php echo $this->_tpl_vars['ad']->id; ?>
&tmpl=component" <?php echo ' rel="{handler: \'iframe\', size: {x: 550, y: 450}}" '; ?>
>
                         <img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsfactory/templates/default/images/tell_a_friend.png" alt="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_TELL_FRIEND'), $this);?>
" title="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_TELL_FRIEND'), $this);?>
" class="ads_noborder detail_img_social" />
                     </a>
                 </span>
             </div>
        </div>
        <span class = "v_spacer_15"></span>
        <div class = "ad_details_tab span12">

                    <div class="pull-left ads_top_align" style="width: 95%;">
                <!-- [+] Tabbed Panes -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/details/t_details_add_tab.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <!-- [-] Tabbed Panes -->
            </div>
        </div>

 <!-- next row -->
	<div style="clear:both;" ></div>
    <span class = "v_spacer_15 grey_line"></span>
    <div class="ad_details span12 ad_details_tag">
        <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => 'footer','item' => $this->_tpl_vars['ad'],'page' => 'ads'), $this);?>

    </div>

    <?php if ($this->_tpl_vars['ad']->get('links.tags') != ''): ?>
        <br />
        <div class="span12 ad_details_tag">
            <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
tags.png" title="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_TAGS'), $this);?>
" alt="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_TAGS'), $this);?>
" class="ads_noborder" />
            <span class="ad_tag1"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_TAGS'), $this);?>
: <?php echo $this->_tpl_vars['ad']->get('links.tags'); ?>
</span>
        </div>
    <?php endif; ?>

	<div style="clear:both;"></div>
</div>

<!-- [-]Add Container -->

<!-- next row -->
<div style="clear:both;"></div>