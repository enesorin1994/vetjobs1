<?php /* Smarty version 2.6.28, created on 2017-03-16 14:48:16
         compiled from elements/details/t_details_add_user.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 'elements/details/t_details_add_user.tpl', 32, false),array('function', 'jtext', 'elements/details/t_details_add_user.tpl', 62, false),)), $this); ?>
<?php if ($this->_tpl_vars['cfg']->ads_logged_posting && ! $this->_tpl_vars['ad']->userid): ?>

	<?php if ($this->_tpl_vars['ad']->ad_type == @ADS_TYPE_PUBLIC): ?>
		<?php if ($this->_tpl_vars['ad']->userid): ?>
			<div class="padding0">
				<span class="ads_padding15"><?php echo $this->_tpl_vars['user']->name; ?>
 (<?php echo $this->_tpl_vars['user']->username; ?>
)</span>
            </div>
		<?php else: ?>
            <div class="padding0">
				<span class="ads_padding15"><?php echo $this->_tpl_vars['ad']->guest_username; ?>
</span>
            </div>
            <div class="padding0">
				<span class="ads_padding15"><?php echo $this->_tpl_vars['ad']->guest_email; ?>
</span>
            </div>
		<?php endif; ?>
	<?php else: ?>
        <?php if ($this->_tpl_vars['ad']->guest_username != ''): ?>
            <div class="padding0">
				<span class="ads_padding15"><?php echo $this->_tpl_vars['ad']->guest_username; ?>
</span>
            </div>
        <?php endif; ?>
	<?php endif; ?>
<?php else: ?>
    <?php if ($this->_tpl_vars['lists']['contactEnabled'] == 0): ?>
		<?php if ($this->_tpl_vars['ad']->ad_type == @ADS_TYPE_PUBLIC): ?>
            <div class="padding0">
                <span class="ads_padding15 ads_top_align">
                    <a href="<?php echo $this->_tpl_vars['ad']->get('links.user_profile'); ?>
"><?php echo $this->_tpl_vars['user']->username; ?>
</a>
                    <a href="<?php echo $this->_tpl_vars['ad']->get('links.otherads'); ?>
">(<?php echo $this->_tpl_vars['count_user_ads']; ?>
)</a>
                    &nbsp;&nbsp;
                    <a target="_blank" href="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_adsfactory&view=adsfactory&format=feed&userid=<?php echo $this->_tpl_vars['ad']->userid; ?>
">
                        <img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsfactory/images/livemarks.png" class="pull-right ads_noborder" alt="<?php echo ((is_array($_tmp='COM_ADS_RSS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" />
                    </a>
                </span>
        	</div>
            <?php if ($this->_tpl_vars['cfg']->show_contact): ?>
                <?php if ($this->_tpl_vars['user']->name != '' || $this->_tpl_vars['user']->surname != ''): ?>
                    <div>
                        <span class="ads_padding15"><?php echo $this->_tpl_vars['user']->name; ?>
 <?php echo $this->_tpl_vars['user']->surname; ?>
</span>
                    </div>
                    <br />
                <?php endif; ?>
                <?php if ($this->_tpl_vars['user']->address != ''): ?>
                    <div>
                        <span class="ads_padding15"><?php echo $this->_tpl_vars['user']->address; ?>
</span>
                    </div>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['user']->city != ''): ?>
                    <div>
                        <span class="ads_padding15"><?php echo $this->_tpl_vars['user']->city; ?>
</span>
                    </div>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['user']->state != '' || $this->_tpl_vars['user']->country != ''): ?>
                    <div>
                        <span class="ads_padding15"><?php echo $this->_tpl_vars['user']->state; ?>
 <?php echo $this->_tpl_vars['user']->country; ?>
</span>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
		<?php else: ?>
			<?php if ($this->_tpl_vars['ad']->ad_type == @ADS_TYPE_PRIVATE && $this->_tpl_vars['userid'] != $this->_tpl_vars['ad']->userid): ?>
                <div>
					<span class="ads_padding15 ads_top_align"><span class="add_text_medium_bold"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_PRIVATE_AD_WARNING'), $this);?>
</span></span>
				</div>
			<?php endif; ?>	
		<?php endif; ?>

	<?php else: ?>         <div class="padding0">
            <span class="ads_padding15"><?php echo $this->_tpl_vars['user']->name; ?>
</span>
        </div>
        <div>
            <span class="ads_padding15"><?php echo $this->_tpl_vars['user']->surname; ?>
</span>
        </div>
        <div>
            <span class="ads_padding15"><?php echo $this->_tpl_vars['user']->address; ?>
</span>
        </div>
        <div>
            <span class="ads_padding15"><?php echo $this->_tpl_vars['user']->city; ?>
</span>
        </div>
        <div>
            <span class="ads_padding15"><?php echo $this->_tpl_vars['user']->state; ?>
</span>
        </div>
        <div>
            <span class="ads_padding15"><?php echo $this->_tpl_vars['user']->country; ?>
</span>
        </div>
	<?php endif; ?>


<?php endif; ?>