<?php /* Smarty version 2.6.28, created on 2017-03-16 14:48:50
         compiled from t_editad.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_editad.tpl', 2, false),array('function', 'import_js_file', 't_editad.tpl', 3, false),array('function', 'jtext', 't_editad.tpl', 32, false),array('function', 'infobullet', 't_editad.tpl', 91, false),array('function', 'printdate', 't_editad.tpl', 137, false),array('modifier', 'translate', 't_editad.tpl', 78, false),array('modifier', 'default', 't_editad.tpl', 241, false),array('modifier', 'count', 't_editad.tpl', 312, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "Stickman.MultiUpload.js"), $this);?>


<?php $this->assign('fieldRequired', $this->_tpl_vars['lists']['fieldRequiredlink']); ?>

<div>
    <?php if ($this->_tpl_vars['payment_items_header']): ?>
        <div class = "ads_headerinfo" xmlns = "http://www.w3.org/1999/html"><?php echo $this->_tpl_vars['payment_items_header']; ?>
</div>
    <?php endif; ?>
</div>

<div>
    <form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=<?php echo $this->_tpl_vars['option']; ?>
&task=save&id=<?php echo $this->_tpl_vars['ad']->id; ?>
" method="post" name="adsForm" id="adsForm" enctype="multipart/form-data" class="form-validate" onsubmit="return adObject.ApplicationFormValidate(this);">
    <input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />
    <input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['ad']->id; ?>
" />
    <input type="hidden" name="task" value="save" />
    <input type="hidden" name="oldid" value="<?php echo $this->_tpl_vars['oldid']; ?>
" />
    <input type="hidden" name="oldpic" value="<?php echo $this->_tpl_vars['oldpic']; ?>
" />
    <input type = "hidden" name = "has_custom_fields_with_cat" value = "<?php echo $this->_tpl_vars['custom_fields_with_cat']; ?>
" />
    <?php echo $this->_tpl_vars['form_token']; ?>


    <?php if (( $this->_tpl_vars['ad']->id && $this->_tpl_vars['ad']->featured != 'none' ) || $this->_tpl_vars['lists']['listingEnabled']): ?>
        <div class = "ads_headerinfo"><?php echo $this->_tpl_vars['edit_24hours_info']; ?>
</div>
    <?php endif; ?>

    <div class="componentheading">
        <h3 class="page-header item-title">
          <?php if ($this->_tpl_vars['ad']->id): ?>
                <span class="page_heading_detail"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_PAGE_EDIT'), $this);?>
 </span>
            <?php if ($this->_tpl_vars['ad']->published): ?>
              <span class="page_heading_status"> ( <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_PAGE_PUBLISHED'), $this);?>
 ) </span>
            <?php else: ?>
              <span class="page_heading_status"> ( <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_PAGE_UNPUBLISHED'), $this);?>
 ) </span>
            <?php endif; ?>
          <?php elseif ($this->_tpl_vars['oldid']): ?>
            <span class="page_heading_detail"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_PAGE_REPUBLISH'), $this);?>
 </span>
          <?php else: ?>
            <span class="page_heading_detail"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_PAGE_ADD_NEW_AD'), $this);?>
 </span>
          <?php endif; ?>
        </h3>
    </div>

    <div id="invalid_info"></div>
	<div class="span12 form-horizontal ads_top_align">
        <div class="control-group">
            <div class="control-label ad_lables">
                <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_TITLE'), $this);?>
<?php echo $this->_tpl_vars['fieldRequired']; ?>

            </div>
            <div class="controls">
                <?php if ($this->_tpl_vars['allow_edit'] == 1): ?>
                    <input class="inputbox required input-xlarge" type="text" id="title" name="title" value="<?php echo $this->_tpl_vars['ad']->title; ?>
" size="80" />
                <?php else: ?>
                    <?php echo $this->_tpl_vars['ad']->title; ?>

                    <input type="hidden" name="title" value="<?php echo $this->_tpl_vars['ad']->title; ?>
" />
                <?php endif; ?>
                <span class="validate_tip" id="tip_title"></span>
            </div>
        </div>
        <div class="control-group">
			<label class = "control-label ad_lables" for="category"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_CATEGORY'), $this);?>
<?php echo $this->_tpl_vars['fieldRequired']; ?>
</label>
            <div class="controls">
                <?php if ($this->_tpl_vars['task'] == 'editad' && $this->_tpl_vars['ad']->published == 1): ?>
                    <?php echo $this->_tpl_vars['ad']->get('CategoryPathString'); ?>

                        <input type = "hidden" name = "cat" value = "<?php echo $this->_tpl_vars['ad']->cat; ?>
">
                <?php else: ?>
                    <?php echo $this->_tpl_vars['lists']['cats']; ?>

                <?php endif; ?>
            </div>
        </div>
                <?php if ($this->_tpl_vars['cfg']->adtype_enable == 0 && $this->_tpl_vars['cfg']->adtype_val != ''): ?>
            <?php echo $this->_tpl_vars['lists']['adtype']; ?>

        <?php else: ?>
            <div class="control-group">
				<label class = "control-label ad_lables"><?php echo ((is_array($_tmp='COM_ADS_TYPE_OF_AD')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php echo $this->_tpl_vars['fieldRequired']; ?>
</label>
                <div class="controls">
                	<?php if ($this->_tpl_vars['cfg']->adtype_enable || $this->_tpl_vars['ad']->id): ?>
                    	<?php if ($this->_tpl_vars['task'] == 'editad' && $this->_tpl_vars['ad']->published == 1): ?>
	                        <strong>
    	                        <?php if ($this->_tpl_vars['ad']->ad_type == @ADS_TYPE_PRIVATE): ?>
        	                            <?php echo ((is_array($_tmp='COM_ADS_PRIVATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

            	                <?php else: ?>
                	                    <?php echo ((is_array($_tmp='COM_ADS_PUBLIC')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                    	        <?php endif; ?>
                        	</strong>
                    	<?php else: ?>
                        	<?php echo $this->_tpl_vars['lists']['adtype']; ?>

                        	<span class="pull-left1">&nbsp;<?php echo $this->_plugins['function']['infobullet'][0][0]->smarty_infobullet(array('text' => ((is_array($_tmp='COM_ADS_AD_TYPE_HELP')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>
&nbsp;</span>
                    	<?php endif; ?>
                	<?php else: ?>
                    	<strong>
                        	<?php if ($this->_tpl_vars['cfg']->adtype_val == @ADS_TYPE_PRIVATE): ?>
                            	<?php echo ((is_array($_tmp='COM_ADS_PRIVATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                        	<?php else: ?>
	                            <?php echo ((is_array($_tmp='COM_ADS_PUBLIC')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

    	                    <?php endif; ?>
        	            </strong>
            	    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

		<?php if ($this->_tpl_vars['cfg']->adpublish_enable == 0 && $this->_tpl_vars['cfg']->adpublish_val != ''): ?>
			<?php echo $this->_tpl_vars['lists']['published']; ?>

		<?php else: ?>
            <div class="control-group">
				<label class = "control-label ad_lables" for="published"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_PUBLISHED'), $this);?>
</label>
				<?php echo $this->_tpl_vars['lists']['published']; ?>

            </div>
		<?php endif; ?>
    </div>
    <div class="span12 form-horizontal">
        <div class="control-group">
			<label class = "control-label ad_lables" for="short_description"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_SHORT_DESCRIPTION'), $this);?>
</label>
            <div class="controls">
                <textarea id="short_description" name="short_description" class="inputbox"><?php echo $this->_tpl_vars['ad']->short_description; ?>
</textarea>(<span id='descr_counter'>0</span>/<?php echo $this->_tpl_vars['cfg']->ads_short_desc_long; ?>
 chars)
            </div>
		</div>
		<?php if ($this->_tpl_vars['cfg']->ads_price_enable == 1): ?>
        	<div class="control-group">
            	<label class = "control-label ad_lables" for="askprice"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ASKPRICE'), $this);?>
</label>
	            <div class="controls">
    	            &nbsp;<input class="inputbox <?php if ($this->_tpl_vars['cfg']->ads_price_compulsory == 1): ?> required <?php endif; ?> validate-numeric input-medium" type="text" id="askprice" name="askprice" value="<?php echo $this->_tpl_vars['ad']->askprice; ?>
" size="10" style="text-align: right !important;" />
        	        <span class="pull-left"><?php echo $this->_tpl_vars['lists']['currency']; ?>
 </span>
            	</div>
			</div>
		<?php endif; ?>
				<?php if ($this->_tpl_vars['time_limited'] == 0): ?>
        	<div class="control-group">
                <label class = "control-label ad_lables" for="start_date"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_START_DATE'), $this);?>
</label>
	           <div class="controls">
					<?php if ($this->_tpl_vars['ad']->id && $this->_tpl_vars['allow_edit'] == 0): ?>
						<span class="required"><?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['ad']->start_date), $this);?>
</span>
						<input class="text_area" type="hidden" name="start_date" id="start_date" size="15" maxlength="19" value="<?php echo $this->_tpl_vars['ad']->start_date; ?>
" alt="start_date"/>
					<?php else: ?>
	                    <span class="required"><?php echo $this->_tpl_vars['lists']['start_date_html']; ?>
</span>
					<?php endif; ?>
	            </div>
			</div>
        	<div class="control-group">
                <label class = "control-label ad_lables" for="end_date"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_END_DATE'), $this);?>
</label>
	            <div class="controls">
					<?php if ($this->_tpl_vars['ad']->id && $this->_tpl_vars['allow_edit'] == 0): ?>
						<span class="required"><?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['ad']->end_date,'use_hour' => 1), $this);?>
</span>
						<input class="text_area" type="hidden" name="end_date" id="end_date" size="15" maxlength="19" value="<?php echo $this->_tpl_vars['ad']->end_date; ?>
" alt="end_date"/>
					<?php else: ?>

						<span class="required"><?php echo $this->_tpl_vars['lists']['end_date_html']; ?>
&nbsp;</span><span class="pull-left"><?php echo $this->_tpl_vars['lists']['tip_max_availability']; ?>
&nbsp;&nbsp;</span>
						
						<?php if ($this->_tpl_vars['cfg']->enable_hour): ?>
							<span class="pull-right" style="padding-right: 110px;">
								<input name="end_hour" size="1" value="00" alt="" class="input-mini inputbox" /> :
								<input name="end_minutes" size="1" value="00" alt="" class="input-mini inputbox" />
							</span>
						<?php endif; ?>
					<?php endif; ?>
            	</div>
	 		</div>
	 	<?php else: ?>
        	<div class="control-group">
                <label class = "control-label ad_lables" for="end_date"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_AVAILABILITY'), $this);?>
<?php echo $this->_tpl_vars['fieldRequired']; ?>
</label>
            	<div class="controls">
                 	<input class="text_area" readonly type="hidden" name="start_date" id="start_date" size="15" maxlength="19" value="<?php echo $this->_tpl_vars['lists']['valab_now']; ?>
" alt="start_date" />
                 	<input class="text_area" type="hidden" name="end_date" id="end_date" size="15" maxlength="19" value="<?php echo $this->_tpl_vars['lists']['valab_end']; ?>
" alt="end_date" />

                	<?php if ($this->_tpl_vars['ad']->id): ?>
                    	<select id="ad_valability" name="ad_valability" onchange="document.getElementById('end_date').value=VList[this.value]; showValabilityPrice(VPriceList[this.value]);">
							<?php $_from = $this->_tpl_vars['lists']['valabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
	                            <?php if ($this->_tpl_vars['ad']->published): ?>
	                                	                                <?php if ($this->_tpl_vars['item'] == $this->_tpl_vars['lists']['db_no_of_days']): ?>
	                                    <option value="<?php echo $this->_tpl_vars['key']; ?>
" selected="selected"><?php echo $this->_tpl_vars['item']; ?>
 days</option>
	                                <?php endif; ?>
	                            <?php else: ?>
	                                	                                <?php if ($this->_tpl_vars['item'] == $this->_tpl_vars['lists']['db_no_of_days']): ?>
	                                    <option value="<?php echo $this->_tpl_vars['key']; ?>
" selected="selected"><?php echo $this->_tpl_vars['item']; ?>
 days</option>
	                                <?php else: ?>
	                                    <option value="<?php echo $this->_tpl_vars['key']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
 days</option>
	                                <?php endif; ?>
	                            <?php endif; ?>
							<?php endforeach; endif; unset($_from); ?>
						</select>
                    	<?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['ad']->start_date), $this);?>
 - <?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['ad']->end_date,'use_hour' => 1), $this);?>

	                <?php else: ?>
						<select id="ad_valability" name="ad_valability" onchange="document.getElementById('end_date').value=VList[this.value]; showValabilityPrice(VPriceList[this.value]);">
							<?php $_from = $this->_tpl_vars['lists']['valabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
								<option value="<?php echo $this->_tpl_vars['key']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
 days</option>
							<?php endforeach; endif; unset($_from); ?>
						</select>
						<span id="pricing_info" style="padding:2px; color:#FF0000;"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_PRICE_LISTING_VALABILITY'), $this);?>
 <?php echo $this->_tpl_vars['valabs_prices'][$this->_tpl_vars['valabs_default_val']]; ?>
 <?php echo $this->_tpl_vars['valabs_currency']; ?>
 <?php echo ((is_array($_tmp='COM_ADS_PACKAGE_LISTING')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span>
	                <?php endif; ?>
            	</div>
	 		</div>
	 	<?php endif; ?>
		
        <div class="control-group">
			<label class = "control-label ad_lables" for="description"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_DESCRIPTION'), $this);?>
<?php echo $this->_tpl_vars['fieldRequired']; ?>
</label>
            <div class="controls">
			    <span class="required"><?php echo $this->_tpl_vars['lists']['description']; ?>
</span>
            </div>
		</div>

        <?php if ($this->_tpl_vars['cfg']->google_key != ""): ?>
            <div class="control-group">
				<label class = "control-label ad_lables"><?php echo ((is_array($_tmp='COM_ADS_GOOGLE_MAP')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                <div class="controls">
        		    <?php if ($this->_tpl_vars['user']->googleMaps_x != "" && $this->_tpl_vars['user']->googleMaps_y != ""): ?>
                            <a href = "#"
                               onclick = "document.getElementById('googleX').value='<?php echo $this->_tpl_vars['user']->googleMaps_x; ?>
';document.getElementById('googleY').value='<?php echo $this->_tpl_vars['user']->googleMaps_y; ?>
'; return false;"><?php echo ((is_array($_tmp='COM_ADS_KEEP_PROFILE_COORDINATES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
                            |
        		    <?php endif; ?>
                    <a href = "#"
                       onclick = "window.open('index.php?option=com_adsfactory&controller=maps&tmpl=component&task=googlemap_tool','SelectGoogleMap','width=650,height=500');return false;"><?php echo ((is_array($_tmp='COM_ADS_SELECT_COORDINATES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
                </div>
            </div>

            <div class="control-group">
                <label class = "control-label ad_lables"><?php echo ((is_array($_tmp='ADS_GOOGLE_X_COORDINATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                <div class="controls">
                    <input class = "inputbox" type = "text" id = "googleX" name = "googlex" value = "<?php echo $this->_tpl_vars['ad']->googlex; ?>
" size = "20" />
                </div>
            </div>
            <div class="control-group">
                <label class = "control-label ad_lables"><?php echo ((is_array($_tmp='ADS_GOOGLE_Y_COORDINATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                <div class="controls">
                    <input class = "inputbox" type = "text" id = "googleY" name = "googley" value = "<?php echo $this->_tpl_vars['ad']->googley; ?>
" size = "20" />
                </div>
            </div>
            <?php if ($this->_tpl_vars['ad']->googlex != "" && $this->_tpl_vars['ad']->googley != ""): ?>
            	<div class="control-group">
	                <div class="control-label">&nbsp;</div>
	                <div class="controls1">
	                	<br />
	                   	<iframe src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_adsfactory&controller=maps&amp;tmpl=component&amp;task=googlemap&amp;x=<?php echo $this->_tpl_vars['ad']->googlex; ?>
&amp;y=<?php echo $this->_tpl_vars['ad']->googley; ?>
"
							style="margin-left: 5px; height: 100%; width: 98%; max-width:<?php echo ((is_array($_tmp=@$this->_tpl_vars['cfg']->googlemap_gx+15)) ? $this->_run_mod_handler('default', true, $_tmp, '600') : smarty_modifier_default($_tmp, '600')); ?>
px; min-height: 350px !important; max-height:<?php echo ((is_array($_tmp=@$this->_tpl_vars['cfg']->googlemap_gy+20)) ? $this->_run_mod_handler('default', true, $_tmp, '4500') : smarty_modifier_default($_tmp, '4500')); ?>
px;"></iframe>	                           
	               	</div>
           		</div>
            <?php endif; ?>
        <?php endif; ?>

        <div class="control-group">
			<label class = "control-label ad_lables" for="ad_city"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_AD_CITY'), $this);?>
</label>
            <div class="controls">
                <input class="inputbox" type="text" id="ad_city" name="ad_city" value="<?php echo $this->_tpl_vars['ad']->ad_city; ?>
" size="20" />
            </div>
		</div>
        <div class="control-group">
			<label class = "control-label ad_lables" for="ad_postcode"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_AD_POSTCODE'), $this);?>
</label>
            <div class="controls">
                <input class="inputbox validate" type="text" id="ad_postcode" name="ad_postcode" value="<?php echo $this->_tpl_vars['ad']->ad_postcode; ?>
" size="20" />
            </div>
		</div>

        <?php if ($this->_tpl_vars['cfg']->enable_images): ?>
            <?php if ($this->_tpl_vars['ad']->picture): ?>
                <div class="control-group">
                    <div class="control-label">
                        <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_MAIN_PICTURE'), $this);?>

                    </div>

                    <div class="controls">
                        <span>
                            <img src="<?php echo $this->_tpl_vars['ADS_PICTURES']; ?>
/resize_<?php echo $this->_tpl_vars['ad']->picture; ?>
" />
                        </span>
                        <?php if ($this->_tpl_vars['allow_edit'] == 1): ?>
							<span class = "ad_dbk_c">
                                <input type="checkbox" id="delete_main_picture" name="delete_main_picture" value="1" />&nbsp;<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_DELETE_PICTURE'), $this);?>

                            </span>
                        <?php endif; ?>
                    </div>
                </div>
            <?php else: ?>
              <?php if ($this->_tpl_vars['allow_edit'] == 1): ?>
                <?php if ($this->_tpl_vars['lists']['imageEnabled'] == 0 || $this->_tpl_vars['main_img_free']): ?>
                    <div class="control-group">
                        <div class="control-label">
                            <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_MAIN_PICTURE'), $this);?>

                        </div>
                        <div class="controls">
                            <input type="file" name="picture" id="picture" />
                            <br /><small class="picture_max_size"><?php echo ((is_array($_tmp='COM_ADS_PICTURE_MAX_SIZE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo $this->_tpl_vars['cfg']->max_picture_size; ?>
kb</small>
                        </div>
                    </div>
                <?php else: ?>
                     <?php if ($this->_tpl_vars['lists']['imageEnabled'] == 1 && $this->_tpl_vars['isGuest'] == 0 && ! $this->_tpl_vars['images_already_payed']): ?>
                        <div class="control-group">
                            <div class="control-label">
                                <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_MAIN_PICTURE'), $this);?>
<?php if ($this->_tpl_vars['cfg']->main_picture_require == 1): ?><?php echo $this->_tpl_vars['fieldRequired']; ?>
 <span class="ads_required">*</span><?php endif; ?>
                            </div>
                            <div class="controls">
                                <?php if ($this->_tpl_vars['cfg']->main_picture_require == 1): ?>
                                    <input type="file" name="picture" id="picture" />
                                <?php else: ?>
                                    <input type="file" name="picture" id="picture" />
                                <?php endif; ?>
                                <br /><small class="picture_max_size"><?php echo ((is_array($_tmp='COM_ADS_PICTURE_MAX_SIZE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo $this->_tpl_vars['cfg']->max_picture_size; ?>
kb</small>
                            </div>
                        </div>
                     <?php endif; ?>
                <?php endif; ?>
              <?php else: ?>
                &nbsp;
              <?php endif; ?>
            <?php endif; ?>

            <?php if (count($this->_tpl_vars['ad']->get('images'))): ?>
                <?php $this->assign('images', $this->_tpl_vars['ad']->get('images')); ?>
                <?php $_from = $this->_tpl_vars['images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['extraimages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['extraimages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['image']):
        $this->_foreach['extraimages']['iteration']++;
?>
                    <div class="control-group">
                        <div class="control-label">
                            <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_NEW_PICTURES'), $this);?>

                        </div>
                        <div class="controls">
                            <img src = "<?php echo $this->_tpl_vars['ADS_PICTURES']; ?>
/resize_<?php echo $this->_tpl_vars['image']->picture; ?>
" />
                            <?php if ($this->_tpl_vars['allow_edit'] == 1): ?>
                                <input type = "checkbox" name = "delete_pictures[]" value = "<?php echo $this->_tpl_vars['image']->id; ?>
" /> <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_DELETE_PICTURE'), $this);?>

                            <?php else: ?>
                               &nbsp;
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; endif; unset($_from); ?>
            <?php endif; ?>

            <?php if ($this->_tpl_vars['task'] == 'republish' && count($this->_tpl_vars['oldidPictures']) > 0): ?>
                <div class="control-group">
                	<div class="control-label"> </div>
                	<div class="controls">
                       <input type = "checkbox" name = "keep_republish_pictures" value = "1" checked="checked" />
                           <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_KEEP_PICTURES'), $this);?>

                    </div>
                </div>
                <?php $this->assign('images', $this->_tpl_vars['oldidPictures']); ?>
                <?php $_from = $this->_tpl_vars['images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['extraimages'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['extraimages']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['image']):
        $this->_foreach['extraimages']['iteration']++;
?>
                    <div class="control-group">
                        <div class="control-label"> </div>
                        <div class="controls">
                            <img src = "<?php echo $this->_tpl_vars['ADS_PICTURES']; ?>
/resize_<?php echo $this->_tpl_vars['image']->picture; ?>
" />
                        </div>
                    </div>
                <?php endforeach; endif; unset($_from); ?>
            <?php endif; ?>

		<?php if ($this->_tpl_vars['ad']->get('imagecount') < $this->_tpl_vars['cfg']->maxnr_images): ?>
          <?php if ($this->_tpl_vars['allow_edit'] == 1): ?>
			<?php if ($this->_tpl_vars['lists']['imageEnabled'] == 1): ?>
		  
		    	<?php if ($this->_tpl_vars['images_upload_free_no'] >= 1): ?>
		  
					<?php if ($this->_tpl_vars['images_upload_free_no'] > $this->_tpl_vars['ad']->get('imagecount') && $this->_tpl_vars['images_upload_free_no'] - $this->_tpl_vars['ad']->get('imagecount') <= $this->_tpl_vars['cfg']->maxnr_images): ?>
                        <div class="control-group">
                            <div class="control-label">
                                <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_NEW_PICTURES'), $this);?>

                            </div>
                            <div class="controls">
					 			<div id="files">
								<input class="inputbox" id="my_file_element1" type="file" name="pictures_1" />
								<div id="files_list1"></div>
								<?php echo '
								<script type="text/javascript">
									var max_img_upload = '; ?>
<?php echo $this->_tpl_vars['cfg']->maxnr_images; ?>
 - <?php echo $this->_tpl_vars['ad']->get('imagecount'); ?>
<?php echo ';
									var images_upload_free_no = '; ?>
<?php echo $this->_tpl_vars['images_upload_free_no']; ?>
 - <?php echo $this->_tpl_vars['ad']->get('imagecount'); ?>
<?php echo ';

									if (max_img_upload >= 0 && images_upload_free_no <= max_img_upload)
									{
                                        window.addEvent(\'domready\', function () {
                                            new MultiUpload($(\'my_file_element1\'), max_img_upload, null, true, true, root + \'components/com_adsfactory/images\');
                                        });
									}
									else {
										alert(\'You can not upload \'+ images_upload_free_no +\' pictures\');
									}
								</script>
								'; ?>

                                </div>
                                <small class="picture_max_size"><?php echo ((is_array($_tmp='COM_ADS_PICTURE_MAX_SIZE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo $this->_tpl_vars['cfg']->max_picture_size; ?>
kb</small>
                            </div>
					 	</div>
				 	<?php else: ?>
				   		<?php if ($this->_tpl_vars['images_upload_free_no'] > $this->_tpl_vars['ad']->get('imagecount') && $this->_tpl_vars['cfg']->maxnr_images <= $this->_tpl_vars['images_upload_free_no']): ?>
                            <div class="control-group">
                                <div class="control-label">
                                    <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_NEW_PICTURES'), $this);?>

                                </div>
                                <div class="controls">
							 		<div id="files">
										<input class="inputbox" id="my_file_element2" type="file" name="pictures_2" />
										<div id="files_list2"></div>
										<?php echo '
										<script type="text/javascript">
											var max_img_upload2 = '; ?>
<?php echo $this->_tpl_vars['cfg']->maxnr_images; ?>
 - <?php echo $this->_tpl_vars['ad']->get('imagecount'); ?>
<?php echo ';
							        
											if (max_img_upload2 >= 0)
											{
                                                window.addEvent(\'domready\', function () {
                                                    new MultiUpload($(\'my_file_element2\'), max_img_upload2, null, true, true, root + \'components/com_adsfactory/images\');
                                                });
											}
											else {
												alert(\'You can not upload \'+ max_img_upload2 +\' pictures\');
											}
										</script>
										'; ?>

                                	</div>
                                	<small class="picture_max_size"><?php echo ((is_array($_tmp='COM_ADS_PICTURE_MAX_SIZE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo $this->_tpl_vars['cfg']->max_picture_size; ?>
kb</small>
                                </div>
                            </div>
				   		<?php endif; ?>
			  		<?php endif; ?>
				<?php else: ?>
                    <?php if ($this->_tpl_vars['isGuest'] == 0 && ! $this->_tpl_vars['images_already_payed']): ?>
                        <div class="control-group">
                            <div class="control-label">
                                <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_NEW_PICTURES'), $this);?>

                            </div>
                            <div class="controls">
								<div id="files">
									<input class="inputbox" id="my_file_element3" type="file" name="pictures_3" />
									<div id="files_list3"></div>
									<?php echo '
									<script type="text/javascript">
									  var max_img_upload3 = '; ?>
<?php echo $this->_tpl_vars['cfg']->maxnr_images; ?>
-<?php echo $this->_tpl_vars['ad']->get('imagecount'); ?>
<?php echo ';
									  if (max_img_upload3 >= 0) {

                                          window.addEvent(\'domready\', function () {
                                              new MultiUpload($(\'my_file_element3\'), max_img_upload3, null, true, true, root + \'components/com_adsfactory/images\');
                                          });

									  } else {
											alert(\'You can not upload \'+ max_img_upload3 +\' pictures\');
									  }
									</script>
                                    '; ?>

								</div>
                                <small class="picture_max_size"><?php echo ((is_array($_tmp='COM_ADS_PICTURE_MAX_SIZE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo $this->_tpl_vars['cfg']->max_picture_size; ?>
kb</small>
                            </div>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			<?php else: ?>                 <div class="control-group">
                    <div class="control-label">
                        <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_NEW_PICTURES'), $this);?>

                    </div>
                    <div class="controls">
                        <div id="files">
                            <input class="inputbox" id="my_file_element" type="file" name="pictures" />
                            <div id="files_list"></div>
                            <?php echo '
                            <script type="text/javascript">
                                window.addEvent(\'domready\', function () {
                                    new MultiUpload($(\'my_file_element\'), '; ?>
<?php echo $this->_tpl_vars['cfg']->maxnr_images; ?>
-<?php echo $this->_tpl_vars['ad']->get('imagecount'); ?>
<?php echo ', null, true, true, root + \'components/com_adsfactory/images\');
                                });
                            </script>
                            '; ?>

                        </div>
                        <small class="picture_max_size"><?php echo ((is_array($_tmp='COM_ADS_PICTURE_MAX_SIZE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo $this->_tpl_vars['cfg']->max_picture_size; ?>
kb</small>
                    </div>
                </div>
			<?php endif; ?>
          <?php else: ?>
            &nbsp;
          <?php endif; ?>
		<?php endif; ?>

        <?php endif; ?>         <div><span>&nbsp;</span></div>
        <?php if ($this->_tpl_vars['cfg']->enable_attach == '1'): ?>
		 	<?php if ($this->_tpl_vars['ad']->atachment): ?>
	            <div class="control-group">
	                <div class="control-label">
	                    <?php if ($this->_tpl_vars['oldid']): ?>
	                        <a target="_blank" href="<?php echo $this->_tpl_vars['links']->getDownloadFileRoute($this->_tpl_vars['oldid'],'attach'); ?>
"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_DOWNLOAD'), $this);?>
</a>
	                    <?php else: ?>
	                        <a target="_blank" href="<?php echo $this->_tpl_vars['links']->getDownloadFileRoute($this->_tpl_vars['ad']->id,'attach'); ?>
"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_DOWNLOAD'), $this);?>
</a>
	                    <?php endif; ?>
	                </div>
	                <div class="controls">
	                    <?php if ($this->_tpl_vars['cfg']->attach_compulsory == '1'): ?>
	                        <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_REPLACE_ATTACHMENT'), $this);?>

	                        <input type="file" name="atachment" id="atachment" class="inputbox<?php if ($this->_tpl_vars['cfg']->attach_compulsory): ?> required<?php endif; ?>" />
	                        <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ALLOWED_EXTENSIONS'), $this);?>
 <?php echo $this->_tpl_vars['cfg']->attach_extensions; ?>

	                    <?php else: ?>
	                        <input type="checkbox" name="delete_atachment" value="1" /> <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_DELETE_ATTACHMENT'), $this);?>

	                    <?php endif; ?>
	
	                </div>
			 	</div>
		 	<?php else: ?>
	            <div class="control-group">
	                <label class = "control-label ad_lables" for="atachment"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ATTACHMENT'), $this);?>
<?php if ($this->_tpl_vars['cfg']->enable_attach == 1 && $this->_tpl_vars['cfg']->attach_compulsory == 1): ?><span style="color:#FF0000;">*</span><?php endif; ?></label>
	                <div class="controls">
	                    <input type="file" name="attachment" id="attachment" />
	                    <br /><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ALLOWED_EXTENSIONS'), $this);?>
 <?php echo $this->_tpl_vars['cfg']->attach_extensions; ?>

	                </div>
			 	</div>
		 	<?php endif; ?>
	 	<?php endif; ?>

        <div class="control-group">
			<label class = "control-label ad_lables" for="video"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_DETAILS_VIDEO'), $this);?>
</label>
            <div class="controls video_sources">
                <div id="video-sources" name="video-sources">
                  <ul style="">
                    <li style="">
                    <a target="_blank" alt="YouTube" title="YouTube" href="http://www.youtube.com/">YouTube</a></li>
                    <li><img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsfactory/images/bullet_grey.png" /></li>
                    <li><a target="_blank" alt="MySpace Video" title="MySpace Video" href="http://vids.myspace.com/">MySpace Video</a></li>
                    <li><img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsfactory/images/bullet_grey.png" /></li>
                    <li><a target="_blank" alt="Vimeo" title="Vimeo" href="https://vimeo.com/">Vimeo</a></li>
                    <li><img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsfactory/images/bullet_grey.png" /></li>
                    <li><a target="_blank" alt="Metacafe" title="Metacafe" href="http://www.metacafe.com/">Metacafe</a></li>
                    <li><img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsfactory/images/bullet_grey.png" /></li>
                    <li><a target="_blank" alt="Howcast" title="Howcast" href="http://www.howcast.com/">Howcast</a></li>
                  </ul>
                </div>
                <input id="video-link" type="text" size="120" maxlength="255" class="inputbox input-xxlarge" style="padding: 2px 0 !important;max-width: 510px;" name="video-link" value="<?php echo $this->_tpl_vars['video_url']; ?>
" >
                <?php if ($this->_tpl_vars['video_id']): ?>
                    <input type="checkbox" name="delete_video" value="<?php echo $this->_tpl_vars['video_id']; ?>
" />&nbsp;<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_DELETE_VIDEO'), $this);?>

                <?php endif; ?>
            </div>
        </div>

        <?php if ($this->_tpl_vars['cfg']->max_nr_tags > 0): ?>
	       	<div class="control-group">
				<label  class = "control-label ad_lables" for="tags"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_TAGS'), $this);?>
</label>
	            <div class="controls">
	                <input class="inputbox" type="text" id="tags" name="tags" value="<?php if ($this->_tpl_vars['ad']->id): ?><?php echo $this->_tpl_vars['ad']->get('tags'); ?>
<?php endif; ?>" size="70" />
	            </div>
	
	        </div>
        <?php endif; ?>

        <div>
            <div class="width100left">
                 <?php echo $this->_tpl_vars['custom_fields_html']; ?>

            </div>
        </div>

		<div><span>&nbsp;</span></div>
        <?php if ($this->_tpl_vars['terms']): ?>
            <div class="control-group">
                <div class="controls">
                    <small>
                        <input type="checkbox" class="inputbox" id="terms" name="agreement" value="1" />
                    <?php echo ((is_array($_tmp='COM_ADS_AGREE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                        <a href="javascript: void(0);" onclick="window.open('<?php echo $this->_tpl_vars['links']->getTermsAndConditionsRoute(); ?>
','messwindow','location=1,status=1,scrollbars=1,width=500,height=500')"
                           id="ad_terms">
                    <?php echo ((is_array($_tmp='COM_ADS_TERMS_AND_CONDITIONS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
                            </small>
                </div>
             </div>
		<?php endif; ?>
	
	<?php if (! $this->_tpl_vars['user']->id && $this->_tpl_vars['cfg']->ads_logged_posting): ?>
		<div><span>&nbsp;</span></div>
        <input type="hidden" name="tk" value="<?php echo $this->_tpl_vars['ad']->token_id; ?>
" />

        <div class="control-group">
            <label class = "control-label ad_lables" for="video"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_POSTER_INFO'), $this);?>
</label>

            <div class="adds_guest_info controls">
                <span class="guest_ad_info">
                    <input class="inputbox required" type="text" id="guest_username" name="guest_username" value="<?php echo $this->_tpl_vars['ad']->guest_username; ?>
" placeholder="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_USER'), $this);?>
 <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_NAME'), $this);?>
" />
                </span>
                <span class="guest_ad_info">
                    <input class="inputbox required validate-email " type="text" id="guest_email" name="guest_email" value="<?php echo $this->_tpl_vars['ad']->guest_email; ?>
" placeholder="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_EMAIL'), $this);?>
" />
                </span>
                <?php if ($this->_tpl_vars['cfg']->enable_captcha && ! $this->_tpl_vars['ad']->id): ?>
                <div>
                    <span class="center">
                        <?php echo $this->_tpl_vars['cs']; ?>

                        <?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_CAPTCHA'), $this);?>

                    </span>
                </div>
                <?php endif; ?>
                <div>
                    <span class="ads_guest_info_cell center">&nbsp;</span>
                </div>
            </div>
        </div>
	<?php endif; ?>

    </div>
	<br />
	<div class="btn-toolbar form-horizontal">
    	<div class="btn-group controls">
        	<input name="save" id="save_ad2" value="<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_SAVE'), $this);?>
" class="btn btn-primary validate" type="submit" />&nbsp;&nbsp;
		</div>
		<div class="btn-group controls">
        	<input name = "cancel" value = "<?php echo ((is_array($_tmp='COM_ADS_CANCEL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class = "btn" type = "submit" />
    	</div>
	</div>

</form>

</div>
<!-- next row -->
<div style="clear:both;" ></div>

<?php 
    {echo JHtml::_('behavior.keepalive');}
 ?>

