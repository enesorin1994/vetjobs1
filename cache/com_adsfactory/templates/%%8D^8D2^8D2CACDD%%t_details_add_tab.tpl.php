<?php /* Smarty version 2.6.28, created on 2017-03-16 14:48:16
         compiled from elements/details/t_details_add_tab.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'createtabslist', 'elements/details/t_details_add_tab.tpl', 1, false),array('function', 'additemtabs', 'elements/details/t_details_add_tab.tpl', 4, false),array('function', 'endtabslist', 'elements/details/t_details_add_tab.tpl', 20, false),array('function', 'startpane', 'elements/details/t_details_add_tab.tpl', 22, false),array('function', 'endpane', 'elements/details/t_details_add_tab.tpl', 41, false),array('modifier', 'translate', 'elements/details/t_details_add_tab.tpl', 4, false),)), $this); ?>
<?php echo $this->_plugins['function']['createtabslist'][0][0]->smarty_createtabslist(array('name' => "content-pane"), $this);?>

    <?php if ($this->_tpl_vars['cfg']->allow_messages): ?>
        <?php if (( ! $this->_tpl_vars['is_logged_in'] && $this->_tpl_vars['cfg']->allow_guest_messaging ) || $this->_tpl_vars['is_logged_in']): ?>
            <?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'tab1','text' => ((is_array($_tmp='COM_ADS_MESSAGES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'active' => '1'), $this);?>

        <?php endif; ?>
    <?php endif; ?>

    <?php if ($this->_tpl_vars['cfg']->google_key && $this->_tpl_vars['cfg']->map_in_ad_details && ( ( $this->_tpl_vars['ad']->googlex && $this->_tpl_vars['ad']->googley ) || ( $this->_tpl_vars['user']->googleMaps_x && $this->_tpl_vars['user']->googleMaps_y ) )): ?>
        <?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'tab2','text' => ((is_array($_tmp='COM_ADS_LOCATION_ON_MAP')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

    <?php endif; ?>

    <?php if ($this->_tpl_vars['ad']->get('imagecount')): ?>
        <?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'tab3','text' => ((is_array($_tmp='COM_ADS_SCREENSHOTS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

    <?php endif; ?>

    <?php if ($this->_tpl_vars['video_link'] != ''): ?>
            <?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'tab4','text' => ((is_array($_tmp='COM_ADS_TAB_VIDEO')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

    <?php endif; ?>
<?php echo $this->_plugins['function']['endtabslist'][0][0]->smarty_endtabslist(array(), $this);?>


<?php echo $this->_plugins['function']['startpane'][0][0]->smarty_startpane(array('name' => "content-pane",'active' => 'tab1'), $this);?>

    <?php if ($this->_tpl_vars['cfg']->allow_messages): ?>
        <?php if (( ! $this->_tpl_vars['is_logged_in'] && $this->_tpl_vars['cfg']->allow_guest_messaging ) || $this->_tpl_vars['is_logged_in']): ?>
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/details/t_tab_messages.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>         <?php endif; ?>
    <?php endif; ?>

    <?php if ($this->_tpl_vars['cfg']->google_key && $this->_tpl_vars['cfg']->map_in_ad_details && ( ( $this->_tpl_vars['ad']->googlex && $this->_tpl_vars['ad']->googley ) || ( $this->_tpl_vars['user']->googleMaps_x && $this->_tpl_vars['user']->googleMaps_y ) )): ?>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/details/t_tab_maps.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>     <?php endif; ?>

    <?php if ($this->_tpl_vars['ad']->get('imagecount')): ?>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/details/t_tab_gallery.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>     <?php endif; ?>

    <?php if ($this->_tpl_vars['video_link'] != ''): ?>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/details/t_tab_video.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>     <?php endif; ?>

<?php echo $this->_plugins['function']['endpane'][0][0]->smarty_endpane(array('name' => "content-pane"), $this);?>