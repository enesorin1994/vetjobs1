<?php /* Smarty version 2.6.28, created on 2017-03-16 14:48:16
         compiled from elements/details/t_action_buttons.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 'elements/details/t_action_buttons.tpl', 7, false),)), $this); ?>
<div class="pull-right text-right padding0">
    <div class="padding0">
        <?php if (! $this->_tpl_vars['ad']->isMyAd() && $this->_tpl_vars['ad']->closed == 0): ?>
            <?php if ($this->_tpl_vars['is_logged_in']): ?>
                <?php if ($this->_tpl_vars['ad']->get('favorite')): ?>
                    <span class = 'add_to_watchlist'><a href = '<?php echo $this->_tpl_vars['ad']->get('links.del_from_watchlist'); ?>
'>
                        <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_0.png" title = "<?php echo ((is_array($_tmp='COM_ADS_REMOVE_FROM_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt = "<?php echo ((is_array($_tmp='COM_ADS_REMOVE_FROM_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" /></a>
                    </span>
                <?php else: ?>
                    <span class = 'add_to_watchlist'>
                        <a href = '<?php echo $this->_tpl_vars['ad']->get('links.add_to_watchlist'); ?>
'>
                            <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_1.png" title = "<?php echo ((is_array($_tmp='COM_ADS_ADD_TO_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                                 alt = "<?php echo ((is_array($_tmp='COM_ADS_ADD_TO_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" /></a>
                    </span>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['ad']->isMyAd()): ?>
            <?php if ($this->_tpl_vars['ad']->closed == 1 && $this->_tpl_vars['ad']->close_by_admin == 0): ?>
                <input type = "button"
                       src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
republish_ad.png"
                       onclick = "window.location = '<?php echo $this->_tpl_vars['ad']->get('links.republish'); ?>
';"
                       value="<?php echo ((is_array($_tmp='COM_ADS_REPUBLISH')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                       class="btn btn-primary"
                       alt = "<?php echo ((is_array($_tmp='COM_ADS_REPUBLISH')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                       title = "<?php echo ((is_array($_tmp='COM_ADS_REPUBLISH')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                        />
                <input type = "button"
                      src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
extend_ad.png"
                      onclick = "window.location = '<?php echo $this->_tpl_vars['ad']->get('links.extend_ad'); ?>
';"
                      value="<?php echo ((is_array($_tmp='COM_ADS_EXTEND')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                      class="btn btn-primary"
                      alt = "<?php echo ((is_array($_tmp='COM_ADS_EXTEND')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                      title = "<?php echo ((is_array($_tmp='COM_ADS_EXTEND')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                       />
            <?php else: ?>
                <input type = "button" src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
edit.png"
                       alt = "<?php echo ((is_array($_tmp='COM_ADS_EDIT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                       title = "<?php echo ((is_array($_tmp='COM_ADS_EDIT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                       style = "border: none; "
                       onclick = "window.location = '<?php echo $this->_tpl_vars['ad']->get('links.edit'); ?>
';"
                       value="<?php echo ((is_array($_tmp='COM_ADS_EDIT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                       class="btn btn-primary"
                        />
                <input type = "button" src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
delete.png"
                       alt = "<?php echo ((is_array($_tmp='COM_ADS_CLOSE_AND_ARCHIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                       title = "<?php echo ((is_array($_tmp='COM_ADS_CLOSE_AND_ARCHIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                       style = "border: none;"
                       onclick = "
                       document.getElementById('overlay').style.display='block';
                       document.getElementById('close_ad_div').style.display='block';
                       "
                       value="<?php echo ((is_array($_tmp='COM_ADS_CLOSE_AND_ARCHIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                       class="btn"
                        />
            <?php endif; ?>
    		            <div id = "overlay" style = "display: none;">
                <div id = "close_ad_div" style = "display:none;">
                    <form action="<?php echo $this->_tpl_vars['ad']->get('links.cancel'); ?>
" method="post" name="cancel_ad_form" id="cancel_ad_form">
                        <input type = "hidden" name = "id" value = "<?php echo $this->_tpl_vars['ad']->id; ?>
" />
                        <input type = "hidden" name = "option" value = "<?php echo $this->_tpl_vars['option']; ?>
" />
                        <input type = "hidden" name = "task" value = "cancelad" />
                        <input type = "hidden" name = "Itemid" value = "<?php echo $this->_tpl_vars['Itemid']; ?>
" />
                        <table>
                            <tr>

                                <td class="ads_top_align">
                                    <div class="cancel_reason"><?php echo ((is_array($_tmp='COM_ADS_CANCEL_REASON')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</div>
                                    <textarea id = "cancel_reason" name = "cancel_reason" cols = "34" rows = "7"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan = "2" class="text-center">
                                    <span class = "v_spacer_5"></span>
                                    <input type = "button" value = "<?php echo ((is_array($_tmp='COM_ADS_CANCEL_AD')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                                           onclick = "if(confirm('<?php echo ((is_array($_tmp='COM_ADS_ARE_YOU_SURE_YOU_WANT_TO_CANCEL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
')) document.getElementById('cancel_ad_form').submit();"
                                           class = "btn btn-primary" />
                                    <input type = "button"
                                           value = "<?php echo ((is_array($_tmp='COM_ADS_CLOSE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                                           onclick = "
                                           document.getElementById('close_ad_div').style.display='none';
                                           document.getElementById('overlay').style.display='none';
                                            document.getElementById('cancel_reason').value = '' "
                                           class = "btn"
                                            />
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
            
                <?php else: ?>
            <?php if ($this->_tpl_vars['ad']->closed == 0): ?>
                <a href = '<?php echo $this->_tpl_vars['ad']->get('links.report'); ?>
'>
                    <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
report_ad_ads.png"
                         title = "<?php echo ((is_array($_tmp='COM_ADS_REPORT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                         alt = "<?php echo ((is_array($_tmp='COM_ADS_REPORT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                            />
                </a>
            <?php endif; ?>
        <?php endif; ?>

    </div>

	<?php if ($this->_tpl_vars['ad']->isMyAd()): ?>
		<?php if ($this->_tpl_vars['cfg']->admin_approval && ! $this->_tpl_vars['ad']->approved): ?>
	            <div>
	                    <div class = "ads_pending_notify"><?php echo ((is_array($_tmp='COM_ADS_AD_IS_PENDING_ADMINISTRATOR_APPROVAL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</div>
	            </div>
		<?php endif; ?>
	<?php endif; ?>

</div>
<!-- [-] Buttons Bar -->