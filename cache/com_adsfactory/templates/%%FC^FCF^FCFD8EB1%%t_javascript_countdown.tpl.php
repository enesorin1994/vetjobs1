<?php /* Smarty version 2.6.28, created on 2017-03-15 13:04:58
         compiled from js/t_javascript_countdown.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'import_js_file', 'js/t_javascript_countdown.tpl', 2, false),array('block', 'import_js_block', 'js/t_javascript_countdown.tpl', 3, false),)), $this); ?>
<?php if ($this->_tpl_vars['cfg']->enable_countdown): ?>
    <?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "countdown.js"), $this);?>

    <?php $this->_tag_stack[] = array('import_js_block', array()); $_block_repeat=true;$this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
        <?php echo '
    	window.addEvent(\'domready\', function(){
    		new ad_countdown( \'.timer\', language["ads_days"],language["ads_expired"]);
    	});
        '; ?>

    <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo $this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
<?php endif; ?>