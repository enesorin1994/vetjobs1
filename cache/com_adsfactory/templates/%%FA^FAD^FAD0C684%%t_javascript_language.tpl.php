<?php /* Smarty version 2.6.28, created on 2017-03-15 13:04:58
         compiled from js/t_javascript_language.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'import_js_block', 'js/t_javascript_language.tpl', 1, false),array('function', 'jtext', 'js/t_javascript_language.tpl', 9, false),array('modifier', 'translate', 'js/t_javascript_language.tpl', 28, false),)), $this); ?>
<?php $this->_tag_stack[] = array('import_js_block', array()); $_block_repeat=true;$this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
var ROOT_HOST = '<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
';
var Validation_Messages=Array();
Validation_Messages["required"] = "This field is required!";
Validation_Messages["email"] = "This field is not a valid email!";

var language=Array();

language["adsfactory_err_enter_message"] 			= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ERR_ENTER_MESSAGE'), $this);?>
';
language["adsfactory_err_login_to_send_message"] 	= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ERR_LOGIN_TO_SEND_MESSAGE'), $this);?>
';
language["adsfactory_err_enter_title"] 				= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ERR_ENTER_TITLE'), $this);?>
';
language["adsfactory_err_dates_invalid"] 			= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ERR_DATES_INVALID'), $this);?>
';
language["adsfactory_err_main_img_comp"] 			= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ERR_MAIN_IMG_COMPULSORY'), $this);?>
';
language["adsfactory_err_price"] 					= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ERR_PRICE'), $this);?>
';
language["adsfactory_err_price_valid"] 				= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ERR_PRICE_VALID'), $this);?>
';
language["adsfactory_err_terms"] 					= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_YOU_MUST_CHECK_THE_TERMS_AND_CONDITIONS'), $this);?>
';
language["adsfactory_err_adtype"] 					= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ERR_ADTYPE'), $this);?>
';
language["adsfactory_select_category"] 				= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_SELECT_CATEGORY'), $this);?>
';
language["adsfactory_choose_category"] 				= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_CHOOSE_CATEGORY'), $this);?>
';
language["adsfactory_fill_in_fields"] 				= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_FILL_IN_FIELDS'), $this);?>
';
language["adsfactory_err_extension"] 				= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ERR_EXTENSION'), $this);?>
';
language["adsfactory_err_postcode"] 				= '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_ERR_POSTCODE'), $this);?>
';
language["ads_current_position"] 				    = '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_CURRENT_POSITION'), $this);?>
';
language["ads_drag_to_change_position"]             = '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_DRAG_TO_CHANGE_POSITION'), $this);?>
';
language["ads_choose_gateway"]                      = '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_CHOOSE_GATEWAY'), $this);?>
'
language["ad_post_in_cat"]                          = '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_POST_AD_IN_THIS_CATEGORY'), $this);?>
';
language["ads_required"]                            = '<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_ADS_FIELD_REQUIRED'), $this);?>
';
language["ads_err_numeric"]                         = '<?php echo ((is_array($_tmp='COM_ADS_FIELD_MUST_BE_NUMERIC')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
';
language["ads_err_email"]                           = '<?php echo ((is_array($_tmp='COM_ADS_EMAIL_IS_NOT_VALID')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
';
language["ads_err_startdate"]                       = '<?php echo ((is_array($_tmp='COM_ADS_AD_START_DATE_IS_NOT_VALID')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
';
language["ads_err_enddate"]                         = '<?php echo ((is_array($_tmp='COM_ADS_AD_END_DATE_IS_NOT_VALID')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
';
language["ads_err_max_valability"]                  = '<?php echo ((is_array($_tmp='COM_ADS_AD_END_DATE_EXCEEDS_MAXIMUM_ALLOWED_LENGTH')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
';
language["ads_days"]                                = '<?php echo ((is_array($_tmp='COM_ADS_DAYS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
';
language["ads_expired"]                             = '<?php echo ((is_array($_tmp='COM_ADS_EXPIRED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
';
language["ads_noresults"]                           = '<?php echo ((is_array($_tmp='COM_ADS_NO_RESULTS_FOUND')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
';

if(typeof Calendar != "undefined")
    Calendar._TT["DEF_DATE_FORMAT"]=dateformat('<?php echo $this->_tpl_vars['cfg']->date_format; ?>
');

<?php if ($this->_tpl_vars['terms']): ?>
    var must_accept_term= true;
<?php else: ?>
    var must_accept_term= false;
<?php endif; ?>

var ad_currency='<?php echo $this->_tpl_vars['ad']->currency; ?>
';	
	
<?php echo '
function dateformat(php_format)
{
    d=\'%Y-%M-%d\';
    if (php_format==\'Y-m-d\') d=\'%Y-%M-%d\';
    if (php_format==\'Y-d-m\') d=\'%Y-%d-%M\';
    if (php_format==\'Y/m/d\') d=\'%Y/%M/%d\';

    return d;
}
'; ?>

<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo $this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>