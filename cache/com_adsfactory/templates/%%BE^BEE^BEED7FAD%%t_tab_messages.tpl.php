<?php /* Smarty version 2.6.28, created on 2017-03-16 14:48:16
         compiled from elements/details/t_tab_messages.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'starttab', 'elements/details/t_tab_messages.tpl', 3, false),array('function', 'cycle', 'elements/details/t_tab_messages.tpl', 15, false),array('function', 'printdate', 'elements/details/t_tab_messages.tpl', 23, false),array('function', 'endtab', 'elements/details/t_tab_messages.tpl', 140, false),array('modifier', 'translate', 'elements/details/t_tab_messages.tpl', 17, false),array('modifier', 'stripslashes', 'elements/details/t_tab_messages.tpl', 34, false),)), $this); ?>
<?php if ($this->_tpl_vars['cfg']->allow_messages): ?>
	<?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => "content-pane",'id' => 'tab1'), $this);?>

<div class="adds_man_list_rest">
<form action = "<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_adsfactory&task=savemessage&controller=messages&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
&id=<?php echo $this->_tpl_vars['ad']->id; ?>
" method = "POST" name = "adMessageForm">
    <input type = "hidden" name = "option" value = "<?php echo $this->_tpl_vars['option']; ?>
" />
    <input type = "hidden" name = "task" value = "savemessage" />
    <input type = "hidden" name = "controller" value = "messages" />
    <input type = "hidden" name = "id" value = "<?php echo $this->_tpl_vars['ad']->id; ?>
" />
    <input type = "hidden" name = "Itemid" value = "<?php echo $this->_tpl_vars['Itemid']; ?>
" />

	<?php $_from = $this->_tpl_vars['ad']->get('messages'); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
			<?php if ($this->_tpl_vars['item']->private && ( $this->_tpl_vars['my']->id == $this->_tpl_vars['item']->userid1 || $this->_tpl_vars['my']->id == $this->_tpl_vars['item']->userid2 || $this->_tpl_vars['ad']->isMyAd() )): ?>
            <div class = "ad_message_<?php echo smarty_function_cycle(array('values' => '0,1'), $this);?>
">
			    <?php if ($this->_tpl_vars['item']->userid2 != 0): ?>
                        <span class = "ad_msg_from"><?php echo ((is_array($_tmp='COM_ADS_FROM')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php if ($this->_tpl_vars['item']->fromuser): ?><a
                                href = "<?php echo $this->_tpl_vars['links']->getUserdetailsRoute($this->_tpl_vars['item']->userid1); ?>
"><?php echo $this->_tpl_vars['item']->fromuser; ?>
</a><?php else: ?><?php echo ((is_array($_tmp='COM_ADS_GUEST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php endif; ?></span>
                        <span class = "ad_msg_to"><?php echo ((is_array($_tmp='COM_ADS_TO')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <a href = "<?php echo $this->_tpl_vars['links']->getUserdetailsRoute($this->_tpl_vars['item']->userid2); ?>
"><?php echo $this->_tpl_vars['item']->touser; ?>
</a></span>
				    <?php else: ?>
                        <i><?php echo ((is_array($_tmp='COM_ADS_BROADCAST_MESSAGE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</i>,
			    <?php endif; ?>
                <span class = "ad_msg_date"><?php echo ((is_array($_tmp='COM_ADS_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['item']->modified), $this);?>
&nbsp;
				<?php if ($this->_tpl_vars['item']->userid1 != $this->_tpl_vars['joomlauser']->id && $this->_tpl_vars['item']->userid2 != 0): ?>
                        <a href = "javascript:void(0);" onclick = "adObject.SendMessage(this,<?php echo $this->_tpl_vars['item']->id; ?>
,0,'<?php echo $this->_tpl_vars['item']->fromuser; ?>
',<?php echo $this->_tpl_vars['item']->private; ?>
);">
					    <?php echo ((is_array($_tmp='COM_ADS_REPLY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
				<?php endif; ?>
            	</span>
                <br />
				<span class = "ad_msg_title">
					<img src = "<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsfactory/images/private_yes.png" alt = "" title = "<?php echo ((is_array($_tmp='COM_ADS_PRIVATE_MESSAGE_TITLE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" />
				</span>

                <div class = "ad_msg_text"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->message)) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
            </div>
            <div class = "ad_message_separator">&nbsp;</div>

			    <?php elseif (! $this->_tpl_vars['item']->private): ?>

            <div class = "ad_message_<?php echo smarty_function_cycle(array('values' => '0,1'), $this);?>
">
			    <?php if ($this->_tpl_vars['item']->userid2 != 0): ?>
                        <span class = "ad_msg_from"><?php echo ((is_array($_tmp='COM_ADS_FROM')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php if ($this->_tpl_vars['item']->fromuser): ?><a
                                href = "<?php echo $this->_tpl_vars['links']->getUserdetailsRoute($this->_tpl_vars['item']->userid1); ?>
"><?php echo $this->_tpl_vars['item']->fromuser; ?>
</a><?php else: ?><?php echo ((is_array($_tmp='COM_ADS_GUEST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php endif; ?></span>
                        <span class = "ad_msg_to"><?php echo ((is_array($_tmp='COM_ADS_TO')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <a href = "<?php echo $this->_tpl_vars['links']->getUserdetailsRoute($this->_tpl_vars['item']->userid2); ?>
"><?php echo $this->_tpl_vars['item']->touser; ?>
</a></span>
				<?php else: ?>
                        <i><?php echo ((is_array($_tmp='COM_ADS_BROADCAST_MESSAGE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</i>,
			    <?php endif; ?>
                <span class = "ad_msg_date"><?php echo ((is_array($_tmp='COM_ADS_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['item']->modified), $this);?>
&nbsp;
				<?php if ($this->_tpl_vars['item']->userid1 != $this->_tpl_vars['joomlauser']->id && $this->_tpl_vars['item']->userid2 != 0): ?>
                    <a href = "javascript:void(0);" onclick = "adObject.SendMessage(this,<?php echo $this->_tpl_vars['item']->id; ?>
,0,'<?php echo $this->_tpl_vars['item']->fromuser; ?>
', <?php echo $this->_tpl_vars['item']->private; ?>
);">
					<?php echo ((is_array($_tmp='COM_ADS_REPLY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
				<?php endif; ?>
            	</span>
                <br />
                <div class = "ad_msg_text"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->message)) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
			</div>
            <div class = "ad_message_separator">&nbsp;</div>

		<?php endif; ?>

	<?php endforeach; endif; unset($_from); ?>

	<?php if (! $this->_tpl_vars['ad']->isMyAd() && ( ! $this->_tpl_vars['ad']->closed )): ?>
        <?php if ($this->_tpl_vars['lists']['contactEnabled'] == 0): ?>
            <a href = "javascript:void(0);" style = "display:auto;"
               onclick = "adObject.SendMessage(this,0,0,'<?php echo $this->_tpl_vars['user']->username; ?>
',<?php echo $this->_tpl_vars['cfg']->ads_messages_public; ?>
);"><?php echo ((is_array($_tmp='COM_ADS_SEND_MESSAGE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        <?php endif; ?>
	<?php else: ?>
        <a href = "javascript:void(0);"
           style = "display:auto;"
           id = "lnk_broadcast_msg"
           onclick = "adObject.SendBroadcastMessage(this);"><?php echo ((is_array($_tmp='COM_ADS_BROADCAST_MESSAGE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
	<?php endif; ?>

    <div id = "ad_message_box" style = "display:none;">

        <input type = "hidden" name = "idmsg" id = "idmsg" value = "">
        <input type = "hidden" name = "msgisprivate" id = "msgisprivate" value = "">
        <input type = "hidden" name = "buyer_id" id = "buyer_id" value = "">

        <a name = "mess"></a>
	    <?php echo ((is_array($_tmp='COM_ADS_MESSAGE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
&nbsp;<span style="text-transform: lowercase;"><?php echo ((is_array($_tmp='COM_ADS_TO')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span>&nbsp;<span id = "message_to"></span><br />
        <textarea class = "" name = "message" id = "message" rows = "15" cols = "60"></textarea>

        <div style = "margin-top: 10px;">
            <input type = "submit"
                   name = "send"
                   value = "<?php echo ((is_array($_tmp='COM_ADS_SEND_MESSAGE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                   class = "btn btn-primary"
                    />
            <input type = "submit"
                   name = "cancel"
                   value = "<?php echo ((is_array($_tmp='COM_ADS_CANCEL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                   class = "btn"
                   onclick = "document.getElementById('ad_message_box').style.display='none';
                                 document.getElementById('message').value = '';
                                 return false;"
                    />
        </div>


	    <?php if ($this->_tpl_vars['cfg']->enable_captcha): ?>
                <div><?php echo ((is_array($_tmp='COM_ADS_PLEASE_ENTER_THIS_CAPTCHA_TEXT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 </div>
		    <?php echo $this->_tpl_vars['cs']; ?>

	    <?php endif; ?>
    </div>
</form>
    </div>
<div id = "broadcast_msg" style = "display:none;">
    <form action = "<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_adsfactory&task=saveBroadcastMessage&controller=messages&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
&id=<?php echo $this->_tpl_vars['ad']->id; ?>
" method = "post" name = "adBroadcastForm">
        <input type = "hidden" name = "option" value = "<?php echo $this->_tpl_vars['option']; ?>
" />
        <input type = "hidden" name = "task" value = "saveBroadcastMessage" />
        <input type = "hidden" name = "controller" value = "messages" />
        <input type = "hidden" name = "id" value = "<?php echo $this->_tpl_vars['ad']->id; ?>
" />
        <input type = "hidden" name = "Itemid" value = "<?php echo $this->_tpl_vars['Itemid']; ?>
" />

        <strong><?php echo ((is_array($_tmp='COM_ADS_BROADCAST_MESSAGES_TO_USERS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</strong>

        <textarea class = "" name = "message" id = "broadcast_message" rows = "15" cols = "60"></textarea>

        <div style = "margin-top: 10px;">
            <input type = "submit"
                   name = "send"
                   value = "<?php echo ((is_array($_tmp='COM_ADS_SEND_MESSAGE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                   class = "btn btn-primary"
                    />
            <input type = "submit"
                   name = "cancel"
                   value = "<?php echo ((is_array($_tmp='COM_ADS_CANCEL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                   class = "btn"
                   onclick = "document.getElementById('broadcast_msg').style.display = 'none';
                                  document.getElementById('broadcast_message').value = '';
                                  document.getElementById('lnk_broadcast_msg').style.display = 'inline';
                                  return false;"
                    />
        </div>
    </form>
</div>
	<?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>

<?php endif; ?>
