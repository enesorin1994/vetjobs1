<?php /* Smarty version 2.6.28, created on 2017-03-29 13:19:42
         compiled from t_category_tree.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_category_tree.tpl', 1, false),array('function', 'import_js_file', 't_category_tree.tpl', 2, false),array('function', 'starttab', 't_category_tree.tpl', 7, false),array('function', 'endtab', 't_category_tree.tpl', 66, false),array('modifier', 'translate', 't_category_tree.tpl', 14, false),array('modifier', 'count', 't_category_tree.tpl', 38, false),)), $this); ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "jobs.js"), $this);?>


<h2><strong><?php echo $this->_tpl_vars['page_title']; ?>
</strong></h2>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/category/t_categories_tabs.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => 'job_tabmenu','id' => 'tab2'), $this);?>

  <?php $_from = $this->_tpl_vars['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['category']):
?>
	<div class="job_treecat">
		<div class="cat_link span12">
			<span class="pull-left"><a href="<?php echo $this->_tpl_vars['category']->link; ?>
"> <?php echo $this->_tpl_vars['category']->catname; ?>
</a></span>
			<span class="job_treecat_icons pull-left">
                <a href="<?php echo $this->_tpl_vars['category']->link; ?>
">
                    <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
folder-horizontal-open.png" border="0" alt="" title="<?php echo ((is_array($_tmp='COM_JOBS_FILTER_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" /></a>
                <a href="<?php echo $this->_tpl_vars['category']->view; ?>
">
                    <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
document-text-image.png" border="0" alt="" title="<?php echo ((is_array($_tmp='COM_JOBS_FILTER_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" /></a>
                <?php if ($this->_tpl_vars['is_logged_in']): ?>
                    <a href="<?php echo $this->_tpl_vars['category']->link_watchlist; ?>
">
                    <?php if ($this->_tpl_vars['category']->watchListed_flag): ?>
                        <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_0.png" border="0" alt="" title="<?php echo ((is_array($_tmp='COM_JOBS_REMOVE_FROM_FAVORITESLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"/>
                    <?php else: ?>
                        <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_1.png" border="0" alt="" title="<?php echo ((is_array($_tmp='COM_JOBS_add_to_watchlist')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"/>
                    <?php endif; ?></a>
                    <?php if ($this->_tpl_vars['isCompany']): ?>
                        <a href = "<?php echo $this->_tpl_vars['category']->link_new_listing; ?>
"><img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
new_listing.png" border = "0" alt = "<?php echo ((is_array($_tmp='COM_JOBS_NEW_LISTING_IN_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                            title = "<?php echo ((is_array($_tmp='COM_JOBS_NEW_LISTING_IN_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="new_listing" /></a>
                    <?php endif; ?>
                <?php endif; ?>
            </span>
            <span style="font-size: 10px;font-weight: normal;" class="pull-left1">
                    (<?php echo $this->_tpl_vars['category']->nr_a; ?>

            	    <?php if ($this->_tpl_vars['category']->nr_a > 1 || ! $this->_tpl_vars['category']->nr_a): ?>
            		    <?php echo ((is_array($_tmp='COM_JOBS_NR_JOBS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

            		    <?php else: ?>
            		    <?php echo ((is_array($_tmp='COM_JOBS_NR_JOB')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

            	    <?php endif; ?>

                    | <?php echo count($this->_tpl_vars['category']->subcategories); ?>

            	    <?php if (count($this->_tpl_vars['category']->subcategories) > 1 || ! count($this->_tpl_vars['category']->subcategories)): ?>
            		    <?php echo ((is_array($_tmp='COM_JOBS_NR_SUBCATS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
)
            		    <?php else: ?>
            		    <?php echo ((is_array($_tmp='COM_JOBS_NR_SUBCATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
)
            	    <?php endif; ?>
            </span>

            <?php if (count($this->_tpl_vars['category']->subcategories) > 0): ?>
            <span class="pull-left1">
                <?php $this->assign('categoryid', $this->_tpl_vars['category']->id); ?>
            	<a style="outline: none;" href="#" onclick='jQuery("#job_cats_<?php echo $this->_tpl_vars['categoryid']; ?>
").slideToggle("slow");return false;'><img src="<?php echo $this->_tpl_vars['TEMPLATE_IMAGES']; ?>
f_expand_01.png" title="<?php echo ((is_array($_tmp='COM_JOBS_HIDE_SUBCATEGORIES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="job_link" style="margin-left:8px;" /></a>
            </span>
            <?php endif; ?>

		</div>
	</div>

    <?php if (count($this->_tpl_vars['category']->subcategories) > 0): ?>
        <br />

        <div id="job_cats_<?php echo $this->_tpl_vars['category']->id; ?>
" class="job_treecatsub">
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/category/t_subcategories.tpl", 'smarty_include_vars' => array('subcategories' => $this->_tpl_vars['category']->subcategories)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </div>
    <?php endif; ?>
  <?php endforeach; endif; unset($_from); ?>

<div style="clear: both;"></div>
<?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>