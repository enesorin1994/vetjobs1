<?php /* Smarty version 2.6.28, created on 2017-03-29 13:01:50
         compiled from t_myusereducation.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_myusereducation.tpl', 3, false),array('function', 'starttab', 't_myusereducation.tpl', 28, false),array('function', 'jtext', 't_myusereducation.tpl', 92, false),array('function', 'printdate', 't_myusereducation.tpl', 127, false),array('function', 'endtab', 't_myusereducation.tpl', 158, false),array('modifier', 'translate', 't_myusereducation.tpl', 28, false),array('modifier', 'date_format', 't_myusereducation.tpl', 128, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>


<?php echo '
<script>
  jQuery(document).ready( function ($)
  {

    $(\'.edit_education\').click( function (event)
      {
          event.preventDefault();
          var id = $(this).attr("rel");

          $.post(root + "index.php?option=com_jobsfactory&task=editeducation&format=raw", {
            id:    id,
            resume_id:     resumeid },
            function(data){
            $(\'#row_edit_education\' + id).html(data);
          });

        return false;
      });
  });
</script>
'; ?>

<?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => "content-pane",'id' => 'usertab2','text' => ((is_array($_tmp='COM_JOBS_TAB_EDUCATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>


<div class="jobs_userprofile">
    <div class="user_edit_header">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr >
                <td align="left">
                    <?php echo ((is_array($_tmp='COM_JOBS_EDIT_USER_EDUCATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                </td>
            </tr>
        </table>
    </div>
<form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" >
	<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />
	<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
    <input type="hidden" name="resume_id" value="<?php echo $this->_tpl_vars['user']->id; ?>
" />
	<input type="hidden" name="controller" value="user" />
	<input type="hidden" name="task" value="saveEducation" />


    <!--resume_user_education-->
    <a title="resume_education" name="resume_education" id="resume_education"></a>

    <?php if ($this->_tpl_vars['task'] == 'userdetails'): ?>
        <div class="jobsfactory_icon jobsfactory_education_add" style="padding: 2px;">
             <a href="#" class="show_education_form" id="show_education_form<?php echo $this->_tpl_vars['user']->id; ?>
" rel="<?php echo $this->_tpl_vars['user']->id; ?>
">
                 <?php echo ((is_array($_tmp='COM_JOBS_EDUCATION_ADD')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </div>
    <?php endif; ?>
    <span style="display: none; padding: 5px;" class="jobsfactory_education_form" id="education_form<?php echo $this->_tpl_vars['user']->id; ?>
">
    	<div>
    		<table cellspacing="0" cellpadding="0" width="80%">
    		  <tr>
    			<td class="education_column">
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/resumedetail/t_education_add.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                </td>

    		  </tr>
    		</table>
    	</div>
    </span>

    <?php if ($this->_tpl_vars['lists']['countedu'] > 0 && ( $this->_tpl_vars['task'] == 'userdetails' )): ?>
        <table width="100%" cellpadding="0" cellspacing="0" class="user_resume" >
            <tr>
                <td align="right" class="resume_edit_section" colspan="2" >
                    <?php echo ((is_array($_tmp='COM_JOBS_CANDIDATE_EDUCATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                </td>
            </tr>
        </table>

        <?php $_from = $this->_tpl_vars['array_education']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['user_education']):
?>
        <div id="row_edit_education<?php echo $this->_tpl_vars['user_education']->id; ?>
" >
          <table class="no_border user_resume table-striped" width="100%" cellpadding="0" cellspacing="0" style="border:solid 1px #DDDDDD; border-top:none; " >
            <tr>
                <td class="job_dbk" align="right">
                    <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_STUDY_LEVEL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                </td>
                <td class="job_dt job_values1">
                    <?php echo $this->_tpl_vars['user_education']->levelname; ?>

                </td>
                <td>
                    <span style="float: right">
                        <a class="edit_education1" href="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&task=editeducation&id=<?php echo $this->_tpl_vars['user_education']->id; ?>
&resume_id=<?php echo $this->_tpl_vars['user']->id; ?>
&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
"><input name="edit" value='<?php echo ((is_array($_tmp='COM_JOBS_EDIT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
' class="btn btn-success" type="button" /></a>
                        <a onclick="return confirm('<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_JOBS_CONFIRM_DELETE'), $this);?>
')" href="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&controller=user&task=delete_education&id=<?php echo $this->_tpl_vars['user_education']->id; ?>
&resume_id=<?php echo $this->_tpl_vars['user']->id; ?>
&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
" class="delete_education" rel="<?php echo $this->_tpl_vars['user_education']->id; ?>
" >
                            <input name="del" value='<?php echo ((is_array($_tmp='COM_JOBS_DELETE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
' class="btn btn-danger" type="button" /></a>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right">
                    <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_EDU_INSTITUTION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                </td>
                <td class="job_dt job_values" colspan="2">
                    <?php echo $this->_tpl_vars['user_education']->edu_institution; ?>

                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right"
                    <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_COUNTRY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                </td>
                <td class="job_dt job_values1" colspan="2">
                    <?php echo $this->_tpl_vars['user_education']->country; ?>

                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right">
                    <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_CITY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                </td>
                <td class="job_dt job_values1" colspan="2">
                    <?php echo $this->_tpl_vars['user_education']->city; ?>

                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right">
                    <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_START_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                </td>
                <td class="job_dt job_values1" colspan="2">
                    <?php if ($this->_tpl_vars['task'] == 'userdetails'): ?>
                        <?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['user_education']->start_date), $this);?>

                        <input class="text_area" type="hidden" name="start_date<?php echo $this->_tpl_vars['user_education']->id; ?>
" id="start_date<?php echo $this->_tpl_vars['user_education']->id; ?>
" size="15" maxlength="19" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['user_education']->start_date)) ? $this->_run_mod_handler('date_format', true, $_tmp, '%Y-%m-%d %H:%M:%S') : smarty_modifier_date_format($_tmp, '%Y-%m-%d %H:%M:%S')); ?>
" alt="start_date"/>
                    <?php else: ?>
                        <?php echo $this->_tpl_vars['lists']['edu_start_date_html']; ?>

                    <?php endif; ?>

                </td>
            </tr>
            <tr>
                <td class="job_dbk" align="right">
                    <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_END_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                </td>
                <td class="job_dt job_values1" colspan="2">
                    <?php if ($this->_tpl_vars['task'] == 'userdetails'): ?>
                        <?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['user_education']->end_date), $this);?>

                        <input class="text_area" type="hidden" name="end_date" id="end_date" size="15" maxlength="19" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['user_education']->end_date)) ? $this->_run_mod_handler('date_format', true, $_tmp, '%Y-%m-%d %H:%M:%S') : smarty_modifier_date_format($_tmp, '%Y-%m-%d %H:%M:%S')); ?>
" alt="end_date"/>
                    <?php else: ?>
                        <?php echo $this->_tpl_vars['lists']['edu_end_date_html']; ?>

                    <?php endif; ?>
                </td>
            </tr>
            <tr><td colspan="3" class="job_dbk_c form_bg0">&nbsp;</td></tr>
          </table>
        </div>

        <?php endforeach; endif; unset($_from); ?>
    <?php endif; ?>

</form>
</div>

<?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>
