<?php /* Smarty version 2.6.28, created on 2017-03-29 12:34:35
         compiled from js/t_javascript_maps.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'import_js_file', 'js/t_javascript_maps.tpl', 1, false),array('block', 'import_js_block', 'js/t_javascript_maps.tpl', 3, false),array('modifier', 'default', 'js/t_javascript_maps.tpl', 4, false),array('modifier', 'translate', 'js/t_javascript_maps.tpl', 39, false),)), $this); ?>
<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "http://maps.googleapis.com/maps/api/js?key=".($this->_tpl_vars['cfg']->google_key)."&sensor=false"), $this);?>


<?php $this->_tag_stack[] = array('import_js_block', array()); $_block_repeat=true;$this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
	var zoom = <?php echo ((is_array($_tmp=@$this->_tpl_vars['cfg']->googlemap_default_zoom)) ? $this->_run_mod_handler('default', true, $_tmp, '12') : smarty_modifier_default($_tmp, '12')); ?>
;
    
    var map; 
    var infoWindow;
    var markersArray = [];

	var gmap_width = <?php echo ((is_array($_tmp=@$this->_tpl_vars['cfg']->googlemap_gx)) ? $this->_run_mod_handler('default', true, $_tmp, 250) : smarty_modifier_default($_tmp, 250)); ?>

	var gmap_height = <?php echo ((is_array($_tmp=@$this->_tpl_vars['cfg']->googlemap_gy)) ? $this->_run_mod_handler('default', true, $_tmp, 80) : smarty_modifier_default($_tmp, 80)); ?>

	
    <?php if (( $this->_tpl_vars['googleMapX'] && $this->_tpl_vars['googleMapY'] )): ?>
		var pointTox=<?php echo $this->_tpl_vars['googleMapX']; ?>
;
		var pointToy=<?php echo $this->_tpl_vars['googleMapY']; ?>
;
	<?php else: ?>
		var pointTox=<?php echo ((is_array($_tmp=@$this->_tpl_vars['cfg']->googlemap_defx)) ? $this->_run_mod_handler('default', true, $_tmp, 33.34433) : smarty_modifier_default($_tmp, 33.34433)); ?>
;
		var pointToy=<?php echo ((is_array($_tmp=@$this->_tpl_vars['cfg']->googlemap_defy)) ? $this->_run_mod_handler('default', true, $_tmp, 22.23223) : smarty_modifier_default($_tmp, 22.23223)); ?>
;
	<?php endif; ?>
    
    <?php echo '
    function load_gmaps() {
        var myOptions = {
          center: new google.maps.LatLng(pointTox, pointToy),
          zoom: zoom,
          mapTypeId: google.maps.MapTypeId.'; ?>
<?php echo $this->_tpl_vars['cfg']->googlemap_maptype; ?>
<?php echo '
        };
        map = new google.maps.Map(document.getElementById("map_canvas"),
                    myOptions);

        infoWindow = new google.maps.InfoWindow();
    }
    var gmap_selectposition=function ()
    {
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(pointTox, pointToy),
          map: map,
          draggable:true,
          title:"'; ?>
<?php echo ((is_array($_tmp='COM_JOBS_CURRENT_POSITION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php echo '"
        });
        markersArray.push(marker);
        
        document.getElementById(\'gmap_posx\').value=marker.getPosition().lat();
        document.getElementById(\'gmap_posy\').value=marker.getPosition().lng();
        google.maps.event.addListener(marker, \'position_changed\', function() {
            document.getElementById(\'gmap_posx\').value=marker.getPosition().lat();
            document.getElementById(\'gmap_posy\').value=marker.getPosition().lng();
          });
        google.maps.event.addListener(map, \'click\', function(mouseEvent) {
                marker.setPosition(mouseEvent.latLng);
          });
        
    }
	function submit_gmap_coords()
	{
		var window = this.opener;
		var xcoord = window.document.getElementById("googleX");
		var ycoord = window.document.getElementById("googleY");
		xcoord.value = document.getElementById("gmap_posx").value;
		ycoord.value = document.getElementById("gmap_posy").value;
		this.close();
	}
    var gmap_showmap=function ()
    {
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(pointTox, pointToy),
          map: map,
          draggable:false,
          title:"'; ?>
<?php echo ((is_array($_tmp='COM_JOBS_JOB_POSITION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php echo '"
        });
        markersArray.push(marker);
    }
    var gmap_refreshjobs=function()
    {
        $("wait_loader").setStyle(\'display\',\'visible\');
   	    limit = $("ads_limitbox").value;
       	cat   = $("category").value;
        searchUrl = \'index.php?option=com_jobsfactory&controller=maps&task=showBookmarks&limit=\'+limit+\'&cat=\'+cat;
        gmap_clearmarkers();
        new Request({
            method: \'GET\', 
            url: searchUrl,
            onSuccess: function(data) {
                gmap_putmarkers(data);
            }
        }).send();   
    }
    var gmap_refreshjobs_search=function()
    {
        $("wait_loader").setStyle(\'display\',\'visible\');
        address = $(\'addressInput\').value;

        $(\'search_sidebar\').set(\'html\',\'\');
        gmap_clearmarkers();
        
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({ \'address\': address },function(results, status){
            if (! results) {
                $(\'search_sidebar\').set(\'html\',language["job_noresults"]);
            }else
                if (status == google.maps.GeocoderStatus.OK) {
        		radius = $(\'radiusSelect\').value;
                for (var i = 0; i < results.length; i++) {
                    center=results[i].geometry.location;
                    if (i==0) map.panTo(center);
                    var circle=new google.maps.Circle({
                        center:center,
                        radius:radius*1000,
                        map:map,
                        fillOpacity:0.4,
                        editable:false   
                    });
                    markersArray.push(circle);
                    searchUrl = \'index.php?option=com_jobsfactory&controller=maps&task=searchBookmarks&lat=\' + center.lat() + \'&lng=\' + center.lng() + \'&radius=\' + radius;
                    new Request({
                        method: \'GET\', 
                        url: searchUrl,
                        onSuccess: function(data) {
                            gmap_putmarkers(data);
                            gmap_putsidebar(data);
                        }
                    }).send();   
                }             
            }else{
                $(\'search_sidebar\').set(\'html\',language["job_noresults"]);
            }
        });
        
        
    }

    function gmap_putmarkers(xmldata)
    {
        var xml = parseXml(xmldata);
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
            var marker = new google.maps.Marker({
              position: new google.maps.LatLng(parseFloat(markers[i].getAttribute(\'lat\')), parseFloat(markers[i].getAttribute(\'lng\'))),
              map: map,
              draggable:false,
              title:markers[i].getAttribute(\'name\'),
              info:markers[i].getAttribute(\'info\')
            });
            google.maps.event.addListener(marker,\'click\', function () {
               infoWindow.setContent(this.info);
               infoWindow.setPosition(this.position);
               infoWindow.open(this.map);
            });
            markersArray.push(marker);
        }
    }
    function gmap_putsidebar(xmldata)
    {
        var xml = parseXml(xmldata);
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
            var div = new Element(\'div\', {class: \'sidebar_element\'});
            div.set(\'html\',\'<b>\' + (i+1)+ \'.</b>&nbsp;\' +
                     markers[i].getAttribute(\'info\'));
            div.position=new google.maps.LatLng(parseFloat(markers[i].getAttribute(\'lat\')), parseFloat(markers[i].getAttribute(\'lng\')));
            div.addEvent(\'click\',function(el){
               infoWindow.setContent(this.innerHTML);
               infoWindow.setPosition(this.position);
               infoWindow.open(map);
            })
            div.inject($(\'search_sidebar\'));
        }       
    }
    function gmap_clearmarkers()
    {
      if (markersArray) 
        for (var i = 0; i < markersArray.length; i++ ) {
          markersArray[i].setMap(null);
        }
      markersArray=[];
        
    }
    function parseXml(str) {
      if (window.ActiveXObject) {
        var doc = new ActiveXObject(\'Microsoft.XMLDOM\');
        doc.loadXML(str);
        return doc;
      } else if (window.DOMParser) {
        return (new DOMParser).parseFromString(str, \'text/xml\');
      }
    }
    '; ?>
    
<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo $this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
