<?php /* Smarty version 2.6.28, created on 2017-03-30 09:53:59
         compiled from t_search_tabs.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 't_search_tabs.tpl', 16, false),)), $this); ?>
<?php echo '
<script type="text/javascript">
    jQuery(document).ready( function ($) {
        $(document).on("click", "#job_tabmenu li a", function(event){
           location.href = this.rel;
        });
    });
</script>
'; ?>


<!-- Begin Content -->
<div>
    <ul class="nav nav-tabs" id="job_tabmenu">
        <li class="<?php if (( 'show_search' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
            <a data-toggle="tab" href="#tab1" rel="<?php echo $this->_tpl_vars['links']->getSearchRoute(); ?>
" >
        <?php echo ((is_array($_tmp='COM_JOBS_SEARCH_JOBS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </li>
        <?php if (( $this->_tpl_vars['cfg']->google_key != "" )): ?>
        <li class="<?php if (( 'googlemaps' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
            <a data-toggle="tab" href="#tab2" rel = "<?php echo $this->_tpl_vars['links']->getSearchonMapRoute(); ?>
">
        <?php echo ((is_array($_tmp='COM_JOBS_RADIUS_SEARCH')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </li>
        <?php endif; ?>
        <li class="<?php if (( 'searchcompanies' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
           <a data-toggle="tab" href="#tab3" rel = "<?php echo $this->_tpl_vars['links']->getSearchonCompaniesRoute(); ?>
">
        <?php echo ((is_array($_tmp='COM_JOBS_SEARCH_COMPANIES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </li>
        <li class="<?php if (( 'searchusers' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
            <a data-toggle="tab" href="#tab4" rel = "<?php echo $this->_tpl_vars['links']->getSearchonUsersRoute(); ?>
">
        <?php echo ((is_array($_tmp='COM_JOBS_SEARCH_CANDIDATES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </li>
    </ul>
</div>
<div id="config-document" class="tab-content">
    <div id="tab1" class="tab-pane <?php if (( 'show_search' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    <?php if (( $this->_tpl_vars['cfg']->google_key != "" )): ?>
    <div id="tab2" class="tab-pane <?php if (( 'googlemaps' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    <?php endif; ?>
    <div id="tab3" class="tab-pane <?php if (( 'searchcompanies' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    <div id="tab4" class="tab-pane <?php if (( 'searchusers' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
</div>

<!-- End Content -->
<div style="clear: both;"></div>