<?php /* Smarty version 2.6.28, created on 2017-03-29 12:37:00
         compiled from t_mywatchlist.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_mywatchlist.tpl', 1, false),array('function', 'import_js_file', 't_mywatchlist.tpl', 4, false),array('modifier', 'translate', 't_mywatchlist.tpl', 6, false),)), $this); ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_countdown.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "jobs.js"), $this);?>


<h2><?php echo ((is_array($_tmp='COM_JOBS_MY_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</h2>
<?php echo '
    <script type="text/javascript">
        jQuery(document).ready( function ($) {
            $(document).on("click", "#job_tabmenu li a", function(event){
               location.href = this.rel;
            });
        });
    </script>
'; ?>


<div align="right" style="text-align:right;">
<ul class="nav nav-tabs" id="job_tabmenu">
    <li class="<?php if (( ! $this->_tpl_vars['filter_watchlist'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
       	<a data-toggle="tab" href="#tab1" rel="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
&controller=watchlist&task=<?php echo $this->_tpl_vars['task']; ?>
&filter_watchlist=">
       	    <?php echo ((is_array($_tmp='COM_JOBS_WATCHLIST_JOBS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
    </li>
    <li class="<?php if (( $this->_tpl_vars['filter_watchlist'] == 'categories' )): ?>active<?php else: ?>inactive<?php endif; ?>">
    	<a data-toggle="tab" href="#tab2" rel="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
&controller=watchlist&task=<?php echo $this->_tpl_vars['task']; ?>
&filter_watchlist=categories">
    	    <?php echo ((is_array($_tmp='COM_JOBS_MY_WATCHLIST_CATEGORIES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
    </li>
    <?php if ($this->_tpl_vars['cfg']->enable_location_management): ?>
        <li class="<?php if (( $this->_tpl_vars['filter_watchlist'] == 'cities' )): ?>active<?php else: ?>inactive<?php endif; ?>">
            <a data-toggle="tab" href="#tab3" rel="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
&controller=watchlist&task=watchlist&filter_watchlist=cities">
                <?php echo ((is_array($_tmp='COM_JOBS_MY_WATCHLIST_CITIES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </li>
        <li class="<?php if (( $this->_tpl_vars['filter_watchlist'] == 'locations' )): ?>active<?php else: ?>inactive<?php endif; ?>">
            <a data-toggle="tab" href="#tab4" rel="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
&controller=watchlist&task=watchlist&filter_watchlist=locations">
                <?php echo ((is_array($_tmp='COM_JOBS_MY_WATCHLIST_LOCATIONS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </li>
    <?php endif; ?>
    <li class="<?php if (( $this->_tpl_vars['filter_watchlist'] == 'companies' )): ?>active<?php else: ?>inactive<?php endif; ?>">
    	<a data-toggle="tab" href="#tab5" rel="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
&controller=watchlist&task=watchlist&filter_watchlist=companies">
    	    <?php echo ((is_array($_tmp='COM_JOBS_MY_WATCHLIST_COMPANIES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
    </li>
</ul>
</div>

<form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory" method="post" name="jobsForm" id="jobsForm">
<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
<input type="hidden" name="controller" value="watchlist" />
<input type="hidden" name="task" value="<?php echo $this->_tpl_vars['task']; ?>
" />
<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/t_header_filter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="job_list_container">
<tr>
    <th style="width: auto; text-align: left;">
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='Title')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'title')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> /
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='Category')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'catname')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </th>
	<th style="width: 120px; text-align: left;"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='Company')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'username')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></th>
    <th style="width: 90px; text-align: left;">
        <?php if ($this->_tpl_vars['cfg']->enable_location_management): ?>
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='City')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'cityname')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> /
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='Location')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'locname')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        <?php else: ?>
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='City')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'job_cityname')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        <?php endif; ?>
    </th>
	<th style="width: 110px; text-align: right;"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='Ending in')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'end_date')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></th>
</tr>
<tr>
   <td class="v_spacer_15" colspan="4">&nbsp;</td>
</tr>
	<?php unset($this->_sections['jobsloop']);
$this->_sections['jobsloop']['name'] = 'jobsloop';
$this->_sections['jobsloop']['loop'] = is_array($_loop=$this->_tpl_vars['job_rows']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['jobsloop']['show'] = true;
$this->_sections['jobsloop']['max'] = $this->_sections['jobsloop']['loop'];
$this->_sections['jobsloop']['step'] = 1;
$this->_sections['jobsloop']['start'] = $this->_sections['jobsloop']['step'] > 0 ? 0 : $this->_sections['jobsloop']['loop']-1;
if ($this->_sections['jobsloop']['show']) {
    $this->_sections['jobsloop']['total'] = $this->_sections['jobsloop']['loop'];
    if ($this->_sections['jobsloop']['total'] == 0)
        $this->_sections['jobsloop']['show'] = false;
} else
    $this->_sections['jobsloop']['total'] = 0;
if ($this->_sections['jobsloop']['show']):

            for ($this->_sections['jobsloop']['index'] = $this->_sections['jobsloop']['start'], $this->_sections['jobsloop']['iteration'] = 1;
                 $this->_sections['jobsloop']['iteration'] <= $this->_sections['jobsloop']['total'];
                 $this->_sections['jobsloop']['index'] += $this->_sections['jobsloop']['step'], $this->_sections['jobsloop']['iteration']++):
$this->_sections['jobsloop']['rownum'] = $this->_sections['jobsloop']['iteration'];
$this->_sections['jobsloop']['index_prev'] = $this->_sections['jobsloop']['index'] - $this->_sections['jobsloop']['step'];
$this->_sections['jobsloop']['index_next'] = $this->_sections['jobsloop']['index'] + $this->_sections['jobsloop']['step'];
$this->_sections['jobsloop']['first']      = ($this->_sections['jobsloop']['iteration'] == 1);
$this->_sections['jobsloop']['last']       = ($this->_sections['jobsloop']['iteration'] == $this->_sections['jobsloop']['total']);
?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/lists/t_watchlistjobs_cell.tpl', 'smarty_include_vars' => array('job' => ($this->_tpl_vars['job_rows'][$this->_sections['jobsloop']['index']]),'index' => ($this->_sections['jobsloop']['rownum']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endfor; endif; ?>
</table>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/t_listfooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</form>
<div style="clear: both;"></div>