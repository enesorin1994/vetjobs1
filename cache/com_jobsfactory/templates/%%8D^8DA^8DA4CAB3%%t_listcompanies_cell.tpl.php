<?php /* Smarty version 2.6.28, created on 2017-03-29 12:58:00
         compiled from elements/lists/t_listcompanies_cell.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', 'elements/lists/t_listcompanies_cell.tpl', 7, false),array('modifier', 'translate', 'elements/lists/t_listcompanies_cell.tpl', 32, false),array('function', 'positions', 'elements/lists/t_listcompanies_cell.tpl', 23, false),array('function', 'jtext', 'elements/lists/t_listcompanies_cell.tpl', 47, false),array('function', 'generate_url', 'elements/lists/t_listcompanies_cell.tpl', 62, false),)), $this); ?>
<?php if ((1 & $this->_tpl_vars['index'])): ?>
	<?php $this->assign('class', '1'); ?>
<?php else: ?>
	<?php $this->assign('class', '2'); ?>
<?php endif; ?>
<?php if ($this->_tpl_vars['company']->featured && $this->_tpl_vars['company']->featured != 'none'): ?>
	<?php $this->assign('class_featured', ((is_array($_tmp="listing-")) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['company']->featured) : smarty_modifier_cat($_tmp, $this->_tpl_vars['company']->featured))); ?>
<?php else: ?>
	<?php $this->assign('class_featured', ""); ?>
<?php endif; ?>

<tr class="<?php if ($this->_tpl_vars['class_featured']): ?><?php echo $this->_tpl_vars['class_featured']; ?>
<?php else: ?>job_row_<?php echo $this->_tpl_vars['class']; ?>
<?php endif; ?>">
	<td  class="job_dt company_title" valign="top" width="110">
        <div class="company_title1">
                <span class="job_category">
                    <?php if ($this->_tpl_vars['company']->picture): ?>
                        <img src="<?php echo $this->_tpl_vars['COMPANY_PICTURES']; ?>
/resize_<?php echo $this->_tpl_vars['company']->picture; ?>
" width="100" />
                    <?php else: ?>
                        <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
/user/logo.png" width="80" />
                    <?php endif; ?>
                </span>
        </div>
	    <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "cell-header",'item' => $this->_tpl_vars['company'],'page' => 'companies'), $this);?>

	</td>
    <td class="job_dbk" valign="top" width="*%">
        <a href="<?php echo $this->_tpl_vars['links']->getUserdetailsRoute($this->_tpl_vars['company']->userid); ?>
" target="_blank" ><?php echo $this->_tpl_vars['company']->name; ?>
</a>
        <br />

        <?php if (! $this->_tpl_vars['company']->is_my_company & $this->_tpl_vars['is_logged_in']): ?>
            <a href="<?php echo $this->_tpl_vars['company']->link_watchlist; ?>
">
                <?php if ($this->_tpl_vars['company']->watchListed_flag): ?>
                    <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_0.png" border="0" alt="" width="16" title="<?php echo ((is_array($_tmp='COM_JOBS_REMOVE_FROM_WATCHLIST_COMPANY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"/>
                <?php else: ?>
                    <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_1.png" border="0" alt="" width="16" title="<?php echo ((is_array($_tmp='COM_JOBS_ADD_TO_WATCHLIST_COMPANY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"/>
                <?php endif; ?>
            </a>
        <?php endif; ?>
        <br />
        <?php echo $this->_tpl_vars['company']->field_of_activity; ?>


    </td>
    <?php if ($this->_tpl_vars['jobs_allow_rating']): ?>
    <td class="job_dbk" valign="top" width="200">
        <?php if ($this->_tpl_vars['company']->users_count >= 5): ?>
            <i class="ads-icon-star"></i>&nbsp;<?php echo $this->_tpl_vars['company']->user_rating; ?>
 <span class="muted">(<?php echo $this->_tpl_vars['company']->users_count; ?>
)</span>
        <?php else: ?>
            <span class="muted small"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_JOBS_NOT_ENOUGH_VOTES_RECEIVED'), $this);?>
</span>
        <?php endif; ?>
        <br />
        <div style="vertical-align: bottom" class="rate-block">
            <?php if ($this->_tpl_vars['company']->my_rating_user): ?>
              <ul class="user-rating">
                <?php unset($this->_sections['full']);
$this->_sections['full']['name'] = 'full';
$this->_sections['full']['start'] = (int)0;
$this->_sections['full']['loop'] = is_array($_loop=$this->_tpl_vars['company']->my_rating_user) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['full']['show'] = true;
$this->_sections['full']['max'] = $this->_sections['full']['loop'];
$this->_sections['full']['step'] = 1;
if ($this->_sections['full']['start'] < 0)
    $this->_sections['full']['start'] = max($this->_sections['full']['step'] > 0 ? 0 : -1, $this->_sections['full']['loop'] + $this->_sections['full']['start']);
else
    $this->_sections['full']['start'] = min($this->_sections['full']['start'], $this->_sections['full']['step'] > 0 ? $this->_sections['full']['loop'] : $this->_sections['full']['loop']-1);
if ($this->_sections['full']['show']) {
    $this->_sections['full']['total'] = min(ceil(($this->_sections['full']['step'] > 0 ? $this->_sections['full']['loop'] - $this->_sections['full']['start'] : $this->_sections['full']['start']+1)/abs($this->_sections['full']['step'])), $this->_sections['full']['max']);
    if ($this->_sections['full']['total'] == 0)
        $this->_sections['full']['show'] = false;
} else
    $this->_sections['full']['total'] = 0;
if ($this->_sections['full']['show']):

            for ($this->_sections['full']['index'] = $this->_sections['full']['start'], $this->_sections['full']['iteration'] = 1;
                 $this->_sections['full']['iteration'] <= $this->_sections['full']['total'];
                 $this->_sections['full']['index'] += $this->_sections['full']['step'], $this->_sections['full']['iteration']++):
$this->_sections['full']['rownum'] = $this->_sections['full']['iteration'];
$this->_sections['full']['index_prev'] = $this->_sections['full']['index'] - $this->_sections['full']['step'];
$this->_sections['full']['index_next'] = $this->_sections['full']['index'] + $this->_sections['full']['step'];
$this->_sections['full']['first']      = ($this->_sections['full']['iteration'] == 1);
$this->_sections['full']['last']       = ($this->_sections['full']['iteration'] == $this->_sections['full']['total']);
?>
                  <li class="full"></li>
                <?php endfor; endif; ?>
                <?php unset($this->_sections['empty']);
$this->_sections['empty']['name'] = 'empty';
$this->_sections['empty']['start'] = (int)$this->_tpl_vars['company']->my_rating_user;
$this->_sections['empty']['loop'] = is_array($_loop=5) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['empty']['show'] = true;
$this->_sections['empty']['max'] = $this->_sections['empty']['loop'];
$this->_sections['empty']['step'] = 1;
if ($this->_sections['empty']['start'] < 0)
    $this->_sections['empty']['start'] = max($this->_sections['empty']['step'] > 0 ? 0 : -1, $this->_sections['empty']['loop'] + $this->_sections['empty']['start']);
else
    $this->_sections['empty']['start'] = min($this->_sections['empty']['start'], $this->_sections['empty']['step'] > 0 ? $this->_sections['empty']['loop'] : $this->_sections['empty']['loop']-1);
if ($this->_sections['empty']['show']) {
    $this->_sections['empty']['total'] = min(ceil(($this->_sections['empty']['step'] > 0 ? $this->_sections['empty']['loop'] - $this->_sections['empty']['start'] : $this->_sections['empty']['start']+1)/abs($this->_sections['empty']['step'])), $this->_sections['empty']['max']);
    if ($this->_sections['empty']['total'] == 0)
        $this->_sections['empty']['show'] = false;
} else
    $this->_sections['empty']['total'] = 0;
if ($this->_sections['empty']['show']):

            for ($this->_sections['empty']['index'] = $this->_sections['empty']['start'], $this->_sections['empty']['iteration'] = 1;
                 $this->_sections['empty']['iteration'] <= $this->_sections['empty']['total'];
                 $this->_sections['empty']['index'] += $this->_sections['empty']['step'], $this->_sections['empty']['iteration']++):
$this->_sections['empty']['rownum'] = $this->_sections['empty']['iteration'];
$this->_sections['empty']['index_prev'] = $this->_sections['empty']['index'] - $this->_sections['empty']['step'];
$this->_sections['empty']['index_next'] = $this->_sections['empty']['index'] + $this->_sections['empty']['step'];
$this->_sections['empty']['first']      = ($this->_sections['empty']['iteration'] == 1);
$this->_sections['empty']['last']       = ($this->_sections['empty']['iteration'] == $this->_sections['empty']['total']);
?>
                  <li></li>
                <?php endfor; endif; ?>
              </ul>
            <?php elseif (! $this->_tpl_vars['company']->is_my_company): ?>
              <div class="actions">
                <a href="<?php echo $this->_plugins['function']['generate_url'][0][0]->smarty_generate_url(array('_' => 'CompanyVoteRoute','id' => $this->_tpl_vars['company']->userid), $this);?>
" class="btn btn-smalls btn-success button-rate-user"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_JOBS_RATE_COMPANY'), $this);?>
</a>
              </div>
            <?php endif; ?>
        </div>
    </td>
    <?php endif; ?>
    <td class="job_dbk" valign="top" width="200">
           <?php echo ((is_array($_tmp='COM_JOBS_LOCATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:
               <?php echo $this->_tpl_vars['company']->city; ?>
<br />
           <?php if ($this->_tpl_vars['company']->no_available_jobs !== null): ?>

               <a href="<?php echo $this->_tpl_vars['links']->getCompanyJobsRoute($this->_tpl_vars['company']->userid); ?>
" target="_blank" >
                    <span class="number"><?php echo $this->_tpl_vars['company']->no_available_jobs; ?>
</span>
                </a> <?php echo ((is_array($_tmp='COM_JOBS_AVAILABLE_JOBS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

           <?php endif; ?>
    </td>
</tr>
<?php if ($this->_tpl_vars['class_featured']): ?>
<tr>
   <td class="v_spacer_7" colspan="4">&nbsp;</td>
</tr>
<?php endif; ?>