<?php /* Smarty version 2.6.28, created on 2017-03-29 12:46:20
         compiled from maps/t_listadds.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 'maps/t_listadds.tpl', 1, false),array('function', 'starttab', 'maps/t_listadds.tpl', 6, false),array('function', 'endtab', 'maps/t_listadds.tpl', 47, false),array('block', 'import_js_block', 'maps/t_listadds.tpl', 10, false),array('modifier', 'translate', 'maps/t_listadds.tpl', 22, false),)), $this); ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1><?php echo $this->_tpl_vars['page_title']; ?>
</h1>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/category/t_categories_tabs.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => 'job_tabmenu','id' => 'tab4'), $this);?>


<?php if ($this->_tpl_vars['cfg']->google_key): ?>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "js/t_javascript_maps.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php $this->_tag_stack[] = array('import_js_block', array()); $_block_repeat=true;$this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
        <?php echo '
        window.addEvent(\'domready\', function () {
            load_gmaps();
            gmap_refreshjobs();
        });
        '; ?>

    <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo $this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>

    <span class="v_spacer_15"></span>
    <div class = "jobs_list_map_filters">
        <div>
            <span class = "label"><?php echo ((is_array($_tmp='COM_JOBS_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span>
            <?php echo $this->_tpl_vars['lists']['category']; ?>

            <label class="control-label" for="ads_limitbox"><?php echo ((is_array($_tmp='COM_JOBS_SHOW')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <span>
                <select id = "ads_limitbox" onchange = "gmap_refreshjobs();" style="max-width:80px;">
                    <option value = "10">10</option>
                    <option value = "20" selected = "">20</option>
                    <option value = "50">50</option>
                    <option value = "0"><?php echo ((is_array($_tmp='COM_JOBS_ALL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</option>
                </select>
            </span>
        </div>
    </div>

    <br />
    <div id = "wait_loader" class="text-center" style = "width:auto; display:none;"><img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
ajax-loader2.gif" /></div>
    <br />
    <div class="map_search">
        <div id = "map_canvas" class="maps_t_listjobs"></div>
    </div>
    	<?php else: ?>
    		<?php echo ((is_array($_tmp='COM_JOBS_NO_GOOGLE_MAP_DEFINED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

    <?php endif; ?>
    <div class="pull-left" style="clear:both;"></div>

<?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>