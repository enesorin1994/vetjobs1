<?php /* Smarty version 2.6.28, created on 2017-03-29 12:35:58
         compiled from t_listjobs.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_listjobs.tpl', 1, false),array('function', 'import_js_file', 't_listjobs.tpl', 4, false),array('modifier', 'translate', 't_listjobs.tpl', 7, false),array('modifier', 'count', 't_listjobs.tpl', 64, false),)), $this); ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_countdown.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "jobs.js"), $this);?>


<h2>
<?php echo ((is_array($_tmp='COM_JOBS_JOBS_LIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

	<a href="index.php?option=com_jobsfactory&task=<?php echo $this->_tpl_vars['task']; ?>
&format=feed&limitstart=" target="_blank">
		<img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_rss.jpg" width="14" border="0" alt="RSS" />
	</a>
</h2>

<?php echo '
<script type="text/javascript">
    jQuery(document).ready( function ($) {
        $(document).on("click", "#job_tabmenu li a", function(event){
           location.href = this.rel;
        });
    });
</script>
'; ?>



<div style="text-align:right;">
<ul class="nav nav-tabs" id="job_tabmenu">
    <li class="<?php if (( $this->_tpl_vars['filter_archive'] == 'active' || $this->_tpl_vars['filter_archive'] == '' )): ?>active<?php else: ?>inactive<?php endif; ?>">
        <a data-toggle="tab" href="#tab1" rel ="<?php echo $this->_tpl_vars['links']['filter_link_active']; ?>
">
            <?php echo ((is_array($_tmp='COM_JOBS_VIEW_ACTIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
    </li>
    <li class="<?php if (( $this->_tpl_vars['filter_archive'] == 'archive' )): ?>active<?php else: ?>inactive<?php endif; ?>">
       	<a data-toggle="tab" href="#tab2" rel="<?php echo $this->_tpl_vars['links']['filter_link_archive']; ?>
">
       	    <?php echo ((is_array($_tmp='COM_JOBS_VIEW_ARCHIVED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
    </li>
</ul>
</div>


<form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
" method="get" name="jobsForm" id="jobsForm">
<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
<input type="hidden" name="task" value="<?php echo $this->_tpl_vars['task']; ?>
" />
<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/t_header_filter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="job_list_container">
<tr style="height: 30px;">
	<th style="width: auto; text-align: left;">
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='COM_JOBS_TITLE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'title')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> /
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='COM_JOBS_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'catname')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> /
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='COM_JOBS_USER')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'username')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </th>
	<th style="width: 190px; text-align: left;">
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='COM_JOBS_DETAILS_ENDING_IN')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'end_date')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </th>
	<th style="width: 120px; text-align: right;">
    	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='COM_JOBS_NR_APPLICATIONS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'nr_applicants')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</th>
</tr>
    <tr>
       <td class="v_spacer_15" colspan="4">&nbsp;</td>
    </tr>

  <?php if (((is_array($_tmp=$this->_tpl_vars['job_rows'])) ? $this->_run_mod_handler('count', true, $_tmp) : count($_tmp))): ?>
	<?php unset($this->_sections['jobsloop']);
$this->_sections['jobsloop']['name'] = 'jobsloop';
$this->_sections['jobsloop']['loop'] = is_array($_loop=$this->_tpl_vars['job_rows']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['jobsloop']['show'] = true;
$this->_sections['jobsloop']['max'] = $this->_sections['jobsloop']['loop'];
$this->_sections['jobsloop']['step'] = 1;
$this->_sections['jobsloop']['start'] = $this->_sections['jobsloop']['step'] > 0 ? 0 : $this->_sections['jobsloop']['loop']-1;
if ($this->_sections['jobsloop']['show']) {
    $this->_sections['jobsloop']['total'] = $this->_sections['jobsloop']['loop'];
    if ($this->_sections['jobsloop']['total'] == 0)
        $this->_sections['jobsloop']['show'] = false;
} else
    $this->_sections['jobsloop']['total'] = 0;
if ($this->_sections['jobsloop']['show']):

            for ($this->_sections['jobsloop']['index'] = $this->_sections['jobsloop']['start'], $this->_sections['jobsloop']['iteration'] = 1;
                 $this->_sections['jobsloop']['iteration'] <= $this->_sections['jobsloop']['total'];
                 $this->_sections['jobsloop']['index'] += $this->_sections['jobsloop']['step'], $this->_sections['jobsloop']['iteration']++):
$this->_sections['jobsloop']['rownum'] = $this->_sections['jobsloop']['iteration'];
$this->_sections['jobsloop']['index_prev'] = $this->_sections['jobsloop']['index'] - $this->_sections['jobsloop']['step'];
$this->_sections['jobsloop']['index_next'] = $this->_sections['jobsloop']['index'] + $this->_sections['jobsloop']['step'];
$this->_sections['jobsloop']['first']      = ($this->_sections['jobsloop']['iteration'] == 1);
$this->_sections['jobsloop']['last']       = ($this->_sections['jobsloop']['iteration'] == $this->_sections['jobsloop']['total']);
?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/lists/t_listjobs_cell.tpl', 'smarty_include_vars' => array('job' => ($this->_tpl_vars['job_rows'][$this->_sections['jobsloop']['index']]),'index' => ($this->_sections['jobsloop']['rownum']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endfor; endif; ?>
    <?php else: ?>
    <tr>
        <td class="job_dbk" valign="top" colspan="3">
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 't_nojobs.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </td>
    </tr>
  <?php endif; ?>
</table>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/t_listfooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</form>
<div class="v_spacer_15">&nbsp;</div>
<div style="clear: both;"></div>