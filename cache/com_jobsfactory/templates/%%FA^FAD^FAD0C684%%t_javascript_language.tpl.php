<?php /* Smarty version 2.6.28, created on 2017-03-29 12:34:35
         compiled from js/t_javascript_language.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'import_js_block', 'js/t_javascript_language.tpl', 1, false),array('modifier', 'translate', 'js/t_javascript_language.tpl', 3, false),array('modifier', 'default', 'js/t_javascript_language.tpl', 30, false),)), $this); ?>
<?php $this->_tag_stack[] = array('import_js_block', array()); $_block_repeat=true;$this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
var language=Array();
language["job_err_fields_compulsory"]="<?php echo ((is_array($_tmp='COM_JOBS_COMPULSORY_FIELDS_UNFILLED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_err_empty_job"]="<?php echo ((is_array($_tmp='COM_JOBS_PRICE_CAN_NOT_BE_EMTPY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_err_terms"]="<?php echo ((is_array($_tmp='COM_JOBS_YOU_MUST_CHECK_THE_TERMS_AND_CONDITIONS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";

language["job_confirm_close_job"]="<?php echo ((is_array($_tmp='COM_JOBS_ARE_YOU_SURE_YOU_WANT_TO_CLOSE_THIS_JOB')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_job_price"]="<?php echo ((is_array($_tmp='COM_JOBS_APPLICATION_PRICE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_required"]="<?php echo ((is_array($_tmp='COM_JOBS_FIELD_REQUIRED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_err_numeric"]="<?php echo ((is_array($_tmp='COM_JOBS_FIELD_MUST_BE_NUMERIC')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_err_email"]="<?php echo ((is_array($_tmp='COM_JOBS_EMAIL_IS_NOT_VALID')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_err_startdate"]="<?php echo ((is_array($_tmp='COM_JOBS_JOB_START_DATE_IS_NOT_VALID')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_err_enddate"]="<?php echo ((is_array($_tmp='COM_JOBS_JOB_END_DATE_IS_NOT_VALID')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_err_max_valability"]="<?php echo ((is_array($_tmp='COM_JOBS_JOB_END_DATE_EXCEEDS_MAXIMUM_ALLOWED_LENGTH')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_notfound"]="<?php echo ((is_array($_tmp='COM_JOBS_NOT_FOUND')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_noresults"]="<?php echo ((is_array($_tmp='COM_JOBS_NO_RESULTS_FOUND')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_post_in_cat"]="<?php echo ((is_array($_tmp='COM_JOBS_POST_JOB_IN_THIS_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_days"]="<?php echo ((is_array($_tmp='COM_JOBS_DAYS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_expired"]="<?php echo ((is_array($_tmp='COM_JOBS_EXPIRED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";
language["job_err_birthdate"]="<?php echo ((is_array($_tmp='COM_JOBS_JOB_BIRTH_DATE_IS_NOT_VALID')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
";

<?php if ($this->_tpl_vars['terms_and_conditions']): ?>
    var must_accept_term= true;
<?php else: ?>
    var must_accept_term= false;
<?php endif; ?>

var job_currency='<?php echo $this->_tpl_vars['job']->currency; ?>
';

var job_max_availability=<?php echo ((is_array($_tmp=@$this->_tpl_vars['cfg']->availability)) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
;
var job_date_format='<?php echo $this->_tpl_vars['cfg']->date_format; ?>
';
<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo $this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>