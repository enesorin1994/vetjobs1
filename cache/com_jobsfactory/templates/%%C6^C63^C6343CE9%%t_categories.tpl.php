<?php /* Smarty version 2.6.28, created on 2017-03-29 16:28:44
         compiled from t_categories.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_categories.tpl', 2, false),array('function', 'starttab', 't_categories.tpl', 8, false),array('function', 'endtab', 't_categories.tpl', 94, false),array('modifier', 'translate', 't_categories.tpl', 15, false),array('modifier', 'count', 't_categories.tpl', 25, false),)), $this); ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<h2><strong><?php echo $this->_tpl_vars['page_title']; ?>
</strong></h2>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/category/t_categories_tabs.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => 'job_tabmenu','id' => 'tab1'), $this);?>


        <div class="text-center" style = "width: 90%;"><?php echo $this->_tpl_vars['filter_letter']; ?>
</div>

    <?php if ($this->_tpl_vars['current_cat']): ?>
        <div class="job_cat_breadcrumb">
            <a href="<?php echo $this->_tpl_vars['links']->getCategoryRoute(); ?>
"><?php echo ((is_array($_tmp='COM_JOBS_ALL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
            <strong><?php echo $this->_tpl_vars['current_cat']->catname; ?>
</strong>
            <?php if ($this->_tpl_vars['current_cat']->description): ?>
                <div class="job_cat_description">
                    <?php echo $this->_tpl_vars['current_cat']->description; ?>

                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
<table class="job_categories" cellspacing="0" cellpadding="3">
<?php if (count($this->_tpl_vars['categories']) == 0): ?>
	<tr>
		<td><?php echo ((is_array($_tmp='COM_JOBS_THERE_ARE_NO_SUBCATEGORIES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</td>
	</tr>
<?php else: ?>
    <?php unset($this->_sections['category']);
$this->_sections['category']['name'] = 'category';
$this->_sections['category']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['category']['show'] = true;
$this->_sections['category']['max'] = $this->_sections['category']['loop'];
$this->_sections['category']['step'] = 1;
$this->_sections['category']['start'] = $this->_sections['category']['step'] > 0 ? 0 : $this->_sections['category']['loop']-1;
if ($this->_sections['category']['show']) {
    $this->_sections['category']['total'] = $this->_sections['category']['loop'];
    if ($this->_sections['category']['total'] == 0)
        $this->_sections['category']['show'] = false;
} else
    $this->_sections['category']['total'] = 0;
if ($this->_sections['category']['show']):

            for ($this->_sections['category']['index'] = $this->_sections['category']['start'], $this->_sections['category']['iteration'] = 1;
                 $this->_sections['category']['iteration'] <= $this->_sections['category']['total'];
                 $this->_sections['category']['index'] += $this->_sections['category']['step'], $this->_sections['category']['iteration']++):
$this->_sections['category']['rownum'] = $this->_sections['category']['iteration'];
$this->_sections['category']['index_prev'] = $this->_sections['category']['index'] - $this->_sections['category']['step'];
$this->_sections['category']['index_next'] = $this->_sections['category']['index'] + $this->_sections['category']['step'];
$this->_sections['category']['first']      = ($this->_sections['category']['iteration'] == 1);
$this->_sections['category']['last']       = ($this->_sections['category']['iteration'] == $this->_sections['category']['total']);
?>
      <?php if ((1 & $this->_sections['category']['rownum'])): ?>
        <tr>
      <?php endif; ?>
	        <td width="50%" valign="top">
                <div class="job_maincat">
                    <a href="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->link; ?>
"><?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->catname; ?>
</a><span style="font-weight: normal;font-size: 12px">(<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->nr_a; ?>
)</span>
                    <?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->nr_a > 0): ?>
                    <a href="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->view; ?>
">
                        <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
document-text-image.png" border="0" alt="" /></a>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['is_logged_in']): ?>
                        <a href="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->link_watchlist; ?>
">
                            <?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->watchListed_flag): ?>
                                <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_0.png" border="0" alt="" width="16" title="<?php echo ((is_array($_tmp='COM_JOBS_REMOVE_FROM_WATCHLIST_CAT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"/>
                            <?php else: ?>
                                <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_1.png" border="0" alt="" width="16" title="<?php echo ((is_array($_tmp='COM_JOBS_ADD_TO_WATCHLIST_CAT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"/>
                            <?php endif; ?>
                        </a>
                        <?php if ($this->_tpl_vars['isCompany']): ?>
                        <a href="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->link_new_listing; ?>
"><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
new_listing.png" border="0" alt="<?php echo ((is_array($_tmp='COM_JOBS_NEW_LISTING_IN_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                            title = "<?php echo ((is_array($_tmp='COM_JOBS_NEW_LISTING_IN_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="new_listing" /></a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="job_subcat_container">
                  <?php if (count($this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories)): ?>
                    <?php unset($this->_sections['subcategory']);
$this->_sections['subcategory']['name'] = 'subcategory';
$this->_sections['subcategory']['loop'] = is_array($_loop=$this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['subcategory']['show'] = true;
$this->_sections['subcategory']['max'] = $this->_sections['subcategory']['loop'];
$this->_sections['subcategory']['step'] = 1;
$this->_sections['subcategory']['start'] = $this->_sections['subcategory']['step'] > 0 ? 0 : $this->_sections['subcategory']['loop']-1;
if ($this->_sections['subcategory']['show']) {
    $this->_sections['subcategory']['total'] = $this->_sections['subcategory']['loop'];
    if ($this->_sections['subcategory']['total'] == 0)
        $this->_sections['subcategory']['show'] = false;
} else
    $this->_sections['subcategory']['total'] = 0;
if ($this->_sections['subcategory']['show']):

            for ($this->_sections['subcategory']['index'] = $this->_sections['subcategory']['start'], $this->_sections['subcategory']['iteration'] = 1;
                 $this->_sections['subcategory']['iteration'] <= $this->_sections['subcategory']['total'];
                 $this->_sections['subcategory']['index'] += $this->_sections['subcategory']['step'], $this->_sections['subcategory']['iteration']++):
$this->_sections['subcategory']['rownum'] = $this->_sections['subcategory']['iteration'];
$this->_sections['subcategory']['index_prev'] = $this->_sections['subcategory']['index'] - $this->_sections['subcategory']['step'];
$this->_sections['subcategory']['index_next'] = $this->_sections['subcategory']['index'] + $this->_sections['subcategory']['step'];
$this->_sections['subcategory']['first']      = ($this->_sections['subcategory']['iteration'] == 1);
$this->_sections['subcategory']['last']       = ($this->_sections['subcategory']['iteration'] == $this->_sections['subcategory']['total']);
?>
                        <div>
                            <a href="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->link; ?>
"><?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->catname; ?>
 (<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->nr_a; ?>
)</a>
                            <?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->nr_a > 0): ?>
                                <a href="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->view; ?>
">
                                    <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
document-text-image.png" border="0" alt="" /></a>
                            <?php endif; ?>

                            <?php if ($this->_tpl_vars['is_logged_in']): ?>
                                <a href="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->link_watchlist; ?>
">
                                    <?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->watchListed_flag): ?>
                                        <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_0.png" border="0" alt=""  width="16" title="<?php echo ((is_array($_tmp='COM_JOBS_REMOVE_FROM_WATCHLIST_CAT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"/>
                                    <?php else: ?>
                                        <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_1.png" border="0" alt="" width="16" title="<?php echo ((is_array($_tmp='COM_JOBS_ADD_TO_WATCHLIST_CAT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" />
                                    <?php endif; ?>
                                </a>
                                <?php if ($this->_tpl_vars['isCompany']): ?>
                                    <a href = "<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->link_new_listing; ?>
"><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
new_listing.png" border = "0"
                                        alt="<?php echo ((is_array($_tmp='COM_JOBS_NEW_LISTING_IN_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                                        title="<?php echo ((is_array($_tmp='COM_JOBS_NEW_LISTING_IN_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                                        class="new_listing" /></a>
                                <?php endif; ?>
                            <?php endif; ?>

                        </div>
                    <?php endfor; endif; ?>
                  <?php endif; ?>
                </div>
	</td>
<?php if (!((1 & $this->_sections['category']['rownum']))): ?>
</tr>
<?php endif; ?>
<?php endfor; endif; ?>
<?php endif; ?>
</table>

<div style="clear: both;"></div>
<?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>
