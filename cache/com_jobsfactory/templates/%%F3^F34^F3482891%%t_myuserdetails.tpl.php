<?php /* Smarty version 2.6.28, created on 2017-03-29 13:01:50
         compiled from t_myuserdetails.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'import_js_file', 't_myuserdetails.tpl', 3, false),array('function', 'createtabslist', 't_myuserdetails.tpl', 20, false),array('function', 'additemtabs', 't_myuserdetails.tpl', 21, false),array('function', 'endtabslist', 't_myuserdetails.tpl', 27, false),array('function', 'startpane', 't_myuserdetails.tpl', 29, false),array('function', 'starttab', 't_myuserdetails.tpl', 30, false),array('function', 'infobullet', 't_myuserdetails.tpl', 86, false),array('function', 'endtab', 't_myuserdetails.tpl', 275, false),array('function', 'endpane', 't_myuserdetails.tpl', 339, false),array('modifier', 'translate', 't_myuserdetails.tpl', 21, false),array('modifier', 'replace', 't_myuserdetails.tpl', 230, false),array('block', 'import_js_block', 't_myuserdetails.tpl', 198, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "job_edit.js"), $this);?>


<?php $this->assign('fieldRequired', $this->_tpl_vars['lists']['fieldRequiredlink']); ?>
<span class="v_spacer_15"></span>

<?php if ($this->_tpl_vars['paymentItems']): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/userprofile/t_profile_tabs.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>

<span class="v_spacer_15"></span>

<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "Stickman.MultiUpload.js"), $this);?>

<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "date.js"), $this);?>

<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "user_resume.js"), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/userprofile/t_profile_validate.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo $this->_plugins['function']['createtabslist'][0][0]->smarty_createtabslist(array('name' => "content-pane"), $this);?>

    <?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'usertab1','name' => "content-pane",'active' => '1','text' => ((is_array($_tmp='COM_JOBS_TAB_GENERAL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

  <?php if ($this->_tpl_vars['user']->userid): ?>
    <?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'usertab2','name' => "content-pane",'active' => '0','text' => ((is_array($_tmp='COM_JOBS_TAB_EDUCATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

    <?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'usertab3','name' => "content-pane",'active' => '0','text' => ((is_array($_tmp='COM_JOBS_TAB_EXPERIENCE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

    <?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'usertab4','name' => "content-pane",'active' => '0','text' => ((is_array($_tmp='COM_JOBS_TAB_OTHER')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

  <?php endif; ?>
<?php echo $this->_plugins['function']['endtabslist'][0][0]->smarty_endtabslist(array(), $this);?>


<?php echo $this->_plugins['function']['startpane'][0][0]->smarty_startpane(array('id' => "content-pane",'name' => "content-pane",'active' => 'usertab1','usecookies' => 0), $this);?>

    <?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => "content-pane",'id' => 'usertab1','text' => ((is_array($_tmp='COM_JOBS_TAB_GENERAL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>


<div class="last_dates">
    <span><?php if ($this->_tpl_vars['user']->last_modified != NULL): ?><?php echo ((is_array($_tmp='COM_JOBS_LAST_MODIFIED_ON')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php echo $this->_tpl_vars['user']->last_modified; ?>
<?php endif; ?></span>
    <span style="float:right;"><?php if ($this->_tpl_vars['user']->last_applied != NULL): ?><?php echo ((is_array($_tmp='COM_JOBS_LAST_APPLIED_ON')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php echo $this->_tpl_vars['user']->last_applied; ?>
<?php endif; ?></span>
</div>

<div class="jobs_userprofile">
<form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" class="form-validate">
	<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />
	<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
    <input type="hidden" name="resume_id" value="<?php echo $this->_tpl_vars['user']->id; ?>
" />

    <?php if ($this->_tpl_vars['userTable']->id): ?>
        <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['userTable']->id; ?>
" />
    <?php endif; ?>
    <input type="hidden" name="useruniqueid" value="<?php echo $this->_tpl_vars['user']->useruniqueid; ?>
" />
    <input type="hidden" name="isCompany" value="0" />
	<input type="hidden" name="task" value="saveUserDetails" />
	<input type="hidden" name="controller" value="user" />

    <div class="user_edit_header">
        <table width="100%" cellpadding="0" cellspacing="0">
        	<tr >
        		<td align="left">
        			<?php echo ((is_array($_tmp='COM_JOBS_EDIT_USER_DETAILS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 (<?php echo $this->_tpl_vars['user']->username; ?>
)
        		</td>
        		<td align="right">
                    <input name="save" value="<?php echo ((is_array($_tmp='COM_JOBS_SAVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="btn btn-primary validate save-button" type="submit" />
					<a class="btn btn-primary hasTooltip" data-toggle="tooltip" data-placement="top" title data-original-title="<?php echo ((is_array($_tmp='COM_JOBS_SAVE_BEFORE_SEND')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" id="emailresume" href="index.php?option=com_jobsfactory&task=emailresume&controller=user&id=<?php echo $this->_tpl_vars['user']->id; ?>
"><?php echo ((is_array($_tmp='COM_JOBS_EMAIL_RESUME_BUTTON')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<a/>
        		</td>
        	</tr>
        </table>
    </div>

    <div class="form-horizontal user_resume" id="generalprofile" style="border:solid 1px #DDDDDD; border-top:none; ">
        <div class="control-group">
            <div class="user_edit_section right">
                <?php echo ((is_array($_tmp='COM_JOBS_GENERAL_PROFILE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

			</div>
		</div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_NAME')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php echo $this->_tpl_vars['fieldRequired']; ?>
</label>
            <div class="controls">
				<input class="inputbox required" type="text" name="name" value="<?php echo $this->_tpl_vars['user']->name; ?>
" size="40" />
			</div>
		</div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_SURNAME')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
				<input class="inputbox" type="text" name="surname" value="<?php echo $this->_tpl_vars['user']->surname; ?>
" size="40" />
			</div>
		</div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_BIRTH_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php echo $this->_tpl_vars['fieldRequired']; ?>
</label>
            <div class="controls"><?php echo $this->_tpl_vars['lists']['birth_date_html']; ?>
 (<?php echo ((is_array($_tmp='COM_JOBS_AGE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php echo $this->_tpl_vars['user']->age; ?>
)
                &nbsp;<?php echo $this->_plugins['function']['infobullet'][0][0]->smarty_infobullet(array('text' => ((is_array($_tmp='user_birthdate_help')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_GENDER')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php echo $this->_tpl_vars['fieldRequired']; ?>
</label>
            <div class="controls1"><?php echo $this->_tpl_vars['lists']['gender']; ?>
<span style="color: red; float: left; width: 15px; padding-left: 3px;" title="<?php echo ((is_array($_tmp='COM_JOBS_FIELD_REQUIRED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="required_span"></span></div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_MARITAL_STATUS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <input class="inputbox" type="text" name="marital_status" value="<?php echo $this->_tpl_vars['user']->marital_status; ?>
" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_PHONE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
				<input class="inputbox" type="text" name="phone" value="<?php echo $this->_tpl_vars['user']->phone; ?>
" size="40" />
			</div>
		</div>
        <div class="control-group">
			<label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_EMAIL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
				<input class="inputbox" type="text" name="email" value="<?php echo $this->_tpl_vars['user']->email; ?>
" size="40" />
			</div>
		</div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_ADRESS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <input class="inputbox" type="text" name="address" value="<?php echo $this->_tpl_vars['user']->address; ?>
" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_CITY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <input class="inputbox" type="text" name="city" value="<?php echo $this->_tpl_vars['user']->city; ?>
" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_COUNTRY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <?php echo $this->_tpl_vars['lists']['country']; ?>

            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_COMPANY_IDENTIFICATION_NO')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <?php echo $this->_tpl_vars['user']->useruniqueid; ?>

            </div>
        </div>

        <div class="job_dbk_c form_bg0">&nbsp;</div>
		<?php if ($this->_tpl_vars['cfg']->allow_messenger): ?>
            <div class="control-group">
    			<label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_YM')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                <div class="controls">
    				<input class="inputbox" type="text" name="YM" value="<?php echo $this->_tpl_vars['user']->YM; ?>
" size="40" />
    			</div>
    		</div>
            <div class="control-group">
    			<label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_HOTMAIL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                <div class="controls">
    				<input class="inputbox" type="text" name="Hotmail" value="<?php echo $this->_tpl_vars['user']->Hotmail; ?>
" size="40" />
    			</div>
    		</div>
            <div class="control-group">
    			<label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_SKYPE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                <div class="controls">
    				<input class="inputbox" type="text" name="Skype" value="<?php echo $this->_tpl_vars['user']->Skype; ?>
" size="40" />
    			</div>
    		</div>
		<?php endif; ?>
        <div class="job_dbk_c form_bg0">&nbsp;</div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_LINKEDIN')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <input class="inputbox" type="text" name="linkedIN" value="<?php echo $this->_tpl_vars['user']->linkedIN; ?>
" size="40" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_FACEBOOK')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <input class="inputbox" type="text" name="facebook" value="<?php echo $this->_tpl_vars['user']->facebook; ?>
" size="40" />
                &nbsp;<?php echo $this->_plugins['function']['infobullet'][0][0]->smarty_infobullet(array('text' => ((is_array($_tmp='user_facebook_help')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

            </div>
        </div>

        <?php if ($this->_tpl_vars['cfg']->enable_images): ?>
                        <?php if ($this->_tpl_vars['user']->picture): ?>
                <div class="control-group">
                    <label class="control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_PICTURE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php if ($this->_tpl_vars['cfg']->main_picture_require): ?><?php echo $this->_tpl_vars['fieldRequired']; ?>
<?php endif; ?></label>
                    <div class="controls">
                        <img src="<?php echo $this->_tpl_vars['RESUME_PICTURES']; ?>
/resize_<?php echo $this->_tpl_vars['user']->picture; ?>
" />
                        <span style = "float: left;">
                            <?php if ($this->_tpl_vars['cfg']->main_picture_require): ?>
                                <input type = "checkbox" name = "replace_my_file_element" value = "1" checked="checked" />
                                <?php echo ((is_array($_tmp='COM_JOBS_REPLACE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                                <br />
                                <input class="inputbox" id="my_file_element" type="file" name="picture_0" />
                            <?php else: ?>
                                <input type = "checkbox" name = "delete_main_picture" id = "my_file_element" value = "1"/><?php echo ((is_array($_tmp='COM_JOBS_DELETE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                            <?php endif; ?>
                        </span>
                                            </div>
                </div>
            <?php else: ?>
                <div class="control-group">
                    <label class="control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_ATTACH_PHOTO')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php if ($this->_tpl_vars['cfg']->main_picture_require): ?><?php echo $this->_tpl_vars['fieldRequired']; ?>
<?php endif; ?></label><br/>
                    <small style="color: Grey;"><?php echo ((is_array($_tmp='COM_JOBS_PICTURE_MAX_SIZE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:<?php echo $this->_tpl_vars['cfg']->max_picture_size; ?>
k</small>
                    <div class="controls">
                        <input class="inputbox<?php if ($this->_tpl_vars['cfg']->main_picture_require): ?> required<?php endif; ?>" id="my_file_element" type="file" name="picture" />
                        <?php $this->_tag_stack[] = array('import_js_block', array()); $_block_repeat=true;$this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
                            <?php echo '
                            window.addEvent(\'domready\', function(){
                                new MultiUpload( $( \'my_file_element\' ), 1, \'_{id}\', true, true ,'; ?>
'<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
'<?php echo ');
                            });
                            '; ?>

                        <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo $this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <?php if ($this->_tpl_vars['cfg']->uploadresume_option): ?>
                        <div class="control-group">
                <div class="job_edit_section">
                    <?php echo ((is_array($_tmp='COM_JOBS_RESUMES_UPLOAD')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                </div>
            </div>
            <div class="control-group">
                 <?php if ($this->_tpl_vars['user']->file_name): ?>
                    <label class="control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_ATTACHMENT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>&nbsp;<br />
                    <div class="controls">
                        <strong><a title="userAttachment" href="<?php echo $this->_tpl_vars['userTable']->get('links.download_file'); ?>
" target="_blank" name="show_att" id="show_att" >
                                    <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
cv.png" border="0" title="<?php echo ((is_array($_tmp='COM_JOBS_PDF_RESUME')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" /><?php echo $this->_tpl_vars['user']->file_name; ?>
</a>
                        </strong>&nbsp;&nbsp;&nbsp;
                       <a href="<?php echo $this->_tpl_vars['userTable']->get('links.deletefile_file'); ?>
"><?php echo ((is_array($_tmp='COM_JOBS_DELETE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
                    </div>
                 <?php else: ?>
                    <label class="control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_ATTACHMENT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                    <div class="controls">
                        <input name="attachment" class="inputbox<?php if ($this->_tpl_vars['cfg']->attach_compulsory): ?> required<?php endif; ?>"  type="file" /><br />
                        <small style="color: Grey;"><?php echo ((is_array($_tmp=((is_array($_tmp='COM_JOBS_MAXIMUM_FILE_SIZE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)))) ? $this->_run_mod_handler('replace', true, $_tmp, "%s", $this->_tpl_vars['cfg']->attach_max_size) : smarty_modifier_replace($_tmp, "%s", $this->_tpl_vars['cfg']->attach_max_size)); ?>
</small>
                        <?php if ($this->_tpl_vars['cfg']->resume_extensions): ?>
                            <br /><small style="color: Grey;"><?php echo ((is_array($_tmp='COM_JOBS_ALLOWED_ATTACHMENT_EXTENSIONS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo $this->_tpl_vars['cfg']->resume_extensions; ?>
</small>
                        <?php endif; ?>
                    </div>
                 <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="job_dbk_c form_bg0">&nbsp;</div>
        <div class="control-group">
            <label class="control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_TOTAL_EXPERIENCE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                 <?php echo $this->_tpl_vars['lists']['total_experience']; ?>

            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_SALARY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <input class="inputbox" type="text" name="desired_salary" value="<?php echo $this->_tpl_vars['user']->desired_salary; ?>
" size="20" />
                <?php echo $this->_tpl_vars['lists']['default_currency']; ?>

            </div>
        </div>
        <div class="control-group">
            <label class="control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_PROFILE_VISIBILITY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <?php echo $this->_tpl_vars['lists']['isVisible']; ?>

            </div>
        </div>
        <div class="job_dbk_c form_bg0">&nbsp;</div>

        <div id="resume" class="custom_fields_box">
            <div class="pull-left">
                 <?php echo $this->_tpl_vars['custom_fields_html']; ?>

            </div>
        </div>
    </div>

    <div class="btn-toolbar form-horizontal user_edit_section">
        <div class="btn-group controls">
            <input type="submit" name="save" value="<?php echo ((is_array($_tmp='COM_JOBS_SAVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="validate btn btn-primary save-button" />&nbsp;&nbsp;
        </div>
    </div>
</form>

</div>
<?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>


<!-- NEW TAB EDUCATION -->
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 't_myusereducation.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-- NEW TAB EXPERIENCE -->
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 't_myuserexperience.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- NEW TAB OTHER -->
<?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => "content-pane",'id' => 'usertab4','text' => ((is_array($_tmp='COM_JOBS_TAB_OTHER')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

    <form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" >
	<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />
	<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
    <input type="hidden" name="resume_id" value="<?php echo $this->_tpl_vars['user']->id; ?>
" />
	<input type="hidden" name="task" value="saveUserSkills" />
	<input type="hidden" name="controller" value="user" />

    <div class="user_edit_header">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr >
                <td align="left">
                    <?php echo ((is_array($_tmp='COM_JOBS_EDIT_USER_OTHER')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                </td>
            </tr>
        </table>
    </div>

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
           <td class="job_dbk" align="right">
               <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_USER_ACHIEVEMENTS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
           </td>
           <td class="job_dbk_c">              <textarea name="user_achievements" id="user_achievements" rows="2" cols="4"
                    style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" ><?php echo $this->_tpl_vars['user']->user_achievements; ?>
</textarea>
           </td>
        </tr>
        <tr>
           <td class="job_dbk" align="right" width="150">
               <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_USER_GOALS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
           </td>
           <td class="job_dbk">
               <textarea name="user_goals" id="user_goals" rows="2" cols="4"
                    style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" ><?php echo $this->_tpl_vars['user']->user_goals; ?>
</textarea>
           </td>
       </tr>
       <tr>
           <td class="job_dbk" align="right">
               <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_USER_HOBBIES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
           </td>
           <td class="job_dbk_c">
               <textarea name="user_hobbies" id="user_hobbies" rows="2" cols="4"
                   style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" ><?php echo $this->_tpl_vars['user']->user_hobbies; ?>
</textarea>
           </td>
       </tr>
        <tr>
            <td align="right" class="user_edit_section"  colspan="2" >
                 <input name="save" value="<?php echo ((is_array($_tmp='COM_JOBS_SAVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="btn btn-primary validate" type="submit" />
            </td>
        </tr>
    </table>
    </form>
<?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>


<?php echo $this->_plugins['function']['endpane'][0][0]->smarty_endpane(array('name' => "content-pane",'id' => "content-pane"), $this);?>
