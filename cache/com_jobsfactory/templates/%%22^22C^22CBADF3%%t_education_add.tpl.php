<?php /* Smarty version 2.6.28, created on 2017-03-29 13:01:50
         compiled from elements/resumedetail/t_education_add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 'elements/resumedetail/t_education_add.tpl', 10, false),)), $this); ?>
<form id="add_education" name="add_education" enctype="multipart/form-data" action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php" method="POST" >
	<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['user_education']->id; ?>
" />
    <input type="hidden" name="resume_id" value="<?php echo $this->_tpl_vars['user']->id; ?>
" />
	<input type="hidden" name="task" value="saveEducation" />
	<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />

	<div class="form-horizontal jobs_top_align" style="border:solid 1px #DDDDDD; border-top:none; ">    
        <div class="control-group">
             <div class="job_edit_section">
                <?php echo ((is_array($_tmp='COM_JOBS_USER_EDUCATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_STUDY_LEVEL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <?php echo $this->_tpl_vars['lists']['studies_level']; ?>

            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_EDU_INSTITUTION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <input class="inputbox input-xlarge" name="edu_institution" type="text" size="60" value="<?php echo $this->_tpl_vars['user_education']->edu_institution; ?>
">
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_COUNTRY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <?php echo $this->_tpl_vars['lists']['country']; ?>

            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_CITY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <input class="inputbox input-xlarge" name="city" type="text" size="60" value="<?php echo $this->_tpl_vars['user_education']->city; ?>
">
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_START_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <?php echo $this->_tpl_vars['lists']['new_edu_start_date_html']; ?>

            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_END_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <?php echo $this->_tpl_vars['lists']['new_edu_end_date_html']; ?>

            </div>
        </div>
        <div class="control-group job_edit_section">
            <div class="pull-right">
                <input type="submit" name="saveeducation" value="<?php echo ((is_array($_tmp='COM_JOBS_SAVE_EDUCATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="btn btn-primary validate" />
                <a href="#" class="cancel_education btn" rel="<?php echo $this->_tpl_vars['user']->id; ?>
" >&nbsp;<?php echo ((is_array($_tmp='COM_JOBS_CANCEL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
            </div>
        </div>
    </div>

</form>
