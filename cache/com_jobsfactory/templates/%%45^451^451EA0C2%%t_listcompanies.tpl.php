<?php /* Smarty version 2.6.28, created on 2017-03-29 12:58:00
         compiled from t_listcompanies.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_listcompanies.tpl', 1, false),array('function', 'import_js_file', 't_listcompanies.tpl', 3, false),array('modifier', 'translate', 't_listcompanies.tpl', 6, false),)), $this); ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "jobs.js"), $this);?>


<h2>
<?php echo ((is_array($_tmp='COM_JOBS_COMPANIES_LIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

	<a href="index.php?option=com_jobsfactory&task=<?php echo $this->_tpl_vars['task']; ?>
&format=feed&limitstart=" target="_blank">
		<img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_rss.jpg" width="14" border="0" alt="RSS" />
	</a>
</h2>


<form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
" method="get" name="jobsForm" id="jobsForm">
<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
<input type="hidden" name="task" value="<?php echo $this->_tpl_vars['task']; ?>
" />
<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/t_header_filter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="job_list_container table-striped">
  <tr style="height: 30px;">
    <th style="width: auto; text-align: left;" class="title_grey">
        <?php echo ((is_array($_tmp='COM_JOBS_LOGO')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

    </th>
	<th style="width: *%; text-align: left;" class="title_grey"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='Name')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'name')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> /
        <?php echo ((is_array($_tmp='COM_JOBS_ACTIVITY_DOMAIN')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

	</th>
    <?php if ($this->_tpl_vars['jobs_allow_rating']): ?>
        <th style="width: auto; text-align: left;">
            <span class="title_grey"> <?php echo ((is_array($_tmp='COM_JOBS_RATINGS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span>
        </th>
    <?php endif; ?>
	<th style="width: 120px; text-align: left;">
	    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/sort_field.tpl', 'smarty_include_vars' => array('label' => ((is_array($_tmp='Location')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)),'key' => 'city')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        <span class="title_grey"> / <?php echo ((is_array($_tmp='COM_JOBS_AVAILABLE_POSTS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span>
	</th>
  </tr>
	<?php unset($this->_sections['companiesloop']);
$this->_sections['companiesloop']['name'] = 'companiesloop';
$this->_sections['companiesloop']['loop'] = is_array($_loop=$this->_tpl_vars['companies_rows']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['companiesloop']['show'] = true;
$this->_sections['companiesloop']['max'] = $this->_sections['companiesloop']['loop'];
$this->_sections['companiesloop']['step'] = 1;
$this->_sections['companiesloop']['start'] = $this->_sections['companiesloop']['step'] > 0 ? 0 : $this->_sections['companiesloop']['loop']-1;
if ($this->_sections['companiesloop']['show']) {
    $this->_sections['companiesloop']['total'] = $this->_sections['companiesloop']['loop'];
    if ($this->_sections['companiesloop']['total'] == 0)
        $this->_sections['companiesloop']['show'] = false;
} else
    $this->_sections['companiesloop']['total'] = 0;
if ($this->_sections['companiesloop']['show']):

            for ($this->_sections['companiesloop']['index'] = $this->_sections['companiesloop']['start'], $this->_sections['companiesloop']['iteration'] = 1;
                 $this->_sections['companiesloop']['iteration'] <= $this->_sections['companiesloop']['total'];
                 $this->_sections['companiesloop']['index'] += $this->_sections['companiesloop']['step'], $this->_sections['companiesloop']['iteration']++):
$this->_sections['companiesloop']['rownum'] = $this->_sections['companiesloop']['iteration'];
$this->_sections['companiesloop']['index_prev'] = $this->_sections['companiesloop']['index'] - $this->_sections['companiesloop']['step'];
$this->_sections['companiesloop']['index_next'] = $this->_sections['companiesloop']['index'] + $this->_sections['companiesloop']['step'];
$this->_sections['companiesloop']['first']      = ($this->_sections['companiesloop']['iteration'] == 1);
$this->_sections['companiesloop']['last']       = ($this->_sections['companiesloop']['iteration'] == $this->_sections['companiesloop']['total']);
?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/lists/t_listcompanies_cell.tpl', 'smarty_include_vars' => array('company' => ($this->_tpl_vars['companies_rows'][$this->_sections['companiesloop']['index']]),'index' => ($this->_sections['companiesloop']['rownum']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endfor; endif; ?>
</table>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/t_listfooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</form>
<div style="clear: both;"></div>