<?php /* Smarty version 2.6.28, created on 2017-03-29 13:01:50
         compiled from t_myuserexperience.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_myuserexperience.tpl', 3, false),array('function', 'starttab', 't_myuserexperience.tpl', 29, false),array('function', 'jtext', 't_myuserexperience.tpl', 93, false),array('function', 'printdate', 't_myuserexperience.tpl', 129, false),array('function', 'endtab', 't_myuserexperience.tpl', 166, false),array('modifier', 'translate', 't_myuserexperience.tpl', 29, false),array('modifier', 'date_format', 't_myuserexperience.tpl', 130, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>


<?php echo '
<script>
  jQuery(document).ready( function ($)
  {
      $(\'.edit_experience\').click( function (event)
     {
         event.preventDefault();
         var id = $(this).attr("rel");

       //$(\'#experience_form\' + id).show("normal");
       $.post(root + "index.php?option=com_jobsfactory&task=editexperience&format=raw", {
           id:    id,
           resume_id:     resumeid },
           function(data){
           //$(\'#photobattle-report-loading\').hide();
           $(\'#row_edit_experience\' + id).html(data);
       });

       return false;
     });
  });
</script>
'; ?>

        <?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => "content-pane",'id' => 'usertab3','text' => ((is_array($_tmp='COM_JOBS_TAB_EXPERIENCE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>


<div class="jobs_userprofile">
<form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php" method="post" name="rJobForm" id="rJobForm" enctype="multipart/form-data" >
	<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />
	<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
    <input type="hidden" name="resume_id" value="<?php echo $this->_tpl_vars['user']->id; ?>
" />
	<input type="hidden" name="task" value="saveExperience" />

    <div class="user_edit_header">
        <table width="100%" cellpadding="0" cellspacing="0">
        	<tr >
        		<td align="left">
        			<?php echo ((is_array($_tmp='COM_JOBS_EDIT_USER_EXPERIENCE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

        		</td>
        	</tr>
        </table>
    </div>

    <!--resume_experience-->
    <a title="resume_experience" name="resume_experience" id="resume_experience"></a>

    <?php if ($this->_tpl_vars['task'] == 'userdetails'): ?>
        <div class="jobsfactory_icon jobsfactory_experience_add" style="padding: 2px;">
             <a href="#" class="show_experience_form" id="show_experience_form<?php echo $this->_tpl_vars['user']->id; ?>
" rel="<?php echo $this->_tpl_vars['user']->id; ?>
">
                 <?php echo ((is_array($_tmp='COM_JOBS_EXPERIENCE_ADD')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </div>
    <?php endif; ?>
    <span style="display: none; padding: 5px;" class="jobsfactory_experience_form" id="experience_form<?php echo $this->_tpl_vars['user']->id; ?>
">
    	<div>
    		<table cellspacing="0" cellpadding="0" width="80%">
    		  <tr>
    			<td class="experience_column">
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/resumedetail/t_experience_add.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                </td>
    		  </tr>
    		</table>
    	</div>
    </span>

<?php if ($this->_tpl_vars['lists']['countexp'] > 0 && $this->_tpl_vars['task'] == 'userdetails'): ?>

  <table width="100%" cellpadding="0" cellspacing="0" style="border:solid 1px #DDDDDD; border-top:none; " >
	<tr>
		<td align="right" class="resume_edit_section" colspan="2" >
            <?php echo ((is_array($_tmp='COM_JOBS_RESUME_EXPERIENCE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

		</td>
	</tr>
  </table>

    <?php $_from = $this->_tpl_vars['array_experience']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['user_experience']):
?>
    <div id="row_edit_experience<?php echo $this->_tpl_vars['user_experience']->id; ?>
" >
    <table class="no_border user_resume table-striped" width="100%" cellpadding="0" cellspacing="0" >
        <tr>
           <td class="job_dbk" align="right">
               <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_COMPANY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
           </td>
           <td class="job_dt job_values1">
               <?php echo $this->_tpl_vars['user_experience']->company; ?>

           </td>
           <td>
               <span style="float: right">
                    <a class="edit_experience1" href="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&task=editexperience&id=<?php echo $this->_tpl_vars['user_experience']->id; ?>
&resume_id=<?php echo $this->_tpl_vars['user']->id; ?>
">
                    <input name="edit" value='<?php echo ((is_array($_tmp='COM_JOBS_EDIT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
' class="btn btn-success" type="button" /></a>
                    <a onclick="return confirm('<?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_JOBS_CONFIRM_DELETE'), $this);?>
')" href="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&task=delete_experience&id=<?php echo $this->_tpl_vars['user_experience']->id; ?>
&resume_id=<?php echo $this->_tpl_vars['user']->id; ?>
" class="delete_experience" rel="<?php echo $this->_tpl_vars['user_experience']->id; ?>
" >
                        <input name="del" value='<?php echo ((is_array($_tmp='COM_JOBS_DELETE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
' class="btn btn-danger" type="button" /></a>
               </span>
           </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_POSITION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                <?php echo $this->_tpl_vars['user_experience']->position; ?>

            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_EXP_RESPONSIBILITIES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                <?php echo $this->_tpl_vars['user_experience']->exp_description; ?>

            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_DOMAIN')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                <?php $this->assign('domain', $this->_tpl_vars['user_experience']->domain); ?>
                <?php echo $this->_tpl_vars['user_experienceTable']->getCatname($this->_tpl_vars['domain']); ?>

            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_START_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                <?php if ($this->_tpl_vars['task'] == 'userdetails'): ?>
                    <?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['user_experience']->start_date), $this);?>

                    <input type="hidden" name="start_date" id="start_date" size="15" maxlength="19" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['user_experience']->start_date)) ? $this->_run_mod_handler('date_format', true, $_tmp, '%Y-%m-%d %H:%M:%S') : smarty_modifier_date_format($_tmp, '%Y-%m-%d %H:%M:%S')); ?>
" />
                <?php else: ?>
                    <?php echo $this->_tpl_vars['lists']['exp_start_date_html']; ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_END_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                <?php if ($this->_tpl_vars['task'] == 'userdetails'): ?>
                    <?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['user_experience']->end_date), $this);?>

                    <input type="hidden" name="end_date" id="end_date" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['user_experience']->end_date)) ? $this->_run_mod_handler('date_format', true, $_tmp, '%Y-%m-%d %H:%M:%S') : smarty_modifier_date_format($_tmp, '%Y-%m-%d %H:%M:%S')); ?>
" />
                <?php else: ?>
                    <?php echo $this->_tpl_vars['lists']['exp_end_date_html']; ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td class="job_dbk" align="right">
                <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_CITY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            </td>
            <td class="job_dt job_values1" colspan="2">
                <?php echo $this->_tpl_vars['user_experience']->city; ?>

            </td>
        </tr>
        <tr><td colspan="3" class="job_dbk_c form_bg0">&nbsp;</td></tr>
        </table>
      </div>
    <?php endforeach; endif; unset($_from); ?>
<?php endif; ?>

</form>
</div>

<?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>