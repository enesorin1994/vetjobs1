<?php /* Smarty version 2.6.28, created on 2017-03-29 13:01:50
         compiled from elements/resumedetail/t_experience_add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 'elements/resumedetail/t_experience_add.tpl', 11, false),)), $this); ?>
<form id="add_experience" name="add_experience" enctype="multipart/form-data" action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php" method="POST" >
	<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['user_experience']->id; ?>
" />
    <input type="hidden" name="resume_id" value="<?php echo $this->_tpl_vars['user']->id; ?>
" />
	<input type="hidden" name="task" value="saveExperience" />
	<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />

    <div class="form-horizontal jobs_top_align" style="border:solid 1px #DDDDDD; border-top:none; ">

        <div class="control-group">
            <div class="job_edit_section">
                <?php echo ((is_array($_tmp='COM_JOBS_RESUME_EXPERIENCE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_COMPANY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <input class="inputbox input-xlarge" name="company" type="text" size="60" value="<?php echo $this->_tpl_vars['user_experience']->company; ?>
" />
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_POSITION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
               <input class="inputbox input-xlarge" name="position" type="text" size="60" value="<?php echo $this->_tpl_vars['user_experience']->position; ?>
" />
           </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_EXP_RESPONSIBILITIES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <textarea name="exp_description" id="exp_description" rows="2" cols="4"
                                style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" ></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_DOMAIN')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <?php echo $this->_tpl_vars['lists']['domain']; ?>

            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_START_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <?php echo $this->_tpl_vars['lists']['exp_start_date_html']; ?>

            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_END_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <?php echo $this->_tpl_vars['lists']['exp_end_date_html']; ?>

            </div>
        </div>
        <div class="control-group">
            <label class = "control-label job_lables"><?php echo ((is_array($_tmp='COM_JOBS_CITY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
            <div class="controls">
                <input class="inputbox input-xlarge" name="city" type="text" size="60" value="<?php echo $this->_tpl_vars['user_experience']->city; ?>
" />
            </div>
        </div>
        <div class="control-group job_edit_section">
            <div class="pull-right">
                  <input type="submit" name="saveexperience" value="<?php echo ((is_array($_tmp='COM_JOBS_SAVE_EXPERIENCE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="btn btn-primary validate" />
                <a href="#" class="cancel_experience btn" rel="<?php echo $this->_tpl_vars['user']->id; ?>
" >&nbsp;<?php echo ((is_array($_tmp='COM_JOBS_CANCEL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
            </div>
        </div>
    </div>

</form>
