<?php /* Smarty version 2.6.28, created on 2017-03-29 12:34:35
         compiled from t_jobdetails.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'startpane', 't_jobdetails.tpl', 2, false),array('function', 'createtabslist', 't_jobdetails.tpl', 4, false),array('function', 'additemtabs', 't_jobdetails.tpl', 5, false),array('function', 'endtabslist', 't_jobdetails.tpl', 12, false),array('function', 'starttab', 't_jobdetails.tpl', 14, false),array('function', 'positions', 't_jobdetails.tpl', 87, false),array('function', 'printdate', 't_jobdetails.tpl', 201, false),array('function', 'jtext', 't_jobdetails.tpl', 263, false),array('function', 'endtab', 't_jobdetails.tpl', 341, false),array('function', 'endpane', 't_jobdetails.tpl', 348, false),array('modifier', 'translate', 't_jobdetails.tpl', 5, false),array('modifier', 'count', 't_jobdetails.tpl', 121, false),)), $this); ?>
<?php echo $this->_plugins['function']['startpane'][0][0]->smarty_startpane(array('id' => "jobdetail-pane",'name' => "jobdetail-pane",'active' => 'tab1','usecookies' => 0), $this);?>


<?php echo $this->_plugins['function']['createtabslist'][0][0]->smarty_createtabslist(array('name' => "jobdetail-pane"), $this);?>

    <?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'tab1','name' => "jobdetail-pane",'active' => '1','text' => ((is_array($_tmp='COM_JOBS_TAB_JOB_DETAIL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

    <?php if (( $this->_tpl_vars['cfg']->google_key != "" ) && $this->_tpl_vars['cfg']->map_in_job_details && ( ( $this->_tpl_vars['job']->googlex && $this->_tpl_vars['job']->googley ) || ( $this->_tpl_vars['user']->googleMaps_x && $this->_tpl_vars['user']->googleMaps_y ) )): ?>
        <?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'tab2','name' => "jobdetail-pane",'active' => '0','text' => ((is_array($_tmp='Location on Map')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

    <?php endif; ?>
	<?php if ($this->_tpl_vars['job']->isMyJob() && $this->_tpl_vars['job']->messages): ?>
	<?php echo $this->_plugins['function']['additemtabs'][0][0]->smarty_additemtabs(array('id' => 'tab3','name' => "jobdetail-pane",'active' => '0','text' => ((is_array($_tmp='COM_JOBS_TAB_SYSTEM_MESSAGES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

	<?php endif; ?>
<?php echo $this->_plugins['function']['endtabslist'][0][0]->smarty_endtabslist(array(), $this);?>


  <?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => "jobdetail-pane",'id' => 'tab1','text' => ((is_array($_tmp='COM_JOBS_TAB_JOB_DETAIL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>


    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_countdown.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <?php if ($this->_tpl_vars['payment_items_header']): ?>
        <div class="jobs_headerinfo" xmlns = "http://www.w3.org/1999/html"><?php echo $this->_tpl_vars['payment_items_header']; ?>
</div>
    <?php endif; ?>
                                <!-- [+]Job Container -->
    <div class="jobContainer">

        <div class="job_details span12 detail_job_header grey_line1">
                        <div class="grey_line1">
                <div class="job_details span8">
                    <?php if ($this->_tpl_vars['job']->get('catname')): ?>
                        <a href="<?php echo $this->_tpl_vars['job']->get('links.filter_cat'); ?>
">
                            <img src="<?php echo $this->_tpl_vars['TEMPLATE_IMAGES']; ?>
folder.png" style="border:0; margin:1px; vertical-align:top;"/> <?php echo $this->_tpl_vars['job']->get('catname'); ?>
</a>
                    <?php else: ?> &nbsp;-&nbsp;
                    <?php endif; ?>
                </div>
                <div class="job_details span4">
                    <span style="display:inline-block;float:right;"><a class="" href="<?php echo $this->_tpl_vars['job']->get('links.jobs_listing'); ?>
"><?php echo ((is_array($_tmp='COM_JOBS_BACK_TO_LIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a></span>
                </div>
            </div>

            <div class="job_detail_text1">
                <div>
                    <div class="job_title_details">
                        <div class="job_details span6">
                            <?php if ($this->_tpl_vars['job']->isMyJob() && ! $this->_tpl_vars['job']->published): ?>
                              <span class="job_title_details"><i><?php echo $this->_tpl_vars['job']->title; ?>
</i> (<?php echo ((is_array($_tmp='COM_JOBS_UNPUBLISHED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
)</span>
                            <?php else: ?>
                              <span class="job_title_details"><?php echo $this->_tpl_vars['job']->title; ?>
</span>
                            <?php endif; ?>
                        </div>
                        <div class="job_details span6"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/jobdetail/t_action_buttons.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
                    </div>
                    <div>
                        <div class="detail_job_description span5">
                            <div class="box_content current">
                                <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_JOB_VALID_UP_TO')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                                &nbsp;<?php echo $this->_tpl_vars['job']->get('enddate_text'); ?>

                                <?php if ($this->_tpl_vars['job']->get('expired')): ?>
                                  <span class="job_expired"><?php echo ((is_array($_tmp='COM_JOBS_JOB_EXPIRED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="job_details span2">
                            <div class="box_content current">
                                <?php if ($this->_tpl_vars['job']->featured != 'none'): ?>
                                    <span class="featured_job_detail"><label><?php echo $this->_tpl_vars['job']->featured; ?>
</label></span>                                <?php else: ?>
                                    &nbsp;
                                <?php endif; ?>
                           </div>
                        </div>
                        <div class="detail_job_description span5" align="right1">
                            <div class="box_content current pull-right">
                                <label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_JOB_UPDATED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                                &nbsp;<?php echo $this->_tpl_vars['job']->get('modifieddate_text'); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div class="jobs_position_header"  style="border-bottom: none">
                   <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => 'header','item' => $this->_tpl_vars['job'],'page' => 'jobs'), $this);?>

                </div>
            </div>
        </div>

        <div class="job_details span12">
                            <?php if ($this->_tpl_vars['job']->isMyJob()): ?>
                    <div class="job_details span12" style="padding-right: 13px;">
                        <div class="box_bottom" style = "text-align: center;">
                            <?php if ($this->_tpl_vars['job']->show_candidate_nr !== '0'): ?>
                                <span class="jobs_heading_status">
                                    <?php echo ((is_array($_tmp='COM_JOBS_NR_CANDIDATES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:
                                </span>
                                <label class="job_lables" style="font-size: 18px !important; float: none;">
                                    <a href="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&task=companylistcandidates&id=<?php echo $this->_tpl_vars['job']->id; ?>
&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
">
                                        <?php echo $this->_tpl_vars['job']->get('nr_applicants'); ?>

                                    </a>
                                </label>
                            <?php endif; ?>
                        </div>

                        <span class="v_spacer_5"></span>
                        </div>
                <?php else: ?>
                    <div class="job_details span12" style="padding-right: 5px;">
                        <?php if ($this->_tpl_vars['job']->close_offer == 0 && $this->_tpl_vars['job']->get('expired') != 1): ?>
                           <?php if ($this->_tpl_vars['is_logged_in'] && $this->_tpl_vars['lists']['hasApplied'] == 0): ?>
                                <div class="box_content" style="text-align: left;">
                                   <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/jobdetail/t_apply.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                </div>
                           <?php elseif ($this->_tpl_vars['is_logged_in'] && $this->_tpl_vars['lists']['hasApplied'] > 0): ?>
                                <div class="box_content applied-msg">
                                   <h3><?php echo ((is_array($_tmp='COM_JOBS_YOU_ALREADY_APPLIED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</h3>
                                   <?php if (count($this->_tpl_vars['jobs'])): ?>
                                       <h4><?php echo ((is_array($_tmp='COM_JOBS_HAVE_APPLIED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo count($this->_tpl_vars['jobs']); ?>
</h4>
                                   <?php endif; ?>
                                </div>
                            <?php else: ?>
                                <div class="box_app_details"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/jobdetail/t_app_guest.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                    </div>

        <div style="clear: both;"></div>
        <div class="add_detail_socialtoolbar span12" style="padding-right: 3px;">
           <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/jobdetail/t_details_job_social.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </div>
        <div class="job_details span12 grey_line1">
            <div>
                                <div class="detail_job_description span7">
                    <div class="box_content">
                        <div style="text-align: justify;" class="job_details_info">

                            <?php echo $this->_tpl_vars['job']->description; ?>


                            <span class="v_spacer_5"></span>

                            <?php if ($this->_tpl_vars['job']->file_name != ""): ?>
                                <a href="<?php echo $this->_tpl_vars['job']->get('links.download_file'); ?>
" target="_blank" >
                                    <img src="<?php echo $this->_tpl_vars['TEMPLATE_IMAGES']; ?>
attach.gif" style="border:0; margin:1px; vertical-align:middle;"/>
                                    <?php echo $this->_tpl_vars['job']->file_name; ?>

                                </a>
                            <?php endif; ?>
                            <span class="v_spacer_5"></span>
                        </div>
                        <div class="box_bottom"></div>
                    </div>
                    <span class="v_spacer_5"></span>

                    <div class="job_details jobs_position">
                        <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "detail-left",'item' => $this->_tpl_vars['job'],'page' => 'jobs'), $this);?>

                    </div>
                    <div style="clear: both;"></div>
                </div>

                                <div class="detail_job_info job_details span5">
                    <div class="box_title"><?php echo ((is_array($_tmp='COM_JOBS_COMPANY_DETAILS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</div>

                    <div width="100%" class="box_right_details">
                        <div class="span12">
                            <label class="job_lables pull-left">
                            <?php if ($this->_tpl_vars['cfg']->employervisibility_enable || $this->_tpl_vars['job']->id): ?>
                                 <?php if ($this->_tpl_vars['job']->employer_visibility == 1): ?>
                                      <a href = "<?php echo $this->_tpl_vars['job']->get('links.employer_profile'); ?>
">
                                        <?php if ($this->_tpl_vars['employer']->name != ''): ?><?php echo $this->_tpl_vars['employer']->name; ?>

                                        <?php else: ?>
                                            <?php echo $this->_tpl_vars['employer']->username; ?>

                                        <?php endif; ?>
                                          <?php if ($this->_tpl_vars['employer']->picture): ?>
                                            <img src="<?php echo $this->_tpl_vars['COMPANY_PICTURES']; ?>
/resize_<?php echo $this->_tpl_vars['employer']->picture; ?>
" width="100" style="float: right;" />
                                          <?php else: ?>
                                            <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
/user/logo.png" width="100" style="float: right;" />
                                          <?php endif; ?>
                                      </a>
                                 <?php else: ?>
                                     <?php if ($this->_tpl_vars['job']->employer_visibility == $this->_tpl_vars['EMPLOYER_VISIBILITIES']['EMPLOYER_TYPE_CONFIDENTIAL']): ?><?php echo ((is_array($_tmp='COM_JOBS_CONFIDENTIAL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php else: ?><?php echo ((is_array($_tmp='COM_JOBS_PUBLIC')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php endif; ?>
                                 <?php endif; ?>
                            <?php else: ?>
                                 <strong><?php if ($this->_tpl_vars['cfg']->employervisibility_val == $this->_tpl_vars['EMPLOYER_VISIBILITIES']['EMPLOYER_TYPE_CONFIDENTIAL']): ?><?php echo ((is_array($_tmp='COM_JOBS_CONFIDENTIAL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php else: ?><?php echo ((is_array($_tmp='COM_JOBS_PUBLIC')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
<?php endif; ?></strong>
                            <?php endif; ?>
                            </label>
                            <span class="left_spacer_10"></span>
                            <a href = "<?php echo $this->_tpl_vars['job']->get('links.employer_profile'); ?>
" title = "<?php echo ((is_array($_tmp='COM_JOBS_DETAILS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"></a>
                        </div>

                        <div style="clear: both;"></div>
                        <div class="span12">
                            <label class="job_lables pull-left"><?php echo ((is_array($_tmp='COM_JOBS_JOINED_SINCE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</label> &nbsp;<?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['employer']->registerDate,'use_hour' => 0), $this);?>

                        </div>

                        <div class="span12">
                            <?php if ($this->_tpl_vars['employer']->city || $this->_tpl_vars['employer']->country): ?>
                                <label class="job_lables pull-left"><?php echo ((is_array($_tmp='COM_JOBS_USER_LOCATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</label>&nbsp;

                                    <?php echo $this->_tpl_vars['employer']->city; ?>
<?php if ($this->_tpl_vars['employer']->city): ?>,<?php endif; ?>
                                    <?php echo $this->_tpl_vars['employer']->country; ?>

                                    <?php if (! $this->_tpl_vars['employer']->country): ?>-<?php endif; ?>

                            <?php endif; ?>
                        </div>
                        <div class="span12">

                            <a href = "<?php echo $this->_tpl_vars['job']->get('links.otherjobs'); ?>
"><?php echo ((is_array($_tmp='COM_JOBS_OTHER_JOBS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
                                    <?php echo ((is_array($_tmp='COM_JOBS_FROM_THIS_USER')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                        </div>
                        <span style="line-height:1px; padding: 0;">&nbsp;</span>
                    </div>

                    <span class="v_spacer_5"></span>
                    <div style="clear: both;"></div>

                    <div class="box_title"><?php echo ((is_array($_tmp='COM_JOBS_DETAILS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</div>
                    <div width="100%" class="box_right_details">
                        <div>
                            <div class="span12">
                                <label class="job_lables pull-left"><?php echo ((is_array($_tmp='COM_JOBS_JOB_TYPE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</label> &nbsp;<?php echo $this->_tpl_vars['lists']['jobType']; ?>

                            </div>
                            <div class="span12">
                                <label class="job_lables pull-left"><?php echo ((is_array($_tmp='COM_JOBS_AVAILABLE_POSTS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</label> &nbsp;<?php echo $this->_tpl_vars['job']->jobs_available; ?>

                            </div>
                            <div class="span12">
                                <label class="job_lables pull-left"><?php echo ((is_array($_tmp='COM_JOBS_JOB_LOCATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</label>&nbsp;
                                    <?php if ($this->_tpl_vars['cfg']->enable_location_management): ?>
                                        <?php if ($this->_tpl_vars['lists']['job_location_state'] || $this->_tpl_vars['lists']['job_location_city']): ?>
                                            <?php echo $this->_tpl_vars['lists']['job_location_city']; ?>
<?php if ($this->_tpl_vars['lists']['job_location_city']): ?>,<?php endif; ?>
                                            <?php echo $this->_tpl_vars['lists']['job_location_state']; ?>
<?php if ($this->_tpl_vars['lists']['job_location_state']): ?>,<?php endif; ?>
                                            <?php echo $this->_tpl_vars['job']->get('job_country'); ?>

                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php echo $this->_tpl_vars['lists']['job_location_city']; ?>
, <?php echo $this->_tpl_vars['lists']['job_location_country']; ?>

                                    <?php endif; ?>
                            </div>
                            <div class="span12">
                                <label class="job_lables pull-left"><?php echo ((is_array($_tmp='COM_JOBS_EXPERIENCE_REQUEST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</label> &nbsp;<?php echo $this->_tpl_vars['lists']['jobExperienceRequested']; ?>

                            </div>
                            <div class="span12">
                                <label class="job_lables pull-left"><?php echo ((is_array($_tmp='COM_JOBS_STUDIES_REQUEST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</label> &nbsp;<?php echo $this->_tpl_vars['lists']['jobStudiesRequested']; ?>

                            </div>
                            <?php if ($this->_tpl_vars['job']->languages_request): ?>
                                <div class="span12">
                                    <label class="job_lables pull-left"><?php echo ((is_array($_tmp='COM_JOBS_LANGUAGES_REQUEST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</label> &nbsp;<?php echo $this->_tpl_vars['job']->languages_request; ?>

                                </div>
                            <?php endif; ?>
                            <div class="span12">
                                <label class="job_lables pull-left"><?php echo ((is_array($_tmp='COM_JOBS_START_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</label> &nbsp;<?php echo $this->_tpl_vars['job']->get('startdate_text'); ?>

                            </div>

                            <div class="span12">
                                <?php if ($this->_tpl_vars['job']->close_offer): ?>
                                    <h4><span class="closed" style="float: left;"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_JOBS_CLOSED_ON'), $this);?>
: <?php if ($this->_tpl_vars['job']->get('closed_date_text')): ?> <?php echo $this->_tpl_vars['job']->get('closed_date_text'); ?>
 <?php endif; ?></span></h4>
                                <?php elseif ($this->_tpl_vars['job']->get('expired')): ?>
                                    <span style="float: right;">
                                         <span class='expired'><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_JOBS_JOB_EXPIRED'), $this);?>
: </span>(<?php echo $this->_tpl_vars['job']->get('enddate_text'); ?>
)
                                    </span>
                                <?php else: ?>
                                    <?php if ($this->_tpl_vars['cfg']->enable_countdown): ?>
                                         <label class="job_lables pull-left"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_JOBS_DETAILS_ENDING_IN'), $this);?>
: </label><span class="timer"> <?php echo $this->_tpl_vars['job']->get('countdown'); ?>
</span>
                                    <?php else: ?>
                                         <label class="job_lables pull-left"><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_JOBS_END_DATE'), $this);?>
: </label> <span class=''> <?php echo $this->_tpl_vars['job']->get('enddate_text'); ?>
</span>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="span12">
                                <label class="job_lables"><?php echo $this->_tpl_vars['job']->hits; ?>
&nbsp;<?php echo ((is_array($_tmp='COM_JOBS_HITS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                            </div>
                            <span style="line-height:1px; padding: 0;">&nbsp;</span>
                        </div>
                    </div>
                    <div class="detail_right_position">
                        <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "detail-right",'item' => $this->_tpl_vars['job'],'page' => 'jobs'), $this);?>

                    </div>
                </div>
            </div>

        </div>

        <div class="job_details span12 grey_line1">
            <div class="detail_job_info span12">
                <div class="job_detail_info">
                    <div>
                        <div class="detail_job_description">
                            <br />
                            <div class="box_title"><?php echo ((is_array($_tmp='COM_JOBS_BENEFITS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</div>
                            <div class="box_content">
                                <div><span class="job_details_info"><?php echo $this->_tpl_vars['job']->benefits; ?>
</span></div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="detail_job_description">
                            <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => 'middle','item' => $this->_tpl_vars['job'],'page' => 'jobs'), $this);?>

                        </div>
                    </div>

                    <div class="grey_line">
                        <div class="detail_job_description">
                            <br /><div class="box_title"><?php echo ((is_array($_tmp='COM_JOBS_ABOUT_US')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</div>
                            <div class="box_content">
                                <?php if ($this->_tpl_vars['job']->about_visibility == 1): ?>
                                    <div><span class="job_details_info"><?php echo $this->_tpl_vars['employer']->about_us; ?>
</span></div>
                                <?php else: ?>
                                    <div><strong><?php echo ((is_array($_tmp='COM_JOBS_CONFIDENTIAL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</strong></div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="job_details job_details_tag">
                            <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => 'footer','item' => $this->_tpl_vars['job'],'page' => 'jobs'), $this);?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="span12">
            <div class="detail_job_info">
                <div class="job_details_tag">
                    <span class = "v_spacer_5"></span>
                    <?php echo $this->_tpl_vars['job']->get('links.tags'); ?>

                </div>
            </div>
        </div>

    </div>

    <div style="clear:both;"></div>
  <?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>

  <?php if ($this->_tpl_vars['cfg']->google_key && $this->_tpl_vars['cfg']->map_in_job_details && ( ( $this->_tpl_vars['job']->googlex && $this->_tpl_vars['job']->googley ) || ( $this->_tpl_vars['user']->googleMaps_x && $this->_tpl_vars['user']->googleMaps_y ) )): ?>
      <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/jobdetail/t_tab_maps.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
  <?php endif; ?>
	<?php if ($this->_tpl_vars['job']->isMyJob() && $this->_tpl_vars['job']->messages): ?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/jobdetail/t_tab_messages.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endif; ?>
<?php echo $this->_plugins['function']['endpane'][0][0]->smarty_endpane(array('name' => "jobdetail-pane",'id' => "jobdetail-pane"), $this);?>
