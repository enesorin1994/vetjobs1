<?php /* Smarty version 2.6.28, created on 2017-03-29 12:34:35
         compiled from elements/jobdetail/t_tab_maps.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'import_js_block', 'elements/jobdetail/t_tab_maps.tpl', 8, false),array('function', 'starttab', 'elements/jobdetail/t_tab_maps.tpl', 24, false),array('function', 'endtab', 'elements/jobdetail/t_tab_maps.tpl', 27, false),array('modifier', 'translate', 'elements/jobdetail/t_tab_maps.tpl', 24, false),)), $this); ?>
<?php if ($this->_tpl_vars['cfg']->google_key && $this->_tpl_vars['cfg']->map_in_job_details && ( ( $this->_tpl_vars['job']->googlex && $this->_tpl_vars['job']->googley ) || ( $this->_tpl_vars['user']->googleMaps_x && $this->_tpl_vars['user']->googleMaps_y ) )): ?>
    <?php if (( $this->_tpl_vars['job']->googlex && $this->_tpl_vars['job']->googley )): ?>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_maps.tpl', 'smarty_include_vars' => array('googleMapX' => ($this->_tpl_vars['job']->googlex),'googleMapY' => ($this->_tpl_vars['job']->googley))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php elseif (( $this->_tpl_vars['user']->googleMaps_x && $this->_tpl_vars['user']->googleMaps_y )): ?>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_maps.tpl', 'smarty_include_vars' => array('googleMapX' => ($this->_tpl_vars['user']->googleMaps_x),'googleMapY' => ($this->_tpl_vars['user']->googleMaps_x))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php endif; ?>

    <?php $this->_tag_stack[] = array('import_js_block', array()); $_block_repeat=true;$this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
     <?php echo '
        window.addEvent(\'domready\', function () {
            var loaded = false;

            $$(\'ul#jobdetail-pane.nav.nav-tabs li a\').addEvent(\'click\',function(){
                if (loaded == false) {
                    load_gmaps();
                }
                gmap_showmap.delay(500);
                loaded = true;
            });
        });
     '; ?>

    <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo $this->_plugins['block']['import_js_block'][0][0]->smarty_import_js_block($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>

   <?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => "jobdetail-pane",'id' => 'tab2','text' => ((is_array($_tmp='COM_JOBS_LOCATION_ON_MAP')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp))), $this);?>

            <div id = "map_canvas" class="maps_detail_jobs"></div>
   <?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>

<?php endif; ?>