<?php /* Smarty version 2.6.28, created on 2017-03-29 12:34:35
         compiled from elements/jobdetail/t_apply.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 'elements/jobdetail/t_apply.tpl', 2, false),array('function', 'import_js_file', 'elements/jobdetail/t_apply.tpl', 3, false),array('modifier', 'translate', 'elements/jobdetail/t_apply.tpl', 6, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "jobs.js"), $this);?>

<?php if ($this->_tpl_vars['hasApplied'] > 0): ?>
   <span class="job_app"> <?php echo ((is_array($_tmp='COM_JOBS_YOU_ALREADY_APPLIED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 </span>
<?php else: ?>
    <?php if ($this->_tpl_vars['job']->published): ?>
        <?php if ($this->_tpl_vars['lists']['isCandidate']): ?>
            <form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&task=sendjobapplication" method="post" name="jobForm" onsubmit="return jobObject.ApplicationFormValidate(this);">
                <input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
"/>
                <input type="hidden" name="task" value="sendjobapplication"/>
                <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['job']->id; ?>
"/>
                <input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
"/>
                <table class="table-striped table grey_line_top">
                    <tr>
                        <td class="job_app">
                            <div class="center">
                                                                <span class="span12">
                                    <input type="submit" name="send" value="<?php echo ((is_array($_tmp='COM_JOBS_SEND_APPLICATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="btn btn-primary btn-block" />
                                </span>

                                <span class="span6">
                                    <?php if ($this->_tpl_vars['terms_and_conditions']): ?>
                                        &nbsp; <input type="checkbox" class="inputbox" name="agreement" value="1" <?php echo $this->_tpl_vars['disable_apps']; ?>
 />
                                        <?php echo ((is_array($_tmp='COM_JOBS_AGREE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

                                        <a href="javascript: void(0);" onclick="window.open('<?php echo $this->_tpl_vars['links']->getTermsAndConditionsRoute(); ?>
','messwindow','location=1,status=1,scrollbars=1,width=500,height=500')" id="job_terms">
                                        <?php echo ((is_array($_tmp='COM_JOBS_TERMS_AND_CONDITIONS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
                                    <?php endif; ?>
                                </span>
                            </div>
                        </td>
                    </tr>
                 </table>
            </form>
        <?php else: ?>
            <?php echo ((is_array($_tmp='COM_JOBS_YOU_ARE_NOT_A_CANDIDATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>