<?php /* Smarty version 2.6.28, created on 2017-03-29 12:35:59
         compiled from elements/t_listfooter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 'elements/t_listfooter.tpl', 8, false),)), $this); ?>
<div class="pagination">
    <?php echo $this->_tpl_vars['pagination']->getPagesLinks(); ?>


    <div class="v_spacer_5">&nbsp;</div>
    <p class="counter">
        <?php echo $this->_tpl_vars['pagination']->getPagesCounter(); ?>
&nbsp;
        <span class="form-limit">
            <?php echo ((is_array($_tmp='COM_JOBS_DISPLAY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
 <?php echo $this->_tpl_vars['pagination']->getLimitBox(); ?>

        </span>     
    </p>
</div>