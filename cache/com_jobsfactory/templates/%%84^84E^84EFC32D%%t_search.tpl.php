<?php /* Smarty version 2.6.28, created on 2017-03-30 09:53:59
         compiled from t_search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'import_js_file', 't_search.tpl', 3, false),array('function', 'set_css', 't_search.tpl', 4, false),array('function', 'starttab', 't_search.tpl', 11, false),array('function', 'jtext', 't_search.tpl', 148, false),array('function', 'endtab', 't_search.tpl', 165, false),array('modifier', 'translate', 't_search.tpl', 6, false),array('modifier', 'count', 't_search.tpl', 145, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'js/t_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['import_js_file'][0][0]->smarty_import_js_file(array('url' => "job_edit.js"), $this);?>

<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>


<h1><?php echo ((is_array($_tmp='COM_JOBS_SEARCH_JOBS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</h1>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 't_search_tabs.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo $this->_plugins['function']['starttab'][0][0]->smarty_starttab(array('name' => 'job_tabmenu','id' => 'tab1'), $this);?>


<form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php" method="get" name="rJobForm" id="rJobForm" class="form-horizontal">
<input type="hidden" name="task" value="showSearchResults"/>
<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
"/>
<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
"/>

<div class="jobs_search" style="max-width: 700px;">

    <div class = "row-fluid">
        <div class = "span9">
            <input type="text" name="keyword" class="inputbox input-block-level " size="50" value="<?php echo $this->_tpl_vars['lists']['keyword']; ?>
" placeholder = "<?php echo ((is_array($_tmp='COM_JOBS_TITLE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" style="margin-bottom: 2px !important;" />
        </div>
        <div class="span3">
            <input type="submit" name="search" value="<?php echo ((is_array($_tmp='COM_JOBS_SEARCH')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="btn btn-primary btn-block btn-medium"/>
        </div>
    </div>
    <div><span>&nbsp;</span></div>
    <div class = "jobs_search_inner">
        <div class = "row-fluid">

            <div class = "span5 offset2">
                <div class="job_search_sub">
                    <input type="checkbox" class="inputbox pull-left" name="in_description" value="1" <?php if ($this->_tpl_vars['lists']['in_description']): ?>checked=checked<?php endif; ?> /> &nbsp;<label
                        class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_SEARCH_IN_DESCRIPTION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>&nbsp;&nbsp;
                </div>
            </div>

            <div class = "span5">
                <div class="job_search_sub">
                    <input type="checkbox" class="inputbox pull-left" name="in_archive" value="1" <?php if ($this->_tpl_vars['lists']['in_archive']): ?>checked=checked<?php endif; ?> /> &nbsp;<label
                        class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_SEARCH_IN_ARCHIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label>
                </div>
            </div>

        </div>
        <div>
            <div><span>&nbsp;</span></div>
        </div>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="control-label job_lables"><label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_USERNAME')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label></div>
            </div>
            <div class="span10">
                <?php echo $this->_tpl_vars['lists']['users']; ?>

            </div>
        </div>
        <?php if ($this->_tpl_vars['lists']['cats']): ?>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="control-label job_lables"><label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label></div>
            </div>
            <div class="span10">
                <?php echo $this->_tpl_vars['lists']['cats']; ?>

            </div>
        </div>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['lists']['country']): ?>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="control-label job_lables"><label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_COUNTRY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label></div>
            </div>
            <div class="span10">
                <?php echo $this->_tpl_vars['lists']['country']; ?>

            </div>
        </div>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['job_location_state']): ?>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="control-label job_lables"><label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_LOCATION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label></div>
            </div>
            <div class="span10">
                <?php echo $this->_tpl_vars['job_location_state']; ?>

            </div>
        </div>
        <?php endif; ?>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="control-label job_lables"><label class="job_lables">
                    <span class="pull-left"><?php echo ((is_array($_tmp='COM_JOBS_CITY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span></label>
                </div>
            </div>
            <div class="span10">
                <?php if ($this->_tpl_vars['cfg']->enable_location_management && ! empty ( $this->_tpl_vars['lists']['city'] )): ?>
                    <div id="cities" name="cities">
                        <?php echo $this->_tpl_vars['lists']['city']; ?>

                    </div>
                <?php else: ?>
                    <input class="inputbox" type="text" size="30" name="city" value="<?php echo $this->_tpl_vars['lists']['city']; ?>
" alt="city">
                <?php endif; ?>
            </div>
        </div>
        <div class = "row-fluid" style="margin: 2px 0 2px 0;">
            <div class = "span2">
                <div class="job_lables"><label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_AFTER_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label></div>
            </div>
            <div class="span10">
                <?php echo $this->_tpl_vars['lists']['afterd']; ?>

            </div>
        </div>
        <div class = "row-fluid" style="margin: 1px 0 2px 0;">
            <div class = "span2">
                <div class="job_lables"><label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_BEFORE_DATE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label></div>
            </div>
            <div class="span10">
                <?php echo $this->_tpl_vars['lists']['befored']; ?>

            </div>
        </div>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="job_lables"><label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_JOB_TYPE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label></div>
            </div>
            <div class="span10">
                <?php echo $this->_tpl_vars['lists']['job_type']; ?>

            </div>
        </div>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="job_lables"><label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_TAB_EXPERIENCE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label></div>
            </div>
            <div class="span10">
                <?php echo $this->_tpl_vars['lists']['experience_request']; ?>

            </div>
        </div>
        <div class = "row-fluid">
            <div class = "span2">
                <div class="job_lables"><label class="job_lables"><?php echo ((is_array($_tmp='COM_JOBS_STUDIES_REQUEST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</label></div>
            </div>
            <div class="span10">
               <?php echo $this->_tpl_vars['lists']['studies_request']; ?>

            </div>
        </div>

        <?php if (count($this->_tpl_vars['custom_fields_html']) > 0): ?>
        <div class = "row-fluid">
            <fieldset class="jobs_search">
                <legend><?php echo $this->_plugins['function']['jtext'][0][0]->smarty_function_jtext(array('text' => 'COM_JOBS_CUSTOM_FIELDS'), $this);?>
</legend>
                <div class="span12 custom_fields_box">
                  <?php echo $this->_tpl_vars['custom_fields_html']; ?>

                </div>
            </fieldset>
        </div>
        <?php endif; ?>

        <div class = "row-fluid">
            <div class="span3">
                <input type="submit" name="search" value="<?php echo ((is_array($_tmp='COM_JOBS_SEARCH')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="btn btn-primary btn-block btn-medium"/>
            </div>
        </div>
    </div>
</div>
</form>

<?php echo $this->_plugins['function']['endtab'][0][0]->smarty_endtab(array(), $this);?>