<?php /* Smarty version 2.6.28, created on 2017-03-29 12:35:59
         compiled from elements/lists/t_listjobs_cell.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', 'elements/lists/t_listjobs_cell.tpl', 7, false),array('modifier', 'translate', 'elements/lists/t_listjobs_cell.tpl', 16, false),array('function', 'positions', 'elements/lists/t_listjobs_cell.tpl', 23, false),array('function', 'printdate', 'elements/lists/t_listjobs_cell.tpl', 103, false),)), $this); ?>
<?php if ((1 & $this->_tpl_vars['index'])): ?>
	<?php $this->assign('class', '1'); ?>
<?php else: ?>
	<?php $this->assign('class', '2'); ?>
<?php endif; ?>
<?php if ($this->_tpl_vars['job']->featured && $this->_tpl_vars['job']->featured != 'none'): ?>
	<?php $this->assign('class_featured', ((is_array($_tmp="listing-")) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['job']->featured) : smarty_modifier_cat($_tmp, $this->_tpl_vars['job']->featured))); ?>
<?php else: ?>
	<?php $this->assign('class_featured', ""); ?>
<?php endif; ?>

<tr class="<?php if ($this->_tpl_vars['class_featured']): ?><?php echo $this->_tpl_vars['class_featured']; ?>
<?php else: ?>job_row_<?php echo $this->_tpl_vars['class']; ?>
<?php endif; ?>">
    <td class="job_dt" valign="top" colspan="2">
        <div class="job_title">
            <?php if ($this->_tpl_vars['class_featured']): ?>
                <img src="<?php echo $this->_tpl_vars['TEMPLATE_IMAGES']; ?>
f_featured.png" title="<?php echo ((is_array($_tmp='COM_JOBS_FEATURED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt="<?php echo ((is_array($_tmp='COM_JOBS_FEATURED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="featured-image" />
            <?php endif; ?>
            <?php if ($this->_tpl_vars['job']->employer_visibility == $this->_tpl_vars['EMPLOYER_VISIBILITIES']['EMPLOYER_TYPE_CONFIDENTIAL']): ?>
                <img src="<?php echo $this->_tpl_vars['TEMPLATE_IMAGES']; ?>
private.png" title="<?php echo ((is_array($_tmp='COM_JOBS_CONFIDENTIAL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt="<?php echo ((is_array($_tmp='COM_JOBS_CONFIDENTIAL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="confidential-image" style="width: 16px;" />
            <?php endif; ?>
            <a href="<?php echo $this->_tpl_vars['job']->get('links.jobs'); ?>
"><?php echo $this->_tpl_vars['job']->title; ?>
</a>

            <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "cell-header",'item' => $this->_tpl_vars['job'],'page' => 'jobs'), $this);?>

        </div>
    </td>
    <td class="job_dbk" valign="top" align="right">
        <span class="job_uniqueid"><?php echo ((is_array($_tmp='COM_JOBS_ID')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
: <?php echo $this->_tpl_vars['job']->jobuniqueID; ?>
</span>
    </td>
</tr>

<tr class="<?php if ($this->_tpl_vars['class_featured']): ?><?php echo $this->_tpl_vars['class_featured']; ?>
<?php else: ?>job_row_<?php echo $this->_tpl_vars['class']; ?>
<?php endif; ?>">
	<td class="job_dbk" valign="top">

        <span class="job_list_category">
            <img src = "<?php echo $this->_tpl_vars['TEMPLATE_IMAGES']; ?>
folder.png"
             title = "<?php echo ((is_array($_tmp='COM_JOBS_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
             alt = "<?php echo ((is_array($_tmp='COM_JOBS_CATEGORY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
             style = "vertical-align: bottom;margin-right: -11px;"
                />
            <?php if ($this->_tpl_vars['job']->get('catname')): ?>
                <a href="<?php echo $this->_tpl_vars['job']->get('links.filter_cat'); ?>
"><?php echo $this->_tpl_vars['job']->get('catname'); ?>
</a>
            <?php else: ?>
                &nbsp;-&nbsp;
            <?php endif; ?>
        </span>
        <?php if ($this->_tpl_vars['job']->has_file): ?>
            <img src = "<?php echo $this->_tpl_vars['TEMPLATE_IMAGES']; ?>
attach.gif"
                 title = "<?php echo ((is_array($_tmp='COM_JOBS_HAS_ATTACHMENTS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                 alt = "<?php echo ((is_array($_tmp='COM_JOBS_HAS_ATTACHMENTS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                 style = "height:16px;vertical-align: middle;margin-left: 3px;"
                    />
            <span style = "color:grey;font-size: 10px;"><?php echo ((is_array($_tmp='COM_JOBS_FILES')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['job']->employer_visibility == $this->_tpl_vars['EMPLOYER_VISIBILITIES']['EMPLOYER_TYPE_PUBLIC']): ?>
            <span class="job_list_category">
                <img src = "<?php echo $this->_tpl_vars['TEMPLATE_IMAGES']; ?>
briefcase.png"
                         title = "<?php echo ((is_array($_tmp='COM_JOBS_COMPANY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                         alt = "<?php echo ((is_array($_tmp='COM_JOBS_COMPANY')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                         style = "vertical-align: bottom;margin-right: -11px;"
                            />
                <a href = "<?php echo $this->_tpl_vars['job']->get('links.otherjobs'); ?>
"
                      title = "<?php echo ((is_array($_tmp='COM_JOBS_MORE_JOBS_FROM_THIS_USER')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                      alt="<?php echo ((is_array($_tmp='COM_JOBS_MORE_JOBS_FROM_THIS_USER')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" >
           	    <?php echo $this->_tpl_vars['job']->get('companyname'); ?>
</a>&nbsp;
            </span>
        <?php endif; ?>
        <?php if (! $this->_tpl_vars['job']->isMyJob() & $this->_tpl_vars['is_logged_in']): ?>
            <?php if ($this->_tpl_vars['job']->get('favorite')): ?>
                <span class='add_to_watchlist'>
                        <a href='<?php echo $this->_tpl_vars['job']->get('links.del_from_watchlist'); ?>
'>
                            <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_0.png" style="height:16px;vertical-align: bottom;" title="<?php echo ((is_array($_tmp='COM_JOBS_REMOVE_FROM_FAVORITESLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt="<?php echo ((is_array($_tmp='COM_JOBS_REMOVE_FROM_FAVORITESLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"/></a>
                    </span>
            <?php else: ?>
                <span class="add_to_watchlist">
                        <a href='<?php echo $this->_tpl_vars['job']->get('links.add_to_watchlist'); ?>
'>
                            <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_1.png" style="height:16px;vertical-align: bottom;" title="<?php echo ((is_array($_tmp='COM_JOBS_ADD_TO_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt="<?php echo ((is_array($_tmp='COM_JOBS_ADD_TO_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"/></a>
                    </span>
            <?php endif; ?>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['job']->job_shortdescription): ?>
            <span class="job_category1">
                &nbsp;<img src="<?php echo $this->_tpl_vars['TEMPLATE_IMAGES']; ?>
f_expand_01.png" title="<?php echo ((is_array($_tmp='COM_JOBS_SHOW_DESCRIPTION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" class="job_link"
                        alt="<?php echo ((is_array($_tmp='COM_JOBS_SHOW_DESCRIPTION')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" onclick="jobObject.toggleDescription('job_short_description_<?php echo $this->_tpl_vars['index']; ?>
',this);"/>
            </span>
            <div id="job_short_description_<?php echo $this->_tpl_vars['index']; ?>
" class="job_short_description">
                <?php echo $this->_tpl_vars['job']->job_shortdescription; ?>

            </div>
        <?php endif; ?>
        <div class="cell-left_css">
        <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "cell-left",'item' => $this->_tpl_vars['job'],'page' => 'jobs'), $this);?>

        </div>
	</td>
	<td class="job_dbk" valign="top">
		<!-- [+] Time Ending -->
		<?php if ($this->_tpl_vars['job']->close_offer): ?>
			<span class='canceled_on'>
			<?php if ($this->_tpl_vars['job']->end_date > $this->_tpl_vars['job']->closed_date): ?>
				<?php echo ((is_array($_tmp='COM_JOBS_CANCELED_ON')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

			<?php else: ?>
				<?php echo ((is_array($_tmp='COM_JOBS_CLOSED_ON')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>

			<?php endif; ?>

			    <?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['job']->closed_date), $this);?>

            </span>
		<?php elseif ($this->_tpl_vars['job']->get('expired')): ?>
		   <span class='expired'><?php echo ((is_array($_tmp='COM_JOBS_EXPIRED')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</span>
		<?php else: ?>
            <?php if ($this->_tpl_vars['cfg']->enable_countdown): ?>
                 <span class="timer"> <?php echo $this->_tpl_vars['job']->get('countdown'); ?>
</span>
            <?php else: ?>
                 <span class=''> <?php echo $this->_tpl_vars['job']->get('enddate_text'); ?>
</span>
            <?php endif; ?>
		<?php endif; ?>
		<!-- [-] Time Ending -->
        <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "cell-middle",'item' => $this->_tpl_vars['job'],'page' => 'jobs'), $this);?>

	</td>
	<td class="job_dbk" valign="top" align="right">
		<div>
            <?php if ($this->_tpl_vars['job']->show_candidate_nr == 1): ?>
                <span class="hasTip" title="<?php echo ((is_array($_tmp='COM_JOBS_NR_APPLICATIONS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
">

    			<?php if ($this->_tpl_vars['job']->get('nr_applicants') !== null): ?>
                    <?php if ($this->_tpl_vars['job']->isMyJob()): ?>
                        <a href="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&task=companylistcandidates&id=<?php echo $this->_tpl_vars['job']->id; ?>
&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
">
                            <span class="number"><?php echo $this->_tpl_vars['job']->get('nr_applicants'); ?>
</span>
                        </a>
                    <?php else: ?>
                        <span class="number"><?php echo $this->_tpl_vars['job']->get('nr_applicants'); ?>
</span>
                    <?php endif; ?>
                    (<?php echo $this->_tpl_vars['job']->get('nr_newapplicants'); ?>
 <?php echo ((is_array($_tmp='COM_JOBS_NEW')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
)
    			<?php else: ?>
    				&nbsp;-&nbsp;
    			<?php endif; ?>
                </span>
            <?php else: ?>
                - 
            <?php endif; ?>

		</div>
        <?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "cell-right",'item' => $this->_tpl_vars['job'],'page' => 'jobs'), $this);?>

	</td>
</tr>
<tr class="<?php if ($this->_tpl_vars['class_featured']): ?><?php echo $this->_tpl_vars['class_featured']; ?>
<?php else: ?>job_row_<?php echo $this->_tpl_vars['class']; ?>
<?php endif; ?>">
	<td colspan="6" class="job_foot right">
		<?php echo $this->_plugins['function']['positions'][0][0]->smarty_positions(array('position' => "cell-footer",'item' => $this->_tpl_vars['job'],'page' => 'jobs'), $this);?>

	</td>
</tr>
<?php if ($this->_tpl_vars['class_featured']): ?>
<tr>
   <td class="v_spacer_7" colspan="4">&nbsp;</td>
</tr>
<?php endif; ?>

