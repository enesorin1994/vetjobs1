<?php /* Smarty version 2.6.28, created on 2017-03-29 12:34:35
         compiled from elements/jobdetail/t_action_buttons.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 'elements/jobdetail/t_action_buttons.tpl', 9, false),)), $this); ?>
<table width="100%" cellpadding="0" cellspacing="0">
	 <tr>
	  <td align="right">
	    <div style="padding:5px;">
          <?php if (! $this->_tpl_vars['job']->isMyJob() && $this->_tpl_vars['job']->close_offer == 0): ?>
            <?php if ($this->_tpl_vars['is_logged_in']): ?>
                <?php if ($this->_tpl_vars['job']->get('favorite')): ?>
                        <span class = 'add_to_watchlist'><a href = '<?php echo $this->_tpl_vars['job']->get('links.del_from_watchlist'); ?>
'>
                            <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_0.png" title = "<?php echo ((is_array($_tmp='COM_JOBS_REMOVE_FROM_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt = "<?php echo ((is_array($_tmp='COM_JOBS_REMOVE_FROM_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" /></a>
                        </span>
                <?php else: ?>
                        <span class = 'add_to_watchlist'>
                            <a href = '<?php echo $this->_tpl_vars['job']->get('links.add_to_watchlist'); ?>
'>
                                <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_watchlist_1.png" title = "<?php echo ((is_array($_tmp='COM_JOBS_ADD_TO_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                                     alt = "<?php echo ((is_array($_tmp='COM_JOBS_ADD_TO_WATCHLIST')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" />
                            </a>
                        </span>
                <?php endif; ?>
            <?php endif; ?>
          <?php endif; ?>
          <?php if ($this->_tpl_vars['job']->isMyJob()): ?>
    	    <?php if ($this->_tpl_vars['job']->close_offer == 1): ?>
			    <input type="button" onclick="window.location = '<?php echo $this->_tpl_vars['job']->get('links.republish'); ?>
';" class="btn btn-primary" value="<?php echo ((is_array($_tmp='COM_JOBS_REPUBLISH')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" alt="<?php echo ((is_array($_tmp='COM_JOBS_REPUBLISH')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" />
		    <?php else: ?>
                <input type="button" value="<?php echo ((is_array($_tmp='COM_JOBS_EDIT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" onclick="window.location = '<?php echo $this->_tpl_vars['job']->get('links.edit'); ?>
';" class="btn btn-primary" alt="<?php echo ((is_array($_tmp='COM_JOBS_EDIT')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" />
                <input type="button" value="<?php echo ((is_array($_tmp='COM_JOBS_CLOSE_AND_ARCHIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" onclick="document.getElementById('close_job_div').style.display='';" class="btn" alt="<?php echo ((is_array($_tmp='COM_JOBS_CLOSE_AND_ARCHIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
" />
            <?php endif; ?>
            <!-- [+] Cancel Form Hidden -->
            <div id="close_job_div" style="display:none;">
                <form name="cancel_job_form" id="cancel_job_form" method="POST" action="<?php echo $this->_tpl_vars['job']->get('links.cancel'); ?>
">
                    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['job']->id; ?>
"/>
                    <input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
"/>
                    <input type="hidden" name="task" value="canceljob"/>
                    <input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
"/>
                    <table>
                        <tr>
                            <td align="right" valign="top"><?php echo ((is_array($_tmp='COM_JOBS_CANCEL_REASON')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
:</td>
                            <td>
                                <textarea id="cancel_reason" name="cancel_reason" rows="5"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><input type="button" value="<?php echo ((is_array($_tmp='COM_JOBS_CANCEL_JOB')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"  onclick="if(confirm('<?php echo ((is_array($_tmp='COM_JOBS_ARE_YOU_SURE_YOU_WANT_TO_CANCEL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
')) document.cancel_job_form.submit();" class="btn btn-primary" />
                            <input type="button" value="<?php echo ((is_array($_tmp='COM_JOBS_CLOSE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"  onclick="document.getElementById('close_job_div').style.display='none';" class="btn" /></td>
                        </tr>
                    </table>
                </form>
            </div>
            <!-- [-] Cancel Form Hidden -->
        		<?php else: ?>
            <?php if ($this->_tpl_vars['job']->close_offer == 0): ?>
                <a href = '<?php echo $this->_tpl_vars['job']->get('links.report'); ?>
'>
                            <img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
report_job.png"
                                 title = "<?php echo ((is_array($_tmp='COM_JOBS_REPORT_JOB_AS_ABUSIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                                 alt = "<?php echo ((is_array($_tmp='COM_JOBS_REPORT_JOB_AS_ABUSIVE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
"
                                    />
                        </a>
            <?php endif; ?>
		<?php endif; ?>
		</div>
	  </td>
	 </tr>
   <?php if ($this->_tpl_vars['job']->isMyJob()): ?>
     <?php if ($this->_tpl_vars['cfg']->admin_approval && ! $this->_tpl_vars['job']->approved): ?>
     <tr>
        <td>
            <div class="jobsfactory_pending_notify"><?php echo ((is_array($_tmp='COM_JOBS_JOB_IS_PENDING_ADMINISTRATOR_APPROVAL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</div>
        </td>
     </tr>
     <?php endif; ?>
   <?php endif; ?>
</table>

<!-- [-] Buttons Bar -->