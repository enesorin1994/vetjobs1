<?php /* Smarty version 2.6.28, created on 2017-03-29 12:46:20
         compiled from elements/category/t_categories_tabs.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 'elements/category/t_categories_tabs.tpl', 17, false),)), $this); ?>
<?php echo '
<script type="text/javascript">
    jQuery(document).ready( function ($) {
        $(document).on("click", "#job_tabmenu li a", function(event){
           location.href = this.rel;
        });
    });
</script>
'; ?>


<!-- Begin Content -->
<div>
    <ul class="nav nav-tabs" id="job_tabmenu">

        <li class = "<?php if (( 'categories' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
            <a data-toggle="tab" href="#tab1" rel = "<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&task=categories&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
">
        <?php echo ((is_array($_tmp='COM_JOBS_VIEW_CATEGORIES_CLASICAL')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </li>
        <li class = "<?php if (( 'tree' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
            <a data-toggle="tab" href="#tab2" rel = "<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&task=tree&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
">
        <?php echo ((is_array($_tmp='COM_JOBS_VIEW_CATEGORIES_TREE')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </li>
        <li class = "<?php if (( 'listlocations' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
            <a  data-toggle="tab" href="#tab3" rel = "<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&task=listlocations&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
">
        <?php echo ((is_array($_tmp='COM_JOBS_VIEW_LOCATIONS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </li>
        <li class = "<?php if (( 'googlemaps' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
            <a data-toggle="tab" href="#tab4" rel = "<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_jobsfactory&task=googlemaps&controller=maps&Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
">
        <?php echo ((is_array($_tmp='COM_JOBS_GOOGLE_MAPS')) ? $this->_run_mod_handler('translate', true, $_tmp) : $this->_plugins['modifier']['translate'][0][0]->smarty_translate($_tmp)); ?>
</a>
        </li>
    </ul>
</div>
<div id="config-document" class="tab-content">
    <div id="tab1" class="tab-pane <?php if (( 'categories' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    <div id="tab2" class="tab-pane <?php if (( 'tree' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    <div id="tab3" class="tab-pane <?php if (( 'listlocations' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
    <div id="tab4" class="tab-pane <?php if (( 'googlemaps' == $this->_tpl_vars['task'] )): ?>active<?php else: ?>inactive<?php endif; ?>">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
    </div>
</div>

<!-- End Content -->
<div style="clear: both;"></div>