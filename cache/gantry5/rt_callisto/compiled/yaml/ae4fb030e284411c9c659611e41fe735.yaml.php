<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/blueprints/styles/navigation.yaml',
    'modified' => 1463470148,
    'data' => [
        'name' => 'Navigation Colors',
        'description' => 'Navigation colors for the Callisto theme',
        'type' => 'section',
        'form' => [
            'fields' => [
                'background' => [
                    'type' => 'input.colorpicker',
                    'label' => 'Background',
                    'default' => '#353535'
                ],
                'text-color' => [
                    'type' => 'input.colorpicker',
                    'label' => 'Text',
                    'default' => '#828282'
                ]
            ]
        ]
    ]
];
