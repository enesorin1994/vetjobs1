<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/68/index.yaml',
    'modified' => 1490863031,
    'data' => [
        'name' => '68',
        'timestamp' => 1490863031,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/home.png',
            'name' => 'features_-_overview',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-6050' => 'Logo'
            ],
            'module' => [
                'position-module-6475' => 'Module Instance',
                'position-module-8986' => 'Module Instance',
                'position-module-4565' => 'Module Instance',
                'position-module-6126' => 'Module Instance'
            ],
            'menu' => [
                'menu-4774' => 'Menu'
            ],
            'social' => [
                'social-9696' => 'Social'
            ],
            'custom' => [
                'custom-3582' => 'Gantry 5 - Next Generation Theme Framework',
                'custom-3561' => 'Add To Watchlist'
            ],
            'content' => [
                'system-content-2146' => 'Page Content'
            ],
            'newsletter' => [
                'newsletter-1649' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-6987' => 'Copyright'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
