<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/70/index.yaml',
    'modified' => 1488888443,
    'data' => [
        'name' => 70,
        'timestamp' => 1488888443,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'features_-_variations',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-3466' => 'System Messages'
            ],
            'logo' => [
                'logo-8333' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-6237' => 'Icon Menu'
            ],
            'menu' => [
                'menu-2294' => 'Menu'
            ],
            'social' => [
                'social-4596' => 'Social'
            ],
            'custom' => [
                'custom-7447' => 'Block Variations',
                'custom-1069' => 'Box 1',
                'custom-6250' => 'Box 2',
                'custom-4307' => 'Box 3',
                'custom-5636' => 'Box 4',
                'custom-2992' => 'Box Grey',
                'custom-1856' => 'Box Pink',
                'custom-6603' => 'Box Red',
                'custom-3649' => 'Box Purple',
                'custom-4857' => 'Box Orange',
                'custom-1099' => 'Box Blue',
                'custom-1659' => 'No Variations',
                'custom-7072' => 'Additional Effects & Classes'
            ],
            'spacer' => [
                'spacer-6152' => 'Spacer'
            ],
            'infolist' => [
                'infolist-9607' => 'Effects',
                'infolist-2122' => 'Utilities',
                'infolist-6850' => 'Utilities Cont.'
            ],
            'copyright' => [
                'copyright-8647' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-2339' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
