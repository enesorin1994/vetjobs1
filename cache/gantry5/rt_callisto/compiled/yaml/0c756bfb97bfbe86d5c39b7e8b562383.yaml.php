<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/config/default/particles/social.yaml',
    'modified' => 1463470148,
    'data' => [
        'enabled' => '1',
        'css' => [
            'class' => 'social-items'
        ],
        'target' => '_blank',
        'items' => [
            0 => [
                'icon' => 'fa fa-twitter fa-fw',
                'text' => '',
                'link' => 'http://twitter.com/rockettheme',
                'name' => 'Twitter'
            ],
            1 => [
                'icon' => 'fa fa-facebook fa-fw',
                'text' => '',
                'link' => 'http://facebook.com/rockettheme',
                'name' => 'Facebook'
            ],
            2 => [
                'icon' => 'fa fa-google fa-fw',
                'text' => '',
                'link' => 'http://plus.google.com/+rockettheme',
                'name' => 'Google'
            ],
            3 => [
                'icon' => 'fa fa-rss fa-fw',
                'text' => '',
                'link' => 'http://www.rockettheme.com/product-updates?rss',
                'name' => 'RSS'
            ]
        ]
    ]
];
