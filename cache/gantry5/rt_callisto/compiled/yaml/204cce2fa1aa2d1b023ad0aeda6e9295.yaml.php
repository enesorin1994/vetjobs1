<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/81/index.yaml',
    'modified' => 1488888444,
    'data' => [
        'name' => 81,
        'timestamp' => 1488888444,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/offline.png',
            'name' => 'pages_-_offline',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-8296' => 'System Messages'
            ],
            'logo' => [
                'logo-9258' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-8921' => 'Icon Menu'
            ],
            'menu' => [
                'menu-8762' => 'Menu'
            ],
            'social' => [
                'social-3244' => 'Social'
            ],
            'custom' => [
                'custom-6609' => 'Offline Page',
                'custom-3347' => 'Offline Content',
                'custom-3454' => 'Maintenance Mode',
                'custom-1260' => 'About Callisto',
                'custom-3619' => 'Sample Sitemap'
            ],
            'newsletter' => [
                'newsletter-2697' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-6874' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-2870' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
