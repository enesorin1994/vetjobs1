<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/blueprints/styles/showcase.yaml',
    'modified' => 1463470148,
    'data' => [
        'name' => 'Showcase Colors',
        'description' => 'Showcase colors for the Callisto theme',
        'type' => 'section',
        'form' => [
            'fields' => [
                'background' => [
                    'type' => 'input.colorpicker',
                    'label' => 'Background',
                    'default' => '#23262f'
                ],
                'text-color' => [
                    'type' => 'input.colorpicker',
                    'label' => 'text',
                    'default' => '#ffffff'
                ]
            ]
        ]
    ]
];
