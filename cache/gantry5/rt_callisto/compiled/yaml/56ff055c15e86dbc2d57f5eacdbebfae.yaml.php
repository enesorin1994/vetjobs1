<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/67/layout.yaml',
    'modified' => 1490861899,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/home.png',
            'name' => 'home',
            'timestamp' => 1463470148
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'logo-7854 80',
                    1 => 'position-module-7600 20'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-5647 80',
                    1 => 'social-3414 20'
                ]
            ],
            '/showcase/' => [
                0 => [
                    0 => 'position-module-4689'
                ],
                1 => [
                    0 => 'system-content-8850'
                ],
                2 => [
                    0 => 'position-module-8679 50',
                    1 => 'position-module-4798 50'
                ],
                3 => [
                    0 => 'contentlist-3684'
                ]
            ],
            '/container-4626/' => [
                0 => [
                    0 => [
                        'mainfeature 67' => [
                            
                        ]
                    ],
                    1 => [
                        'sidefeature 33' => [
                            
                        ]
                    ]
                ]
            ],
            '/main/' => [
                
            ],
            '/extension/' => [
                
            ],
            '/footer/' => [
                0 => [
                    0 => 'contact-8860'
                ]
            ],
            '/copyright/' => [
                0 => [
                    0 => 'copyright-5334'
                ]
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'mainfeature' => [
                'type' => 'section'
            ],
            'sidefeature' => [
                'type' => 'section'
            ],
            'container-4626' => [
                'attributes' => [
                    'id' => 'g-feature',
                    'boxed' => ''
                ]
            ],
            'main' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'logo-7854' => [
                'attributes' => [
                    'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                ],
                'block' => [
                    'class' => 'g-logo-block'
                ]
            ],
            'position-module-7600' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '97',
                    'key' => 'module-instance'
                ]
            ],
            'menu-5647' => [
                'attributes' => [
                    'menu' => 'main-menu'
                ],
                'block' => [
                    'class' => 'g-menu-block'
                ]
            ],
            'position-module-4689' => [
                'title' => 'searchbox',
                'attributes' => [
                    'module_id' => '106',
                    'key' => 'searchbox'
                ]
            ],
            'system-content-8850' => [
                'block' => [
                    'variations' => 'box2'
                ]
            ],
            'position-module-8679' => [
                'title' => 'latest',
                'attributes' => [
                    'module_id' => '103',
                    'key' => 'latest'
                ],
                'block' => [
                    'variations' => 'rounded shadow center equal-height box-orange'
                ]
            ],
            'position-module-4798' => [
                'title' => 'featured',
                'attributes' => [
                    'module_id' => '101',
                    'key' => 'featured'
                ],
                'block' => [
                    'variations' => 'box-orange shadow rounded equal-height center'
                ]
            ],
            'contentlist-3684' => [
                'title' => 'FP Main Content List',
                'attributes' => [
                    'title' => '                      Cele mai multe joburi in domeniul Medicinii Veterinare!',
                    'image' => 'gantry-media://vete.jpg',
                    'imagetag' => 'VetJobs',
                    'headline' => 'VetJobs, platforma ta in domeniul medicinii veterinare.',
                    'subtitle' => 'Rapid si usor',
                    'desc' => 'VetJobs ofera utilizatorilor sai o interfata usor accesibila, fiind foarte usor, atat pentru viitorii angajati cat si pentru angajatori, sa aplice sau sa posteze noi oferte de locuri de munca.',
                    'readmore' => '',
                    'link' => ''
                ],
                'block' => [
                    'class' => 'fp-contentlist',
                    'variations' => 'box2 rounded'
                ]
            ],
            'contact-8860' => [
                'title' => 'Contact Details',
                'attributes' => [
                    'title' => 'Contact Details',
                    'contactitems' => [
                        0 => [
                            'icon' => 'fa fa-phone fa-fw',
                            'text' => '0766666666',
                            'label' => 'Call Us'
                        ],
                        1 => [
                            'icon' => 'fa fa-map-marker fa-fw',
                            'text' => 'Bucharest, Romania',
                            'label' => 'Office'
                        ],
                        2 => [
                            'icon' => 'fa fa-envelope fa-fw',
                            'text' => 'mail@vetjobs.ro',
                            'label' => 'Say Hi'
                        ],
                        3 => [
                            'icon' => 'fa fa-comment fa-fw',
                            'text' => '<a href="#">Live Chat with Us</a>',
                            'label' => 'Need Advice?'
                        ]
                    ]
                ]
            ],
            'copyright-5334' => [
                'attributes' => [
                    'date' => [
                        'start' => '2007'
                    ],
                    'owner' => 'RocketTheme, LLC'
                ],
                'block' => [
                    'class' => 'center'
                ]
            ]
        ]
    ]
];
