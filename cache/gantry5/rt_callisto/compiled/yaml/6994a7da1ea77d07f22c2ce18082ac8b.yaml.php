<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/_error/index.yaml',
    'modified' => 1488888442,
    'data' => [
        'name' => '_error',
        'timestamp' => 1488888442,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/error.png',
            'name' => '_error',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-3821' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-8173' => 'Icon Menu'
            ],
            'menu' => [
                'menu-7443' => 'Menu'
            ],
            'social' => [
                'social-5102' => 'Social'
            ],
            'custom' => [
                'custom-3466' => 'Whooops! Error!',
                'custom-4361' => 'Nothing to See Here',
                'custom-9506' => 'About Callisto',
                'custom-3876' => 'Sample Sitemap'
            ],
            'content' => [
                'system-content-7438' => 'Page Content'
            ],
            'newsletter' => [
                'newsletter-6473' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-8564' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-4164' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
