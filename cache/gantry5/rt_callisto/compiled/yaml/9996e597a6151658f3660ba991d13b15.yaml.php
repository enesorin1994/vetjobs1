<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/83/index.yaml',
    'modified' => 1490784802,
    'data' => [
        'name' => '83',
        'timestamp' => 1490784802,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-8844' => 'Logo / Image'
            ],
            'module' => [
                'position-module-6670' => 'Module Instance',
                'position-module-8446' => 'search',
                'position-module-2531' => 'Module Instance',
                'position-module-9396' => 'Module Instance',
                'position-module-6545' => 'Module Instance'
            ],
            'menu' => [
                'menu-4871' => 'Menu'
            ],
            'social' => [
                'social-4022' => 'Social'
            ],
            'content' => [
                'system-content-8106' => 'Page Content'
            ],
            'date' => [
                'date-8867' => 'Date'
            ],
            'infolist' => [
                'infolist-5873' => 'Info List'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
