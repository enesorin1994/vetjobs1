<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/69/index.yaml',
    'modified' => 1488888443,
    'data' => [
        'name' => 69,
        'timestamp' => 1488888443,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'features_-_typography',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-8658' => 'System Messages'
            ],
            'logo' => [
                'logo-1092' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-7378' => 'Icon Menu'
            ],
            'menu' => [
                'menu-8546' => 'Menu'
            ],
            'social' => [
                'social-1670' => 'Social'
            ],
            'custom' => [
                'custom-4745' => 'Particle Typography Header',
                'custom-7725' => 'Content - Content List',
                'custom-8660' => 'Content - Image Grid',
                'custom-9557' => 'Content - Info List',
                'custom-3309' => 'Content - Promo Image',
                'custom-2068' => 'Content - Contact',
                'custom-4935' => 'Button Variations Header',
                'custom-6880' => 'Button Variations Content',
                'custom-1330' => 'Standard Typography Header',
                'custom-4033' => 'Standard Typography'
            ],
            'contentlist' => [
                'contentlist-7803' => 'Content List'
            ],
            'imagegrid' => [
                'imagegrid-7310' => 'Image Grid'
            ],
            'infolist' => [
                'infolist-3012' => 'Info List'
            ],
            'promoimage' => [
                'promoimage-9935' => 'Promo Image'
            ],
            'contact' => [
                'contact-3983' => 'Contact'
            ],
            'copyright' => [
                'copyright-4817' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-8246' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
