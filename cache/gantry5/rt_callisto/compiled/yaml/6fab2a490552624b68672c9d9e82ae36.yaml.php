<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/71/index.yaml',
    'modified' => 1488888443,
    'data' => [
        'name' => 71,
        'timestamp' => 1488888443,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/2-col-right.png',
            'name' => 'pages_-_about_us',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-6772' => 'System Messages'
            ],
            'logo' => [
                'logo-6361' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-9377' => 'Icon Menu'
            ],
            'menu' => [
                'menu-7662' => 'Menu'
            ],
            'social' => [
                'social-5673' => 'Social'
            ],
            'custom' => [
                'custom-1928' => 'About Us',
                'custom-6040' => 'Our Mission',
                'custom-9443' => 'Our Values',
                'custom-4229' => 'Our Solutions',
                'custom-4124' => 'Why You Should Join Us',
                'custom-6840' => 'Introduction - Mission - Business',
                'custom-8135' => 'Sophisticated',
                'custom-2795' => 'Responsive',
                'custom-7698' => 'Powerful',
                'custom-9206' => 'We Always Try to Create a Difference',
                'custom-3558' => 'About Callisto',
                'custom-3447' => 'Sample Sitemap'
            ],
            'infolist' => [
                'infolist-8619' => 'Modern Design',
                'infolist-9622' => 'Huge Features',
                'infolist-8969' => 'Multi Purpose'
            ],
            'newsletter' => [
                'newsletter-4059' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-2967' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-9712' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
