<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/75/index.yaml',
    'modified' => 1488888444,
    'data' => [
        'name' => 75,
        'timestamp' => 1488888444,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/2-col-right.png',
            'name' => 'pages_-_portfolio',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-7116' => 'System Messages'
            ],
            'logo' => [
                'logo-4477' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-5604' => 'Icon Menu'
            ],
            'menu' => [
                'menu-9602' => 'Menu'
            ],
            'social' => [
                'social-2840' => 'Social'
            ],
            'custom' => [
                'custom-4105' => 'Portfolio',
                'custom-9959' => 'Responsive Layout',
                'custom-4317' => '100% Satisfaction Guaranteed',
                'custom-2075' => 'About Callisto',
                'custom-6987' => 'Sample Sitemap'
            ],
            'contentlist' => [
                'contentlist-7883' => 'Callisto Premium Project'
            ],
            'promoimage' => [
                'promoimage-6802' => 'Magazine',
                'promoimage-1805' => 'Logo',
                'promoimage-2200' => 'Brochure',
                'promoimage-8987' => 'Decorations',
                'promoimage-4954' => 'Illustrations',
                'promoimage-2007' => 'Photos'
            ],
            'infolist' => [
                'infolist-4794' => 'Upcoming Projects',
                'infolist-8397' => 'Upcoming Projects Cont.',
                'infolist-5052' => 'Upcoming Projects Cont.'
            ],
            'newsletter' => [
                'newsletter-9595' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-3022' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-5649' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
