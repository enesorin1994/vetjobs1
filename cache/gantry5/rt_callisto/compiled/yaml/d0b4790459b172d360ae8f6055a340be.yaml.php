<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/82/layout.yaml',
    'modified' => 1490265488,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'pages_-_clients',
            'timestamp' => 1463470148
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'logo-4920 80',
                    1 => 'position-module-1922 20'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-8752'
                ]
            ],
            '/showcase/' => [
                0 => [
                    0 => 'position-module-3373'
                ],
                1 => [
                    0 => 'system-content-8183'
                ],
                2 => [
                    0 => 'position-module-8126 50',
                    1 => 'position-module-2057 50'
                ]
            ],
            '/container-8748/' => [
                0 => [
                    0 => [
                        'mainbar 67' => [
                            
                        ]
                    ],
                    1 => [
                        'sidebar 33' => [
                            
                        ]
                    ]
                ]
            ],
            '/extension/' => [
                0 => [
                    0 => 'date-8645'
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'infolist-1186'
                ]
            ],
            '/copyright/' => [
                
            ],
            'offcanvas' => [
                0 => [
                    0 => 'mobile-menu-8532'
                ]
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'mainbar' => [
                'type' => 'section'
            ],
            'sidebar' => [
                'type' => 'section'
            ],
            'container-8748' => [
                'title' => 'Main',
                'attributes' => [
                    'id' => 'g-main',
                    'boxed' => ''
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'logo-4920' => [
                'attributes' => [
                    'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                ],
                'block' => [
                    'class' => 'g-logo-block'
                ]
            ],
            'position-module-1922' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '97',
                    'key' => 'module-instance'
                ]
            ],
            'menu-8752' => [
                'block' => [
                    'class' => 'g-menu-block'
                ]
            ],
            'position-module-3373' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '127',
                    'key' => 'module-instance'
                ]
            ],
            'position-module-8126' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '103',
                    'key' => 'module-instance'
                ],
                'block' => [
                    'variations' => 'box-blue shadow rounded equal-height center'
                ]
            ],
            'position-module-2057' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '123',
                    'key' => 'module-instance'
                ],
                'block' => [
                    'variations' => 'box-blue'
                ]
            ],
            'infolist-1186' => [
                'title' => 'Info List'
            ],
            'mobile-menu-8532' => [
                'title' => 'Mobile Menu'
            ]
        ]
    ]
];
