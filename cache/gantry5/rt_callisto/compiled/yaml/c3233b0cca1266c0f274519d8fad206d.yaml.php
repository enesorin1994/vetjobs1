<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/_error/layout.yaml',
    'modified' => 1488888442,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/error.png',
            'name' => '_error',
            'timestamp' => 1463470148
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'logo-3821 56',
                    1 => 'iconmenu-8173 44'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-7443 80',
                    1 => 'social-5102 20'
                ]
            ],
            '/showcase/' => [
                0 => [
                    0 => 'custom-3466'
                ]
            ],
            '/container-9126/' => [
                0 => [
                    0 => [
                        'mainbar 67' => [
                            0 => [
                                0 => 'system-content-7438'
                            ]
                        ]
                    ],
                    1 => [
                        'sidebar 33' => [
                            
                        ]
                    ]
                ]
            ],
            '/extension/' => [
                0 => [
                    0 => 'custom-4361'
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'custom-9506 33.3333',
                    1 => 'newsletter-6473 33.3333',
                    2 => 'custom-3876 33.3334'
                ]
            ],
            '/copyright/' => [
                0 => [
                    0 => 'copyright-8564'
                ]
            ],
            'offcanvas' => [
                0 => [
                    0 => 'mobile-menu-4164'
                ]
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'class' => 'box1',
                    'boxed' => ''
                ]
            ],
            'mainbar' => [
                'type' => 'section'
            ],
            'sidebar' => [
                'type' => 'section'
            ],
            'container-9126' => [
                'title' => 'Main',
                'attributes' => [
                    'id' => 'g-main',
                    'boxed' => ''
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'logo-3821' => [
                'layout' => false,
                'block' => [
                    'class' => 'g-logo-block'
                ]
            ],
            'iconmenu-8173' => [
                'title' => 'Icon Menu',
                'layout' => false,
                'block' => [
                    'class' => 'flush'
                ]
            ],
            'menu-7443' => [
                'layout' => false,
                'block' => [
                    'class' => 'g-menu-block'
                ]
            ],
            'social-5102' => [
                'layout' => false
            ],
            'custom-3466' => [
                'title' => 'Whooops! Error!',
                'attributes' => [
                    'html' => '<div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Whooops! Error!</h2>
	<div class="g-layercontent-subtitle">Looks like something went wrong. Sorry!</div>
</div>'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'flush center'
                ]
            ],
            'system-content-7438' => [
                'layout' => false,
                'block' => [
                    'class' => 'nomarginall center g-error',
                    'variations' => 'box2'
                ]
            ],
            'custom-4361' => [
                'title' => 'Nothing to See Here',
                'attributes' => [
                    'html' => '<div class="g-layercontent">
	<h2 class="g-layercontent-title">Nothing to See Here</h2>
	<div class="g-layercontent-subtitle">Please use the navigation above or contact us to find what you are looking for.</div>
	<a href="#" class="button button-2">Contact Us</a>
</div>'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'flush center',
                    'variations' => 'box1'
                ]
            ],
            'custom-9506' => [
                'title' => 'About Callisto',
                'attributes' => [
                    'html' => '<h2 class="g-title">About Callisto</h2>

<p>All demo content is for sample purposes only, intended to represent a live site.</p>

<p>The sample pages are intended to show how Callisto can be constructed on your site.</p>'
                ],
                'layout' => false
            ],
            'newsletter-6473' => [
                'attributes' => [
                    'title' => 'Newsletter',
                    'headtext' => 'Subscribe to our newsletter and stay updated on the latest developments and special offers!',
                    'inputboxtext' => 'Email Address',
                    'buttontext' => 'Join',
                    'uri' => 'rocketthemeblog',
                    'buttonclass' => 'button button-3'
                ],
                'layout' => false
            ],
            'custom-3876' => [
                'title' => 'Sample Sitemap',
                'attributes' => [
                    'html' => '<h2 class="g-title">Sample Sitemap</h2>

<div class="g-grid">
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Home</a></li>
			<li><a href="#">Features</a></li>
			<li><a href="#">Typography</a></li>
			<li><a href="#">Particles</a></li>
			<li><a href="#">Variations</a></li>
		</ul>		
	</div>
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Buttons</a></li>
			<li><a href="#">Pages</a></li>
			<li><a href="#">Guide</a></li>
			<li><a href="#">Support</a></li>
			<li><a href="#">Download</a></li>
		</ul>		
	</div>	
</div>'
                ],
                'layout' => false
            ],
            'copyright-8564' => [
                'attributes' => [
                    'date' => [
                        'start' => '2007'
                    ],
                    'owner' => 'RocketTheme, LLC'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'center'
                ]
            ],
            'mobile-menu-4164' => [
                'title' => 'Mobile Menu',
                'layout' => false
            ]
        ]
    ]
];
