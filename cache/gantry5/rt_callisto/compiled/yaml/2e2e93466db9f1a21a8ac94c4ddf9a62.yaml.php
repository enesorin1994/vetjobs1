<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/80/index.yaml',
    'modified' => 1489745687,
    'data' => [
        'name' => '80',
        'timestamp' => 1489745687,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/offline.png',
            'name' => 'pages_-_coming_soon',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-7370' => 'Logo'
            ],
            'menu' => [
                'menu-4265' => 'Menu'
            ],
            'social' => [
                'social-1354' => 'Social'
            ],
            'module' => [
                'position-module-3493' => 'Module Instance',
                'position-module-4744' => 'Module Instance',
                'position-module-2992' => 'Module Instance',
                'position-module-5401' => 'Module Instance',
                'position-module-4643' => 'Module Instance'
            ],
            'content' => [
                'system-content-5575' => 'Page Content'
            ],
            'custom' => [
                'custom-3288' => 'Add a job to your Watchlist'
            ],
            'newsletter' => [
                'newsletter-1051' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-8574' => 'Copyright'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
