<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/76/index.yaml',
    'modified' => 1488888443,
    'data' => [
        'name' => 76,
        'timestamp' => 1488888443,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'pages_-_clients',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-5471' => 'System Messages'
            ],
            'logo' => [
                'logo-4920' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-2030' => 'Icon Menu'
            ],
            'menu' => [
                'menu-8752' => 'Menu'
            ],
            'social' => [
                'social-3107' => 'Social'
            ],
            'custom' => [
                'custom-3299' => 'Clients',
                'custom-3667' => 'Willing to Work With Us',
                'custom-4453' => 'About Callisto',
                'custom-5713' => 'Sample Sitemap'
            ],
            'imagegrid' => [
                'imagegrid-2349' => 'Image Grid'
            ],
            'newsletter' => [
                'newsletter-2334' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-1541' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-8532' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
