<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/71/layout.yaml',
    'modified' => 1488888443,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/2-col-right.png',
            'name' => 'pages_-_about_us',
            'timestamp' => 1463470148
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'system-messages-6772'
                ],
                1 => [
                    0 => 'logo-6361 56',
                    1 => 'iconmenu-9377 44'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-7662 80',
                    1 => 'social-5673 20'
                ]
            ],
            '/showcase/' => [
                0 => [
                    0 => 'custom-1928'
                ]
            ],
            '/container-2905/' => [
                0 => [
                    0 => [
                        'mainbar 67' => [
                            0 => [
                                0 => 'custom-6040 33.3333',
                                1 => 'custom-9443 33.3333',
                                2 => 'custom-4229 33.3333'
                            ],
                            1 => [
                                0 => 'custom-4124'
                            ],
                            2 => [
                                0 => 'custom-6840'
                            ],
                            3 => [
                                0 => 'custom-8135 33.3333',
                                1 => 'custom-2795 33.3333',
                                2 => 'custom-7698 33.3333'
                            ],
                            4 => [
                                0 => 'infolist-8619 33.3333',
                                1 => 'infolist-9622 33.3333',
                                2 => 'infolist-8969 33.3333'
                            ]
                        ]
                    ],
                    1 => [
                        'sidebar 33' => [
                            
                        ]
                    ]
                ]
            ],
            '/extension/' => [
                0 => [
                    0 => 'custom-9206'
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'custom-3558 33.3333',
                    1 => 'newsletter-4059 33.3333999999',
                    2 => 'custom-3447 33.3333'
                ]
            ],
            '/copyright/' => [
                0 => [
                    0 => 'copyright-2967'
                ]
            ],
            'offcanvas' => [
                0 => [
                    0 => 'mobile-menu-9712'
                ]
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'mainbar' => [
                'type' => 'section'
            ],
            'sidebar' => [
                'type' => 'section'
            ],
            'container-2905' => [
                'title' => 'Main',
                'attributes' => [
                    'id' => 'g-main',
                    'boxed' => ''
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'system-messages-6772' => [
                'layout' => false
            ],
            'logo-6361' => [
                'layout' => false,
                'block' => [
                    'class' => 'g-logo-block'
                ]
            ],
            'iconmenu-9377' => [
                'title' => 'Icon Menu',
                'layout' => false,
                'block' => [
                    'class' => 'flush'
                ]
            ],
            'menu-7662' => [
                'layout' => false,
                'block' => [
                    'class' => 'g-menu-block'
                ]
            ],
            'social-5673' => [
                'layout' => false
            ],
            'custom-1928' => [
                'title' => 'About Us',
                'attributes' => [
                    'html' => '<div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">About Us</h2>
	<div class="g-layercontent-subtitle">Who We Are</div>
</div>'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'flush center'
                ]
            ],
            'custom-6040' => [
                'title' => 'Our Mission',
                'attributes' => [
                    'html' => '<h2 class="g-title">Our Mission</h2>
<p>Objectively innovate empowered manufactured products whereas parallel platforms. Holistically predominate extensible testing procedures for reliable supply chains.</p>'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'center nomarginall',
                    'variations' => 'box-blue'
                ]
            ],
            'custom-9443' => [
                'title' => 'Our Values',
                'attributes' => [
                    'html' => '<h2 class="g-title">Our Values</h2>
<p>Objectively innovate empowered manufactured products whereas parallel platforms. Holistically predominate extensible testing procedures for reliable supply chains.</p>'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'center nomarginall',
                    'variations' => 'box1'
                ]
            ],
            'custom-4229' => [
                'title' => 'Our Solutions',
                'attributes' => [
                    'html' => '<h2 class="g-title">Our Solution</h2>
<p>Objectively innovate empowered manufactured products whereas parallel platforms. Holistically predominate extensible testing procedures for reliable supply chains.</p>'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'center nomarginall',
                    'variations' => 'box-orange'
                ]
            ],
            'custom-4124' => [
                'title' => 'Why You Should Join Us',
                'attributes' => [
                    'html' => '<div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Why You Should Join Us</h2>
	<div class="g-layercontent-subtitle">Choose the theme that suits your needs. 100% satisfaction guaranteed.</div>
</div>'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'flush center',
                    'variations' => 'box2'
                ]
            ],
            'custom-6840' => [
                'title' => 'Introduction - Mission - Business',
                'attributes' => [
                    'html' => '<div class="g-grid center">
    <div class="g-block">
        <div class="g-content">
          <img src="gantry-theme://images/demo/pages/pages/about-us/img-01.jpg" alt="image">
          <h2 class="g-title">Introduction</h2>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
          <img src="gantry-theme://images/demo/pages/pages/about-us/img-02.jpg" alt="image">
          <h2 class="g-title">Mission</h2>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
          <img src="gantry-theme://images/demo/pages/pages/about-us/img-03.jpg" alt="image">
          <h2 class="g-title">Business</h2>
        </div>
    </div>
</div>'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'flush',
                    'variations' => 'box2'
                ]
            ],
            'custom-8135' => [
                'title' => 'Sophisticated',
                'attributes' => [
                    'html' => '<h2 class="g-title"><span class="fa fa-dashboard fa-fw fa-3x"></span> Sophisticated</h2>
<p>Dynamically procrastinate B2C users after installed base benefits.</p>'
                ],
                'layout' => false,
                'block' => [
                    'variations' => 'box3'
                ]
            ],
            'custom-2795' => [
                'title' => 'Responsive',
                'attributes' => [
                    'html' => '<h2 class="g-title"><span class="fa fa-arrows-alt fa-fw fa-3x"></span> Responsive</h2>
<p>Dynamically procrastinate B2C users after installed base benefits.</p>'
                ],
                'layout' => false,
                'block' => [
                    'variations' => 'box3'
                ]
            ],
            'custom-7698' => [
                'title' => 'Powerful',
                'attributes' => [
                    'html' => '<h2 class="g-title"><span class="fa fa-sliders fa-fw fa-3x"></span> Powerful</h2>
<p>Dynamically procrastinate B2C users after installed base benefits.</p>'
                ],
                'layout' => false,
                'block' => [
                    'variations' => 'box3'
                ]
            ],
            'infolist-8619' => [
                'title' => 'Modern Design',
                'attributes' => [
                    'infolists' => [
                        0 => [
                            'link' => '#',
                            'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                            'title' => 'Modern Design'
                        ],
                        1 => [
                            'link' => '#',
                            'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                            'title' => 'Awesome Support'
                        ]
                    ]
                ],
                'layout' => false
            ],
            'infolist-9622' => [
                'title' => 'Huge Features',
                'attributes' => [
                    'infolists' => [
                        0 => [
                            'link' => '#',
                            'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                            'title' => 'Huge Features'
                        ],
                        1 => [
                            'link' => '#',
                            'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                            'title' => 'Demo Content'
                        ]
                    ]
                ],
                'layout' => false
            ],
            'infolist-8969' => [
                'title' => 'Multi Purpose',
                'attributes' => [
                    'infolists' => [
                        0 => [
                            'link' => '#',
                            'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                            'title' => 'Multi Purpose'
                        ],
                        1 => [
                            'link' => '#',
                            'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                            'title' => 'Gantry 5'
                        ]
                    ]
                ],
                'layout' => false
            ],
            'custom-9206' => [
                'title' => 'We Always Try to Create a Difference',
                'attributes' => [
                    'html' => '<div class="g-layercontent">
	<h2 class="g-layercontent-title">We Always Try to Create a Difference</h2>
	<div class="g-layercontent-subtitle">Versatile and Flexible Features Powered by the Gantry Framework.</div>
	<a href="http://www.rockettheme.com/joomla/templates/callisto" class="button button-2">Download Callisto</a>
</div>'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'flush center',
                    'variations' => 'box1'
                ]
            ],
            'custom-3558' => [
                'title' => 'About Callisto',
                'attributes' => [
                    'html' => '<h2 class="g-title">About Callisto</h2>

<p>All demo content is for sample purposes only, intended to represent a live site.</p>

<p>The sample pages are intended to show how Callisto can be constructed on your site.</p>'
                ],
                'layout' => false
            ],
            'newsletter-4059' => [
                'attributes' => [
                    'title' => 'Newsletter',
                    'headtext' => 'Subscribe to our newsletter and stay updated on the latest developments and special offers!',
                    'inputboxtext' => 'Email Address',
                    'buttontext' => 'Join',
                    'uri' => 'rocketthemeblog',
                    'buttonclass' => 'button button-3'
                ],
                'layout' => false
            ],
            'custom-3447' => [
                'title' => 'Sample Sitemap',
                'attributes' => [
                    'html' => '<h2 class="g-title">Sample Sitemap</h2>

<div class="g-grid">
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Home</a></li>
			<li><a href="#">Features</a></li>
			<li><a href="#">Typography</a></li>
			<li><a href="#">Particles</a></li>
			<li><a href="#">Variations</a></li>
		</ul>		
	</div>
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Buttons</a></li>
			<li><a href="#">Pages</a></li>
			<li><a href="#">Guide</a></li>
			<li><a href="#">Support</a></li>
			<li><a href="#">Download</a></li>
		</ul>		
	</div>	
</div>'
                ],
                'layout' => false
            ],
            'copyright-2967' => [
                'attributes' => [
                    'date' => [
                        'start' => '2007'
                    ],
                    'owner' => 'RocketTheme, LLC'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'center'
                ]
            ],
            'mobile-menu-9712' => [
                'title' => 'Mobile Menu',
                'layout' => false
            ]
        ]
    ]
];
