<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/default/index.yaml',
    'modified' => 1488888442,
    'data' => [
        'name' => 'default',
        'timestamp' => 1488888442,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-9283' => 'System Messages'
            ],
            'logo' => [
                'logo-8920' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-8998' => 'Icon Menu'
            ],
            'menu' => [
                'menu-8316' => 'Menu'
            ],
            'social' => [
                'social-4391' => 'Social'
            ],
            'content' => [
                'system-content-9244' => 'Page Content'
            ],
            'custom' => [
                'custom-1806' => 'About Callisto',
                'custom-8186' => 'Sample Sitemap'
            ],
            'newsletter' => [
                'newsletter-2485' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-4423' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-3736' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
