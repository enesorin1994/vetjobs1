<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/80/layout.yaml',
    'modified' => 1489745687,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/offline.png',
            'name' => 'pages_-_coming_soon',
            'timestamp' => 1463470148
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'logo-7370'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-4265 80',
                    1 => 'social-1354 20'
                ]
            ],
            '/showcase/' => [
                0 => [
                    0 => 'position-module-3493'
                ],
                1 => [
                    0 => 'position-module-4744 33.3',
                    1 => 'position-module-2992 33.3',
                    2 => 'position-module-5401 33.3'
                ]
            ],
            '/container-2571/' => [
                0 => [
                    0 => [
                        'mainbar 67' => [
                            0 => [
                                0 => 'system-content-5575'
                            ]
                        ]
                    ],
                    1 => [
                        'sidebar 33' => [
                            0 => [
                                0 => 'position-module-4643'
                            ]
                        ]
                    ]
                ]
            ],
            '/extension/' => [
                0 => [
                    0 => 'custom-3288'
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'newsletter-1051'
                ]
            ],
            '/copyright/' => [
                0 => [
                    0 => 'copyright-8574'
                ]
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'mainbar' => [
                'type' => 'section'
            ],
            'sidebar' => [
                'type' => 'section'
            ],
            'container-2571' => [
                'title' => 'Main',
                'attributes' => [
                    'id' => 'g-main',
                    'boxed' => ''
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'logo-7370' => [
                'attributes' => [
                    'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                ],
                'block' => [
                    'class' => 'g-logo-block center'
                ]
            ],
            'menu-4265' => [
                'attributes' => [
                    'menu' => 'main-menu'
                ]
            ],
            'position-module-3493' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '93',
                    'key' => 'module-instance'
                ]
            ],
            'position-module-4744' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '103',
                    'key' => 'module-instance'
                ]
            ],
            'position-module-2992' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '123',
                    'key' => 'module-instance'
                ]
            ],
            'position-module-5401' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '131'
                ]
            ],
            'position-module-4643' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '97',
                    'key' => 'module-instance'
                ]
            ],
            'custom-3288' => [
                'title' => 'Add a job to your Watchlist',
                'attributes' => [
                    'html' => '<div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Add a job to your Watchlist</h2>
	<div class="g-layercontent-subtitle">Press the <strong>Add to Watchlist</strong> button under the Job Title</div>
	'
                ],
                'block' => [
                    'class' => 'flush center',
                    'variations' => 'box1'
                ]
            ],
            'newsletter-1051' => [
                'attributes' => [
                    'title' => 'Newsletter',
                    'headtext' => 'Aboneaza-te la newsletter-ul VetJobs si ramai la curent cu ultimele joburi din Medicina veterinara!',
                    'inputboxtext' => 'Email Address',
                    'buttontext' => 'Join',
                    'uri' => 'rocketthemeblog',
                    'buttonclass' => 'button button-3'
                ]
            ],
            'copyright-8574' => [
                'attributes' => [
                    'date' => [
                        'start' => '2007'
                    ],
                    'owner' => 'RocketTheme, LLC'
                ],
                'block' => [
                    'class' => 'center'
                ]
            ]
        ]
    ]
];
