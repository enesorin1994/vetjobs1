<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/config/default/particles/iconmenu.yaml',
    'modified' => 1463470148,
    'data' => [
        'enabled' => '1',
        'class' => '',
        'target' => '_parent',
        'items' => [
            0 => [
                'icon' => 'fa fa-diamond',
                'text' => 'Features',
                'link' => 'http://docs.gantry.org/gantry5/basics/key-features',
                'name' => 'Features'
            ],
            1 => [
                'icon' => 'fa fa-rocket',
                'text' => 'Gantry 5',
                'link' => 'http://www.gantry.org/',
                'name' => 'Gantry 5'
            ],
            2 => [
                'icon' => 'fa fa-gear',
                'text' => 'Addons',
                'link' => 'http://docs.gantry.org/gantry5/particles',
                'name' => 'Addons'
            ],
            3 => [
                'icon' => 'fa fa-cloud-download',
                'text' => 'Download',
                'link' => 'http://www.rockettheme.com/joomla/templates/callisto',
                'name' => 'Download'
            ]
        ]
    ]
];
