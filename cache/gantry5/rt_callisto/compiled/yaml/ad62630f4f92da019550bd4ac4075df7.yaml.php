<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/80/styles.yaml',
    'modified' => 1489571583,
    'data' => [
        'preset' => 'preset7',
        'base' => [
            'background' => '#ffffff'
        ],
        'accent' => [
            'color-1' => '#1abc9c',
            'color-2' => '#007e65'
        ],
        'header' => [
            'background' => '#1abc9c',
            'text-color' => '#138871'
        ],
        'navigation' => [
            'background' => '#ffffff',
            'text-color' => '#888888'
        ],
        'showcase' => [
            'background' => '#e8e8e8',
            'text-color' => '#878787'
        ],
        'feature' => [
            'background' => '#f5f5f5'
        ],
        'main' => [
            'background' => '#f5f5f5'
        ],
        'footer' => [
            'background' => '#ffffff',
            'text-color' => '#888888'
        ],
        'copyright' => [
            'background' => '#f5f5f5'
        ],
        'extension' => [
            'background' => '#f5f5f5'
        ],
        'offcanvas' => [
            'background' => '#f0f0f0',
            'text-color' => '#888888'
        ]
    ]
];
