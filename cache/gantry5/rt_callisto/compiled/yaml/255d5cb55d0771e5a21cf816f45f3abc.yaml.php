<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/menu/main-menu.yaml',
    'modified' => 1489579782,
    'data' => [
        'ordering' => [
            'home' => '',
            'jobs' => '',
            'job-categories' => '',
            'companies' => '',
            'candidates' => '',
            'my-job-list' => '',
            'add-job' => '',
            'profile' => '',
            'advertisements' => '',
            'new-ad' => '',
            'my-advertisements' => '',
            'new-course' => '',
            'available-courses' => '',
            'events-profile' => ''
        ],
        'items' => [
            'home' => [
                'id' => 455
            ],
            'jobs' => [
                'id' => 441
            ],
            'companies' => [
                'id' => 443
            ],
            'candidates' => [
                'id' => 444
            ],
            'job-categories' => [
                'id' => 442
            ],
            'my-job-list' => [
                'id' => 445
            ],
            'add-job' => [
                'id' => 446
            ],
            'profile' => [
                'id' => 448
            ],
            'advertisements' => [
                'id' => 449
            ],
            'new-ad' => [
                'id' => 450
            ],
            'my-advertisements' => [
                'id' => 451
            ],
            'new-course' => [
                'id' => 452
            ],
            'available-courses' => [
                'id' => 453
            ],
            'events-profile' => [
                'id' => 454
            ]
        ]
    ]
];
