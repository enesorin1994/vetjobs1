<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/blueprints/styles/feature.yaml',
    'modified' => 1463470148,
    'data' => [
        'name' => 'Feature Colors',
        'description' => 'Feature colors for the Callisto theme',
        'type' => 'section',
        'form' => [
            'fields' => [
                'background' => [
                    'type' => 'input.colorpicker',
                    'label' => 'Background',
                    'default' => '#1a1a1a'
                ],
                'text-color' => [
                    'type' => 'input.colorpicker',
                    'label' => 'Text',
                    'default' => '#878787'
                ]
            ]
        ]
    ]
];
