<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/78/index.yaml',
    'modified' => 1488888443,
    'data' => [
        'name' => 78,
        'timestamp' => 1488888443,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/2-col-right.png',
            'name' => 'pages_-_blog',
            'timestamp' => 1463470148
        ],
        'positions' => [
            'sidebar-a' => 'Main Menu Module',
            'sidebar-b' => 'Login Module',
            'sidebar-c' => 'Who\'s Online Module'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-8775' => 'System Messages'
            ],
            'logo' => [
                'logo-1457' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-2140' => 'Icon Menu'
            ],
            'menu' => [
                'menu-9364' => 'Menu'
            ],
            'social' => [
                'social-5709' => 'Social'
            ],
            'custom' => [
                'custom-2394' => 'Our Blog',
                'custom-3728' => 'Gantry 5 Guides',
                'custom-1834' => 'Share Some Ideas',
                'custom-1556' => 'About Callisto',
                'custom-4987' => 'Sample Sitemap'
            ],
            'content' => [
                'system-content-2578' => 'Page Content'
            ],
            'position' => [
                'position-2984' => 'Main Menu Module',
                'position-4718' => 'Login Module',
                'position-5988' => 'Who\'s Online Module'
            ],
            'newsletter' => [
                'newsletter-2790' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-3971' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-3851' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
