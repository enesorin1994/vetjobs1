<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/default/layout.yaml',
    'modified' => 1488888442,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1463470148
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'system-messages-9283'
                ],
                1 => [
                    0 => 'logo-8920 56',
                    1 => 'iconmenu-8998 44'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-8316 80',
                    1 => 'social-4391 20'
                ]
            ],
            '/showcase/' => [
                
            ],
            '/container-5483/' => [
                0 => [
                    0 => [
                        'mainbar 67' => [
                            0 => [
                                0 => 'system-content-9244'
                            ]
                        ]
                    ],
                    1 => [
                        'sidebar 33' => [
                            
                        ]
                    ]
                ]
            ],
            '/extension/' => [
                
            ],
            '/footer/' => [
                0 => [
                    0 => 'custom-1806 33.3333',
                    1 => 'newsletter-2485 33.3333',
                    2 => 'custom-8186 33.3334'
                ]
            ],
            '/copyright/' => [
                0 => [
                    0 => 'copyright-4423'
                ]
            ],
            'offcanvas' => [
                0 => [
                    0 => 'mobile-menu-3736'
                ]
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'class' => 'box1',
                    'boxed' => ''
                ]
            ],
            'mainbar' => [
                'type' => 'section'
            ],
            'sidebar' => [
                'type' => 'section'
            ],
            'container-5483' => [
                'title' => 'Main',
                'attributes' => [
                    'id' => 'g-main',
                    'boxed' => ''
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'system-messages-9283' => [
                'layout' => false
            ],
            'logo-8920' => [
                'layout' => false,
                'block' => [
                    'class' => 'g-logo-block'
                ]
            ],
            'iconmenu-8998' => [
                'title' => 'Icon Menu',
                'layout' => false,
                'block' => [
                    'class' => 'flush'
                ]
            ],
            'menu-8316' => [
                'layout' => false,
                'block' => [
                    'class' => 'g-menu-block'
                ]
            ],
            'social-4391' => [
                'layout' => false
            ],
            'system-content-9244' => [
                'layout' => false,
                'block' => [
                    'class' => 'nomarginall',
                    'variations' => 'box2'
                ]
            ],
            'custom-1806' => [
                'title' => 'About Callisto',
                'attributes' => [
                    'html' => '<h2 class="g-title">About Callisto</h2>

<p>All demo content is for sample purposes only, intended to represent a live site.</p>

<p>The sample pages are intended to show how Callisto can be constructed on your site.</p>'
                ],
                'layout' => false
            ],
            'newsletter-2485' => [
                'attributes' => [
                    'title' => 'Newsletter',
                    'headtext' => 'Subscribe to our newsletter and stay updated on the latest developments and special offers!',
                    'inputboxtext' => 'Email Address',
                    'buttontext' => 'Join',
                    'uri' => 'rocketthemeblog',
                    'buttonclass' => 'button button-3'
                ],
                'layout' => false
            ],
            'custom-8186' => [
                'title' => 'Sample Sitemap',
                'attributes' => [
                    'html' => '<h2 class="g-title">Sample Sitemap</h2>

<div class="g-grid">
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Home</a></li>
			<li><a href="#">Features</a></li>
			<li><a href="#">Typography</a></li>
			<li><a href="#">Particles</a></li>
			<li><a href="#">Variations</a></li>
		</ul>		
	</div>
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Buttons</a></li>
			<li><a href="#">Pages</a></li>
			<li><a href="#">Guide</a></li>
			<li><a href="#">Support</a></li>
			<li><a href="#">Download</a></li>
		</ul>		
	</div>	
</div>'
                ],
                'layout' => false
            ],
            'copyright-4423' => [
                'attributes' => [
                    'date' => [
                        'start' => '2007'
                    ],
                    'owner' => 'RocketTheme, LLC'
                ],
                'layout' => false,
                'block' => [
                    'class' => 'center'
                ]
            ],
            'mobile-menu-3736' => [
                'title' => 'Mobile Menu',
                'layout' => false
            ]
        ]
    ]
];
