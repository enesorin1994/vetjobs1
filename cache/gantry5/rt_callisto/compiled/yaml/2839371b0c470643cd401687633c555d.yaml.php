<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/73/index.yaml',
    'modified' => 1488888444,
    'data' => [
        'name' => 73,
        'timestamp' => 1488888444,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/home.png',
            'name' => 'pages_-_services',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-9875' => 'System Messages'
            ],
            'logo' => [
                'logo-5652' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-4553' => 'Icon Menu'
            ],
            'menu' => [
                'menu-8687' => 'Menu'
            ],
            'social' => [
                'social-4006' => 'Social'
            ],
            'custom' => [
                'custom-4317' => 'What We Do',
                'custom-2009' => 'Desktop',
                'custom-6781' => 'Mobile',
                'custom-4676' => 'Cloud',
                'custom-9914' => 'Databases',
                'custom-6351' => 'Graphics',
                'custom-5527' => 'Analyst',
                'custom-1391' => 'Save Time and Effort',
                'custom-8504' => 'About Callisto',
                'custom-9343' => 'Sample Sitemap'
            ],
            'contentlist' => [
                'contentlist-7881' => 'Why Choose Us'
            ],
            'infolist' => [
                'infolist-7878' => 'Graphic Design',
                'infolist-7247' => 'Programming',
                'infolist-5944' => 'Hosting'
            ],
            'newsletter' => [
                'newsletter-9268' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-6999' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-9068' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
