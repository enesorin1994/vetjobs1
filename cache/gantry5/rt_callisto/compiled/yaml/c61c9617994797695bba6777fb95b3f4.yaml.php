<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/82/index.yaml',
    'modified' => 1490265488,
    'data' => [
        'name' => '82',
        'timestamp' => 1490265488,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'pages_-_clients',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-4920' => 'Logo'
            ],
            'module' => [
                'position-module-1922' => 'Module Instance',
                'position-module-3373' => 'Module Instance',
                'position-module-8126' => 'Module Instance',
                'position-module-2057' => 'Module Instance'
            ],
            'menu' => [
                'menu-8752' => 'Menu'
            ],
            'content' => [
                'system-content-8183' => 'Page Content'
            ],
            'date' => [
                'date-8645' => 'Date'
            ],
            'infolist' => [
                'infolist-1186' => 'Info List'
            ],
            'mobile-menu' => [
                'mobile-menu-8532' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
