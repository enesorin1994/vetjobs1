<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/77/index.yaml',
    'modified' => 1488888444,
    'data' => [
        'name' => 77,
        'timestamp' => 1488888444,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'pages_-_faq',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-1063' => 'System Messages'
            ],
            'logo' => [
                'logo-4549' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-9842' => 'Icon Menu'
            ],
            'menu' => [
                'menu-4594' => 'Menu'
            ],
            'social' => [
                'social-4670' => 'Social'
            ],
            'custom' => [
                'custom-7657' => 'Frequently Asked Questions',
                'custom-7428' => 'Email',
                'custom-5683' => 'Phone',
                'custom-5533' => 'Chat',
                'custom-7055' => 'Installation',
                'custom-5783' => 'Basic Usage',
                'custom-4611' => 'Customization',
                'custom-2802' => 'Development',
                'custom-9726' => 'Accounts',
                'custom-1759' => 'Subscription',
                'custom-5230' => 'Security',
                'custom-1542' => 'Organization',
                'custom-5286' => 'Partnership',
                'custom-4220' => 'Live Widget',
                'custom-4485' => 'Web API',
                'custom-7313' => 'Legal Stuff',
                'custom-6548' => 'FAQ Didn\'t Solve Your Problem?',
                'custom-8985' => 'About Callisto',
                'custom-2335' => 'Sample Sitemap'
            ],
            'infolist' => [
                'infolist-4721' => 'What prices?',
                'infolist-7588' => 'What delivery?'
            ],
            'newsletter' => [
                'newsletter-4669' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-7691' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-5647' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
