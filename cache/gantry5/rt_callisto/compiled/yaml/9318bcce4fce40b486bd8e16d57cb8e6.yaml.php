<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/66/layout.yaml',
    'modified' => 1490784938,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1463470148
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'logo-8844 80',
                    1 => 'position-module-6670 20'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-4871 80',
                    1 => 'social-4022 20'
                ]
            ],
            '/showcase/' => [
                0 => [
                    0 => 'position-module-8446'
                ],
                1 => [
                    0 => 'system-content-8106'
                ],
                2 => [
                    0 => 'position-module-2531 50',
                    1 => 'position-module-9396 50'
                ],
                3 => [
                    0 => 'position-module-6545'
                ]
            ],
            '/container-6538/' => [
                0 => [
                    0 => [
                        'mainbar 73' => [
                            
                        ]
                    ],
                    1 => [
                        'sidebar 27' => [
                            
                        ]
                    ]
                ]
            ],
            '/extension/' => [
                0 => [
                    0 => 'date-8867'
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'infolist-5873'
                ]
            ],
            '/copyright/' => [
                
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'class' => 'box1',
                    'boxed' => ''
                ]
            ],
            'mainbar' => [
                'type' => 'section'
            ],
            'sidebar' => [
                'type' => 'section'
            ],
            'container-6538' => [
                'title' => 'Main',
                'attributes' => [
                    'id' => 'g-main',
                    'boxed' => ''
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'logo-8844' => [
                'title' => 'Logo / Image',
                'attributes' => [
                    'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                ]
            ],
            'position-module-6670' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '97',
                    'key' => 'module-instance'
                ]
            ],
            'menu-4871' => [
                'attributes' => [
                    'menu' => 'main-menu'
                ]
            ],
            'social-4022' => [
                'attributes' => [
                    'items' => [
                        0 => [
                            'icon' => 'fa fa-twitter fa-fw',
                            'text' => '',
                            'link' => 'http://twitter.com/rockettheme',
                            'name' => 'Twitter'
                        ],
                        1 => [
                            'icon' => 'fa fa-facebook fa-fw',
                            'text' => '',
                            'link' => 'http://facebook.com/rockettheme',
                            'name' => 'Facebook'
                        ],
                        2 => [
                            'icon' => 'fa fa-google fa-fw',
                            'text' => '',
                            'link' => 'http://plus.google.com/+rockettheme',
                            'name' => 'Google'
                        ]
                    ]
                ]
            ],
            'position-module-8446' => [
                'title' => 'search',
                'attributes' => [
                    'module_id' => '106',
                    'key' => 'search'
                ]
            ],
            'system-content-8106' => [
                'block' => [
                    'variations' => 'box2'
                ]
            ],
            'position-module-2531' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '103',
                    'key' => 'module-instance'
                ],
                'block' => [
                    'variations' => 'shadow rounded equal-height center box-orange'
                ]
            ],
            'position-module-9396' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '101',
                    'key' => 'module-instance'
                ],
                'block' => [
                    'variations' => 'shadow2 rounded equal-height center box-orange'
                ]
            ],
            'position-module-6545' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '88',
                    'key' => 'module-instance'
                ]
            ],
            'infolist-5873' => [
                'title' => 'Info List'
            ]
        ]
    ]
];
