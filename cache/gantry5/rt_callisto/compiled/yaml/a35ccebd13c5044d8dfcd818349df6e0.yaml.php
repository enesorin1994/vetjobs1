<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\xampp\\htdocs\\Vet/templates/rt_callisto/custom/config/67/index.yaml',
    'modified' => 1490861899,
    'data' => [
        'name' => '67',
        'timestamp' => 1490861899,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/home.png',
            'name' => 'home',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainfeature' => 'Mainfeature',
            'sidefeature' => 'Sidefeature',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-7854' => 'Logo'
            ],
            'module' => [
                'position-module-7600' => 'Module Instance',
                'position-module-4689' => 'searchbox',
                'position-module-8679' => 'latest',
                'position-module-4798' => 'featured'
            ],
            'menu' => [
                'menu-5647' => 'Menu'
            ],
            'social' => [
                'social-3414' => 'Social'
            ],
            'content' => [
                'system-content-8850' => 'Page Content'
            ],
            'contentlist' => [
                'contentlist-3684' => 'FP Main Content List'
            ],
            'contact' => [
                'contact-8860' => 'Contact Details'
            ],
            'copyright' => [
                'copyright-5334' => 'Copyright'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
