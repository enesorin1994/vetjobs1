<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/72/index.yaml',
    'modified' => 1488888444,
    'data' => [
        'name' => 72,
        'timestamp' => 1488888444,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/home.png',
            'name' => 'pages_-_our_team',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-5949' => 'System Messages'
            ],
            'logo' => [
                'logo-7046' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-1932' => 'Icon Menu'
            ],
            'menu' => [
                'menu-5121' => 'Menu'
            ],
            'social' => [
                'social-6041' => 'Social'
            ],
            'custom' => [
                'custom-3966' => 'The Team',
                'custom-4967' => 'Joe Jensen',
                'custom-6428' => 'Paul Lee',
                'custom-4440' => 'Completely synergize',
                'custom-1572' => 'Louis Wells',
                'custom-4351' => 'Sara Valdez',
                'custom-1114' => 'Joseph Hart',
                'custom-1752' => 'Amy Guzman',
                'custom-5663' => 'Randy Hicks',
                'custom-3118' => 'Grace Patel',
                'custom-9191' => 'Roy Fisher',
                'custom-5822' => 'Pamela Little',
                'custom-7300' => 'Scott Bell',
                'custom-8031' => 'We Are Hiring',
                'custom-5380' => 'About Callisto',
                'custom-8970' => 'Sample Sitemap'
            ],
            'newsletter' => [
                'newsletter-2159' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-6143' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-6744' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
