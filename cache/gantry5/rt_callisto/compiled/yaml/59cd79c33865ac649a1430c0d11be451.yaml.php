<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/_offline/index.yaml',
    'modified' => 1488888442,
    'data' => [
        'name' => '_offline',
        'timestamp' => 1488888442,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/offline.png',
            'name' => '_offline',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-2769' => 'System Messages'
            ],
            'logo' => [
                'logo-5779' => 'Logo'
            ],
            'custom' => [
                'custom-7874' => 'Offline',
                'custom-3384' => 'Maintenance Mode',
                'custom-8999' => 'About Callisto'
            ],
            'content' => [
                'system-content-7060' => 'Page Content'
            ],
            'newsletter' => [
                'newsletter-9375' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-8319' => 'Copyright'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
