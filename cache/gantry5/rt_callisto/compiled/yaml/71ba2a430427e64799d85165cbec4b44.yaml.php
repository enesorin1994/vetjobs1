<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/74/index.yaml',
    'modified' => 1488888444,
    'data' => [
        'name' => 74,
        'timestamp' => 1488888444,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'pages_-_pricing',
            'timestamp' => 1463470148
        ],
        'positions' => [
            
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'showcase' => 'Showcase',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'extension' => 'Extension',
            'copyright' => 'Copyright',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-7836' => 'System Messages'
            ],
            'logo' => [
                'logo-5119' => 'Logo'
            ],
            'iconmenu' => [
                'iconmenu-7329' => 'Icon Menu'
            ],
            'menu' => [
                'menu-2296' => 'Menu'
            ],
            'social' => [
                'social-1452' => 'Social'
            ],
            'custom' => [
                'custom-2931' => 'Awesome Plans',
                'custom-9245' => 'Basic',
                'custom-6358' => 'Standard',
                'custom-3237' => 'Pro',
                'custom-7704' => 'Try it Out',
                'custom-4712' => 'No Hidden Fees',
                'custom-6680' => 'About Callisto',
                'custom-4411' => 'Sample Sitemap'
            ],
            'infolist' => [
                'infolist-8720' => 'Common',
                'infolist-7128' => 'Common Cont.'
            ],
            'newsletter' => [
                'newsletter-3456' => 'Newsletter'
            ],
            'copyright' => [
                'copyright-6625' => 'Copyright'
            ],
            'mobile-menu' => [
                'mobile-menu-8540' => 'Mobile Menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
