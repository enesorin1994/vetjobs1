<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Vet/templates/rt_callisto/custom/config/68/layout.yaml',
    'modified' => 1490863031,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/home.png',
            'name' => 'features_-_overview',
            'timestamp' => 1463470148
        ],
        'layout' => [
            '/header/' => [
                0 => [
                    0 => 'logo-6050 80',
                    1 => 'position-module-6475 20'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'menu-4774 80',
                    1 => 'social-9696 20'
                ]
            ],
            '/showcase/' => [
                0 => [
                    0 => 'position-module-8986'
                ],
                1 => [
                    0 => 'custom-3582'
                ],
                2 => [
                    0 => 'system-content-2146'
                ],
                3 => [
                    0 => 'position-module-4565 50',
                    1 => 'position-module-6126 50'
                ]
            ],
            '/container-8514/' => [
                0 => [
                    0 => [
                        'mainbar 67' => [
                            
                        ]
                    ],
                    1 => [
                        'sidebar 33' => [
                            
                        ]
                    ]
                ]
            ],
            '/extension/' => [
                0 => [
                    0 => 'custom-3561'
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'newsletter-1649'
                ]
            ],
            '/copyright/' => [
                0 => [
                    0 => 'copyright-6987'
                ]
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'mainbar' => [
                'type' => 'section'
            ],
            'sidebar' => [
                'type' => 'section'
            ],
            'container-8514' => [
                'title' => 'Main',
                'attributes' => [
                    'id' => 'g-main',
                    'boxed' => ''
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'logo-6050' => [
                'attributes' => [
                    'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                ],
                'block' => [
                    'class' => 'g-logo-block'
                ]
            ],
            'position-module-6475' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '97',
                    'key' => 'module-instance'
                ]
            ],
            'menu-4774' => [
                'block' => [
                    'class' => 'g-menu-block'
                ]
            ],
            'position-module-8986' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '106',
                    'key' => 'module-instance'
                ]
            ],
            'custom-3582' => [
                'title' => 'Gantry 5 - Next Generation Theme Framework',
                'attributes' => [
                    'html' => '<div class="g-layercontent">
    <h1 class="g-layercontent-title g-uppercase">VetJobs </h1>
    <h3 class="g-layercontent-title g-uppercase">Cauta job-uri in categoriile de mai jos:</h3>
    <h5 class="g-layercontent-subtitle nomarginall"></h5>

    <a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=1&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Animale mici/companie</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=2&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Animale de ferma</a></br>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=3&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Clinica mixta</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=4&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Ecvine</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=5&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Animale exotice</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=2&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Animale de ferma</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=6&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Management</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=7&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Acvacultura</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=9&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Circumscriptie veterinara</a>




</div>

'
                ],
                'block' => [
                    'class' => 'flush center'
                ]
            ],
            'system-content-2146' => [
                'block' => [
                    'variations' => 'box2'
                ]
            ],
            'position-module-4565' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '103',
                    'key' => 'module-instance'
                ],
                'block' => [
                    'variations' => 'shadow rounded equal-height center box-orange'
                ]
            ],
            'position-module-6126' => [
                'title' => 'Module Instance',
                'attributes' => [
                    'module_id' => '123',
                    'key' => 'module-instance'
                ],
                'block' => [
                    'variations' => 'box-blue shadow rounded equal-height center'
                ]
            ],
            'custom-3561' => [
                'title' => 'Add To Watchlist',
                'attributes' => [
                    'html' => '<div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Add a job to your Watchlist</h2>
	<div class="g-layercontent-subtitle">Press the <strong>Add to Watchlist</strong> button under the Job Title</div>
	'
                ],
                'block' => [
                    'class' => 'flush center',
                    'variations' => 'box1'
                ]
            ],
            'newsletter-1649' => [
                'attributes' => [
                    'title' => 'Newsletter',
                    'headtext' => 'Aboneaza-te la newsletter-ul VetJobs si ramai la curent cu ultimele joburi din Medicina veterinara!',
                    'inputboxtext' => 'E-mail adress',
                    'buttontext' => 'Join',
                    'buttonclass' => 'button button-3'
                ],
                'block' => [
                    'class' => 'flush center'
                ]
            ],
            'copyright-6987' => [
                'attributes' => [
                    'date' => [
                        'start' => '2007'
                    ],
                    'owner' => 'RocketTheme, LLC'
                ],
                'block' => [
                    'class' => 'center'
                ]
            ]
        ]
    ]
];
