<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledBlueprints',
    'timestamp' => 1489567545,
    'checksum' => '83a74ca4c17ddf000b0a4a5737a415d0',
    'files' => [
        'templates/rt_callisto/particles' => [
            'particles/contact' => [
                'file' => 'templates/rt_callisto/particles/contact.yaml',
                'modified' => 1463470148
            ],
            'particles/contentlist' => [
                'file' => 'templates/rt_callisto/particles/contentlist.yaml',
                'modified' => 1463470148
            ],
            'particles/iconmenu' => [
                'file' => 'templates/rt_callisto/particles/iconmenu.yaml',
                'modified' => 1463470148
            ],
            'particles/imagegrid' => [
                'file' => 'templates/rt_callisto/particles/imagegrid.yaml',
                'modified' => 1463470148
            ],
            'particles/infolist' => [
                'file' => 'templates/rt_callisto/particles/infolist.yaml',
                'modified' => 1463470148
            ],
            'particles/newsletter' => [
                'file' => 'templates/rt_callisto/particles/newsletter.yaml',
                'modified' => 1463470148
            ],
            'particles/promoimage' => [
                'file' => 'templates/rt_callisto/particles/promoimage.yaml',
                'modified' => 1463470148
            ],
            'particles/simplecounter' => [
                'file' => 'templates/rt_callisto/particles/simplecounter.yaml',
                'modified' => 1463470148
            ]
        ],
        'media/gantry5/engines/nucleus/particles' => [
            'particles/analytics' => [
                'file' => 'media/gantry5/engines/nucleus/particles/analytics.yaml',
                'modified' => 1489567502
            ],
            'particles/assets' => [
                'file' => 'media/gantry5/engines/nucleus/particles/assets.yaml',
                'modified' => 1489567502
            ],
            'particles/branding' => [
                'file' => 'media/gantry5/engines/nucleus/particles/branding.yaml',
                'modified' => 1489567502
            ],
            'particles/content' => [
                'file' => 'media/gantry5/engines/nucleus/particles/content.yaml',
                'modified' => 1489567502
            ],
            'particles/contentarray' => [
                'file' => 'media/gantry5/engines/nucleus/particles/contentarray.yaml',
                'modified' => 1489567502
            ],
            'particles/copyright' => [
                'file' => 'media/gantry5/engines/nucleus/particles/copyright.yaml',
                'modified' => 1489567502
            ],
            'particles/custom' => [
                'file' => 'media/gantry5/engines/nucleus/particles/custom.yaml',
                'modified' => 1489567502
            ],
            'particles/date' => [
                'file' => 'media/gantry5/engines/nucleus/particles/date.yaml',
                'modified' => 1489567502
            ],
            'particles/frameworks' => [
                'file' => 'media/gantry5/engines/nucleus/particles/frameworks.yaml',
                'modified' => 1489567502
            ],
            'particles/lightcase' => [
                'file' => 'media/gantry5/engines/nucleus/particles/lightcase.yaml',
                'modified' => 1489567502
            ],
            'particles/logo' => [
                'file' => 'media/gantry5/engines/nucleus/particles/logo.yaml',
                'modified' => 1489567502
            ],
            'particles/menu' => [
                'file' => 'media/gantry5/engines/nucleus/particles/menu.yaml',
                'modified' => 1489567502
            ],
            'particles/messages' => [
                'file' => 'media/gantry5/engines/nucleus/particles/messages.yaml',
                'modified' => 1489567502
            ],
            'particles/mobile-menu' => [
                'file' => 'media/gantry5/engines/nucleus/particles/mobile-menu.yaml',
                'modified' => 1489567502
            ],
            'particles/module' => [
                'file' => 'media/gantry5/engines/nucleus/particles/module.yaml',
                'modified' => 1489567502
            ],
            'particles/position' => [
                'file' => 'media/gantry5/engines/nucleus/particles/position.yaml',
                'modified' => 1489567502
            ],
            'particles/social' => [
                'file' => 'media/gantry5/engines/nucleus/particles/social.yaml',
                'modified' => 1489567502
            ],
            'particles/spacer' => [
                'file' => 'media/gantry5/engines/nucleus/particles/spacer.yaml',
                'modified' => 1489567502
            ],
            'particles/totop' => [
                'file' => 'media/gantry5/engines/nucleus/particles/totop.yaml',
                'modified' => 1489567502
            ]
        ],
        'templates/rt_callisto/blueprints' => [
            'styles/accent' => [
                'file' => 'templates/rt_callisto/blueprints/styles/accent.yaml',
                'modified' => 1463470148
            ],
            'styles/base' => [
                'file' => 'templates/rt_callisto/blueprints/styles/base.yaml',
                'modified' => 1463470148
            ],
            'styles/breakpoints' => [
                'file' => 'templates/rt_callisto/blueprints/styles/breakpoints.yaml',
                'modified' => 1463470148
            ],
            'styles/copyright' => [
                'file' => 'templates/rt_callisto/blueprints/styles/copyright.yaml',
                'modified' => 1463470148
            ],
            'styles/extension' => [
                'file' => 'templates/rt_callisto/blueprints/styles/extension.yaml',
                'modified' => 1463470148
            ],
            'styles/feature' => [
                'file' => 'templates/rt_callisto/blueprints/styles/feature.yaml',
                'modified' => 1463470148
            ],
            'styles/font' => [
                'file' => 'templates/rt_callisto/blueprints/styles/font.yaml',
                'modified' => 1463470148
            ],
            'styles/footer' => [
                'file' => 'templates/rt_callisto/blueprints/styles/footer.yaml',
                'modified' => 1463470148
            ],
            'styles/header' => [
                'file' => 'templates/rt_callisto/blueprints/styles/header.yaml',
                'modified' => 1463470148
            ],
            'styles/main' => [
                'file' => 'templates/rt_callisto/blueprints/styles/main.yaml',
                'modified' => 1463470148
            ],
            'styles/menu' => [
                'file' => 'templates/rt_callisto/blueprints/styles/menu.yaml',
                'modified' => 1463470148
            ],
            'styles/navigation' => [
                'file' => 'templates/rt_callisto/blueprints/styles/navigation.yaml',
                'modified' => 1463470148
            ],
            'styles/offcanvas' => [
                'file' => 'templates/rt_callisto/blueprints/styles/offcanvas.yaml',
                'modified' => 1463470148
            ],
            'styles/showcase' => [
                'file' => 'templates/rt_callisto/blueprints/styles/showcase.yaml',
                'modified' => 1463470148
            ]
        ],
        'media/gantry5/engines/nucleus/blueprints' => [
            'page/assets' => [
                'file' => 'media/gantry5/engines/nucleus/blueprints/page/assets.yaml',
                'modified' => 1489567501
            ],
            'page/body' => [
                'file' => 'media/gantry5/engines/nucleus/blueprints/page/body.yaml',
                'modified' => 1489567501
            ],
            'page/head' => [
                'file' => 'media/gantry5/engines/nucleus/blueprints/page/head.yaml',
                'modified' => 1489567501
            ]
        ]
    ],
    'data' => [
        'items' => [
            'particles.contact' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles' => [
                'type' => '_parent',
                'name' => 'particles',
                'form_field' => false
            ],
            'particles.contact.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable icon menu particles.',
                'default' => true,
                'name' => 'particles.contact.enabled'
            ],
            'particles.contact.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.contact.class'
            ],
            'particles.contact.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.contact.title'
            ],
            'particles.contact.contactitems' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Contact Items',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'label',
                'ajax' => true,
                'name' => 'particles.contact.contactitems'
            ],
            'particles.contact.contactitems.*' => [
                'type' => '_parent',
                'name' => 'particles.contact.contactitems.*',
                'form_field' => false
            ],
            'particles.contact.contactitems.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'name' => 'particles.contact.contactitems.*.icon'
            ],
            'particles.contact.contactitems.*.label' => [
                'type' => 'input.text',
                'label' => 'Label',
                'name' => 'particles.contact.contactitems.*.label'
            ],
            'particles.contact.contactitems.*.text' => [
                'type' => 'input.text',
                'label' => 'Value',
                'name' => 'particles.contact.contactitems.*.text'
            ],
            'particles.contentlist' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.contentlist.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable icon menu particles.',
                'default' => true,
                'name' => 'particles.contentlist.enabled'
            ],
            'particles.contentlist.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.contentlist.class'
            ],
            'particles.contentlist.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.contentlist.title'
            ],
            'particles.contentlist.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select desired image.',
                'name' => 'particles.contentlist.image'
            ],
            'particles.contentlist.imagetag' => [
                'type' => 'input.text',
                'label' => 'Image Tag',
                'description' => 'Customize the image tag.',
                'name' => 'particles.contentlist.imagetag'
            ],
            'particles.contentlist.headline' => [
                'type' => 'input.text',
                'label' => 'Headline',
                'description' => 'Customize the headline.',
                'name' => 'particles.contentlist.headline'
            ],
            'particles.contentlist.subtitle' => [
                'type' => 'input.text',
                'label' => 'Subtitle',
                'description' => 'Customize the subtitle.',
                'name' => 'particles.contentlist.subtitle'
            ],
            'particles.contentlist.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.contentlist.desc'
            ],
            'particles.contentlist.readmore' => [
                'type' => 'input.text',
                'label' => 'Readmore Text',
                'description' => 'Specify the readmore text.',
                'name' => 'particles.contentlist.readmore'
            ],
            'particles.contentlist.link' => [
                'type' => 'input.text',
                'label' => 'Readmore Link',
                'description' => 'Specify the readmore link address.',
                'name' => 'particles.contentlist.link'
            ],
            'particles.contentlist.cols' => [
                'type' => 'select.select',
                'label' => 'Grid Column',
                'description' => 'Select the grid column amount for the list items',
                'default' => 'g-listgrid-4cols',
                'options' => [
                    'g-listgrid-2cols' => '2 Columns',
                    'g-listgrid-3cols' => '3 Columns',
                    'g-listgrid-4cols' => '4 Columns',
                    'g-listgrid-5cols' => '5 Columns'
                ],
                'name' => 'particles.contentlist.cols'
            ],
            'particles.contentlist.listgriditems' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Content Lists',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.contentlist.listgriditems'
            ],
            'particles.contentlist.listgriditems.*' => [
                'type' => '_parent',
                'name' => 'particles.contentlist.listgriditems.*',
                'form_field' => false
            ],
            'particles.contentlist.listgriditems.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'skip' => true,
                'name' => 'particles.contentlist.listgriditems.*.title'
            ],
            'particles.contentlist.listgriditems.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'name' => 'particles.contentlist.listgriditems.*.icon'
            ],
            'particles.contentlist.listgriditems.*.text' => [
                'type' => 'input.text',
                'label' => 'Text',
                'name' => 'particles.contentlist.listgriditems.*.text'
            ],
            'particles.contentlist.listgriditems.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.contentlist.listgriditems.*.link'
            ],
            'particles.iconmenu' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.iconmenu.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable icon menu particles.',
                'default' => true,
                'name' => 'particles.iconmenu.enabled'
            ],
            'particles.iconmenu.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.iconmenu.class'
            ],
            'particles.iconmenu.target' => [
                'type' => 'select.select',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_blank',
                'options' => [
                    '_parent' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.iconmenu.target'
            ],
            'particles.iconmenu.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Icon Menu Items',
                'description' => 'Create each icon menu item to display.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.iconmenu.items'
            ],
            'particles.iconmenu.items.*' => [
                'type' => '_parent',
                'name' => 'particles.iconmenu.items.*',
                'form_field' => false
            ],
            'particles.iconmenu.items.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.iconmenu.items.*.name'
            ],
            'particles.iconmenu.items.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'name' => 'particles.iconmenu.items.*.icon'
            ],
            'particles.iconmenu.items.*.text' => [
                'type' => 'input.text',
                'label' => 'Text',
                'name' => 'particles.iconmenu.items.*.text'
            ],
            'particles.iconmenu.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.iconmenu.items.*.link'
            ],
            'particles.imagegrid' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.imagegrid.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable icon menu particles.',
                'default' => true,
                'name' => 'particles.imagegrid.enabled'
            ],
            'particles.imagegrid.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.imagegrid.class'
            ],
            'particles.imagegrid.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.imagegrid.title'
            ],
            'particles.imagegrid.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.imagegrid.desc'
            ],
            'particles.imagegrid.cols' => [
                'type' => 'select.select',
                'label' => 'Grid Column',
                'description' => 'Select the grid column amount',
                'default' => 'g-imagegrid-2cols',
                'options' => [
                    'g-imagegrid-2cols' => '2 Columns',
                    'g-imagegrid-3cols' => '3 Columns',
                    'g-imagegrid-4cols' => '4 Columns',
                    'g-imagegrid-5cols' => '5 Columns'
                ],
                'name' => 'particles.imagegrid.cols'
            ],
            'particles.imagegrid.album' => [
                'type' => 'input.text',
                'label' => 'Album Name',
                'description' => 'Customize the album name.',
                'name' => 'particles.imagegrid.album'
            ],
            'particles.imagegrid.imagegriditems' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Images',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.imagegrid.imagegriditems'
            ],
            'particles.imagegrid.imagegriditems.*' => [
                'type' => '_parent',
                'name' => 'particles.imagegrid.imagegriditems.*',
                'form_field' => false
            ],
            'particles.imagegrid.imagegriditems.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'skip' => true,
                'name' => 'particles.imagegrid.imagegriditems.*.title'
            ],
            'particles.imagegrid.imagegriditems.*.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Promo Image',
                'description' => 'Select desired promo image.',
                'name' => 'particles.imagegrid.imagegriditems.*.image'
            ],
            'particles.imagegrid.imagegriditems.*.caption' => [
                'type' => 'input.text',
                'label' => 'Caption',
                'description' => 'Customize the image caption.',
                'name' => 'particles.imagegrid.imagegriditems.*.caption'
            ],
            'particles.infolist' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.infolist.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the particle.',
                'default' => true,
                'name' => 'particles.infolist.enabled'
            ],
            'particles.infolist.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.infolist.class'
            ],
            'particles.infolist.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.infolist.title'
            ],
            'particles.infolist.infolists' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Info Lists',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.infolist.infolists'
            ],
            'particles.infolist.infolists.*' => [
                'type' => '_parent',
                'name' => 'particles.infolist.infolists.*',
                'form_field' => false
            ],
            'particles.infolist.infolists.*.title' => [
                'type' => 'input.text',
                'label' => 'Item Title',
                'description' => 'Customize the item title text.',
                'name' => 'particles.infolist.infolists.*.title'
            ],
            'particles.infolist.infolists.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'description' => 'Specify the link address.',
                'name' => 'particles.infolist.infolists.*.link'
            ],
            'particles.infolist.infolists.*.description' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.infolist.infolists.*.description'
            ],
            'particles.newsletter' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.newsletter.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable icon menu particles.',
                'default' => true,
                'name' => 'particles.newsletter.enabled'
            ],
            'particles.newsletter.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.newsletter.class'
            ],
            'particles.newsletter.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.newsletter.title'
            ],
            'particles.newsletter.headtext' => [
                'type' => 'textarea.textarea',
                'label' => 'Heading Text',
                'description' => 'Customize the heading text.',
                'name' => 'particles.newsletter.headtext'
            ],
            'particles.newsletter.inputboxtext' => [
                'type' => 'input.text',
                'label' => 'InputBox Text',
                'description' => 'Customize the InputBox text.',
                'name' => 'particles.newsletter.inputboxtext'
            ],
            'particles.newsletter.buttontext' => [
                'type' => 'input.text',
                'label' => 'Button Text',
                'description' => 'Customize the Button text.',
                'name' => 'particles.newsletter.buttontext'
            ],
            'particles.newsletter.uri' => [
                'type' => 'input.text',
                'label' => 'Feedburner URI',
                'description' => 'Please put your Feedburner Email Subscriptions URI here.',
                'name' => 'particles.newsletter.uri'
            ],
            'particles.newsletter.buttonclass' => [
                'type' => 'input.selectize',
                'label' => 'Button Classes',
                'description' => 'CSS class name for the join button.',
                'name' => 'particles.newsletter.buttonclass'
            ],
            'particles.promoimage' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.promoimage.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable icon menu particles.',
                'default' => true,
                'name' => 'particles.promoimage.enabled'
            ],
            'particles.promoimage.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.promoimage.class'
            ],
            'particles.promoimage.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.promoimage.title'
            ],
            'particles.promoimage.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Promo Image',
                'description' => 'Select desired promo image.',
                'name' => 'particles.promoimage.image'
            ],
            'particles.promoimage.promoimagetitle' => [
                'type' => 'input.text',
                'label' => 'Promo Image Title',
                'description' => 'Customize the promo image title text.',
                'name' => 'particles.promoimage.promoimagetitle'
            ],
            'particles.promoimage.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.promoimage.desc'
            ],
            'particles.promoimage.iconbutton' => [
                'type' => 'input.icon',
                'label' => 'Icon Button',
                'name' => 'particles.promoimage.iconbutton'
            ],
            'particles.promoimage.promoimageicons' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Icons',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.promoimage.promoimageicons'
            ],
            'particles.promoimage.promoimageicons.*' => [
                'type' => '_parent',
                'name' => 'particles.promoimage.promoimageicons.*',
                'form_field' => false
            ],
            'particles.promoimage.promoimageicons.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'skip' => true,
                'name' => 'particles.promoimage.promoimageicons.*.title'
            ],
            'particles.promoimage.promoimageicons.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'name' => 'particles.promoimage.promoimageicons.*.icon'
            ],
            'particles.promoimage.promoimageicons.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.promoimage.promoimageicons.*.link'
            ],
            'particles.simplecounter' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.simplecounter.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the particle.',
                'default' => true,
                'name' => 'particles.simplecounter.enabled'
            ],
            'particles.simplecounter.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.simplecounter.class'
            ],
            'particles.simplecounter.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.simplecounter.title'
            ],
            'particles.simplecounter.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.simplecounter.desc'
            ],
            'particles.simplecounter.date' => [
                'type' => 'input.text',
                'label' => 'Date',
                'description' => 'Set the date.',
                'name' => 'particles.simplecounter.date'
            ],
            'particles.simplecounter.month' => [
                'type' => 'select.selectize',
                'label' => 'Month',
                'description' => 'Select the month',
                'default' => 0,
                'options' => [
                    0 => 'January',
                    1 => 'February',
                    2 => 'March',
                    3 => 'April',
                    4 => 'May',
                    5 => 'June',
                    6 => 'July',
                    7 => 'August',
                    8 => 'September',
                    9 => 'October',
                    10 => 'November',
                    11 => 'December'
                ],
                'name' => 'particles.simplecounter.month'
            ],
            'particles.simplecounter.year' => [
                'type' => 'input.text',
                'label' => 'Year',
                'description' => 'Set the year.',
                'name' => 'particles.simplecounter.year'
            ],
            'particles.simplecounter.daytext' => [
                'type' => 'input.text',
                'label' => 'Day Text',
                'description' => 'Set the text for Day.',
                'name' => 'particles.simplecounter.daytext'
            ],
            'particles.simplecounter.daystext' => [
                'type' => 'input.text',
                'label' => 'Days Text',
                'description' => 'Set the text for Days.',
                'name' => 'particles.simplecounter.daystext'
            ],
            'particles.simplecounter.hourtext' => [
                'type' => 'input.text',
                'label' => 'Hour Text',
                'description' => 'Set the text for Hour.',
                'name' => 'particles.simplecounter.hourtext'
            ],
            'particles.simplecounter.hourstext' => [
                'type' => 'input.text',
                'label' => 'Hours Text',
                'description' => 'Set the text for Hours.',
                'name' => 'particles.simplecounter.hourstext'
            ],
            'particles.simplecounter.minutetext' => [
                'type' => 'input.text',
                'label' => 'Minute Text',
                'description' => 'Set the text for Minute.',
                'name' => 'particles.simplecounter.minutetext'
            ],
            'particles.simplecounter.minutestext' => [
                'type' => 'input.text',
                'label' => 'Minutes Text',
                'description' => 'Set the text for Minutes.',
                'name' => 'particles.simplecounter.minutestext'
            ],
            'particles.simplecounter.secondtext' => [
                'type' => 'input.text',
                'label' => 'Second Text',
                'description' => 'Set the text for Second.',
                'name' => 'particles.simplecounter.secondtext'
            ],
            'particles.simplecounter.secondstext' => [
                'type' => 'input.text',
                'label' => 'Seconds Text',
                'description' => 'Set the text for Seconds.',
                'name' => 'particles.simplecounter.secondstext'
            ],
            'particles.analytics' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.analytics.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable analytic particles.',
                'default' => true,
                'name' => 'particles.analytics.enabled'
            ],
            'particles.analytics.ua' => [
                'type' => '_parent',
                'name' => 'particles.analytics.ua',
                'form_field' => false
            ],
            'particles.analytics.ua.code' => [
                'type' => 'input.text',
                'description' => 'Enter the Google UA tracking code for analytics (UA-XXXXXXXX-X)',
                'label' => 'UA Code',
                'name' => 'particles.analytics.ua.code'
            ],
            'particles.analytics.ua.anonym' => [
                'type' => 'input.checkbox',
                'description' => 'Send only Anonymous IP Addresses (mandatory in Europe)',
                'label' => 'Anonym Statistics',
                'default' => false,
                'name' => 'particles.analytics.ua.anonym'
            ],
            'particles.analytics.ua.ssl' => [
                'type' => 'input.checkbox',
                'description' => 'Send all data using SSL, even from insecure (HTTP) pages.',
                'label' => 'Force SSL use',
                'default' => false,
                'name' => 'particles.analytics.ua.ssl'
            ],
            'particles.analytics.ua.debug' => [
                'type' => 'input.checkbox',
                'description' => 'Enable the debug version of analytics.js - DON\'T USE ON PRODUCTION!',
                'label' => 'Use Debug Mode',
                'default' => false,
                'name' => 'particles.analytics.ua.debug'
            ],
            'particles.assets' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.assets.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable CSS/JS particles.',
                'default' => true,
                'name' => 'particles.assets.enabled'
            ],
            'particles.assets.css' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'CSS',
                'description' => 'Add remove or modify custom CSS assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.assets.css'
            ],
            'particles.assets.css.*' => [
                'type' => '_parent',
                'name' => 'particles.assets.css.*',
                'form_field' => false
            ],
            'particles.assets.css.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.assets.css.*.name'
            ],
            'particles.assets.css.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'filter' => '\\.(css|less|scss|sass)$',
                'root' => 'gantry-assets://',
                'name' => 'particles.assets.css.*.location'
            ],
            'particles.assets.css.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline CSS',
                'description' => 'Adds inline CSS for quick snippets.',
                'name' => 'particles.assets.css.*.inline'
            ],
            'particles.assets.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag attributes',
                'exclude' => [
                    0 => 'src',
                    1 => 'type'
                ],
                'name' => 'particles.assets.extra',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value'
            ],
            'particles.assets.priority' => [
                'type' => 'input.number',
                'label' => 'Load Priority',
                'description' => 'Sets the load priority of the asset in the page. Value can be between 10 (first) and -10 (last). Default value is 0.',
                'default' => 0,
                'min' => -10,
                'max' => 10,
                'name' => 'particles.assets.priority'
            ],
            'particles.assets.javascript' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Javascript',
                'description' => 'Add remove or modify custom Javascript assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.assets.javascript'
            ],
            'particles.assets.javascript.*' => [
                'type' => '_parent',
                'name' => 'particles.assets.javascript.*',
                'form_field' => false
            ],
            'particles.assets.javascript.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.assets.javascript.*.name'
            ],
            'particles.assets.javascript.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'filter' => '\\.(jsx?|coffee)$',
                'root' => 'gantry-assets://',
                'name' => 'particles.assets.javascript.*.location'
            ],
            'particles.assets.javascript.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline JavaScript',
                'description' => 'Adds inline JavaScript for quick snippets.',
                'name' => 'particles.assets.javascript.*.inline'
            ],
            'particles.assets.in_footer' => [
                'type' => 'input.checkbox',
                'label' => 'Before </body>',
                'description' => 'Whether you want the script to load at the end of the body tag or inside head',
                'default' => false,
                'name' => 'particles.assets.in_footer'
            ],
            'particles.branding' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.branding.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable to the particles.',
                'default' => true,
                'name' => 'particles.branding.enabled'
            ],
            'particles.branding.content' => [
                'type' => 'textarea.textarea',
                'label' => 'Content',
                'description' => 'Create or modify custom branding content.',
                'default' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'name' => 'particles.branding.content'
            ],
            'particles.branding.css' => [
                'type' => '_parent',
                'name' => 'particles.branding.css',
                'form_field' => false
            ],
            'particles.branding.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'branding',
                'name' => 'particles.branding.css.class'
            ],
            'particles.content' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.content.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable page content.',
                'default' => true,
                'name' => 'particles.content.enabled'
            ],
            'particles.contentarray' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.contentarray.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable Joomla Articles particles.',
                'default' => true,
                'name' => 'particles.contentarray.enabled'
            ],
            'particles.contentarray.article' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article',
                'form_field' => false
            ],
            'particles.contentarray.article.filter' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.filter',
                'form_field' => false
            ],
            'particles.contentarray.article.filter.categories' => [
                'type' => 'joomla.categories',
                'label' => 'Categories',
                'description' => 'Select the categories the articles should be taken from.',
                'overridable' => false,
                'name' => 'particles.contentarray.article.filter.categories'
            ],
            'particles.contentarray.article.filter.articles' => [
                'type' => 'input.text',
                'label' => 'Articles',
                'description' => 'Enter the Joomla articles that should be shown. It should be a list of article IDs separated with a comma (i.e. 1,2,3,4,5).',
                'overridable' => false,
                'name' => 'particles.contentarray.article.filter.articles'
            ],
            'particles.contentarray.article.filter.featured' => [
                'type' => 'select.select',
                'label' => 'Featured Articles',
                'description' => 'Select how Featured articles should be filtered.',
                'default' => '',
                'options' => [
                    'include' => 'Include Featured',
                    'exclude' => 'Exclude Featured',
                    'only' => 'Only Featured'
                ],
                'overridable' => false,
                'name' => 'particles.contentarray.article.filter.featured'
            ],
            'particles.contentarray.article.limit' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.limit',
                'form_field' => false
            ],
            'particles.contentarray.article.limit.total' => [
                'type' => 'input.text',
                'label' => 'Number of Articles',
                'description' => 'Enter the maximum number of articles to display.',
                'default' => 2,
                'pattern' => '\\d{1,2}',
                'overridable' => false,
                'name' => 'particles.contentarray.article.limit.total'
            ],
            'particles.contentarray.article.limit.columns' => [
                'type' => 'select.select',
                'label' => 'Number of columns',
                'description' => 'Select the number of columns that you want articles to appear in.',
                'default' => 2,
                'options' => [
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => 5,
                    6 => 6
                ],
                'overridable' => false,
                'name' => 'particles.contentarray.article.limit.columns'
            ],
            'particles.contentarray.article.limit.start' => [
                'type' => 'input.text',
                'label' => 'Start From',
                'description' => 'Enter offset specifying the first article to return. The default is \'0\' (the first article).',
                'default' => 0,
                'pattern' => '\\d{1,2}',
                'overridable' => false,
                'name' => 'particles.contentarray.article.limit.start'
            ],
            'particles.contentarray.article.sort' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.sort',
                'form_field' => false
            ],
            'particles.contentarray.article.sort.orderby' => [
                'type' => 'select.select',
                'label' => 'Order By',
                'description' => 'Select how the articles should be ordered by.',
                'default' => 'publish_up',
                'options' => [
                    'publish_up' => 'Published Date',
                    'created' => 'Created Date',
                    'modified' => 'Last Modified Date',
                    'title' => 'Title',
                    'ordering' => 'Ordering',
                    'hits' => 'Hits',
                    'id' => 'ID',
                    'alias' => 'Alias'
                ],
                'overridable' => false,
                'name' => 'particles.contentarray.article.sort.orderby'
            ],
            'particles.contentarray.article.sort.ordering' => [
                'type' => 'select.select',
                'label' => 'Ordering Direction',
                'description' => 'Select the direction the articles should be ordered by.',
                'default' => 'ASC',
                'options' => [
                    'ASC' => 'Ascending',
                    'DESC' => 'Descending'
                ],
                'overridable' => false,
                'name' => 'particles.contentarray.article.sort.ordering'
            ],
            'particles.contentarray._tab_articles' => [
                'label' => 'Articles',
                'overridable' => false,
                'name' => 'particles.contentarray._tab_articles'
            ],
            'particles.contentarray.article.display' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display',
                'form_field' => false
            ],
            'particles.contentarray.article.display.image' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.image',
                'form_field' => false
            ],
            'particles.contentarray.article.display.image.enabled' => [
                'type' => 'select.select',
                'label' => 'Image',
                'description' => 'Select if and what image of the article should be shown.',
                'default' => 'intro',
                'options' => [
                    'intro' => 'Intro',
                    'full' => 'Full',
                    '' => 'None'
                ],
                'name' => 'particles.contentarray.article.display.image.enabled'
            ],
            'particles.contentarray.article.display.text' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.text',
                'form_field' => false
            ],
            'particles.contentarray.article.display.text.type' => [
                'type' => 'select.select',
                'label' => 'Article Text',
                'description' => 'Select if and how the article text should be shown.',
                'default' => 'intro',
                'options' => [
                    'intro' => 'Introduction',
                    'full' => 'Full Article',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.text.type'
            ],
            'particles.contentarray.article.display.text.limit' => [
                'type' => 'input.text',
                'label' => 'Text Limit',
                'description' => 'Type in the number of characters the article text should be limited to.',
                'default' => '',
                'pattern' => '\\d+',
                'name' => 'particles.contentarray.article.display.text.limit'
            ],
            'particles.contentarray.article.display.text.formatting' => [
                'type' => 'select.select',
                'label' => 'Text Formatting',
                'description' => 'Select the formatting you want to use to display the article text.',
                'default' => 'text',
                'options' => [
                    'text' => 'Plain Text',
                    'html' => 'HTML'
                ],
                'name' => 'particles.contentarray.article.display.text.formatting'
            ],
            'particles.contentarray.article.display.title' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.title',
                'form_field' => false
            ],
            'particles.contentarray.article.display.title.enabled' => [
                'type' => 'select.select',
                'label' => 'Title',
                'description' => 'Select if the article title should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.title.enabled'
            ],
            'particles.contentarray.article.display.title.limit' => [
                'type' => 'input.text',
                'label' => 'Title Limit',
                'description' => 'Enter the maximum number of characters the article title should be limited to.',
                'pattern' => '\\d+(\\.\\d+){0,1}',
                'name' => 'particles.contentarray.article.display.title.limit'
            ],
            'particles.contentarray.article.display.date' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.date',
                'form_field' => false
            ],
            'particles.contentarray.article.display.date.enabled' => [
                'type' => 'select.select',
                'label' => 'Date',
                'description' => 'Select if the article date should be shown.',
                'default' => 'published',
                'options' => [
                    'created' => 'Show Created Date',
                    'published' => 'Show Published Date',
                    'modified' => 'Show Modified Date',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.date.enabled'
            ],
            'particles.contentarray.article.display.date.format' => [
                'type' => 'select.date',
                'label' => 'Date Format',
                'description' => 'Select preferred date format. Leave empty not to display a date.',
                'default' => 'l, F d, Y',
                'selectize' => [
                    'allowEmptyOption' => true
                ],
                'options' => [
                    'l, F d, Y' => 'Date1',
                    'l, d F' => 'Date2',
                    'D, d F' => 'Date3',
                    'F d' => 'Date4',
                    'd F' => 'Date5',
                    'd M' => 'Date6',
                    'D, M d, Y' => 'Date7',
                    'D, M d, y' => 'Date8',
                    'l' => 'Date9',
                    'l j F Y' => 'Date10',
                    'j F Y' => 'Date11'
                ],
                'name' => 'particles.contentarray.article.display.date.format'
            ],
            'particles.contentarray._tab_display' => [
                'label' => 'Display',
                'name' => 'particles.contentarray._tab_display'
            ],
            'particles.contentarray.article.display.read_more' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.read_more',
                'form_field' => false
            ],
            'particles.contentarray.article.display.read_more.enabled' => [
                'type' => 'select.select',
                'label' => 'Read More',
                'description' => 'Select if the article \'Read More\' button should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.read_more.enabled'
            ],
            'particles.contentarray.article.display.read_more.label' => [
                'type' => 'input.text',
                'label' => 'Read More Label',
                'description' => 'Type in the label for the \'Read More\' button.',
                'name' => 'particles.contentarray.article.display.read_more.label'
            ],
            'particles.contentarray.article.display.read_more.css' => [
                'type' => 'input.selectize',
                'label' => 'Button CSS Classes',
                'description' => 'CSS class name for the \'Read More\' button.',
                'name' => 'particles.contentarray.article.display.read_more.css'
            ],
            'particles.contentarray._tab_readmore' => [
                'label' => 'Read More',
                'name' => 'particles.contentarray._tab_readmore'
            ],
            'particles.contentarray.article.display.author' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.author',
                'form_field' => false
            ],
            'particles.contentarray.article.display.author.enabled' => [
                'type' => 'select.select',
                'label' => 'Author',
                'description' => 'Select if the article author should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.author.enabled'
            ],
            'particles.contentarray.article.display.category' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.category',
                'form_field' => false
            ],
            'particles.contentarray.article.display.category.enabled' => [
                'type' => 'select.select',
                'label' => 'Category',
                'description' => 'Select if and how the article category should be shown.',
                'default' => 'link',
                'options' => [
                    'show' => 'Show',
                    'link' => 'Show with Link',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.category.enabled'
            ],
            'particles.contentarray.article.display.hits' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.hits',
                'form_field' => false
            ],
            'particles.contentarray.article.display.hits.enabled' => [
                'type' => 'select.select',
                'label' => 'Hits',
                'description' => 'Select if the article hits should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.hits.enabled'
            ],
            'particles.contentarray._tab_extras' => [
                'label' => 'Extras',
                'name' => 'particles.contentarray._tab_extras'
            ],
            'particles.contentarray.css' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.css',
                'form_field' => false
            ],
            'particles.contentarray.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.contentarray.css.class'
            ],
            'particles.contentarray.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag Attributes',
                'description' => 'Extra Tag attributes.',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value',
                'exclude' => [
                    0 => 'id',
                    1 => 'class'
                ],
                'name' => 'particles.contentarray.extra'
            ],
            'particles.copyright' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.copyright.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the particle.',
                'default' => true,
                'name' => 'particles.copyright.enabled'
            ],
            'particles.copyright.date' => [
                'type' => '_parent',
                'name' => 'particles.copyright.date',
                'form_field' => false
            ],
            'particles.copyright.date.start' => [
                'type' => 'input.text',
                'label' => 'Start Year',
                'description' => 'Select the copyright start year.',
                'default' => 'now',
                'name' => 'particles.copyright.date.start'
            ],
            'particles.copyright.date.end' => [
                'type' => 'input.text',
                'label' => 'End Year',
                'description' => 'Select the copyright end year.',
                'default' => 'now',
                'name' => 'particles.copyright.date.end'
            ],
            'particles.copyright.owner' => [
                'type' => 'input.text',
                'label' => 'Copyright owner',
                'description' => 'Add copyright owner name.',
                'name' => 'particles.copyright.owner'
            ],
            'particles.custom' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.custom.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the particle.',
                'default' => true,
                'name' => 'particles.custom.enabled'
            ],
            'particles.custom.html' => [
                'type' => 'textarea.textarea',
                'label' => 'Custom HTML',
                'description' => 'Enter custom HTML into here.',
                'overridable' => false,
                'name' => 'particles.custom.html'
            ],
            'particles.custom.twig' => [
                'type' => 'input.checkbox',
                'label' => 'Process Twig',
                'description' => 'Enable Twig template processing in the content. Twig will be processed before shortcodes.',
                'default' => '0',
                'name' => 'particles.custom.twig'
            ],
            'particles.custom.filter' => [
                'type' => 'input.checkbox',
                'label' => 'Process shortcodes',
                'description' => 'Enable shortcode processing / filtering in the content.',
                'default' => '0',
                'name' => 'particles.custom.filter'
            ],
            'particles.date' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.date.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable date particles.',
                'default' => true,
                'name' => 'particles.date.enabled'
            ],
            'particles.date.css' => [
                'type' => '_parent',
                'name' => 'particles.date.css',
                'form_field' => false
            ],
            'particles.date.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'date',
                'name' => 'particles.date.css.class'
            ],
            'particles.date.date' => [
                'type' => '_parent',
                'name' => 'particles.date.date',
                'form_field' => false
            ],
            'particles.date.date.formats' => [
                'type' => 'select.date',
                'label' => 'Format',
                'description' => 'Select preferred date format.',
                'default' => 'l, F d, Y',
                'selectize' => [
                    'allowEmptyOption' => true
                ],
                'options' => [
                    'l, F d, Y' => 'Date1',
                    'l, d F' => 'Date2',
                    'D, d F' => 'Date3',
                    'F d' => 'Date4',
                    'd F' => 'Date5',
                    'd M' => 'Date6',
                    'D, M d, Y' => 'Date7',
                    'D, M d, y' => 'Date8',
                    'l' => 'Date9',
                    'l j F Y' => 'Date10',
                    'j F Y' => 'Date11'
                ],
                'name' => 'particles.date.date.formats'
            ],
            'particles.frameworks' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.frameworks.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable Frameworks atom.',
                'default' => true,
                'name' => 'particles.frameworks.enabled'
            ],
            'particles.frameworks.jquery' => [
                'type' => '_parent',
                'name' => 'particles.frameworks.jquery',
                'form_field' => false
            ],
            'particles.frameworks.jquery.enabled' => [
                'type' => 'enable.enable',
                'label' => 'Framework',
                'default' => 0,
                'name' => 'particles.frameworks.jquery.enabled'
            ],
            'particles.frameworks.jquery.ui_core' => [
                'type' => 'enable.enable',
                'label' => 'UI Core',
                'default' => 0,
                'name' => 'particles.frameworks.jquery.ui_core'
            ],
            'particles.frameworks.jquery.ui_sortable' => [
                'type' => 'enable.enable',
                'label' => 'UI Sortable',
                'default' => 0,
                'name' => 'particles.frameworks.jquery.ui_sortable'
            ],
            'particles.frameworks.bootstrap' => [
                'type' => '_parent',
                'name' => 'particles.frameworks.bootstrap',
                'form_field' => false
            ],
            'particles.frameworks.bootstrap.enabled' => [
                'type' => 'enable.enable',
                'label' => 'Framework',
                'default' => 0,
                'name' => 'particles.frameworks.bootstrap.enabled'
            ],
            'particles.frameworks.mootools' => [
                'type' => '_parent',
                'name' => 'particles.frameworks.mootools',
                'form_field' => false
            ],
            'particles.frameworks.mootools.enabled' => [
                'type' => 'enable.enable',
                'label' => 'Framework',
                'default' => 0,
                'name' => 'particles.frameworks.mootools.enabled'
            ],
            'particles.frameworks.mootools.more' => [
                'type' => 'enable.enable',
                'label' => 'Mootools More',
                'default' => 0,
                'name' => 'particles.frameworks.mootools.more'
            ],
            'particles.lightcase' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.lightcase.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable Lightcase atom.',
                'default' => true,
                'name' => 'particles.lightcase.enabled'
            ],
            'particles.logo' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.logo.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable logo particles.',
                'default' => true,
                'name' => 'particles.logo.enabled'
            ],
            'particles.logo.url' => [
                'type' => 'input.text',
                'label' => 'Url',
                'description' => 'Url for the image. Leave empty to go to home page.',
                'name' => 'particles.logo.url'
            ],
            'particles.logo.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select desired logo image.',
                'name' => 'particles.logo.image'
            ],
            'particles.logo.link' => [
                'type' => 'input.checkbox',
                'label' => 'Link',
                'description' => 'Renders Logo/Image with a link.',
                'default' => true,
                'name' => 'particles.logo.link'
            ],
            'particles.logo.svg' => [
                'type' => 'textarea.textarea',
                'label' => 'SVG Code',
                'description' => 'Your SVG code that will be added inline to the site.',
                'name' => 'particles.logo.svg'
            ],
            'particles.logo.text' => [
                'type' => 'input.text',
                'label' => 'Text',
                'description' => 'Input logo description text.',
                'name' => 'particles.logo.text'
            ],
            'particles.logo.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'Set a specific CSS class for custom styling.',
                'name' => 'particles.logo.class'
            ],
            'particles.menu' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.menu.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the menu particle.',
                'default' => true,
                'name' => 'particles.menu.enabled'
            ],
            'particles.menu.menu' => [
                'type' => 'menu.list',
                'label' => 'Menu',
                'description' => 'Select menu to be used with the particle.',
                'default' => '',
                'selectize' => [
                    'allowEmptyOption' => true
                ],
                'options' => [
                    '' => 'Use Default Menu',
                    '-active-' => 'Use Active Menu'
                ],
                'name' => 'particles.menu.menu'
            ],
            'particles.menu.base' => [
                'type' => 'menu.item',
                'label' => 'Base Item',
                'description' => 'Select a menu item to always be used as the base for the menu display.',
                'default' => '/',
                'options' => [
                    '/' => 'Active'
                ],
                'name' => 'particles.menu.base'
            ],
            'particles.menu.startLevel' => [
                'type' => 'input.text',
                'label' => 'Start Level',
                'description' => 'Set the start level of the menu.',
                'default' => 1,
                'name' => 'particles.menu.startLevel'
            ],
            'particles.menu.maxLevels' => [
                'type' => 'input.text',
                'label' => 'Max Levels',
                'description' => 'Set the maximum number of menu levels to display.',
                'default' => 0,
                'name' => 'particles.menu.maxLevels'
            ],
            'particles.menu.renderTitles' => [
                'type' => 'input.checkbox',
                'label' => 'Render Titles',
                'description' => 'Renders the titles/tooltips of the Menu Items for accessibility.',
                'default' => 0,
                'name' => 'particles.menu.renderTitles'
            ],
            'particles.menu.hoverExpand' => [
                'type' => 'input.checkbox',
                'label' => 'Expand on Hover',
                'description' => 'Allows to enable / disable the ability to expand menu items by hover or click only',
                'default' => 1,
                'name' => 'particles.menu.hoverExpand'
            ],
            'particles.menu.mobileTarget' => [
                'type' => 'input.checkbox',
                'label' => 'Mobile Target',
                'description' => 'Check this field if you want this menu to become the target for Mobile Menu and to appear in Offcanvas',
                'default' => 0,
                'name' => 'particles.menu.mobileTarget'
            ],
            'particles.messages' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.messages.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable system messages.',
                'default' => true,
                'name' => 'particles.messages.enabled'
            ],
            'particles.mobile-menu' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.mobile-menu.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable spacer.',
                'default' => true,
                'name' => 'particles.mobile-menu.enabled'
            ],
            'particles.module' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.module.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable module positions.',
                'default' => true,
                'name' => 'particles.module.enabled'
            ],
            'particles.module.module_id' => [
                'type' => 'gantry.module',
                'label' => 'Module Id',
                'class' => 'g-urltemplate input-small',
                'picker_label' => 'Pick a Module',
                'description' => 'Enter module Id.',
                'pattern' => '\\d+',
                'overridable' => false,
                'name' => 'particles.module.module_id'
            ],
            'particles.module.chrome' => [
                'type' => 'input.text',
                'label' => 'Chrome',
                'description' => 'Module chrome.',
                'name' => 'particles.module.chrome'
            ],
            'particles.position' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.position.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable module positions.',
                'default' => true,
                'name' => 'particles.position.enabled'
            ],
            'particles.position.key' => [
                'type' => 'input.text',
                'label' => 'Key',
                'description' => 'Position name.',
                'pattern' => '[A-Za-z0-9-]+',
                'overridable' => false,
                'name' => 'particles.position.key'
            ],
            'particles.position.chrome' => [
                'type' => 'input.text',
                'label' => 'Chrome',
                'description' => 'Module chrome in this position.',
                'name' => 'particles.position.chrome'
            ],
            'particles.social' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.social.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable social particles.',
                'default' => true,
                'name' => 'particles.social.enabled'
            ],
            'particles.social.css' => [
                'type' => '_parent',
                'name' => 'particles.social.css',
                'form_field' => false
            ],
            'particles.social.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'social',
                'name' => 'particles.social.css.class'
            ],
            'particles.social.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.social.title'
            ],
            'particles.social.target' => [
                'type' => 'select.select',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_blank',
                'options' => [
                    '_parent' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.social.target'
            ],
            'particles.social.display' => [
                'type' => 'input.radios',
                'label' => 'Display',
                'description' => 'How to display the Social Icons',
                'default' => 'both',
                'options' => [
                    'icons_only' => 'Icons Only',
                    'text_only' => 'Text Only',
                    'both' => 'Both'
                ],
                'name' => 'particles.social.display'
            ],
            'particles.social.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Social Items',
                'description' => 'Create each social item to display.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.social.items'
            ],
            'particles.social.items.*' => [
                'type' => '_parent',
                'name' => 'particles.social.items.*',
                'form_field' => false
            ],
            'particles.social.items.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.social.items.*.name'
            ],
            'particles.social.items.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'name' => 'particles.social.items.*.icon'
            ],
            'particles.social.items.*.text' => [
                'type' => 'input.text',
                'label' => 'Text',
                'name' => 'particles.social.items.*.text'
            ],
            'particles.social.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.social.items.*.link'
            ],
            'particles.spacer' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.spacer.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable spacer.',
                'default' => true,
                'name' => 'particles.spacer.enabled'
            ],
            'particles.totop' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.totop.enabled' => [
                'type' => 'checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable to top particles.',
                'default' => true,
                'name' => 'particles.totop.enabled'
            ],
            'particles.totop.css' => [
                'type' => '_parent',
                'name' => 'particles.totop.css',
                'form_field' => false
            ],
            'particles.totop.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'totop',
                'name' => 'particles.totop.css.class'
            ],
            'particles.totop.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'description' => 'A Font Awesome icon to be displayed for the link.',
                'name' => 'particles.totop.icon'
            ],
            'particles.totop.content' => [
                'type' => 'input.text',
                'label' => 'Text',
                'description' => 'The text to be displayed for the link. HTML is allowed.',
                'name' => 'particles.totop.content'
            ],
            'styles.accent' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles' => [
                'type' => '_parent',
                'name' => 'styles',
                'form_field' => false
            ],
            'styles.accent.color-1' => [
                'type' => 'input.colorpicker',
                'label' => 'Accent Color 1',
                'default' => '#ecbc2e',
                'name' => 'styles.accent.color-1'
            ],
            'styles.accent.color-2' => [
                'type' => 'input.colorpicker',
                'label' => 'Accent Color 2',
                'default' => '#8b5717',
                'name' => 'styles.accent.color-2'
            ],
            'styles.base' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.base.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Base Background',
                'default' => '#1f1f1f',
                'name' => 'styles.base.background'
            ],
            'styles.base.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Base Text Color',
                'default' => '#888888',
                'name' => 'styles.base.text-color'
            ],
            'styles.base.favicon' => [
                'type' => 'input.imagepicker',
                'label' => 'Favicon',
                'filter' => '.(jpe?g|gif|png|svg|ico)$',
                'name' => 'styles.base.favicon'
            ],
            'styles.breakpoints' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.breakpoints.large-desktop-container' => [
                'type' => 'input.text',
                'label' => 'Large Desktop',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '75rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.large-desktop-container'
            ],
            'styles.breakpoints.desktop-container' => [
                'type' => 'input.text',
                'label' => 'Desktop',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '60rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.desktop-container'
            ],
            'styles.breakpoints.tablet-container' => [
                'type' => 'input.text',
                'label' => 'Tablet',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '48rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.tablet-container'
            ],
            'styles.breakpoints.large-mobile-container' => [
                'type' => 'input.text',
                'label' => 'Mobile',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '30rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.large-mobile-container'
            ],
            'styles.breakpoints.mobile-menu-breakpoint' => [
                'type' => 'input.text',
                'label' => 'Mobile Menu',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '48rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.mobile-menu-breakpoint'
            ],
            'styles.copyright' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.copyright.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#1a1a1a',
                'name' => 'styles.copyright.background'
            ],
            'styles.copyright.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#878787',
                'name' => 'styles.copyright.text-color'
            ],
            'styles.extension' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.extension.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#1a1a1a',
                'name' => 'styles.extension.background'
            ],
            'styles.extension.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#878787',
                'name' => 'styles.extension.text-color'
            ],
            'styles.feature' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.feature.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#1a1a1a',
                'name' => 'styles.feature.background'
            ],
            'styles.feature.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#878787',
                'name' => 'styles.feature.text-color'
            ],
            'styles.font' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.font.family-default' => [
                'type' => 'input.fonts',
                'label' => 'Body Font',
                'default' => 'sourcesanspro, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'name' => 'styles.font.family-default'
            ],
            'styles.font.family-title' => [
                'type' => 'input.fonts',
                'label' => 'Title Font',
                'default' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'name' => 'styles.font.family-title'
            ],
            'styles.font.family-promo' => [
                'type' => 'input.fonts',
                'label' => 'Promo Font',
                'default' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'name' => 'styles.font.family-promo'
            ],
            'styles.footer' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.footer.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#1f1f1f',
                'name' => 'styles.footer.background'
            ],
            'styles.footer.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#878787',
                'name' => 'styles.footer.text-color'
            ],
            'styles.header' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.header.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ecbc2e',
                'name' => 'styles.header.background'
            ],
            'styles.header.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#b08523',
                'name' => 'styles.header.text-color'
            ],
            'styles.main' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.main.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#1a1a1a',
                'name' => 'styles.main.background'
            ],
            'styles.main.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#878787',
                'name' => 'styles.main.text-color'
            ],
            'styles.menu' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.menu.col-width' => [
                'type' => 'input.text',
                'label' => 'Simple Dropdown Width',
                'description' => 'Specify the default width of menu dropdowns for simple mode in rem, em or px units. This width can be overridden on each individual menu item from the menu editor.',
                'default' => '180px',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|px)',
                'name' => 'styles.menu.col-width'
            ],
            'styles.menu.animation' => [
                'type' => 'select.selectize',
                'label' => 'Dropdown Animation',
                'description' => 'Select the dropdown animation.',
                'default' => 'g-fade',
                'options' => [
                    'g-no-animation' => 'No Animation',
                    'g-fade' => 'Fade',
                    'g-zoom' => 'Zoom',
                    'g-fade-in-up' => 'Fade In Up'
                ],
                'name' => 'styles.menu.animation'
            ],
            'styles.navigation' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.navigation.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#353535',
                'name' => 'styles.navigation.background'
            ],
            'styles.navigation.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#828282',
                'name' => 'styles.navigation.text-color'
            ],
            'styles.offcanvas' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.offcanvas.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#1b1b1f',
                'name' => 'styles.offcanvas.background'
            ],
            'styles.offcanvas.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#ffffff',
                'name' => 'styles.offcanvas.text-color'
            ],
            'styles.offcanvas.width' => [
                'type' => 'input.text',
                'label' => 'Panel Width',
                'description' => 'Set offcanvas size in rem, em, px, or percentage unit values',
                'default' => '17rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.offcanvas.width'
            ],
            'styles.offcanvas.toggle-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Toggle Color',
                'default' => '#ffffff',
                'name' => 'styles.offcanvas.toggle-color'
            ],
            'styles.offcanvas.toggle-visibility' => [
                'type' => 'select.selectize',
                'label' => 'Toggle Visibility',
                'description' => 'Choose the OffCanvas Toggle Visibility.',
                'default' => 1,
                'options' => [
                    1 => 'Mobile Menu',
                    2 => 'Always'
                ],
                'name' => 'styles.offcanvas.toggle-visibility'
            ],
            'styles.showcase' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.showcase.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#23262f',
                'name' => 'styles.showcase.background'
            ],
            'styles.showcase.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'text',
                'default' => '#ffffff',
                'name' => 'styles.showcase.text-color'
            ],
            'page.assets' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'page' => [
                'type' => '_parent',
                'name' => 'page',
                'form_field' => false
            ],
            'page.assets.favicon' => [
                'type' => 'input.imagepicker',
                'label' => 'Favicon',
                'filter' => '.(jpe?g|gif|png|svg|ico)$',
                'name' => 'page.assets.favicon'
            ],
            'page.assets.touchicon' => [
                'type' => 'input.imagepicker',
                'label' => 'Touch Icon',
                'description' => 'A PNG only image that will be used as icon for Touch Devices. Recommended 180x180 or 192x192.',
                'filter' => '.png$',
                'name' => 'page.assets.touchicon'
            ],
            'page.assets.css' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'CSS',
                'description' => 'Add remove or modify custom CSS assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'page.assets.css'
            ],
            'page.assets.css.*' => [
                'type' => '_parent',
                'name' => 'page.assets.css.*',
                'form_field' => false
            ],
            'page.assets.css.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'page.assets.css.*.name'
            ],
            'page.assets.css.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'filter' => '\\.(css|less|scss|sass)$',
                'root' => 'gantry-assets://',
                'name' => 'page.assets.css.*.location'
            ],
            'page.assets.css.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline CSS',
                'description' => 'Adds inline CSS for quick snippets.',
                'name' => 'page.assets.css.*.inline'
            ],
            'page.assets.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag attributes',
                'exclude' => [
                    0 => 'src',
                    1 => 'type'
                ],
                'name' => 'page.assets.extra',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value'
            ],
            'page.assets.priority' => [
                'type' => 'input.number',
                'label' => 'Load Priority',
                'description' => 'Sets the load priority of the asset in the page. Value can be between 10 (first) and -10 (last). Default value is 0.',
                'default' => 0,
                'min' => -10,
                'max' => 10,
                'name' => 'page.assets.priority'
            ],
            'page.assets.javascript' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Javascript',
                'description' => 'Add remove or modify custom Javascript assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'page.assets.javascript'
            ],
            'page.assets.javascript.*' => [
                'type' => '_parent',
                'name' => 'page.assets.javascript.*',
                'form_field' => false
            ],
            'page.assets.javascript.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'page.assets.javascript.*.name'
            ],
            'page.assets.javascript.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'filter' => '\\.(jsx?|coffee)$',
                'root' => 'gantry-assets://',
                'name' => 'page.assets.javascript.*.location'
            ],
            'page.assets.javascript.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline JavaScript',
                'description' => 'Adds inline JavaScript for quick snippets.',
                'name' => 'page.assets.javascript.*.inline'
            ],
            'page.assets.in_footer' => [
                'type' => 'input.checkbox',
                'label' => 'Before </body>',
                'description' => 'Whether you want the script to load at the end of the body tag or inside head',
                'default' => false,
                'name' => 'page.assets.in_footer'
            ],
            'page.body' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'page.body.attribs' => [
                'type' => '_parent',
                'name' => 'page.body.attribs',
                'form_field' => false
            ],
            'page.body.attribs.id' => [
                'type' => 'input.text',
                'label' => 'Body Id',
                'default' => NULL,
                'name' => 'page.body.attribs.id'
            ],
            'page.body.attribs.class' => [
                'type' => 'input.selectize',
                'label' => 'Body Classes',
                'default' => 'gantry',
                'name' => 'page.body.attribs.class'
            ],
            'page.body.attribs.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag Attributes',
                'description' => 'Extra Tag attributes.',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value',
                'exclude' => [
                    0 => 'id',
                    1 => 'class'
                ],
                'name' => 'page.body.attribs.extra'
            ],
            'page.body.layout' => [
                'type' => '_parent',
                'name' => 'page.body.layout',
                'form_field' => false
            ],
            'page.body.layout.sections' => [
                'type' => 'select.selectize',
                'label' => 'Sections Layout',
                'description' => 'Default layout container behavior for Sections',
                'default' => 0,
                'options' => [
                    0 => 'Fullwidth (Boxed Content)',
                    2 => 'Fullwidth (Flushed Content)',
                    1 => 'Boxed',
                    3 => 'Remove Container'
                ],
                'name' => 'page.body.layout.sections'
            ],
            'page.body.body_top' => [
                'type' => 'textarea.textarea',
                'label' => 'After <body>',
                'description' => 'Anything in this field will be appended right after the opening body tag',
                'name' => 'page.body.body_top'
            ],
            'page.body.body_bottom' => [
                'type' => 'textarea.textarea',
                'label' => 'Before </body>',
                'description' => 'Anything in this field will be appended right before the closing body tag',
                'name' => 'page.body.body_bottom'
            ],
            'page.head' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'page.head.meta' => [
                'type' => 'collection.keyvalue',
                'label' => 'Meta Tags',
                'description' => 'Meta Tags for extras such as Facebook and Twitter.',
                'key_placeholder' => 'og:title, og:site_name, twitter:site',
                'value_placeholder' => 'Value',
                'default' => NULL,
                'name' => 'page.head.meta'
            ],
            'page.head.head_bottom' => [
                'type' => 'textarea.textarea',
                'label' => 'Custom Content',
                'description' => 'Anything in this field will be appended to the head tag',
                'name' => 'page.head.head_bottom'
            ],
            'page.head.atoms' => [
                'type' => 'input.hidden',
                'override_target' => '#atoms .atoms-list + input[type="checkbox"]',
                'array' => true,
                'name' => 'page.head.atoms'
            ]
        ],
        'rules' => [
            
        ],
        'nested' => [
            'particles' => [
                'contact' => [
                    'enabled' => 'particles.contact.enabled',
                    'class' => 'particles.contact.class',
                    'title' => 'particles.contact.title',
                    'contactitems' => [
                        '*' => [
                            'icon' => 'particles.contact.contactitems.*.icon',
                            'label' => 'particles.contact.contactitems.*.label',
                            'text' => 'particles.contact.contactitems.*.text'
                        ]
                    ]
                ],
                'contentlist' => [
                    'enabled' => 'particles.contentlist.enabled',
                    'class' => 'particles.contentlist.class',
                    'title' => 'particles.contentlist.title',
                    'image' => 'particles.contentlist.image',
                    'imagetag' => 'particles.contentlist.imagetag',
                    'headline' => 'particles.contentlist.headline',
                    'subtitle' => 'particles.contentlist.subtitle',
                    'desc' => 'particles.contentlist.desc',
                    'readmore' => 'particles.contentlist.readmore',
                    'link' => 'particles.contentlist.link',
                    'cols' => 'particles.contentlist.cols',
                    'listgriditems' => [
                        '*' => [
                            'title' => 'particles.contentlist.listgriditems.*.title',
                            'icon' => 'particles.contentlist.listgriditems.*.icon',
                            'text' => 'particles.contentlist.listgriditems.*.text',
                            'link' => 'particles.contentlist.listgriditems.*.link'
                        ]
                    ]
                ],
                'iconmenu' => [
                    'enabled' => 'particles.iconmenu.enabled',
                    'class' => 'particles.iconmenu.class',
                    'target' => 'particles.iconmenu.target',
                    'items' => [
                        '*' => [
                            'name' => 'particles.iconmenu.items.*.name',
                            'icon' => 'particles.iconmenu.items.*.icon',
                            'text' => 'particles.iconmenu.items.*.text',
                            'link' => 'particles.iconmenu.items.*.link'
                        ]
                    ]
                ],
                'imagegrid' => [
                    'enabled' => 'particles.imagegrid.enabled',
                    'class' => 'particles.imagegrid.class',
                    'title' => 'particles.imagegrid.title',
                    'desc' => 'particles.imagegrid.desc',
                    'cols' => 'particles.imagegrid.cols',
                    'album' => 'particles.imagegrid.album',
                    'imagegriditems' => [
                        '*' => [
                            'title' => 'particles.imagegrid.imagegriditems.*.title',
                            'image' => 'particles.imagegrid.imagegriditems.*.image',
                            'caption' => 'particles.imagegrid.imagegriditems.*.caption'
                        ]
                    ]
                ],
                'infolist' => [
                    'enabled' => 'particles.infolist.enabled',
                    'class' => 'particles.infolist.class',
                    'title' => 'particles.infolist.title',
                    'infolists' => [
                        '*' => [
                            'title' => 'particles.infolist.infolists.*.title',
                            'link' => 'particles.infolist.infolists.*.link',
                            'description' => 'particles.infolist.infolists.*.description'
                        ]
                    ]
                ],
                'newsletter' => [
                    'enabled' => 'particles.newsletter.enabled',
                    'class' => 'particles.newsletter.class',
                    'title' => 'particles.newsletter.title',
                    'headtext' => 'particles.newsletter.headtext',
                    'inputboxtext' => 'particles.newsletter.inputboxtext',
                    'buttontext' => 'particles.newsletter.buttontext',
                    'uri' => 'particles.newsletter.uri',
                    'buttonclass' => 'particles.newsletter.buttonclass'
                ],
                'promoimage' => [
                    'enabled' => 'particles.promoimage.enabled',
                    'class' => 'particles.promoimage.class',
                    'title' => 'particles.promoimage.title',
                    'image' => 'particles.promoimage.image',
                    'promoimagetitle' => 'particles.promoimage.promoimagetitle',
                    'desc' => 'particles.promoimage.desc',
                    'iconbutton' => 'particles.promoimage.iconbutton',
                    'promoimageicons' => [
                        '*' => [
                            'title' => 'particles.promoimage.promoimageicons.*.title',
                            'icon' => 'particles.promoimage.promoimageicons.*.icon',
                            'link' => 'particles.promoimage.promoimageicons.*.link'
                        ]
                    ]
                ],
                'simplecounter' => [
                    'enabled' => 'particles.simplecounter.enabled',
                    'class' => 'particles.simplecounter.class',
                    'title' => 'particles.simplecounter.title',
                    'desc' => 'particles.simplecounter.desc',
                    'date' => 'particles.simplecounter.date',
                    'month' => 'particles.simplecounter.month',
                    'year' => 'particles.simplecounter.year',
                    'daytext' => 'particles.simplecounter.daytext',
                    'daystext' => 'particles.simplecounter.daystext',
                    'hourtext' => 'particles.simplecounter.hourtext',
                    'hourstext' => 'particles.simplecounter.hourstext',
                    'minutetext' => 'particles.simplecounter.minutetext',
                    'minutestext' => 'particles.simplecounter.minutestext',
                    'secondtext' => 'particles.simplecounter.secondtext',
                    'secondstext' => 'particles.simplecounter.secondstext'
                ],
                'analytics' => [
                    'enabled' => 'particles.analytics.enabled',
                    'ua' => [
                        'code' => 'particles.analytics.ua.code',
                        'anonym' => 'particles.analytics.ua.anonym',
                        'ssl' => 'particles.analytics.ua.ssl',
                        'debug' => 'particles.analytics.ua.debug'
                    ]
                ],
                'assets' => [
                    'enabled' => 'particles.assets.enabled',
                    'css' => [
                        '*' => [
                            'name' => 'particles.assets.css.*.name',
                            'location' => 'particles.assets.css.*.location',
                            'inline' => 'particles.assets.css.*.inline'
                        ]
                    ],
                    'extra' => 'particles.assets.extra',
                    'priority' => 'particles.assets.priority',
                    'javascript' => [
                        '*' => [
                            'name' => 'particles.assets.javascript.*.name',
                            'location' => 'particles.assets.javascript.*.location',
                            'inline' => 'particles.assets.javascript.*.inline'
                        ]
                    ],
                    'in_footer' => 'particles.assets.in_footer'
                ],
                'branding' => [
                    'enabled' => 'particles.branding.enabled',
                    'content' => 'particles.branding.content',
                    'css' => [
                        'class' => 'particles.branding.css.class'
                    ]
                ],
                'content' => [
                    'enabled' => 'particles.content.enabled'
                ],
                'contentarray' => [
                    'enabled' => 'particles.contentarray.enabled',
                    '_tab_articles' => 'particles.contentarray._tab_articles',
                    'article' => [
                        'filter' => [
                            'categories' => 'particles.contentarray.article.filter.categories',
                            'articles' => 'particles.contentarray.article.filter.articles',
                            'featured' => 'particles.contentarray.article.filter.featured'
                        ],
                        'limit' => [
                            'total' => 'particles.contentarray.article.limit.total',
                            'columns' => 'particles.contentarray.article.limit.columns',
                            'start' => 'particles.contentarray.article.limit.start'
                        ],
                        'sort' => [
                            'orderby' => 'particles.contentarray.article.sort.orderby',
                            'ordering' => 'particles.contentarray.article.sort.ordering'
                        ],
                        'display' => [
                            'image' => [
                                'enabled' => 'particles.contentarray.article.display.image.enabled'
                            ],
                            'text' => [
                                'type' => 'particles.contentarray.article.display.text.type',
                                'limit' => 'particles.contentarray.article.display.text.limit',
                                'formatting' => 'particles.contentarray.article.display.text.formatting'
                            ],
                            'title' => [
                                'enabled' => 'particles.contentarray.article.display.title.enabled',
                                'limit' => 'particles.contentarray.article.display.title.limit'
                            ],
                            'date' => [
                                'enabled' => 'particles.contentarray.article.display.date.enabled',
                                'format' => 'particles.contentarray.article.display.date.format'
                            ],
                            'read_more' => [
                                'enabled' => 'particles.contentarray.article.display.read_more.enabled',
                                'label' => 'particles.contentarray.article.display.read_more.label',
                                'css' => 'particles.contentarray.article.display.read_more.css'
                            ],
                            'author' => [
                                'enabled' => 'particles.contentarray.article.display.author.enabled'
                            ],
                            'category' => [
                                'enabled' => 'particles.contentarray.article.display.category.enabled'
                            ],
                            'hits' => [
                                'enabled' => 'particles.contentarray.article.display.hits.enabled'
                            ]
                        ]
                    ],
                    '_tab_display' => 'particles.contentarray._tab_display',
                    '_tab_readmore' => 'particles.contentarray._tab_readmore',
                    '_tab_extras' => 'particles.contentarray._tab_extras',
                    'css' => [
                        'class' => 'particles.contentarray.css.class'
                    ],
                    'extra' => 'particles.contentarray.extra'
                ],
                'copyright' => [
                    'enabled' => 'particles.copyright.enabled',
                    'date' => [
                        'start' => 'particles.copyright.date.start',
                        'end' => 'particles.copyright.date.end'
                    ],
                    'owner' => 'particles.copyright.owner'
                ],
                'custom' => [
                    'enabled' => 'particles.custom.enabled',
                    'html' => 'particles.custom.html',
                    'twig' => 'particles.custom.twig',
                    'filter' => 'particles.custom.filter'
                ],
                'date' => [
                    'enabled' => 'particles.date.enabled',
                    'css' => [
                        'class' => 'particles.date.css.class'
                    ],
                    'date' => [
                        'formats' => 'particles.date.date.formats'
                    ]
                ],
                'frameworks' => [
                    'enabled' => 'particles.frameworks.enabled',
                    'jquery' => [
                        'enabled' => 'particles.frameworks.jquery.enabled',
                        'ui_core' => 'particles.frameworks.jquery.ui_core',
                        'ui_sortable' => 'particles.frameworks.jquery.ui_sortable'
                    ],
                    'bootstrap' => [
                        'enabled' => 'particles.frameworks.bootstrap.enabled'
                    ],
                    'mootools' => [
                        'enabled' => 'particles.frameworks.mootools.enabled',
                        'more' => 'particles.frameworks.mootools.more'
                    ]
                ],
                'lightcase' => [
                    'enabled' => 'particles.lightcase.enabled'
                ],
                'logo' => [
                    'enabled' => 'particles.logo.enabled',
                    'url' => 'particles.logo.url',
                    'image' => 'particles.logo.image',
                    'link' => 'particles.logo.link',
                    'svg' => 'particles.logo.svg',
                    'text' => 'particles.logo.text',
                    'class' => 'particles.logo.class'
                ],
                'menu' => [
                    'enabled' => 'particles.menu.enabled',
                    'menu' => 'particles.menu.menu',
                    'base' => 'particles.menu.base',
                    'startLevel' => 'particles.menu.startLevel',
                    'maxLevels' => 'particles.menu.maxLevels',
                    'renderTitles' => 'particles.menu.renderTitles',
                    'hoverExpand' => 'particles.menu.hoverExpand',
                    'mobileTarget' => 'particles.menu.mobileTarget'
                ],
                'messages' => [
                    'enabled' => 'particles.messages.enabled'
                ],
                'mobile-menu' => [
                    'enabled' => 'particles.mobile-menu.enabled'
                ],
                'module' => [
                    'enabled' => 'particles.module.enabled',
                    'module_id' => 'particles.module.module_id',
                    'chrome' => 'particles.module.chrome'
                ],
                'position' => [
                    'enabled' => 'particles.position.enabled',
                    'key' => 'particles.position.key',
                    'chrome' => 'particles.position.chrome'
                ],
                'social' => [
                    'enabled' => 'particles.social.enabled',
                    'css' => [
                        'class' => 'particles.social.css.class'
                    ],
                    'title' => 'particles.social.title',
                    'target' => 'particles.social.target',
                    'display' => 'particles.social.display',
                    'items' => [
                        '*' => [
                            'name' => 'particles.social.items.*.name',
                            'icon' => 'particles.social.items.*.icon',
                            'text' => 'particles.social.items.*.text',
                            'link' => 'particles.social.items.*.link'
                        ]
                    ]
                ],
                'spacer' => [
                    'enabled' => 'particles.spacer.enabled'
                ],
                'totop' => [
                    'enabled' => 'particles.totop.enabled',
                    'css' => [
                        'class' => 'particles.totop.css.class'
                    ],
                    'icon' => 'particles.totop.icon',
                    'content' => 'particles.totop.content'
                ]
            ],
            'styles' => [
                'accent' => [
                    'color-1' => 'styles.accent.color-1',
                    'color-2' => 'styles.accent.color-2'
                ],
                'base' => [
                    'background' => 'styles.base.background',
                    'text-color' => 'styles.base.text-color',
                    'favicon' => 'styles.base.favicon'
                ],
                'breakpoints' => [
                    'large-desktop-container' => 'styles.breakpoints.large-desktop-container',
                    'desktop-container' => 'styles.breakpoints.desktop-container',
                    'tablet-container' => 'styles.breakpoints.tablet-container',
                    'large-mobile-container' => 'styles.breakpoints.large-mobile-container',
                    'mobile-menu-breakpoint' => 'styles.breakpoints.mobile-menu-breakpoint'
                ],
                'copyright' => [
                    'background' => 'styles.copyright.background',
                    'text-color' => 'styles.copyright.text-color'
                ],
                'extension' => [
                    'background' => 'styles.extension.background',
                    'text-color' => 'styles.extension.text-color'
                ],
                'feature' => [
                    'background' => 'styles.feature.background',
                    'text-color' => 'styles.feature.text-color'
                ],
                'font' => [
                    'family-default' => 'styles.font.family-default',
                    'family-title' => 'styles.font.family-title',
                    'family-promo' => 'styles.font.family-promo'
                ],
                'footer' => [
                    'background' => 'styles.footer.background',
                    'text-color' => 'styles.footer.text-color'
                ],
                'header' => [
                    'background' => 'styles.header.background',
                    'text-color' => 'styles.header.text-color'
                ],
                'main' => [
                    'background' => 'styles.main.background',
                    'text-color' => 'styles.main.text-color'
                ],
                'menu' => [
                    'col-width' => 'styles.menu.col-width',
                    'animation' => 'styles.menu.animation'
                ],
                'navigation' => [
                    'background' => 'styles.navigation.background',
                    'text-color' => 'styles.navigation.text-color'
                ],
                'offcanvas' => [
                    'background' => 'styles.offcanvas.background',
                    'text-color' => 'styles.offcanvas.text-color',
                    'width' => 'styles.offcanvas.width',
                    'toggle-color' => 'styles.offcanvas.toggle-color',
                    'toggle-visibility' => 'styles.offcanvas.toggle-visibility'
                ],
                'showcase' => [
                    'background' => 'styles.showcase.background',
                    'text-color' => 'styles.showcase.text-color'
                ]
            ],
            'page' => [
                'assets' => [
                    'favicon' => 'page.assets.favicon',
                    'touchicon' => 'page.assets.touchicon',
                    'css' => [
                        '*' => [
                            'name' => 'page.assets.css.*.name',
                            'location' => 'page.assets.css.*.location',
                            'inline' => 'page.assets.css.*.inline'
                        ]
                    ],
                    'extra' => 'page.assets.extra',
                    'priority' => 'page.assets.priority',
                    'javascript' => [
                        '*' => [
                            'name' => 'page.assets.javascript.*.name',
                            'location' => 'page.assets.javascript.*.location',
                            'inline' => 'page.assets.javascript.*.inline'
                        ]
                    ],
                    'in_footer' => 'page.assets.in_footer'
                ],
                'body' => [
                    'attribs' => [
                        'id' => 'page.body.attribs.id',
                        'class' => 'page.body.attribs.class',
                        'extra' => 'page.body.attribs.extra'
                    ],
                    'layout' => [
                        'sections' => 'page.body.layout.sections'
                    ],
                    'body_top' => 'page.body.body_top',
                    'body_bottom' => 'page.body.body_bottom'
                ],
                'head' => [
                    'meta' => 'page.head.meta',
                    'head_bottom' => 'page.head.head_bottom',
                    'atoms' => 'page.head.atoms'
                ]
            ]
        ],
        'dynamic' => [
            
        ],
        'filter' => [
            'validation' => true
        ],
        'configuration' => [
            'particles' => [
                'branding' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'copyright' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'custom' => [
                    'caching' => [
                        'type' => 'config_matches',
                        'values' => [
                            'twig' => '0',
                            'filter' => '0'
                        ]
                    ]
                ],
                'logo' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'menu' => [
                    'caching' => [
                        'type' => 'menu'
                    ]
                ],
                'mobile-menu' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'social' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'spacer' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'totop' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ]
            ]
        ]
    ]
];
