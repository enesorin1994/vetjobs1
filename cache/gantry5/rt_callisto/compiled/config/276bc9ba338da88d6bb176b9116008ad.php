<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1490184238,
    'checksum' => '53336015ed750234f803c7141530f9bd',
    'files' => [
        'templates/rt_callisto/custom/config/80' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/80/assignments.yaml',
                'modified' => 1490184218
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/80/index.yaml',
                'modified' => 1489745687
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/80/layout.yaml',
                'modified' => 1489745687
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/80/page/head.yaml',
                'modified' => 1488888443
            ],
            'styles' => [
                'file' => 'templates/rt_callisto/custom/config/80/styles.yaml',
                'modified' => 1489571583
            ]
        ]
    ],
    'data' => [
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ],
        'index' => [
            'name' => '80',
            'timestamp' => 1489745687,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/offline.png',
                'name' => 'pages_-_coming_soon',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-7370' => 'Logo'
                ],
                'menu' => [
                    'menu-4265' => 'Menu'
                ],
                'social' => [
                    'social-1354' => 'Social'
                ],
                'module' => [
                    'position-module-3493' => 'Module Instance',
                    'position-module-4744' => 'Module Instance',
                    'position-module-2992' => 'Module Instance',
                    'position-module-5401' => 'Module Instance',
                    'position-module-4643' => 'Module Instance'
                ],
                'content' => [
                    'system-content-5575' => 'Page Content'
                ],
                'custom' => [
                    'custom-3288' => 'Add a job to your Watchlist'
                ],
                'newsletter' => [
                    'newsletter-1051' => 'Newsletter'
                ],
                'copyright' => [
                    'copyright-8574' => 'Copyright'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/offline.png',
                'name' => 'pages_-_coming_soon',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-7370'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-4265 80',
                        1 => 'social-1354 20'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'position-module-3493'
                    ],
                    1 => [
                        0 => 'position-module-4744 33.3',
                        1 => 'position-module-2992 33.3',
                        2 => 'position-module-5401 33.3'
                    ]
                ],
                '/container-2571/' => [
                    0 => [
                        0 => [
                            'mainbar 67' => [
                                0 => [
                                    0 => 'system-content-5575'
                                ]
                            ]
                        ],
                        1 => [
                            'sidebar 33' => [
                                0 => [
                                    0 => 'position-module-4643'
                                ]
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'custom-3288'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'newsletter-1051'
                    ]
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'copyright-8574'
                    ]
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section'
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-2571' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'logo-7370' => [
                    'attributes' => [
                        'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                    ],
                    'block' => [
                        'class' => 'g-logo-block center'
                    ]
                ],
                'menu-4265' => [
                    'attributes' => [
                        'menu' => 'main-menu'
                    ]
                ],
                'position-module-3493' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '93',
                        'key' => 'module-instance'
                    ]
                ],
                'position-module-4744' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '103',
                        'key' => 'module-instance'
                    ]
                ],
                'position-module-2992' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '123',
                        'key' => 'module-instance'
                    ]
                ],
                'position-module-5401' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '131'
                    ]
                ],
                'position-module-4643' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '97',
                        'key' => 'module-instance'
                    ]
                ],
                'custom-3288' => [
                    'title' => 'Add a job to your Watchlist',
                    'attributes' => [
                        'html' => '<div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Add a job to your Watchlist</h2>
	<div class="g-layercontent-subtitle">Press the <strong>Add to Watchlist</strong> button under the Job Title</div>
	'
                    ],
                    'block' => [
                        'class' => 'flush center',
                        'variations' => 'box1'
                    ]
                ],
                'newsletter-1051' => [
                    'attributes' => [
                        'title' => 'Newsletter',
                        'headtext' => 'Aboneaza-te la newsletter-ul VetJobs si ramai la curent cu ultimele joburi din Medicina veterinara!',
                        'inputboxtext' => 'Email Address',
                        'buttontext' => 'Join',
                        'uri' => 'rocketthemeblog',
                        'buttonclass' => 'button button-3'
                    ]
                ],
                'copyright-8574' => [
                    'attributes' => [
                        'date' => [
                            'start' => '2007'
                        ],
                        'owner' => 'RocketTheme, LLC'
                    ],
                    'block' => [
                        'class' => 'center'
                    ]
                ]
            ]
        ],
        'page' => [
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'styles' => [
            'preset' => 'preset7',
            'base' => [
                'background' => '#ffffff'
            ],
            'accent' => [
                'color-1' => '#1abc9c',
                'color-2' => '#007e65'
            ],
            'header' => [
                'background' => '#1abc9c',
                'text-color' => '#138871'
            ],
            'navigation' => [
                'background' => '#ffffff',
                'text-color' => '#888888'
            ],
            'showcase' => [
                'background' => '#e8e8e8',
                'text-color' => '#878787'
            ],
            'feature' => [
                'background' => '#f5f5f5'
            ],
            'main' => [
                'background' => '#f5f5f5'
            ],
            'footer' => [
                'background' => '#ffffff',
                'text-color' => '#888888'
            ],
            'copyright' => [
                'background' => '#f5f5f5'
            ],
            'extension' => [
                'background' => '#f5f5f5'
            ],
            'offcanvas' => [
                'background' => '#f0f0f0',
                'text-color' => '#888888'
            ]
        ]
    ]
];
