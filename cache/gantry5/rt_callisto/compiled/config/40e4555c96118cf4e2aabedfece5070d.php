<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1490265491,
    'checksum' => '781fd7206a2e70ccbb455693e2893fd4',
    'files' => [
        'templates/rt_callisto/custom/config/82' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/82/assignments.yaml',
                'modified' => 1489746919
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/82/index.yaml',
                'modified' => 1490265488
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/82/layout.yaml',
                'modified' => 1490265488
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/82/page/head.yaml',
                'modified' => 1489746631
            ],
            'styles' => [
                'file' => 'templates/rt_callisto/custom/config/82/styles.yaml',
                'modified' => 1489746947
            ]
        ],
        'templates/rt_callisto/custom/config/default' => [
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/default/index.yaml',
                'modified' => 1488888442
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/default/layout.yaml',
                'modified' => 1488888442
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/default/page/head.yaml',
                'modified' => 1488888442
            ]
        ],
        'templates/rt_callisto/config/default' => [
            'particles/iconmenu' => [
                'file' => 'templates/rt_callisto/config/default/particles/iconmenu.yaml',
                'modified' => 1463470148
            ],
            'particles/logo' => [
                'file' => 'templates/rt_callisto/config/default/particles/logo.yaml',
                'modified' => 1463470148
            ],
            'particles/social' => [
                'file' => 'templates/rt_callisto/config/default/particles/social.yaml',
                'modified' => 1463470148
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'css' => [
                    'class' => 'branding'
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'date' => [
                    'start' => 'now',
                    'end' => 'now'
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => true,
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'link' => true,
                'url' => '',
                'image' => 'gantry-theme://images/logo/callisto.png',
                'text' => 'Callisto',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => true,
                'menu' => '',
                'base' => '/',
                'startLevel' => 1,
                'maxLevels' => 0,
                'renderTitles' => 0,
                'hoverExpand' => 1,
                'mobileTarget' => 0
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'social-items'
                ],
                'target' => '_blank',
                'display' => 'both',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-twitter fa-fw',
                        'text' => '',
                        'link' => 'http://twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ],
                    1 => [
                        'icon' => 'fa fa-facebook fa-fw',
                        'text' => '',
                        'link' => 'http://facebook.com/rockettheme',
                        'name' => 'Facebook'
                    ],
                    2 => [
                        'icon' => 'fa fa-google fa-fw',
                        'text' => '',
                        'link' => 'http://plus.google.com/+rockettheme',
                        'name' => 'Google'
                    ],
                    3 => [
                        'icon' => 'fa fa-rss fa-fw',
                        'text' => '',
                        'link' => 'http://www.rockettheme.com/product-updates?rss',
                        'name' => 'RSS'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'css' => [
                    'class' => 'totop'
                ]
            ],
            'contact' => [
                'enabled' => true
            ],
            'contentlist' => [
                'enabled' => true,
                'cols' => 'g-listgrid-4cols'
            ],
            'iconmenu' => [
                'enabled' => '1',
                'target' => '_parent',
                'class' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-diamond',
                        'text' => 'Features',
                        'link' => 'http://docs.gantry.org/gantry5/basics/key-features',
                        'name' => 'Features'
                    ],
                    1 => [
                        'icon' => 'fa fa-rocket',
                        'text' => 'Gantry 5',
                        'link' => 'http://www.gantry.org/',
                        'name' => 'Gantry 5'
                    ],
                    2 => [
                        'icon' => 'fa fa-gear',
                        'text' => 'Addons',
                        'link' => 'http://docs.gantry.org/gantry5/particles',
                        'name' => 'Addons'
                    ],
                    3 => [
                        'icon' => 'fa fa-cloud-download',
                        'text' => 'Download',
                        'link' => 'http://www.rockettheme.com/joomla/templates/callisto',
                        'name' => 'Download'
                    ]
                ]
            ],
            'imagegrid' => [
                'enabled' => true,
                'cols' => 'g-imagegrid-2cols'
            ],
            'infolist' => [
                'enabled' => true
            ],
            'newsletter' => [
                'enabled' => true
            ],
            'promoimage' => [
                'enabled' => true
            ],
            'simplecounter' => [
                'enabled' => true,
                'month' => 0
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true,
                'priority' => 0,
                'in_footer' => false
            ],
            'content' => [
                'enabled' => true
            ],
            'contentarray' => [
                'enabled' => true,
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show'
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show'
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ]
                ]
            ],
            'date' => [
                'enabled' => true,
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => true
            ],
            'module' => [
                'enabled' => true
            ],
            'position' => [
                'enabled' => true
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#1abc9c',
                'color-2' => '#007e65'
            ],
            'base' => [
                'background' => '#ffffff',
                'text-color' => '#888888'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '48rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '48rem'
            ],
            'copyright' => [
                'background' => '#f5f5f5',
                'text-color' => '#878787'
            ],
            'extension' => [
                'background' => '#f5f5f5',
                'text-color' => '#878787'
            ],
            'feature' => [
                'background' => '#f5f5f5',
                'text-color' => '#878787'
            ],
            'font' => [
                'family-default' => 'sourcesanspro, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-title' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-promo' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif'
            ],
            'footer' => [
                'background' => '#ffffff',
                'text-color' => '#888888'
            ],
            'header' => [
                'background' => '#1abc9c',
                'text-color' => '#138871'
            ],
            'main' => [
                'background' => '#f5f5f5',
                'text-color' => '#878787'
            ],
            'menu' => [
                'col-width' => '180px',
                'animation' => 'g-fade'
            ],
            'navigation' => [
                'background' => '#ffffff',
                'text-color' => '#888888'
            ],
            'offcanvas' => [
                'background' => '#f0f0f0',
                'text-color' => '#888888',
                'width' => '17rem',
                'toggle-color' => '#ffffff',
                'toggle-visibility' => 1
            ],
            'showcase' => [
                'background' => '#e8e8e8',
                'text-color' => '#878787'
            ],
            'preset' => 'preset7'
        ],
        'page' => [
            'assets' => [
                'priority' => 0,
                'in_footer' => false
            ],
            'body' => [
                'attribs' => [
                    'class' => 'gantry'
                ],
                'layout' => [
                    'sections' => 0
                ]
            ],
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'index' => [
            'name' => '82',
            'timestamp' => 1490265488,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'pages_-_clients',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-4920' => 'Logo'
                ],
                'module' => [
                    'position-module-1922' => 'Module Instance',
                    'position-module-3373' => 'Module Instance',
                    'position-module-8126' => 'Module Instance',
                    'position-module-2057' => 'Module Instance'
                ],
                'menu' => [
                    'menu-8752' => 'Menu'
                ],
                'content' => [
                    'system-content-8183' => 'Page Content'
                ],
                'date' => [
                    'date-8645' => 'Date'
                ],
                'infolist' => [
                    'infolist-1186' => 'Info List'
                ],
                'mobile-menu' => [
                    'mobile-menu-8532' => 'Mobile Menu'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'pages_-_clients',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-4920 80',
                        1 => 'position-module-1922 20'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-8752'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'position-module-3373'
                    ],
                    1 => [
                        0 => 'system-content-8183'
                    ],
                    2 => [
                        0 => 'position-module-8126 50',
                        1 => 'position-module-2057 50'
                    ]
                ],
                '/container-8748/' => [
                    0 => [
                        0 => [
                            'mainbar 67' => [
                                
                            ]
                        ],
                        1 => [
                            'sidebar 33' => [
                                
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'date-8645'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'infolist-1186'
                    ]
                ],
                '/copyright/' => [
                    
                ],
                'offcanvas' => [
                    0 => [
                        0 => 'mobile-menu-8532'
                    ]
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section'
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-8748' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'logo-4920' => [
                    'attributes' => [
                        'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                    ],
                    'block' => [
                        'class' => 'g-logo-block'
                    ]
                ],
                'position-module-1922' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '97',
                        'key' => 'module-instance'
                    ]
                ],
                'menu-8752' => [
                    'block' => [
                        'class' => 'g-menu-block'
                    ]
                ],
                'position-module-3373' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '127',
                        'key' => 'module-instance'
                    ]
                ],
                'position-module-8126' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '103',
                        'key' => 'module-instance'
                    ],
                    'block' => [
                        'variations' => 'box-blue shadow rounded equal-height center'
                    ]
                ],
                'position-module-2057' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '123',
                        'key' => 'module-instance'
                    ],
                    'block' => [
                        'variations' => 'box-blue'
                    ]
                ],
                'infolist-1186' => [
                    'title' => 'Info List'
                ],
                'mobile-menu-8532' => [
                    'title' => 'Mobile Menu'
                ]
            ]
        ],
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ]
    ]
];
