<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1490089748,
    'checksum' => '816acab4f554b9bb6a2c30d1fa3657f0',
    'files' => [
        'templates/rt_callisto/custom/config/70' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/70/assignments.yaml',
                'modified' => 1488888964
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/70/index.yaml',
                'modified' => 1488888443
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/70/layout.yaml',
                'modified' => 1488888443
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/70/page/head.yaml',
                'modified' => 1488888443
            ]
        ],
        'templates/rt_callisto/custom/config/default' => [
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/default/index.yaml',
                'modified' => 1488888442
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/default/layout.yaml',
                'modified' => 1488888442
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/default/page/head.yaml',
                'modified' => 1488888442
            ]
        ],
        'templates/rt_callisto/config/default' => [
            'particles/iconmenu' => [
                'file' => 'templates/rt_callisto/config/default/particles/iconmenu.yaml',
                'modified' => 1463470148
            ],
            'particles/logo' => [
                'file' => 'templates/rt_callisto/config/default/particles/logo.yaml',
                'modified' => 1463470148
            ],
            'particles/social' => [
                'file' => 'templates/rt_callisto/config/default/particles/social.yaml',
                'modified' => 1463470148
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'css' => [
                    'class' => 'branding'
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'date' => [
                    'start' => 'now',
                    'end' => 'now'
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => true,
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'link' => true,
                'url' => '',
                'image' => 'gantry-theme://images/logo/callisto.png',
                'text' => 'Callisto',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => true,
                'menu' => '',
                'base' => '/',
                'startLevel' => 1,
                'maxLevels' => 0,
                'renderTitles' => 0,
                'hoverExpand' => 1,
                'mobileTarget' => 0
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'social-items'
                ],
                'target' => '_blank',
                'display' => 'both',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-twitter fa-fw',
                        'text' => '',
                        'link' => 'http://twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ],
                    1 => [
                        'icon' => 'fa fa-facebook fa-fw',
                        'text' => '',
                        'link' => 'http://facebook.com/rockettheme',
                        'name' => 'Facebook'
                    ],
                    2 => [
                        'icon' => 'fa fa-google fa-fw',
                        'text' => '',
                        'link' => 'http://plus.google.com/+rockettheme',
                        'name' => 'Google'
                    ],
                    3 => [
                        'icon' => 'fa fa-rss fa-fw',
                        'text' => '',
                        'link' => 'http://www.rockettheme.com/product-updates?rss',
                        'name' => 'RSS'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'css' => [
                    'class' => 'totop'
                ]
            ],
            'contact' => [
                'enabled' => true
            ],
            'contentlist' => [
                'enabled' => true,
                'cols' => 'g-listgrid-4cols'
            ],
            'iconmenu' => [
                'enabled' => '1',
                'target' => '_parent',
                'class' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-diamond',
                        'text' => 'Features',
                        'link' => 'http://docs.gantry.org/gantry5/basics/key-features',
                        'name' => 'Features'
                    ],
                    1 => [
                        'icon' => 'fa fa-rocket',
                        'text' => 'Gantry 5',
                        'link' => 'http://www.gantry.org/',
                        'name' => 'Gantry 5'
                    ],
                    2 => [
                        'icon' => 'fa fa-gear',
                        'text' => 'Addons',
                        'link' => 'http://docs.gantry.org/gantry5/particles',
                        'name' => 'Addons'
                    ],
                    3 => [
                        'icon' => 'fa fa-cloud-download',
                        'text' => 'Download',
                        'link' => 'http://www.rockettheme.com/joomla/templates/callisto',
                        'name' => 'Download'
                    ]
                ]
            ],
            'imagegrid' => [
                'enabled' => true,
                'cols' => 'g-imagegrid-2cols'
            ],
            'infolist' => [
                'enabled' => true
            ],
            'newsletter' => [
                'enabled' => true
            ],
            'promoimage' => [
                'enabled' => true
            ],
            'simplecounter' => [
                'enabled' => true,
                'month' => 0
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true,
                'priority' => 0,
                'in_footer' => false
            ],
            'content' => [
                'enabled' => true
            ],
            'contentarray' => [
                'enabled' => true,
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show'
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show'
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ]
                ]
            ],
            'date' => [
                'enabled' => true,
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => true
            ],
            'module' => [
                'enabled' => true
            ],
            'position' => [
                'enabled' => true
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#ecbc2e',
                'color-2' => '#8b5717'
            ],
            'base' => [
                'background' => '#1f1f1f',
                'text-color' => '#888888'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '48rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '48rem'
            ],
            'copyright' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'extension' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'feature' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'font' => [
                'family-default' => 'sourcesanspro, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-title' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-promo' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif'
            ],
            'footer' => [
                'background' => '#1f1f1f',
                'text-color' => '#878787'
            ],
            'header' => [
                'background' => '#ecbc2e',
                'text-color' => '#b08523'
            ],
            'main' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'menu' => [
                'col-width' => '180px',
                'animation' => 'g-fade'
            ],
            'navigation' => [
                'background' => '#353535',
                'text-color' => '#828282'
            ],
            'offcanvas' => [
                'background' => '#1b1b1f',
                'text-color' => '#ffffff',
                'width' => '17rem',
                'toggle-color' => '#ffffff',
                'toggle-visibility' => 1
            ],
            'showcase' => [
                'background' => '#23262f',
                'text-color' => '#ffffff'
            ]
        ],
        'page' => [
            'assets' => [
                'priority' => 0,
                'in_footer' => false
            ],
            'body' => [
                'attribs' => [
                    'class' => 'gantry'
                ],
                'layout' => [
                    'sections' => 0
                ]
            ],
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'index' => [
            'name' => 70,
            'timestamp' => 1488888443,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'features_-_variations',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-3466' => 'System Messages'
                ],
                'logo' => [
                    'logo-8333' => 'Logo'
                ],
                'iconmenu' => [
                    'iconmenu-6237' => 'Icon Menu'
                ],
                'menu' => [
                    'menu-2294' => 'Menu'
                ],
                'social' => [
                    'social-4596' => 'Social'
                ],
                'custom' => [
                    'custom-7447' => 'Block Variations',
                    'custom-1069' => 'Box 1',
                    'custom-6250' => 'Box 2',
                    'custom-4307' => 'Box 3',
                    'custom-5636' => 'Box 4',
                    'custom-2992' => 'Box Grey',
                    'custom-1856' => 'Box Pink',
                    'custom-6603' => 'Box Red',
                    'custom-3649' => 'Box Purple',
                    'custom-4857' => 'Box Orange',
                    'custom-1099' => 'Box Blue',
                    'custom-1659' => 'No Variations',
                    'custom-7072' => 'Additional Effects & Classes'
                ],
                'spacer' => [
                    'spacer-6152' => 'Spacer'
                ],
                'infolist' => [
                    'infolist-9607' => 'Effects',
                    'infolist-2122' => 'Utilities',
                    'infolist-6850' => 'Utilities Cont.'
                ],
                'copyright' => [
                    'copyright-8647' => 'Copyright'
                ],
                'mobile-menu' => [
                    'mobile-menu-2339' => 'Mobile Menu'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'features_-_variations',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'system-messages-3466'
                    ],
                    1 => [
                        0 => 'logo-8333 56',
                        1 => 'iconmenu-6237 44'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-2294 80',
                        1 => 'social-4596 20'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'custom-7447'
                    ]
                ],
                '/container-1252/' => [
                    0 => [
                        0 => [
                            'mainbar 67' => [
                                0 => [
                                    0 => 'custom-1069 33.3333',
                                    1 => 'custom-6250 33.3333',
                                    2 => 'custom-4307 33.3333'
                                ],
                                1 => [
                                    0 => 'custom-5636 33.3333',
                                    1 => 'custom-2992 33.3333',
                                    2 => 'custom-1856 33.3333'
                                ],
                                2 => [
                                    0 => 'custom-6603 33.3333',
                                    1 => 'custom-3649 33.3333',
                                    2 => 'custom-4857 33.3333'
                                ],
                                3 => [
                                    0 => 'custom-1099 33.3333',
                                    1 => 'custom-1659 33.3334',
                                    2 => 'spacer-6152 33.3332999999'
                                ]
                            ]
                        ],
                        1 => [
                            'sidebar 33' => [
                                
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'custom-7072'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'infolist-9607 33.3333',
                        1 => 'infolist-2122 33.3332999999',
                        2 => 'infolist-6850 33.3334'
                    ]
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'copyright-8647'
                    ]
                ],
                'offcanvas' => [
                    0 => [
                        0 => 'mobile-menu-2339'
                    ]
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => ''
                    ]
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-1252' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => 'box4',
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'system-messages-3466' => [
                    'layout' => false
                ],
                'logo-8333' => [
                    'layout' => false,
                    'block' => [
                        'class' => 'g-logo-block'
                    ]
                ],
                'iconmenu-6237' => [
                    'title' => 'Icon Menu',
                    'layout' => false,
                    'block' => [
                        'class' => 'flush'
                    ]
                ],
                'menu-2294' => [
                    'layout' => false,
                    'block' => [
                        'class' => 'g-menu-block'
                    ]
                ],
                'social-4596' => [
                    'layout' => false
                ],
                'custom-7447' => [
                    'title' => 'Block Variations',
                    'attributes' => [
                        'html' => '<div class="g-layercontent">
    <h1 class="g-layercontent-title">Block Variations</h1>
    <h5 class="g-layercontent-subtitle nomarginall">Individualize your block content with many available variations</h5>

    <a href="http://docs.gantry.org/gantry5/configure/layout-manager#blocks" class="button button-3"><i class="fa fa-fw fa-columns"></i> Learn More</a>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'flush center'
                    ]
                ],
                'custom-1069' => [
                    'title' => 'Box 1',
                    'attributes' => [
                        'html' => '<h3>Box 1</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box1'
                    ]
                ],
                'custom-6250' => [
                    'title' => 'Box 2',
                    'attributes' => [
                        'html' => '<h3>Box 2</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2'
                    ]
                ],
                'custom-4307' => [
                    'title' => 'Box 3',
                    'attributes' => [
                        'html' => '<h3>Box 3</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box3'
                    ]
                ],
                'custom-5636' => [
                    'title' => 'Box 4',
                    'attributes' => [
                        'html' => '<h3>Box 4</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box4'
                    ]
                ],
                'custom-2992' => [
                    'title' => 'Box Grey',
                    'attributes' => [
                        'html' => '<h3>Box Grey</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box-grey'
                    ]
                ],
                'custom-1856' => [
                    'title' => 'Box Pink',
                    'attributes' => [
                        'html' => '<h3>Box Pink</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box-pink'
                    ]
                ],
                'custom-6603' => [
                    'title' => 'Box Red',
                    'attributes' => [
                        'html' => '<h3>Box Red</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box-red'
                    ]
                ],
                'custom-3649' => [
                    'title' => 'Box Purple',
                    'attributes' => [
                        'html' => '<h3>Box Purple</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box-purple'
                    ]
                ],
                'custom-4857' => [
                    'title' => 'Box Orange',
                    'attributes' => [
                        'html' => '<h3>Box Orange</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box-orange'
                    ]
                ],
                'custom-1099' => [
                    'title' => 'Box Blue',
                    'attributes' => [
                        'html' => '<h3>Box Blue</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box-blue'
                    ]
                ],
                'custom-1659' => [
                    'title' => 'No Variations',
                    'attributes' => [
                        'html' => '<h3>No Variation</h3>
<p>Lorem<strong> ipsum dolor</strong> sit amet, <a href="#">consecetur</a> adipiscing elit <em>donec sit</em> amet nibh.</p>
<a href="#" class="button">Read More</a>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'shadow'
                    ]
                ],
                'spacer-6152' => [
                    'layout' => false
                ],
                'custom-7072' => [
                    'title' => 'Additional Effects & Classes',
                    'attributes' => [
                        'html' => '<div class="g-layercontent">
	<h2 class="g-layercontent-title">Additional Effects &amp; Classes</h2>
	<div class="g-layercontent-subtitle">A collection of additional variations for structural and subtle stylistic adjustments</div>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'flush center',
                        'variations' => 'box3'
                    ]
                ],
                'infolist-9607' => [
                    'title' => 'Effects',
                    'attributes' => [
                        'title' => 'Effects',
                        'infolists' => [
                            0 => [
                                'link' => '',
                                'description' => 'Add squared corners to a block',
                                'title' => 'Square'
                            ],
                            1 => [
                                'link' => '',
                                'description' => 'Add rounded corners to a block',
                                'title' => 'Rounded'
                            ],
                            2 => [
                                'link' => '',
                                'description' => 'Add a shadow effect to a block',
                                'title' => 'Shadow 1'
                            ],
                            3 => [
                                'link' => '',
                                'description' => 'Add a different shadow to a block',
                                'title' => 'Shadow 2'
                            ]
                        ]
                    ],
                    'layout' => false
                ],
                'infolist-2122' => [
                    'title' => 'Utilities',
                    'attributes' => [
                        'title' => 'Utilities',
                        'infolists' => [
                            0 => [
                                'link' => '',
                                'description' => 'Add low opacity to a block',
                                'title' => 'Disabled'
                            ],
                            1 => [
                                'link' => '',
                                'description' => 'Center the block content',
                                'title' => 'Center'
                            ],
                            2 => [
                                'link' => '',
                                'description' => 'Apply background to the whole block',
                                'title' => 'Outer Box'
                            ],
                            3 => [
                                'link' => '',
                                'description' => 'Remove outer margin/padding ',
                                'title' => 'Flush'
                            ]
                        ]
                    ],
                    'layout' => false
                ],
                'infolist-6850' => [
                    'title' => 'Utilities Cont.',
                    'attributes' => [
                        'title' => '<span class="hidden-phone">&nbsp;</span><span class="visible-phone">Utilities</span>',
                        'infolists' => [
                            0 => [
                                'link' => '',
                                'description' => 'Align block content to the left',
                                'title' => 'Align Left'
                            ],
                            1 => [
                                'link' => '',
                                'description' => 'Align block content to the right',
                                'title' => 'Align Right'
                            ],
                            2 => [
                                'link' => '',
                                'description' => 'Remove outer margin',
                                'title' => 'No Margin'
                            ],
                            3 => [
                                'link' => '',
                                'description' => 'Remove outer padding',
                                'title' => 'No Padding'
                            ]
                        ]
                    ],
                    'layout' => false
                ],
                'copyright-8647' => [
                    'attributes' => [
                        'date' => [
                            'start' => '2007'
                        ],
                        'owner' => 'RocketTheme, LLC'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'center'
                    ]
                ],
                'mobile-menu-2339' => [
                    'title' => 'Mobile Menu',
                    'layout' => false
                ]
            ]
        ],
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ]
    ]
];
