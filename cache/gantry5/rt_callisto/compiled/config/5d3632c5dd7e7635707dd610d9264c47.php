<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1490863130,
    'checksum' => 'f38e12963930a107338bcde6a8a40bc3',
    'files' => [
        'templates/rt_callisto/custom/config/68' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/68/assignments.yaml',
                'modified' => 1489742753
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/68/index.yaml',
                'modified' => 1490863031
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/68/layout.yaml',
                'modified' => 1490863031
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/68/page/head.yaml',
                'modified' => 1488888198
            ],
            'styles' => [
                'file' => 'templates/rt_callisto/custom/config/68/styles.yaml',
                'modified' => 1490277473
            ]
        ],
        'templates/rt_callisto/custom/config/default' => [
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/default/index.yaml',
                'modified' => 1488888442
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/default/layout.yaml',
                'modified' => 1488888442
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/default/page/head.yaml',
                'modified' => 1488888442
            ]
        ],
        'templates/rt_callisto/config/default' => [
            'particles/iconmenu' => [
                'file' => 'templates/rt_callisto/config/default/particles/iconmenu.yaml',
                'modified' => 1463470148
            ],
            'particles/logo' => [
                'file' => 'templates/rt_callisto/config/default/particles/logo.yaml',
                'modified' => 1463470148
            ],
            'particles/social' => [
                'file' => 'templates/rt_callisto/config/default/particles/social.yaml',
                'modified' => 1463470148
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'css' => [
                    'class' => 'branding'
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'date' => [
                    'start' => 'now',
                    'end' => 'now'
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => true,
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'link' => true,
                'url' => '',
                'image' => 'gantry-theme://images/logo/callisto.png',
                'text' => 'Callisto',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => true,
                'menu' => '',
                'base' => '/',
                'startLevel' => 1,
                'maxLevels' => 0,
                'renderTitles' => 0,
                'hoverExpand' => 1,
                'mobileTarget' => 0
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'social-items'
                ],
                'target' => '_blank',
                'display' => 'both',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-twitter fa-fw',
                        'text' => '',
                        'link' => 'http://twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ],
                    1 => [
                        'icon' => 'fa fa-facebook fa-fw',
                        'text' => '',
                        'link' => 'http://facebook.com/rockettheme',
                        'name' => 'Facebook'
                    ],
                    2 => [
                        'icon' => 'fa fa-google fa-fw',
                        'text' => '',
                        'link' => 'http://plus.google.com/+rockettheme',
                        'name' => 'Google'
                    ],
                    3 => [
                        'icon' => 'fa fa-rss fa-fw',
                        'text' => '',
                        'link' => 'http://www.rockettheme.com/product-updates?rss',
                        'name' => 'RSS'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'css' => [
                    'class' => 'totop'
                ]
            ],
            'contact' => [
                'enabled' => true
            ],
            'contentlist' => [
                'enabled' => true,
                'cols' => 'g-listgrid-4cols'
            ],
            'iconmenu' => [
                'enabled' => '1',
                'target' => '_parent',
                'class' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-diamond',
                        'text' => 'Features',
                        'link' => 'http://docs.gantry.org/gantry5/basics/key-features',
                        'name' => 'Features'
                    ],
                    1 => [
                        'icon' => 'fa fa-rocket',
                        'text' => 'Gantry 5',
                        'link' => 'http://www.gantry.org/',
                        'name' => 'Gantry 5'
                    ],
                    2 => [
                        'icon' => 'fa fa-gear',
                        'text' => 'Addons',
                        'link' => 'http://docs.gantry.org/gantry5/particles',
                        'name' => 'Addons'
                    ],
                    3 => [
                        'icon' => 'fa fa-cloud-download',
                        'text' => 'Download',
                        'link' => 'http://www.rockettheme.com/joomla/templates/callisto',
                        'name' => 'Download'
                    ]
                ]
            ],
            'imagegrid' => [
                'enabled' => true,
                'cols' => 'g-imagegrid-2cols'
            ],
            'infolist' => [
                'enabled' => true
            ],
            'newsletter' => [
                'enabled' => true
            ],
            'promoimage' => [
                'enabled' => true
            ],
            'simplecounter' => [
                'enabled' => true,
                'month' => 0
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true,
                'priority' => 0,
                'in_footer' => false
            ],
            'content' => [
                'enabled' => true
            ],
            'contentarray' => [
                'enabled' => true,
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show'
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show'
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ]
                ]
            ],
            'date' => [
                'enabled' => true,
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => true
            ],
            'module' => [
                'enabled' => true
            ],
            'position' => [
                'enabled' => true
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#ecbc2e',
                'color-2' => '#8b5717'
            ],
            'base' => [
                'background' => '#1f1f1f',
                'text-color' => '#888888'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '48rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '48rem'
            ],
            'copyright' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'extension' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'feature' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'font' => [
                'family-default' => 'sourcesanspro, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-title' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-promo' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif'
            ],
            'footer' => [
                'background' => '#1f1f1f',
                'text-color' => '#878787'
            ],
            'header' => [
                'background' => '#ecbc2e',
                'text-color' => '#b08523'
            ],
            'main' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'menu' => [
                'col-width' => '180px',
                'animation' => 'g-fade'
            ],
            'navigation' => [
                'background' => '#353535',
                'text-color' => '#828282'
            ],
            'offcanvas' => [
                'background' => '#1b1b1f',
                'text-color' => '#ffffff',
                'width' => '17rem',
                'toggle-color' => '#ffffff',
                'toggle-visibility' => 1
            ],
            'showcase' => [
                'background' => '#23262f',
                'text-color' => '#ffffff'
            ],
            'preset' => 'preset1'
        ],
        'page' => [
            'assets' => [
                'priority' => 0,
                'in_footer' => false
            ],
            'body' => [
                'attribs' => [
                    'class' => 'gantry'
                ],
                'layout' => [
                    'sections' => 0
                ]
            ],
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'index' => [
            'name' => '68',
            'timestamp' => 1490863031,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/home.png',
                'name' => 'features_-_overview',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-6050' => 'Logo'
                ],
                'module' => [
                    'position-module-6475' => 'Module Instance',
                    'position-module-8986' => 'Module Instance',
                    'position-module-4565' => 'Module Instance',
                    'position-module-6126' => 'Module Instance'
                ],
                'menu' => [
                    'menu-4774' => 'Menu'
                ],
                'social' => [
                    'social-9696' => 'Social'
                ],
                'custom' => [
                    'custom-3582' => 'Gantry 5 - Next Generation Theme Framework',
                    'custom-3561' => 'Add To Watchlist'
                ],
                'content' => [
                    'system-content-2146' => 'Page Content'
                ],
                'newsletter' => [
                    'newsletter-1649' => 'Newsletter'
                ],
                'copyright' => [
                    'copyright-6987' => 'Copyright'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/home.png',
                'name' => 'features_-_overview',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-6050 80',
                        1 => 'position-module-6475 20'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-4774 80',
                        1 => 'social-9696 20'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'position-module-8986'
                    ],
                    1 => [
                        0 => 'custom-3582'
                    ],
                    2 => [
                        0 => 'system-content-2146'
                    ],
                    3 => [
                        0 => 'position-module-4565 50',
                        1 => 'position-module-6126 50'
                    ]
                ],
                '/container-8514/' => [
                    0 => [
                        0 => [
                            'mainbar 67' => [
                                
                            ]
                        ],
                        1 => [
                            'sidebar 33' => [
                                
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'custom-3561'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'newsletter-1649'
                    ]
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'copyright-6987'
                    ]
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section'
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-8514' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'logo-6050' => [
                    'attributes' => [
                        'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                    ],
                    'block' => [
                        'class' => 'g-logo-block'
                    ]
                ],
                'position-module-6475' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '97',
                        'key' => 'module-instance'
                    ]
                ],
                'menu-4774' => [
                    'block' => [
                        'class' => 'g-menu-block'
                    ]
                ],
                'position-module-8986' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '106',
                        'key' => 'module-instance'
                    ]
                ],
                'custom-3582' => [
                    'title' => 'Gantry 5 - Next Generation Theme Framework',
                    'attributes' => [
                        'html' => '<div class="g-layercontent">
    <h1 class="g-layercontent-title g-uppercase">VetJobs </h1>
    <h3 class="g-layercontent-title g-uppercase">Cauta job-uri in categoriile de mai jos:</h3>
    <h5 class="g-layercontent-subtitle nomarginall"></h5>

    <a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=1&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Animale mici/companie</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=2&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Animale de ferma</a></br>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=3&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Clinica mixta</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=4&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Ecvine</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=5&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Animale exotice</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=2&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Animale de ferma</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=6&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Management</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=7&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Acvacultura</a>
<a href="http://localhost/vet/index.php?option=com_jobsfactory&task=listjobs&cat=9&Itemid=442" class="button button-3 button-xlarge"><i class="fa fa-fw fa-university"></i> Circumscriptie veterinara</a>




</div>

'
                    ],
                    'block' => [
                        'class' => 'flush center'
                    ]
                ],
                'system-content-2146' => [
                    'block' => [
                        'variations' => 'box2'
                    ]
                ],
                'position-module-4565' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '103',
                        'key' => 'module-instance'
                    ],
                    'block' => [
                        'variations' => 'shadow rounded equal-height center box-orange'
                    ]
                ],
                'position-module-6126' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '123',
                        'key' => 'module-instance'
                    ],
                    'block' => [
                        'variations' => 'box-blue shadow rounded equal-height center'
                    ]
                ],
                'custom-3561' => [
                    'title' => 'Add To Watchlist',
                    'attributes' => [
                        'html' => '<div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Add a job to your Watchlist</h2>
	<div class="g-layercontent-subtitle">Press the <strong>Add to Watchlist</strong> button under the Job Title</div>
	'
                    ],
                    'block' => [
                        'class' => 'flush center',
                        'variations' => 'box1'
                    ]
                ],
                'newsletter-1649' => [
                    'attributes' => [
                        'title' => 'Newsletter',
                        'headtext' => 'Aboneaza-te la newsletter-ul VetJobs si ramai la curent cu ultimele joburi din Medicina veterinara!',
                        'inputboxtext' => 'E-mail adress',
                        'buttontext' => 'Join',
                        'buttonclass' => 'button button-3'
                    ],
                    'block' => [
                        'class' => 'flush center'
                    ]
                ],
                'copyright-6987' => [
                    'attributes' => [
                        'date' => [
                            'start' => '2007'
                        ],
                        'owner' => 'RocketTheme, LLC'
                    ],
                    'block' => [
                        'class' => 'center'
                    ]
                ]
            ]
        ],
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ]
    ]
];
