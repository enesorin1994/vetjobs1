<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1490861902,
    'checksum' => '1dfee473a141755c4d924d48edf600df',
    'files' => [
        'templates/rt_callisto/custom/config/67' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/67/assignments.yaml',
                'modified' => 1489569339
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/67/index.yaml',
                'modified' => 1490861899
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/67/layout.yaml',
                'modified' => 1490861899
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/67/page/head.yaml',
                'modified' => 1488888177
            ],
            'styles' => [
                'file' => 'templates/rt_callisto/custom/config/67/styles.yaml',
                'modified' => 1490277494
            ]
        ],
        'templates/rt_callisto/custom/config/default' => [
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/default/index.yaml',
                'modified' => 1488888442
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/default/layout.yaml',
                'modified' => 1488888442
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/default/page/head.yaml',
                'modified' => 1488888442
            ]
        ],
        'templates/rt_callisto/config/default' => [
            'particles/iconmenu' => [
                'file' => 'templates/rt_callisto/config/default/particles/iconmenu.yaml',
                'modified' => 1463470148
            ],
            'particles/logo' => [
                'file' => 'templates/rt_callisto/config/default/particles/logo.yaml',
                'modified' => 1463470148
            ],
            'particles/social' => [
                'file' => 'templates/rt_callisto/config/default/particles/social.yaml',
                'modified' => 1463470148
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'css' => [
                    'class' => 'branding'
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'date' => [
                    'start' => 'now',
                    'end' => 'now'
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => true,
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'link' => true,
                'url' => '',
                'image' => 'gantry-theme://images/logo/callisto.png',
                'text' => 'Callisto',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => true,
                'menu' => '',
                'base' => '/',
                'startLevel' => 1,
                'maxLevels' => 0,
                'renderTitles' => 0,
                'hoverExpand' => 1,
                'mobileTarget' => 0
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'social-items'
                ],
                'target' => '_blank',
                'display' => 'both',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-twitter fa-fw',
                        'text' => '',
                        'link' => 'http://twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ],
                    1 => [
                        'icon' => 'fa fa-facebook fa-fw',
                        'text' => '',
                        'link' => 'http://facebook.com/rockettheme',
                        'name' => 'Facebook'
                    ],
                    2 => [
                        'icon' => 'fa fa-google fa-fw',
                        'text' => '',
                        'link' => 'http://plus.google.com/+rockettheme',
                        'name' => 'Google'
                    ],
                    3 => [
                        'icon' => 'fa fa-rss fa-fw',
                        'text' => '',
                        'link' => 'http://www.rockettheme.com/product-updates?rss',
                        'name' => 'RSS'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'css' => [
                    'class' => 'totop'
                ]
            ],
            'contact' => [
                'enabled' => true
            ],
            'contentlist' => [
                'enabled' => true,
                'cols' => 'g-listgrid-4cols'
            ],
            'iconmenu' => [
                'enabled' => '1',
                'target' => '_parent',
                'class' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-diamond',
                        'text' => 'Features',
                        'link' => 'http://docs.gantry.org/gantry5/basics/key-features',
                        'name' => 'Features'
                    ],
                    1 => [
                        'icon' => 'fa fa-rocket',
                        'text' => 'Gantry 5',
                        'link' => 'http://www.gantry.org/',
                        'name' => 'Gantry 5'
                    ],
                    2 => [
                        'icon' => 'fa fa-gear',
                        'text' => 'Addons',
                        'link' => 'http://docs.gantry.org/gantry5/particles',
                        'name' => 'Addons'
                    ],
                    3 => [
                        'icon' => 'fa fa-cloud-download',
                        'text' => 'Download',
                        'link' => 'http://www.rockettheme.com/joomla/templates/callisto',
                        'name' => 'Download'
                    ]
                ]
            ],
            'imagegrid' => [
                'enabled' => true,
                'cols' => 'g-imagegrid-2cols'
            ],
            'infolist' => [
                'enabled' => true
            ],
            'newsletter' => [
                'enabled' => true
            ],
            'promoimage' => [
                'enabled' => true
            ],
            'simplecounter' => [
                'enabled' => true,
                'month' => 0
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true,
                'priority' => 0,
                'in_footer' => false
            ],
            'content' => [
                'enabled' => true
            ],
            'contentarray' => [
                'enabled' => true,
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show'
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show'
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ]
                ]
            ],
            'date' => [
                'enabled' => true,
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => true
            ],
            'module' => [
                'enabled' => true
            ],
            'position' => [
                'enabled' => true
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#ecbc2e',
                'color-2' => '#8b5717'
            ],
            'base' => [
                'background' => '#1f1f1f',
                'text-color' => '#888888'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '48rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '48rem'
            ],
            'copyright' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'extension' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'feature' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'font' => [
                'family-default' => 'sourcesanspro, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-title' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-promo' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif'
            ],
            'footer' => [
                'background' => '#1f1f1f',
                'text-color' => '#878787'
            ],
            'header' => [
                'background' => '#ecbc2e',
                'text-color' => '#b08523'
            ],
            'main' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'menu' => [
                'col-width' => '180px',
                'animation' => 'g-fade'
            ],
            'navigation' => [
                'background' => '#353535',
                'text-color' => '#828282'
            ],
            'offcanvas' => [
                'background' => '#1b1b1f',
                'text-color' => '#ffffff',
                'width' => '17rem',
                'toggle-color' => '#ffffff',
                'toggle-visibility' => 1
            ],
            'showcase' => [
                'background' => '#23262f',
                'text-color' => '#ffffff'
            ],
            'preset' => 'preset1'
        ],
        'page' => [
            'assets' => [
                'priority' => 0,
                'in_footer' => false
            ],
            'body' => [
                'attribs' => [
                    'class' => 'gantry'
                ],
                'layout' => [
                    'sections' => 0
                ]
            ],
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'index' => [
            'name' => '67',
            'timestamp' => 1490861899,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/home.png',
                'name' => 'home',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainfeature' => 'Mainfeature',
                'sidefeature' => 'Sidefeature',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'main' => 'Main',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-7854' => 'Logo'
                ],
                'module' => [
                    'position-module-7600' => 'Module Instance',
                    'position-module-4689' => 'searchbox',
                    'position-module-8679' => 'latest',
                    'position-module-4798' => 'featured'
                ],
                'menu' => [
                    'menu-5647' => 'Menu'
                ],
                'social' => [
                    'social-3414' => 'Social'
                ],
                'content' => [
                    'system-content-8850' => 'Page Content'
                ],
                'contentlist' => [
                    'contentlist-3684' => 'FP Main Content List'
                ],
                'contact' => [
                    'contact-8860' => 'Contact Details'
                ],
                'copyright' => [
                    'copyright-5334' => 'Copyright'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/home.png',
                'name' => 'home',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-7854 80',
                        1 => 'position-module-7600 20'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-5647 80',
                        1 => 'social-3414 20'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'position-module-4689'
                    ],
                    1 => [
                        0 => 'system-content-8850'
                    ],
                    2 => [
                        0 => 'position-module-8679 50',
                        1 => 'position-module-4798 50'
                    ],
                    3 => [
                        0 => 'contentlist-3684'
                    ]
                ],
                '/container-4626/' => [
                    0 => [
                        0 => [
                            'mainfeature 67' => [
                                
                            ]
                        ],
                        1 => [
                            'sidefeature 33' => [
                                
                            ]
                        ]
                    ]
                ],
                '/main/' => [
                    
                ],
                '/extension/' => [
                    
                ],
                '/footer/' => [
                    0 => [
                        0 => 'contact-8860'
                    ]
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'copyright-5334'
                    ]
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainfeature' => [
                    'type' => 'section'
                ],
                'sidefeature' => [
                    'type' => 'section'
                ],
                'container-4626' => [
                    'attributes' => [
                        'id' => 'g-feature',
                        'boxed' => ''
                    ]
                ],
                'main' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'logo-7854' => [
                    'attributes' => [
                        'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                    ],
                    'block' => [
                        'class' => 'g-logo-block'
                    ]
                ],
                'position-module-7600' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '97',
                        'key' => 'module-instance'
                    ]
                ],
                'menu-5647' => [
                    'attributes' => [
                        'menu' => 'main-menu'
                    ],
                    'block' => [
                        'class' => 'g-menu-block'
                    ]
                ],
                'position-module-4689' => [
                    'title' => 'searchbox',
                    'attributes' => [
                        'module_id' => '106',
                        'key' => 'searchbox'
                    ]
                ],
                'system-content-8850' => [
                    'block' => [
                        'variations' => 'box2'
                    ]
                ],
                'position-module-8679' => [
                    'title' => 'latest',
                    'attributes' => [
                        'module_id' => '103',
                        'key' => 'latest'
                    ],
                    'block' => [
                        'variations' => 'rounded shadow center equal-height box-orange'
                    ]
                ],
                'position-module-4798' => [
                    'title' => 'featured',
                    'attributes' => [
                        'module_id' => '101',
                        'key' => 'featured'
                    ],
                    'block' => [
                        'variations' => 'box-orange shadow rounded equal-height center'
                    ]
                ],
                'contentlist-3684' => [
                    'title' => 'FP Main Content List',
                    'attributes' => [
                        'title' => '                      Cele mai multe joburi in domeniul Medicinii Veterinare!',
                        'image' => 'gantry-media://vete.jpg',
                        'imagetag' => 'VetJobs',
                        'headline' => 'VetJobs, platforma ta in domeniul medicinii veterinare.',
                        'subtitle' => 'Rapid si usor',
                        'desc' => 'VetJobs ofera utilizatorilor sai o interfata usor accesibila, fiind foarte usor, atat pentru viitorii angajati cat si pentru angajatori, sa aplice sau sa posteze noi oferte de locuri de munca.',
                        'readmore' => '',
                        'link' => ''
                    ],
                    'block' => [
                        'class' => 'fp-contentlist',
                        'variations' => 'box2 rounded'
                    ]
                ],
                'contact-8860' => [
                    'title' => 'Contact Details',
                    'attributes' => [
                        'title' => 'Contact Details',
                        'contactitems' => [
                            0 => [
                                'icon' => 'fa fa-phone fa-fw',
                                'text' => '0766666666',
                                'label' => 'Call Us'
                            ],
                            1 => [
                                'icon' => 'fa fa-map-marker fa-fw',
                                'text' => 'Bucharest, Romania',
                                'label' => 'Office'
                            ],
                            2 => [
                                'icon' => 'fa fa-envelope fa-fw',
                                'text' => 'mail@vetjobs.ro',
                                'label' => 'Say Hi'
                            ],
                            3 => [
                                'icon' => 'fa fa-comment fa-fw',
                                'text' => '<a href="#">Live Chat with Us</a>',
                                'label' => 'Need Advice?'
                            ]
                        ]
                    ]
                ],
                'copyright-5334' => [
                    'attributes' => [
                        'date' => [
                            'start' => '2007'
                        ],
                        'owner' => 'RocketTheme, LLC'
                    ],
                    'block' => [
                        'class' => 'center'
                    ]
                ]
            ]
        ],
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ]
    ]
];
