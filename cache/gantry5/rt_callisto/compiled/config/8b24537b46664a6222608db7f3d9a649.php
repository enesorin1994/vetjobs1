<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1490787795,
    'checksum' => '1d1d821473ce6a1fc24dcfd946babd7a',
    'files' => [
        'templates/rt_callisto/custom/config/83' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/83/assignments.yaml',
                'modified' => 1490264666
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/83/index.yaml',
                'modified' => 1490784802
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/83/layout.yaml',
                'modified' => 1490784802
            ],
            'page/assets' => [
                'file' => 'templates/rt_callisto/custom/config/83/page/assets.yaml',
                'modified' => 1490264603
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/83/page/head.yaml',
                'modified' => 1490264603
            ],
            'styles' => [
                'file' => 'templates/rt_callisto/custom/config/83/styles.yaml',
                'modified' => 1490277522
            ]
        ]
    ],
    'data' => [
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ],
        'index' => [
            'name' => '83',
            'timestamp' => 1490784802,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-8844' => 'Logo / Image'
                ],
                'module' => [
                    'position-module-6670' => 'Module Instance',
                    'position-module-8446' => 'search',
                    'position-module-2531' => 'Module Instance',
                    'position-module-9396' => 'Module Instance',
                    'position-module-6545' => 'Module Instance'
                ],
                'menu' => [
                    'menu-4871' => 'Menu'
                ],
                'social' => [
                    'social-4022' => 'Social'
                ],
                'content' => [
                    'system-content-8106' => 'Page Content'
                ],
                'date' => [
                    'date-8867' => 'Date'
                ],
                'infolist' => [
                    'infolist-5873' => 'Info List'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-8844 80',
                        1 => 'position-module-6670 20'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-4871 80',
                        1 => 'social-4022 20'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'position-module-8446'
                    ],
                    1 => [
                        0 => 'position-module-2531 50',
                        1 => 'position-module-9396 50'
                    ],
                    2 => [
                        0 => 'position-module-6545'
                    ]
                ],
                '/container-6538/' => [
                    0 => [
                        0 => [
                            'mainbar 80' => [
                                0 => [
                                    0 => 'system-content-8106'
                                ]
                            ]
                        ],
                        1 => [
                            'sidebar 20' => [
                                
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'date-8867'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'infolist-5873'
                    ]
                ],
                '/copyright/' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => 'box1',
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section'
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-6538' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'logo-8844' => [
                    'title' => 'Logo / Image',
                    'attributes' => [
                        'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                    ]
                ],
                'position-module-6670' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '97',
                        'key' => 'module-instance'
                    ]
                ],
                'menu-4871' => [
                    'attributes' => [
                        'menu' => 'main-menu'
                    ]
                ],
                'social-4022' => [
                    'attributes' => [
                        'items' => [
                            0 => [
                                'icon' => 'fa fa-twitter fa-fw',
                                'text' => '',
                                'link' => 'http://twitter.com/rockettheme',
                                'name' => 'Twitter'
                            ],
                            1 => [
                                'icon' => 'fa fa-facebook fa-fw',
                                'text' => '',
                                'link' => 'http://facebook.com/rockettheme',
                                'name' => 'Facebook'
                            ],
                            2 => [
                                'icon' => 'fa fa-google fa-fw',
                                'text' => '',
                                'link' => 'http://plus.google.com/+rockettheme',
                                'name' => 'Google'
                            ]
                        ]
                    ]
                ],
                'position-module-8446' => [
                    'title' => 'search',
                    'attributes' => [
                        'module_id' => '106',
                        'key' => 'search'
                    ]
                ],
                'position-module-2531' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '103',
                        'key' => 'module-instance'
                    ],
                    'block' => [
                        'variations' => 'shadow rounded equal-height center box-orange'
                    ]
                ],
                'position-module-9396' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '101',
                        'key' => 'module-instance'
                    ],
                    'block' => [
                        'variations' => 'shadow2 rounded equal-height center box-orange'
                    ]
                ],
                'position-module-6545' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '88',
                        'key' => 'module-instance'
                    ]
                ],
                'system-content-8106' => [
                    'block' => [
                        'variations' => 'box2'
                    ]
                ],
                'infolist-5873' => [
                    'title' => 'Info List'
                ]
            ]
        ],
        'page' => [
            'assets' => [
                'css' => [
                    0 => [
                        'location' => 'components/com_roksprocket/layouts/tabs/themes/default/tabs.css',
                        'inline' => '',
                        'extra' => [
                            
                        ],
                        'priority' => '0',
                        'name' => 'Tabs'
                    ]
                ],
                'javascript' => [
                    0 => [
                        'location' => 'components/com_roksprocket/layouts/tabs/themes/default/tabs.js',
                        'inline' => '',
                        'in_footer' => '0',
                        'extra' => [
                            
                        ],
                        'priority' => '0',
                        'name' => 'Tabs'
                    ]
                ]
            ],
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'styles' => [
            'preset' => 'preset1'
        ]
    ]
];
