<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1489567545,
    'checksum' => 'b4c7b1cb268912a20fbba5c6569a8e16',
    'files' => [
        'templates/rt_callisto/custom/config/default' => [
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/default/index.yaml',
                'modified' => 1488888442
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/default/layout.yaml',
                'modified' => 1488888442
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/default/page/head.yaml',
                'modified' => 1488888442
            ]
        ],
        'templates/rt_callisto/config/default' => [
            'particles/iconmenu' => [
                'file' => 'templates/rt_callisto/config/default/particles/iconmenu.yaml',
                'modified' => 1463470148
            ],
            'particles/logo' => [
                'file' => 'templates/rt_callisto/config/default/particles/logo.yaml',
                'modified' => 1463470148
            ],
            'particles/social' => [
                'file' => 'templates/rt_callisto/config/default/particles/social.yaml',
                'modified' => 1463470148
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'css' => [
                    'class' => 'branding'
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'date' => [
                    'start' => 'now',
                    'end' => 'now'
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => true,
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'link' => true,
                'url' => '',
                'image' => 'gantry-theme://images/logo/callisto.png',
                'text' => 'Callisto',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => true,
                'menu' => '',
                'base' => '/',
                'startLevel' => 1,
                'maxLevels' => 0,
                'renderTitles' => 0,
                'hoverExpand' => 1,
                'mobileTarget' => 0
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'social-items'
                ],
                'target' => '_blank',
                'display' => 'both',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-twitter fa-fw',
                        'text' => '',
                        'link' => 'http://twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ],
                    1 => [
                        'icon' => 'fa fa-facebook fa-fw',
                        'text' => '',
                        'link' => 'http://facebook.com/rockettheme',
                        'name' => 'Facebook'
                    ],
                    2 => [
                        'icon' => 'fa fa-google fa-fw',
                        'text' => '',
                        'link' => 'http://plus.google.com/+rockettheme',
                        'name' => 'Google'
                    ],
                    3 => [
                        'icon' => 'fa fa-rss fa-fw',
                        'text' => '',
                        'link' => 'http://www.rockettheme.com/product-updates?rss',
                        'name' => 'RSS'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'css' => [
                    'class' => 'totop'
                ]
            ],
            'contact' => [
                'enabled' => true
            ],
            'contentlist' => [
                'enabled' => true,
                'cols' => 'g-listgrid-4cols'
            ],
            'iconmenu' => [
                'enabled' => '1',
                'target' => '_parent',
                'class' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-diamond',
                        'text' => 'Features',
                        'link' => 'http://docs.gantry.org/gantry5/basics/key-features',
                        'name' => 'Features'
                    ],
                    1 => [
                        'icon' => 'fa fa-rocket',
                        'text' => 'Gantry 5',
                        'link' => 'http://www.gantry.org/',
                        'name' => 'Gantry 5'
                    ],
                    2 => [
                        'icon' => 'fa fa-gear',
                        'text' => 'Addons',
                        'link' => 'http://docs.gantry.org/gantry5/particles',
                        'name' => 'Addons'
                    ],
                    3 => [
                        'icon' => 'fa fa-cloud-download',
                        'text' => 'Download',
                        'link' => 'http://www.rockettheme.com/joomla/templates/callisto',
                        'name' => 'Download'
                    ]
                ]
            ],
            'imagegrid' => [
                'enabled' => true,
                'cols' => 'g-imagegrid-2cols'
            ],
            'infolist' => [
                'enabled' => true
            ],
            'newsletter' => [
                'enabled' => true
            ],
            'promoimage' => [
                'enabled' => true
            ],
            'simplecounter' => [
                'enabled' => true,
                'month' => 0
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true,
                'priority' => 0,
                'in_footer' => false
            ],
            'content' => [
                'enabled' => true
            ],
            'contentarray' => [
                'enabled' => true,
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show'
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show'
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ]
                ]
            ],
            'date' => [
                'enabled' => true,
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => true
            ],
            'module' => [
                'enabled' => true
            ],
            'position' => [
                'enabled' => true
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#ecbc2e',
                'color-2' => '#8b5717'
            ],
            'base' => [
                'background' => '#1f1f1f',
                'text-color' => '#888888'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '48rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '48rem'
            ],
            'copyright' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'extension' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'feature' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'font' => [
                'family-default' => 'sourcesanspro, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-title' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-promo' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif'
            ],
            'footer' => [
                'background' => '#1f1f1f',
                'text-color' => '#878787'
            ],
            'header' => [
                'background' => '#ecbc2e',
                'text-color' => '#b08523'
            ],
            'main' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'menu' => [
                'col-width' => '180px',
                'animation' => 'g-fade'
            ],
            'navigation' => [
                'background' => '#353535',
                'text-color' => '#828282'
            ],
            'offcanvas' => [
                'background' => '#1b1b1f',
                'text-color' => '#ffffff',
                'width' => '17rem',
                'toggle-color' => '#ffffff',
                'toggle-visibility' => 1
            ],
            'showcase' => [
                'background' => '#23262f',
                'text-color' => '#ffffff'
            ]
        ],
        'page' => [
            'assets' => [
                'priority' => 0,
                'in_footer' => false
            ],
            'body' => [
                'attribs' => [
                    'class' => 'gantry'
                ],
                'layout' => [
                    'sections' => 0
                ]
            ],
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'index' => [
            'name' => 'default',
            'timestamp' => 1488888442,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-9283' => 'System Messages'
                ],
                'logo' => [
                    'logo-8920' => 'Logo'
                ],
                'iconmenu' => [
                    'iconmenu-8998' => 'Icon Menu'
                ],
                'menu' => [
                    'menu-8316' => 'Menu'
                ],
                'social' => [
                    'social-4391' => 'Social'
                ],
                'content' => [
                    'system-content-9244' => 'Page Content'
                ],
                'custom' => [
                    'custom-1806' => 'About Callisto',
                    'custom-8186' => 'Sample Sitemap'
                ],
                'newsletter' => [
                    'newsletter-2485' => 'Newsletter'
                ],
                'copyright' => [
                    'copyright-4423' => 'Copyright'
                ],
                'mobile-menu' => [
                    'mobile-menu-3736' => 'Mobile Menu'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'system-messages-9283'
                    ],
                    1 => [
                        0 => 'logo-8920 56',
                        1 => 'iconmenu-8998 44'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-8316 80',
                        1 => 'social-4391 20'
                    ]
                ],
                '/showcase/' => [
                    
                ],
                '/container-5483/' => [
                    0 => [
                        0 => [
                            'mainbar 67' => [
                                0 => [
                                    0 => 'system-content-9244'
                                ]
                            ]
                        ],
                        1 => [
                            'sidebar 33' => [
                                
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    
                ],
                '/footer/' => [
                    0 => [
                        0 => 'custom-1806 33.3333',
                        1 => 'newsletter-2485 33.3333',
                        2 => 'custom-8186 33.3334'
                    ]
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'copyright-4423'
                    ]
                ],
                'offcanvas' => [
                    0 => [
                        0 => 'mobile-menu-3736'
                    ]
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => 'box1',
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section'
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-5483' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'system-messages-9283' => [
                    'layout' => false
                ],
                'logo-8920' => [
                    'layout' => false,
                    'block' => [
                        'class' => 'g-logo-block'
                    ]
                ],
                'iconmenu-8998' => [
                    'title' => 'Icon Menu',
                    'layout' => false,
                    'block' => [
                        'class' => 'flush'
                    ]
                ],
                'menu-8316' => [
                    'layout' => false,
                    'block' => [
                        'class' => 'g-menu-block'
                    ]
                ],
                'social-4391' => [
                    'layout' => false
                ],
                'system-content-9244' => [
                    'layout' => false,
                    'block' => [
                        'class' => 'nomarginall',
                        'variations' => 'box2'
                    ]
                ],
                'custom-1806' => [
                    'title' => 'About Callisto',
                    'attributes' => [
                        'html' => '<h2 class="g-title">About Callisto</h2>

<p>All demo content is for sample purposes only, intended to represent a live site.</p>

<p>The sample pages are intended to show how Callisto can be constructed on your site.</p>'
                    ],
                    'layout' => false
                ],
                'newsletter-2485' => [
                    'attributes' => [
                        'title' => 'Newsletter',
                        'headtext' => 'Subscribe to our newsletter and stay updated on the latest developments and special offers!',
                        'inputboxtext' => 'Email Address',
                        'buttontext' => 'Join',
                        'uri' => 'rocketthemeblog',
                        'buttonclass' => 'button button-3'
                    ],
                    'layout' => false
                ],
                'custom-8186' => [
                    'title' => 'Sample Sitemap',
                    'attributes' => [
                        'html' => '<h2 class="g-title">Sample Sitemap</h2>

<div class="g-grid">
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Home</a></li>
			<li><a href="#">Features</a></li>
			<li><a href="#">Typography</a></li>
			<li><a href="#">Particles</a></li>
			<li><a href="#">Variations</a></li>
		</ul>		
	</div>
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Buttons</a></li>
			<li><a href="#">Pages</a></li>
			<li><a href="#">Guide</a></li>
			<li><a href="#">Support</a></li>
			<li><a href="#">Download</a></li>
		</ul>		
	</div>	
</div>'
                    ],
                    'layout' => false
                ],
                'copyright-4423' => [
                    'attributes' => [
                        'date' => [
                            'start' => '2007'
                        ],
                        'owner' => 'RocketTheme, LLC'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'center'
                    ]
                ],
                'mobile-menu-3736' => [
                    'title' => 'Mobile Menu',
                    'layout' => false
                ]
            ]
        ]
    ]
];
