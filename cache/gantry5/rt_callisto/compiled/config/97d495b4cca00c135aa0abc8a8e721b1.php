<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1490184367,
    'checksum' => 'ede3d361a6163a4dcbb2a0b0bcc73627',
    'files' => [
        'templates/rt_callisto/custom/config/69' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/69/assignments.yaml',
                'modified' => 1488888757
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/69/index.yaml',
                'modified' => 1488888443
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/69/layout.yaml',
                'modified' => 1488888443
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/69/page/head.yaml',
                'modified' => 1488888443
            ]
        ]
    ],
    'data' => [
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ],
        'index' => [
            'name' => 69,
            'timestamp' => 1488888443,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'features_-_typography',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-8658' => 'System Messages'
                ],
                'logo' => [
                    'logo-1092' => 'Logo'
                ],
                'iconmenu' => [
                    'iconmenu-7378' => 'Icon Menu'
                ],
                'menu' => [
                    'menu-8546' => 'Menu'
                ],
                'social' => [
                    'social-1670' => 'Social'
                ],
                'custom' => [
                    'custom-4745' => 'Particle Typography Header',
                    'custom-7725' => 'Content - Content List',
                    'custom-8660' => 'Content - Image Grid',
                    'custom-9557' => 'Content - Info List',
                    'custom-3309' => 'Content - Promo Image',
                    'custom-2068' => 'Content - Contact',
                    'custom-4935' => 'Button Variations Header',
                    'custom-6880' => 'Button Variations Content',
                    'custom-1330' => 'Standard Typography Header',
                    'custom-4033' => 'Standard Typography'
                ],
                'contentlist' => [
                    'contentlist-7803' => 'Content List'
                ],
                'imagegrid' => [
                    'imagegrid-7310' => 'Image Grid'
                ],
                'infolist' => [
                    'infolist-3012' => 'Info List'
                ],
                'promoimage' => [
                    'promoimage-9935' => 'Promo Image'
                ],
                'contact' => [
                    'contact-3983' => 'Contact'
                ],
                'copyright' => [
                    'copyright-4817' => 'Copyright'
                ],
                'mobile-menu' => [
                    'mobile-menu-8246' => 'Mobile Menu'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'features_-_typography',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'system-messages-8658'
                    ],
                    1 => [
                        0 => 'logo-1092 56',
                        1 => 'iconmenu-7378 44'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-8546 80',
                        1 => 'social-1670 20'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'custom-4745'
                    ]
                ],
                '/container-5088/' => [
                    0 => [
                        0 => [
                            'mainbar 67' => [
                                0 => [
                                    0 => 'contentlist-7803 50',
                                    1 => 'custom-7725 50'
                                ],
                                1 => [
                                    0 => 'custom-8660 50',
                                    1 => 'imagegrid-7310 50'
                                ],
                                2 => [
                                    0 => 'infolist-3012 50',
                                    1 => 'custom-9557 50'
                                ],
                                3 => [
                                    0 => 'custom-3309 50',
                                    1 => 'promoimage-9935 50'
                                ],
                                4 => [
                                    0 => 'contact-3983 50',
                                    1 => 'custom-2068 50'
                                ]
                            ]
                        ],
                        1 => [
                            'sidebar 33' => [
                                
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'custom-4935'
                    ],
                    1 => [
                        0 => 'custom-6880'
                    ],
                    2 => [
                        0 => 'custom-1330'
                    ],
                    3 => [
                        0 => 'custom-4033'
                    ]
                ],
                '/footer/' => [
                    
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'copyright-4817'
                    ]
                ],
                'offcanvas' => [
                    0 => [
                        0 => 'mobile-menu-8246'
                    ]
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section'
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-5088' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'system-messages-8658' => [
                    'layout' => false
                ],
                'logo-1092' => [
                    'layout' => false,
                    'block' => [
                        'class' => 'g-logo-block'
                    ]
                ],
                'iconmenu-7378' => [
                    'title' => 'Icon Menu',
                    'layout' => false,
                    'block' => [
                        'class' => 'flush'
                    ]
                ],
                'menu-8546' => [
                    'layout' => false,
                    'block' => [
                        'class' => 'g-menu-block'
                    ]
                ],
                'social-1670' => [
                    'layout' => false
                ],
                'custom-4745' => [
                    'title' => 'Particle Typography Header',
                    'attributes' => [
                        'html' => '<div class="g-layercontent">
    <h1 class="g-layercontent-title">Typography</h1>
    <h5 class="g-layercontent-subtitle nomarginall">A selection of custom <strong>Particles</strong> available with Callisto</h5>

    <a href="http://docs.gantry.org/gantry5/particles" class="button button-3"> Learn More</a>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'flush center'
                    ]
                ],
                'contentlist-7803' => [
                    'title' => 'Content List',
                    'attributes' => [
                        'image' => 'gantry-theme://images/demo/pages/features/typography/img-01.jpg',
                        'imagetag' => 'Image Tag',
                        'headline' => 'Headline',
                        'subtitle' => 'Subtitle',
                        'desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet nibh. Vivamus non arcu.',
                        'readmore' => 'Read More Text',
                        'link' => '#',
                        'cols' => 'g-listgrid-2cols',
                        'listgriditems' => [
                            0 => [
                                'icon' => 'fa fa-file-text-o fa-fw',
                                'text' => 'List item with individual icon controls',
                                'link' => '',
                                'title' => 'List Item 1'
                            ],
                            1 => [
                                'icon' => 'fa fa-bell fa-fw',
                                'text' => 'List item with individual icon controls',
                                'link' => '',
                                'title' => 'List Item 2'
                            ],
                            2 => [
                                'icon' => 'fa fa-cloud fa-fw',
                                'text' => 'List item with individual icon controls',
                                'link' => '',
                                'title' => 'List Item 3'
                            ],
                            3 => [
                                'icon' => 'fa fa-arrows fa-fw',
                                'text' => 'List item with individual icon controls',
                                'link' => '',
                                'title' => 'List Item 4'
                            ],
                            4 => [
                                'icon' => 'fa fa-camera fa-fw',
                                'text' => 'List item with individual icon controls',
                                'link' => '',
                                'title' => 'List Item 5'
                            ],
                            5 => [
                                'icon' => 'fa fa-adjust fa-fw',
                                'text' => 'List item with individual icon controls',
                                'link' => '',
                                'title' => 'List Item 6'
                            ]
                        ]
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'custom-7725' => [
                    'title' => 'Content - Content List',
                    'attributes' => [
                        'html' => '<div class="g-layercontent nopaddingall">
	<h1 class="g-layercontent-title g-uppercase">Particle</h1>
	<h3 class="g-layercontent-title g-uppercase">Content List</h3>
	<p class="g-layercontent-promotext nomarginall">The Content List is a versatile content particle that has separate sections for its text and image content, as well as the lists. The particle requires an image, with options for a tag overlay, and various adjacent text/button options.</p>
	<div class="g-layercontent-promotext">The particle also features collection lists, with support for 1-5 columns. Each list item has its own unique icon, link and text settings.</div>

	<a href="http://docs.gantry.org/gantry5/particles" class="button button-3"> Learn More</a>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'custom-8660' => [
                    'title' => 'Content - Image Grid',
                    'attributes' => [
                        'html' => '<div class="g-layercontent nopaddingall">
	<h1 class="g-layercontent-title g-uppercase">Particle</h1>
	<h3 class="g-layercontent-title g-uppercase">Image Grid</h3>
	<p class="g-layercontent-promotext nomarginall">The Image Grid particle is a simple solution for displaying a small grid of images. Choose up to 5 columns, and an unlimited amount of rows. All images are clickable and a RokBox modal will show the full sized image.</p>
	<div class="g-layercontent-promotext hidden-tablet">The particle also supports collection lists for creating new image entries. Each image has settings for caption that appear in the RokBox modal and the path of the file.</div>

	<a href="http://docs.gantry.org/gantry5/particles" class="button button-3"> Learn More</a>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'imagegrid-7310' => [
                    'title' => 'Image Grid',
                    'attributes' => [
                        'title' => 'Title',
                        'desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet nibh. Vivamus non arcu.',
                        'cols' => 'g-imagegrid-2cols',
                        'album' => 'Typography',
                        'imagegriditems' => [
                            0 => [
                                'image' => 'gantry-theme://images/demo/pages/features/overview/img-04.jpg',
                                'caption' => 'Caption 1',
                                'title' => 'Image 1'
                            ],
                            1 => [
                                'image' => 'gantry-theme://images/demo/pages/features/overview/img-05.jpg',
                                'caption' => 'Caption 2',
                                'title' => 'Image 2'
                            ],
                            2 => [
                                'image' => 'gantry-theme://images/demo/pages/features/overview/img-06.jpg',
                                'caption' => 'Caption 3',
                                'title' => 'Image 3'
                            ],
                            3 => [
                                'image' => 'gantry-theme://images/demo/pages/features/overview/img-07.jpg',
                                'caption' => 'Caption 4',
                                'title' => 'Image 4'
                            ]
                        ]
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'infolist-3012' => [
                    'title' => 'Info List',
                    'attributes' => [
                        'title' => 'Title',
                        'infolists' => [
                            0 => [
                                'link' => '#',
                                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet nibh. Vivamus non arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                                'title' => 'List Item 1'
                            ],
                            1 => [
                                'link' => '#',
                                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet nibh. Vivamus non arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                                'title' => 'List Item 2'
                            ]
                        ]
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'custom-9557' => [
                    'title' => 'Content - Info List',
                    'attributes' => [
                        'html' => '<div class="g-layercontent nopaddingall">
	<h1 class="g-layercontent-title g-uppercase">Particle</h1>
	<h3 class="g-layercontent-title g-uppercase">Info List</h3>
	<p class="g-layercontent-promotext nomarginall">Info List is a simple particle for creating stacked list items with linkable titles and descriptions underneath. All items are separated by a border, and can be created via the collection list interface for quick and easy setup.</p>
	<a href="http://docs.gantry.org/gantry5/particles" class="button button-3"> Learn More</a>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'custom-3309' => [
                    'title' => 'Content - Promo Image',
                    'attributes' => [
                        'html' => '<div class="g-layercontent nopaddingall">
	<h1 class="g-layercontent-title g-uppercase">Particle</h1>
	<h3 class="g-layercontent-title g-uppercase">Promo Image</h3>
	<p class="g-layercontent-promotext nomarginall">The Promo Image particle offers a simple interface for adding a featured image, with overlay support for a title, a description and icons. Icons can be added via the collection list interface for quick and easy setup, each with individual link settings.</p>
	<a href="http://docs.gantry.org/gantry5/particles" class="button button-3"> Learn More</a>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'promoimage-9935' => [
                    'title' => 'Promo Image',
                    'attributes' => [
                        'title' => 'Title',
                        'image' => 'gantry-theme://images/demo/pages/features/overview/img-03.jpg',
                        'promoimagetitle' => 'Promo Image Title',
                        'desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet nibh. Vivamus non arcu.',
                        'iconbutton' => 'fa fa-fw fa-file-o',
                        'promoimageicons' => [
                            0 => [
                                'icon' => 'fa fa-cloud fa-fw',
                                'link' => '#',
                                'title' => 'Icon 1'
                            ],
                            1 => [
                                'icon' => 'fa fa-adjust fa-fw',
                                'link' => '#',
                                'title' => 'Icon 2'
                            ],
                            2 => [
                                'icon' => 'fa fa-camera fa-fw',
                                'link' => '#',
                                'title' => 'Icon 3'
                            ]
                        ]
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'contact-3983' => [
                    'attributes' => [
                        'title' => 'Title',
                        'contactitems' => [
                            0 => [
                                'icon' => 'fa fa-phone fa-fw',
                                'text' => '+1(555)-555-555',
                                'label' => 'Contact Item 1'
                            ],
                            1 => [
                                'icon' => 'fa fa-map-marker fa-fw',
                                'text' => '555 RocketTheme Blvd, CO',
                                'label' => 'Contact Item 2'
                            ],
                            2 => [
                                'icon' => 'fa fa-envelope fa-fw',
                                'text' => 'hello@no-reply.com',
                                'label' => 'Contact Item 3'
                            ],
                            3 => [
                                'icon' => 'fa fa-comment fa-fw',
                                'text' => '<a href="#">Live Chat with Us</a>',
                                'label' => 'Contact Item 4'
                            ]
                        ]
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'custom-2068' => [
                    'title' => 'Content - Contact',
                    'attributes' => [
                        'html' => '<div class="g-layercontent nopaddingall">
	<h1 class="g-layercontent-title g-uppercase">Particle</h1>
	<h3 class="g-layercontent-title g-uppercase">Contact</h3>
	<p class="g-layercontent-promotext nomarginall">The Contact particle provides an efficient mechanism of providing a contact list. Items are created via the collection list interface allowing you to configure each entry with individual icons and values.</p>

	<a href="http://docs.gantry.org/gantry5/particles" class="button button-3"> Learn More</a>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'custom-4935' => [
                    'title' => 'Button Variations Header',
                    'attributes' => [
                        'html' => '<div class="g-layercontent">
    <h1 class="g-layercontent-title">Typography</h1>
    <h5 class="g-layercontent-subtitle nomarginall">A selection of custom <strong>Button Variations</strong> available with Callisto</h5>

    <a href="http://docs.gantry.org/gantry5/particles" class="button button-2"> Learn More</a>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'flush center',
                        'variations' => 'box1'
                    ]
                ],
                'custom-6880' => [
                    'title' => 'Button Variations Content',
                    'attributes' => [
                        'html' => '<h3>Buttons: 1-4</h3>
<p>
<a href="#" class="button">Default Button</a>
<a href="#" class="button button-2">Button 2</a>
<a href="#" class="button button-3">Button 3</a>
<a href="#" class="button button-4">Button 4</a>
</p>
<pre class="prettyprint">
&lt;a href=&quot;#&quot; class=&quot;button&quot;&gt;Default Button&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-2&quot;&gt;Button 2&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-3&quot;&gt;Button 3&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-4&quot;&gt;Button 4&lt;/a&gt;
</pre>
<h3>Buttons: Color</h3>
<p>
<a href="#" class="button button-grey">Button Grey</a>
<a href="#" class="button button-pink">Button Pink</a>
<a href="#" class="button button-red">Button Red</a>
<a href="#" class="button button-purple">Button Purple</a>
<a href="#" class="button button-orange">Button Orange</a>
<a href="#" class="button button-blue">Button Blue</a>
</p>
<pre class="prettyprint">
&lt;a href=&quot;#&quot; class=&quot;button button-grey&quot;&gt;Button Grey&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-pink&quot;&gt;Button Pink&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-red&quot;&gt;Button Red&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-purple&quot;&gt;Button Purple&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-orange&quot;&gt;Button Orange&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-blue&quot;&gt;Button Blue&lt;/a&gt;
</pre>
<h3>Buttons: Color Compounded with Buttons 2-4</h3>
<p>
<a href="#" class="button button-2 button-grey">Button Grey</a>
<a href="#" class="button button-3 button-pink">Button Pink</a>
<a href="#" class="button button-4 button-red">Button Red</a>
<a href="#" class="button button-2 button-purple">Button Purple</a>
<a href="#" class="button button-3 button-orange">Button Orange</a>
<a href="#" class="button button-4 button-blue">Button Blue</a>
</p>
<pre class="prettyprint">
&lt;a href=&quot;#&quot; class=&quot;button button-2 button-grey&quot;&gt;Button Grey&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-3 button-pink&quot;&gt;Button Pink&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-4 button-red&quot;&gt;Button Red&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-2 button-purple&quot;&gt;Button Purple&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-3 button-orange&quot;&gt;Button Orange&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-4 button-blue&quot;&gt;Button Blue&lt;/a&gt;
</pre>
<h3>Buttons: Square</h3>
<p>
<a href="#" class="button button-square">Default Button</a>
<a href="#" class="button button-square button-3">Button 3</a>
<a href="#" class="button button-square button-2 button-grey">Button Grey</a>
<a href="#" class="button button-square button-3 button-pink">Button Pink</a>
<a href="#" class="button button-square button-2 button-red">Button Red</a>
<a href="#" class="button button-square button-3 button-purple">Button Purple</a>
<a href="#" class="button button-square button-2 button-orange">Button Orange</a>
<a href="#" class="button button-square button-3 button-blue">Button Blue</a>
</p>
<pre class="prettyprint">
&lt;a href=&quot;#&quot; class=&quot;button button-square&quot;&gt;Default Button&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-square button-3&quot;&gt;Button 3&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-square button-2 button-grey&quot;&gt;Button Grey&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-square button-3 button-pink&quot;&gt;Button Pink&lt;/a&gt;
</pre>
<h3>Buttons: Icons</h3>
<p>
<a href="#" class="button"><i class="fa fa-fw fa-download"></i> Default Button</a>
<a href="#" class="button button-2"><i class="fa fa-fw fa-cloud"></i> Button 2</a>
<a href="#" class="button button-3"><i class="fa fa-fw fa-star"></i> Button 3</a>
<a href="#" class="button button-4"><i class="fa fa-fw fa-car"></i> Button 4</a>
<a href="#" class="button button-grey"><i class="fa fa-fw fa-tag"></i> Button Grey</a>
<a href="#" class="button button-pink"><i class="fa fa-fw fa-university"></i> Button Pink</a>
<a href="#" class="button button-red"><i class="fa fa-fw fa-edit"></i> Button Red</a>
<a href="#" class="button button-purple"><i class="fa fa-fw fa-lock"></i> Button Purple</a>
<a href="#" class="button button-orange"><i class="fa fa-fw fa-clock-o"></i> Button Orange</a>
<a href="#" class="button button-blue"><i class="fa fa-fw fa-arrows"></i> Button Blue</a>
</p>
<pre class="prettyprint">
&lt;a href=&quot;#&quot; class=&quot;button&quot;&gt;&lt;i class=&quot;fa fa-fw fa-download&quot;&gt;&lt;/i&gt; Default Button&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-2&quot;&gt;&lt;i class=&quot;fa fa-fw fa-cloud&quot;&gt;&lt;/i&gt; Button 2&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-3&quot;&gt;&lt;i class=&quot;fa fa-fw fa-star&quot;&gt;&lt;/i&gt; Button 3&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-4&quot;&gt;&lt;i class=&quot;fa fa-fw fa-car&quot;&gt;&lt;/i&gt; Button 4&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-grey&quot;&gt;&lt;i class=&quot;fa fa-fw fa-tag&quot;&gt;&lt;/i&gt; Button Grey&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-pink&quot;&gt;&lt;i class=&quot;fa fa-fw fa-university&quot;&gt;&lt;/i&gt; Button Pink&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-red&quot;&gt;&lt;i class=&quot;fa fa-fw fa-edit&quot;&gt;&lt;/i&gt; Button Red&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-purple&quot;&gt;&lt;i class=&quot;fa fa-fw fa-lock&quot;&gt;&lt;/i&gt; Button Purple&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-orange&quot;&gt;&lt;i class=&quot;fa fa-fw fa-clock-o&quot;&gt;&lt;/i&gt; Button Orange&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-blue&quot;&gt;&lt;i class=&quot;fa fa-fw fa-arrows&quot;&gt;&lt;/i&gt; Button Blue&lt;/a&gt;
</pre>
<h3>Buttons: Icons Only</h3>
<p>
<a href="#" class="button"><i class="fa fa-fw fa-download"></i></a>
<a href="#" class="button button-2"><i class="fa fa-fw fa-cloud"></i></a>
<a href="#" class="button button-3"><i class="fa fa-fw fa-star"></i></a>
<a href="#" class="button button-4"><i class="fa fa-fw fa-car"></i></a>
<a href="#" class="button button-grey"><i class="fa fa-fw fa-tag"></i></a>
<a href="#" class="button button-pink"><i class="fa fa-fw fa-university"></i></a>
<a href="#" class="button button-red"><i class="fa fa-fw fa-edit"></i></a>
<a href="#" class="button button-purple"><i class="fa fa-fw fa-lock"></i></a>
<a href="#" class="button button-orange"><i class="fa fa-fw fa-clock-o"></i></a>
<a href="#" class="button button-blue"><i class="fa fa-fw fa-arrows"></i></a>
</p>
<pre class="prettyprint">
&lt;a href=&quot;#&quot; class=&quot;button&quot;&gt;&lt;i class=&quot;fa fa-fw fa-download&quot;&gt;&lt;/i&gt;&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-2&quot;&gt;&lt;i class=&quot;fa fa-fw fa-cloud&quot;&gt;&lt;/i&gt;&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-3&quot;&gt;&lt;i class=&quot;fa fa-fw fa-star&quot;&gt;&lt;/i&gt;&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-4&quot;&gt;&lt;i class=&quot;fa fa-fw fa-car&quot;&gt;&lt;/i&gt;&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-grey&quot;&gt;&lt;i class=&quot;fa fa-fw fa-tag&quot;&gt;&lt;/i&gt;&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-pink&quot;&gt;&lt;i class=&quot;fa fa-fw fa-university&quot;&gt;&lt;/i&gt;&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-red&quot;&gt;&lt;i class=&quot;fa fa-fw fa-edit&quot;&gt;&lt;/i&gt;&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-purple&quot;&gt;&lt;i class=&quot;fa fa-fw fa-lock&quot;&gt;&lt;/i&gt;&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-orange&quot;&gt;&lt;i class=&quot;fa fa-fw fa-clock-o&quot;&gt;&lt;/i&gt;&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-blue&quot;&gt;&lt;i class=&quot;fa fa-fw fa-arrows&quot;&gt;&lt;/i&gt;&lt;/a&gt;
</pre>
<h3>Buttons: Sizes</h3>
<p>
<a href="#" class="button button-xlarge button-grey">Button XLarge</a>
<a href="#" class="button button-large button-pink">Button Large</a>
<a href="#" class="button button-medium button-red">Button Medium</a>
<a href="#" class="button button-small button-purple">Button Small</a>
<a href="#" class="button button-xsmall button-orange">Button XSmall</a>
</p>
<pre class="prettyprint">
&lt;a href=&quot;#&quot; class=&quot;button button-xlarge button-grey&quot;&gt;Button XLarge&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-large button-pink&quot;&gt;Button Large&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-medium button-red&quot;&gt;Button Medium&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-small button-purple&quot;&gt;Button Small&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-xsmall button-orange&quot;&gt;Button XSmall&lt;/a&gt;
</pre>
<h3>Buttons: Block</h3>
<p>
<a href="#" class="button button-block">Default Button</a>
<a href="#" class="button button-block button-2">Button 2</a>
<a href="#" class="button button-block button-3">Button 3</a>
<a href="#" class="button button-block button-4">Button 4</a>
<a href="#" class="button button-block button-grey">Button Grey</a>
</p>
<pre class="prettyprint">
&lt;a href=&quot;#&quot; class=&quot;button button-block&quot;&gt;Default Button&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-block button-2&quot;&gt;Button 2&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-block button-3&quot;&gt;Button 3&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-block button-4&quot;&gt;Button 4&lt;/a&gt;
&lt;a href=&quot;#&quot; class=&quot;button button-block button-grey&quot;&gt;Button Grey&lt;/a&gt;
</pre>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box2 g-outer-box'
                    ]
                ],
                'custom-1330' => [
                    'title' => 'Standard Typography Header',
                    'attributes' => [
                        'html' => '<div class="g-layercontent">
    <h1 class="g-layercontent-title">Typography</h1>
    <h5 class="g-layercontent-subtitle nomarginall">An example of  <strong>Standard Typography</strong> available with Callisto</h5>

    <a href="http://docs.gantry.org/gantry5/particles" class="button button-3"> Learn More</a>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box4 g-outer-box center'
                    ]
                ],
                'custom-4033' => [
                    'title' => 'Standard Typography',
                    'attributes' => [
                        'html' => '<div class="g-grid">
    <div class="g-block">
        <div class="g-content">
            <h2>Headings</h2>
            <p>All HTML headings, <code>&lt;h1&gt;</code> through <code>&lt;h6&gt;</code> are available.</p>
            <div>
                <h1>h1. Heading 1</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet nibh. Vivamus non arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <h2>h2. Heading 2</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet nibh. Vivamus non arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <h3>h3. Heading 3</h3>
				<p class="visible-large">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet nibh. Vivamus non arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <h4>h4. Heading 4</h4>
                <h5>h5. Heading 5</h5>
                <h6>h6. Heading 6</h6>
            </div>
        </div>
    </div>

    <div class="g-block">
        <div class="g-content">
            <h2>Emphasis</h2>
            <h4><code>&lt;em&gt;</code></h4>
            <p>For emphasizing a snippet of text with <em>stress</em></p>
            <p>The following snippet of text is <em>rendered as italicized text</em>.</p>
<pre class="prettyprint">&lt;em&gt;rendered as italicized text&lt;/em&gt;</pre>
            <h4><code>&lt;strong&gt;</code></h4>
            <p>For emphasizing a snippet of text with <strong>important</strong></p>
            <p>The following snippet of text is <strong>rendered as bold text</strong>.</p>
<pre class="prettyprint">&lt;strong&gt;rendered as bold text&lt;/strong&gt;</pre>
            <h4><code>&lt;small&gt;</code></h4>
            <p>For de-emphasizing inline or blocks of text, <small>use the small tag.</small></p>
            <p><small>This line of text is meant to be treated as fine print.</small></p>
        </div>
    </div>
</div>

<div class="g-grid">
    <div class="g-block">
        <div class="g-content nomargintop nopaddingtop">
            <h2 class="nomargintop">Notice Styles</h3>
            <p class="alert alert-success">Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
<pre class="prettyprint">&lt;p class="alert alert-success"&gt;...&lt;/p&gt;</pre>
            <p class="alert alert-info">Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
<pre class="prettyprint">&lt;p class="alert alert-info"&gt;...&lt;/p&gt;</pre>
            <p class="alert alert-warning">Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            <pre class="prettyprint">&lt;p class="alert alert-warning"&gt;...&lt;/p&gt;</pre>
            <p class="alert alert-error">Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
<pre class="prettyprint">&lt;p class="alert alert-error"&gt;...&lt;/p&gt;</pre>
        </div>
    </div>
</div>

<div class="g-grid">
    <div class="g-block">
        <div class="g-content nomargintop nopaddingtop">
            <h2 class="nomargintop">Blockquotes</h2>
            <p>For quoting blocks of content from another source within your document.</p>
            <h3>Default blockquote</h3>
            <p>Wrap <code>&lt;blockquote&gt;</code> around any <abbr title="HyperText Markup Language">HTML</abbr> as the quote. For straight quotes we recommend a <code>&lt;p&gt;</code>.</p>
            <blockquote>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
            </blockquote>
<pre class="prettyprint">&lt;blockquote&gt;
&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.&lt;/p&gt;
&lt;/blockquote&gt;</pre>

            <h3>Blockquote options</h3>
            <span>Style and content changes for simple variations on a standard blockquote.</span><br/>

            <h4>Naming a source</h4>
            <p>Add <code>&lt;small&gt;</code> tag for identifying the source. Wrap the name of the source work in <code>&lt;cite&gt;</code>.</p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                </blockquote>
<pre class="prettyprint">&lt;blockquote&gt;
&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.&lt;/p&gt;
&lt;small&gt;Someone famous &lt;cite title="Source Title"&gt;Source Title&lt;/cite&gt;&lt;/small&gt;
&lt;/blockquote&gt;</pre>
        </div>
    </div>
</div>

<div class="g-grid">
    <div class="g-block">
        <div class="g-content nomargintop nopaddingtop">
            <h2 class="nomargintop">Code</h2>
            <h4>Inline</h2>
            <p>Wrap inline snippets of code with <code>&lt;code&gt;</code>.</p>
            For example, <code>&lt;section&gt;</code> should be wrapped as inline.
<pre class="prettyprint">For example, &lt;code&gt;&lt;section&gt;&lt;/code&gt; should be wrapped as inline.</pre>
            <p><strong>Note:</strong> Be sure to keep code within <code>&lt;pre&gt;</code> tags as close to the left as possible; it will render all tabs.</p>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content nomargintop nopaddingtop">
            <h2 class="nomargintop">&nbsp;</h2>
            <h4>Basic block</h2>
                <p>Use <code>&lt;pre&gt;</code> for multiple lines of code. Be sure to escape any angle brackets in the code for proper rendering.</p>
<pre class="prettyprint">&lt;p&gt;Sample text here...&lt;/p&gt;</pre>
<pre class="prettyprint">&lt;pre&gt;
&amp;lt;p&amp;gt;Sample text here...&amp;lt;/p&amp;gt;
&lt;/pre&gt;</pre>
        </div>
    </div>
</div>

<div class="g-grid">
    <div class="g-block">
        <div class="g-content nomargintop nopaddingtop">
            <h2 class="nomargintop">Tables</h2>
            <table>
                <thead><tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                </tr></thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>@twitter</td>
                </tr>
                </tbody>
            </table>
<pre class="prettyprint">&lt;table class="table"&gt;
…
&lt;/table&gt;</pre>
        </div>
    </div>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'flush',
                        'variations' => 'box2'
                    ]
                ],
                'copyright-4817' => [
                    'attributes' => [
                        'date' => [
                            'start' => '2007'
                        ],
                        'owner' => 'RocketTheme, LLC'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'center'
                    ]
                ],
                'mobile-menu-8246' => [
                    'title' => 'Mobile Menu',
                    'layout' => false
                ]
            ]
        ],
        'page' => [
            'head' => [
                'atoms' => [
                    0 => [
                        'title' => 'Prettify JS/CSS',
                        'type' => 'assets',
                        'attributes' => [
                            'enabled' => 1,
                            'css' => [
                                
                            ],
                            'javascript' => [
                                0 => [
                                    'location' => 'gantry-theme://external/prettify/run_prettify.js',
                                    'extra' => [
                                        
                                    ],
                                    'name' => 'Prettify'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];
