<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1490103215,
    'checksum' => '729bfaa648a6a56e6ccb7e7c51e8e1bd',
    'files' => [
        'templates/rt_callisto/custom/config/82' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/82/assignments.yaml',
                'modified' => 1489746919
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/82/index.yaml',
                'modified' => 1490101304
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/82/layout.yaml',
                'modified' => 1490101304
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/82/page/head.yaml',
                'modified' => 1489746631
            ],
            'styles' => [
                'file' => 'templates/rt_callisto/custom/config/82/styles.yaml',
                'modified' => 1489746947
            ]
        ]
    ],
    'data' => [
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ],
        'index' => [
            'name' => '82',
            'timestamp' => 1490101304,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'pages_-_clients',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-4920' => 'Logo'
                ],
                'module' => [
                    'position-module-1922' => 'Module Instance',
                    'position-module-8126' => 'Module Instance',
                    'position-module-2057' => 'Module Instance',
                    'position-module-3373' => 'Module Instance'
                ],
                'menu' => [
                    'menu-8752' => 'Menu'
                ],
                'content' => [
                    'system-content-8183' => 'Page Content'
                ],
                'date' => [
                    'date-8645' => 'Date'
                ],
                'infolist' => [
                    'infolist-1186' => 'Info List'
                ],
                'mobile-menu' => [
                    'mobile-menu-8532' => 'Mobile Menu'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'pages_-_clients',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-4920 80',
                        1 => 'position-module-1922 20'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-8752'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'position-module-8126 33.3',
                        1 => 'position-module-2057 33.3',
                        2 => 'position-module-3373 33.3'
                    ]
                ],
                '/container-8748/' => [
                    0 => [
                        0 => [
                            'mainbar 67' => [
                                0 => [
                                    0 => 'system-content-8183'
                                ]
                            ]
                        ],
                        1 => [
                            'sidebar 33' => [
                                
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'date-8645'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'infolist-1186'
                    ]
                ],
                '/copyright/' => [
                    
                ],
                'offcanvas' => [
                    0 => [
                        0 => 'mobile-menu-8532'
                    ]
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section'
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-8748' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'logo-4920' => [
                    'attributes' => [
                        'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                    ],
                    'block' => [
                        'class' => 'g-logo-block'
                    ]
                ],
                'position-module-1922' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '97',
                        'key' => 'module-instance'
                    ]
                ],
                'menu-8752' => [
                    'block' => [
                        'class' => 'g-menu-block'
                    ]
                ],
                'position-module-8126' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '103',
                        'key' => 'module-instance'
                    ],
                    'block' => [
                        'variations' => 'box-blue'
                    ]
                ],
                'position-module-2057' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '123',
                        'key' => 'module-instance'
                    ],
                    'block' => [
                        'variations' => 'box-blue'
                    ]
                ],
                'position-module-3373' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '127'
                    ],
                    'block' => [
                        'variations' => 'box-blue'
                    ]
                ],
                'infolist-1186' => [
                    'title' => 'Info List'
                ],
                'mobile-menu-8532' => [
                    'title' => 'Mobile Menu'
                ]
            ]
        ],
        'page' => [
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'styles' => [
            'preset' => 'preset7',
            'base' => [
                'background' => '#ffffff'
            ],
            'accent' => [
                'color-1' => '#1abc9c',
                'color-2' => '#007e65'
            ],
            'header' => [
                'background' => '#1abc9c',
                'text-color' => '#138871'
            ],
            'navigation' => [
                'background' => '#ffffff',
                'text-color' => '#888888'
            ],
            'showcase' => [
                'background' => '#e8e8e8',
                'text-color' => '#878787'
            ],
            'feature' => [
                'background' => '#f5f5f5'
            ],
            'main' => [
                'background' => '#f5f5f5'
            ],
            'footer' => [
                'background' => '#ffffff',
                'text-color' => '#888888'
            ],
            'copyright' => [
                'background' => '#f5f5f5'
            ],
            'extension' => [
                'background' => '#f5f5f5'
            ],
            'offcanvas' => [
                'background' => '#f0f0f0',
                'text-color' => '#888888'
            ]
        ]
    ]
];
