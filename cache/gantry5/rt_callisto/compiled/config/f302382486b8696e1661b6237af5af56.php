<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1489670763,
    'checksum' => 'edca08f2239f119dfcbfb3b8c1e82665',
    'files' => [
        'templates/rt_callisto/custom/config/71' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/71/assignments.yaml',
                'modified' => 1488889112
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/71/index.yaml',
                'modified' => 1488888443
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/71/layout.yaml',
                'modified' => 1488888443
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/71/page/head.yaml',
                'modified' => 1488888443
            ]
        ],
        'templates/rt_callisto/custom/config/default' => [
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/default/index.yaml',
                'modified' => 1488888442
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/default/layout.yaml',
                'modified' => 1488888442
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/default/page/head.yaml',
                'modified' => 1488888442
            ]
        ],
        'templates/rt_callisto/config/default' => [
            'particles/iconmenu' => [
                'file' => 'templates/rt_callisto/config/default/particles/iconmenu.yaml',
                'modified' => 1463470148
            ],
            'particles/logo' => [
                'file' => 'templates/rt_callisto/config/default/particles/logo.yaml',
                'modified' => 1463470148
            ],
            'particles/social' => [
                'file' => 'templates/rt_callisto/config/default/particles/social.yaml',
                'modified' => 1463470148
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'css' => [
                    'class' => 'branding'
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'date' => [
                    'start' => 'now',
                    'end' => 'now'
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => true,
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'link' => true,
                'url' => '',
                'image' => 'gantry-theme://images/logo/callisto.png',
                'text' => 'Callisto',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => true,
                'menu' => '',
                'base' => '/',
                'startLevel' => 1,
                'maxLevels' => 0,
                'renderTitles' => 0,
                'hoverExpand' => 1,
                'mobileTarget' => 0
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'social-items'
                ],
                'target' => '_blank',
                'display' => 'both',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-twitter fa-fw',
                        'text' => '',
                        'link' => 'http://twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ],
                    1 => [
                        'icon' => 'fa fa-facebook fa-fw',
                        'text' => '',
                        'link' => 'http://facebook.com/rockettheme',
                        'name' => 'Facebook'
                    ],
                    2 => [
                        'icon' => 'fa fa-google fa-fw',
                        'text' => '',
                        'link' => 'http://plus.google.com/+rockettheme',
                        'name' => 'Google'
                    ],
                    3 => [
                        'icon' => 'fa fa-rss fa-fw',
                        'text' => '',
                        'link' => 'http://www.rockettheme.com/product-updates?rss',
                        'name' => 'RSS'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'css' => [
                    'class' => 'totop'
                ]
            ],
            'contact' => [
                'enabled' => true
            ],
            'contentlist' => [
                'enabled' => true,
                'cols' => 'g-listgrid-4cols'
            ],
            'iconmenu' => [
                'enabled' => '1',
                'target' => '_parent',
                'class' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-diamond',
                        'text' => 'Features',
                        'link' => 'http://docs.gantry.org/gantry5/basics/key-features',
                        'name' => 'Features'
                    ],
                    1 => [
                        'icon' => 'fa fa-rocket',
                        'text' => 'Gantry 5',
                        'link' => 'http://www.gantry.org/',
                        'name' => 'Gantry 5'
                    ],
                    2 => [
                        'icon' => 'fa fa-gear',
                        'text' => 'Addons',
                        'link' => 'http://docs.gantry.org/gantry5/particles',
                        'name' => 'Addons'
                    ],
                    3 => [
                        'icon' => 'fa fa-cloud-download',
                        'text' => 'Download',
                        'link' => 'http://www.rockettheme.com/joomla/templates/callisto',
                        'name' => 'Download'
                    ]
                ]
            ],
            'imagegrid' => [
                'enabled' => true,
                'cols' => 'g-imagegrid-2cols'
            ],
            'infolist' => [
                'enabled' => true
            ],
            'newsletter' => [
                'enabled' => true
            ],
            'promoimage' => [
                'enabled' => true
            ],
            'simplecounter' => [
                'enabled' => true,
                'month' => 0
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true,
                'priority' => 0,
                'in_footer' => false
            ],
            'content' => [
                'enabled' => true
            ],
            'contentarray' => [
                'enabled' => true,
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show'
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show'
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ]
                ]
            ],
            'date' => [
                'enabled' => true,
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => true
            ],
            'module' => [
                'enabled' => true
            ],
            'position' => [
                'enabled' => true
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#ecbc2e',
                'color-2' => '#8b5717'
            ],
            'base' => [
                'background' => '#1f1f1f',
                'text-color' => '#888888'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '48rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '48rem'
            ],
            'copyright' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'extension' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'feature' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'font' => [
                'family-default' => 'sourcesanspro, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-title' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-promo' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif'
            ],
            'footer' => [
                'background' => '#1f1f1f',
                'text-color' => '#878787'
            ],
            'header' => [
                'background' => '#ecbc2e',
                'text-color' => '#b08523'
            ],
            'main' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'menu' => [
                'col-width' => '180px',
                'animation' => 'g-fade'
            ],
            'navigation' => [
                'background' => '#353535',
                'text-color' => '#828282'
            ],
            'offcanvas' => [
                'background' => '#1b1b1f',
                'text-color' => '#ffffff',
                'width' => '17rem',
                'toggle-color' => '#ffffff',
                'toggle-visibility' => 1
            ],
            'showcase' => [
                'background' => '#23262f',
                'text-color' => '#ffffff'
            ]
        ],
        'page' => [
            'assets' => [
                'priority' => 0,
                'in_footer' => false
            ],
            'body' => [
                'attribs' => [
                    'class' => 'gantry'
                ],
                'layout' => [
                    'sections' => 0
                ]
            ],
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'index' => [
            'name' => 71,
            'timestamp' => 1488888443,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/2-col-right.png',
                'name' => 'pages_-_about_us',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-6772' => 'System Messages'
                ],
                'logo' => [
                    'logo-6361' => 'Logo'
                ],
                'iconmenu' => [
                    'iconmenu-9377' => 'Icon Menu'
                ],
                'menu' => [
                    'menu-7662' => 'Menu'
                ],
                'social' => [
                    'social-5673' => 'Social'
                ],
                'custom' => [
                    'custom-1928' => 'About Us',
                    'custom-6040' => 'Our Mission',
                    'custom-9443' => 'Our Values',
                    'custom-4229' => 'Our Solutions',
                    'custom-4124' => 'Why You Should Join Us',
                    'custom-6840' => 'Introduction - Mission - Business',
                    'custom-8135' => 'Sophisticated',
                    'custom-2795' => 'Responsive',
                    'custom-7698' => 'Powerful',
                    'custom-9206' => 'We Always Try to Create a Difference',
                    'custom-3558' => 'About Callisto',
                    'custom-3447' => 'Sample Sitemap'
                ],
                'infolist' => [
                    'infolist-8619' => 'Modern Design',
                    'infolist-9622' => 'Huge Features',
                    'infolist-8969' => 'Multi Purpose'
                ],
                'newsletter' => [
                    'newsletter-4059' => 'Newsletter'
                ],
                'copyright' => [
                    'copyright-2967' => 'Copyright'
                ],
                'mobile-menu' => [
                    'mobile-menu-9712' => 'Mobile Menu'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/2-col-right.png',
                'name' => 'pages_-_about_us',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'system-messages-6772'
                    ],
                    1 => [
                        0 => 'logo-6361 56',
                        1 => 'iconmenu-9377 44'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-7662 80',
                        1 => 'social-5673 20'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'custom-1928'
                    ]
                ],
                '/container-2905/' => [
                    0 => [
                        0 => [
                            'mainbar 67' => [
                                0 => [
                                    0 => 'custom-6040 33.3333',
                                    1 => 'custom-9443 33.3333',
                                    2 => 'custom-4229 33.3333'
                                ],
                                1 => [
                                    0 => 'custom-4124'
                                ],
                                2 => [
                                    0 => 'custom-6840'
                                ],
                                3 => [
                                    0 => 'custom-8135 33.3333',
                                    1 => 'custom-2795 33.3333',
                                    2 => 'custom-7698 33.3333'
                                ],
                                4 => [
                                    0 => 'infolist-8619 33.3333',
                                    1 => 'infolist-9622 33.3333',
                                    2 => 'infolist-8969 33.3333'
                                ]
                            ]
                        ],
                        1 => [
                            'sidebar 33' => [
                                
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'custom-9206'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'custom-3558 33.3333',
                        1 => 'newsletter-4059 33.3333999999',
                        2 => 'custom-3447 33.3333'
                    ]
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'copyright-2967'
                    ]
                ],
                'offcanvas' => [
                    0 => [
                        0 => 'mobile-menu-9712'
                    ]
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section'
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-2905' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'system-messages-6772' => [
                    'layout' => false
                ],
                'logo-6361' => [
                    'layout' => false,
                    'block' => [
                        'class' => 'g-logo-block'
                    ]
                ],
                'iconmenu-9377' => [
                    'title' => 'Icon Menu',
                    'layout' => false,
                    'block' => [
                        'class' => 'flush'
                    ]
                ],
                'menu-7662' => [
                    'layout' => false,
                    'block' => [
                        'class' => 'g-menu-block'
                    ]
                ],
                'social-5673' => [
                    'layout' => false
                ],
                'custom-1928' => [
                    'title' => 'About Us',
                    'attributes' => [
                        'html' => '<div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">About Us</h2>
	<div class="g-layercontent-subtitle">Who We Are</div>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'flush center'
                    ]
                ],
                'custom-6040' => [
                    'title' => 'Our Mission',
                    'attributes' => [
                        'html' => '<h2 class="g-title">Our Mission</h2>
<p>Objectively innovate empowered manufactured products whereas parallel platforms. Holistically predominate extensible testing procedures for reliable supply chains.</p>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'center nomarginall',
                        'variations' => 'box-blue'
                    ]
                ],
                'custom-9443' => [
                    'title' => 'Our Values',
                    'attributes' => [
                        'html' => '<h2 class="g-title">Our Values</h2>
<p>Objectively innovate empowered manufactured products whereas parallel platforms. Holistically predominate extensible testing procedures for reliable supply chains.</p>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'center nomarginall',
                        'variations' => 'box1'
                    ]
                ],
                'custom-4229' => [
                    'title' => 'Our Solutions',
                    'attributes' => [
                        'html' => '<h2 class="g-title">Our Solution</h2>
<p>Objectively innovate empowered manufactured products whereas parallel platforms. Holistically predominate extensible testing procedures for reliable supply chains.</p>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'center nomarginall',
                        'variations' => 'box-orange'
                    ]
                ],
                'custom-4124' => [
                    'title' => 'Why You Should Join Us',
                    'attributes' => [
                        'html' => '<div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Why You Should Join Us</h2>
	<div class="g-layercontent-subtitle">Choose the theme that suits your needs. 100% satisfaction guaranteed.</div>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'flush center',
                        'variations' => 'box2'
                    ]
                ],
                'custom-6840' => [
                    'title' => 'Introduction - Mission - Business',
                    'attributes' => [
                        'html' => '<div class="g-grid center">
    <div class="g-block">
        <div class="g-content">
          <img src="gantry-theme://images/demo/pages/pages/about-us/img-01.jpg" alt="image">
          <h2 class="g-title">Introduction</h2>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
          <img src="gantry-theme://images/demo/pages/pages/about-us/img-02.jpg" alt="image">
          <h2 class="g-title">Mission</h2>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
          <img src="gantry-theme://images/demo/pages/pages/about-us/img-03.jpg" alt="image">
          <h2 class="g-title">Business</h2>
        </div>
    </div>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'flush',
                        'variations' => 'box2'
                    ]
                ],
                'custom-8135' => [
                    'title' => 'Sophisticated',
                    'attributes' => [
                        'html' => '<h2 class="g-title"><span class="fa fa-dashboard fa-fw fa-3x"></span> Sophisticated</h2>
<p>Dynamically procrastinate B2C users after installed base benefits.</p>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box3'
                    ]
                ],
                'custom-2795' => [
                    'title' => 'Responsive',
                    'attributes' => [
                        'html' => '<h2 class="g-title"><span class="fa fa-arrows-alt fa-fw fa-3x"></span> Responsive</h2>
<p>Dynamically procrastinate B2C users after installed base benefits.</p>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box3'
                    ]
                ],
                'custom-7698' => [
                    'title' => 'Powerful',
                    'attributes' => [
                        'html' => '<h2 class="g-title"><span class="fa fa-sliders fa-fw fa-3x"></span> Powerful</h2>
<p>Dynamically procrastinate B2C users after installed base benefits.</p>'
                    ],
                    'layout' => false,
                    'block' => [
                        'variations' => 'box3'
                    ]
                ],
                'infolist-8619' => [
                    'title' => 'Modern Design',
                    'attributes' => [
                        'infolists' => [
                            0 => [
                                'link' => '#',
                                'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                                'title' => 'Modern Design'
                            ],
                            1 => [
                                'link' => '#',
                                'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                                'title' => 'Awesome Support'
                            ]
                        ]
                    ],
                    'layout' => false
                ],
                'infolist-9622' => [
                    'title' => 'Huge Features',
                    'attributes' => [
                        'infolists' => [
                            0 => [
                                'link' => '#',
                                'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                                'title' => 'Huge Features'
                            ],
                            1 => [
                                'link' => '#',
                                'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                                'title' => 'Demo Content'
                            ]
                        ]
                    ],
                    'layout' => false
                ],
                'infolist-8969' => [
                    'title' => 'Multi Purpose',
                    'attributes' => [
                        'infolists' => [
                            0 => [
                                'link' => '#',
                                'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                                'title' => 'Multi Purpose'
                            ],
                            1 => [
                                'link' => '#',
                                'description' => 'Collaboratively administrate empowered markets via plug-and-play networks',
                                'title' => 'Gantry 5'
                            ]
                        ]
                    ],
                    'layout' => false
                ],
                'custom-9206' => [
                    'title' => 'We Always Try to Create a Difference',
                    'attributes' => [
                        'html' => '<div class="g-layercontent">
	<h2 class="g-layercontent-title">We Always Try to Create a Difference</h2>
	<div class="g-layercontent-subtitle">Versatile and Flexible Features Powered by the Gantry Framework.</div>
	<a href="http://www.rockettheme.com/joomla/templates/callisto" class="button button-2">Download Callisto</a>
</div>'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'flush center',
                        'variations' => 'box1'
                    ]
                ],
                'custom-3558' => [
                    'title' => 'About Callisto',
                    'attributes' => [
                        'html' => '<h2 class="g-title">About Callisto</h2>

<p>All demo content is for sample purposes only, intended to represent a live site.</p>

<p>The sample pages are intended to show how Callisto can be constructed on your site.</p>'
                    ],
                    'layout' => false
                ],
                'newsletter-4059' => [
                    'attributes' => [
                        'title' => 'Newsletter',
                        'headtext' => 'Subscribe to our newsletter and stay updated on the latest developments and special offers!',
                        'inputboxtext' => 'Email Address',
                        'buttontext' => 'Join',
                        'uri' => 'rocketthemeblog',
                        'buttonclass' => 'button button-3'
                    ],
                    'layout' => false
                ],
                'custom-3447' => [
                    'title' => 'Sample Sitemap',
                    'attributes' => [
                        'html' => '<h2 class="g-title">Sample Sitemap</h2>

<div class="g-grid">
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Home</a></li>
			<li><a href="#">Features</a></li>
			<li><a href="#">Typography</a></li>
			<li><a href="#">Particles</a></li>
			<li><a href="#">Variations</a></li>
		</ul>		
	</div>
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Buttons</a></li>
			<li><a href="#">Pages</a></li>
			<li><a href="#">Guide</a></li>
			<li><a href="#">Support</a></li>
			<li><a href="#">Download</a></li>
		</ul>		
	</div>	
</div>'
                    ],
                    'layout' => false
                ],
                'copyright-2967' => [
                    'attributes' => [
                        'date' => [
                            'start' => '2007'
                        ],
                        'owner' => 'RocketTheme, LLC'
                    ],
                    'layout' => false,
                    'block' => [
                        'class' => 'center'
                    ]
                ],
                'mobile-menu-9712' => [
                    'title' => 'Mobile Menu',
                    'layout' => false
                ]
            ]
        ],
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ]
    ]
];
