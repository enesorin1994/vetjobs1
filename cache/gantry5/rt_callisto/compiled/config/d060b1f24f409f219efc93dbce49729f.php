<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1490784969,
    'checksum' => 'b7071c47307a90e58058df0c66f59da1',
    'files' => [
        'templates/rt_callisto/custom/config/66' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/66/assignments.yaml',
                'modified' => 1488965847
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/66/index.yaml',
                'modified' => 1490784938
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/66/layout.yaml',
                'modified' => 1490784938
            ],
            'page/assets' => [
                'file' => 'templates/rt_callisto/custom/config/66/page/assets.yaml',
                'modified' => 1489143062
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/66/page/head.yaml',
                'modified' => 1489143062
            ],
            'styles' => [
                'file' => 'templates/rt_callisto/custom/config/66/styles.yaml',
                'modified' => 1490277393
            ]
        ],
        'templates/rt_callisto/custom/config/default' => [
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/default/index.yaml',
                'modified' => 1488888442
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/default/layout.yaml',
                'modified' => 1488888442
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/default/page/head.yaml',
                'modified' => 1488888442
            ]
        ],
        'templates/rt_callisto/config/default' => [
            'particles/iconmenu' => [
                'file' => 'templates/rt_callisto/config/default/particles/iconmenu.yaml',
                'modified' => 1463470148
            ],
            'particles/logo' => [
                'file' => 'templates/rt_callisto/config/default/particles/logo.yaml',
                'modified' => 1463470148
            ],
            'particles/social' => [
                'file' => 'templates/rt_callisto/config/default/particles/social.yaml',
                'modified' => 1463470148
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'css' => [
                    'class' => 'branding'
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'date' => [
                    'start' => 'now',
                    'end' => 'now'
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => true,
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'link' => true,
                'url' => '',
                'image' => 'gantry-theme://images/logo/callisto.png',
                'text' => 'Callisto',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => true,
                'menu' => '',
                'base' => '/',
                'startLevel' => 1,
                'maxLevels' => 0,
                'renderTitles' => 0,
                'hoverExpand' => 1,
                'mobileTarget' => 0
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'social-items'
                ],
                'target' => '_blank',
                'display' => 'both',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-twitter fa-fw',
                        'text' => '',
                        'link' => 'http://twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ],
                    1 => [
                        'icon' => 'fa fa-facebook fa-fw',
                        'text' => '',
                        'link' => 'http://facebook.com/rockettheme',
                        'name' => 'Facebook'
                    ],
                    2 => [
                        'icon' => 'fa fa-google fa-fw',
                        'text' => '',
                        'link' => 'http://plus.google.com/+rockettheme',
                        'name' => 'Google'
                    ],
                    3 => [
                        'icon' => 'fa fa-rss fa-fw',
                        'text' => '',
                        'link' => 'http://www.rockettheme.com/product-updates?rss',
                        'name' => 'RSS'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'css' => [
                    'class' => 'totop'
                ]
            ],
            'contact' => [
                'enabled' => true
            ],
            'contentlist' => [
                'enabled' => true,
                'cols' => 'g-listgrid-4cols'
            ],
            'iconmenu' => [
                'enabled' => '1',
                'target' => '_parent',
                'class' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-diamond',
                        'text' => 'Features',
                        'link' => 'http://docs.gantry.org/gantry5/basics/key-features',
                        'name' => 'Features'
                    ],
                    1 => [
                        'icon' => 'fa fa-rocket',
                        'text' => 'Gantry 5',
                        'link' => 'http://www.gantry.org/',
                        'name' => 'Gantry 5'
                    ],
                    2 => [
                        'icon' => 'fa fa-gear',
                        'text' => 'Addons',
                        'link' => 'http://docs.gantry.org/gantry5/particles',
                        'name' => 'Addons'
                    ],
                    3 => [
                        'icon' => 'fa fa-cloud-download',
                        'text' => 'Download',
                        'link' => 'http://www.rockettheme.com/joomla/templates/callisto',
                        'name' => 'Download'
                    ]
                ]
            ],
            'imagegrid' => [
                'enabled' => true,
                'cols' => 'g-imagegrid-2cols'
            ],
            'infolist' => [
                'enabled' => true
            ],
            'newsletter' => [
                'enabled' => true
            ],
            'promoimage' => [
                'enabled' => true
            ],
            'simplecounter' => [
                'enabled' => true,
                'month' => 0
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true,
                'priority' => 0,
                'in_footer' => false
            ],
            'content' => [
                'enabled' => true
            ],
            'contentarray' => [
                'enabled' => true,
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show'
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show'
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ]
                ]
            ],
            'date' => [
                'enabled' => true,
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => true
            ],
            'module' => [
                'enabled' => true
            ],
            'position' => [
                'enabled' => true
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#ecbc2e',
                'color-2' => '#8b5717'
            ],
            'base' => [
                'background' => '#1f1f1f',
                'text-color' => '#888888'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '48rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '48rem'
            ],
            'copyright' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'extension' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'feature' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'font' => [
                'family-default' => 'sourcesanspro, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-title' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-promo' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif'
            ],
            'footer' => [
                'background' => '#1f1f1f',
                'text-color' => '#878787'
            ],
            'header' => [
                'background' => '#ecbc2e',
                'text-color' => '#b08523'
            ],
            'main' => [
                'background' => '#1a1a1a',
                'text-color' => '#878787'
            ],
            'menu' => [
                'col-width' => '180px',
                'animation' => 'g-fade'
            ],
            'navigation' => [
                'background' => '#353535',
                'text-color' => '#828282'
            ],
            'offcanvas' => [
                'background' => '#1b1b1f',
                'text-color' => '#ffffff',
                'width' => '17rem',
                'toggle-color' => '#ffffff',
                'toggle-visibility' => 1
            ],
            'showcase' => [
                'background' => '#23262f',
                'text-color' => '#ffffff'
            ],
            'preset' => 'preset1'
        ],
        'page' => [
            'assets' => [
                'priority' => 0,
                'in_footer' => false,
                'css' => [
                    0 => [
                        'location' => 'components/com_roksprocket/layouts/tabs/themes/default/tabs.css',
                        'inline' => '',
                        'extra' => [
                            
                        ],
                        'priority' => '0',
                        'name' => 'Tabs'
                    ]
                ],
                'javascript' => [
                    0 => [
                        'location' => 'components/com_roksprocket/layouts/tabs/themes/default/tabs.js',
                        'inline' => '',
                        'in_footer' => '0',
                        'extra' => [
                            
                        ],
                        'priority' => '0',
                        'name' => 'Tabs'
                    ]
                ]
            ],
            'body' => [
                'attribs' => [
                    'class' => 'gantry'
                ],
                'layout' => [
                    'sections' => 0
                ]
            ],
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'index' => [
            'name' => '66',
            'timestamp' => 1490784938,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-8844' => 'Logo / Image'
                ],
                'module' => [
                    'position-module-6670' => 'Module Instance',
                    'position-module-8446' => 'search',
                    'position-module-2531' => 'Module Instance',
                    'position-module-9396' => 'Module Instance',
                    'position-module-6545' => 'Module Instance'
                ],
                'menu' => [
                    'menu-4871' => 'Menu'
                ],
                'social' => [
                    'social-4022' => 'Social'
                ],
                'content' => [
                    'system-content-8106' => 'Page Content'
                ],
                'date' => [
                    'date-8867' => 'Date'
                ],
                'infolist' => [
                    'infolist-5873' => 'Info List'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-8844 80',
                        1 => 'position-module-6670 20'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-4871 80',
                        1 => 'social-4022 20'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'position-module-8446'
                    ],
                    1 => [
                        0 => 'system-content-8106'
                    ],
                    2 => [
                        0 => 'position-module-2531 50',
                        1 => 'position-module-9396 50'
                    ],
                    3 => [
                        0 => 'position-module-6545'
                    ]
                ],
                '/container-6538/' => [
                    0 => [
                        0 => [
                            'mainbar 73' => [
                                
                            ]
                        ],
                        1 => [
                            'sidebar 27' => [
                                
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'date-8867'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'infolist-5873'
                    ]
                ],
                '/copyright/' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => 'box1',
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section'
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-6538' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'logo-8844' => [
                    'title' => 'Logo / Image',
                    'attributes' => [
                        'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                    ]
                ],
                'position-module-6670' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '97',
                        'key' => 'module-instance'
                    ]
                ],
                'menu-4871' => [
                    'attributes' => [
                        'menu' => 'main-menu'
                    ]
                ],
                'social-4022' => [
                    'attributes' => [
                        'items' => [
                            0 => [
                                'icon' => 'fa fa-twitter fa-fw',
                                'text' => '',
                                'link' => 'http://twitter.com/rockettheme',
                                'name' => 'Twitter'
                            ],
                            1 => [
                                'icon' => 'fa fa-facebook fa-fw',
                                'text' => '',
                                'link' => 'http://facebook.com/rockettheme',
                                'name' => 'Facebook'
                            ],
                            2 => [
                                'icon' => 'fa fa-google fa-fw',
                                'text' => '',
                                'link' => 'http://plus.google.com/+rockettheme',
                                'name' => 'Google'
                            ]
                        ]
                    ]
                ],
                'position-module-8446' => [
                    'title' => 'search',
                    'attributes' => [
                        'module_id' => '106',
                        'key' => 'search'
                    ]
                ],
                'system-content-8106' => [
                    'block' => [
                        'variations' => 'box2'
                    ]
                ],
                'position-module-2531' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '103',
                        'key' => 'module-instance'
                    ],
                    'block' => [
                        'variations' => 'shadow rounded equal-height center box-orange'
                    ]
                ],
                'position-module-9396' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '101',
                        'key' => 'module-instance'
                    ],
                    'block' => [
                        'variations' => 'shadow2 rounded equal-height center box-orange'
                    ]
                ],
                'position-module-6545' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '88',
                        'key' => 'module-instance'
                    ]
                ],
                'infolist-5873' => [
                    'title' => 'Info List'
                ]
            ]
        ],
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ]
    ]
];
