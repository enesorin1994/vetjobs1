<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1490184219,
    'checksum' => '4c13d94f18cf40e3d064c00e2bcd2610',
    'files' => [
        'templates/rt_callisto/custom/config/80' => [
            'assignments' => [
                'file' => 'templates/rt_callisto/custom/config/80/assignments.yaml',
                'modified' => 1490184218
            ],
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/80/index.yaml',
                'modified' => 1489745687
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/80/layout.yaml',
                'modified' => 1489745687
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/80/page/head.yaml',
                'modified' => 1488888443
            ],
            'styles' => [
                'file' => 'templates/rt_callisto/custom/config/80/styles.yaml',
                'modified' => 1489571583
            ]
        ],
        'templates/rt_callisto/custom/config/default' => [
            'index' => [
                'file' => 'templates/rt_callisto/custom/config/default/index.yaml',
                'modified' => 1488888442
            ],
            'layout' => [
                'file' => 'templates/rt_callisto/custom/config/default/layout.yaml',
                'modified' => 1488888442
            ],
            'page/head' => [
                'file' => 'templates/rt_callisto/custom/config/default/page/head.yaml',
                'modified' => 1488888442
            ]
        ],
        'templates/rt_callisto/config/default' => [
            'particles/iconmenu' => [
                'file' => 'templates/rt_callisto/config/default/particles/iconmenu.yaml',
                'modified' => 1463470148
            ],
            'particles/logo' => [
                'file' => 'templates/rt_callisto/config/default/particles/logo.yaml',
                'modified' => 1463470148
            ],
            'particles/social' => [
                'file' => 'templates/rt_callisto/config/default/particles/social.yaml',
                'modified' => 1463470148
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'css' => [
                    'class' => 'branding'
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'date' => [
                    'start' => 'now',
                    'end' => 'now'
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => true,
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'link' => true,
                'url' => '',
                'image' => 'gantry-theme://images/logo/callisto.png',
                'text' => 'Callisto',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => true,
                'menu' => '',
                'base' => '/',
                'startLevel' => 1,
                'maxLevels' => 0,
                'renderTitles' => 0,
                'hoverExpand' => 1,
                'mobileTarget' => 0
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'social-items'
                ],
                'target' => '_blank',
                'display' => 'both',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-twitter fa-fw',
                        'text' => '',
                        'link' => 'http://twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ],
                    1 => [
                        'icon' => 'fa fa-facebook fa-fw',
                        'text' => '',
                        'link' => 'http://facebook.com/rockettheme',
                        'name' => 'Facebook'
                    ],
                    2 => [
                        'icon' => 'fa fa-google fa-fw',
                        'text' => '',
                        'link' => 'http://plus.google.com/+rockettheme',
                        'name' => 'Google'
                    ],
                    3 => [
                        'icon' => 'fa fa-rss fa-fw',
                        'text' => '',
                        'link' => 'http://www.rockettheme.com/product-updates?rss',
                        'name' => 'RSS'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'css' => [
                    'class' => 'totop'
                ]
            ],
            'contact' => [
                'enabled' => true
            ],
            'contentlist' => [
                'enabled' => true,
                'cols' => 'g-listgrid-4cols'
            ],
            'iconmenu' => [
                'enabled' => '1',
                'target' => '_parent',
                'class' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-diamond',
                        'text' => 'Features',
                        'link' => 'http://docs.gantry.org/gantry5/basics/key-features',
                        'name' => 'Features'
                    ],
                    1 => [
                        'icon' => 'fa fa-rocket',
                        'text' => 'Gantry 5',
                        'link' => 'http://www.gantry.org/',
                        'name' => 'Gantry 5'
                    ],
                    2 => [
                        'icon' => 'fa fa-gear',
                        'text' => 'Addons',
                        'link' => 'http://docs.gantry.org/gantry5/particles',
                        'name' => 'Addons'
                    ],
                    3 => [
                        'icon' => 'fa fa-cloud-download',
                        'text' => 'Download',
                        'link' => 'http://www.rockettheme.com/joomla/templates/callisto',
                        'name' => 'Download'
                    ]
                ]
            ],
            'imagegrid' => [
                'enabled' => true,
                'cols' => 'g-imagegrid-2cols'
            ],
            'infolist' => [
                'enabled' => true
            ],
            'newsletter' => [
                'enabled' => true
            ],
            'promoimage' => [
                'enabled' => true
            ],
            'simplecounter' => [
                'enabled' => true,
                'month' => 0
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true,
                'priority' => 0,
                'in_footer' => false
            ],
            'content' => [
                'enabled' => true
            ],
            'contentarray' => [
                'enabled' => true,
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show'
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show'
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ]
                ]
            ],
            'date' => [
                'enabled' => true,
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => true
            ],
            'module' => [
                'enabled' => true
            ],
            'position' => [
                'enabled' => true
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#1abc9c',
                'color-2' => '#007e65'
            ],
            'base' => [
                'background' => '#ffffff',
                'text-color' => '#888888'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '48rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '48rem'
            ],
            'copyright' => [
                'background' => '#f5f5f5',
                'text-color' => '#878787'
            ],
            'extension' => [
                'background' => '#f5f5f5',
                'text-color' => '#878787'
            ],
            'feature' => [
                'background' => '#f5f5f5',
                'text-color' => '#878787'
            ],
            'font' => [
                'family-default' => 'sourcesanspro, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-title' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'family-promo' => 'lato, Helvetica, Tahoma, Geneva, Arial, sans-serif'
            ],
            'footer' => [
                'background' => '#ffffff',
                'text-color' => '#888888'
            ],
            'header' => [
                'background' => '#1abc9c',
                'text-color' => '#138871'
            ],
            'main' => [
                'background' => '#f5f5f5',
                'text-color' => '#878787'
            ],
            'menu' => [
                'col-width' => '180px',
                'animation' => 'g-fade'
            ],
            'navigation' => [
                'background' => '#ffffff',
                'text-color' => '#888888'
            ],
            'offcanvas' => [
                'background' => '#f0f0f0',
                'text-color' => '#888888',
                'width' => '17rem',
                'toggle-color' => '#ffffff',
                'toggle-visibility' => 1
            ],
            'showcase' => [
                'background' => '#e8e8e8',
                'text-color' => '#878787'
            ],
            'preset' => 'preset7'
        ],
        'page' => [
            'assets' => [
                'priority' => 0,
                'in_footer' => false
            ],
            'body' => [
                'attribs' => [
                    'class' => 'gantry'
                ],
                'layout' => [
                    'sections' => 0
                ]
            ],
            'head' => [
                'atoms' => [
                    
                ]
            ]
        ],
        'index' => [
            'name' => '80',
            'timestamp' => 1489745687,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/offline.png',
                'name' => 'pages_-_coming_soon',
                'timestamp' => 1463470148
            ],
            'positions' => [
                
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'mainbar' => 'Mainbar',
                'sidebar' => 'Sidebar',
                'extension' => 'Extension',
                'copyright' => 'Copyright',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-7370' => 'Logo'
                ],
                'menu' => [
                    'menu-4265' => 'Menu'
                ],
                'social' => [
                    'social-1354' => 'Social'
                ],
                'module' => [
                    'position-module-3493' => 'Module Instance',
                    'position-module-4744' => 'Module Instance',
                    'position-module-2992' => 'Module Instance',
                    'position-module-5401' => 'Module Instance',
                    'position-module-4643' => 'Module Instance'
                ],
                'content' => [
                    'system-content-5575' => 'Page Content'
                ],
                'custom' => [
                    'custom-3288' => 'Add a job to your Watchlist'
                ],
                'newsletter' => [
                    'newsletter-1051' => 'Newsletter'
                ],
                'copyright' => [
                    'copyright-8574' => 'Copyright'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/offline.png',
                'name' => 'pages_-_coming_soon',
                'timestamp' => 1463470148
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-7370'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'menu-4265 80',
                        1 => 'social-1354 20'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'position-module-3493'
                    ],
                    1 => [
                        0 => 'position-module-4744 33.3',
                        1 => 'position-module-2992 33.3',
                        2 => 'position-module-5401 33.3'
                    ]
                ],
                '/container-2571/' => [
                    0 => [
                        0 => [
                            'mainbar 67' => [
                                0 => [
                                    0 => 'system-content-5575'
                                ]
                            ]
                        ],
                        1 => [
                            'sidebar 33' => [
                                0 => [
                                    0 => 'position-module-4643'
                                ]
                            ]
                        ]
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'custom-3288'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'newsletter-1051'
                    ]
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'copyright-8574'
                    ]
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section'
                ],
                'sidebar' => [
                    'type' => 'section'
                ],
                'container-2571' => [
                    'title' => 'Main',
                    'attributes' => [
                        'id' => 'g-main',
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'logo-7370' => [
                    'attributes' => [
                        'image' => 'gantry-media://cropped-headervetdrug3.jpg'
                    ],
                    'block' => [
                        'class' => 'g-logo-block center'
                    ]
                ],
                'menu-4265' => [
                    'attributes' => [
                        'menu' => 'main-menu'
                    ]
                ],
                'position-module-3493' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '93',
                        'key' => 'module-instance'
                    ]
                ],
                'position-module-4744' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '103',
                        'key' => 'module-instance'
                    ]
                ],
                'position-module-2992' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '123',
                        'key' => 'module-instance'
                    ]
                ],
                'position-module-5401' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '131'
                    ]
                ],
                'position-module-4643' => [
                    'title' => 'Module Instance',
                    'attributes' => [
                        'module_id' => '97',
                        'key' => 'module-instance'
                    ]
                ],
                'custom-3288' => [
                    'title' => 'Add a job to your Watchlist',
                    'attributes' => [
                        'html' => '<div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Add a job to your Watchlist</h2>
	<div class="g-layercontent-subtitle">Press the <strong>Add to Watchlist</strong> button under the Job Title</div>
	'
                    ],
                    'block' => [
                        'class' => 'flush center',
                        'variations' => 'box1'
                    ]
                ],
                'newsletter-1051' => [
                    'attributes' => [
                        'title' => 'Newsletter',
                        'headtext' => 'Aboneaza-te la newsletter-ul VetJobs si ramai la curent cu ultimele joburi din Medicina veterinara!',
                        'inputboxtext' => 'Email Address',
                        'buttontext' => 'Join',
                        'uri' => 'rocketthemeblog',
                        'buttonclass' => 'button button-3'
                    ]
                ],
                'copyright-8574' => [
                    'attributes' => [
                        'date' => [
                            'start' => '2007'
                        ],
                        'owner' => 'RocketTheme, LLC'
                    ],
                    'block' => [
                        'class' => 'center'
                    ]
                ]
            ]
        ],
        'assignments' => [
            'menu' => [
                
            ],
            'style' => [
                
            ]
        ]
    ]
];
