<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '58cba663916024.29819823',
    'content' => '<div class="g-content g-particle">
                                    <div class="g-layercontent">
    <div class="g-grid">
        <div class="g-block size-48">
            <div class="g-grid">
                <div class="g-block size-48">
                    <img src="/vet/templates/rt_callisto/images/demo/pages/features/overview/img-04.jpg" alt="Menu Editor" class="rounded"/>
                </div>
                <div class="g-block size-4"></div>
                <div class="g-block size-48">
                    <h5 class="g-layercontent-subtitle g-uppercase nomarginall">Dropdown Menu</h5>
                    <p class="nomarginall">A Dropdown Menu system, with inline icons, multiple columns and much more.</p>

                    <a href="" class="button button-3">Learn More</a>
                </div>
            </div>
        </div>
        <div class="g-block size-4"></div>
        <div class="g-block size-48">
            <div class="g-grid">
                <div class="g-block size-48">
                    <img src="/vet/templates/rt_callisto/images/demo/pages/features/overview/img-05.jpg" alt="Positions" class="rounded"/>
                </div>
                <div class="g-block size-4"></div>
                <div class="g-block size-48">
                    <h5 class="g-layercontent-subtitle g-uppercase nomarginall">Positions</h5>
                    <p class="nomarginall">Unlimited positions, with drag and drop add, delete, move and resize capabilities.</p>

                    <a href="" class="button button-3">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="g-layercontent">
    <div class="g-grid">
        <div class="g-block size-48">
            <div class="g-grid">
                <div class="g-block size-48">
                    <img src="/vet/templates/rt_callisto/images/demo/pages/features/overview/img-06.jpg" alt="Variations" class="rounded"/>
                </div>
                <div class="g-block size-4"></div>
                <div class="g-block size-48">
                    <h5 class="g-layercontent-subtitle g-uppercase nomarginall">Variations</h5>
                    <p class="nomarginall">Enhance positions and particles with stylistic and structural block variations.</p>

                    <a href="" class="button button-3">Learn More</a>
                </div>
            </div>
        </div>
        <div class="g-block size-4"></div>
        <div class="g-block size-48">
            <div class="g-grid">
                <div class="g-block size-48">
                    <img src="/vet/templates/rt_callisto/images/demo/pages/features/overview/img-07.jpg" alt="Presets" class="rounded"/>
                </div>
                <div class="g-block size-4"></div>
                <div class="g-block size-48">
                    <h5 class="g-layercontent-subtitle g-uppercase nomarginall">Presets</h5>
                    <p class="nomarginall">Choose from a selection of ten preset styles, easily customizable from the admin.</p>

                    <a href="" class="button button-3">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="g-layercontent">
    <div class="g-grid">
        <div class="g-block size-48">
            <div class="g-grid">
                <div class="g-block size-48">
                    <img src="/vet/templates/rt_callisto/images/demo/pages/features/overview/img-08.jpg" alt="Responsive" class="rounded"/>
                </div>
                <div class="g-block size-4"></div>
                <div class="g-block size-48">
                    <h5 class="g-layercontent-subtitle g-uppercase nomarginall">Responsive</h5>
                    <p class="nomarginall">A layout that adapts to the viewing device, such as laptop, tablet or mobile.</p>

                    <a href="" class="button button-3">Learn More</a>
                </div>
            </div>
        </div>
        <div class="g-block size-4"></div>
        <div class="g-block size-48">
            <div class="g-grid">
                <div class="g-block size-48">
                    <img src="/vet/templates/rt_callisto/images/demo/pages/features/overview/img-09.jpg" alt="Typography" class="rounded"/>
                </div>
                <div class="g-block size-4"></div>
                <div class="g-block size-48">
                    <h5 class="g-layercontent-subtitle g-uppercase nomarginall">Typography</h5>
                    <p class="nomarginall">Enhance default and custom content with rich typography and FontAwesome icons.</p>

                    <a href="" class="button button-3">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</div>
            
    </div>'
];
