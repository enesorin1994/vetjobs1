<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '58c8ff90341875.72740759',
    'content' => '<div class="g-content g-particle">
                                    <div class="g-social social-items">
                    <a target="_blank" href="http://twitter.com/rockettheme" title="" aria-label="">
                <span class="fa fa-twitter fa-fw"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="http://facebook.com/rockettheme" title="" aria-label="">
                <span class="fa fa-facebook fa-fw"></span>                <span class="g-social-text"></span>            </a>
                    <a target="_blank" href="http://plus.google.com/+rockettheme" title="" aria-label="">
                <span class="fa fa-google fa-fw"></span>                <span class="g-social-text"></span>            </a>
            </div>
            
    </div>'
];
