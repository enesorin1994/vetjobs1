<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '58c917bbd91060.73574501',
    'content' => '<div class="g-content g-particle">
                                    <div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Add a job to your Watchlist</h2>
	<div class="g-layercontent-subtitle">Press the <strong>Add to Watchlist</strong> under the Job Title</div>
	
            
    </div>'
];
