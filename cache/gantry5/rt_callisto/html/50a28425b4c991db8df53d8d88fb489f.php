<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '58d8cf780bc4c0.73151341',
    'content' => '<div class="g-content g-particle">
                                    <div class="g-layercontent">
	<h2 class="g-layercontent-title">Nothing to See Here</h2>
	<div class="g-layercontent-subtitle">Please use the navigation above or contact us to find what you are looking for.</div>
	<a href="#" class="button button-2">Contact Us</a>
</div>
            
    </div>'
];
