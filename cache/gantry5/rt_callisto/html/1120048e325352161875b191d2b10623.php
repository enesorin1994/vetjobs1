<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '58d8cf780df741.29255187',
    'content' => '<div class="g-content g-particle">
                                    <h2 class="g-title">Sample Sitemap</h2>

<div class="g-grid">
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Home</a></li>
			<li><a href="#">Features</a></li>
			<li><a href="#">Typography</a></li>
			<li><a href="#">Particles</a></li>
			<li><a href="#">Variations</a></li>
		</ul>		
	</div>
	<div class="g-block">
		<ul class="nomarginall noliststyle">
			<li><a href="#">Buttons</a></li>
			<li><a href="#">Pages</a></li>
			<li><a href="#">Guide</a></li>
			<li><a href="#">Support</a></li>
			<li><a href="#">Download</a></li>
		</ul>		
	</div>	
</div>
            
    </div>'
];
