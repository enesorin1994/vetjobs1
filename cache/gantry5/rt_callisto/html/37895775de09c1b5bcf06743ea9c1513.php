<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '58c9095f99cce3.90115108',
    'content' => '<div class="g-content g-particle">
                                    <h2 class="g-title">Responsive Layout</h2>
<p><span>Callisto is built with a Responsive Layout, which means it automatically adapts to the viewing device, so will expand and contract accordingly to the size and resolution of the screen, whether mobile<span class="hidden-tablet">, tablet</span> or desktop. This ensures a consistent<span class="hidden-tablet">, and easy to maintain,</span> appearance for your site.</span></p>
            
    </div>'
];
