<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '58d8cf7807dcb1.05128222',
    'content' => '<div class="g-content g-particle">
                                    <div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Whooops! Error!</h2>
	<div class="g-layercontent-subtitle">Looks like something went wrong. Sorry!</div>
</div>
            
    </div>'
];
