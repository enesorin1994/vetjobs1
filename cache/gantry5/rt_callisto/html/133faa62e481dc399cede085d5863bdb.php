<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '58c9181b3748b0.43644791',
    'content' => '<div class="g-content g-particle">
                                    <div class="g-layercontent g-layercontent-small">
	<h2 class="g-layercontent-title">Add a job to your Watchlist</h2>
	<div class="g-layercontent-subtitle">Press the <strong>Add to Watchlist</strong> button under the Job Title</div>
	
            
    </div>'
];
