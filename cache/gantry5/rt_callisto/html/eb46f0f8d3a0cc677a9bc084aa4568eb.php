<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '58cba6639d1851.20919820',
    'content' => '<div class="g-content g-particle">
                                    <div class="g-layercontent">
    <div class="g-grid">
        <div class="g-block size-50">
            <div class="g-content">
                <div class="browser-wrapper">
                    <div class="browser-bar">
                        <span class="browser-button"></span>
                    </div>
                    <img src="/vet/templates/rt_callisto/images/demo/pages/features/overview/img-01.jpg" alt="Menu Editor"/>
                </div>
            </div>
        </div>
        <div class="g-block size-50">
            <div class="g-content">
                <h1 class="g-layercontent-title g-uppercase">Gantry 5</h1>
                <h3 class="g-layercontent-title g-uppercase"><span class="hidden-tablet">Drag &amp; Drop </span>Menu Editor</h3>
                <p class="g-layercontent-promotext nomarginall">Gantry 5 features an advanced and user friendly Menu Editor, which augments the core menu with a rich, drag and drop enhanced interface<span class="hidden-tablet">, to easily change the menu\'s frontend appearance, such as columns</span>.</p>
                <div class="visible-large"><div class="g-layercontent-promotext">The Menu Editor panel takes what your CMS\' built-in Menu Manager has and enables you to override it. Changes you make in this panel do not in any way affect the way the CMS handles Menu items.</div></div>
    
                <a href="http://docs.gantry.org/gantry5/configure/menu-editor" class="button button-3"><i class="fa fa-fw fa-list"></i> Learn More</a>
            </div>
        </div>
    </div>
</div>

<div class="g-layercontent">
    <div class="g-grid">
        <div class="g-block size-50">
            <div class="g-content">
                <h1 class="g-layercontent-title g-uppercase">Gantry 5</h1>
                <h3 class="g-layercontent-title g-uppercase"><span class="hidden-tablet">Drag &amp; Drop </span>Layout Manager</h3>
                <p class="g-layercontent-promotext nomarginall">Gantry 5\'s layout manager also benefits from a drag and drop interface, providing an easy mechanism for adding,<span class="hidden-tablet"> removing, configuring,</span> moving and resizing positions and particles.</p>
                <div class="visible-large"><div class="g-layercontent-promotext">Due to the way layouts are now created and managed in Gantry 5, there are no restrictions to the number of positions you can have, and are now truly unlimited.</div></div>
    
                <a href="http://docs.gantry.org/gantry5/configure/layout-manager" class="button button-3"><i class="fa fa-fw fa-columns"></i> Learn More</a>
            </div>
        </div>
        <div class="g-block size-50">
            <div class="g-content">
                <div class="browser-wrapper">
                    <div class="browser-bar">
                        <span class="browser-button"></span>
                    </div>
                    <img src="/vet/templates/rt_callisto/images/demo/pages/features/overview/img-02.jpg" alt="Layout Manager"/>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="g-layercontent">
    <div class="g-grid">
        <div class="g-block size-50">
            <div class="g-content">
                <div class="browser-wrapper">
                    <div class="browser-bar">
                        <span class="browser-button"></span>
                    </div>
                    <img src="/vet/templates/rt_callisto/images/demo/pages/features/overview/img-03.jpg" alt="Menu Editor"/>
                </div>
            </div>
        </div>
        <div class="g-block size-50">
            <div class="g-content">
                <h1 class="g-layercontent-title g-uppercase">Gantry 5</h1>
                <h3 class="g-layercontent-title g-uppercase">Styles Settings<span class="hidden-tablet">  &amp; Presets</span></h3>
                <p class="g-layercontent-promotext nomarginall">Style customization is a very simple process with Gantry 5. All available, pre-built presets are displayed in the Styles tab<span class="hidden-tablet"> for you to preview, individually customize and apply</span>.</p>
                <div class="visible-large"><div class="g-layercontent-promotext">Each preset has an assortment of individual style options, such as text and background colors for the various sections, allowing for swift and easy color scheme changes.</div></div>
    
                <a href="http://docs.gantry.org/gantry5/configure/styles" class="button button-3"><i class="fa fa-fw fa-adjust"></i> Learn More</a>
            </div>
        </div>
    </div>
</div>
            
    </div>'
];
