<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '58c9095fa50810.26321979',
    'content' => '<div class="g-content g-particle">
                                    <h2 class="g-title">Demo Information</h2>

<p>All demo content is for sample purposes only, intended to represent a live site. All content images are licensed from <a href="http://tookapic.com">tookapic.com</a> for exclusive use on this demo only.</p>
<p>Note: Callisto theme is built on the Gantry 5 framework, the most customizable and powerful version of the framework yet.</p>

<a href="http://www.rockettheme.com/joomla/templates/callisto" class="button button-3"><i class="fa fa-cloud-download"></i> Download</a>
            
    </div>'
];
