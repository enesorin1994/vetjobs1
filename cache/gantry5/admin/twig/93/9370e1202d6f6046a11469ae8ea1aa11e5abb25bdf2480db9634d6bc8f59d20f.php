<?php

/* @gantry-admin/partials/php_unsupported.html.twig */
class __TwigTemplate_8a57c1a58beedad09c3c3229dcb97cea684eaf284f8080394856eb5d79652cc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["php_version"] = twig_constant("PHP_VERSION");
        // line 2
        echo "
";
        // line 3
        if ((is_string($__internal_c2de782fae69124711edc31c35437ef6f4184987536154ccbf0d1b36abb4ab26 = (isset($context["php_version"]) ? $context["php_version"] : null)) && is_string($__internal_1071ee28e0fd3b3685bdad81b7055e795255e3953657a6ba226acabc74adc5a9 = "5.4") && ('' === $__internal_1071ee28e0fd3b3685bdad81b7055e795255e3953657a6ba226acabc74adc5a9 || 0 === strpos($__internal_c2de782fae69124711edc31c35437ef6f4184987536154ccbf0d1b36abb4ab26, $__internal_1071ee28e0fd3b3685bdad81b7055e795255e3953657a6ba226acabc74adc5a9)))) {
            // line 4
            echo "<div class=\"g-grid\">
    <div class=\"g-block alert alert-warning g-php-outdated\">
        ";
            // line 6
            echo $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->transFilter("GANTRY5_PLATFORM_PHP54_WARNING", (isset($context["php_version"]) ? $context["php_version"] : null));
            echo "
    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "@gantry-admin/partials/php_unsupported.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 6,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set php_version = constant('PHP_VERSION') %}

{% if php_version starts with '5.4' %}
<div class=\"g-grid\">
    <div class=\"g-block alert alert-warning g-php-outdated\">
        {{ 'GANTRY5_PLATFORM_PHP54_WARNING'|trans(php_version)|raw }}
    </div>
</div>
{% endif %}", "@gantry-admin/partials/php_unsupported.html.twig", "C:\\xampp\\htdocs\\Vet\\administrator\\components\\com_gantry5\\templates\\partials\\php_unsupported.html.twig");
    }
}
