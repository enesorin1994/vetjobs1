<?php

/* forms/fields/input/imagepicker.html.twig */
class __TwigTemplate_cbfd77cb92e4bac5abafa9f70b10d2f66714b72625d39fd4c35860865aeccff8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("forms/fields/input/filepicker.html.twig", "forms/fields/input/imagepicker.html.twig", 1);
        $this->blocks = array(
            'input' => array($this, 'block_input'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "forms/fields/input/filepicker.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_input($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $context["field"] = twig_array_merge(array("icon" => "fa-file-image-o", "filter" => ".(jpe?g|gif|png|svg)\$"), (isset($context["field"]) ? $context["field"] : null));
        // line 5
        echo "    ";
        $this->displayParentBlock("input", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "forms/fields/input/imagepicker.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'forms/fields/input/filepicker.html.twig' %}

{% block input %}
    {% set field = {'icon': 'fa-file-image-o', 'filter': '\\.(jpe?g|gif|png|svg)\$'}|merge(field) %}
    {{ parent() }}
{% endblock %}
", "forms/fields/input/imagepicker.html.twig", "C:\\xampp\\htdocs\\Vet\\administrator\\components\\com_gantry5\\templates\\forms\\fields\\input\\imagepicker.html.twig");
    }
}
