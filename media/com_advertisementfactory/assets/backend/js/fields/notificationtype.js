window.addEvent('domready', function() {
  $$('.advertisementfactory-notification-tokens a').addEvent('click', function(event) {
    event.preventDefault();

    var editor = this.getParents('table')[0].getAttribute('rel');
    var text   = this.text;

    jInsertEditorText(text, editor);
  });
});
