window.addEvent('domready', function() {
  var timer = $('advertisementfactory-timer');

  timer.innerHTML = AdvertisementFactory.get('timeout');

  setInterval(function () {
    var current = parseInt(timer.innerHTML);

    if (current === 0) {
      return false;
    }

    timer.innerHTML = current - 1;
  }, 1 * 1000);

  setTimeout(function () {
    window.location.href = AdvertisementFactory.get('redirect');
  }, AdvertisementFactory.get('timeout') * 1000);
});
