window.addEvent('domready', function() {
  SqueezeBox.initialize();

  SqueezeBox.open($('advertisementfactory-modal-ad'), {
    handler: 'adopt',
    size: {x: AdvertisementFactory.get('window_width'), y: AdvertisementFactory.get('window_height')},
    closable: false
  });

  $('advertisementfactory-modal-close').addEvent('click', function () {
    SqueezeBox.close();
    clearInterval(interval);
    return false;
  });

  var timer = setTimeout(function () {
    SqueezeBox.close();
    clearInterval(interval);
  }, AdvertisementFactory.get('window_close_seconds') * 1000);

  var interval = setInterval(function () {
    var timer = $('advertisementfactory-timer');

    if (!timer) {
      clearInterval(interval);
    }

    var time = parseInt(timer.getProperty('html'));

    time = time > 0 ? time - 1 : 0;

    timer.setProperty('html', time);
  }, 1000);
});
