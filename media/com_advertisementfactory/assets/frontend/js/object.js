var AdvertisementFactory = {

  options: {},

  set: function (option, value) {
    this.options[option] = value;
  },

  get: function (option) {
    return this.options[option];
  }
}
