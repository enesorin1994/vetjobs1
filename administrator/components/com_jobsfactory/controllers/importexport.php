<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JJobsfactoryAdminControllerImportexport extends JControllerLegacy
{

    function execute($task)
    {
        if (file_exists(JPATH_COMPONENT_ADMINISTRATOR . DS . 'toolbar.importexport.php'))
            require JPATH_COMPONENT_ADMINISTRATOR . DS . 'toolbar.importexport.php';

        parent::execute($task);
    }

    function ImportExport()
    {
        $view = $this->getView('importexport', 'html');
        $view->display();
    }

    function ImportExportLoc()
    {
        $view = $this->getView('importexport', 'html');
        $view->display('location');
    }

    function ExportToXls()
    {
        $db = JFactory::getDBO();
        $db->setQuery("SELECT * FROM #__" . APP_PREFIX . "_fields ORDER BY ordering ASC, page ASC ");
        $fields = $db->loadObjectList();
        if (count($fields) > 0) {
            $view = $this->getView('importexport', 'html');
            $view->assign('fields', $fields);
            $view->display('exporttoxls');

        }
        else
            $this->do_ExportToXls();
        return true;
    }

    function ExportToXlsLoc()
    {
        $this->do_ExportToXlsLoc();
        return true;
    }

    function do_ExportToXls()
    {

        $db = JFactory::getDBO();
        $profile = JobsHelperTools::getUserProfileObject();

        $tableName = $profile->getIntegrationTable();
        $tableKey = $profile->getIntegrationKey();
        $integration = $profile->getIntegrationArray();

        $cid    = $this->input->get('cid', array(), 'array');

        $sql = "
    		SELECT	*,  c.catname, u.username, prof.*  \r\n
    		FROM #__jobsfactory_jobs a \r\n
    		LEFT JOIN `$tableName` prof ON a.userid= prof.`$tableKey`  \r\n
    		LEFT JOIN #__jobsfactory_categories c ON a.cat = c.id \r\n
    		LEFT JOIN #__users u ON a.userid = u.id
		";

        $db->setQuery($sql);
        $result = $db->loadAssocList();

        $headers["username"] = "User";
        $headers["title"] = "Title";
        $headers["shortdescription"] = "Short description";
        $headers["description"] = "Description";
        $headers["start_date"] = "Start date";
        $headers["end_date"] = "End date";
        $headers["published"] = "Published";
        $headers["catname"] = "Category";
        $labels = array("username", "title", "shortdescription", "description", "start_date", "end_date", "published", "catname");

        if (count($cid) > 0) {
            foreach ($cid as $fieldname) {
                $field = CustomFieldsFactory::getFieldObject($fieldname);
                $headers[$fieldname] = $field->name;
                $labels[] = $fieldname;
            }
        }

        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . "thefactory" . DS . "library" . DS . "admin.xlscreator.php");

        $filename = "ExportXLS";

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"" . $filename . ".xls\"");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        JTheFactoryXLSCreator::createXLSAdv($result, $labels, $headers, "Exported Jobs");
        exit;
    }

    function do_ExportToXlsLoc()
    {

        $db = JFactory::getDBO();

        $sql = "
            SELECT a.*,
            GROUP_CONCAT( DISTINCT ci.cityname SEPARATOR  ', ' ) AS 'cities'
            FROM #__jobsfactory_locations a
            LEFT JOIN #__jobsfactory_cities ci ON ci.location = a.id
            GROUP BY a.id
            ORDER BY a.country ASC, a.locname ASC
        ";

        $db->setQuery($sql);
        $result = $db->loadAssocList();

        $headers["locname"] = "Location";
        $headers["country"] = "Country";
        $headers["cities"] = "Cities";
        $labels = array("locname", "country", "cities");

        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . "thefactory" . DS . "library" . DS . "admin.xlscreator.php");

        $filename = "ExportXLSLoc";

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"" . $filename . ".xls\"");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        JTheFactoryXLSCreator::createXLSAdv($result, $labels, $headers, "Exported Locations");
        exit;
    }

    function ShowAdmImportForm()
    {
        $view = $this->getView('import', 'html');
        $view->assign('errors', '');
        $view->display();
    }

    function ShowAdmImportFormLoc()
    {
        $view = $this->getView('import', 'html');
        $view->assign('errors', '');
        $view->display('locations');
    }

    function ImportCSV()
    {
        $err = JobsHelperJob::ImportFromCSV();

        if (!count($err))
            $this->setRedirect("index.php?option=com_jobsfactory&task=jobs", JText::_("COM_JOBS_JOBS_SUCCESSFULLY_IMPORTED"));
        else
        {
            $view = $this->getView('import', 'html');
            $view->assign('errors', $err);
            $view->display();
        }

    }

    function ImportCSVLoc()
    {
        $err = JobsHelperJob::ImportFromCSVLoc();
        if (!count($err))
            $this->setRedirect("index.php?option=com_jobsfactory&task=city.cities", JText::_("COM_JOBS_LOCATIONS_SUCCESSFULLY_IMPORTED"));
        else
        {
            $view = $this->getView('import', 'html');
            $view->assign('errors', $err);
            $view->display('locations');
        }

    }

}
