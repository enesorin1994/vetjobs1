<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

// Access the file from Joomla environment
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.controller');

class JJobsfactoryAdminControllerMaps extends JControllerLegacy
{

    public function __construct()
	{
		parent::__construct();
	}

	/**
	 * searchBookmarks
	 */
    public function searchBookmarks(){

        // Start XML file, create parent node
        $cfg = JTheFactoryHelper::getConfig();
        $app = JFactory::getApplication();

        $center_lat = $app->input->getFloat('lat',0.00);
        $center_lng = $app->input->getFloat('lng',0.00);
        $radius = $app->input->getFloat('radius',0.00);
        $cat = $app->input->getInt('cat',0);

        $dom = new DOMDocument("1.0");
        $node = $dom->createElement("markers");
        $parnode = $dom->appendChild($node);

        $db = JFactory::getDbo();

        $where = array();
        $where[] 	= " `a`.`close_by_admin` = 0 ";
        $where[] = " a.close_offer = 0 ";
        $where[] = " a.published = 1 ";
        $where[] = " a.googlex != '' ";
        $where[] = " a.googley != '' ";

        if($cat)
            $where[] = " a.cat = '{$cat}' ";


        //To search by kilometers instead of miles, replace 3959 with 6371.
        $distance_setting = 3959;

        if($cfg->googlemap_distance==1)
            $distance_setting = 6371;


        // Search the rows in the markers table
        $db->setQuery("
        SELECT a.id, a.shortdescription, a.title as name,
            c.catname,
            googlex as lat, googley as lng ,
            ( $distance_setting * acos( cos( radians('$center_lat') ) * cos( radians( googlex ) ) * cos( radians( googley ) - radians('$center_lng') ) + sin( radians('$center_lat') ) * sin( radians( googlex ) ) ) ) AS distance
          FROM #__jobsfactory_jobs as a
          LEFT JOIN `#__jobsfactory_categories` c ON a.cat = c.id
        WHERE ".
        implode("AND",$where)
        ."
        HAVING distance < '$radius' ORDER BY distance
        ");

        $rows = $db->loadObjectList();

        ob_end_clean();
        header("Content-type: text/xml");

        // Iterate through the rows, adding XML nodes for each
        foreach ($rows as $row){
          $node = $dom->createElement("marker");
          $newnode = $parnode->appendChild($node);
          $newnode->setAttribute("name", $row->name);
          $newnode->setAttribute("info", "<strong><a href='".JobsHelperRoute::getJobDetailRoute($row->id)."'>".$row->name."</a></strong><br>".
                                        $row->catname."<br />".
                                        $row->shortdescription);
          $newnode->setAttribute("lat", $row->lat);
          $newnode->setAttribute("lng", $row->lng);
          $newnode->setAttribute("link", JobsHelperRoute::getJobDetailRoute($row->id));
          $newnode->setAttribute("distance", $row->distance);
        }

        echo $dom->saveXML();
        exit;
    }

    /**
     * showBookmarks
     */
    public function showBookmarks(){
        //Google maps
        $app = JFactory::getApplication();
        $cat = $app->input->getInt('cat',0);
        $start = $app->input->getInt('limitstart',0);
        $limit = $app->input->getInt('limit',JobsHelperTools::getItemsPerPage());
        if($limit==0){
            $limit_sql = "";
        }else{
            $limit_sql = "LIMIT $start,$limit";
        }
        // Start XML file, create parent node
        $dom = new DOMDocument("1.0");
        $node = $dom->createElement("markers");
        $parnode = $dom->appendChild($node);

        $db = JFactory::getDbo();

        $where = array();
        $where[] 	= " `a`.`close_by_admin` = 0 ";
        $where[] = " a.close_offer = 0 ";
        $where[] = " a.published = 1 ";
        $where[] = " a.googlex != '' ";
        $where[] = " a.googley != '' ";

        if($cat!=0)
            $where[] = " a.cat = '{$cat}' ";

        // Search the rows in the markers table
        $db->setQuery("
            SELECT a.id, a.shortdescription, a.title as name,
                c.catname,
                googlex as lat, googley as lng
              FROM #__jobsfactory_jobs as a
              LEFT JOIN `#__jobsfactory_categories` c ON a.cat = c.id
              WHERE
        ".
            implode("AND",$where)
        ."
        $limit_sql ");

        $rows = $db->loadObjectList();

        ob_end_clean();
        header("Content-type: text/xml");

        // Iterate through the rows, adding XML nodes for each
        foreach ($rows as $row){
          $node = $dom->createElement("marker");
          $newnode = $parnode->appendChild($node);
          $newnode->setAttribute("name", $row->name);
          $newnode->setAttribute("info", "<strong><a href='".JobsHelperRoute::getJobDetailRoute($row->id)."'>".$row->name."</a></strong><br>".
                                        $row->catname."<br />".
                                        $row->shortdescription);
          $newnode->setAttribute("lat", $row->lat);
          $newnode->setAttribute("lng", $row->lng);
          $newnode->setAttribute("link", JRoute::_(JobsHelperRoute::getJobDetailRoute($row->id)));
        }

        echo $dom->saveXML();
        exit;

    }

    function googlemap(){
        $smarty = new JobsSmarty();
        $app = JFactory::getApplication();

        $x   = $app->input->getFloat('x',0.00);
        $y   = $app->input->getFloat('y',0.00);

        $view=$this->getView('maps','html');
		$view->googleMapX = $x;
        $view->googleMapY = $y;
                
        $view->display("t_googlemap.tpl");
    }

    function googlemap_tool(){
        $cfg = JTheFactoryHelper::getConfig();

        $view=$this->getView('maps','html');
		$view->assign("cfg", $cfg);
		$view->googleMapX = $cfg->googlemap_defx;
        $view->googleMapY = $cfg->googlemap_defy;
        
        $view->display();
    }


    /**
     * saveGoogleCoords
     */
    function saveGoogleCoords()
    {
        $app = JFactory::getApplication();
        $x = trim($app->input->getFloat('googleMaps_x'),0.00);
        $y = trim($app->input->getFloat('googleMaps_y'),0.00);

        $url     = base64_decode($app->input->post->get('return_url', null, 'base64'));
        $my = JFactory::getUser();

        $userprofile=JobsHelperTools::getUserProfileObject();
        $userprofile->integrationObject->setGMapCoordinates($my->id,$x,$y);

        $this->setRedirect($url);
    }
}
