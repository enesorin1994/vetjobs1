<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

    defined('_JEXEC') or die('Restricted access');

    jimport('joomla.application.component.model');

    class JJobsfactoryAdminControllerCity extends JControllerLegacy
    {

        function execute($task)
        {
            if (file_exists(JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.admin.php'))
                require JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.admin.php';

            if (file_exists(JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'city.php'))
                require_once JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'city.php';

            parent::execute($task);
        }

        function Cities()
        {
            $app = JFactory::getApplication();
            $cfg = JTheFactoryHelper::getConfig();
            $context = 'com_jobsfactory.city.cities.';
            $reset = JFactory::getApplication()->input->getInt('reset');

            $filter_order = $app->getUserStateFromRequest($context . 'filter_order', 'filter_order', '', 'cmd');
            $filter_order_Dir = $app->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', '', 'word');

            if ($reset) {
                //$app->setUserState($context, null);
                $filter_location = '';
                $filter_approved = '';
                $search = '';
            } else {
                $filter_location = $app->getUserStateFromRequest($context . 'filter_location', 'filter_location', '', 'string');
                $filter_approved = $app->getUserStateFromRequest($context . 'filter_approved', 'filter_approved', '', 'string');
                $search = $app->getUserStateFromRequest($context . 'search', 'search', '', 'string');
            }
            $search = JString::strtolower($search);

            $cityModel = JModelLegacy::getInstance('City', 'JobsAdminModel');
            $cities = $cityModel->getCityTree(0,true,$reset,$filter_location,$filter_approved,$search);
            $pagination = $cityModel->get('pagination');

            $view = $this->getView('city', 'html');
            $view->cities = $cities;
            $view->pagination = $pagination;

            $view->order_Dir = $filter_order_Dir;
            $view->order = $filter_order;
            $view->filter_location = $filter_location;
            $view->filter_approved = $filter_approved;
            $view->search = $search;
            $view->cfg = $cfg;

            $view->display();
        }

        function CityJSON()
        {
            $cityModel = JModelLegacy::getInstance('City', 'JobsAdminModel');
            $startnode = JFactory::getApplication()->input->getInt('node', 0);
            if ($startnode <= 0) $startnode = 0;

            $cities = $cityModel->getCitiesNested($startnode, false);

            foreach ($cities as $city) {
                $cities_arr[] = array(
                    'id'          => $city->id,
                    'cityname'        => $city->cityname,
                    'lft'         => $city->left + 1,
                    'rht'         => $city->right + 1,
                    'description' => $city->description
                );
            }
            $obj = new stdClass();
            $obj->success = true;
            $obj->data = $cities_arr;
            header('Content-type:application/json');
            echo json_encode($obj);
            exit;
        }

        function NewCityJSON()
        {
            $catModel = JModelLegacy::getInstance('City', 'JobsAdminModel');
            //var_dump($newcat);
            exit;
            $app = JFactory::getApplication();
            $parent = $app->input->getInt('parent', 0);
            if ($parent <= 0) $parent = 0;

            $data = $app->input->getInt('data');

            $newcity = json_decode($data);
        }

        function CitiesWidget()
        {

            $view = $this->getView('city', 'html');
            $view->display('widget');
        }

        function DelCities()
        {
            $cids    = $this->input->get('cid', array(), 'array');

            $cityModel = JModelLegacy::getInstance('City', 'JobsAdminModel');
            $nr = $cityModel->delCity($cids);
            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=city.cities', $nr . " " . JText::_('FACTORY_CITIES_DELETED'));

        }

        function EditCity()
        {
            $cid    = $this->input->get('cid', array(), 'array');
            if (is_array($cid)) $cid = $cid[0];

            $cityModel = JModelLegacy::getInstance('City', 'JobsAdminModel');
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');
            $citytable = JTable::getInstance('City', 'Table');
            $citytable->load($cid);

            $locations = JHtml::_('factorylocation.select', 'location', '', $citytable->location, false); //$city->country

            $view = $this->getView('city', 'html');
            $view->assignRef('row', $citytable);
            $view->assignRef('locations', $locations);
            $view->assignRef('countries', $countries);
            $view->display('form');
        }

        function SaveCity()
        {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');
            $cityobj = JTable::getInstance('City', 'Table');
            $cityobj->bind(JTheFactoryHelper::getRequestArray('post', 'html'));

            $lang = JFactory::getLanguage();
            $cityobj->hash = $lang->transliterate($cityobj->cityname);
            $cityobj->hash = md5(strtolower(JFilterOutput::stringURLSafe($cityobj->hash)));

            if ($cityobj->store()) {
                $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=city.cities', JText::_('FACTORY_LOCATION_SAVED'));
            } else {
                $this->setRedirect("index.php?option=".APP_EXTENSION."&task=city.cities",JText::_("FACTORY_LOCATION_NOT_SAVED"));
                return false;
            }
        }

        function NewCity()
        {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');
            $citytable = JTable::getInstance('City', 'Table');
            $locations = JHtml::_('factorylocation.select', 'location', '', $citytable->location, false); //$city->country
            $countries = JHtml::_('country.selectlist', 'country', "id='country' onchange='jobsRefreshLocations(this);'", '', false); //$location->country id="country"

            $view = $this->getView('city', 'html');
            $view->assignRef('row', $citytable);
            $view->assignRef('locations', $locations);
            $view->assignRef('countries', $countries);
            $view->display('form');
        }

        function saveCityOrder()
        {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');
            $citytable = JTable::getInstance('City', 'Table');

            foreach ($_REQUEST as $k => $v) {
                if (substr($k, 0, 6) == 'order_') {
                    $id = substr($k, 6);
                    $citytable->load($id);
                    $citytable->ordering = $v;
                    $citytable->store();
                }
            }
            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=city.cities', JText::_('FACTORY_ORDERING_SAVED'));
        }

        /**
         * Quick endless level categories to ROOT
         *
         * @modified: 23/09/09
         **/
        function QuickAddCity()
        {
            $textcities = $this->input->getString('quickadd', '');
            $parentid = $this->input->getString('parentId','');
            $cityModel = JModelLegacy::getInstance('City', 'JobsAdminModel');
            $cityModel->quickAddFromText($textcities,$parentid);
            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=city.cities', JText::_('FACTORY_LOCATION_ADDED'));
        }

        // move categories in other parent Cities
        function showMoveCities()
        {

            $database = JFactory::getDbo();
            $cid_arr    = $this->input->get('cid', array(0), 'array');
            if (empty($cid_arr)) {
                JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
            }

            $cid_list = implode(",", $cid_arr);

            $cityModel = JModelLegacy::getInstance('City', 'JobsAdminModel');

            $database->setQuery("SELECT * FROM `" . $cityModel->city_table . "` WHERE id IN ($cid_list);");
            $changed_cities = $database->loadObjectList();

            $html_tree = JHtml::_('factorylocation.select', 'location', '', '', false);
            $countries = JHtml::_('country.selectlist', 'country', "id='country' onchange='jobsRefreshLocations(this);'", '', false); //$location->country id="country"


            $view = $this->getView('city', 'html');
            $view->assignRef('cid_list', $cid_list);
            $view->assignRef('cities', $changed_cities);
            $view->assignRef('location', $html_tree);
            $view->assignRef('countries', $countries);

            $view->display('movecity');
        }


        function doMoveCities()
        {
            $database = JFactory::getDbo();
            $app = JFactory::getApplication();
            $cid = $app->input->getString('cid', "0");
            $location = $app->input->getString('location', "0");

            $cityModel = JModelLegacy::getInstance('City', 'JobsAdminModel');

            $database->setQuery("UPDATE `" . $cityModel->city_table . "` SET location = '{$location}' WHERE id IN ({$cid})");
            $database->execute();
            $nr = $database->getAffectedRows();

            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=city.cities', $nr . " " . JText::_('FACTORY_CITIES_MOVED'));
        }


        function Publish_City()
        {
            $cids    = $this->input->get('cid', array(), 'array');
            $nr = count($cids);
            $cids_string = implode(",", $cids);

            $cityModel = JModelLegacy::getInstance('City', 'JobsAdminModel');
            $db = JFactory::getDbo();
            $db->setQuery("UPDATE `" . $cityModel->city_table . "` SET status = 1 WHERE id in ($cids_string)");
            $db->execute();

            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=city.cities', $nr . " " . JText::_('FACTORY_CITIES_PUBLISHED'));
        }

        function Unpublish_City()
        {

            $cids    = $this->input->get('cid', array(), 'array');
            $nr = count($cids);
            $cids_string = implode(",", $cids);

            $catModel = JModelLegacy::getInstance('City', 'JobsAdminModel');
            $db = JFactory::getDbo();
            $db->setQuery("UPDATE `" . $catModel->city_table . "` SET status = 0 WHERE id in ($cids_string)");
            $db->execute();

            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=city.cities', $nr . " " . JText::_('FACTORY_CITIES_UNPUBLISHED'));
        }

        function RefreshLocation() {
            $parentName = $this->input->getCmd('countryid');
            $db = JFactory::getDbo();
            //if ($enabled_only)
            $filter = "`status`=1 and ";
            $db->setQuery("SELECT l.* FROM `#__jobsfactory_locations` l WHERE {$filter} `country`='" . $parentName . "' order by `ordering`");

            $locations = $db->loadObjectList();
            $options = array();

            foreach ($locations as $location)
            {
                $location->depth = 1;
                $spacer = str_pad('', ($location->depth - 1) * 3, '-');
                $options[] = JHtml::_('select.option', $location->id, $spacer . stripslashes($location->locname));
            }

            if (!empty($options)) {
                //$html_tree = JHtml::_('select.genericlist', $options, 'location', "onchange='jobsRefreshCities(this);'", 'value', 'text', $options[0]->value);
                $html_tree = JHtml::_('select.genericlist', $options, 'location', "", 'value', 'text', $options[0]->value);
            } else {
                $html_tree = '';
            }

            echo $html_tree;
        }

    }

