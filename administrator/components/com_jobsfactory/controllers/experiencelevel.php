<?php 
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class JJobsfactoryAdminControllerExperiencelevel extends JControllerLegacy
{

    function execute($task)
    {
        if (file_exists(JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.admin.php'))
            require JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.admin.php';

        if (file_exists(JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'experiencelevel.php'))
            require_once JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'experiencelevel.php';

        parent::execute($task);
    }


    function Editexperiencelevel()
    {
        $id = JFactory::getApplication()->input->getInt("id");
        $cid    = $this->input->get('cid', array(0), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        if (!$id && is_array($cid)) {
            $id = $cid[0];
        }

        $experiencelevelModel = JModelLegacy::getInstance('Experiencelevel','JobsAdminModel');
        $experiencelevel = JTable::getInstance('ExperiencelevelTable','JTheFactory');

        if($id){
            if (!$experiencelevel->load($id))
            {
                JError::raiseNotice(101,JText::_("Error loading Experiencelevel")." $id");
                $this->setRedirect("index.php?option=".APP_EXTENSION."&task=experiencelevel.listexperiencelevel");
                return;
            }
        }

        $experiencelevels = $experiencelevelModel->getExperiencelevels();
        $opts = array();
        $lists = array();

        $opts[] = JHtml::_('select.option', '', JText::_("EXPERIENCELEVEL_SELECT"));
        $opts[] = JHtml::_('select.option', 1, JText::_("COM_JOBS_PUBLISHED"));
        $opts[] = JHtml::_('select.option', 0, JText::_("COM_JOBS_UNPUBLISHED"));
        $lists['published'] =	JHtml::_('select.genericlist',  $opts, 'published', 'class="inputbox" alt="published"',  'value', 'text',$experiencelevel->published);

        $view = $this->getView('experiencelevel', 'html');
        $view->assignRef('row',$experiencelevel);
        $view->assignRef('experiencelevels',$experiencelevels);
        $view->assignRef('lists', $lists);

        $view->display('form');
    }

    function SaveExperiencelevel()
    {
        $input = JFactory::getApplication()->input;
        $id = $input->getInt("id");

        $experiencelevelobj = JTable::getInstance('ExperiencelevelTable','JTheFactory');

        if($id) {
            $experiencelevelobj->load($id);
        }

        $data['levelname'] =  $input->getHtml('levelname','');
        $data['published'] = $input->get('published');

        $experiencelevelobj->bind($data);

        if ($experiencelevelobj->store()) {

            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=experiencelevel.listexperiencelevel",JText::_("Experience type saved"));
        } else {
            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=experiencelevel.listexperiencelevel",JText::_("Experience type NOT saved"));
            return false;
        }
    }

    function NewExperiencelevel()
    {
        $experienceleveltable = JTable::getInstance('ExperiencelevelTable','JTheFactory');

        $opts = array();
        $lists = array();

        $opts[] = JHtml::_('select.option', '', JText::_("EXPERIENCELEVEL_SELECT"));
        $opts[] = JHtml::_('select.option', 1,JText::_("COM_JOBS_PUBLISHED"));
        $opts[] = JHtml::_('select.option', 0,JText::_("COM_JOBS_UNPUBLISHED"));
        $lists['published'] =	JHtml::_('select.genericlist',  $opts, 'published', 'class="inputbox" alt="published"',  'value', 'text',1);

        $view = $this->getView('experiencelevel', 'html');
        $view->assignRef('row',$experienceleveltable);
        $view->assignRef('lists', $lists);

        $view->display('form');
    }


    function ApplyExperiencelevel()
    {
        $experiencelevel = self::SaveExperiencelevel();

        if ($experiencelevel)
            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=experiencelevel.edit&id=$experiencelevel->id",JText::_("Experience type saved"));
        else
            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=experiencelevel.listexperiencelevel",JText::_("Experience type not saved"));
                
    }

    function SaveExperiencelevel2New()
    {
        self::SaveExperiencelevel();
        $this->setRedirect("index.php?option=".APP_EXTENSION."&task=experiencelevel.newexperiencelevel",JText::_("Experience type saved"));
        //redirect to new
    }

    function DeleteExperiencelevel()
    {
        $cids = $this->input->get('cid', array(), 'array');
        if (empty($cids)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        if (!is_array($cids)) $cids = array($cids);

        $experiencelevel_object = JTable::getInstance('experiencelevelTable','JTheFactory');
        $i = 0;
        foreach($cids as $cid)
            if ($experiencelevel_object->load($cid)){
                $experiencelevel_object->delete();
                $i++;   
            }
        
        $this->setRedirect("index.php?option=".APP_EXTENSION."&task=experiencelevel.listexperiencelevel",$i.' '.JText::_("Experience type deleted"));
    }

    function ListExperiencelevel()
    {
        /*
         * @var $model JTheFactoryModelFields;
         * */

        $model = JModelLegacy::getInstance('Experiencelevel','JobsAdminModel');
        $filter_page = JFactory::getApplication()->input->getWord('filter_page');

        $filters = array();
        $opts[] = JHtml::_('select.option','',JText::_("All"));
        $filters['page'] = JHtml::_('select.genericlist',$opts,'filter_page',"onchange=this.form.submit()",'value','text',$filter_page);

        $rows = $model->getExperiencelevels(false);
		$pagination = $model->getPagination();
        
        $view = $this->getView('experiencelevel', 'html');
        $view->assign('filter_html',$filters);
        $view->assignRef('rows',$rows);
        $view->assignRef('pagination',$pagination);
        $view->display();
    }

    function Reorder()
    {
        $experiencelevel = JTable::getInstance('ExperiencelevelTable','JTheFactory');

		foreach ($_REQUEST as $k=>$v){
	        if (substr($k,0,6)=='order_'){
	            $id=substr($k,6);
                $experiencelevel->load($id);
                $experiencelevel->ordering = $v;
                $experiencelevel->store();
	        }
	    }

	    $this->setRedirect('index.php?option='.APP_EXTENSION.'&task=experiencelevel.listexperiencelevel',JText::_('Ordering saved'));
    }

}
