<?php 
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

class JJobsfactoryAdminControllerStudieslevel extends JControllerLegacy
{

    function execute($task)
    {
       if (file_exists(JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.admin.php'))
           require JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.admin.php';

       if (file_exists(JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'studieslevel.php'))
           require_once JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'studieslevel.php';

       parent::execute($task);
    }

    function Editstudieslevel()
    {
        $app = JFactory::getApplication();
        $id = $app->input->getInt("id");
        $cid    = $app->input->get('cid', array(), 'array');

        if (!$id && is_array($cid)) {
            $id = $cid[0];
        }

        $studieslevelModel = JModelLegacy::getInstance('Studieslevel','JobsAdminModel');

        $studieslevel = JTable::getInstance('StudieslevelTable','JTheFactory');
        if($id){
            if (!$studieslevel->load($id))
            {
                JError::raiseNotice(101,JText::_("Error loading Studieslevel")." $id");
                $this->setRedirect("index.php?option=".APP_EXTENSION."&task=studieslevel.liststudieslevel");
                return;
            }
        }

        $studieslevels = $studieslevelModel->getStudieslevels();
        $opts = array();
        $lists = array();
        $opts[] = JHtml::_('select.option', '', JText::_("STUDYLEVEL_SELECT"));
        $opts[] = JHtml::_('select.option', 1,JText::_("STUDYLEVEL_PUBLISHED"));
        $opts[] = JHtml::_('select.option', 0,JText::_("STUDYLEVEL_UNPUBLISHED"));
        $lists['published']=	JHtml::_('select.genericlist',  $opts, 'published', 'class="inputbox" alt="published"',  'value', 'text',$studieslevel->published);

        $view = $this->getView('studieslevel', 'html');
        $view->assign('row',$studieslevel);
        $view->assign('studieslevels',$studieslevels);
        $view->assign('lists', $lists);

        $view->display('form');
    }

    function SaveStudieslevel()
    {
        $app = JFactory::getApplication();
        $id = $app->input->getInt('id');
        $studieslevelobj = JTable::getInstance('StudieslevelTable','JTheFactory');

        if($id) {
            $studieslevelobj->load($id);
        }

        $studieslevel_posted = $app->input->getArray($_POST);
        $data['levelname'] = $app->input->getHtml('levelname','');
        $data['published'] = $app->input->get('published');

        // Bind the data.
        if (!$studieslevelobj->bind($data))
        {
            $this->setError($studieslevelobj->getError());
            return false;
   		}

        if ($studieslevelobj->store()) {

            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=studieslevel.liststudieslevel",JText::_("Study level saved"));
        } else {
            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=studieslevel.liststudieslevel",JText::_("Study level NOT saved"));
            return false;
        }
    }

    function NewStudieslevel()
    {
        $studiesleveltable = JTable::getInstance('StudieslevelTable','JTheFactory');

        $opts = array();
        $lists = array();
        $opts[] = JHtml::_('select.option', '', JText::_("STUDYLEVEL_SELECT"));
        $opts[] = JHtml::_('select.option', 1,JText::_("STUDYLEVEL_PUBLISHED"));
        $opts[] = JHtml::_('select.option', 0,JText::_("STUDYLEVEL_UNPUBLISHED"));
        $lists['published']=	JHtml::_('select.genericlist',  $opts, 'published', 'class="inputbox" alt="published"',  'value', 'text',1);

        $view = $this->getView('studieslevel', 'html');
        $view->assign('row',$studiesleveltable);
        $view->assign('lists', $lists);

        $view->display('form');
    }


    function ApplyStudieslevel()
    {
        $studieslevel = self::SaveStudieslevel();

        if ($studieslevel)
            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=studieslevel.edit&id=$studieslevel->id",JText::_("Study level saved"));
        else
            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=studieslevel.liststudieslevel",JText::_("Study level not saved"));
                
    }

    function SaveStudieslevel2New()
    {
        self::SaveStudieslevel();
        $this->setRedirect("index.php?option=".APP_EXTENSION."&task=studieslevel.newstudieslevel",JText::_("Study level saved"));
        //redirect to new
    }

    function DeleteStudieslevel()
    {
        $cids    = $this->input->get('cid', array(), 'array');
        if (empty($cids)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        if (!is_array($cids)) $cids=array($cids);

        $studieslevel_object = JTable::getInstance('studieslevelTable','JTheFactory');
        $i = 0;
        foreach($cids as $cid)
            if ($studieslevel_object->load($cid)){
                $studieslevel_object->delete();
                $i++;   
            }
        
        $this->setRedirect("index.php?option=".APP_EXTENSION."&task=studieslevel.liststudieslevel",$i.' '.JText::_("Experience type deleted"));
    }

    function ListStudieslevel()
    {
        /*
         * @var $model JTheFactoryModelFields;
         * */

        $model = JModelLegacy::getInstance('Studieslevel','JobsAdminModel');
        $filter_page = JFactory::getApplication()->input->getWord('filter_page');

        $filters = array();
        $opts[] = JHtml::_('select.option','',JText::_("All"));
        $filters['page'] = JHtml::_('select.genericlist',$opts,'filter_page',"onchange=this.form.submit()",'value','text',$filter_page);

        $rows = $model->getStudieslevels(false);
		$pagination = $model->getPagination();
        
        $view=$this->getView('studieslevel', 'html');
        $view->assign('filter_html',$filters);
        $view->assignRef('rows',$rows);
        $view->assignRef('pagination',$pagination);
        $view->display();
    }

    function Reorder()
    {
        $studieslevel = JTable::getInstance('StudieslevelTable','JTheFactory');

		foreach ($_REQUEST as $k=>$v){
	        if (substr($k,0,6)=='order_'){
	            $id=substr($k,6);
                $studieslevel->load($id);
                $studieslevel->ordering = $v;
                $studieslevel->store();
	        }
	    }

	    $this->setRedirect('index.php?option='.APP_EXTENSION.'&task=studieslevel.liststudieslevel',JText::_('Ordering saved'));
    }

}
