<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

    defined('_JEXEC') or die('Restricted access');

    class JJobsfactoryAdminControllerLocation extends JControllerLegacy
    {

        function execute($task)
        {
            if (file_exists(JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.admin.php'))
                require JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.admin.php';

            if (file_exists(JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'location.php'))
                require_once JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'location.php';

            parent::execute($task);
        }

        function Locations()
        {
            $app = JFactory::getApplication();
            $cfg = JTheFactoryHelper::getConfig();
            $context = 'com_jobsfactory.location.locations.';
            $reset = JFactory::getApplication()->input->getInt('reset');

            $filter_order = $app->getUserStateFromRequest($context . 'filter_order', 'filter_order', '', 'cmd');
            $filter_order_Dir = $app->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', '', 'word');

            if ($reset) {
                //$app->setUserState($context, null);
                $filter_country = '';
                $filter_approved = '';
                $search = '';
            } else {
                $filter_country = $app->getUserStateFromRequest($context . 'filter_country', 'filter_country', '', 'string');
                $filter_approved = $app->getUserStateFromRequest($context . 'filter_approved', 'filter_approved', '', 'string');
                $search = $app->getUserStateFromRequest($context . 'search', 'search', '', 'string');
            }
            $search = JString::strtolower($search);

            $locationModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');
            $locations = $locationModel->getLocationTree(0,true,0,$reset,$filter_country,$filter_approved,$search);
            $pagination = $locationModel->get('pagination');

            $view = $this->getView('location', 'html');
            $view->assignRef('locations', $locations);
            $view->assignRef('pagination', $pagination);

            $sideBar = JHtmlSidebar::render();
            $view->assignRef('sidebar', $sideBar);
            $view->assign('order_Dir', $filter_order_Dir);
            $view->assign('order', $filter_order);
            $view->assign('filter_country', $filter_country);
            $view->assign('filter_approved', $filter_approved);
            $view->assign('search', $search);
            $view->assign('cfg', $cfg);

            $view->display();
        }

        function LocationJSON()
        {
            $locationModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');
            $startnode = JFactory::getApplication()->input->getInt('node', 0);
            if ($startnode <= 0) $startnode = 0;

            $locations = $locationModel->getLocationsNested($startnode, false);

            foreach ($locations as $location) {
                $locations_arr[] = array(
                    'id'          => $location->id,
                    'locname'        => $locations->locname,
                    'lft'         => $location->left + 1,
                    'rht'         => $location->right + 1,
                    'description' => $location->description
                );
            }
            $obj = new stdClass();
            $obj->success = true;
            $obj->data = $locations_arr;
            header('Content-type:application/json');
            echo json_encode($obj);
            exit;
        }

        function NewLocationJSON()
        {
            $catModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');
            exit;
            $parent = JFactory::getApplication()->input->getInt('parent', 0);
            if ($parent <= 0) $parent = 0;

            $data = JFactory::getApplication()->input->getInt('data');
            $newlocation = json_decode($data);
        }

        function LocationsWidget()
        {
            $view = $this->getView();
            $view->display('widget');
        }

        function DelLocations()
        {
            $cids = JFactory::getApplication()->input->get('cid', array(), 'array');
            $locationModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');
            $nr = $locationModel->delLocation($cids);
            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=location.locations', $nr . " " . JText::_('FACTORY_LOCATIONS_DELETED'));

        }

        function EditLocation()
        {
            $cid = JFactory::getApplication()->input->get('cid', array(), 'array');
            if (is_array($cid)) $cid = $cid[0];

            $locationModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');
            $locationtable = JTable::getInstance('Location', 'Table');
            $locationtable->load($cid);

            $countries = JHtml::_('country.selectlist', 'country', '', $locationtable->country, false); //$location->country

            $view = $this->getView('location', 'html');
            $view->assignRef('row', $locationtable);
            $view->assignRef('countries', $countries);
            $view->display('form');
        }

        function SaveLocation()
        {

            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');
            $locationobj = JTable::getInstance('Location', 'Table');

            //$locationobj->bind($_REQUEST);
            $locationobj->bind(JTheFactoryHelper::getRequestArray('post', 'html'));
            $lang = JFactory::getLanguage();
            $locationobj->hash = $lang->transliterate($locationobj->locname);
            $locationobj->hash = md5(strtolower(JFilterOutput::stringURLSafe($locationobj->hash)));

            if ($locationobj->store()) {
                $locationModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');
                $qu = $locationModel->getLocationPathString($locationobj->id);

                if ('WIN' == substr(PHP_OS, 0, 3)) {
                    $separator = "\r\n";
                } else {
                    $separator = "\n";
                }
            }
            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=location.locations', JText::_('FACTORY_LOCATION_SAVED'));
        }

        function NewLocation()
        {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');
            $locationtable = JTable::getInstance('Location', 'Table');

            $countries = JHtml::_('country.selectlist', 'country', '', '', false); //$location->country id="country"

            $view = $this->getView('location', 'html');
            $view->assignRef('row', $locationtable);
            $view->assignRef('countries', $countries);
            $view->display('form');
        }

        function saveLocationOrder()
        {
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');
            $locationtable = JTable::getInstance('Location', 'Table');

            foreach ($_REQUEST as $k => $v) {
                if (substr($k, 0, 6) == 'order_') {
                    $id = substr($k, 6);
                    $locationtable->load($id);
                    $locationtable->ordering = $v;
                    $locationtable->store();
                }
            }
            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=location.locations', JText::_('FACTORY_ORDERING_SAVED'));
        }

        /**
         * Quick endless level categories to ROOT
         *
         * @modified: 23/09/09
         **/
        function QuickAddLocation()
        {

            $textlocations  = JFactory::getApplication()->input->getString('quickadd', '');
            $parentid       = JFactory::getApplication()->input->getString('parentId','');
            $locationModel  = JModelLegacy::getInstance('Location', 'JobsAdminModel');
            $locationModel->quickAddFromText($textlocations,$parentid);
            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=location.locations', JText::_('FACTORY_LOCATION_ADDED'));
        }

        // move categories in other parent Locations
        function showMoveLocations()
        {
            $database = JFactory::getDbo();
            $cid_arr    = $this->input->get('cid', array(0), 'array');
            if (empty($cid_arr)) {
                JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
            }
            $cid_list = implode(",", $cid_arr);

            $locationModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');

            $database->setQuery("SELECT * FROM `" . $locationModel->location_table . "` WHERE id IN ($cid_list);");
            $changed_locations = $database->loadObjectList();

            $html_tree = JHtml::_('country.selectlist', 'country', '', '', false);

            $view = $this->getView('location', 'html');
            $view->assignRef('cid_list', $cid_list);
            $view->assignRef('locations', $changed_locations);
            $view->assignRef('country', $html_tree);
            $view->display('movelocation');
        }


        function doMoveLocations()
        {
            $database = JFactory::getDbo();
            $app = JFactory::getApplication();
            $cid = $app->input->getString('cid', "0");
            $country = $app->input->getString('country', "0");

            $locationModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');

            $database->setQuery("UPDATE `" . $locationModel->location_table . "` SET country = '{$country}' WHERE id IN ({$cid})");
            $database->execute();
            $nr = $database->getAffectedRows();

            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=location.locations', $nr . " " . JText::_('FACTORY_LOCATIONS_MOVED'));
        }


        function Publish_Location()
        {

            $cids = JFactory::getApplication()->input->get('cid', array(), 'array');
            $nr = count($cids);
            $cids_string = implode(",", $cids);

            $catModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');
            $db = JFactory::getDbo();
            $db->setQuery("UPDATE `" . $catModel->location_table . "` SET status = 1 WHERE id in ($cids_string)");
            $db->execute();

            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=location.locations', $nr . " " . JText::_('FACTORY_LOCATIONS_PUBLISHED'));
        }

        function Unpublish_Location()
        {

            $cids = JFactory::getApplication()->input->get('cid', array(), 'array');
            $nr = count($cids);
            $cids_string = implode(",", $cids);

            $catModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');
            $db = JFactory::getDbo();
            $db->setQuery("UPDATE `" . $catModel->location_table . "` SET status = 0 WHERE id in ($cids_string)");
            $db->execute();

            $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=location.locations', $nr . " " . JText::_('FACTORY_LOCATIONS_UNPUBLISHED'));
        }

    }
