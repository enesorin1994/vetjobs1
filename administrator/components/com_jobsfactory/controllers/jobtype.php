<?php 
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

class JJobsfactoryAdminControllerJobtype extends JControllerLegacy
{

    function execute($task)
    {
        if (file_exists(JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.admin.php'))
            require JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.admin.php';

        if (file_exists(JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'jobtype.php'))
            require_once JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'jobtype.php';

        parent::execute($task);
    }

    function Editjobtype()
    {
        $app = JFactory::getApplication();
        $id = $app->input->getInt("id");
        $cid = $app->input->get('cid', array(), 'array');

        if (!$id && is_array($cid)) {
            $id = $cid[0];
        }

        $jobtypeModel = JModelLegacy::getInstance('Jobtype','JobsAdminModel');
        $jobtype = JTable::getInstance('JobtypeTable','JTheFactory');
        if($id){
            if (!$jobtype->load($id))
            {
                JError::raiseNotice(101,JText::_("Error loading Jobtype")." $id");
                $this->setRedirect("index.php?option=".APP_EXTENSION."&task=jobtype.listjobtype");
                return;
            }
        }

        $jobtypes = $jobtypeModel->getJobtypes();
        $opts = array();
        $lists = array();
        $opts[] = JHtml::_('select.option', '', JText::_("JOBTYPE_SELECT"));
        $opts[] = JHtml::_('select.option', 1,JText::_("COM_JOBS_PUBLISHED"));
        $opts[] = JHtml::_('select.option', 0,JText::_("COM_JOBS_UNPUBLISHED"));
        $lists['published']=	JHtml::_('select.genericlist',  $opts, 'published', 'class="inputbox" alt="published"',  'value', 'text',$jobtype->published);

        $view = $this->getView('jobtype', 'html');
        $view->assignRef('row',$jobtype);
        $view->assignRef('jobtypes',$jobtypes);
        $view->assignRef('lists', $lists);

        $view->display('form');
    }

    function SaveJobtype()
    {
        $id = JFactory::getApplication()->input->getInt("id");
        $jobtypeobj = JTable::getInstance('JobtypeTable','JTheFactory');

        if($id) {
            $jobtypeobj->load($id);
        }

        $input = JFactory::getApplication()->input;
        $data['typename'] = $input->getHtml('typename','');
        $data['published'] = $input->get('published');

        // Bind the data.
        if (!$jobtypeobj->bind($data))
        {
            $this->setError($jobtypeobj->getError());
            return false;
        }

        if ($jobtypeobj->store()) {
            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=jobtype.listjobtype",JText::_("Job type saved"));
        } else {
            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=jobtype.listjobtype",JText::_("Job type NOT saved"));
            return false;
        }
    }

    function NewJobtype()
    {
        $jobtypetable = JTable::getInstance('JobtypeTable','JTheFactory');

        $opts = array();
        $lists = array();
        $opts[] = JHtml::_('select.option', '', JText::_("JOBTYPE_SELECT"));
        $opts[] = JHtml::_('select.option', 1,JText::_("COM_JOBS_PUBLISHED"));
        $opts[] = JHtml::_('select.option', 0,JText::_("COM_JOBS_UNPUBLISHED"));
        $lists['published'] =	JHtml::_('select.genericlist',  $opts, 'published', 'class="inputbox" alt="published"',  'value', 'text',1);

        $view = $this->getView('jobtype', 'html');
        $view->assignRef('row',$jobtypetable);
        $view->assign('lists', $lists);

        $view->display('form');
    }


    function ApplyJobtype()
    {
        $jobtype = self::SaveJobtype();

        if ($jobtype)
            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=jobtype.edit&id=$jobtype->id",JText::_("Job type saved"));
        else
            $this->setRedirect("index.php?option=".APP_EXTENSION."&task=jobtype.listjobtype",JText::_("Job type not saved"));
                
    }

    function SaveJobtype2New()
    {
        self::SaveJobtype();
        $this->setRedirect("index.php?option=".APP_EXTENSION."&task=jobtype.newjobtype",JText::_("Job type saved"));
        //redirect to new
    }

    function DeleteJobtype()
    {
        $cids    = $this->input->get('cid', array(), 'array');
        if (empty($cids)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }
        if (!is_array($cids)) $cids=array($cids);

        $jobtype_object = JTable::getInstance('jobtypeTable','JTheFactory');
        $i=0;        
        foreach($cids as $cid)
            if ($jobtype_object->load($cid)){
                $jobtype_object->delete();
                $i++;   
            }
        
        $this->setRedirect("index.php?option=".APP_EXTENSION."&task=jobtype.listjobtype",$i.' '.JText::_("Job type deleted"));
        
    }

    function ListJobtype()
    {
        /*
         * @var $model JTheFactoryModelFields;
         * */

        $model = JModelLegacy::getInstance('Jobtype','JobsAdminModel');
        $filter_page = JFactory::getApplication()->input->getWord('filter_page');

        $filters = array();
        $opts[] = JHtml::_('select.option','',JText::_("All"));
        $filters['page'] = JHtml::_('select.genericlist',$opts,'filter_page',"onchange=this.form.submit()",'value','text',$filter_page);

        $rows = $model->getJobtypes(false);
		$pagination = $model->getPagination();

        $view = $this->getView('jobtype', 'html');
        $view->assignRef('filter_html',$filters);
        $view->assignRef('rows',$rows);
        $view->assignRef('pagination',$pagination);

        $sideBar = JHtmlSidebar::render();
        $view->assignRef('sidebar', $sideBar);

        $view->display();
    }

    function Reorder()
    {
        $jobtype = JTable::getInstance('JobtypeTable','JTheFactory');

		foreach ($_REQUEST as $k=>$v){
	        if (substr($k,0,6)=='order_'){
	            $id=substr($k,6);
                $jobtype->load($id);
                $jobtype->ordering = $v;
                $jobtype->store();
	        }
	    }

	    $this->setRedirect('index.php?option='.APP_EXTENSION.'&task=jobtype.listjobtype',JText::_('Ordering saved'));
    }

}
