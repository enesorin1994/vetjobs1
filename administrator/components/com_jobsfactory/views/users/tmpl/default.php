<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
<?php endif; ?>

<div id="j-main-container" class="span10">
<form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">
    <table class="table">
      <tr>
          <td valign="top">
            <table class="table table-striped">
                <tr>
                  <th colspan="2"><?php echo JText::_("COM_JOBS_USER_DETAILS"); ?></th>
                </tr>
                <tr>
                   <td><?php echo JText::_("COM_JOBS_USER_GROUP"); ?></td>
                   <td><?php echo ($this->lists['isCompany']) ?  JText::_('COM_JOBS_IS_COMPANY') : JText::_('COM_JOBS_IS_CANDIDATE');?></td>
                </tr>
                <tr>
                    <td width="30%"><?php echo JText::_("COM_JOBS_USERNAME"); ?></td>
                    <td><?php echo $this->user->username; ?></td>
                </tr>
                <tr>
                    <td><?php echo JText::_("COM_JOBS_NAME"); ?></td>
                    <td><?php echo $this->user->name; ?></td>
                 </tr>
                 <tr>
                    <td><?php echo JText::_("COM_JOBS_SURNAME"); ?></td>
                    <td><?php echo $this->user->surname; ?></td>
                 </tr>
                 <?php if ($this->lists['isCompany']) { ?>
                     <tr>
                        <td><?php echo JText::_("COM_JOBS_COMPANY_WEBSITE"); ?></td>
                        <td><?php echo $this->user->website; ?></td>
                     </tr>
                     <tr>
                       <td><?php echo JText::_("COM_JOBS_CONTACT_USERNAME"); ?></td>
                       <td><?php echo $this->user->contactname; ?></td>
                     </tr>
                     <tr>
                       <td><?php echo JText::_("COM_JOBS_CONTACT_PHONE"); ?></td>
                       <td><?php echo $this->user->contactphone; ?></td>
                     </tr>
                     <tr>
                        <td><?php echo JText::_("COM_JOBS_CONTACT_EMAIL"); ?></td>
                        <td><?php echo $this->user->contactemail;?></td>
                     </tr>
                     <tr>
                        <td><?php echo JText::_("COM_JOBS_ABOUT_US"); ?></td>
                        <td><?php if($this->user->about_us) echo $this->user->about_us; else echo "-";?></td>
                     </tr>
                 <?php } ?>
                 <?php if ($this->lists['isCandidate']) { ?>
                     <tr>
                        <td><?php echo JText::_("COM_JOBS_AGE"); ?></td>
                        <td><?php echo $this->lists['age']; ?></td>
                     </tr>
                     <tr>
                        <td><?php echo JText::_("COM_JOBS_PHONE"); ?></td>
                        <td><?php echo $this->user->phone; ?></td>
                     </tr>
                     <tr>
                        <td><?php echo JText::_("COM_JOBS_EMAIL"); ?></td>
                        <td><a href="mailto:<?php echo $this->user->email; ?>"><?php echo $this->user->email;?></a></td>
                     </tr>
                 <?php } ?>
                 <tr>
                    <td><?php echo JText::_("COM_JOBS_ADDRESS"); ?></td>
                    <td><?php echo $this->user->address; ?></td>
                 </tr>
                 <tr>
                    <td><?php echo JText::_("COM_JOBS_CITY"); ?></td>
                    <td><?php echo $this->user->city; ?></td>
                 </tr>
                 <tr>
                    <td><?php echo JText::_("COM_JOBS_COMPANY_IDENTIFICATION_NO"); ?></td>
                    <td><?php echo $this->user->useruniqueid;?></td>
                 </tr>

                <?php if ( isset($this->user_fields) )
                    foreach($this->user_fields as $field )
                    { ?>
                    <tr>
                        <td><?php echo $field->name; ?></td>
                        <td><?php echo $this->user->{$field->db_name}; ?></td>
                    </tr>
                <?php } ?>

                 <tr>
                    <td valign="top"><b><?php echo JText::_("COM_JOBS_STATISTICS"); ?></b></td>
                    <td valign="top">
                        <?php echo JText::_("COM_JOBS_NUMBER"); ?>: <?php echo $this->lists['nr_jobs'];?><br/>
                    </td>
                 </tr>
                 <tr>
                    <td><?php echo JText::_("COM_JOBS_LAST_JOB"); ?></td>
                    <td><?php if($this->lists['last_ads_placed']) echo $this->lists['last_ads_placed']; else echo '-';?></td>
                 </tr>
                 <tr>
                    <td><?php echo JText::_("COM_JOBS_FIRST_JOB"); ?></td>
                    <td><?php if($this->lists['first_ads_placed']) echo $this->lists['first_ads_placed']; else echo "-";?></td>
                 </tr>
            </table>
          </td>
      </tr>
      <tr>
       <td valign="top">
		    <table class="table">
		        <tr>
		    	  <th width="250"><?php echo JText::_("COM_JOBS_USER_BALANCE"); ?></th>
		    	  <td align="left"><?php echo $this->balance->balance," ",$this->balance->currency; ?></td>
		    	</tr>
		   	</table>
       </td>
      </tr>
    </table>

	<input type="hidden" name="option" value="com_jobsfactory">
	<input type="hidden" name="task" value="detailuser">
	<input type="hidden" name="id" value="<?php echo $this->user->userid; ?>">
</form>
</div>