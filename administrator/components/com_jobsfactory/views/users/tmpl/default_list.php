<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
     	<div id="j-sidebar-container" class="span2">
       		<?php echo $this->sidebar; ?>
       	</div>
<?php endif; ?>
<div id="j-main-container" class="span10">
<form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">

    <div id="filter-bar" class="btn-toolbar">
        <div class="filter-search btn-group pull-left">
            <?php echo JText::_( 'COM_JOBS_FILTER' ); ?>:
            <input type="text" name="search" id="search" value="<?php echo $this->search;?>" class="text_area" onchange="document.adminForm.submit();" title="<?php echo JText::_( 'COM_JOBS_FILTER_BY_TITLE_OR_ENTER_ADD_ID' );?>"/>

        </div>

        <div class="btn-group pull-left">
            <button class="btn hasTooltip" type="submit" name="filterbutton" title="<?php echo JText::_('COM_JOBS_GO'); ?>"><i class="icon-search"></i></button>
            <button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('search').value='';this.form.submit();"><i class="icon-remove"></i></button>
        </div>
    </div>

    <table class="table table-striped">
	<thead>
		<tr>
			<th width="4"><?php echo JText::_( 'COM_JOBS_NUM' ); ?></th>
			<th width="5">
                <input type="checkbox" name="toggle" value="" class="checklist-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
            </th>
			<th width="5%" class="title"><?php echo JText::_("COM_JOBS_BLOCKUNBLOCK"); ?></th>
            <th class="title" width="15%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_USERNAME'), 'u.username', $this->order_Dir, $this->order,'users'); ?></th>
            <th class="title"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_NAME'), 'u.name', $this->order_Dir, $this->order,'users' ); ?></th>
            <th width="5%" class="title"><?php echo JText::_("COM_JOBS_VERIFIED"); ?></th>
			<th width="5%" class="title"><?php echo JText::_("COM_JOBS_POWERSELLER"); ?></th>
			<th width="5%" class="title"><?php echo JText::_("COM_JOBS_IS_COMPANY"); ?></th>
			<th width="5%" class="title"><?php echo JText::_("COM_JOBS_IS_CANDIDATE"); ?></th>
            <th width="5%" class="title"><?php echo JText::_("COM_JOBS_TOTAL"); ?></th>
            <th width="5%" class="title"><?php echo JText::_("COM_JOBS_PUBLISHED"); ?></th>
            <th width="5%" class="title"><?php echo JText::_("COM_JOBS_FEATURED"); ?></th>
            <th width="5%" class="title"><?php echo JText::_("COM_JOBS_CLOSED"); ?></th>
            <th width="5%" class="title"><?php echo JText::_("COM_JOBS_BLOCKED"); ?></th>
            <th width="5%" class="title"><?php echo JText::_("COM_JOBS_APPLIED"); ?></th>
			<th class="title" width="20%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_EMAIL'), 'u.email', $this->order_Dir, $this->order,'users' ); ?></th>
			<th align="center" width="20%"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_COUNTRY'), 'b.country',$this->order_Dir, $this->order,'users'); ?></th>
			<th width="3%" class="title"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_USERID'), 'b.userid', $this->order_Dir, $this->order,'users'); ?></th>
		</tr>
	</thead>
	<tfoot>
	<tr>
		<td colspan="18">
			<?php echo $this->pagination->getListFooter(); ?>
		</td>
	</tr>
	</tfoot>
	<tbody>
	<?php
    $countrytable = JTable::getInstance('country', 'Table');

	$k = 0;
	for ($i=0, $n=count($this->users); $i < $n; $i++) {
		$row = &$this->users[$i];
        if (isset($row->country)) {
            $row->country      = $countrytable->getCountryName($row->country);
        }
	    $link 	= 'index.php?option=com_jobsfactory&task=detailUser&cid[]='. $row->userid;

	?>
		<tr class="<?php echo "row$k"; ?>">
			<td>
				<?php echo $this->pagination->getRowOffset( $i ); ?>
			</td>
			<td align="center">
				<?php echo JHTML::_('grid.id', $i, $row->userid ); ?>
			</td>
			<?php

			if ( $row->block) {
                $img = JHtml::_('image','admin/publish_x.png',JText::_( 'COM_JOBS_PUBLISH' ),null,true);
				$alt = JText::_( 'COM_JOBS_CLICK_TO_UNBLOCK_USER' );
			} else {
                $img = JHtml::_('image','admin/publish_g.png',JText::_( 'COM_JOBS_BLOCK_USER' ),null,true);
				$alt = JText::_( 'COM_JOBS_CLICK_TO_BLOCK_USER' );
			}

            if (isset( $row->verified)) {
                if ( $row->verified) {
                    $imgv = 'verified_1.gif';
                    $altv = JText::_( 'COM_JOBS_VERIFIED' );
                } else {
                    $imgv = 'verified_0.gif';
                    $altv = JText::_( 'COM_JOBS_UNVERIFIED' );
                }
            }

			if (isset( $row->powerseller)) {
                if ($row->powerseller) {
				    $imgp = 'verified_1.gif';
				    $altp = JText::_( 'COM_JOBS_UNSET_POWERSELLER' );
                } else {
                    $imgp = 'verified_0.gif';
                    $altp = JText::_( 'COM_JOBS_SET_POWERSELLER' );
                }
            }

			if ( JobsHelperTools::isCompany($row->userid)) {
				$imgs = 'isCompany.png';
				$alts = JText::_( 'COM_JOBS_IS_COMPANY' );
				$tasks='unsetCompany';
			} else {
				$imgs = 'isCompany2.png';
				$alts = JText::_( 'COM_JOBS_NOT_COMPANY' );
                $tasks='setCompany';
			}
			
			if (JobsHelperTools::isCandidate($row->userid) ) {
				$imgb = 'isCandidate.png';
				$altb = JText::_( 'COM_JOBS_IS_CANDIDATE' );
				$taskb='unsetCandidate';
			} else {
				$imgb = 'isCandidate2.png';
				$altb = JText::_( 'COM_JOBS_NOT_CANDIDATE' );
                $taskb='setCandidate';
			}
			?>

			<td nowrap="nowrap" align="center">
				<span class="editlinktip hasTip" title="<?php echo JText::_( $alt );?>">
					<a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->block==='1' ? 'unblockuser' : 'blockuser' ?>')">
							<?php echo $img;?>
					</a>		
				</span>
			</td>
            <td align="left">
                <?php echo $row->username; ?>
            </td>
			<td>
				<?php if($row->id && $this->cfg->profile_mode=='component') { ?>
				    <a href="<?php echo JRoute::_( $link ); ?>"><?php echo $row->surname," ",$row->name; ?></a>
				<?php } else {?>
                    <?php echo (isset($row->surname)) ? $row->surname : ''," ",$row->name; ?>
				<?php } ?>
			</td>
			<?php if (($row->id || $this->cfg->profile_mode!='component') && (isset( $row->verified) && isset( $row->powerseller)) ) { ?>
    			<td nowrap="nowrap" align="center">
    				<span class="editlinktip hasTip" title="<?php echo JText::_( $altv );?>">
    					<a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->verified ? 'unsetVerify':'setVerify' ?>')">
    							<img src="<?php echo JURI::root();?>components/com_jobsfactory/images/<?php echo $imgv;?>" width="16" height="16" border="0" >
    					</a>		
    				</span>	
    			</td>
    			<td nowrap="nowrap" align="center">
    				<span class="editlinktip hasTip" title="<?php echo JText::_( $altp );?>">
    				<a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->powerseller ? 'unsetPowerseller':'setPowerseller' ?>')">
    						<img src="<?php echo JURI::root();?>components/com_jobsfactory/images/<?php echo $imgp;?>" width="16" height="16" border="0" >
    				</a>		
    				</span>
    			</td>
			<?php } else { ?>
                <?php if ($this->cfg->profile_mode =='component') { ?>
    			    <td nowrap="nowrap" align="center" colspan="2"><?php echo JText::_("COM_JOBS_PROFILE_NOT_FILLED");?></td>
                <?php } else { ?>
                    <td nowrap="nowrap" align="center" colspan="2"><?php echo JText::_("COM_JOBS_PROFILE_NOT_DEFINED");?></td>
			    <?php }
                  } ?>

			<td nowrap="nowrap" align="center">
				<span class="editlinktip hasTip" title="<?php echo JText::_( $alts );?>">
				    <a href="index.php?option=com_users&task=user.edit&id=<?php echo $row->userid;?>" >
						<img src="<?php echo JURI::root();?>components/com_jobsfactory/images/<?php echo $imgs;?>" width="24" height="24" border="0" >
					</a>	
				</span>
			</td>
			<td nowrap="nowrap" align="center">
				<span class="editlinktip hasTip" title="<?php echo JText::_( $altb );?>">
                    <a href="index.php?option=com_users&task=user.edit&id=<?php echo $row->userid;?>" >
						<img src="<?php echo JURI::root();?>components/com_jobsfactory/images/<?php echo $imgb;?>" width="24" height="24" border="0" >
					</a>	
				</span>		
			</td>
            <td align="center">
                <?php echo $row->nr_jobs;?>
            </td>
            <td align="center">
                <?php echo $row->nr_published;?>
            </td>
            <td align="center">
                <?php echo $row->nr_featured;?>
            </td>
            <td align="center">
                <?php echo $row->nr_closed;?>
            </td>
            <td align="center">
                <?php echo $row->nr_blocked;?>
            </td>
            <td align="center">
                <?php echo (isset($this->lists['applied'][$row->userid])) ?   $this->lists['applied'][$row->userid]->applied : 0; echo JText::_('COM_JOBS_JOBS'); ?>
            </td>
			<td align="left">
				<?php echo $row->email; ?>
			</td>
			<td nowrap="nowrap">
				<?php echo (isset( $row->country)) ? $row->country : ''; ?>
			</td>
			<td>
				<?php echo $row->userid; ?>
			</td>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	</tbody>
	</table>

<input type="hidden" name="option" value="com_jobsfactory" />
<input type="hidden" name="task" value="users" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $this->order; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->order_Dir; ?>" />
</form>
</div>