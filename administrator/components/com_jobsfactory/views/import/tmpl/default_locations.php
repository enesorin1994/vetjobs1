<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php
    JFactory::getDocument()->addScriptDeclaration(
        "
    		function validateForm(){
    			var locations = document.getElementById('locations');
    			var cities = document.getElementById('cities');

    			if(!locations.value && !cities.value) {
    				alert('".JText::_("COM_JOBS_NO_CSV_FILE")."');
    				return false;
    			}
    		}
        "
    );
?>

<div class='well well-small'>
  <fieldset class="adminform">
    <legend><?php echo JText::_( 'COM_JOBS_IMPORT_LOC_CSV' ); ?></legend>

	  <form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm" onsubmit="return validateForm();" enctype="multipart/form-data">
	  <input type="hidden" name="option" value="com_jobsfactory">
	  <input type="hidden" name="task" value="importexport.importcsvloc">

	  <table width="100%" class="adminlist">
            <tr>
                <td width="150">
                    <?php echo JText::_('COM_JOBS_CSV_LOCATIONS_FILE'); echo JHTML::_('tooltip', JText::_('COM_JOBS_CSV_HELP')); ?>
                </td>
                <td>
                    <input type="file" name="locations" id="locations" size=50>
                </td>
            </tr>
            <tr>
                <td width="100">
                    <?php echo JText::_('COM_JOBS_CSV_CITIES_FILE'); echo JHTML::_('tooltip', JText::_('COM_JOBS_CSV_HELP')); ?>
                </td>
                <td>
                    <input type="file" name="cities" id="cities" size=50>
                </td>
            </tr>
	  </table>
	  </form>
	<div align="left">
        <?php if (!empty($this->errors)) :?>
            <div>
                <strong><?php echo JText::_( 'COM_JOBS_THERE_HAVE_BEEN_ERRORS' ); ?></strong><br />
                <ul style="color: red;">
                <?php foreach($this->errors as $err):?>
                    <li><?php echo $err;?></li>
                <?php endforeach;?>
                </ul>
            </div>
        <?php endif;?>
	</div>

  </fieldset>
</div>