<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php
    JHtml::_('bootstrap.tooltip');
    JHtml::_('behavior.multiselect');
    JHtml::_('dropdown.init');
    JHtml::_('formbehavior.chosen', 'select');
?>

<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
<?php endif; ?>

<?php
    $doc = JFactory::getDocument();
    $doc->addScriptDeclaration(
        "
        	function delecat(nr,enable){
                el=document.getElementById('location_new_'+nr);
                el.disabled=enable;
            }
            function showQuickAdd()
            {
                el=document.getElementById('quickadd');
                el.style.display='block';
                el=document.getElementById('quickaddbutton');
                el.style.display='none';
            }
         "
	);
    ?>
<div class="row-fluid">

 <form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">
    <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
    <input type="hidden" name="task" value="location.locations" />
    <input type="hidden" name="reset" value="0" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $this->order; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $this->order_Dir; ?>" />

     <div class="span10" id="j-main-container">
         <table>
             <tr>
                 <td width="100%">
                     <div class="pull-left">
                         <?php echo JText::_( 'COM_JOBS_FILTER' ); ?>:
                         <input type="text" name="search" id="search" size="30" value="<?php echo $this->search;?>" class="text_area"  title="<?php echo JText::_( 'COM_JOBS_FILTER_BY_TITLE_OR_ENTER_LOCATION_ID' );?>" />
                         <?php echo JText::_( 'COM_JOBS_COUNTRY_FILTER' ); ?>:
                         <input type="text" name="filter_country" id="filter_country" size="30" value="<?php echo $this->filter_country;?>" class="text_area" title="<?php echo JText::_( 'COM_JOBS_BY_COUNTRY' );?>" />

                         <?php if ($this->cfg->admin_approval):?>
                             <?php echo JText::_( 'COM_JOBS_APPROVAL_STATUS' ); ?>:
                             <?php echo JHtml::_('jobapproved.selectlist','filter_approved','',$this->filter_approved); ?>
                         <?php endif;?>
                     </div>
                     <div class="btn-group pull-left">
                         <button class="btn hasTooltip" type="submit" name="filterbutton" title="<?php echo JText::_('COM_JOBS_GO'); ?>"><i class="icon-search"></i></button>
                         <button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="this.form.reset.value='1';this.form.submit();"><i class="icon-remove"></i></button>
                     </div>
                 </td>
                 <td nowrap="nowrap">

                 </td>
             </tr>
         </table>

     <table class="table-striped" width="90%">
     <tr>
        <th width="30">
            <input type="checkbox" name="toggle" value="" class="checklist-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
        </th>
        <th width="20">
            <a href="javascript:submitbutton('location.saveLocationOrder')" class="saveorder" title="<?php echo JText::_('FACTORY_SAVE_ORDERING');?>" ></a>
        </th>
        <th width="25%"><?php echo JHTML::_('grid.sort', JText::_('FACTORY_LOCATIONS'), 'c.locname',$this->order_Dir, $this->order,'location.locations') ; ?></th>
        <th width="*%"><?php echo JHTML::_('grid.sort', JText::_('FACTORY_COUNTRY'),'countryname',$this->order_Dir, $this->order,'location.locations') ; ?></th>
        <th width="5%"><?php echo JHTML::_('grid.sort', JText::_('FACTORY_STATUS'),'c.status',$this->order_Dir, $this->order,'location.locations') ; ?></th>
     </tr>
     <?php foreach($this->locations as $row):?>
     <tr>
        <td><?php echo JHTML::_('grid.id',$row->id,$row->id,false);?></td>
        <td><input name='order_<?php echo $row->id;?>' type='text' class='inputbox input-mini' size='1' value='<?php echo $row->ordering;?>' /></td>
        <td><span style="padding-left: 2px;">
            <sup>|<span style="text-decoration:underline;">&nbsp;&nbsp;&nbsp;</span></sup>
            <a href='index.php?option=<?php echo APP_EXTENSION;?>&task=location.editlocation&cid=<?php echo $row->id;?>'><?php echo $row->locname;?></a>
            </span>
        </td>
        <td><?php echo $row->countryname;?></td>
        <td><?php 
    			if ($row->status ) {
    				$img = 'tick.png';
    				$alt = JText::_( 'FACTORY_PUBLISHED__UNPUBLISH' );
    				$task = "location.unpublish_location";
    			} else {
    				$img = 'publish_x.png';
    				$alt = JText::_( 'FACTORY_UNPUBLISHED__PUBLISH' );
    				$task = "location.publish_location";
    			}
            ?>
               <span class="editlinktip hasTip" title="<?php echo JText::_( 'FACTORY_PUBLISH_INFORMATION')." ".$alt;?>" >
    		 	<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=<?php echo $task;?>&cid[]=<?php echo $row->id;?>" >
                    <?php echo JHtml::_('image','admin/'.$img, null, NULL, $alt); ?>
                </span> 
            </td>
     </tr>
     <?php endforeach;?>
       <tfoot>
         <tr>
             <td colspan="2"><?php echo $this->pagination->getLimitBox(); ?></td>
             <td colspan="3" style="text-align: center">
                 <?php echo $this->pagination->getListFooter(); ?>
             </td>
         </tr>
       </tfoot>
     </table>
     </div>


	<div id="quickaddbutton" class="span8">
		<button class="button"  onclick="showQuickAdd();" type="button"><?php echo JText::_('FACTORY_QUICKADD');?></button>
		</div>
		<div style="display:none;" id="quickadd"><br />
		<span style="font-size:14px;font-weight:bolder"><?php echo JText::_('FACTORY_TO_QUICK_ADD_LOCATIONS'); ?></span><br /><br />
            <span>
                <?php  echo JText::_('FACTORY_COUNTRY');
                    echo JHtml::_('country.selectlist', 'parentId', "id='parentId'", '');
                 ?>
            </span><br />
		<textarea name="quickadd" cols="40" rows="10"></textarea><br />
		<button class="button" onclick="submitform('location.quickaddlocation');"><?php echo JText::_('FACTORY_QUICKADD_LOCATIONS');?></button>
	</div>

</form>
</div>
