<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<form name="adminForm" id="adminForm" method="post" action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>">
    <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
    <input type="hidden" name="task" value="location.doMoveLocations" />
    <input type="hidden" name="cid" value="<?php echo $this->cid_list;?>" />

    <table class="paramlist admintable" width='100%'>
    <tr>
      <td class="paramlist_key" style="width:40% !important;" ><?php echo JText::_("FACTORY_MOVE_LOCATIONS");?>:</td>
      <td>
          <?php foreach ($this->locations as $location){
              echo $location->locname."<br />";
          }?>
      </td>
    </tr>
    <tr>
      <td class="paramlist_key"><?php echo JText::_("FACTORY_TO_LOCATION");?>:</td>
      <td>
        <?php echo $this->country;?>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center" class="paramlist_key">
      &nbsp;
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <a href="#" onclick="document.adminForm.submit();">
            <?php echo JTheFactoryAdminHelper::imageAdmin('move.png', JText::_("FACTORY_MOVE")); ?>
            <?php echo JText::_("FACTORY_MOVE");?></a>
      </td>
    </tr>
    </table>

</form>
