<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php
    JHtml::_('behavior.framework', true);
    $doc=JFactory::getDocument();
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/location/js/jxlib.standalone.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/location/js/locale.english.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/location/js/base.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/location/js/browser.record.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/location/js/browser.adaptor.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/location/js/dialog.location.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/location/js/browser.js');
    $doc->addStyleSheet(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/location/js/themes/crispin/jxtheme.uncompressed.css');
    $doc->addStyleSheet(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/location/js/browser.css');
    $doc->addScriptDeclaration("
            window.addEvent('load', function() {
                Jx.setLanguage('en-US');
                var locationBrowser = new LB.locationBrowser({
                    container: 'tree-container',
                    urls: {
                        insert: 'index.php?option=".APP_EXTENSION."&task=location.newlocationjson',
                        'delete': 'index.php?option=".APP_EXTENSION."&task=location.deletelocationjson',
                        update: 'index.php?option=".APP_EXTENSION."&task=location.updatelocationjson',
                        read: 'index.php?option=".APP_EXTENSION."&task=location.locationjson'
                   }          
                });
            });
    ");
    $doc->addStyleDeclaration("
        .test-container {
            width: 100%;
            height: 500px;
            border: 1px black solid;
        }
    ");

?>
  <div class="test-container" id="tree-container">
  
  </div>
