<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
     	<div id="j-sidebar-container" class="span2">
       		<?php echo $this->sidebar; ?>
       	</div>
<?php endif; ?>

<div id="cpanel" class="row-fluid">
    <div class='span6'>
<?php
  $link = 'index.php?option=com_jobsfactory&amp;task=orders.listing';
  JTheFactoryAdminHelper::quickiconButton( $link, 'admin/paymentitems.png', JText::_("COM_JOBS_VIEW_ORDERS" ));

  $link = 'index.php?option=com_jobsfactory&amp;task=payments.listing';
  JTheFactoryAdminHelper::quickiconButton( $link, 'admin/payments.png', JText::_("COM_JOBS_VIEW_PAYMENTS" ));

    $link = 'index.php?option=com_jobsfactory&amp;task=balances.listing';
    JTheFactoryAdminHelper::quickiconButton( $link, 'admin/payments.png', JText::_("COM_JOBS_USER_PAYMENT_BALANCES" ));

  ?>
    </div>
</div>
<div style="height:100px">&nbsp;</div>
<div style="clear:both;"> </div>
