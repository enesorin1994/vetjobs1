<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
	// Access the file from Joomla environment
	defined('_JEXEC') or die('Restricted access');
	$doc = JFactory::getDocument();
    $cfg = JTheFactoryHelper::getConfig();

	$doc->addScriptDeclaration("
            window.addEvent('domready', function(){
                load_gmaps();
                gmap_selectposition.delay(500);
            });
        ");

?>
<h3 style = "color:#FAA000; text-decoration:underline;"><?php echo JText::_("COM_JOBS_GOOGLEMAPS"); ?></h3>
<div>

	<?php if ($cfg->google_key != ""): ?>

    <div id = "map_canvas" style = "width: 500px; height: 300px"></div>

	<?php else: ?>
	<?php echo JText::_('COM_JOBS_NO_GOOGLE_MAP_DEFINED'); ?>
	<?php endif; ?>
</div>
<div style = "text-align:center;width: 500px; ">
    <input type = "text" readonly = "readonly" id = "gmap_posx" value = "<?php echo $this->googleMapX; ?>" />
    <input type = "text" readonly = "readonly" id = "gmap_posy" value = "<?php echo $this->googleMapY; ?>" />
    <br />

    <h3><a href = "#" style = "color:#FAA000; font-weight:bold;" onclick = "submit_gmap_coords()"><?php echo JText::_("COM_JOBS_SELECT"); ?></a></h3>
</div>
