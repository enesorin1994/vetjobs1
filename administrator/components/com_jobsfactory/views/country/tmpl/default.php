<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php
    JHtml::_('bootstrap.tooltip');
    JHtml::_('behavior.multiselect');
    JHtml::_('dropdown.init');
    JHtml::_('formbehavior.chosen', 'select');
?>

<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
<?php endif; ?>

<div class="row-fluid">

<form name="adminForm" id="adminForm" action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="get">
<input type="hidden" name="option" value="com_jobsfactory" />
<input type="hidden" name="task" value="countries.listing" />
<input type="hidden" name="boxchecked" value="" />

    <div class="span10" id="j-main-container">
<table class="table-striped" width="80%">
<tr>
	<th colspan="4" align="right">
		<?php echo JText::_("COM_JOBS_FILTER"),':';?><input type="text" name="searchfilter" value="<?php echo $this->search;?>" />
		<?php echo JText::_("COM_JOBS_PUBLISHED"),':';?>
		<?php echo $this->active_filter;?>
	</th>
</tr>
<tr>
	<th width="5">
        <input type="checkbox" name="toggle" value="" class="checklist-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
    </th>
	<th><?php echo JText::_("COM_JOBS_COUNTRY");?></th>
	<th align="center"><?php echo JText::_("COM_JOBS_COUNTRY_CODE");?></th>
	<th align="center" width="35"><?php echo JText::_("COM_JOBS_PUBLISHED");?></th>
</tr>
<?php 
$odd=0;
foreach ($this->countries as $k => $country) {?>
<tr class="row<?php echo ($odd=1-$odd);?>">
	<td align="center">
		<?php echo JHTML::_('grid.id', $k, $country->id );?>
	</td>
	<td><?php echo $country->name;?></td>
	<td align="center"><?php echo $country->simbol;?></td>
	<td align="center">
		<?php 
		$link 	= 'index.php?option=com_jobsfactory&task=countries.toggle&cid[]='. $country->id;
		if ( $country->active == 1 ) {
			$img = 'tick.png';
			$alt = JText::_( 'COM_JOBS_PUBLISHED__UNPUBLISH' );
		} else {
			$img = 'publish_x.png';
			$alt = JText::_( 'COM_JOBS_UNPUBLISHED__PUBLISH' );
		}
		?>
		<span class="editlinktip hasTip" title="<?php echo JText::_( 'Publish Information<br />'.$alt );?>">
			<a href="<?php echo $link;?>" >
                <?php echo JTheFactoryAdminHelper::imageAdmin($img,$alt);?>
		    </a>
		</span>
	</td>
</tr>
<?php } ?>
<tr>
	<th colspan="4" align="right">
		<?php echo $this->pagination->getListFooter();?>
	</th>
</tr>
</table>
        </div>
</form>
</div>
