<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php 
	$app = JFactory::getApplication();
    $message=$app->getUserState('com_installer.extension_message');
    
    echo $message;
?>
<table width="100%">
<tr>
    <td>
        <h1>
        The installation detected that you already had a previous installed version of Jobs Factory.
        </h1>
    </td>
</tr>
<tr>
    <td>
        <h2>
        The previously existing Jobs Template folder WAS NOT overwritten in order to preserve any changes you might have done. If you like to overwrite the contents of the template folder please click the button below
        </h2>            
    </td>        
</tr>
<tr>
    <td>
        <a href="#" onclick="if(confirm('Are you sure that you want to overwrite your existing Jobs templates?')) window.location='index.php?option=com_jobsfactory&task=installtemplates'">
        Overwrite Tempates now!
        </a>
    </td>        
</tr>
</table>
