<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
       	<div id="j-sidebar-container" class="span2">
       		<?php echo $this->sidebar; ?>
       	</div>
       <?php endif; ?>

<?php
    JFactory::getDocument()->addScriptDeclaration(
        "
    	function submitbutton(action){
    		if (document.adminForm.message.value || action==''){
    		    submitform(action);
    			return true;
    		}
    		alert('".JText::_("COM_JOBS_YOU_CAN_NOT_SEND_AN_EMPTY_MESSAGE")."');

    	}
        "
    );
?>
<div id="j-main-container" class="span10">
     <form method="post" action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" name="adminForm" id="adminForm">
      <table width="100%">
       <tr>
    	<td><h2><?php echo JText::_("COM_JOBS_TO"); ?>:&nbsp;<?php echo $this->user->name;?>(<?php echo $this->user->username;?>)</h2></td>
       </tr>
       <tr>
    	<td><h3><?php echo JText::_("COM_JOBS_REGARDING_JOB"); ?>:&nbsp;<?php echo $this->job->title;?></h3></td>
       </tr>
       <tr>
    	<td><?php echo JText::_("COM_JOBS_JOB_ID"); ?>:&nbsp;<?php echo $this->job->id;?></td>
       </tr>
       <tr>
        <td>
        <textarea name="message" class="jobmessage" cols="80" rows="10"></textarea>
        </td>
       </tr>
       <tr>
        <td>
        <input type="submit" name="submit" value="<?php echo JText::_("COM_JOBS_SEND"); ?>" class="inputbutton"/>
        </td>
       </tr>
      </table>
    	 <input type="hidden" name="job_id" value="<?php echo $this->job->id;?>"/>
    	 <input type="hidden" name="option" value="com_jobsfactory"/>
    	 <input type="hidden" name="task" value="send_message_job"/>
         <input type="hidden" name="return_task" value="<?php echo $this->return_task;?>"/>
     </form>
</div>
