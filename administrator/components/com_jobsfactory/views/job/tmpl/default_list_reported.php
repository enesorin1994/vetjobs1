<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
       	<div id="j-sidebar-container" class="span2">
       		<?php echo $this->sidebar; ?>
       	</div>
       <?php endif; ?>

<?php
JHtml::_('behavior.multiselect');
?>
<div id="j-main-container" class="span10">
	<form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">
	<input type="hidden" name="option" value="com_jobsfactory" />
	<input type="hidden" name="task" value="reported_jobs" />
	<input type="hidden" name="status" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->order; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->order_Dir; ?>" />
	<table>
		<tr>
			<td width="100%">
				<?php echo JText::_( 'COM_JOBS_FILTER' ); ?>:
				<input type="text" name="search" id="search"  size="30" value="<?php echo $this->search;?>" class="text_area" title="<?php echo JText::_( 'COM_JOBS_FILTER_BY_TITLE_OR_ENTER_ADD_ID' );?>"/>
				<button onclick="this.form.submit();"><?php echo JText::_( 'COM_JOBS_GO' ); ?></button>
                <?php echo JText::_( 'COM_JOBS_FILTER_STATUS' ); ?>:&nbsp;
				<select name="filter_solved" onchange="this.form.submit();">
					<option value=""  <?php if($this->solved=='')  echo "selected"; ?> ><?php echo JText::_("COM_JOBS_ALL");?></option>
					<option value="1" <?php if($this->solved=='1') echo "selected"; ?> ><?php echo JText::_("COM_JOBS_SOLVED");?></option>
					<option value="0" <?php if($this->solved=='0') echo "selected"; ?> ><?php echo JText::_("COM_JOBS_UNSOLVED");?></option>
				</select>
			</td>
			<td nowrap="nowrap" align="right">
			</td>
		</tr>
	</table>

	<table class="table table-striped" cellspacing="1">
	<thead>
		<tr>
			<th width="5"><?php echo JText::_( 'COM_JOBS_NUM' ); ?></th>
			<th width="5">
                <input type="checkbox" name="toggle" value="" class="checklist-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
            </th>
			<th width="6%" class="center"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_SOLVED__IN_PROCESS'), 'solved', $this->order_Dir, $this->order,'reported_jobs' ); ?></th>
			<th class="center" width="15%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_REPORTED_DATE'), 'modified', $this->order_Dir, $this->order ,'reported_jobs'); ?></th>
			<th class="left" width="25%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_REPORTED_JOB'), 'title', $this->order_Dir, $this->order,'reported_jobs' ); ?></th>
			<th class="center" width="10%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_REPORTED_BY'), 'username', $this->order_Dir, $this->order,'reported_jobs' ); ?></th>
			<th class="center"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_MESSAGE'), 'message', $this->order_Dir, $this->order,'reported_jobs' ); ?></th>
			<th></th>
		</tr>
	</thead>
	<tfoot>
	<tr>
		<td colspan="9">
			<?php echo $this->pagination->getListFooter(); ?>
		</td>
	</tr>
	</tfoot>
	<tbody>
	<?php
	$k = 0;
	for ($i=0, $n=count($this->jobs); $i < $n; $i++) {
		$row = &$this->jobs[$i];
		$link 	= 'index.php?option=com_jobsfactory&task=edit&cid[]='. $row->job_id;
		$img 	= $row->solved ? 'tick.png':'publish_x.png';
		$img2 	= $row->processing ? 'tick.png' : 'publish_x.png';
		$task1 	= $row->solved ? 'unsolved' : 'solved';
		$alt 	= $row->solved ?  JText::_('COM_JOBS_SOLVED__CLICK_TO_REOPEN'):JText::_('COM_JOBS_UNSOLVED__CLICK_TO_MARK_AS_DONE');
		$message=(strlen($row->message)>30)?substr($row->message,0,30)."..":$row->message;
		$title=(strlen($row->title)>30)?substr($row->title,0,30)."..":$row->title;
		if($row->close_by_admin) $title.="(".JText::_("COM_JOBS_CLOSED_BY_ADMIN")." )";

		?>
		<tr class="<?php echo "row$k"; ?>">
			<td>
				<?php echo $this->pagination->getRowOffset( $i ); ?>
			</td>
			<td class="center">
				<?php echo JHTML::_('grid.id', $i, $row->job_id ); ?>
			</td>
		 	<td class="center">
				<span class="editlinktip hasTip">
					<a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->solved ? 'unsolved' : 'solved' ?>')">
                    	<img src="<?php echo JURI::root();?>components/<?php echo APP_EXTENSION;?>/images/<?php echo $img;?>" width="12" height="12" border="0" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>"/>
					</a>
				</span>
			</td>
			<td class="center">
				<?php echo htmlspecialchars($row->modified, ENT_QUOTES); ?>
			</td>
			<td>
				<a href="<?php echo JRoute::_( $link ); ?>"><?php echo htmlspecialchars($title, ENT_QUOTES); ?></a>
			</td>
			<td class="center">
				<?php if($row->username) echo htmlspecialchars($row->username, ENT_QUOTES); else echo "---"; ?>
			</td>
			<td class="center">
				<?php echo htmlspecialchars($message, ENT_QUOTES);; ?>
			</td>
			<td>
				<a href="index.php?option=com_jobsfactory&task=write_admin_message&return_task=reported_jobs&job_id=<?php echo $row->job_id; ?>"><?php echo JText::_("COM_JOBS_WRITE_MESSAGE"); ?></a>
			</td>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	</tbody>
	</table>
</form>
</div>