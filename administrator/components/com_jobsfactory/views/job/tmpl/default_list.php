<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.multiselect');
    if(!empty( $this->sidebar)): ?>
        <div id="j-sidebar-container" class="span2">
            <?php echo $this->sidebar; ?>
        </div>
    <?php endif; ?>
<div id="j-main-container" class="span10">
<form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">
	<table>
		<tr>
			<td width="100%">
                <div class="pull-left">
                    <?php echo JText::_( 'COM_JOBS_FILTER' ); ?>:
                    <input type="text" name="search" id="search" size="30" value="<?php echo $this->search;?>" class="text_area"  title="<?php echo JText::_( 'COM_JOBS_FILTER_BY_TITLE_OR_ENTER_ADD_ID' );?>" />
                    <?php echo JText::_( 'COM_JOBS_USERNAME_FILTER' ); ?>:
                    <input type="text" name="filter_authorid" id="filter_authorid" size="30" value="<?php echo $this->filter_authorid;?>" class="text_area" title="<?php echo JText::_( 'COM_JOBS_BY_USERNAME' );?>" />

                    <?php if ($this->cfg->admin_approval):?>
                        <?php echo JText::_( 'COM_JOBS_APPROVAL_STATUS' ); ?>:
                        <?php echo JHtml::_('jobapproved.selectlist','filter_approved','',$this->filter_approved); ?>
                    <?php endif;?>
                </div>
                <div class="btn-group pull-left">
                    <button class="btn hasTooltip" type="submit" name="filterbutton" title="<?php echo JText::_('COM_JOBS_GO'); ?>"><i class="icon-search"></i></button>
                    <button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('search').value='';document.id('filter_authorid').value='';this.form.submit();"><i class="icon-remove"></i></button>
                </div>
			</td>
			<td nowrap="nowrap">

			</td>
		</tr>
	</table>
    <table class="table table-striped">
	<thead>
		<tr>
			<th width="5"><?php echo JText::_( 'COM_JOBS_NUM' ); ?></th>
			<th width="5">
                <input type="checkbox" name="toggle" value="" class="checklist-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
            </th>
			<th><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_POSITION'), 'a.title', $this->order_Dir, $this->order,'jobs' ); ?></th>
            <?php if ($this->cfg->admin_approval):?>
    			<th width="1%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_APPROVAL_STATUS'), 'a.approved', $this->order_Dir, $this->order,'jobs' ); ?></th>
            <?php endif;?>
			<th width="1%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_PUBLISHED'), 'a.published', $this->order_Dir, $this->order,'jobs' ); ?></th>
			<th width="1%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_RUNNING_CLOSED'), 'a.close_offer', $this->order_Dir, $this->order,'jobs' ); ?></th>
            <th align="center" width="80">
                <?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_NR_CANDIDATES'), 'nr_applicants', $this->order_Dir, $this->order,'jobs' ); ?>
            </th>
            <th align="center" width="10"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_HITS'), 'a.hits', $this->order_Dir, $this->order,'jobs' ); ?></th>
			<th align="center" width="20"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_EXPIRE_DATE'), 'a.end_date', $this->order_Dir, $this->order,'jobs' ); ?></th>
			<th width="1%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_BLOCKED'), 'a.close_by_admin', $this->order_Dir, $this->order,'jobs' ); ?></th>
			<th width="3%"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_ID'), 'a.id', $this->order_Dir, $this->order,'jobs' ); ?></th>
		</tr>
	</thead>
	<tfoot>
	<tr>
         <td colspan="2"><?php echo $this->pagination->getLimitBox(); ?></td>
         <td colspan="10" style="text-align: center">
             <?php echo $this->pagination->getListFooter(); ?>
         </td>
	</tr>
	</tfoot>
	<tbody>
	<?php
	$k = 0;
	for ($i=0, $n=count($this->jobs); $i < $n; $i++) {
		$row = &$this->jobs[$i];
		$link 	= 'index.php?option=com_jobsfactory&task=edit&cid[]='. $row->id;
		$link_user 	= 'index.php?option=com_jobsfactory&task=detailUser&cid[]='. $row->userid;

        $img_publish=($row->published) ? JHtml::_('image','admin/tick.png','','',JText::_( 'COM_JOBS_PUBLISHED')) : JHtml::_('image','admin/disabled.png','','',JText::_( 'COM_JOBS_DISABLED'));
        $alt_publish=($row->published) ? JText::_( 'COM_JOBS_PUBLISHED__CLICK_TO_UNPUBLISH' ) : JText::_( 'COM_JOBS_UNPUBLISHED__CLICK_TO_PUBLISH' );

        $img_block=($row->close_by_admin) ? JHtml::_('image','admin/publish_r.png','','',JText::_( 'COM_JOBS_CLOSED_BY_ADMIN') ) : JHtml::_('image','admin/disabled.png','','',JText::_( 'COM_JOBS_OPENED'));
        $alt_block=($row->close_by_admin) ? JText::_( 'COM_JOBS_BLOCKED__CLICK_TO_UNBLOCK' ):JText::_( 'COM_JOBS_RUNNING__CLICK_TO_BLOCK' );

        $img_close=($row->close_offer) ? JHtml::_('image','admin/publish_r.png','','',JText::_( 'COM_JOBS_CLOSED')) : JHtml::_('image','admin/tick.png','','',JText::_( 'COM_JOBS_OPENED'));
        $alt_close=($row->close_offer) ? JText::_( 'COM_JOBS_CLOSED__CLICK_TO_OPEN' ):JText::_( 'COM_JOBS_RUNNING__CLICK_TO_CLOSE' );

        $img_approved=($row->approved) ? JHtml::_('image','admin/tick.png','','',JText::_( 'COM_JOBS_APPROVED')) : JHtml::_('image','admin/publish_x.png','','',JText::_( 'COM_JOBS_PENDING'));
        $alt_approved=($row->approved) ? JText::_( 'COM_JOBS_APPROVED__CLICK_TO_CHANGE' ):JText::_( 'COM_JOBS_PENDING_APPROVAL__CLICK_TO_CHANGE' );
		?>
		<tr class="<?php echo "row$k"; ?>">
			<td class="right">
				<?php echo $this->pagination->getRowOffset( $i ); ?>
			</td>
			<td class="center">
				<?php echo JHTML::_('grid.id', $i, $row->id );?>
			</td>
			<td>
				<a href="<?php echo JRoute::_( $link ); ?>"><?php echo htmlspecialchars($row->title, ENT_QUOTES); ?></a><br />
				<br />
                    <?php echo JText::_( 'COM_JOBS_AUTHOR');?>: <a href="<?php echo JRoute::_( $link_user ); ?>"><?php echo htmlspecialchars($row->editor, ENT_QUOTES); ?></a><br />
                    <?php echo JText::_( 'COM_JOBS_CATEGORY');?>: <?php if($row->name) echo htmlspecialchars($row->name, ENT_QUOTES); else echo "---"; ?><br />
			</td>
            <?php if ($this->cfg->admin_approval):?>
    			<td class="center">
    				<span class="editlinktip hasTip" title="<?php echo JText::_( $alt_approved );?>">
    				    <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','approve_toggle')">
    					<?php echo $img_approved;?></a>
    				</span>
    			</td>
            <?php endif;?>
			<td class="center">
				<span class="editlinktip hasTip" title="<?php echo JText::_( $alt_publish );?>">
				    <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->published ? 'unpublish' : 'publish' ?>')">
					<?php echo $img_publish;?></a>
				</span>
			</td>
			<td class="center">
				<span class="editlinktip hasTip" title="<?php echo JText::_( $alt_close );?>">
				    <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->close_offer ? 'openoffer' : 'closeoffer' ?>')">
					    <?php echo $img_close;?>
				    </a>
				</span>
			</td>
            <td nowrap="nowrap" class="right">
                <?php echo ($row->nr_applicants==0)?"0":$row->nr_applicants; ?>&nbsp;
            </td>
            <td nowrap="nowrap" class="right">
                <?php echo $row->hits; ?>
            </td>
			<td nowrap="nowrap" class="center">
				<?php echo $row->end_date; ?>
			</td>

			<td class="center">
				<span class="editlinktip hasTip" title="<?php echo JText::_( $alt_block );?>">
				    <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->close_by_admin ? 'unblock' : 'block' ?>')">
					    <?php echo $img_block;?>
				    </a>
				</span>
			</td>

			<td class="right">
				<?php echo $row->id; ?>
			</td>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	</tbody>
	</table>
<input type="hidden" name="option" value="com_jobsfactory" />
<input type="hidden" name="task" value="jobs" />
<input type="hidden" name="reset" value="0" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $this->order; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->order_Dir; ?>" />
</form>
</div>