<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.calendar');
JHtml::_('bootstrap.tooltip');

$editorName = JFactory::getConfig()->get('editor');
$editor = JEditor::getInstance($editorName);

$editorBenefitsName = JFactory::getConfig()->get('editor');
$editorBenefits = JEditor::getInstance($editorBenefitsName);

JFilterOutput::objectHTMLSafe( $this->row, ENT_QUOTES );

    if(!empty( $this->sidebar)): ?>
    	<div id="j-sidebar-container" class="span2">
    		<?php echo $this->sidebar; ?>
    	</div>
    <?php endif; ?>
<style>
    fieldset input, fieldset textarea, fieldset select, fieldset img, fieldset button {
        float: none;
    }
    .job_notice{
        font-size:120%;
        font-weight: bolder;
        color:#FFFF00;
        background-color:#A80000;
        -moz-border-radius: 15px;
        border-radius: 12px;   
        padding:20px 10px 20px 10px;
        border: 1px solid #000;
        text-align: center;
    }
</style>

<div class="span8">
    <?php if ($this->cfg->admin_approval && !$this->row->approved) :?>
        <div class="job_notice"><?php echo JText::_("COM_JOBS_THIS_JOB_REQUIRES_ADMIN_APPROVAL"); ?></div>
    <?php endif;?>
</div>

<div id="j-main-container" class="span10">
  <form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">

  <table width="100%">
    <tr>
        <td width="50%">
            <table class="admintable" style="width:auto;">
            <tr>
                <td class="paramlist_key">
                    <label for="title" width="100"><?php echo JText::_( 'COM_JOBS_POSITION' ); ?></label>
                </td>
                <td class="paramlist_value">
                    <input class="text_area required" type="text" name="title" id="title" value="<?php echo $this->row->title; ?>" size="50" maxlength="50"  />
                </td>
            </tr>
            <tr>
                <td class="paramlist_key">
                    <label width="100"><?php echo JText::_( 'COM_JOBS_CATEGORY' ); ?></label>
                </td>
                <td class="paramlist_value"><?php echo $this->category; ?></td>
            </tr>
            <tr>
                <td class="paramlist_key">
                    <label class="job_lables"><?php echo JText::_( 'COM_JOBS_EXPERIENCE_REQUEST');?></label>
                </td>
                <td class="paramlist_value">
                    <?php echo $this->experience_request; ?>
                </td>
            </tr>
            <tr>
                <td class="paramlist_key">
                   <label class="job_lables"><?php echo JText::_( 'COM_JOBS_STUDIES_REQUEST');?></label>
               </td>
               <td class="paramlist_value">
                   <?php echo $this->studies_request; ?>
               </td>
            </tr>
            <tr>
                <td class="paramlist_key">
                    <label class="job_lables"><?php echo JText::_( 'COM_JOBS_LANGUAGES_REQUEST');?></label>
                </td>
                <td class="paramlist_value">
                    <input class="inputbox" type="text" name="languages_request" value="<?php echo $this->row->languages_request;?>" size="40" />
                </td>
            </tr>
            <tr>
                <td class="paramlist_key">
                    <label class="job_lables"><?php echo JText::_( 'COM_JOBS_NO_JOBS_AVAILABLE');?></label>
                </td>
                <td class="paramlist_value">
                    <input class="inputbox validate-numeric required" type="text" size="7" name="jobs_available" value="<?php echo $this->row->jobs_available;?>" alt="jobs_available">
                </td>
            </tr>
            <tr>
                <td class="job_dbk_c" align="right">
                    <?php if ($this->cfg->enable_location_management) { ?>
                        <label class="job_lables"><?php echo JText::_( 'COM_JOBS_JOB_USE_COUNTRY_LOCATION');?></label>
                    <?php } else { ?>
                        <label class="job_lables"><?php echo JText::_( 'COM_JOBS_JOB_USE_COUNTRY');?></label>
                    <?php } ?>
                </td>
                <td class="job_dbk_c">
                    <?php echo $this->use_location; ?>
                </td>
            </tr>


            <?php if ($this->cfg->enable_location_management) { ?>
            <?php if ($this->countries != '' && $this->use_location != '') { ?>
                <tr id="countrylist">
                    <td class="paramlist_key">
                        <label class="job_lables"><?php echo JText::_( 'COM_JOBS_JOB_COUNTRY_LOCATION');?></label>
                    </td>
                    <td class="paramlist_value">
                       <?php if (!empty($this->countries) ) { ?>
                            <?php echo $this->countries; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr><td class="job_dbk_c" style="text-align: center;" colspan="2"><span id='countryError' style='color:red'></span></td></tr>
            <?php } ?>

            <tr id="statelist">
               <td class="paramlist_key">
                   <label class="job_lables"><?php echo JText::_( 'COM_JOBS_JOB_STATE_LOCATION');?></label>
               </td>
               <td class="paramlist_value">
                   <div id="states" name="states">
                    <?php  echo $this->states; ?>
                   </div>
               </td>
            </tr>
            <?php } ?>
            <tr>
                <td class="paramlist_key">
                    <label class="job_lables"><?php echo JText::_( 'COM_JOBS_JOB_CITY_LOCATION');?></label>
                </td>
                <td class="paramlist_value">
                   <?php if ($this->cfg->enable_location_management && !empty($this->cities) ) { ?>
                            <div id="cities" name="cities">
                                <?php echo $this->cities; ?>
                            </div>
                        <?php } else { ?>
                            <input class="inputbox required" type="text" size="60" name="job_cityname" value="<?php echo $this->row->job_cityname;?>" alt="job_cityname">
                        <?php } ?>
                </td>
            </tr>
            <tr><td class="paramlist_value" colspan="2"><br /></td></tr>
            <tr>
                <td class="paramlist_key"><?php echo JText::_("COM_JOBS_FEATURED_DISPLAY");?></td>
                <td class="paramlist_value"><?php echo $this->featured; ?></td>
            </tr>
            <tr>
                <td class="paramlist_value" colspan="2"><br /></td>
            </tr>
            <tr>
                <td class="paramlist_key">
                    <label width="100"><?php echo JText::_( 'COM_JOBS_SHORT_DESCRIPTION' ); ?></label>
                </td>
                <td class="paramlist_value">
                    <textarea name="shortdescription" style="width:500px;" rows="10"><?php echo $this->row->shortdescription; ?></textarea>
                </td>
            </tr>
            <tr>
                <td class="paramlist_key"><label width="100"><?php echo JText::_( 'COM_JOBS_DESCRIPTION' ); ?></label></td>
                <td class="paramlist_value">
                    <?php echo $editor->display( 'description',  $this->row->description, '500', '300', '10', '10', array('pagebreak', 'readmore') ) ;?>
                </td>
            </tr>
            <tr>
                <td class="paramlist_key"><label width="100"><?php echo JText::_( 'COM_JOBS_BENEFITS' ); ?></label></td>
                <td class="paramlist_value">
                    <?php echo $editorBenefits->display( 'benefits',  $this->row->benefits, '500', '300', '10', '10', array('pagebreak', 'readmore') ) ;?>
                </td>
            </tr>
            <tr>
                <td class="paramlist_key">
                    <label width="100"><?php echo JText::_( 'COM_JOBS_TAGS' ); ?></label>
                </td>
                <td class="paramlist_value">
                    <input type="text" name="tags" size="50" value="<?php echo $this->row->tags; ?>"/>
                </td>
            </tr>
                <tr>
                    <td class="paramlist_key">
                        <label width="100"><?php echo JText::_( 'COM_JOBS_SHOW_CANDIDATES_NUMBER' ); ?></label>
                    </td>
                    <td class="paramlist_value">
                        <?php echo JHTML::_('select.booleanlist','show_candidate_nr','',$this->row->show_candidate_nr);?>
                    </td>
                </tr>
                <tr>
                    <td class="paramlist_key">
                        <label width="100"><?php echo JText::_( 'COM_JOBS_ATTACHED_FILE' ); ?></label>
                    </td>
                    <td class="paramlist_value">
                        <?php if  ($this->row->has_file) : ?>
                            <?php echo $this->row->file_name;?>
                            <input type="checkbox" name="delete_attach" value="1">
                        <?php else: ?>
                            <input type="file" name="attachment" value="" class="inputbox">
                        <?php endif;?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="custom_fields_box">
                    <fieldset>
                        <legend><?php echo JText::_('COM_JOBS_CUSTOM_FIELDS');?></legend>
                        <?php
                           echo JHtml::_('customfields.displayfieldshtml',$this->row,$this->fields);
                        ?>
                    </fieldset>
                    </td>
                </tr>

            </table>
        </td>
        <td valign="top">
        <?php
                ///////////////////// PANE DETAILS START //////////////////////////////////
            echo JHtml::_('bootstrap.startAccordion', 'jobs-pane', array('allowAllClose' => 'true'));
            echo JHtml::_('bootstrap.addSlide', 'jobs-pane', JText::_('COM_JOBS_DETAILS'), 'detail-page');
        ?>
            <table width="100%" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
                <tr>
                    <td valign="top">
                        <label width="100">
                            <?php echo JText::_( 'COM_JOBS_HITS' ); ?>
                        </label>
                    </td>
                    <td><?php echo $this->row->hits;?></td>
                </tr>
                <tr>
                    <td valign="top">
                        <label width="100">
                            <?php echo JText::_( 'COM_JOBS_PUBLISH_STATUS' ); ?>
                        </label>
                    </td>
                    <td><?php echo JHTML::_('select.booleanlist','published','',$this->row->published);?></td>
                </tr>
                <tr>
                    <td>
                        <label width="100"><?php echo JText::_("COM_JOBS_JOB_CLOSED");?></label>
                    </td>
                    <td>
                        <?php echo JHTML::_('select.booleanlist','close_offer','',$this->row->close_offer);?>
                    </td>
                </tr>
                <tr>
                    <td><label width="100"><?php echo JText::_("COM_JOBS_CLOSED_BY_ADMIN"); ?></label></td>
                    <td>
                        <?php echo JHTML::_('select.booleanlist','close_by_admin','',$this->row->close_by_admin); ?><br/>
                        <?php if ($this->row->close_offer==1) echo "Closed on ".$this->row->closed_date; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label width="100">
                            <?php echo JText::_( 'COM_JOBS_JOB_TYPE' ); ?>
                        </label>
                    </td>
                    <td>
                        <?php echo JHtml::_('jobtype.selectlist','job_type','',$this->row->job_type); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label width="100">
                            <?php echo JText::_( 'COM_JOBS_START_DATE' ); ?>
                        </label>
                    </td>
                    <td>
                        <?php //echo JHtml::_('calendar', $this->row->start_date, 'start_date', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19')); ?>
                        <?php echo JHtml::_('calendar', JHtml::date($this->row->start_date, 'Y-m-d H:i:s'), 'start_date', 'start_date', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19')); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label width="100">
                            <?php echo JText::_( 'COM_JOBS_END_DATE' ); ?>
                        </label>
                    </td>
                    <td>
                        <?php echo JHtml::_('calendar', JHtml::date($this->row->end_date, 'Y-m-d H:i:s'), 'end_date', 'end_date', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19')); ?>
                    </td>
                </tr>
            </table>
        <?php ///////////////////// PANE DETAILS END //////////////////////////////////    ?>
    <?php
            echo JHtml::_('bootstrap.endSlide');
            ///////////////////// PANE MESSAGES START //////////////////////////////////
            $title = JText::_("COM_JOBS_MESSAGES");
            echo JHtml::_('bootstrap.addSlide', 'jobs-pane', $title, 'messages-page');
        ?>
        <table class="adminlist" width="100%" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
            <tr>
                <th><?php echo JText::_("COM_JOBS_FROM");?></th>
                <th><?php echo JText::_("COM_JOBS_TO");?></th>
                <th><?php echo JText::_("COM_JOBS_MESSAGE");?></th>
                <th><?php echo JText::_("COM_JOBS_DATE");?></th>
            </tr>
            <?php
            if(isset($this->row->messages) && count($this->row->messages))
            foreach ($this->row->messages as $k => $m) { ?>
            <tr>
                <td><?php echo $m->fromuser; ?></td>
                <td><?php echo $m->touser; ?></td>
                <td><span class="editlinktip hasTip" title="<?php echo $m->message,".."; ?>"><?php echo substr($m->message,0, 20); ?></span></td>
                <td><?php echo $m->modified; ?></td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="4" align="center"><input type="button" class='button' onclick="location.href='index.php?option=com_jobsfactory&task=write_admin_message&return_task=jobs&job_id=<?php echo $this->row->id; ?>'" value="&nbsp;&nbsp;&nbsp;<?php echo JText::_("COM_JOBS_SEND"); ?>&nbsp;&nbsp;&nbsp;"></td>
            </tr>
        </table>
    <?php ///////////////////// PANE MESSAGES END //////////////////////////////////    ?>
    <?php
        ///////////////////// PANE USER DETAILS START //////////////////////////////////
            echo JHtml::_('bootstrap.endSlide');
            $title = JText::_( 'COM_JOBS_EMPLOYER_DETAILS' );
            echo JHtml::_('bootstrap.addSlide', 'jobs-pane', $title, 'employerdetail-page');
        ?>
        <table width="100%" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
            <tr>
                <th colspan="2"><?php echo JText::_("COM_JOBS_USER_DETAILS");?></th>
            </tr>
            <tr>
                <td><?php echo JText::_("COM_JOBS_USERNAME");?></td>
                <td><?php echo $this->row->userdetails->username;?></td>
            </tr>
            <tr>
                <td><?php echo JText::_("COM_JOBS_NAME");?></td>
                <td><?php echo $this->row->userdetails->name, " ", $this->row->userdetails->surname;?></td>
            </tr>
            <?php if(isset($this->row->userdetails->phone)) { ?>
            <tr>
                <td><?php echo JText::_("COM_JOBS_PHONE");?></td>
                <td><?php echo $this->row->userdetails->phone; ?></td>
            </tr>
            <?php } ?>
            <?php if(isset($this->row->userdetails->email)) { ?>
            <tr>
                <td><?php echo JText::_("COM_JOBS_EMAIL");?></td>
                <td><?php echo $this->row->userdetails->email; ?></td>
            </tr>
            <?php } ?>
            <?php if(isset($this->row->userdetails->address)) { ?>
            <tr>
                <td><?php echo JText::_("COM_JOBS_ADDRESS");?></td>
                <td><?php echo $this->row->userdetails->address; ?></td>
            </tr>
            <?php } ?>
            <tr>
                <td><?php echo JText::_("COM_JOBS_CITY");?></td>
                <td><?php echo $this->row->userdetails->city; ?></td>
            </tr>
            <tr>
                <td><?php echo JText::_("COM_JOBS_COUNTRY");?></td>
                <td><?php echo $this->row->userdetails->country; ?></td>
            </tr>
            <tr>
                <td><?php echo JText::_("COM_JOBS_YM");?></td>
                <td><?php echo $this->row->userdetails->YM; ?></td>
            </tr>
            <?php if ( isset($this->user_fields) )
                foreach($this->user_fields as $field )
                { ?>
                <tr>
                    <td><?php echo $field->name; ?></td>
                    <td><?php echo $this->row->userdetails->{$field->db_name}; ?></td>
                </tr>
                <?php } ?>
        </table>
        <?php ///////////////////// PANE USER DETAILS END //////////////////////////////////    ?>
        <?php
            echo JHtml::_('bootstrap.endSlide');
            echo JHtml::_('bootstrap.endAccordion');
        ?>
        </td>
    </tr>
    </table>

	<input type="hidden" name="option" value="com_jobsfactory" />
	<input type="hidden" name="id" value="<?php echo $this->row->id; ?>" />
	<input type="hidden" name="cid" value="<?php echo $this->row->id; ?>" />
	<input type="hidden" name="task" value="save" />
</form>

  </div>
<script type="text/javascript" language="javascript">
    function jobsRefreshCities(locationselect)
    {
        var selectCountry = document.getElementById("job_country").value;

        if (typeof(selectCountry) !== 'undefined' && selectCountry !== '') {

            document.getElementById("countryError").innerHTML = "";

            var selectVal = document.getElementById("job_location_state");

            if (selectVal !== null && selectVal.value != '') {
                var urli = "index.php?option=com_jobsfactory&task=RefreshCity&format=raw&locationid="+selectVal.value;

                    var FieldsAjaxCall = new Request({
                        method: 'get',
                        url: urli,
                        onComplete: jobsDisplayCities

                    }).send();
               } else {
                   document.getElementById("cities").innerHTML = 'No city defined for this state';
               }

        } else {
            document.getElementById("countryError").innerHTML = "You must select a country!";
           	isValid = false;
            return;
        }
    }

function jobsDisplayCities() {
    var cities = this.response.text;
    document.getElementById("cities").innerHTML = cities;
}

function jobsRefreshLocations(countryselect)
{
    document.getElementById("countryError").innerHTML = "";
    var selectVal = document.getElementById("job_country").value;
    var urli = "index.php?option=com_jobsfactory&task=RefreshLocation&format=raw&countryid="+selectVal;

    var FieldsAjaxCall = new Request({
        method: 'get',
        url: urli,
        onComplete: jobsDisplayLocations

    }).send();
}

function jobsDisplayLocations()
{
    var locations = this.response.text;

    if (locations != '') {
        document.getElementById("states").innerHTML = locations;
        jobsRefreshCities(this);
    } else {
        document.getElementById("states").innerHTML = 'No state defined for this country';
        jobsRefreshCities(this);
    }
}

</script>