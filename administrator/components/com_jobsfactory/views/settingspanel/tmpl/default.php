<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>
<?php
   $doc = JFactory::getDocument();
   $doc->addStyleDeclaration("
        table.admintable td.paramlist_key 
        {
            width:170px !important;
        }
    ");
?>
<div id="cpanel" class="row-fluid">
    <div style="width: 450px;float:left; text-align: center;">
        <div class='well well-small'>
            <fieldset class="adminform">
            <legend><?php echo JText::_( 'COM_JOBS_CONTROL_PANEL' ); ?></legend>
            
            <?php
                $link = 'index.php?option=com_jobsfactory&amp;task=config.display';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/settings.png', JText::_("COM_JOBS_GENERAL_SETTINGS") );
                $link = 'index.php?option=com_jobsfactory&amp;task=category.categories';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/categories.png', JText::_("COM_JOBS_MANAGE_CATEGORIES"));
                $link = 'index.php?option=com_jobsfactory&amp;task=fields.listfields';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/fieds_manager.png', JText::_("COM_JOBS_CUSTOM_FIELDS") );

                $link = 'index.php?option=com_jobsfactory&amp;task=jobtype.listjobtype';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/jobtypes.png', JText::_("COM_JOBS_JOB_TYPES" ));
                $link = 'index.php?option=com_jobsfactory&amp;task=experiencelevel.listexperiencelevel';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/experience.png', JText::_("COM_JOBS_EXPERIENCE_LEVELS"));
                $link = 'index.php?option=com_jobsfactory&amp;task=studieslevel.liststudieslevel';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/studies.png', JText::_("COM_JOBS_STUDIES_LEVELS"));

                $link = 'index.php?option=com_jobsfactory&amp;task=city.cities';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/cities.png', JText::_("COM_JOBS_MANAGE_CITIES") );
                $link = 'index.php?option=com_jobsfactory&amp;task=location.locations';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/locations.png', JText::_("COM_JOBS_MANAGE_LOCATIONS") );
                $link = 'index.php?option=com_jobsfactory&amp;task=countries.listing';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/country_manager.png', JText::_("COM_JOBS_COUNTRIES_MANAGER") );

                $link = 'index.php?option=com_jobsfactory&amp;task=currencies.listing';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/currency_manager.png', JText::_("COM_JOBS_CURRENCY_MANAGER") );
                $link = 'index.php?option=com_jobsfactory&amp;task=pricing.listing';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/paymentitems.png', JText::_("COM_JOBS_PAYMENT_ITEMS" ));
                $link = 'index.php?option=com_jobsfactory&amp;task=gateways.listing';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/paymentconfig.png', JText::_("COM_JOBS_PAYMENT_GATEWAYS"));

                $link = 'index.php?option=com_jobsfactory&amp;task=mailman.mails';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/mailsettings.png', JText::_("COM_JOBS_MAIL_SETTINGS" ));
                $link = 'index.php?option=com_jobsfactory&amp;task=integration';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/cb_integration.png', JText::_("COM_JOBS_PROFILE_INTEGRATION"));
                $link = 'index.php?option=com_jobsfactory&amp;task=themes.listthemes';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/themes.png', JText::_("COM_JOBS_THEMES") );

                $link = 'index.php?option=com_jobsfactory&amp;task=importexport.importexport';
                JTheFactoryAdminHelper::quickiconButton( $link, 'settings/update.png', JText::_("COM_JOBS_IMPORT_EXPORT") );
              ?>
            </fieldset>
        </div>
    </div>
    <div class="span4" style="float:left;width: 500px;">
        <div class='well well-small'>
            <fieldset class="adminform">
            <legend><?php echo JText::_( 'COM_JOBS_VERSION_INFORMATION' ); ?></legend>
            	<table class="span8">
            	<tr>
            		<td class="paramlist_key" width="190"><?php echo JText::_("COM_JOBS_YOUR_INSTALLED_VERSION");?></td>
            		<td><?php echo COMPONENT_VERSION; ?></td>
            	</tr>
            	<tr>
            		<td class="paramlist_key" width="190"><?php echo JText::_("COM_JOBS_ACTIVE_PROFILE_INTEGRATION");?></td>
            		<td><?php 
                        switch ($this->cfg->profile_mode)
                        {
                            case 'component':
                                    echo JText::_('COM_JOBS_COMPONENT_PROFILE');
                                break;
                            case 'cb':
                                    echo JText::_('COM_JOBS_COMMUNITY_BUILDER');
                                break;
                            case 'love':
                                    echo JText::_('COM_JOBS_LOVE_FACTORY');
                                break;
                        }
                     ?></td>
            	</tr>
            	<tr>
            		<td class="paramlist_key" width="190"><?php echo JText::_("COM_JOBS_CURRENT_ACTIVE_THEME");?></td>
            		<td><?php echo $this->cfg->theme; ?></td>
            	</tr>
            	<tr>
            		<td class="paramlist_key" width="190"><?php echo JText::_("COM_JOBS_CURRENT_ENABLED_GATEWAYS");?></td>
            		<td><strong>
                        <ul>
                        <?php 
                        foreach($this->gateways as $gateway)
                            echo "<li>",$gateway->paysystem,"</li>";                       
                        ;?>
                        </ul>
                        </strong>
                    </td>
            	</tr>
            	<tr>
            		<td class="paramlist_key" width="190"><?php echo JText::_("COM_JOBS_CURRENT_ENABLED_PAYMENT_ITEMS");?></td>
            		<td><strong>
                        <ul>
                        <?php 
                        foreach($this->items as $item)
                            echo "<li>",$item->itemname,"</li>";                       
                        ;?>
                        </ul>
                        </strong>
                    </td>
            	</tr>
                </table>
            </fieldset>
        </div>
        <div class='well well-small'>
            <fieldset class="adminform">
            <legend><?php echo JText::_( 'COM_JOBS_CRON_INFORMATION' ); ?></legend>
            	<table class="span8">
            	<tr>
            		<td class="paramlist_key" width="190"><?php echo JText::_("COM_JOBS_LATEST_CRON_TASK_RUN");?></td>
                    <td><?php echo intval($this->latest_cron_time) ? JHtml::date($this->latest_cron_time, $this->cfg->date_format . ' ' . $this->cfg->date_time_format) : $this->latest_cron_time; ?></td>

            	</tr>
            	<tr>
            		<td class="paramlist_key" width="190"><?php echo JText::_("COM_JOBS_READ_MORE_ABOUT_CRON_JOB_SETUP");?></td>
            		<td><a href="index.php?option=com_jobsfactory&task=cronjob_info"><?php echo JText::_("COM_JOBS_CLICK_HERE");?></a></td>
            	</tr>
                </table>
            </fieldset>
        </div>
    </div>
</div>
<div style="height:100px">&nbsp;</div>
<div style="clear:both;"> </div>

