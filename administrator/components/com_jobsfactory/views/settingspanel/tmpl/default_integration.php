<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<form name="adminForm" id="adminForm" method="post" action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>">
<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
<input type="hidden" name="task" value="changeprofileintegration" />
    <div class='width-100 fltlft'>
    	<fieldset class="adminform">
    		<legend><?php echo JText::_( 'COM_JOBS_USER_PROFILE_SETTINGS' ); ?></legend>
    		<table class="paramlist admintable" width="100%">
                <tr>
                    <td width="20%">
                        <?php echo "<img alt='Tooltip' src='" . JURI::root() . "components/" . APP_EXTENSION . "/images/tooltip.png' />"; ?>
                        <?php echo JText::_("COM_JOBS_USER_PROFILE_CB_IMPORTANT");  ?>
                    </td>
                    <td width="80%"><?php echo JText::_("COM_JOBS_USER_PROFILE_CB_IMPORTANT_INFO");  ?>
                        <?php echo JText::_("COM_JOBS_USER_PROFILE_CB_IMPORTANT_INFO_CANDIDATES");  ?>
                        <?php echo JText::_("COM_JOBS_USER_PROFILE_CB_IMPORTANT_INFO2");  ?>

                    </td>
                </tr>
                <tr>
    				<td width="20%" class="paramlist_key"><?php echo JText::_("COM_JOBS_USER_PROFILE");  ?>: </td>
    				<td width="80%">
                        <?php echo $this->profile_select_list;?>
                        <input name="save" type="submit" class="btn btn-primary" style="vertical-align: top; margin-top:1px;" value="<?php echo JText::_('COM_JOBS_SAVE');?>"/>
    				</td>
    			</tr>
                <?php if ($this->current_profile_mode<>'component'):?>
    			<tr>
    				<td width="20%" class="paramlist_key"><?php echo JText::_("COM_JOBS_CONFIGURE_INTEGRATION");  ?>:</td>
    				<td width="80%">
                        <div style='background: none repeat scroll 0 0 #dedede;' class='btn featured_box_content'>
                            <a class='btn-link' href="<?php echo $this->configure_link;?>"><?php echo JText::_("COM_JOBS_SET_UP_FIELD_ASSIGNMENTS");  ?></a>
                        </div>
    				</td>
    			</tr>
                <?php endif; ?> 
    			<tr>
    				<td width="20%"></td>
    				<td width="80%">
    				</td>
    			</tr>
    		</table> 
        </fieldset>
    </div>
</form>
