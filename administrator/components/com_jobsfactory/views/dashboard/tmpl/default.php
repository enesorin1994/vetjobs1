<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
     	<div id="j-sidebar-container" class="span2">
       		<?php echo $this->sidebar; ?>
       	</div>
<?php endif; ?>
<div id="j-main-container" class="span10">
<form action="<?php echo JRoute::_('index.php?option=com_jobsfactory&task=dashboard'); ?>" method="post" name="adminForm" id="adminForm">
<div class="span10">

    <table class="table span10">
		<tr>
			<td width="33%" align="center">
                <table width="100%" class="paramlist admintable">
                <tr>
                    <td class="paramlist_key"><?php echo JText::_("COM_JOBS_TOTAL"); ?>:</td>
                    <td class="paramlist_value"><strong><?php  echo $this->lists["nr_total"];?></strong></td>
                </tr>
                <tr>
                    <td class="paramlist_key"><?php echo JText::_("COM_JOBS_ACTIVE"); ?>:</td>
                    <td class="paramlist_value"><strong><?php  echo $this->lists["nr_active"];?></strong></td>
                </tr>
                <tr>
                    <td class="paramlist_key"><?php echo JText::_("COM_JOBS_PUBLISHED"); ?>:</td>
                    <td class="paramlist_value"><strong><?php  echo $this->lists["nr_published"];?></strong></td>
                </tr>
                </table>
			</td>
			<td width="33%" align="center">
                <table width="100%" class="paramlist admintable">
                <tr>
                    <td class="paramlist_key"><?php echo JText::_("COM_JOBS_UNPUBLISHED"); ?>:</td>
                    <td class="paramlist_value"><strong><?php  echo $this->lists["nr_unpublished"];?></strong></td>
                </tr>
                <tr>
                    <td class="paramlist_key"><?php echo JText::_("COM_JOBS_EXPIRED"); ?>:</td>
                    <td class="paramlist_value"><strong><?php  echo $this->lists["nr_expired"];?></strong></td>
                </tr>
                <tr>
                    <td class="paramlist_key"><?php echo JText::_("COM_JOBS_BLOCKED"); ?>:</td>
                    <td class="paramlist_value"><strong><?php  echo $this->lists["nr_blocked"];?></strong></td>
                </tr>
                </table>
			</td>
			<td align="center">
                <table width="100%" class="paramlist admintable">
                <tr>
                    <td class="paramlist_key"><?php echo JText::_("COM_JOBS_REGISTERED_USERS"); ?>:</td>
                    <td class="paramlist_value"><strong><?php  echo $this->lists["nr_r_users"];?></strong></td>
                </tr>
                <tr>
                    <td class="paramlist_key"><?php echo JText::_("COM_JOBS_ACTIVE_USERS"); ?>:</td>
                    <td class="paramlist_value"><strong><?php  echo $this->lists["nr_a_users"] . ' ( ' .$this->lists["nr_a_candidates"] .' candidates )';?></strong></td>
                </tr>
                <tr>
                    <td class="paramlist_key"><?php echo JText::_("COM_JOBS_MESSAGES"); ?>:</td>
                    <td class="paramlist_value"><strong><?php  echo $this->lists["nr_messages"];?></strong></td>
                </tr>
                </table>
			</td>
		</tr>
	</table>

    <!-- Lates Jobs -->

    <table class="table table-striped table-condensed">
	<thead>
		<tr>
            <th class="right" width="3%"><?php echo JText::_('COM_JOBS_ID'); ?></th>
            <th width="5">
                <input type="checkbox" name="toggle" value="" class="checklist-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
            </th>
			<th width="38%"><?php echo JText::_('COM_JOBS_POSITION'); ?></th>
            <?php if ($this->cfg->admin_approval):?>
    			<th width="1%" nowrap="nowrap"><?php echo JText::_('COM_JOBS_APPROVAL_STATUS'); ?></th>
            <?php endif;?>
			<th width="1%" nowrap="nowrap"><?php echo JText::_('COM_JOBS_PUBLISHED'); ?></th>
			<th width="1%" nowrap="nowrap"><?php echo JText::_('COM_JOBS_RUNNING_CLOSED'); ?></th>
            <th class="right" width="10"><?php echo JText::_('COM_JOBS_NR_CANDIDATES'); ?></th>
            <th class="right" width="10"><?php echo JText::_('COM_JOBS_HITS'); ?></th>
			<th class="center" width="20"><?php echo JText::_('COM_JOBS_EXPIRE_DATE'); ?></th>
			<th width="1%" nowrap="nowrap"><?php echo JText::_('COM_JOBS_BLOCKED'); ?></th>

		</tr>
	</thead>
	<tfoot>
	<tr>
		<td colspan="12">
			<?php echo $this->pagination->getListFooter(); ?>
		</td>
	</tr>
	</tfoot>
	<tbody>
	<?php
	$k = 0;
	for ($i=0, $n=count($this->jobs); $i < $n; $i++) {
		$row = &$this->jobs[$i];
		$link 	= 'index.php?option=com_jobsfactory&task=edit&cid[]='. $row->id;
		$link_user 	= 'index.php?option=com_jobsfactory&task=detailUser&cid[]='. $row->userid;

        $img_publish=($row->published) ? JHtml::_('image','admin/tick.png','','',JText::_( 'COM_JOBS_PUBLISHED')) : JHtml::_('image','admin/disabled.png','','',JText::_( 'COM_JOBS_DISABLED'));
        $alt_publish=($row->published) ? JText::_( 'COM_JOBS_PUBLISHED__CLICK_TO_UNPUBLISH' ) : JText::_( 'COM_JOBS_UNPUBLISHED__CLICK_TO_PUBLISH' );

        $img_block=($row->close_by_admin) ? JHtml::_('image','admin/publish_r.png','','',JText::_( 'COM_JOBS_CLOSED_BY_ADMIN') ) : JHtml::_('image','admin/disabled.png','','',JText::_( 'COM_JOBS_OPENED'));
        $alt_block=($row->close_by_admin) ? JText::_( 'COM_JOBS_BLOCKED__CLICK_TO_UNBLOCK' ):JText::_( 'COM_JOBS_RUNNING__CLICK_TO_BLOCK' );

        $img_close=($row->close_offer) ? JHtml::_('image','admin/publish_r.png','','',JText::_( 'COM_JOBS_CLOSED')) : JHtml::_('image','admin/tick.png','','',JText::_( 'COM_JOBS_OPENED'));
        $alt_close=($row->close_offer) ? JText::_( 'COM_JOBS_CLOSED__CLICK_TO_OPEN' ):JText::_( 'COM_JOBS_RUNNING__CLICK_TO_CLOSE' );

        $img_approved=($row->approved) ? JHtml::_('image','admin/tick.png','','',JText::_( 'COM_JOBS_APPROVED')) : JHtml::_('image','admin/publish_x.png','','',JText::_( 'COM_JOBS_PENDING'));
        $alt_approved=($row->approved) ? JText::_( 'COM_JOBS_APPROVED__CLICK_TO_CHANGE' ):JText::_( 'COM_JOBS_PENDING_APPROVAL__CLICK_TO_CHANGE' );

		?>
		<tr class="<?php echo "row$k"; ?>">
            <td class="right"><?php echo $row->id; ?></td>
            <td class="center">
                <?php echo JHTML::_('grid.id', $i, $row->id );?>
            </td>
			<td>
				<a href="<?php echo JRoute::_( $link ); ?>"><?php echo htmlspecialchars($row->title, ENT_QUOTES); ?></a><br />
				<br />
                    <?php echo JText::_( 'COM_JOBS_AUTHOR');?>: <a href="<?php echo JRoute::_( $link_user ); ?>"><?php echo htmlspecialchars($row->editor, ENT_QUOTES); ?></a><br />
                    <?php echo JText::_( 'COM_JOBS_CATEGORY');?>: <?php if($row->name) echo htmlspecialchars($row->name, ENT_QUOTES); else echo "---"; ?><br />
			</td>
            <?php if ($this->cfg->admin_approval):?>
    			<td class="center">
    				<span class="editlinktip hasTip" title="<?php echo JText::_( $alt_approved );?>">
    				    <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','approve_toggle')">
    					<?php echo $img_approved;?></a>
    				</span>
    			</td>
            <?php endif;?>
			<td class="center">
				<span class="editlinktip hasTip" title="<?php echo JText::_( $alt_publish );?>">
				    <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->published ? 'unpublish' : 'publish' ?>')">
					<?php echo $img_publish;?></a>
				</span>
			</td>
			<td class="center">
				<span class="editlinktip hasTip" title="<?php echo JText::_( $alt_close );?>">
				    <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->close_offer ? 'openoffer' : 'closeoffer' ?>')">
					    <?php echo $img_close;?>
				    </a>
				</span>
			</td>
            <td nowrap="nowrap" class="right">
                <?php echo ($row->nr_applicants==0)?"0":$row->nr_applicants; ?>&nbsp;
            </td>
            <td nowrap="nowrap" class="right">
                <?php echo $row->hits; ?>
            </td>
			<td nowrap="nowrap" class="center">
				<?php echo $row->end_date; ?>
			</td>

			<td class="center">
				<span class="editlinktip hasTip" title="<?php echo JText::_( $alt_block );?>">
				    <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->close_by_admin ? 'unblock' : 'block' ?>')">
					    <?php echo $img_block;?>
				    </a>
				</span>
			</td>


		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	</tbody>
	</table>

    <!-- Latest Payments -->
    <table class="table table-striped table-condensed" cellspacing="1">
        <thead>
        <tr>
            <th width="60" class="center"><?php echo JText::_("FACTORY_PAYMENTID"); ?></th>
            <th width="200" class="left"><?php echo JText::_("FACTORY_PAYMENT_DATE"); ?></th>
            <th width="200" class="left"><?php echo JText::_("FACTORY_PAYMENT_STATUS"); ?></th>
            <th class="title" width="250"><?php echo JText::_("FACTORY_USERNAME");?></th>
            <th class="title" width="150" nowrap="nowrap"><?php echo JText::_("FACTORY_PAYMENT_AMOUNT");?></th>
            <th class="title" width="10%"><?php echo JText::_("FACTORY_ORDER_NUMBER");?></th>
            <th class="right" width="10%"><?php echo JText::_("FACTORY_REF_NUMBER"); ?></th>
            <th class="title" width="10%"><?php echo JText::_("FACTORY_PAYMENT_METHOD"); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $k=0;
        for ($i=0, $n=count($this->payments); $i<$n; $i++)
        {
            $row= &$this->payments[$i];

            $link_orderdetails='index.php?option='.APP_EXTENSION.'&task=orders.viewdetails&id='.$row->orderid;

            $link_paymentdetails='index.php?option='.APP_EXTENSION.'&task=payments.viewdetails&id='.$row->id;
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td class="right"><?php echo $row->id;?></td>
            <td class="left"><?php echo $row->date;?></td>
            <td class="left"><strong style="font-size: 110%"><?php echo $row->status;?></strong></td>
            <td class="left"><?php echo JHtml::_('link', 'index.php?option='.APP_EXTENSION.'&task=detailUser&cid[]='.$row->userid, $row->username);?></td>
            <td class="center">
                <a href="<?php echo $link_paymentdetails;?>"
                   title="<?php echo JText::_("FACTORY_VIEW_PAYMENT_DETAILS");?>">
                    <?php echo $row->amount, " ", $row->currency; ?>
                </a>
            </td>
            <td class="center">
                <a href="<?php echo $link_orderdetails;?>"
                   title="<?php echo JText::_("FACTORY_VIEW_ORDER_DETAILS"); ?>">
                    <?php echo $row->orderid;?>
                </a>
            </td>
            <td class="right"><?php echo $row->refnumber;?></td>
            <td class="left"><?php echo $row->payment_method;?></td>

        </tr>
            <?php
            $k=1-$k;
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="15">
                <?php echo $this->pagination->getListFooter(); ?>
            </td>
        </tr>
        </tfoot>
    </table>

</div>

      <input type="hidden" name="option" value="com_jobsfactory" />
      <input type="hidden" name="task" value="dashboard" />
      <input type="hidden" name="reset" value="0" />
      <input type="hidden" name="boxchecked" value="0" />
</form>
</div>

