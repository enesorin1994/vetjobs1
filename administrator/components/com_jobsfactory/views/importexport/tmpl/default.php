<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<div id="cpanel" class="row-fluid">
    <div class='well well-small'>
        <fieldset class="adminform">
        <legend><?php echo JText::_( 'COM_JOBS_IMPORTEXPORT_JOBS' ); ?></legend>
        
        <?php
          $link = 'index.php?option=com_jobsfactory&amp;task=importexport.showadmimportform';
          JTheFactoryAdminHelper::quickiconButton( $link, 'tools/importjobs.png', JText::_("COM_JOBS_IMPORT_CSV") );
        
          $link = 'index.php?option=com_jobsfactory&amp;task=importexport.exportToXls';
          JTheFactoryAdminHelper::quickiconButton( $link, 'tools/xlsexport.png', JText::_("COM_JOBS_EXPORT_XLS"));
        ?>
        </fieldset>
    </div>
    <div class='well well-small'>
        <fieldset class="adminform">
          <legend><?php echo JText::_( 'COM_JOBS_IMPORTEXPORT_JOBS_LOC' ); ?></legend>

            <?php
              $link = 'index.php?option=com_jobsfactory&amp;task=importexport.showadmimportformloc';
              JTheFactoryAdminHelper::quickiconButton( $link, 'tools/importjobs.png', JText::_("COM_JOBS_IMPORT_LOC_CSV") );

              $link = 'index.php?option=com_jobsfactory&amp;task=importexport.exportToXlsLoc';
              JTheFactoryAdminHelper::quickiconButton( $link, 'tools/xlsexport.png', JText::_("COM_JOBS_EXPORT_LOC_XLS"));

            ?>
        </fieldset>
      </div>
</div>
<div style="height:100px">&nbsp;</div>
<div style="clear:both;"> </div>
