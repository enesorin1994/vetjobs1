<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php
        $cfg=CustomFieldsFactory::getConfig();
    ?>
<form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">

	<h2><?php echo JText::_("COM_JOBS_SELECT_CUSTOM_FIELDS_TO_EXPORT");?></h2>
    <table class="span8 table table-striped">
    	<tr>
    		<th width="25"><?php echo JText::_("COM_JOBS_CHECK");?></th>
    		<th><?php echo JText::_("COM_JOBS_FIELD_NAME");?></th>
    		<th><?php echo JText::_("COM_JOBS_TYPE");?></th>
    		<th><?php echo JText::_("COM_JOBS_SECTION");?></th>
    	</tr>
    <?php foreach ($this->fields as $f => $field ){ ?>
    	<tr>
    		<td>
    			<input type="checkbox" name="cid[]" value="<?php echo $field->db_name;?>" /> 
    		</td>
    		<td>
    			<?php echo $field->name;?> <?php echo $field->db_name;?>
    		</td>
    		<td>
    			<?php echo $field->ftype;?>
    		</td>
    		<td>	
    			<?php echo $cfg['pages'][$field->page];?>
    		</td>
    	</tr>
    <?php } ?>	
    <tr>
    	<td colspan="4" align="center">
    	<a href="#" onclick="document.adminForm.submit();">
    		<img src="<?php echo JURI::root();?>/components/com_jobsfactory/images/admin/xlsexport.png" style="vertical-align:middle;" />
            <br />
            <?php echo JText::_("COM_JOBS_EXPORT_NOW");?>
		</a>
    	</td>
    </tr>
    </table>
    
<input type="hidden" name="option" value="com_jobsfactory" />
<input type="hidden" name="task" value="importexport.do_ExportToXls" />
</form>
