<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<div id="cpanel" class='well well-small'>
  <div class='well well-small'>
    <fieldset class="adminform">
      <legend><?php echo JText::_( 'COM_JOBS_IMPORTEXPORT_JOBS_LOC' ); ?></legend>
        
        <?php
          $link = 'index.php?option=com_jobsfactory&amp;task=importexport.showadmimportformloc';
          JTheFactoryAdminHelper::quickiconButton( $link, 'importjobs.png', JText::_("COM_JOBS_IMPORT_LOC_CSV") );
        
          $link = 'index.php?option=com_jobsfactory&amp;task=importexport.exportToXlsLoc';
          JTheFactoryAdminHelper::quickiconButton( $link, 'xlsexport.png', JText::_("COM_JOBS_EXPORT_LOC_XLS"));
          
        ?>
    </fieldset>
  </div>
</div>
<div style="height:100px">&nbsp;</div>
<div style="clear:both;"> </div>
