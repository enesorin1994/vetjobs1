<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php
    JHtml::_('bootstrap.tooltip');
    JHtml::_('behavior.multiselect');
    JHtml::_('dropdown.init');
    JHtml::_('formbehavior.chosen', 'select');
?>

<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
<?php endif; ?>

<div class="span8" id="j-main-container">

<form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">
     <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
     <input type="hidden" name="task" value="studieslevel.savestudieslevel" />
     <input type="hidden" name="id" value="<?php echo $this->row->id;?>" />
    <div class="row-fluid">
    <div class='well-small'>

    	<table class="table-stripped" align="left">
    		<tr valign="top">
    		  <td width="10%"><?php echo JText::_('COM_JOBS_NAME');?>:&nbsp;</td>
    		  <td>
    		   <input type="text" name="levelname" value="<?php echo $this->row->levelname;?>" />
    		  </td>
    	 	</tr>
            <tr valign="top">
              <td width="10%"><?php echo JText::_('COM_JOBS_ENABLED');?>:&nbsp;</td>
              <td><?php echo $this->lists['published']; ?></td>
            </tr>
    	</table>
    </div>
    </div>
    </form>
</div>