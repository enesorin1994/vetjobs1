<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
<?php endif; ?>

<div id="j-main-container" class="span10">
<form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">
    <table class="table table-striped" cellspacing="1">
	<thead>
		<tr>
            <th width="5" class="center">&nbsp;</th>
			<th width="5" class="center">#</th>
			<th width="10%" class="center"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_MESSAGE_DATE'), 'modified', $this->order_Dir, $this->order ,'comments_administrator'); ?></th>
			<th class="title"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_JOB'), 'jobtitle', $this->order_Dir, $this->order ,'comments_administrator'); ?></th>
			<th width="10%" class="center"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_FROM_USER'), 'rauthor', $this->order_Dir, $this->order,'comments_administrator' ); ?></th>
			<th width="10%" class="center"><?php echo JHTML::_('grid.sort',   JText::_('COM_JOBS_TO_USER'), 'user_replied', $this->order_Dir, $this->order,'comments_administrator' ); ?></th>
			<th><?php echo "Message";?></th>
			<th width="3%" class="center"><?php echo JText::_("COM_JOBS_PUBLISHUNPUBLISH"); ?></th>
			<th width="3%" class="center"><?php echo JText::_("COM_JOBS_DELETE");?></th>
		</tr>
	</thead>
	<tbody>
	<?php
	$k = 0;
	for ($i=0, $n=count( $this->messages ); $i < $n; $i++) {
		$row = &$this->messages[$i];
        $img_publish = ($row->published) ? JHtml::_('image','admin/tick.png','','',JText::_('COM_JOBS_PUBLISHED')): JHtml::_('image','admin/disabled.png','','',JText::_('COM_JOBS_DISABLED'));
        $img_delete = JHtml::_('image','admin/publish_x.png','','',JText::_('COM_JOBS_DELETE'));
		?>
		<tr class="<?php echo "row$k"; ?>">
            <td class="center"><?php echo JHTML::_('grid.id', $i, $row->id );?></td>
			<td class="center"><?php echo $i+1; ?></td>
			<td class="center"><?php echo $row->modified;?></td>
			<td class="left">
			    <a href="index.php?option=com_jobsfactory&task=edit&cid[]=<?php echo $row->job_id;?>">
			        <?php echo $row->jobtitle;?></a></td>
			<td class="center"><a href="index.php?option=com_jobsfactory&task=detailUser&cid[]=<?php echo $row->userid1;?>">
			    <?php echo $row->rauthor;?></a></td>
			<td class="center"><a href="index.php?option=com_jobsfactory&task=detailUser&cid[]=<?php echo $row->userid2;?>">
			    <?php echo $row->user_replied;?></a></td>
			<td class="left"><?php echo $row->message; ?></td>
			<td class="center">
                <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','toggle_comment')">
        		<?php echo $img_publish;?></a>
			</td>
            <td class="center">
                 <a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','del_comment')">
                <?php echo $img_delete;?></a>
            </td>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	<tr>
		<td colspan="9">
			<?php echo $this->pagination->getListFooter(); ?>
		</td>
	</tr>
	</tbody>
	</table>
<input type="hidden" name="option" value="com_jobsfactory" />
<input type="hidden" name="task" value="comments_administrator" />
<input type="hidden" name="id" value="" /> 
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $this->order; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->order_Dir; ?>" />
</form>
</div>