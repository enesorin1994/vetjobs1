<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
<?php endif; ?>
<?php
    JHtml::_('bootstrap.tooltip');
    JHtml::_('behavior.multiselect');
    JHtml::_('dropdown.init');
    JHtml::_('formbehavior.chosen', 'select');
?>
<?php
    $doc = JFactory::getDocument();
    $doc->addScriptDeclaration(
        "
            function jobsRefreshLocations(countryselect)
            {
                var selectVal = document.getElementById('country').value;
                var urli = 'index.php?option=com_jobsfactory&controller=city&task=RefreshLocation&format=raw&countryid='+selectVal;

                	var FieldsAjaxCall = new Request({
                		method: 'get',
                		url: urli,
                		onComplete: jobsDisplayLocations

                	}).send();

            }

            function jobsDisplayLocations()
            {
                var locations = this.response.text;

                if (locations != '') {
                    document.getElementById('states').innerHTML = locations;
                } else {
                    document.getElementById('states').innerHTML = 'No state defined for this country';
                }

            }
         "
	);
//&extension=' . $input->getCmd('extension', 'com_jobsfactory')
    ?>

 <form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">
 <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
 <input type="hidden" name="task" value="city.savecity" />
 <input type="hidden" name="id" value="<?php echo $this->row->id;?>" />
	<table class="adminForm" align="left">
		<tr valign="top">
		  <td width="10%"><?php echo JText::_('FACTORY_CITY_NAME');?>:&nbsp;</td>
		  <td>
		   <input type="text" name="cityname" value="<?php echo $this->row->cityname;?>" />
		  </td>
	 	</tr>
       <!-- <tr valign="top">
          <td width="10%"><?php /*echo JText::_('FACTORY_PARENT_COUNTRY');*/?>:&nbsp;</td>
          <td><?php /*echo $this->countries; */?></td>
        </tr>-->
	 	<tr valign="top">
		  <td width="10%"><?php echo JText::_('FACTORY_PARENT_LOCATION');?>:&nbsp;</td>
		  <td><div id="states" name="states">
                <?php echo $this->locations; ?>
              </div>
          </td>
	 	</tr>
	</table>
</form>

