<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
<?php endif; ?>

<?php
    $doc = JFactory::getDocument();
    $doc->addScriptDeclaration(
        "
            function jobsRefreshLocations(countryselect)
            {
                var selectVal = document.getElementById('country').value;
                var urli = 'index.php?option=com_jobsfactory&controller=city&task=RefreshLocation&format=raw&countryid='+selectVal;

                	var FieldsAjaxCall = new Request({
                		method: 'get',
                		url: urli,
                		onComplete: jobsDisplayLocations

                	}).send();
            }

            function jobsDisplayLocations()
            {
                var locations = this.response.text;

                if (locations != '') {
                    document.getElementById('states').innerHTML = locations;
                } else {
                    document.getElementById('states').innerHTML = 'No state defined for this country';
                }
            }
         "
	);
?>

<form name="adminForm" id="adminForm" method="post" action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>">
<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
<input type="hidden" name="task" value="city.doMoveCities" />
<input type="hidden" name="cid" value="<?php echo $this->cid_list;?>" />

<table class="paramlist admintable" width='100%'>
<tr>
  <td class="paramlist_key" style="width:40% !important;" ><?php echo JText::_("FACTORY_MOVE_CITIES");?>:</td>
  <td>
	  <?php foreach ($this->cities as $city){
		  echo $city->cityname."<br />";
	  }?>
  </td>
</tr>
<tr valign="top">
  <td width="10%"><?php echo JText::_('FACTORY_PARENT_COUNTRY');?>:&nbsp;</td>
  <td><?php echo $this->countries; ?></td>
</tr>
<tr>
  <td class="paramlist_key"><?php echo JText::_("FACTORY_TO_CITY");?>:</td>
  <td><div id="states" name="states">
	<?php echo $this->location;?>
      </div>
  </td>
</tr>
<tr>
  <td colspan="2" align="center" class="paramlist_key">
  &nbsp;
  </td>
</tr>
<tr>
  <td colspan="2" align="center">
	<a href="#" onclick="document.adminForm.submit();">
        <?php echo JTheFactoryAdminHelper::imageAdmin('move.png', JText::_("FACTORY_MOVE")); ?>
        <?php echo JTheFactoryAdminHelper::imageAdmin('move_f2.png','');?>
        <?php echo JText::_("FACTORY_MOVE");?></a>
  </td>
</tr>
</table>

</form>
