<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');
    $app = JFactory::getApplication();
    $input = $app->input;
?>

<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
<?php endif; ?>

<?php
    $doc = JFactory::getDocument();
    $doc->addScriptDeclaration(
        "
        	function delecat(nr,enable){
                el=document.getElementById('city_new_'+nr);
                el.disabled=enable;
            }
            function showQuickAdd()
            {
                el=document.getElementById('quickadd');
                el.style.display='block';
                el=document.getElementById('quickaddbutton');
                el.style.display='none';
            }

            function jobsRefreshLocations(countryselect)
            {
                var selectVal = document.getElementById('country').value;
                var urli = 'index.php?option=com_jobsfactory&task=RefreshLocation&format=raw&countryid='+selectVal;

                	var FieldsAjaxCall = new Request({
                		method: 'get',
                		url: urli,
                		onComplete: jobsDisplayLocations

                	}).send();
            }

            function jobsDisplayLocations()
            {
                var locations = this.response.text;

                if (locations != '') {
                    document.getElementById('states').innerHTML = locations;
                } else {
                    document.getElementById('states').innerHTML = 'No state defined for this country';
                }

            }
         "
	);
//&extension=' . $input->getCmd('extension', 'com_jobsfactory')
    ?>
 <form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post" name="adminForm" id="adminForm">
    <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
    <input type="hidden" name="task" value="city.cities" />
    <input type="hidden" name="reset" value="0" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $this->order; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $this->order_Dir; ?>" />

     <div class="span10" id="j-main-container">
         <table>
              <tr>
                  <td width="100%">
                      <div class="pull-left">
                          <?php echo JText::_( 'COM_JOBS_FILTER' ); ?>:
                          <input type="text" name="search" id="search" size="30" value="<?php echo $this->search;?>" class="text_area"  title="<?php echo JText::_( 'COM_JOBS_FILTER_BY_TITLE_OR_ENTER_CITY_ID' );?>" />
                          <?php echo JText::_( 'COM_JOBS_LOCATION_FILTER' ); ?>:
                          <input type="text" name="filter_location" id="filter_location" size="30" value="<?php echo $this->filter_location;?>" class="text_area" title="<?php echo JText::_( 'COM_JOBS_BY_LOCATION' );?>" />

                          <?php if ($this->cfg->admin_approval):?>
                              <?php echo JText::_( 'COM_JOBS_APPROVAL_STATUS' ); ?>:
                              <?php echo JHtml::_('jobapproved.selectlist','filter_approved','',$this->filter_approved); ?>
                          <?php endif;?>
                      </div>
                      <div class="btn-group pull-left">
                          <button class="btn hasTooltip" type="submit" name="filterbutton" title="<?php echo JText::_('COM_JOBS_GO'); ?>"><i class="icon-search"></i></button>
                          <button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="this.form.reset.value='1';this.form.submit();"><i class="icon-remove"></i></button>
                      </div>
                  </td>
                  <td nowrap="nowrap">

                  </td>
              </tr>
         </table>

     <table class="table-striped" width="90%">
     <tr>
        <th width="30">
            <input type="checkbox" name="toggle" value="" class="checklist-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
        </th>
        <th width="40">
            <a href="javascript:submitbutton('city.saveCityOrder')" class="saveorder" title="<?php echo JText::_('FACTORY_SAVE_ORDERING');?>" ></a>
        </th>
         <th width="25%"><?php echo JHTML::_('grid.sort', JText::_('FACTORY_CITIES'), 'c.cityname',$this->order_Dir, $this->order,'city.cities') ; ?></th>
         <th width="*%"><?php echo JHTML::_('grid.sort', JText::_('FACTORY_LOCATION'),'c.location',$this->order_Dir, $this->order,'city.cities') ; ?></th>
         <th width="5%"><?php echo JHTML::_('grid.sort', JText::_('FACTORY_STATUS'),'c.status',$this->order_Dir, $this->order,'city.cities') ; ?></th>
     </tr>
     <?php foreach($this->cities as $row):?>
     <tr>
        <td><?php echo JHTML::_('grid.id',$row->id,$row->id,false);?></td>
        <td><input name='order_<?php echo $row->id;?>' type='text' class='inputbox input-mini' size='1' value='<?php echo $row->ordering;?>' /></td>
        <td><span style="padding-left: 2px;">
            <sup>|<span style="text-decoration:underline;">&nbsp;&nbsp;&nbsp;</span></sup>
            <a href='index.php?option=<?php echo APP_EXTENSION;?>&task=city.editcity&cid=<?php echo $row->id;?>'><?php echo $row->cityname;?></a>
            </span>
        </td>
        <td><?php echo JTheFactoryCityHelper::getLocationName($row->location);?></td>
        <td><?php 
    			if ($row->status ) {
    				$img = 'tick.png';
    				$alt = JText::_( 'FACTORY_PUBLISHED__UNPUBLISH' );
    				$task = "city.unpublish_city";
    			} else {
    				$img = 'publish_x.png';
    				$alt = JText::_( 'FACTORY_UNPUBLISHED__PUBLISH' );
    				$task = "city.publish_city";
    			}
            ?>
               <span class="editlinktip hasTip" title="<?php echo JText::_( 'FACTORY_PUBLISH_INFORMATION')." ".$alt;?>" >
    		 	<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=<?php echo $task;?>&cid[]=<?php echo $row->id;?>" >
                    <?php echo JTheFactoryAdminHelper::imageAdmin($img,$alt);?>
                </span>
            </td>
     </tr>
     <?php endforeach;?>

       <tfoot>
        <tr>
            <td colspan="2"><?php echo $this->pagination->getLimitBox(); ?></td>
            <td colspan="3" style="text-align: center">
                <?php echo $this->pagination->getListFooter(); ?>
            </td>
        </tr>
       </tfoot>
     </table>
 </div>
	<div id="quickaddbutton">
		<button class="button"  onclick="showQuickAdd();" type="button"><?php echo JText::_('FACTORY_QUICKADD');?></button>
		</div>
		<div style="display:none;" id="quickadd"><br />
		<span style="font-size:14px;font-weight:bolder"><?php echo JText::_('FACTORY_TO_QUICK_ADD_CITIES'); ?></span><br /><br />
            <span>
                    <?php  echo JText::_('FACTORY_CITY_NAME');
                        echo JHtml::_('factorylocation.select', 'parentId', "id='parentId'", '');
                     ?>
                     </span><br />
		<textarea name="quickadd" cols="40" rows="10"></textarea><br />
		<button class="button" onclick="submitform('city.quickaddcity');"><?php echo JText::_('FACTORY_QUICKADD_CITIES');?></button>
	</div>
</form>
