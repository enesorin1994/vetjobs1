<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
<?php endif; ?>

<?php
    JHtml::_('behavior.framework', true);
    $doc=JFactory::getDocument();
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/city/js/jxlib.standalone.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/city/js/locale.english.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/city/js/base.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/city/js/browser.record.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/city/js/browser.adaptor.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/city/js/dialog.city.js');
    $doc->addScript(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/city/js/browser.js');
    $doc->addStyleSheet(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/city/js/themes/crispin/jxtheme.uncompressed.css');
    $doc->addStyleSheet(JURI::base().'/components/'.APP_EXTENSION.'/thefactory/city/js/browser.css');
    $doc->addScriptDeclaration("
            window.addEvent('load', function() {
                Jx.setLanguage('en-US');
                var cityBrowser = new CYB.cityBrowser({
                    container: 'tree-container',
                    urls: {
                        insert: 'index.php?option=".APP_EXTENSION."&task=city.newcityjson',
                        'delete': 'index.php?option=".APP_EXTENSION."&task=city.deletecityjson',
                        update: 'index.php?option=".APP_EXTENSION."&task=city.updatecityjson',
                        read: 'index.php?option=".APP_EXTENSION."&task=city.cityjson'
                   }          
                });
            });
    ");
    $doc->addStyleDeclaration("
        .test-container {
            width: 100%;
            height: 500px;
            border: 1px black solid;
        }
    ");

?>
  <div class="test-container" id="tree-container">
  
  </div>
