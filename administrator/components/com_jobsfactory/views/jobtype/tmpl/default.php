<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php if(!empty( $this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
<?php endif; ?>


<form action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="get" name="adminForm" id="adminForm">
  <table class="adminlist" cellspacing="1">
	<thead>
		<tr>
			<th width="5" align="center">#</th>
			<th width="80" align="center">
				<a href="javascript:submitbutton('jobtype.reorder')" class="saveorder" title="<?php echo JText::_('Save ordering');?>" ></a>
			</th>
			<th class="title"><?php echo JText::_("Job type Name");?></th>
            <th class="title" width="3%"><?php echo JText::_("Enabled");?></th>
			<th class="title" width="10%"><?php echo JText::_("Id"); ?></th>
		</tr>
	</thead>
	<tbody>
	<?php
	$k = 0;
	for ($i=0, $n=count( $this->rows ); $i < $n; $i++) {
		$row = &$this->rows[$i];
		$link 	= 'index.php?option='.APP_EXTENSION.'&task=jobtype.editjobtype&id='. $row->id;
		?>
		<tr class="<?php echo "row$k"; ?>">
			<td align="center"><?php echo JHtml::_('grid.id', $i, $row->id); ?></td>
            <td align="center">
                <input type="text"  size="5" name="order_<?php echo $row->id;?>" value="<?php echo $row->ordering;?>" class="text_area input-mini" style="text-align: center" />
            </td>
			<td><a href="<?php echo $link; ?>"><?php echo htmlspecialchars($row->typename, ENT_QUOTES); ?></a></td>
			<td align="center"><?php echo ($row->published)?JText::_("Yes"):JText::_("No"); ?></td>
            <td align="center"><?php echo $row->id; ?></td>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="11">
			<?php echo $this->pagination->getListFooter(); ?>
		</td>
	</tr>
	</tfoot>
	</table>
<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
<input type="hidden" name="task" value="jobtype.listjobtype" />
<input type="hidden" name="boxchecked" value="0" />
</form>
