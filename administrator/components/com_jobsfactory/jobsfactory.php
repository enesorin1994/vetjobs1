<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

if (!JFactory::getUser()->authorise('core.manage', 'com_jobsfactory'))
{
    JFactory::getApplication()->enqueueMessage(JText::_("JERROR_ALERTNOAUTHOR"),'error');
}

//Load Framework
require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'thefactory'.DS.'application'.DS.'application.class.php');
$MyApp=JTheFactoryApplication::getInstance();
$MyApp->Initialize();
$MyApp->assets_url = JUri::root().'administrator/components/'.APP_EXTENSION.'/assets/';

require_once(JPATH_COMPONENT_SITE.DS.'defines.php');
require_once(JPATH_COMPONENT_SITE.DS.'options.php');
require_once (JPATH_COMPONENT_SITE.DS.'helpers'.DS.'jobsfactory.php');
// Include dependencies
jimport('joomla.application.component.controller');
jimport('joomla.form.helper');

if(!JFolder::exists(JOB_PICTURES_PATH)) JFolder::create(JOB_PICTURES_PATH);
if(!JFolder::exists(JOB_UPLOAD_FOLDER)) JFolder::create(JOB_UPLOAD_FOLDER);
if(!JFolder::exists(JOB_TEMPLATE_CACHE)) JFolder::create(JOB_TEMPLATE_CACHE);

$doc= JFactory::getDocument();
$doc->addStyleSheet(JURI::base().'components/com_jobsfactory/css/jobsfactory.css');

$admintask = JFactory::getApplication()->input->getCmd('task','dashboard');

$MyApp->dispatch($admintask);

