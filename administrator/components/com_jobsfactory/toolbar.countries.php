<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

    JHtmlSidebar::addEntry(
        JText::_('COM_JOBS_SETTINGS'),
        'index.php?option=com_jobsfactory&task=settingsmanager',
        'settingsmanager'
    );

    JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_( 'COM_JOBS_COUNTRY_MANAGER' ).' ]</small></small>' , 'categories.png' );
    JToolBarHelper::custom( 'countries.toggle', 'apply', 'apply', JText::_('COM_JOBS_ENABLEDISABLE'));

