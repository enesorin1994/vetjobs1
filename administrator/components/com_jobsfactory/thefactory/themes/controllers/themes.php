<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: themes
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryThemesController
	 */
	class JTheFactoryThemesController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'Themes';
		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'Themes';
		/**
		 * modulename
		 *
		 * @var string
		 */
		var $modulename = 'Themes';

		/**
		 * listThemes
		 *
		 */
		public function listThemes()
		{
			$themefiles = JTheFactoryThemesHelper::getThemeList();
			$themes     = array();

			foreach ($themefiles as $themefile) $themes[] = JTheFactoryThemesHelper::getThemeHeader($themefile);

			// TODO Factory: compatibility with J2.5
			$jVersion = new JVersion();
			$sideBar  = $jVersion->isCompatible('3.0') ? JHtmlSidebar::render() : NULL;

			$view          = $this->getView('themes');
			$view->sidebar = $sideBar;
			$view->themes  = $themes;

			$view->display('list');
		}

		/**
		 * cloneTheme
		 *
		 * @return bool
		 */
		public function cloneTheme()
		{
			$themename = JFactory::getApplication()->input->get('theme', array(0), 'array');

			if (is_array($themename)) $themename = $themename[0];

			$themedir = JTheFactoryThemesHelper::getThemeDir();

			if (!JTheFactoryThemesHelper::isThemeFile("{$themedir}/{$themename}/theme.xml")) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes", JText::_("FACTORY_THEME_IS_NOT_VALID"), 'error');

				return TRUE;
			}

			$theme = JTheFactoryThemesHelper::getThemeHeader("{$themedir}/{$themename}/theme.xml");

			$view            = $this->getView('themes');
			$view->theme     = $theme;
			$view->themename = $themename;

			$view->display('clone');
		}

		/**
		 * doclone
		 *
		 * @return bool
		 */
		public function doclone()
		{
			$input          = JFactory::getApplication()->input;
			$themename      = $input->getString('themename', '');
			$newname        = $input->getString('name', '');
			$newfolder      = $input->getString('themefolder', '');
			$newdescription = $input->getHtml('description', '');
			jimport('joomla.filesystem.folder');
			jimport('joomla.filesystem.file');

			if (is_array($themename)) $themename = $themename[0];

			$themedir = JTheFactoryThemesHelper::getThemeDir();

			if (!JTheFactoryThemesHelper::isThemeFile("{$themedir}/{$themename}/theme.xml")) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes", JText::_("FACTORY_THEME_IS_NOT_VALID"), 'error');

				return TRUE;
			}

			if (JFolder::exists($themedir . "/" . $newfolder)) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes", JText::_("FACTORY_FOLDER_ALREADY_EXISTS") . " " . $newfolder, 'error');

				return TRUE;
			}

			JFolder::copy($themedir . "/" . $themename, $themedir . "/" . $newfolder);
			$manifest_file = $themedir . "/" . $newfolder . "/theme.xml";
			$xml           = simplexml_load_file($manifest_file);

			unset($xml['priority']);
			$xml['folder']     = $newfolder;
			$xml->name         = $newname;
			$xml->description  = $newdescription;
			$xml->creationDate = date('Y-m-d');

			$xmlcontent = $xml->asXML();
			JFile::write($manifest_file, $xmlcontent);

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes", JText::_("FACTORY_THEME_SAVED"));
		}

		/**
		 * cancel
		 *
		 */
		public function cancel()
		{
			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes");
		}

		/**
		 * delete
		 *
		 * @return bool
		 */
		public function delete()
		{
			$themename = JFactory::getApplication()->input->get('theme', array(0), 'array');

			if (is_array($themename)) $themename = $themename[0];

			$themedir = JTheFactoryThemesHelper::getThemeDir();

			if (JTheFactoryThemesHelper::isCurrentTheme($themename)) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes", JText::_("FACTORY_THEME_DELETE_CURRENT_THEME"), 'error');

				return TRUE;
			}

			if (!JTheFactoryThemesHelper::isThemeFile("{$themedir}/{$themename}/theme.xml")) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes", JText::_("FACTORY_THEME_IS_NOT_VALID"), 'error');

				return TRUE;
			}

			if (JTheFactoryThemesHelper::isCoreTheme("{$themedir}/{$themename}/theme.xml")) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes", JText::_("FACTORY_THEME_IS_CORE_AND_CAN_NOT_BE_DELETED"), 'error');

				return TRUE;
			}

			jimport('joomla.filesystem.folder');
			JFolder::delete($themedir . "/" . $themename);

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes", JText::_("FACTORY_THEME_DELETED"));
		}

		/**
		 * upload
		 *
		 */
		public function upload()
		{
			$view = $this->getView('themes');
			$view->display('upload');
		}

		/**
		 * doupload
		 *
		 * @return bool
		 */
		public function doupload()
		{
			// Get the uploaded file information
			$files    = JFactory::getApplication()->input->files->getArray($_FILES);
			$userfile = $files['theme'];

			// Make sure that file uploads are enabled in php
			if (!(bool)ini_get('file_uploads')) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.upload", JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLFILE'), 'error');

				return TRUE;
			}

			// Make sure that zlib is loaded so that the package can be unpacked
			if (!extension_loaded('zlib')) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.upload", JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLZLIB'), 'error');

				return TRUE;
			}

			// If there is no uploaded file, we have a problem...
			if (!is_array($userfile)) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.upload", JText::_('COM_INSTALLER_MSG_INSTALL_NO_FILE_SELECTED'), 'error');

				return TRUE;
			}

			// Check if there was a problem uploading the file.
			if ($userfile['error'] || $userfile['size'] < 1) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.upload", JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLUPLOADERROR'), 'error');

				return TRUE;
			}

			// Build the appropriate paths
			$config   = JFactory::getConfig();
			$tmp_dest = $config->get('tmp_path') . '/' . $userfile['name'];
			$tmp_src  = $userfile['tmp_name'];

			// Move uploaded file
			jimport('joomla.filesystem.file');
			JFile::upload($tmp_src, $tmp_dest);
			jimport('joomla.installer.helper');

			$package = JTheFactoryThemesHelper::unpackTheme($tmp_dest);

			// Was the package unpacked?
			if (!$package) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.upload", JText::_('COM_INSTALLER_UNABLE_TO_FIND_INSTALL_PACKAGE'), 'error');

				return TRUE;
			}

			JTheFactoryThemesHelper::installTheme($package['dir']);

			if (!is_file($package['packagefile'])) {
				$config                 = JFactory::getConfig();
				$package['packagefile'] = $config->get('tmp_path') . '/' . $package['packagefile'];
			}

			JInstallerHelper::cleanupInstall($package['packagefile'], $package['extractdir']);

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes", JText::_("FACTORY_THEME_INSTALLED"));
		}

		/**
		 * setDefault
		 *
		 */
		public function setDefault()
		{
			$themename = JFactory::getApplication()->input->get('theme', array(), 'array');

			if (is_array($themename)) $themename = $themename[0];

			JTheFactoryThemesHelper::setCurrentTheme($themename);

			$smarty = new JTheFactorySmarty();
			$smarty->clear_compiled_tpl();

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=themes.listthemes", JText::_("FACTORY_CURRENT_THEME_SET"));
		}
	}
 
