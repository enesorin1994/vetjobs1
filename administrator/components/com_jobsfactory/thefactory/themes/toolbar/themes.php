<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: themes
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryThemesToolbar
	 */
	class JTheFactoryThemesToolbar
	{
		/**
		 * display
		 *
		 * @static
		 *
		 * @param null $task
		 */
		public static function display($task = NULL)
		{
			$appname = JTheFactoryApplication::getInstance()->getApplicationName();

			JToolBarHelper::title(JText::_('FACTORY_THEME_MANAGEMENT') . ' - ' . $appname);
			JTheFactoryThemesSubmenu::subMenuListPages();

			switch ($task) {
				default:
				case 'listthemes':
					JToolBarHelper::custom('themes.upload', 'upload.png', 'upload_2.png', JText::_('FACTORY_UPLOAD_THEME_PACK'), FALSE);
					JToolBarHelper::save2copy('themes.clonetheme', JText::_('FACTORY_CLONE_THEME'));
					JToolBarHelper::deleteList('', 'themes.delete', JText::_('FACTORY_DELETE_THEME'));
					JToolBarHelper::custom('settingsmanager', 'back', 'back', JText::_('FACTORY_BACK'), FALSE);
					break;
				case 'clonetheme':
					JToolBarHelper::save('themes.doclone');
					JToolBarHelper::cancel('themes.cancel');
					break;
				case 'upload':
					JToolBarHelper::save('themes.doupload');
					JToolBarHelper::cancel('themes.cancel');
					break;
			}

		}
	} // End Class
