<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: themes
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>
<div class = "row-fluid">
	<div class = "span12">
		<form action = "index.php" method = "post" name = "adminForm" id = "adminForm">
			<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
			<input type = "hidden" name = "task" value = "themes.setdefault" />
			<input type = "hidden" name = "boxchecked" value = "0" />


			<?php if (!empty($this->sidebar)): ?>
				<div id = "j-sidebar-container" style = "padding-right: 15px;">
					<span style = "white-space: nowrap;"><?php echo $this->sidebar; ?></span>
				</div>
			<?php endif; ?>

			<div id = "j-main-container">

				<table class = "adminlist table table-condensed">
					<thead>
					<tr>
						<th width = "10" colspan = "2">
							<?php echo JText::_('FACTORY_NR'); ?>
						</th>
						<th class = "title" width = "15%">
							<?php echo JText::_('FACTORY_THEME_NAME'); ?>
						</th>
						<th>
							<?php echo JText::_('FACTORY_DESCRIPTION'); ?>
						</th>
						<th>
							<?php echo JText::_('FACTORY_FOLDER'); ?>
						</th>
						<th>
							<?php echo JText::_('FACTORY_AUTHOR'); ?>
						</th>
						<th>
							<?php echo JText::_('FACTORY_VERSION'); ?>
						</th>
						<th>
							&nbsp;
						</th>
					</tr>
					</thead>
					<?php $i = 1; ?>
					<?php foreach ($this->themes as $theme): ?>
						<tr>
							<td width = "10"><?php echo $i++; ?></td>
							<td width = "10"><?php echo JHtml::_('grid.id', $i, basename(dirname($theme->manifest_file)), FALSE, 'theme'); ?> </td>
							<td><?php echo $theme->name; ?></td>
							<td><?php echo $theme->description; ?></td>
							<td><?php echo basename(dirname($theme->manifest_file)); ?></td>
							<td><?php echo $theme->author; ?></td>
							<td><?php echo $theme->version; ?></td>
							<?php if (JTheFactoryThemesHelper::isCurrentTheme($theme->manifest_file)):; ?>
								<td><?php echo JTheFactoryAdminHelper::imageAdmin('tick.png', "Current Template", "title='" . JText::_('FACTORY_CURRENT_THEME') . "'"); ?></td>
							<?php else: ?>
								<td>
									<a href = "#" onclick = "return listItemTask('cb<?php echo $i; ?>','themes.setdefault');">
										<?php echo JTheFactoryAdminHelper::imageAdmin('disabled.png', "Set Current", "title='" . JText::_('FACTORY_USE_THIS_THEME') . "'"); ?>
									</a>
								</td>
							<?php endif; ?>
						</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</form>
	</div>
</div>
