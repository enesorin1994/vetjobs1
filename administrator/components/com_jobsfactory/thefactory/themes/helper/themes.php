<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: themes
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryThemesHelper
	 */
	class JTheFactoryThemesHelper
	{
		/**
		 * getCurrentTheme
		 *
		 * @static
		 *
		 * @return string
		 */
		public static function getCurrentTheme()
		{
			$cfg = JTheFactoryHelper::getConfig();

			return self::getThemeDir() . "/" . $cfg->theme . "/theme.xml";
		}

		/**
		 * getThemeDir
		 *
		 * @static
		 *
		 * @return string
		 */
		public static function getThemeDir()
		{
			return JPATH_ROOT . '/components/' . APP_EXTENSION . '/templates';
		}

		/**
		 * isCurrentTheme
		 *
		 * @static
		 *
		 * @param $theme
		 *
		 * @return bool
		 */
		public static function isCurrentTheme($theme)
		{
			$cfg = JTheFactoryHelper::getConfig();
			if (basename($theme) == 'theme.xml') $theme = basename(dirname($theme));

			return $theme == $cfg->theme;
		}

		/**
		 * getThemeList
		 *
		 * @static
		 *
		 * @return array
		 */
		public static function getThemeList()
		{
			jimport('joomla.filesystem.folder');
			$themelist = array();
			$themedir  = self::getThemeDir();
			$folders   = JFolder::folders($themedir);
			if (count($folders)) foreach ($folders as $folder) if (file_exists($themedir . "/" . $folder . '/theme.xml') && self::isThemeFile($themedir . "/" . $folder . '/theme.xml')) $themelist[] = $themedir . "/" . $folder . '/theme.xml';

			return $themelist;
		}

		/**
		 * isThemeFile
		 *
		 * @static
		 *
		 * @param $manifest_file
		 *
		 * @return bool
		 */
		public static function isThemeFile($manifest_file)
		{
			$xml = simplexml_load_file($manifest_file);

			if (!$xml) {
				return FALSE;
			}

			if (!isset($xml->attributes()->type)) return FALSE;
			if ((string)$xml->attributes()->type != "theme") return FALSE;

			return TRUE;
		}

		/**
		 * isCoreTheme
		 *
		 * @static
		 *
		 * @param $manifest_file
		 *
		 * @return bool
		 */
		public static function isCoreTheme($manifest_file)
		{
			if (!self::isThemeFile($manifest_file)) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_FILE_IS_NOT_A_VALID_THEME_MANIFEST") . " {$manifest_file}", 'error');

				return FALSE;
			}

			$xml = simplexml_load_file($manifest_file);

			if (!isset($xml->attributes()->priority)) return FALSE;
			if ((string)$xml->attributes()->priority == "core") return TRUE;

			return FALSE;

		}

		/**
		 * getThemePages
		 *
		 * @static
		 *
		 * @param $manifest_file
		 *
		 * @return array|null
		 */
		public static function getThemePages($manifest_file)
		{
			if (!self::isThemeFile($manifest_file)) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_FILE_IS_NOT_A_VALID_THEME_MANIFEST") . " {$manifest_file}", 'error');

				return NULL;
			}

			$xml   = simplexml_load_file($manifest_file);
			$pages = array();

			if (isset($xml->pages->page) && count($xml->pages->page)) {
				foreach ($xml->pages->page as $page) {
					$p              = new stdClass();
					$p->name        = (string)$page->attributes()->name;
					$p->description = (string)$page->attributes()->description;
					$p->thumbnail   = (string)$page->attributes()->thumbnail;
					$pages[]        = $p;
				}
			}

			return $pages;
		}

		/**
		 * getThemeHeader
		 *
		 * @static
		 *
		 * @param $manifest_file
		 *
		 * @return null|stdClass
		 */
		public static function getThemeHeader($manifest_file)
		{
			if (!self::isThemeFile($manifest_file)) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_FILE_IS_NOT_A_VALID_THEME_MANIFEST") . " {$manifest_file}", 'error');

				return NULL;
			}

			$xml = simplexml_load_file($manifest_file);

			$header                = new stdClass();
			$header->name          = (string)$xml->name;
			$header->author        = (string)$xml->author;
			$header->creationdate  = (string)$xml->creationDate;
			$header->copyright     = (string)$xml->copyright;
			$header->license       = (string)$xml->license;
			$header->authoremail   = (string)$xml->authorEmail;
			$header->authorurl     = (string)$xml->authorUrl;
			$header->version       = (string)$xml->version;
			$header->description   = (string)$xml->description;
			$header->manifest_file = $manifest_file;
			$header->folder        = (string)$xml->attributes()->folder;
			$header->priority      = (string)$xml->attributes()->priority;

			return $header;
		}

		/**
		 * getPage
		 *
		 * @static
		 *
		 * @param $manifest_file
		 * @param $pagename
		 *
		 * @return null
		 */
		public static function getPage($manifest_file, $pagename)
		{
			if (!self::isThemeFile($manifest_file)) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_FILE_IS_NOT_A_VALID_THEME_MANIFEST") . " {$manifest_file}", 'error');

				return NULL;
			}

			$xml = simplexml_load_file($manifest_file);

			if (isset($xml->pages->page) && count($xml->pages->page)) {
				foreach ($xml->pages->page as $page) if ((string)$page->attributes()->name == $pagename) return $page;
			}

			return NULL;
		}

		/**
		 * getPagePositions
		 *
		 * @static
		 *
		 * @param $manifest_file
		 * @param $pagename
		 *
		 * @return array|null
		 */
		public static function getPagePositions($manifest_file, $pagename)
		{
			if (!self::isThemeFile($manifest_file)) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_FILE_IS_NOT_A_VALID_THEME_MANIFEST") . " {$manifest_file}", 'error');

				return NULL;
			}

			$xml       = simplexml_load_file($manifest_file);
			$positions = array();

			if (isset($xml->pages->page) && count($xml->pages->page)) {
				foreach ($xml->pages->page as $page) if ((string)$page->attributes()->name == $pagename) if (isset($page->positions->position) && count($page->positions->position)) foreach ($page->positions->position as $position) {
					$p           = new stdClass();
					$p->name     = (string)$position->attributes()->name;
					$p->pagename = (string)$pagename;
					$positions[] = $p;
				}
			}

			return $positions;

		}

		/**
		 * installTheme
		 *
		 * @static
		 *
		 * @param $sourcepath
		 *
		 * @return null
		 */
		public static function installTheme($sourcepath)
		{
			if (!self::isThemeFile($sourcepath . "/theme.xml")) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_FILE_IS_NOT_A_VALID_THEME_MANIFEST") . " {$sourcepath}/theme.xml", 'error');

				return NULL;
			}

			$xml         = simplexml_load_file($sourcepath . "/theme.xml");
			$destfolder  = (string)$xml->attributes()->folder;
			$destination = self::getThemeDir() . "/" . $destfolder;
			jimport("joomla.filesystem.folder");
			JFolder::copy($sourcepath, $destination);
		}

		/**
		 * unpackTheme
		 *
		 * @static
		 *
		 * @param $p_filename
		 *
		 * @return array|bool
		 */
		public static function unpackTheme($p_filename)
		{
			// Path to the archive
			$archivename = $p_filename;

			// Temporary folder to extract the archive into
			$tmpdir = uniqid('install_');

			// Clean the paths to use for archive extraction
			$extractdir  = JPath::clean(dirname($p_filename) . '/' . $tmpdir);
			$archivename = JPath::clean($archivename);
			jimport('joomla.filesystem.archive');
			// Do the unpacking of the archive
			$result = JArchive::extract($archivename, $extractdir);

			if ($result === FALSE) {
				return FALSE;
			}


			/*
			 * Let's set the extraction directory and package file in the result array so we can
			 * cleanup everything properly later on.
			 */
			$retval['extractdir']  = $extractdir;
			$retval['packagefile'] = $archivename;

			/*
			 * Try to find the correct install directory.  In case the package is inside a
			 * subdirectory detect this and set the install directory to the correct path.
			 *
			 * List all the items in the installation directory.  If there is only one, and
			 * it is a folder, then we will set that folder to be the installation folder.
			 */
			$dirList = array_merge(JFolder::files($extractdir, ''), JFolder::folders($extractdir, ''));

			if (count($dirList) == 1) {
				if (JFolder::exists($extractdir . '/' . $dirList[0])) {
					$extractdir = JPath::clean($extractdir . '/' . $dirList[0]);
				}
			}

			/*
			 * We have found the install directory so lets set it and then move on
			 * to detecting the extension type.
			 */
			$retval['dir'] = $extractdir;

			return $retval;
		}

		/**
		 * setCurrentTheme
		 *
		 * @static
		 *
		 * @param $theme
		 */
		public static function setCurrentTheme($theme)
		{
			$MyApp = JTheFactoryApplication::getInstance();

			$cfg        = JTheFactoryHelper::getConfig();
			$cfg->theme = $theme;

			JTheFactoryHelper::modelIncludePath('config');
			$formxml = JPATH_ROOT . "/administrator/components/" . APP_EXTENSION . "/" . $MyApp->getIniValue('configxml');
			$model   = JModelLegacy::getInstance('Config', 'JTheFactoryModel', array('formxml' => $formxml));

			$model->save($cfg);


		}
	}
