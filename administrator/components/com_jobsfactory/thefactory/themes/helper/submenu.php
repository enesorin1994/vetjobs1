<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: themes
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryThemesSubmenu
	 */
	class JTheFactoryThemesSubmenu
	{
		/**
		 * subMenuListPages
		 *
		 * @static
		 *
		 */
		public static function subMenuListPages()
		{
			$task     = JFactory::getApplication()->input->getCmd('task', '');
			$jVersion = new JVersion();

			if ($jVersion->isCompatible('3.0')) {
				JHtmlSidebar::addEntry(JText::_('FACTORY_THEME_LIST'), 'index.php?option=' . APP_EXTENSION . '&task=themes.listthemes', 'themes.listthemes' == $task ? TRUE : FALSE);
			} else {
				JSubMenuHelper::addEntry(JText::_('FACTORY_THEME_LIST'), 'index.php?option=' . APP_EXTENSION . '&task=themes.listthemes', FALSE);
			}
		}

	}
