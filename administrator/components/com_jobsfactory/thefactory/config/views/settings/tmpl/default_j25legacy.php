<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: config
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
	jimport('joomla.html.html.tabs');
	JHTML::_("behavior.tooltip");
?>
<form action = "index.php" method = "post" name = "adminForm" id = "adminForm">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?> ">
	<input type = "hidden" name = "task" value = "config.savesettings">
	<?php
		echo JHtml::_('tabs.start', 'config-tabs-' . APP_EXTENSION . '_configuration', array('useCookie' => 0));
		foreach ($this->groups as $group) {
			echo JHtml::_('tabs.panel', JText::_($group->title), $group->name);
			$this->currentgroup = $group;
			echo $this->loadTemplate('fieldgroup');
		}
		echo JHtml::_('tabs.end');
	?>
</form>
