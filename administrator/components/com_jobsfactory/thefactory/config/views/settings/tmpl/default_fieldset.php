<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: config
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
	JHtml::_('behavior.tooltip');

	$fields = $this->form->getFieldset((string)$this->currentfieldset->attributes()->name);
?>
<table class = "paramlist admintable" width = "100%">
	<?php foreach ($fields as $field): ?>
		<?php if ((string)$this->currentfieldset->attributes()->hidelabel): ?>
			<tr>
				<td colspan = "2"><?php echo $field->input; ?></td>
			</tr>
		<?php else: ?>
			<tr>
				<td class = "paramlist_key" style = "width:45%;"><label><?php echo $field->label; ?></label></td>
				<td><?php echo $field->input; ?></td>
			</tr>
		<?php endif; ?>
	<?php endforeach; ?>
</table>
