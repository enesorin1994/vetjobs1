<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: config
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
	$fieldsets = JTheFactoryConfigHelper::getFieldsets($this->formxml, $this->currentgroup->name);
	//fieldsets are XML Elements , not JFieldsets
	$jVersion = new JVersion();
	$classWidth = $jVersion->isCompatible('3.0') ? 'fltlft' : 'width-100 fltlft';
?>
<table class = "paramlist admintable" width = "100%">
	<tr>
		<td valign = "top" width = "50%">
			<?php
				$i = 1;
				foreach ($fieldsets as $fieldset) {
					echo "<div class=' " . $classWidth . " '><fieldset class='adminform'>
	                <legend>" . $fieldset->attributes()->label . "</legend>";
					$this->currentfieldset = $fieldset;
					echo $this->loadTemplate("fieldset");
					echo "</fieldset></div>";
					if ($i == ceil(count($fieldsets) / 2)) echo "</td><td valign='top'>";
					$i++;
				}
			?>
		</td>
	</tr>
</table>

