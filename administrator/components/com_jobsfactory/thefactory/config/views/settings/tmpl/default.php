<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: config
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
	JHtml::_("behavior.tooltip");
	JHtml::_("behavior.modal");
	JHtml::_('formbehavior.chosen', 'select');
	jimport('joomla.html.html.bootstrap');
?>

<form action = "index.php?option=<?php echo APP_EXTENSION; ?>" method = "post" name = "adminForm" id = "adminForm">
	<input type = "hidden" name = "task" value = "config.savesettings">
	<?php
		echo JHtml::_('bootstrap.startPane', 'myTab', array('active' => 'fieldgroup1'));
	?>
	<ul class = "nav nav-tabs" id = "myTab">
		<?php foreach ($this->groups as $group): ?>
			<li class = "<?php if ($group->name == 'fieldgroup1') echo 'active'; ?>"><a data-toggle = "tab" href = "#<?php echo $group->name ?>"><?php echo $group->title; ?></a></li>
		<?php endforeach; ?>
	</ul>
	<?php
		foreach ($this->groups as $group) {
			echo JHtml::_('bootstrap.addPanel', 'myTab', $group->name);
			$this->currentgroup = $group;
			echo $this->loadTemplate('fieldgroup');
			echo JHtml::_('bootstrap.endPanel');
		}
		echo JHtml::_('bootstrap.endPane');
	?>
</form>
