<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: config
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryConfigController
	 */
	class JTheFactoryConfigController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'Config';
		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'Config';
		/**
		 * formxml
		 *
		 * @var null|string
		 */
		var $formxml = NULL;

		/**
		 *
		 */
		public function __construct()
		{
			$MyApp         = JTheFactoryApplication::getInstance();
			$this->formxml = JPATH_ROOT . "/administrator/components/" . APP_EXTENSION . "/" . $MyApp->getIniValue('configxml');

			parent::__construct();

		}

		/**
		 * display
		 *
		 * @param bool  $cachable
		 * @param array $urlparams
		 */
		public function display($cachable = FALSE, $urlparams = array())
		{
			jimport('joomla.form.form');
			$form = JForm::getInstance('thefactory_component_config', $this->formxml);
			$cfg  = JTheFactoryHelper::getConfig();
			$data = JArrayHelper::fromObject($cfg);
			$form->bind($data);
			$groups = JTheFactoryConfigHelper::getFieldGroups($this->formxml);
			JTheFactoryEventsHelper::triggerEvent('onDisplaySettings', array($form, $groups, $data));
			$jversion = new JVersion();

			$view          = $this->getView('settings');
			$view->groups  = $groups;
			$view->form    = $form;
			$view->formxml = $this->formxml;

			$jversion->isCompatible('3.0') ? $view->display() : $view->display('j25legacy');
		}

		/**
		 * SaveSettings
		 *
		 */
		public function SaveSettings()
		{
			JTheFactoryEventsHelper::triggerEvent('onBeforeSaveSettings');

			$model = JModelLegacy::getInstance('Config', 'JTheFactoryModel', array('formxml' => $this->formxml));

			$data = $model->getDataFromRequest();
			if ($model->save($data)) JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_SETTINGS_SAVED"));
			else JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_ERROR_SAVE_SETTINGS"), 'error');

			JTheFactoryEventsHelper::triggerEvent('onAfterSaveSettings');

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=config.display");
		}
	}
