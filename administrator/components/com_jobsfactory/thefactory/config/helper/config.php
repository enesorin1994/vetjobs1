<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: config
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryConfigHelper
	{
		/**
		 * getFieldGroups
		 *
		 * @static
		 *
		 * @param string $filename
		 *
		 * @return array
		 */
		public static function getFieldGroups($filename)
		{
			$xml    = simplexml_load_file($filename);
			$groups = array();
			if (isset($xml->groups) && count($xml->groups)) {
				foreach ($xml->groups as $fields) {
					$g        = new stdClass();
					$g->name  = (string)$fields->attributes()->name;
					$g->title = (string)$fields->attributes()->label;
					$groups[] = $g;
				}
			}

			return $groups;
		}

		/**
		 * getFieldsets
		 *
		 * @static
		 *
		 * @param string $filename
		 * @param string $group groupname
		 *
		 * @return array
		 */
		public static function getFieldsets($filename, $group)
		{
			$xml      = simplexml_load_file($filename);
			$elements = $xml->xpath('//groups[@name="' . (string)$group . '"]');
			$sets     = array();
			foreach ($elements as $element) {
				if ($tmp = $element->xpath('descendant::fieldset[@name] | descendant::field[@fieldset]/@fieldset')) {
					$sets = array_merge($sets, (array)$tmp);
				}
			}

			return $sets;
		}
	}


