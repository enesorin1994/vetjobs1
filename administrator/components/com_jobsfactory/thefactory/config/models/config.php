<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: config
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.application.component.model');
	jimport('joomla.application.component.helper');

	class JTheFactoryModelConfig extends JModelLegacy
	{
		var $context = 'config';
		var $formxml = NULL;

		public function __construct($config)
		{
			parent::__construct($config);
			$this->context = APP_EXTENSION . "_config.";
			$this->formxml = $config['formxml'];
		}

		public function getDataFromRequest()
		{
			jimport('joomla.form.form');
			$data = JTheFactoryHelper::getRequestArray('post', 'html');
			unset($data['option']);
			unset($data['task']);
			foreach ($data as $k => $d)
				if (strpos($k, '_') === 0)
					unset($data[$k]);

			$form = JForm::getInstance('thefactory_component_config', $this->formxml);

			$fields = $form->getFieldset();
			foreach ($fields as $field)
				if ($field->type == 'Checkbox')
					$data[$field->name] = array_key_exists($field->name, $data) ? intval($data[$field->name]) : 0;

			return $data;
		}

		public function save($data)
		{
			// Get the previous configuration.
			if (is_object($data)) {
				$data = Joomla\Utilities\ArrayHelper::fromObject($data);
			}

			$prev = JTheFactoryHelper::getConfig();
			$prev = Joomla\Utilities\ArrayHelper::fromObject($prev);
			$data = array_merge($prev, $data);

			// Remove form token from data
			foreach($data as $k => $v){
				if ($k == JSession::getFormToken()) {
					unset($data[$k]);
				}
			}

			$configfile = JTheFactoryAdminHelper::getConfigFile();

			$config = new Joomla\Registry\Registry('thefactory_component_config');
			$config->loadArray($data);

			jimport('joomla.filesystem.path');
			jimport('joomla.filesystem.file');
			jimport('joomla.client.helper');

			// Get the new FTP credentials.
			$ftp = JClientHelper::getCredentials('ftp', TRUE);

			// Attempt to make the file writeable if using FTP.
			if (!$ftp['enabled'] && JPath::isOwner($configfile) && !JPath::setPermissions($configfile, '0644')) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_SETTINGS_FILE_IS_NOT_WRITABLE"), 'error');

			}

			// Attempt to write the configuration file as a PHP class named JConfig.
			$configString = $config->toString('PHP', array('class' => ucfirst(APP_PREFIX) . "Config", 'closingtag' => FALSE));
			if (!JFile::write($configfile, $configString)) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_SETTINGS_FILE_WRITE_FAILED"), 'error');

				return FALSE;
			}

			// Attempt to make the file unwriteable if using FTP.
			if (!$ftp['enabled'] && JPath::isOwner($configfile) && !JPath::setPermissions($configfile, '0444')) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_SETTINGS_FILE_IS_NOT_WRITABLE"), 'error');
			}

			return TRUE;
		}
	}
