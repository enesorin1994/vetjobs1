<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: library
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryDatabaseCache
	 */
	class JTheFactoryDatabaseCache extends JCacheController
	{
		/**
		 * db
		 *
		 * @var JDatabaseDriver
		 */
		private $db;
		/**
		 * group
		 *
		 * @var array
		 */
		private $group;

		/**
		 * @param array $group
		 * @param array $options
		 */
		public function __construct($group, $options = array())
		{
			parent::__construct($options);
			$this->db    = JFactory::getDbo();
			$this->group = $group;

		}

		/**
		 * cachedMethods
		 *
		 * @return array
		 */
		private function cachedMethods()
		{
			return array(
				'loadAssoc',
				'loadAssocList',
				'loadColumn',
				'loadObject',
				'loadObjectList',
				'loadResult',
				'loadRow',
				'loadRowList'
			);
		}

		/**
		 * __call
		 *
		 * @param string $name
		 * @param array  $arguments
		 *
		 * @return mixed
		 * @throws BadMethodCallException
		 */
		public function __call($name, $arguments)
		{
			if (!method_exists($this->db, $name)) {
				throw new BadMethodCallException("Method $name not supported in " . get_class($this));
			}
			if (!in_array($name, $this->cachedMethods())) {
				if (!is_array($arguments))
					$Args = !empty($arguments) ? array(&$arguments) : array();
				else
					$Args = & $arguments;

				$callback = array($this->db, $name);

				return call_user_func_array($callback, $Args);
			}

			$id   = $this->makeId($name, $arguments);
			$data = $this->cache->get($id, $this->group);

			$locktest             = new stdClass;
			$locktest->locked     = NULL;
			$locktest->locklooped = NULL;

			if ($data === FALSE) {
				$locktest = $this->cache->lock($id);
				if ($locktest->locked == TRUE && $locktest->locklooped == TRUE)
					$data = $this->cache->get($id, $this->group);
			}

			if ($data !== FALSE) {
				$result = unserialize(trim($data));
				if ($locktest->locked == TRUE)
					$this->cache->unlock($id);
			} else {
				if (!is_array($arguments))
					$Args = !empty($arguments) ? array(&$arguments) : array();
				else
					$Args = & $arguments;

				if ($locktest->locked == FALSE)
					$locktest = $this->cache->lock($id);

				$callback = array($this->db, $name);
				$result   = call_user_func_array($callback, $Args);

				// Store the cache data
				$this->cache->store(serialize($result), $id, $this->group);
				if ($locktest->locked == TRUE)
					$this->cache->unlock($id);
			}

			return $result;

		}

		/**
		 * makeId
		 *
		 * @param $methodname
		 * @param $args
		 *
		 * @return string
		 */
		private function makeId($methodname, $args)
		{
			return md5(serialize(array($this->db->getQuery(),
				$methodname,
				$args
			)));
		}

	}
