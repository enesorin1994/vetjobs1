<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: library
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryDatabaseQuery
	 */
	class JTheFactoryDatabaseQuery extends JObject
	{

		/**
		 * select
		 *
		 * @var array
		 */
		private $select = array();
		/**
		 * from
		 *
		 * @var array
		 */
		private $from = array();
		/**
		 * join
		 *
		 * @var array
		 */
		private $join = array();
		/**
		 * where
		 *
		 * @var array
		 */
		private $where = array();
		/**
		 * group
		 *
		 * @var array
		 */
		private $group = array();
		/**
		 * having
		 *
		 * @var array
		 */
		private $having = array();
		/**
		 * order
		 *
		 * @var array
		 */
		private $order = array();

		/**
		 * select
		 *
		 * @param $field
		 */
		public function select($field)
		{
			$field        = (array)$field;
			$this->select = array_merge($this->select, $field);
		}

		/**
		 * from
		 *
		 * @param        $tableName
		 * @param string $tableAlias
		 */
		public function from($tableName, $tableAlias = '')
		{
			$table               = array();
			$table['tableName']  = $tableName;
			$table['tableAlias'] = $tableAlias;

			$this->from[] = $table;
		}

		/**
		 * join
		 *
		 * @param $joinType
		 * @param $tableName
		 * @param $tableAlias
		 * @param $joinOn
		 */
		public function join($joinType, $tableName, $tableAlias, $joinOn)
		{
			$table               = array();
			$table['tableName']  = $tableName;
			$table['tableAlias'] = $tableAlias;
			$table['joinOn']     = $joinOn;

			$this->join[$joinType][] = $table;
		}

		/**
		 * where
		 *
		 * @param      $conditions
		 * @param null $logicalOperator
		 */
		public function where($conditions, $logicalOperator = NULL)
		{

			$conditions = (array)$conditions;

			if (!is_null($logicalOperator)) {
				$this->where[] = implode(' ' . $logicalOperator . ' ', $conditions);
			} else {
				$this->where = array_merge($this->where, $conditions);
			}
		}

		/**
		 * group
		 *
		 * @param $field
		 */
		public function group($field)
		{
			$field       = (array)$field;
			$this->group = array_merge($this->group, $field);
		}

		/**
		 * order
		 *
		 * @param $field
		 */
		public function order($field)
		{
			$field       = (array)$field;
			$this->order = array_merge($this->order, $field);
		}

		/**
		 * having
		 *
		 * @param $field
		 */
		public function having($field)
		{
			$field        = (array)$field;
			$this->having = array_merge($this->having, $field);
		}

		/**
		 * get
		 *
		 * @param string $queryPart
		 *
		 * @return null
		 */
        public function get($queryPart, $default = NULL)
		{
			$queryParts = array('select', 'from', 'join', 'where', 'group', 'having', 'order');
			if (in_array(strtolower($queryPart), $queryParts)) {
				return $this->$queryPart;
			}

			return NULL;
		}

		/**
		 * set
		 *
		 * @param string $queryPart
		 * @param null   $value
		 *
		 * @return array|null
		 */
		public function set($queryPart, $value = NULL)
		{
			$queryParts = array('select', 'from', 'join', 'where', 'group', 'having', 'order');
			if (in_array(strtolower($queryPart), $queryParts)) {
				if (is_array($value)) $this->$queryPart = $value;
				elseif ($value !== NULL) $this->$queryPart = array($value);
				else
					$this->$queryPart = NULL;

				return $this->$queryPart;
			}

			return NULL;
		}

		/**
		 * getQueriedTables
		 *
		 * @return array
		 */
		public function getQueriedTables()
		{

			$queriedTables = array();

			$from = $this->from;
			foreach ($from as $table) {
				$queriedTables[$table['tableAlias']] = $table['tableName'];
			}
			$joins = $this->join;
			foreach ($joins as $joinType => $tables) {
				foreach ($tables as $table) {
					$queriedTables[$table['tableAlias']] = $table['tableName'];
				}
			}

			return $queriedTables;
		}

		/**
		 * __toString
		 *
		 * @return string
		 */
		public function __toString()
		{

			$sqlQuery = '';

			$br = JDEBUG ? PHP_EOL : '';

			//SELECT
			$sqlQuery .= 'SELECT ' . $br . implode(', ' . $br, $this->select) . $br;


			//FROM
			$sqlQuery .= ' FROM ' . $br;
			foreach ($this->from as $table) {
				$sqlQuery .= $table['tableName'] . ($table['tableAlias'] ? (' AS ' . $table['tableAlias']) : '') . $br;
			}

			//JOIN
			foreach ($this->join as $joinType => $tables) {
				foreach ($tables as $table) {
					$sqlQuery .= ' ' . strtoupper($joinType) . ' JOIN ' . $table['tableName'] . ' AS ' . $table['tableAlias'] . ' ON ' . $table['joinOn'] . $br;
				}
			}

			//WHERE
			if (count($this->where)) {
				$sqlQuery .= ' WHERE ' . $br;
				$sqlQuery .= ' (' . implode(') AND' . $br . ' (', $this->where) . ') ' . $br;
			}

			//GROUP BY
			if (count($this->group)) {
				$sqlQuery .= ' GROUP BY ' . $br;
				$sqlQuery .= implode(', ', $this->group) . $br;
			}

			//HAVING
			if (count($this->having)) {
				$sqlQuery .= ' HAVING ' . $br;
				$sqlQuery .= implode(' AND ', $this->having) . $br;
			}

			//ORDER BY
			if (count($this->order)) {
				$sqlQuery .= ' ORDER BY ' . $br;
				$sqlQuery .= implode(', ', $this->order) . $br;
			}

			return $sqlQuery;
		}
	}
