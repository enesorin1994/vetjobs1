<?php
	/**
	 * ------------------------------------------------------------------------
	 *   thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: library
	 * @version   : 2.5.0
	 *            -------------------------------------------------------------------------
	 */

	defined('_JEXEC') or die('Restricted access');

	/**
	 * JTheFactoryController - ancestor for framework controllers
	 *
	 *  Controller class registers default toolbars for framework module,
	 *      views and helpers. Also if tables or models exists, the classes
	 *      are registered automatically
	 *
	 *  Modulename is usually the folder name where all the code for a subpacket lies (payments, mailman, etc)
	 *
	 */
	class JTheFactoryController extends JControllerLegacy
	{
		/**
		 * name
		 *
		 * @var
		 */
		public $name;
		/**
		 * _name
		 *
		 * @var array|string
		 */
		public $_name;
		/**
		 * modulename
		 *
		 * @var array|string
		 */
		public $modulename;
		/**
		 * basepath
		 *
		 * @var string
		 */
		public $basepath;
		/**
		 * _app
		 *
		 * @var
		 */
		private $_app;

		/**
		 * JTheFactoryController constructor
		 *
		 * @param null $modulename
		 */
		public function __construct($modulename = NULL)
		{
			$this->modulename = $modulename;
			$this->_name      = $this->name;
			if (!$modulename) $this->modulename = $this->name;

			$this->basepath = $this->getApp()->app_path_admin . strtolower($this->modulename);

			parent::__construct();

			self::addViewPath($this->basepath . "/views");
			self::addViewPath(JPATH_ADMINISTRATOR . '/components/' . APP_EXTENSION . "/views");

			JTheFactoryHelper::loadModuleLanguage($this->modulename);
			$this->registerClass('toolbar');
			$this->registerClass('helper');
			$this->registerClass('submenu');
			if (file_exists($this->basepath . "/models")) JTheFactoryHelper::modelIncludePath($this->modulename);
			if (file_exists($this->basepath . "/tables")) JTheFactoryHelper::tableIncludePath($this->modulename);
		}

		/**
		 * JTheFactoryController::getClassName() - returns the default subclass for the current framework module
		 *
		 *      Common $type parameter values are "toolbar", "helper", "submenu"
		 *
		 *
		 * @param string $type
		 *
		 * @return string
		 */
		public function getClassName($type)
		{
			return 'JTheFactory' . ucfirst($this->name) . ucfirst($type);
		}

		/**
		 * JTheFactoryController::registerClass()
		 *      registers a module class
		 *      the class must be in the subfolder "$type"
		 *      from the main module path.
		 *      usual $type values are "toolbar", "helper", "submenu"
		 *
		 * @param string $type
		 *
		 * @return void
		 */
		public function registerClass($type)
		{
			$filename = $this->basepath . '/' . strtolower($type) . '/' . strtolower($this->name) . '.php';
			if (file_exists($filename)) JLoader::register($this->getClassName($type), $filename);
			else
				JLoader::register($this->getClassName($type), $this->basepath . '/helper/' . strtolower($type) . '.php');

		}

		/**
		 * JTheFactoryController::execute() - extends ancestor Execute
		 *      Task can be in the format module.task
		 *
		 * @param mixed $task
		 *
		 * @return
		 */
		public function execute($task)
		{
			if (strpos($task, '.') !== FALSE) //task=controller.task?
			{
				$task = explode('.', $task);
				$task = $task[1];
			}
			//Module has toolbar?
			if (class_exists($this->getClassName('toolbar')) && is_callable(array($this->getClassName('toolbar'), 'display'))) call_user_func(array($this->getClassName('toolbar'), 'display'), $task);

			return parent::execute($task);
		}

		/**
		 * JTheFactoryController::getView() - exstends ancestor getView
		 *
		 *      Adds framework module viewpath to searchpath. Default View-type is HTML
		 *      returns a view.
		 *
		 * @param string $name
		 * @param string $type
		 * @param string $prefix
		 * @param mixed  $config
		 *
		 * @return $view
		 */
		function getView($name = '', $type = 'html', $prefix = '', $config = array())
		{
			if ($name) $config['template_path'] = $this->basepath . "/views/" . strtolower($name) . "/tmpl";
			else
				$config['template_path'] = $this->basepath . "/views/" . strtolower($this->modulename) . "/tmpl";

			return parent::getView($name, $type, ($prefix ? $prefix : ('JTheFactoryView' . ucfirst($this->modulename))), $config);
		}

		/**
		 * JTheFactoryController::getApp() - returns the current FactoryFramework APP object
		 *
		 * @return
		 */
		function getApp()
		{
			return $this->_app ? ($this->_app) : ($this->_app = JTheFactoryApplication::getInstance());
		}
	}
