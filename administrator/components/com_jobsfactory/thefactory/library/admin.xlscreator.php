<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: library
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * JTheFactoryXLSCreator
	 *
	 *      Helper Class to create XLS files
	 *      Requires PEAR Excel Package
	 *
	 */
	class JTheFactoryXLSCreator extends JObject
	{
		/**
		 * JTheFactoryXLSCreator::createXLS() - creates a XLS list
		 *
		 *      Creates a XLS file to standard output based on an array $rows
		 *          with a header row - $row_headers
		 *      Workbook title is optional ($title)
		 *      The header row will be in bold and center aligned
		 *
		 * @param array  $rows
		 * @param array  $row_headers
		 * @param string $title
		 *
		 * @return void
		 */
		public function createXLS(&$rows, $row_headers = NULL, $title = NULL)
		{

			ob_clean();
			ob_start();

			require_once(JPATH_SITE . '/components/' . APP_EXTENSION . '/libraries/pear/PEAR.php');
			require_once(JPATH_SITE . '/components/' . APP_EXTENSION . '/libraries/Excel/Writer.php');

			// Creating a workbook
			$workbook = new Spreadsheet_Excel_Writer();

			$worksheet = $workbook->addWorksheet($title);

			$BIFF = new Spreadsheet_Excel_Writer_BIFFwriter();

			$format = new Spreadsheet_Excel_Writer_Format($BIFF);
			$format->setBold(1);
			$format->setAlign('center');

			for ($k = 0; $k < count($row_headers); $k++) {
				$worksheet->write(0, $k, $row_headers[$k], $format);
			}

			for ($i = count($row_headers) ? 1 : 0; $i < count($rows); $i++) {
				for ($k = 0; $k < count($rows[$i]); $k++) {
					$worksheet->write($i, $k, $rows[$i][$k]);
				}
			}
			$workbook->close();
			$attachment = ob_get_contents();

			@ob_end_clean();
			echo $attachment;

		}

		/**
		 * JTheFactoryXLSCreator::createXLSAdv() - creates a XLS list
		 *
		 *      Creates a XLS file to standard output based on an array $rows
		 *          with a header row - $row_headers
		 *      Rows and header rows are associative arrays with keys in $row_labels
		 *      Column order will be that in $row_labels
		 *      Workbook title is optional ($title)
		 *      The header row will be in bold and center aligned
		 *
		 * @param array  $rows
		 * @param array  $row_labels
		 * @param array  $row_headers
		 * @param string $title
		 *
		 * @return void
		 */

		public static function createXLSAdv(&$rows, $row_labels, $row_headers, $title = NULL)
		{
			ob_clean();
			ob_start();

			require_once(JPATH_SITE . '/components/' . APP_EXTENSION . '/libraries/pear/PEAR.php');
			require_once(JPATH_SITE . '/components/' . APP_EXTENSION . '/libraries/Excel/Writer.php');

			// Creating a workbook
			$workbook = new Spreadsheet_Excel_Writer();

			$worksheet = $workbook->addWorksheet($title);

			$BIFF = new Spreadsheet_Excel_Writer_BIFFwriter();

			$format = new Spreadsheet_Excel_Writer_Format($BIFF);
			$format->setBold(1);
			$format->setAlign('center');

			for ($k = 0; $k < count($row_labels); $k++) {
				$worksheet->write(0, $k, $row_headers[$row_labels[$k]], $format);
			}

			for ($i = 0; $i < count($rows); $i++) {
				for ($k = 0; $k < count($row_labels); $k++) {
					$worksheet->write($i + 1, $k, $rows[$i][$row_labels[$k]]);
				}
			}
			$workbook->close();
			$attachment = ob_get_contents();

			@ob_end_clean();
			echo $attachment;

		}

	}
