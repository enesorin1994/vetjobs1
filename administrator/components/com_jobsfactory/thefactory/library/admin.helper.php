<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: library
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * JTheFactoryAdminHelper
	 *
	 *      Generic Helper Class
	 */
	class JTheFactoryAdminHelper extends JObject
	{

		/**
		 * quickiconButton
		 *
		 * @param        $link
		 * @param        $image
		 * @param        $text
		 * @param string $attribs
		 */
		public static function quickiconButton($link, $image, $text, $attribs = "")
		{
			?>
			<div style = "float:left;">
				<div class = "icon" style = "text-align: center;">
					<a href = "<?php echo $link; ?>">
						<?php echo self::imageAdmin($image, $text, "class ='quick_icon_image_button' " . $attribs); ?>
						<span style = "clear: both;"><?php echo $text; ?></span>
					</a>
				</div>
			</div>
		<?php
		}

		/**
		 * getConfigFile
		 *
		 * @return string
		 */
		public static function getConfigFile()
		{
			$MyApp      = JTheFactoryApplication::getInstance();
			$configfile = $MyApp->getIniValue('option_file');

			return JPATH_SITE . '/components/' . APP_EXTENSION . '/' . $configfile;
		}

		/**
		 * imageAdmin
		 *
		 * @param      $img
		 * @param null $alt
		 * @param null $attribs
		 *
		 * @return mixed
		 */
		public static function imageAdmin($img, $alt = NULL, $attribs = NULL)
		{
			$MyApp = JTheFactoryApplication::getInstance();

			return JHTML::_('image', $MyApp->assets_url . 'images/' . $img, $alt, $attribs);
		}
	}
