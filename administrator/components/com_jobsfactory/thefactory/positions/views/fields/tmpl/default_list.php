<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: positions
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	JHtml::_('formbehavior.chosen', 'select');
?>

<div class = "row-fluid">
	<div class = "span12">
		<form action = "index.php" method = "post" name = "adminForm" id = "adminForm">
			<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
			<input type = "hidden" name = "task" value = "positions.listfields" />
			<input type = "hidden" name = "boxchecked" value = "0" />

			<?php if (!empty($this->sidebar)): ?>
				<div id = "j-sidebar-container" style = "padding-right: 15px;">
					<span style = "white-space: nowrap;"><?php echo $this->sidebar; ?></span>
				</div>
			<?php endif; ?>

			<div id = "j-main-container">
				<div class = "info_header" style = "margin-bottom: 10px;">
					<?php echo JText::_('FACTORY_CURRENT_THEME') . ' <b>' . $this->themeheader->name . '</b>'; ?>
					<div>
						<?php echo $this->themeheader->description; ?>
					</div>
				</div>
				<div style="margin-bottom: 10px;">
					<?php echo JText::_("FACTORY_CURRENT_PAGE") . ' ' . $this->pagehtml; ?>
					<?php echo JText::_("FACTORY_CURRENT_POSITION") . ' ' . $this->positionshtml; ?>
					<input type = "button" class = "btn" value = "<?php echo JText::_('FACTORY_ASSIGN_FIELDS'); ?>"
					       onclick = "Joomla.submitbutton('positions.assignfields')" />
				</div>
				<table class = "table adminlist table-condensed">
					<thead>
					<tr>
						<th width = "10" colspan = "2">
							<?php echo JText::_('FACTORY_'); ?>
						</th>
						<th class = "title" width = "15%" align = "left">
							<?php echo JText::_('FACTORY_FIELD_NAME'); ?>
						</th>
						<th align = "left">
							<?php echo JText::_('FACTORY_FIELD_SECTION'); ?>
						</th>
						<th align = "left">
							<?php echo JText::_('FACTORY_FIELD_TYPE'); ?>
						</th>
						<th align = "left">
							&nbsp;
						</th>
					</tr>
					</thead>
					<?php $i = 1; ?>
					<?php foreach ($this->fields as $field): ?>
						<tr>
							<td width = "10"><?php echo $i++; ?></td>
							<td width = "10"><?php echo JHtml::_('grid.id', $i, $field->id, FALSE, 'id'); ?> </td>
							<td>
								<a href = "<?php echo JURI::base(); ?>index.php?option=<?php echo APP_EXTENSION; ?>&task=fields.edit&id=<?php echo $field->id; ?>">
									<?php echo $field->name; ?></a></td>
							<td><?php echo $field->page; ?></td>
							<td><?php echo $field->ftype; ?></td>
							<td>
								<a href = "index.php?option=<?php echo APP_EXTENSION; ?>&task=positions.delete&field=<?php echo $field->id; ?>&page=<?php echo $this->page; ?>&position=<?php echo $this->position; ?>">
									<?php echo JTheFactoryAdminHelper::imageAdmin('publish_r.png', JText::_("FACTORY_DELETE_ASSIGNMENT"), 'title="' . JText::_("FACTORY_DELETE_ASSIGNMENT") . '"'); ?>
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
			</div>

		</form>
	</div>
</div>
