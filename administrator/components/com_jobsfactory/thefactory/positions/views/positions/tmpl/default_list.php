<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: positions
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	JHtml::_('formbehavior.chosen', 'select');
?>

<form action = "index.php" method = "post" name = "adminForm" id = "adminForm">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "positions.listpositions" />
	<input type = "hidden" name = "boxchecked" value = "0" />

	<div class = "info_header" style = "margin-bottom: 10px;">
		<?php echo JText::_('FACTORY_CURRENT_THEME') . ' <b>' . $this->themeheader->name . '</b>'; ?>
		<div>
			<?php echo $this->themeheader->description; ?>
		</div>
	</div>
	<div style = "margin-bottom: 10px;">
		<?php echo JText::_("FACTORY_CURRENT_PAGE") . ' ' . $this->pagehtml; ?>
		<?php //echo 't_'.$this->page.'.tpl' ?>
	</div>
	<table class = "table adminlist table-condensed">
		<thead>
		<tr>
			<th width = "10" colspan = "2">
				<?php echo JText::_('FACTORY_'); ?>
			</th>
			<th class = "title" width = "15%">
				<?php echo JText::_('FACTORY_POSITION_NAME'); ?>
			</th>
			<th width = "*%">
				<?php echo JText::_('FACTORY_ASSIGNED_FIELDS'); ?>
			</th>
		</tr>
		</thead>
		<?php $i = 1; ?>
		<?php foreach ($this->positions as $position): ?>
			<tr>
				<td width = "10"><?php echo $i++; ?></td>
				<td width = "10"><?php echo JHtml::_('grid.id', $i, $position->name, FALSE, 'positon'); ?> </td>
				<td>
					<a href = "<?php echo JURI::base(); ?>index.php?option=<?php echo APP_EXTENSION; ?>&task=positions.listfields&page=<?php echo $position->pagename; ?>&position=<?php echo $position->name; ?>">
						<?php echo $position->name; ?></a></td>
				<td><?php echo JText::_("FACTORY_ASSIGNED_FIELDS"), count($position->fields), "<br />";
						echo "<ul>";
						for ($j = 0; $j < 10 && $j < count($position->fields); $j++) echo "<li>", $position->fields[$j]->name, "</li>";
						echo "</ul>";
					?></td>

			</tr>
		<?php endforeach; ?>
	</table>
</form>
