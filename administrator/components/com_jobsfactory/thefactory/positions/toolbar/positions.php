<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: positions
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryPositionsToolbar
	 */
	class JTheFactoryPositionsToolbar
	{
		/**
		 * display
		 *
		 * @static
		 *
		 * @param null $task
		 */
		public static function display($task = NULL)
		{
			$appname = JTheFactoryApplication::getInstance()->getApplicationName();
			JToolBarHelper::title(JText::_('FACTORY_THEME_POSITIONS_MANAGEMENT') . ' - ' . $appname);
			JTheFactoryPositionsSubmenu::subMenuListPages();
			switch ($task) {
				default:
				case 'listpages':
					JToolBarHelper::title(JText::_('FACTORY_CUSTOM_FIELDS') . ' - ' . JText::_('FACTORY_PAGES_LIST') . ' - ' . $appname);
					JToolBarHelper::custom('positions.listpositions', 'edit.png', 'edit_f2.png', JText::_('FACTORY_EDIT_PAGE_POSITONS'), TRUE);
					JToolBarHelper::custom("positions.listfields", 'back', 'back', JText::_('FACTORY_BACK'), FALSE);
					break;
				case 'listpositions':
					JToolBarHelper::title(JText::_('FACTORY_CUSTOM_FIELDS') . ' - ' . JText::_('FACTORY_POSITIONS_LIST') . ' - ' . $appname);
					JToolBarHelper::custom('positions.listfields', 'edit.png', 'edit_f2.png', JText::_('FACTORY_EDIT_FIELDS_IN_POSITON'), TRUE);
					JToolBarHelper::custom("positions.listfields", 'back', 'back', JText::_('FACTORY_BACK'), FALSE);
					break;
				case 'listfields':
					JToolBarHelper::title(JText::_('FACTORY_CUSTOM_FIELDS') . ' - ' . JText::_('FACTORY_PUBLISH_ON_TEMPLATE') . ' - ' . $appname);
					JToolBarHelper::custom('positions.listpages', 'archive.png', '', JText::_('FACTORY_PAGES_LIST'), FALSE);
					JToolBarHelper::custom('positions.listpositions', 'archive.png', '', JText::_('FACTORY_POSITIONS_LIST'), FALSE);
					JToolBarHelper::custom('fields.listfields', 'back', 'back', JText::_('FACTORY_BACK'), FALSE);
					break;
				case 'assignfields':
					JToolBarHelper::save("positions.saveassigns", JText::_('FACTORY_SAVE_ASSIGNS'));
					JToolBarHelper::cancel("positions.cancelassigns", JText::_('FACTORY_CANCEL'));
					break;
			}

		}
	}
