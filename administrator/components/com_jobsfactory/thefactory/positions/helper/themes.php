<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: positions
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryThemesHelper
	 */
	class JTheFactoryThemesHelper
	{
		/**
		 * getThemePages
		 *
		 * @static
		 *
		 * @param $manifest_file
		 *
		 * @return array|null
		 */
		public static function getThemePages($manifest_file)
		{
			$xml = simplexml_load_file($manifest_file);
			if (!$xml) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_FILE_IS_NOT_A_VALID_THEME_MANIFEST") . " {$manifest_file}", 'error');

				return NULL;
			}
			$pages = array();
			if (isset($xml->pages->page) && count($xml->pages->page)) {
				foreach ($xml->pages->page as $page) {
					$p              = new stdClass();
					$p->name        = (string)$page->attributes()->name;
					$p->description = (string)$page->attributes()->description;
					$p->thumbnail   = (string)$page->attributes()->thumbnail;
					$pages[]        = $p;
				}
			}

			return $pages;
		}

		/**
		 * getThemeHeader
		 *
		 * @static
		 *
		 * @param $manifest_file
		 *
		 * @return null|stdClass
		 */
		public static function getThemeHeader($manifest_file)
		{
			$xml = simplexml_load_file($manifest_file);
			if (!$xml) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_FILE_IS_NOT_A_VALID_THEME_MANIFEST") . " {$manifest_file}", 'error');

				return NULL;
			}

			$header               = new stdClass();
			$header->name         = (string)$xml->name;
			$header->author       = (string)$xml->author;
			$header->creationdate = (string)$xml->creationDate;
			$header->copyright    = (string)$xml->copyright;
			$header->license      = (string)$xml->license;
			$header->authoremail  = (string)$xml->authorEmail;
			$header->authorurl    = (string)$xml->authorUrl;
			$header->version      = (string)$xml->version;
			$header->description  = (string)$xml->description;

			return $header;
		}

		/**
		 * getPagePositions
		 *
		 * @static
		 *
		 * @param $manifest_file
		 * @param $pagename
		 *
		 * @return array|null
		 */
		public static function getPagePositions($manifest_file, $pagename)
		{
			$xml = simplexml_load_file($manifest_file);
			if (!$xml) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_FILE_IS_NOT_A_VALID_THEME_MANIFEST") . " {$manifest_file}", 'error');

				return NULL;
			}
			$positions = array();

			if (isset($xml->pages->page) && count($xml->pages->page)) {
				foreach ($xml->pages->page as $page) if ((string)$page->attributes()->name == $pagename) if (isset($page->positions->position) && count($page->positions->position)) foreach ($page->positions->position as $position) {
					$p           = new stdClass();
					$p->name     = (string)$position->attributes()->name;
					$p->pagename = (string)$pagename;
					$positions[] = $p;
				}
			}

			return $positions;

		}

	}
