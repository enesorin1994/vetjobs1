<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: positions
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryPositionsHelper
	 */
	class JTheFactoryPositionsHelper
	{
		/**
		 * htmlPageSelect
		 *
		 * @static
		 *
		 * @param      $pages
		 * @param null $selectedpage
		 * @param bool $displayTmplFileName
		 *
		 * @return string
		 */
		public static function htmlPageSelect($pages, $selectedpage = NULL, $displayTmplFileName = FALSE)
		{

			$pageoptions = array();
			for ($i = 0; $i < count($pages); $i++) $pageoptions[] = JHtml::_('select.option', $pages[$i]->description, $pages[$i]->name);
			$fileName = $displayTmplFileName ? 't_' . $selectedpage . '.tpl' : '';

			return JHtml::_('select.genericlist', $pageoptions, 'page', "onchange='this.form.submit();'", 'text', 'value', $selectedpage) . $fileName;

		}

		/**
		 * htmlPositionSelect
		 *
		 * @static
		 *
		 * @param      $positions
		 * @param null $selectedposition
		 *
		 * @return mixed
		 */
		public static function htmlPositionSelect($positions, $selectedposition = NULL)
		{

			$options = array();
			for ($i = 0; $i < count($positions); $i++) $options[] = JHtml::_('select.option', $positions[$i]->name, $positions[$i]->name);

			return JHtml::_('select.genericlist', $options, 'position', "onchange='this.form.submit();'", 'text', 'value', $selectedposition);

		}

		/**
		 * htmlFieldsMultiselect
		 *
		 * @static
		 *
		 * @param array  $fields
		 * @param string $selectname
		 * @param string $attrib
		 * @param string $selected
		 * @param array  $excludelist
		 *
		 * @return mixed
		 */
		public static function htmlFieldsMultiselect($fields, $selectname, $attrib = '', $selected = NULL, $excludelist = NULL)
		{
			$options = array();
			for ($i = 0; $i < count($fields); $i++) {
				if ($excludelist) {
					$exclude = FALSE;
					for ($j = 0; $j < count($excludelist); $j++) if ($excludelist[$j]->name == $fields[$i]->name) {
						$exclude = TRUE;
						break;
					}
					if ($exclude) continue;
				}
				$options[] = JHtml::_('select.option', $fields[$i]->name, $fields[$i]->id);
			}

			return JHtml::_('select.genericlist', $options, $selectname, "multiple size='10' style='width:200px;' " . $attrib, 'text', 'value', $selected);

		}
	}
