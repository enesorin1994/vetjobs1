<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: positions
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.application.component.model');

	/**
	 * Class JTheFactoryModelPositions
	 */
	class JTheFactoryModelPositions extends JModelLegacy
	{
		/**
		 * context
		 *
		 * @var string
		 */
		var $context = 'positions';
		/**
		 * _tablename
		 *
		 * @var null|string
		 */
		var $_tablename = NULL;
		/**
		 * _tablename_fields
		 *
		 * @var null|string
		 */
		var $_tablename_fields = NULL;

		/**
		 *
		 */
		public function __construct()
		{
			$this->context           = APP_EXTENSION . "_positions.";
			$this->_tablename        = '#__' . APP_PREFIX . '_fields_positions';
			$this->_tablename_fields = '#__' . APP_PREFIX . '_fields';

			parent::__construct();
		}

		/**
		 * getFieldsForPage
		 *
		 * @param $page
		 *
		 * @return mixed
		 */
		public function getFieldsForPage($page)
		{
			$db = new JTheFactoryDatabaseCache(APP_EXTENSION . '.positions');

			$db->setQuery("SELECT `f`.* FROM `{$this->_tablename_fields}` AS `f`
					          LEFT JOIN `{$this->_tablename}` AS `p` ON `p`.`fieldid`=`f`.`id`
					          WHERE `p`.`templatepage`='$page'
					          AND `f`.`status`=1
					          ORDER BY `p`.`ordering`
        ");

			return $db->loadObjectList();

		}

		/**
		 * getFieldsForPosition
		 *
		 * @param $pagename
		 * @param $positionname
		 *
		 * @return mixed
		 */
		public function getFieldsForPosition($pagename, $positionname)
		{
			$db = new JTheFactoryDatabaseCache(APP_EXTENSION . '.positions');
			$db->setQuery("SELECT `f`.*
						  FROM `{$this->_tablename_fields}` AS `f`
						  LEFT JOIN `{$this->_tablename}` AS `p` ON `p`.`fieldid`=`f`.`id`
						  WHERE `p`.`templatepage`='{$pagename}'
						  AND `p`.`position`='{$positionname}' AND `f`.`status`=1
						  ORDER BY `f`.`ordering`, `p`.`ordering`
        ");

			return $db->loadObjectList();
		}

		/**
		 * addFieldsToPosition
		 *
		 * @param $pagename
		 * @param $positionname
		 * @param $fields
		 */
		public function addFieldsToPosition($pagename, $positionname, $fields)
		{
			if (!count($fields)) return;
			$db = $this->getDbo();
			$i  = 1;
			foreach ($fields as $field) {
				$db->setQuery("INSERT INTO `{$this->_tablename}`
							 SET `fieldid`='{$field}',
							       `templatepage`='{$pagename}',
							       `position`='{$positionname}',
							       `ordering`='$i'
            ");
				$db->execute();
				$i++;
			}
		}

		/**
		 * deleteFieldsFromPosition
		 *
		 * @param      $pagename
		 * @param      $positionname
		 * @param null $fields
		 */
		public function deleteFieldsFromPosition($pagename, $positionname, $fields = NULL)
		{
			$w = "";
			if ($fields) {
				if (is_array($fields)) {
					$ids = array();
					foreach ($fields as $field) {
						if (is_object($field)) $ids[] = $field->id;
						else
							$ids[] = $field;
					}
					$w = " and fieldid in (" . implode(",", $ids) . ")";
				} else {
					$w = " and fieldid='$fields'";
				}

			}
			$db = $this->getDbo();
			$db->setQuery("DELETE FROM `{$this->_tablename}`
						  WHERE `templatepage`='{$pagename}'
						  AND `position`='{$positionname}'
              {$w}
        ");
			$db->execute();
		}

		/**
		 * getAllFields
		 *
		 * @param null $fieldpage
		 *
		 * @return mixed
		 */
		public function getAllFields($fieldpage = NULL)
		{
			$w = " WHERE `f`.`status`=1 ";
			if ($fieldpage) {
				$w .= " AND `f`.`page`='$fieldpage' ";
			}

			$db = new JTheFactoryDatabaseCache(APP_EXTENSION . '.positions');
			$db->setQuery("SELECT `f`.* FROM `{$this->_tablename_fields}` AS `f`
						{$w}
						ORDER BY `f`.`ordering`
        ");

			return $db->loadObjectList();
		}

	} // End Class
