<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: positions
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryPositionsController
	 */
	class JTheFactoryPositionsController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'Positions';
		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'Positions';
		/**
		 * modulename
		 *
		 * @var string
		 */
		var $modulename = 'Positions';

		/**
		 *
		 */
		public function __construct()
		{
			parent::__construct();
			$MyApp = JTheFactoryApplication::getInstance();
			JLoader::register('JTheFactoryThemesHelper', $MyApp->app_path_admin . 'themes/helper/themes.php');

		}

		/**
		 * listPages
		 *
		 */
		public function listPages()
		{
			$model = JModelLegacy::getInstance('Positions', 'JTheFactoryModel');

			$theme       = JTheFactoryThemesHelper::getCurrentTheme();
			$themeheader = JTheFactoryThemesHelper::getThemeHeader($theme);
			$pages       = JTheFactoryThemesHelper::getThemePages($theme);

			for ($i = 0; $i < count($pages); $i++) $pages[$i]->fields = $model->getFieldsForPage($pages[$i]->name);

			$view              = $this->getView('pages');
			$view->pages       = $pages;
			$view->themeheader = $themeheader;

			$view->display('list');
		}

		/**
		 * listPositions
		 *
		 */
		public function listPositions()
		{
			$page = JFactory::getApplication()->input->get('page', array(), 'array');

			$model       = JModelLegacy::getInstance('Positions', 'JTheFactoryModel');
			$theme       = JTheFactoryThemesHelper::getCurrentTheme();
			$themeheader = JTheFactoryThemesHelper::getThemeHeader($theme);
			$pages       = JTheFactoryThemesHelper::getThemePages($theme);

			if (is_array($page) && count($pages)) $page = $pages[0]->name; // default
			$positions = JTheFactoryThemesHelper::getPagePositions($theme, $page);

			for ($i = 0; $i < count($pages); $i++) $pages[$i]->fields = $model->getFieldsForPage($pages[$i]->name);

			for ($i = 0; $i < count($positions); $i++) $positions[$i]->fields = $model->getFieldsForPosition($positions[$i]->pagename, $positions[$i]->name);

			$pagehtml = JTheFactoryPositionsHelper::htmlPageSelect($pages, $page, TRUE);

			$view              = $this->getView('positions');
			$view->page        = $page;
			$view->pagehtml    = $pagehtml;
			$view->positions   = $positions;
			$view->themeheader = $themeheader;

			$view->display('list');
		}

		/**
		 * listFields
		 *
		 */
		public function listFields()
		{
			$input    = JFactory::getApplication()->input;
			$page     = $input->getString('page', '');
			$position = $input->getString('position', '');
			if (is_array($position)) $position = $position[0];

			$model       = JModelLegacy::getInstance('Positions', 'JTheFactoryModel');
			$theme       = JTheFactoryThemesHelper::getCurrentTheme();
			$themeheader = JTheFactoryThemesHelper::getThemeHeader($theme);
			$pages       = JTheFactoryThemesHelper::getThemePages($theme);

			if (!$page && count($pages)) $page = $pages[0]->name;
			$positions = JTheFactoryThemesHelper::getPagePositions($theme, $page);

			if (!$page && count($pages)) $page = $pages[0]->name; // default
			if (!$position && count($positions)) $position = $positions[0]->name; // default

			for ($i = 0; $i < count($pages); $i++) $pages[$i]->fields = $model->getFieldsForPage($pages[$i]->name);

			for ($i = 0; $i < count($positions); $i++) $positions[$i]->fields = $model->getFieldsForPosition($positions[$i]->pagename, $positions[$i]->name);

			$pagehtml      = JTheFactoryPositionsHelper::htmlPageSelect($pages, $page);
			$positionshtml = JTheFactoryPositionsHelper::htmlPositionSelect($positions, $position);
			$fields        = $model->getFieldsForPosition($page, $position);

			// TODO Factory: compatibility with J2.5
			$jVersion = new JVersion();
			$sideBar  = $jVersion->isCompatible('3.0') ? JHtmlSidebar::render() : NULL;

			$view                = $this->getView('fields');
			$view->page          = $page;
			$view->fields        = $fields;
			$view->sidebar       = $sideBar;
			$view->pagehtml      = $pagehtml;
			$view->position      = $position;
			$view->themeheader   = $themeheader;
			$view->positionshtml = $positionshtml;

			$view->display('list');
		}

		/**
		 * assignFields
		 *
		 */
		public function assignFields()
		{
			$input    = JFactory::getApplication()->input;
			$page     = $input->getString('page', '');
			$position = $input->getString('position', '');
			$model    = JModelLegacy::getInstance('Positions', 'JTheFactoryModel');
			$theme    = JTheFactoryThemesHelper::getCurrentTheme();

			$theme_page = JTheFactoryThemesHelper::getPage($theme, $page);
			$db_page    = (string)$theme_page->attributes()->fieldpage;

			$fields     = $model->getFieldsForPosition($page, $position);
			$fields_all = $model->getAllFields($db_page);


			$htmlfields_all = JTheFactoryPositionsHelper::htmlFieldsMultiselect($fields_all, 'fields_all[]', '', '', $fields);
			$htmlfields     = JTheFactoryPositionsHelper::htmlFieldsMultiselect($fields, 'fields[]');

			JHTML::_('behavior.framework'); //load mootools before fields.js
			JHTML::script("administrator/components/" . APP_EXTENSION . "/thefactory/positions/js/positions.js");

			$view                 = $this->getView('fields');
			$view->page           = $page;
			$view->fields         = $fields;
			$view->pageobj        = $theme_page;
			$view->position       = $position;
			$view->allfields      = $fields_all;
			$view->htmlfields     = $htmlfields;
			$view->htmlfields_all = $htmlfields_all;

			$view->display('assign');
		}

		/**
		 * cancelAssigns
		 *
		 */
		public function cancelAssigns()
		{
			$input    = JFactory::getApplication()->input;
			$page     = $input->getString('page', '');
			$position = $input->getString('position', '');
			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=positions.listfields&page=' . $page . '&position=' . $position);
		}

		/**
		 * saveAssigns
		 *
		 */
		public function saveAssigns()
		{
			/** @var $model JTheFactoryModelPositions
			@var $input JInput
			 */
			$input    = JFactory::getApplication()->input;
			$page     = $input->getString('page', '');
			$fields   = $input->get('fields', array(), 'array');
			$position = $input->getString('position', '');

			$model = JModelLegacy::getInstance('Positions', 'JTheFactoryModel');
			$model->deleteFieldsFromPosition($page, $position);
			$model->addFieldsToPosition($page, $position, $fields);

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=positions.listfields&page=' . $page . '&position=' . $position);
		}

		/**
		 * delete
		 *
		 */
		public function delete()
		{
			/** @var $model JTheFactoryModelPositions
			@var $input JInput
			 */
			$input    = JFactory::getApplication()->input;
			$page     = $input->getString('page', '');
			$fieldid  = $input->get('field', array(), 'array');
			$position = $input->getString('position', '');

			$model = JModelLegacy::getInstance('Positions', 'JTheFactoryModel');
			$model->deleteFieldsFromPosition($page, $position, $fieldid);

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=positions.listfields&page=' . $page . '&position=' . $position);
		}
	} // End Class
