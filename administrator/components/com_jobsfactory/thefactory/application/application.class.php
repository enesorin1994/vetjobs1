<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @package   : thefactory
	 * @subpackage: application
	 *--------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * defined constants
	 *         APP_EXTENSION  - Extension name with com_
	 *         APP_PREFIX     - Table prefix used for extension
	 *         COMPONENT_VERSION  - Extension Version
	 *         SMARTY_DIR         - Smarty folder
	 *
	 * @property string            $appname                    Instance name - should be the Name of the extension (the part after com_)
	 * @property string            $prefix                     A component specific prefix (for tables, session contexts, etc)
	 * @property string            $ini                        The application.ini content
	 * @property string            $version                    Framework version
	 * @property array()           $_modules                   registered modules
	 * @property JControllerLegacy $_controller                loaded frontend or backend controller
	 * @property string            $_stopexecution             Flag set by onBeforeExecuteTask Events that prevents the controller execution
	 * @property boolean           $frontpage                  Flag set to 0 if the application is run on administrator backend
	 * @property string            $app_path_admin             Path to framework admin files
	 * @property string            $app_path_front             Path to framework frontend files
	 */
	if (class_exists('JTheFactoryApplication')) return NULL;

	class JTheFactoryApplication extends JObject
	{

		/* the Application Instance Name */
		public $appname = 'default';

		/* the Application Component prefix */
		public $prefix = NULL;

		/* the Application Instance Configuration */
		private $ini = NULL;

		/* the Application Framework version */
		public $version = '3.0.0';

		/* the Application loaded addons */
		private $_modules = array();
		/* loaded controller - to be executed*/
		private $_controller = NULL;
		/* flag that stops controller execution - used by onBeforeExecuteTask event */
		private $_stopexecution = FALSE;

		/* is frontpage Instance? */
		public $frontpage = 0;

		public $app_path_admin = '';
		public $app_path_front = '';
		/* url to image, css assets - can be changed by extension */
		public $assets_url = '';

		/**
		 * JTheFactoryApplication::getInstance()
		 *
		 * Enhances a Instance of an Application in an static
		 * array of applications
		 *
		 * @param string   $configfile path to Application.ini
		 * @param bool|int $front      true for Frontpage instance - if null, we detect it from Joomla
		 * @param string   $name       instance name
		 *
		 * @return JTheFactoryApplication
		 */
		public static function getInstance($configfile = NULL, $front = NULL, $name = 'default')
		{
			static $instances;
			if ($front === NULL) {
				$front = JFactory::getApplication()->isAdmin() ? 0 : 1;
			}
			if (!isset($instances[$name])) $instances[$name] = new JTheFactoryApplication($configfile, $front);

			return $instances[$name];
		}

		/**
		 * __construct
		 *
		 * JTheFactoryApplication Constructor
		 *
		 * @param string  $configfile path to Application.ini
		 * @param boolean $runfront   true for Frontpage instance
		 *
		 * @throws Exception                throws it if the configuration file is not found
		 */
		public function __construct($configfile = NULL, $runfront = NULL)
		{
			// Trick for replacing JPATH_COMPONENT_ADMINISTRATOR constant
			// which break entire code if is used outside integrator component of this framework
			$compName     = '';
			$pathSegments = explode(DIRECTORY_SEPARATOR, __DIR__);

			foreach ($pathSegments as $k => $dir) {
				if (preg_match('/^com_(\w+)$/', $dir)) {
					$compName = $dir;
				}
			}

			if ($runfront !== NULL) $this->frontpage = $runfront;

			if (!$configfile) $configfile = JPATH_ADMINISTRATOR . '/components/' . $compName . '/application.ini';

			if (!file_exists($configfile)) {
				throw new Exception('Unable to load application configfile: ' . $configfile);
			}

			if (function_exists('parse_ini_file')) {
				$ini = parse_ini_file($configfile, TRUE);
			} else {
				jimport('joomla.registry.registry');
				jimport('joomla.filesystem.file');

				$handler = JRegistryFormat::getInstance('INI');
				$data    = $handler->stringToObject(file_get_contents($configfile), TRUE);

				$ini = JArrayHelper::fromObject($data);
			}

			$this->ini            = $ini;
			$this->appname        = $this->getIniValue('name');
			$this->prefix         = $this->getIniValue('prefix');
			$this->description    = $this->getIniValue('description');
			$this->app_path_admin = JPATH_ROOT . '/administrator/components/com_' . strtolower($this->appname) . '/thefactory/';
			$this->app_path_front = JPATH_ROOT . '/components/com_' . strtolower($this->appname) . '/thefactory/';

			if (is_int($this->getIniValue('error_reporting'))) error_reporting(JDEBUG ? E_ALL : $this->getIniValue('error_reporting'));

			if (!defined('APP_EXTENSION')) define('APP_EXTENSION', 'com_' . $this->appname);
			if (!defined('APP_PREFIX')) define('APP_PREFIX', $this->prefix);
			if (!defined('COMPONENT_VERSION')) define('COMPONENT_VERSION', $this->getIniValue('version'));

			//Default value, you can override it to have different icons for your extension
			$this->assets_url = JUri::root() . 'administrator/components/' . APP_EXTENSION . '/thefactory/assets/';

			/* DEFINES set up in Application ini file */
			$defines = $this->getSection('defines');
			if (count($defines))
				foreach ($defines as $const => $val)
					if (!defined($const)) define($const, $val);

			// LIBRARIES, HELPERS &
			jimport('joomla.application.component.model');
			jimport('joomla.filesystem.folder');

			/* Frontend Helper is always registered */
			JLoader::register('JTheFactoryHelper', $this->app_path_front . 'front.helper.php');
			if ($this->frontpage) {
				JLoader::register('JTheFactoryController', $this->app_path_front . 'front.controller.php');
				$modules = JFolder::folders($this->app_path_front, '', FALSE, TRUE);
			} else {
				JLoader::register('JTheFactoryController', $this->app_path_admin . 'library/admin.controller.php');
				JLoader::register('JTheFactoryView', $this->app_path_admin . 'library/admin.view.php');
				JLoader::register('JTheFactoryAdminHelper', $this->app_path_admin . 'library/admin.helper.php');
				$modules = JFolder::folders($this->app_path_admin, '', FALSE, TRUE);
			}

			/**
			 *  Framework modules can have a special REGISTER Class in the
			 *      default folder (directly in root)
			 * This way you can do specific tasks before starting up the application
			 */
			if (count($modules))
				foreach ($modules as $module)
					if (file_exists($module . '/register.php')) {
						require_once($module . '/register.php');
						$className = 'JTheFactory' . ucfirst(basename($module)) . 'Register';
						call_user_func(array($className, 'registerModule'), $this);
						$this->_modules[] = $module;
					}

			JLoader::register('JTheFactoryDatabase', $this->app_path_admin . 'library/database/admin.database.php');
			JLoader::register('JTheFactoryDatabaseCache', $this->app_path_admin . 'library/database/admin.dbcache.php');
			// Custom Fields
			if ($this->getIniValue('use_extended_profile')) {
				JLoader::register('JTheFactoryUserProfile', $this->app_path_front . 'front.userprofile.php');
			}
			if ($this->getIniValue('use_smarty_templates')) {
				if (!defined('SMARTY_DIR')) define('SMARTY_DIR', JPATH_SITE . '/components/' . APP_EXTENSION . '/libraries/smarty/libs/');
				JLoader::register('Smarty', SMARTY_DIR . 'Smarty.class.php');
				JLoader::register('JTheFactorySmarty', $this->app_path_front . 'front.smarty.php');
				JLoader::register('JTheFactorySmartyView', $this->app_path_front . 'front.smartyview.php');
			}
		}

		/**
		 * getSection
		 *
		 *  reads a full configuration section
		 *
		 * @param string $section section name ([extension], [extended-profile])
		 *
		 * @return array()
		 */
		public function getSection($section)
		{
			return isset($this->ini[$section]) ? $this->ini[$section] : NULL;
		}

		/**
		 * getIniValue
		 *
		 * @param string $key     the INI key (in the section) to get
		 * @param string $section the INI section where to read the key
		 *
		 * @return string
		 */
		public function getIniValue($key, $section = 'extension')
		{
			return isset($this->ini[$section][$key]) ? $this->ini[$section][$key] : NULL;
		}

		/**
		 * JTheFactoryApplication::Initialize() - called in the entry point of the extension
		 *
		 *  loads some "magic" files like defines.php,loads the main helper file (APP_PREFIX.php)
		 *  loads other helpers (through helper::loadHelperclasses)
		 *  loads the needed Controller (first it tries to find a factoryFramework controller)
		 *  adds htmlelements and formelements to path (admin)
		 *  adds tables to paths
		 *  implements Events:  onBeforeExecuteTask
		 *
		 *
		 * @param string $task          task that needs to be run. if null we read it from $_REQUEST
		 *                              tasks for a specific controller can be specified like controller.task
		 *                              or specify the controller name in with &controller=..
		 *
		 * @throws Exception            if controller is not found
		 * @return void
		 */
		public function Initialize($task = NULL)
		{
			JDEBUG ? $GLOBALS['_PROFILER']->mark('theFactory startInitialise') : NULL;
			$input = JFactory::getApplication()->input;
			jimport('joomla.application.component.controller');
			if (!$this->frontpage) {
				JFormHelper::addFieldPath(JPATH_ADMINISTRATOR . '/components/' . APP_EXTENSION . '/formelements');

			}
			JHtml::addIncludePath(JPATH_ADMINISTRATOR . '/components/' . APP_EXTENSION . '/htmlelements');
			JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/' . APP_EXTENSION . '/tables');

			if (file_exists(JPATH_SITE . '/components/' . APP_EXTENSION . '/defines.php')) require_once(JPATH_SITE . '/components/' . APP_EXTENSION . '/defines.php');

			if (file_exists(JPATH_SITE . '/components/' . APP_EXTENSION . '/helpers/' . strtolower($this->appname) . '.php')) require_once(JPATH_SITE . '/components/' . APP_EXTENSION . '/helpers/' . strtolower($this->appname) . '.php');
			$class_name = ucfirst($this->appname) . 'Helper';
			if (is_callable(array($class_name, 'loadHelperClasses')))
				call_user_func(array($class_name, 'loadHelperClasses'));

			JTheFactoryEventsHelper::triggerEvent('onBeforeExecuteTask', array(&$this->_stopexecution));

			if ($this->_stopexecution) return;
			if (!$task) $task = $input->getCmd('task', '');

			$this->_controller = JTheFactoryHelper::loadController($task); //Try to load Framework controllers
			if (!$this->_controller) {
				$controllerClass = $input->getWord('controller', '');
				if (!$controllerClass && strpos($task, '.') !== FALSE) //task=controller.task?
				{
					$task            = explode('.', $task);
					$controllerClass = $task[0];
				}
				if ($controllerClass) {

					$path = JPATH_BASE . '/components/' . APP_EXTENSION . '/controllers/' . basename($controllerClass) . '.php';

					if (!file_exists($path))
						throw new Exception(JText::_('ERROR_CONTROLLER_NOT_FOUND'));
					require_once($path);
					$controllerClass = 'J' . ucfirst($this->appname) . ($this->frontpage ? "" : "Admin") . 'Controller' . ucfirst($controllerClass);

					$this->_controller = new $controllerClass;
				} else {
					$this->_controller = JControllerLegacy::getInstance('J' . ucfirst($this->appname) . ($this->frontpage ? "" : "Admin"));
				}
			}
		}

		/**
		 * JTheFactoryApplication::dispatch()
		 *
		 * executes the current task (using the controller loaded in Initialize)
		 * implements Events:  onAfterExecuteTask
		 *
		 * @param string $task task that needs to be executed. if null then we read it from $_REQUEST
		 *
		 * @return void
		 */
		public function dispatch($task = NULL)
		{
			JDEBUG ? $GLOBALS['_PROFILER']->mark('theFactory startDispatch') : NULL;
			if ($this->_stopexecution) return;
			if (!$this->_controller) return;
			if (!$task) $task = JFactory::getApplication()->input->getCmd('task', '');

			if (strpos($task, '.') !== FALSE) //task=controller.task?
			{
				$task = explode('.', $task);
				$task = $task[1];
			}
			$this->_controller->execute($task);

			JTheFactoryEventsHelper::triggerEvent('onAfterExecuteTask', array($this->_controller));

			$this->_controller->redirect();

			JDEBUG ? $GLOBALS['_PROFILER']->mark('theFactory endDispatch') : NULL;

		}

		/**
		 * setDefaultTask
		 *
		 * @param string $task task to set as default (to the loaded controller)
		 */
		public function setDefaultTask($task)
		{
			$this->_controller->registerDefaultTask($task);
		}

		/**
		 * getApplicationName
		 *  returns the long Application name (human readable)
		 *  it's read from application.ini
		 *
		 *
		 * @param bool $withversion if true returns the Application name and version
		 *
		 * @return string
		 */
		public function getApplicationName($withversion = FALSE)
		{
			$appname = $this->getIniValue('description') . ($withversion ? (' v' . $this->getIniValue('version')) : '');

			return $appname;

		}

		/**
		 * getRegisteredAddons
		 * returns all registerd framework modules
		 *
		 * @return array
		 */
		public function getRegisteredAddons()
		{
			return $this->_modules;
		}

		/**
		 * checkRegisteredAddon
		 * verifies if a specific module is registered with this app
		 *
		 * @param $modulename
		 *
		 * @return bool
		 */
		public function checkRegisteredAddon($modulename)
		{
			return in_array(strtolower($modulename), $this->_modules);

		}

	}
