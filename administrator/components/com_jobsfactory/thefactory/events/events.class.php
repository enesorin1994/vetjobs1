<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: events
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.event.event');

	/**
	 * Class JTheFactoryEvents
	 */
	class JTheFactoryEvents extends JEvent
	{
		/**
		 *
		 */
		public function __construct()
		{
			$dispatcher = JTheFactoryDispatcher::getInstance();
			parent::__construct($dispatcher);
		}
	}
