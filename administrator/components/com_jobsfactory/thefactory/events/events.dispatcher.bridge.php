<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: events
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.event.dispatcher');

	/**
	 * Class JTheFactoryDispatcherBridge
	 *
	 * This class role is to load
	 * the correct Joomla Dispatcher
	 * depending by Joomla version either 2.5 or 3.x
	 */
	class JTheFactoryDispatcherBridge
	{
		/**
		 * dispatcher
		 *
		 * @var
		 */
		protected $dispatcher;

		/**
		 *
		 */
		public function __construct()
		{
			$this->setDispatcher();
		}

		/**
		 * setDispatcher
		 *
		 * @return JEventDispatcher
		 */
		public function setDispatcher()
		{
			$jVersion = new JVersion();
			if ($jVersion->isCompatible('3')) {
				$this->dispatcher = JEventDispatcher::getInstance();
			} else {
				$this->dispatcher = JDispatcher::getInstance();
			}

		}

		/**
		 * __call magic method
		 *
		 * @param string $name
		 * @param array  $arguments
		 *
		 * @throws Exception
		 * @access public
		 * @return mixed
		 */
		public function __call($name, array $arguments)
		{

			if (method_exists($this->dispatcher, $name)) {
				return call_user_func_array(array($this->dispatcher, $name), $arguments);
			}

			throw new Exception('No such method to load: ' . $name);
		}

	}
