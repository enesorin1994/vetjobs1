<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: events
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryEventsHelper
	{
		/**
		 * registerEvents
		 *    registers all Classes in the inputFolder. Classes in the folder must inherit JTheFactoryEvents
		 *    name convention JTheFactoryEvent[name] must be in file [name].php
		 *
		 * @static
		 *
		 * @param string $eventfiles_path folder containing classes
		 */
		public static function registerEvents($eventfiles_path)
		{
			jimport("joomla.filesystem.folder");
			jimport("joomla.filesystem.file");

			$files = JFolder::files($eventfiles_path, '\.php$');
			if (count($files)) foreach ($files as $file) {
				$classname = 'JTheFactoryEvent' . ucfirst(substr($file, 0, strlen($file) - 4));
				require_once "{$eventfiles_path}/{$file}";
				if (class_exists($classname)) new $classname;
			}
		}

		/**
		 * triggerEvent
		 *
		 * @static
		 *
		 * @param string $event eventName (ex: onBeforeExecuteTask)
		 * @param array  $args  parameters array for Event.
		 *
		 * @return array
		 */
		public static function triggerEvent($event, $args = NULL)
		{
			$dispatcher = JTheFactoryDispatcher::getInstance();

			return $dispatcher->trigger($event, $args);
		}
	}
