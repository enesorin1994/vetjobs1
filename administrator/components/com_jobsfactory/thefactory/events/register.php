<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: events
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	/**
	 * Class JTheFactoryEventsRegister
	 */
	class JTheFactoryEventsRegister
	{
		/**
		 * registerModule
		 *
		 * @param null $app
		 */
		public static function registerModule($app = NULL)
		{
			if (!$app) $app = JTheFactoryApplication::getInstance();

			if ($app->getIniValue('use_events')) {
				JLoader::register('JTheFactoryEvents', $app->app_path_admin . 'events/events.class.php');
				JLoader::register('JTheFactoryEventsHelper', $app->app_path_admin . 'events/events.helper.php');
				JLoader::register('JTheFactoryDispatcherBridge', $app->app_path_admin . 'events/events.dispatcher.bridge.php');
				JLoader::register('JTheFactoryDispatcher', $app->app_path_admin . 'events/events.dispatcher.php');
				if ($app->frontpage) JTheFactoryEventsHelper::registerEvents(JPATH_SITE . '/components/com_' . strtolower($app->getIniValue('name')) . '/events');
				else
					JTheFactoryEventsHelper::registerEvents(JPATH_SITE . '/administrator/components/com_' . strtolower($app->getIniValue('name')) . '/events');
				JTheFactoryHelper::loadModuleLanguage('events');
			}


		}

	}
