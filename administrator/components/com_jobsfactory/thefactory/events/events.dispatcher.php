<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: events
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.event.dispatcher');

	/**
	 * Class JTheFactoryDispatcher
	 */
	class JTheFactoryDispatcher extends JTheFactoryDispatcherBridge
	{
		/**
		 * getInstance
		 *
		 * @return JTheFactoryDispatcher
		 */
		public static function getInstance()
		{
			static $instance;

			if (!is_object($instance)) {
				$instance = new JTheFactoryDispatcher;
			}

			return $instance;
		}


	}
