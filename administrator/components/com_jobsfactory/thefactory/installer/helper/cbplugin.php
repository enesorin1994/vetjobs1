<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: installer
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class TheFactoryInstallerCBPluginHelper
	 */
	class TheFactoryInstallerCBPluginHelper
	{
		/**
		 * _sourcepath
		 *
		 * @var null
		 */
		protected $_sourcepath = NULL;

		/**
		 * TheFactoryInstallerCBPluginHelper constructor
		 *
		 * @param $sourcepath
		 */
		public function __construct($sourcepath)
		{
			$this->_sourcepath = $sourcepath;
		}

		/**
		 * InstallCBPlugin
		 *
		 * @param $plugintitle
		 * @param $tabtitle
		 * @param $pluginname
		 * @param $folder
		 * @param $class
		 *
		 * @return string
		 */
		public function InstallCBPlugin($plugintitle, $tabtitle, $pluginname, $folder, $class)
		{
			$db = JFactory::getDbo();

			$query = "SELECT `id` FROM `#__comprofiler_plugin` WHERE `element`='$pluginname.plugin'";
			$db->setQuery($query);
			$plugid = $db->loadResult();

			if (!$plugid) {
				$query = "INSERT INTO `#__comprofiler_plugin`
						    SET `name` = '$plugintitle',
    			`element`='$pluginname.plugin',
    			`type`='user',
    			`folder`='$folder',
    			`ordering`=99,
    			`published`=1,
    			`iscore`=0
    		";
				$db->setQuery($query);
				$db->execute();

				$plugid = $db->insertid();
			}
			$query = "SELECT COUNT(*) FROM `#__comprofiler_tabs` WHERE `pluginid` = '{$plugid}'";
			$db->setQuery($query);
			$tabs = $db->loadResult();
			if (!$tabs) {
				$query = "INSERT INTO `#__comprofiler_tabs`
						    SET `title` = '{$tabtitle}',
        		`ordering`=999,
        		`enabled`=1,
				`pluginclass` = '{$class}',
        		`pluginid`='{$plugid}',
        		`fields`=0,
        		`displaytype`='tab',
        		`position`='cb_tabmain',
        		`viewaccesslevel` = 2
        	";
				$db->setQuery($query);
				$db->execute();
			}
			JFolder::create(JPATH_ROOT . '/components/com_comprofiler/plugin/user/' . $folder);

			$source_file      = $this->_sourcepath . "/$pluginname.plugin.php";
			$destination_file = JPATH_ROOT . "/components/com_comprofiler/plugin/user/$folder/$pluginname.plugin.php";
			JFile::copy($source_file, $destination_file);

			$source_file      = $this->_sourcepath . "/$pluginname.plugin.xml";
			$destination_file = JPATH_ROOT . "/components/com_comprofiler/plugin/user/$folder/$pluginname.plugin.xml";
			JFile::copy($source_file, $destination_file);

			return "<span>Installed Plugin " . $plugintitle . "</span><br/>";
			//return JText::sprintf("FACTORY_CBPLUGIN_INSTALLED",$plugintitle);
		}

	} // End Class
