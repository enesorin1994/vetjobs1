<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: installer
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.filesystem.file');
	jimport('joomla.filesystem.folder');
	jimport('joomla.filesystem.archive');
	jimport('joomla.filesystem.path');

	/**
	 * Class TheFactoryInstaller
	 */
	class TheFactoryInstaller
	{

		/**
		 * cbplugins
		 *
		 * @var null
		 */
		public $cbplugins = NULL;
		/**
		 * plugins
		 *
		 * @var null
		 */
		public $plugins = NULL;
		/**
		 * modules
		 *
		 * @var null
		 */
		public $modules = NULL;
		/**
		 * menus
		 *
		 * @var null
		 */
		public $menus = NULL;
		/**
		 * joomfish
		 *
		 * @var null
		 */
		public $joomfish = NULL;
		/**
		 * message
		 *
		 * @var null
		 */
		public $message = NULL;

		/**
		 * SQL
		 *
		 * @var null
		 */
		public $SQL = NULL;

		/**
		 * errors
		 *
		 * @var null
		 */
		public $errors = NULL;
		/**
		 * warnings
		 *
		 * @var null
		 */
		public $warnings = NULL;

		/**
		 * version
		 *
		 * @var null
		 */
		public $version = NULL;
		/**
		 * versionprevious
		 *
		 * @var null
		 */
		public $versionprevious = NULL;
		/**
		 * extension_message
		 *
		 * @var null
		 */
		public $extension_message = NULL;

		/**
		 * _extension
		 *
		 * @var null
		 */
		protected $_extension = NULL;
		/**
		 * _adapter
		 *
		 * @var null
		 */
		protected $_adapter = NULL;
		/**
		 * _componentid
		 *
		 * @var null
		 */
		protected $_componentid = NULL;
		/**
		 * _sourcepath
		 *
		 * @var null|string
		 */
		protected $_sourcepath = NULL;

		/**
		 * TheFactoryInstaller constructor
		 *
		 * @param $extensionname
		 * @param $adapter
		 */
		public function __construct($extensionname, $adapter)
		{
			$this->_extension  = $extensionname;
			$this->_adapter    = $adapter;
			$this->_sourcepath = $adapter->getParent()->getPath('source') . '/components/' . $extensionname . '/installer';


			// Try extending time
			@set_time_limit(240);

			if (version_compare(phpversion(), '5.0.0', '<')) {
				$this->warnings[] = "PHP Version is pretty old. Consider upgrading it!";
			}

			//try to increase the memory limit step by step
			$memMax = trim(@ini_get('memory_limit'));
			if ($memMax) {
				$last = strtolower($memMax{strlen($memMax) - 1});
				switch ($last) {
					case 'g':
						$memMax *= 1024;
					case 'm':
						$memMax *= 1024;
					case 'k':
						$memMax *= 1024;
				}
				if ($memMax < 16000000) {
					@ini_set('memory_limit', '16M');
				}
				if ($memMax < 32000000) {
					@ini_set('memory_limit', '32M');
				}
				if ($memMax < 48000000) {
					@ini_set('memory_limit', '48M');
				}
			}

			@ignore_user_abort(TRUE);

			$this->version         = $this->getCurrentVersion();
			$this->versionprevious = $this->getPreviousVersion();
		}

		/**
		 * getCurrentVersion
		 *
		 * @return null
		 */
		public function getCurrentVersion()
		{
			$configfile = $this->_adapter->getParent()->getPath('source') . '/administrator/components/' . $this->_extension . '/application.ini';

			if (!file_exists($configfile)) {
				return NULL;
			}

			if (function_exists('parse_ini_file')) {
				$ini = parse_ini_file($configfile, TRUE);
			} else {
				jimport('joomla.registry.registry');
				jimport('joomla.filesystem.file');

				$handler = JRegistryFormat::getInstance('INI');
				$data    = $handler->stringToObject(file_get_contents($configfile));

				$ini = JArrayHelper::fromObject($data);
			}

			return $ini['extension']['version'];
		}

		/**
		 * getPreviousVersion
		 *
		 * @return null
		 */
		public function getPreviousVersion()
		{
			$configfile = JPATH_ADMINISTRATOR . '/components/' . $this->_extension . '/application.ini';

			if (!file_exists($configfile)) {
				return NULL;
			}

			if (function_exists('parse_ini_file')) {
				$ini = parse_ini_file($configfile, TRUE);
			} else {
				jimport('joomla.registry.registry');
				jimport('joomla.filesystem.file');

				$handler = JRegistryFormat::getInstance('INI');
				$data    = $handler->stringToObject(file_get_contents($configfile));

				$ini = JArrayHelper::fromObject($data);
			}

			return $ini['extension']['version'];
		}

		/**
		 * AddMenuItem
		 *
		 * @param      $menuname
		 * @param      $menuitemtitle
		 * @param      $itemalias
		 * @param      $link
		 * @param int  $access
		 * @param null $params
		 */
		public function AddMenuItem($menuname, $menuitemtitle, $itemalias, $link, $access = 0, $params = NULL)
		{
			if (!isset($this->menus[$menuname])) $this->menus[$menuname] = array();

			$this->menus[$menuname][] = array('name' => $menuitemtitle, 'alias' => $itemalias, 'link' => $link, 'access' => $access, 'params' => $params);

		}

		/**
		 * AddSQLStatement
		 *
		 * @param $sql
		 */
		public function AddSQLStatement($sql)
		{
			$this->SQL[] = $sql;
		}

		/**
		 * AddSQLFromFile
		 *
		 * @param $filename
		 */
		public function AddSQLFromFile($filename)
		{
			$filename = $this->_sourcepath . "/$filename";
			if (!file_exists($filename)) return;
			$contents = fread(fopen($filename, 'r'), filesize($filename));

			$db  = JFactory::getDbo();
			$sql = $db->splitSql($contents);

			if (count($this->SQL)) {
				$this->SQL = array_merge($this->SQL, $sql);
			} else {
				$this->SQL = $sql;
			}
		}

		/**
		 * AddModule
		 *
		 * @param        $module_name
		 * @param        $module_title
		 * @param string $module_params
		 */
		public function AddModule($module_name, $module_title, $module_params = '')
		{
			$this->modules[] = array('name' => $module_name, 'title' => $module_title, 'params' => $module_params);

		}

		/**
		 * AddJoomfish
		 *
		 * @param $xmlfile
		 */
		public function AddJoomfish($xmlfile)
		{
			$this->joomfish[] = $xmlfile;
		}

		/**
		 * AddPlugin
		 *
		 * @param $plugin_name
		 * @param $plugin_title
		 * @param $plugin_type
		 * @param $plugin_params
		 */
		public function AddPlugin($plugin_name, $plugin_title, $plugin_type, $plugin_params)
		{
			$this->plugins[] = array('name' => $plugin_name, 'title' => $plugin_title, 'type' => $plugin_type, 'params' => $plugin_params);
		}

		/**
		 * AddCBPlugin
		 *
		 * @param $plugintitle
		 * @param $tabtitle
		 * @param $pluginname
		 * @param $classname
		 */
		public function AddCBPlugin($plugintitle, $tabtitle, $pluginname, $classname)
		{
			$this->cbplugins[] = array('title' => $plugintitle, 'tab' => $tabtitle, 'name' => $pluginname, 'class' => $classname

			);
		}

		/**
		 * AddMessage
		 *
		 * @param $msg
		 */
		public function AddMessage($msg)
		{
			$this->message[] = $msg;
		}

		/**
		 * AddMessageFromFile
		 *
		 * @param $filename
		 */
		public function AddMessageFromFile($filename)
		{
			$filename = $this->_sourcepath . "/$filename";
			if (!file_exists($filename)) return;
			$contents        = fread(fopen($filename, 'r'), filesize($filename));
			$this->message[] = $contents;

		}

		/**
		 * install
		 */
		public function install()
		{

			$db = JFactory::getDbo();
			//Install Menus
			$db->setQuery("SELECT `extension_id` FROM `#__extensions` WHERE `element` ='{$this->_extension}'");
			$this->_componentid = $db->loadResult();
			// Add Menus
			if (count($this->menus)) {
				require_once(dirname(__FILE__) . '/helper/menu.php');
				foreach ($this->menus as $menuname => $menuitems) {
					$menu              = new TheFactoryInstallerMenuHelper();
					$menu->title       = $menuname;
					$menu->componentid = $this->_componentid;
					$j                 = 0;
					foreach ($menuitems as $item) {
						$menu->AddMenuItem($item['name'], $item['alias'], $item['link'], $j++, $item['access'], $item['params']);
					}
					$this->extension_message[] = $menu->storeMenu();
				}
			}

			//Install CB Plugins
			if (count($this->cbplugins)) {
				$db->setQuery("SELECT * FROM `#__extensions` WHERE `element`='com_comprofiler'");
				$comprofiler = $db->loadResult();
				if (count($comprofiler) <= 0) {
					$this->extension_message[] = "<div>";
					$this->extension_message[] = "<h3>Community Builder not detected</h3> <br />";
					$this->extension_message[] = "</div>";
				} else {
					require_once(dirname(__FILE__) . '/helper/cbplugin.php');
					$cbpluginhelper = new TheFactoryInstallerCBPluginHelper($this->_sourcepath . '/cb_plug');
					foreach ($this->cbplugins as $plugin) {
						$this->extension_message[] = $cbpluginhelper->InstallCBPlugin($plugin['title'], $plugin['tab'], $plugin['name'], 'plug_' . $plugin['name'], $plugin['class']);
					}
				}
			}
			//Install SQL
			if (count($this->SQL)) {
				foreach ($this->SQL as $sql) {
					if (trim($sql)) { //empty queries
						$db->setQuery($sql);
						if (!$db->execute()) $this->errors[] = $db->getErrorMsg();
					}

				}

			}

			if ($this->message) {
				$this->extension_message[] = "<table width='100%'>";
				foreach ($this->message as $message) {
					$this->extension_message[] = "<tr>";
					$this->extension_message[] = "<td><div>{$message}</div></td>";
					$this->extension_message[] = "</tr>";
				}
				$this->extension_message[] = "</table>";
			}


		}

		/**
		 * upgrade
		 */
		public function upgrade()
		{
			$db = JFactory::getDbo();

			//Install CB Plugins
			if (count($this->cbplugins)) {
				$db->setQuery("SELECT * FROM `#__extensions` WHERE `element`='com_comprofiler'");
				$comprofiler = $db->loadResult();
				if (count($comprofiler) <= 0) {
					$this->extension_message[] = "<div>";
					$this->extension_message[] = "<h2>Community Builder not detected</h2> <br>";
					$this->extension_message[] = "</div>";
				} else {
					require_once(dirname(__FILE__) . '/helper/cbplugin.php');
					$cbpluginhelper = new TheFactoryInstallerCBPluginHelper($this->_sourcepath . '/cb_plug');
					foreach ($this->cbplugins as $plugin) {
						$this->extension_message[] = $cbpluginhelper->InstallCBPlugin($plugin['title'], $plugin['tab'], $plugin['name'], 'plug_' . $plugin['name'], $plugin['class']);
					}
				}
			}

			$update_class = $this->_sourcepath . '/upgrade/upgrade.php';
			if (JFile::exists($update_class)) {
				require_once($this->_sourcepath . '/upgrade/upgrade.php');
				JTheFactoryUpgrade::upgrade($this->versionprevious);
			}

			$update_sql = $this->_sourcepath . '/upgrade/upgrade_all.sql';
			if (JFile::exists($update_sql)) $this->AddSQLFromFile($update_sql);

			// Get all sql upgrade files
			$upgradeSqlFiles = JFolder::files($this->_sourcepath . '/upgrade/', '.sql');
			// Identify previous version for upgrade sql file in the array files
			$versionPreviousSqlFile  = 'upgrade_' . $this->versionprevious . '.sql';
			$kVersionPreviousSqlFile = (int)array_search($versionPreviousSqlFile, $upgradeSqlFiles);

			// Do sql upgrade only if installed version is not the same with that we want to install
			if (version_compare($this->version, $this->versionprevious, '>')) {
				// Upgrade only from files start with the previous version (installed)
				//  - ignoring older ones
				//  - including all intermediary upgrading sql files
				$upgradeStartWith = array_slice($upgradeSqlFiles, $kVersionPreviousSqlFile, count($upgradeSqlFiles));

				foreach ($upgradeStartWith as $sqlFile) {
					$this->AddSQLFromFile('upgrade/' . $sqlFile);
				}
			}

			//Install SQL
			if (count($this->SQL)) {
				foreach ($this->SQL as $sql) {
					// Don't run empty string as query; it trigger 500 error
					if (!trim($sql)) continue;

					$db->setQuery($sql);
					if (!$db->execute()) $this->errors[] = $db->getErrorMsg();

				}

			}

			if ($this->message) {
				$this->extension_message[] = "<table width='100%'>";
				foreach ($this->message as $message) {
					$this->extension_message[] = "<tr>";
					$this->extension_message[] = "<td><div>{$message}</div></td>";
					$this->extension_message[] = "</tr>";
				}
				$this->extension_message[] = "</table>";
			}

		}

	}
