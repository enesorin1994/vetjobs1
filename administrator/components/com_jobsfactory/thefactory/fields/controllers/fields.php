<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryFieldsController
	 */
	class JTheFactoryFieldsController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		public $name = 'Fields';
		/**
		 * modulename
		 *
		 * @var string
		 */
		public $modulename = 'fields';

		/**
		 * Edit
		 *
		 */
		public function Edit()
		{
			$id = JFactory::getApplication()->input->getInt("id", 0);

			$field = JTable::getInstance('FieldsTable', 'JTheFactory');

			$parameters_plugins = NULL;

			if ($id) {
				if (!$field->load($id)) {
					$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=fields.listfields", JText::_("FACTORY_ERROR_LOADING_FIELD") . " $id", 'error');

					return;
				}
			} else
				$field->setDefaults();

			$lists = JTheFactoryFieldsHelper::createHTMLObjectsForField($field);

			JHTML::_('behavior.framework'); //load mootools before fields.js
			JHTML::script("administrator/components/" . APP_EXTENSION . "/thefactory/fields/js/fields.js");
			JHTML::stylesheet("administrator/components/" . APP_EXTENSION . "/thefactory/fields/css/fields.css");

			if ($field->id && $field->categoryfilter) {
				$model    = JModelLegacy::getInstance('Fields', 'JTheFactoryModel');
				$assigned = $model->getAssignedCats($field);
			} else { //select all by default
				$assigned = "all";
			}

			$lists->category = JHtml::_('factorycategory.select', 'parent[]', 'style="width:200px" multiple size=10' . (($field->categoryfilter) ? '' : ' disabled'), $assigned, TRUE);

			// TODO Factory: For J2.5 compatibility
			$jVersion   = new JVersion();
			$classWidth = $jVersion->isCompatible('3.0') ? 'fltlft' : 'width-100 fltlft';

			$view        = $this->getView('edit');
			$view->lists = $lists;
			$view->field = $field;

			// TODO Factory: For J2.5 compatibility
			$view->classWidth = $classWidth;

			$jVersion->isCompatible('3.0') ? $view->display() : $view->display('joomla25legacy');
		}

		/**
		 * _SaveField
		 *
		 * @return JTheFactoryFieldsTable|null
		 */
		private function _SaveField()
		{
			/**
			 * @var $field      JTheFactoryFieldsTable;
			 * @var $field_type FactoryFieldTypes;
			 * @var $model      JTheFactoryModelFields;
			 * */
			$input = JFactory::getApplication()->input;

			$id         = $input->getInt('id', 0);
			$cat_filter = $input->get('parent', array(), 'array');

			$field = JTable::getInstance('FieldsTable', 'JTheFactory');
			$model = JModelLegacy::getInstance('Fields', 'JTheFactoryModel');
			$cfg   = CustomFieldsFactory::getConfig();

			$field->bind(JTheFactoryHelper::getRequestArray('request', 'html'));
			$paramobj      = new JRegistry($input->get('params', array(), 'array'));
			$field->params = $paramobj->toString('INI');

			if ($field->id) {
				//some fields do not change
				/* @var $db JDatabase */
				$db = JFactory::getDbo();
				$db->setQuery("select * from " . $field->getTableName() . " where " . $field->getKeyName() . "='" . $field->id . "'");
				$f                = $db->loadObject();
				$field->db_name   = $f->db_name;
				$field->page      = $f->page;
				$field->own_table = $f->own_table;
			} else {
				$field->own_table = $cfg['tables'][$field->page];

			}
			$errors = $field->check();

			if (count($errors)) {
				$message = JText::_("FACTORY_ERROR_SAVING_FIELDBR");
				$message .= join("<br />", $errors);

				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=fields.edit&id=$id", $message, 'error');

				return NULL;
			}

			if (!$field->store()) {
				$message = JText::_("FACTORY_ERROR_SAVING_FIELDBR");
				$app     = JFactory::getApplication();

				$app->redirect("index.php?option=" . APP_EXTENSION . "&task=fields.edit&id=$id", $message);

				return NULL;
			}

			$field_type = CustomFieldsFactory::getFieldType($field->ftype);
			if ($field_type->has_options) {
				$field_options = $input->get('field_option', array(), 'array');

				if (count($field_options)) foreach ($field_options as $opt) $field->store_option($opt);
			}

			if ($field->categoryfilter && $cfg['has_category'][$field->page]) $model->setAssignedCats($field, $cat_filter);

			return $field;
		}

		/**
		 * SaveField
		 *
		 */
		function SaveField()
		{
			self::_SaveField();

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=fields.listfields", JText::_("FACTORY_FIELD_SAVED"));
		}

		/**
		 * ApplyField
		 *
		 */
		function ApplyField()
		{
			$field = self::_SaveField();
			if ($field) $this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=fields.edit&id=$field->id", JText::_("FACTORY_FIELD_SAVED"));
			else
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=fields.listfields", JText::_("FACTORY_FIELD_NOT_SAVED"));

		}

		/**
		 * SaveField2New
		 *
		 */
		function SaveField2New()
		{
			self::_SaveField();
			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=fields.edit", JText::_("FACTORY_FIELD_SAVED"));
			//redirect to new
		}

		/**
		 * DeleteField
		 *
		 */
		function DeleteField()
		{
			$cids = JFactory::getApplication()->input->get('cid', array(), 'array');
			if (!is_array($cids)) $cids = array($cids);

			$field_object = JTable::getInstance('FieldsTable', 'JTheFactory');
			$i            = 0;
			foreach ($cids as $cid) if ($field_object->load($cid)) {
				$field_object->delete();
				$i++;
			}

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=fields.listfields", $i . ' ' . JText::_("FACTORY_FIELDS_DELETED"));

		}

		/**
		 * ListFields
		 *
		 */
		function ListFields()
		{
			/**
			 * @var $model JTheFactoryModelFields;
			 * */

			$model       = JModelLegacy::getInstance('Fields', 'JTheFactoryModel');
			$filter_page = JFactory::getApplication()->input->getWord('filter_page', '');

			$cfg = CustomFieldsFactory::getConfig();

			$filters = array();
			$opts[]  = JHtml::_('select.option', '', JText::_("FACTORY_ALL"));
			foreach ($cfg['pages'] as $page => $text) $opts[] = JHtml::_('select.option', $page, $text);
			$filters['page'] = JHtml::_('select.genericlist', $opts, 'filter_page', "onchange=this.form.submit()", 'value', 'text', $filter_page);
			$rows            = $model->getFields(FALSE, $filter_page);

			$pagination = $model->getPagination();

			$view              = $this->getView('list');
			$view->filter_html = $filters;
			$view->rows        = $rows;
			$view->pagination  = $pagination;
			// This check due to compatibility with J2.5
			$jVersion      = new JVersion();
			$sideBar       = $jVersion->isCompatible('3.0') ? JHtmlSidebar::render() : NULL;
			$view->sidebar = $sideBar;

			$view->display();
		}

		/**
		 * Reorder
		 *
		 */
		public function Reorder()
		{
			$field = JTable::getInstance('FieldsTable', 'JTheFactory');

			foreach ($_REQUEST as $k => $v) {
				if (substr($k, 0, 6) == 'order_') {
					$id = substr($k, 6);
					$field->load($id);
					$field->ordering = $v;
					$field->store();
				}
			}

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=fields.listfields', JText::_('FACTORY_ORDERING_SAVED'));

		}

		/**
		 * SaveOptions
		 *
		 */
		public function SaveOptions()
		{
			/**
			 * @var JTheFactoryFieldsTable $field
			 * @var string                 $format
			 * @var int                    $id
			 * */
			$input         = JFactory::getApplication()->input;
			$format        = $input->getWord('format', 'html');
			$id            = $input->getInt('fieldid', 0);
			$field_options = $input->get('field_option', array(), 'array');

			$field = JTable::getInstance('FieldsTable', 'JTheFactory');

			if (!$field->load($id)) {
				$view          = $this->getView('edit', $format);
				$view->message = JText::_("FACTORY_FIELD_DOES_NOT_EXIST_ID") . $id;

				$view->display('error');

				return;
			}
			if (count($field_options)) foreach ($field_options as $opt) $field->store_option($opt);

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=fields.getfieldoptions&format=$format");

			return;
		}

		/**
		 * saveOption
		 *
		 */
		public function saveOption()
		{
			$input       = JFactory::getApplication()->input;
			$format      = $input->getWord('format', 'html');
			$id          = $input->getInt('fieldid', 0);
			$optionid    = $input->getInt('id', 0);
			$optionvalue = $input->getString('optionvalue', '');

			$field = JTable::getInstance('FieldsTable', 'JTheFactory');
			if (!$field->load($id)) {
				$view          = $this->getView('edit', $format);
				$view->message = JText::_("FACTORY_FIELD_DOES_NOT_EXIST_ID") . $id;
				$view->display('error');

				return;
			}

			$field->update_option($optionid, $optionvalue);

			$view        = $this->getView('edit', $format);
			$view->field = $field;

			$view->display('saveoption');
		}

		/**
		 * getFieldOptions
		 *
		 */
		public function getFieldOptions()
		{
			$input  = JFactory::getApplication()->input;
			$format = $input->getWord('format', 'html');
			$id     = $input->getInt('fieldid', 0);

			$field = JTable::getInstance('FieldsTable', 'JTheFactory');
			if (!$field->load($id)) {
				$view          = $this->getView('edit', $format);
				$view->message = JText::_("FACTORY_FIELD_DOES_NOT_EXIST_ID") . $id;
				$view->display('error');

				return;
			}

			$view        = $this->getView('edit', $format);
			$view->field = $field;

			$view->display('options');
		}

		/**
		 * DeleteOption
		 *
		 */
		public function DeleteOption()
		{
			$input   = JFactory::getApplication()->input;
			$format  = $input->getWord('format', 'html');
			$fieldid = $input->getInt('fieldid', 0);
			$id      = $input->getInt('optionid', 0);

			$field = JTable::getInstance('FieldsTable', 'JTheFactory');
			if (!$field->load($fieldid)) {
				$view          = $this->getView('edit', $format);
				$view->message = JText::_("FACTORY_FIELD_DOES_NOT_EXIST_ID") . $id;
				$view->display('error');
			}

			if (!$field->del_option($id)) {
				$view          = $this->getView('edit', $format);
				$view->message = $field->getDbo()->getErrorMsg();

				$view->display('error');

				return;
			}

			$view        = $this->getView('edit', $format);
			$view->field = $field;

			$view->display('deleteoption');
		}

		/**
		 * getFieldtypeParams
		 *
		 */
		public function getFieldtypeParams()
		{
			$input = JFactory::getApplication()->input;

			$format    = $input->getWord('format', 'html');
			$fieldid   = $input->getInt('fieldid', 0);
			$fieldtype = $input->getWord('ftype', '');

			$fieldtypeobj = CustomFieldsFactory::getFieldType($fieldtype);
			$paramsvalues = '';

			$field = NULL;
			if ($fieldid) {
				$field = JTable::getInstance('FieldsTable', 'JTheFactory');
				if (!$field->load($fieldid)) {
					$view          = $this->getView('edit', $format);
					$view->message = JText::_("FACTORY_FIELD_DOES_NOT_EXIST_ID") . $id;
					$view->display('error');

					return;
				}
				$paramsvalues = $field->params;
			}

			$view              = $this->getView('edit', $format);
			$view->field       = $field;
			$view->paramvalues = $paramsvalues;
			$view->field_type  = $fieldtypeobj;

			$view->display('update_params');
		}

	}
