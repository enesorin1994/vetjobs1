<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FactoryFieldValidator
	 */
	abstract class FactoryFieldValidator
	{
		/**
		 * name
		 *
		 * @var string
		 */
		public $name = '';
		/**
		 * classname
		 *
		 * @var string
		 */
		public $classname = '';
		/**
		 * errormessages
		 *
		 * @var array
		 */
		public $errormessages = array();

		/**
		 * validateValue
		 *
		 * @param      $value
		 * @param null $params
		 *
		 * @return bool
		 */
		public function validateValue($value, $params = NULL)
		{
			return TRUE;
		}

		/**
		 * validateJS
		 *
		 * @return string
		 */
		public function validateJS()
		{
			return '';
		}

		/**
		 * getParams
		 *
		 * @return null
		 */
		public function getParams()
		{
			return NULL;
		}

	}

