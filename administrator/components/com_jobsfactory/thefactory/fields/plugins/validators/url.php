<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FieldValidator_url
	 */
	class FieldValidator_url extends FactoryFieldValidator
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'URL Validator';
		/**
		 * classname
		 *
		 * @var string
		 */
		var $classname = 'url';
		/**
		 * errormessages
		 *
		 * @var array
		 */
		var $errormessages = array();

		/**
		 * validateValue
		 *
		 * @param      $value
		 * @param null $params
		 *
		 * @return bool
		 */
		public function validateValue($value, $params = NULL)
		{
			$this->errormessages = array();
			$result              = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $value);
			if (!$result) {
				$this->errormessages[] = JText::_("FACTORY_URL_FIELD_IS_NOT_VALID");

				return FALSE;
			}

			return TRUE;
		}

		/**
		 * validateJS
		 *
		 * @return string
		 */
		public function validateJS()
		{

			return "
            Window.onDomReady(function() {
                    document.formvalidator.setHandler('url', function(value) {
            			regex=/^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;
            			return regex.test(value);
                    })
            })
        ";
		}

	}

