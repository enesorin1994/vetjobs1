<?php
/**------------------------------------------------------------------------
 * thefactory - The Factory Class Library - v 3.0.0
 * ------------------------------------------------------------------------
 *
 * @author    thePHPfactory
 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *            Websites: http://www.thePHPfactory.com
 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build     : 01/04/2012
 * @package   : thefactory
 * @subpackage: custom_fields
 *            -------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

/**
 * Class Form Validate Rule for Images
 */
     
 class JFormRuleImage extends JFormRule{
	protected $type = 'Image';
    protected $imageext=array('jpg','jpeg','png','gif');
    
    public function test(SimpleXMLElement $element, $value, $group = null, Joomla\Registry\Registry $input = null, JForm $form = null)
    {
		$required = ((string) $element['compulsory'] == 'true' || (string) $element['compulsory'] == 'required');
        $field_name=(string)$element['name'];
        
        $res=true;
        
        if (!$required) {
            return $res;
        }        
        $app=JFactory::getApplication();
        $files=$app->input->files->get('jform');
        if (isset($files[$field_name]['name'])){
            $ext=strtolower(JFile::getExt($files[$field_name]['name']));
            $res=$res && in_array($ext,$this->imageext);
        }
        $res=$res && (!empty($files[$field_name]['name']));
        
        return $res;
    }     
 }
