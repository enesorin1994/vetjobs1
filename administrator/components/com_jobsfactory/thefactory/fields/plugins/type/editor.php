<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FieldType_editor
	 */
	class FieldType_editor extends FactoryFieldTypes
	{
		/**
		 * type_name
		 *
		 * @var string
		 */
		var $type_name = "Editor";
		/**
		 * class_name
		 *
		 * @var string
		 */
		var $class_name = "editor";
		/**
		 * has_options
		 *
		 * @var int
		 */
		var $has_options = 0;
		/**
		 * multiple
		 *
		 * @var int
		 */
		var $multiple = 0;
		/**
		 * sql_type
		 *
		 * @var string
		 */
		var $sql_type = "text";
		/**
		 * length
		 *
		 * @var null
		 */
		var $length = NULL;
		/**
		 * store_as_id
		 *
		 * @var bool
		 */
		var $store_as_id = FALSE;
		/**
		 * _params
		 *
		 * @var array
		 */
		var $_params = array("width" => "text", "height" => "text", "cols" => "numeric", "rows" => "numeric");

		/**
		 * getFieldHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return string
		 */
		public function getFieldHTML($field, $fieldvalue = NULL)
		{
			jimport('joomla.html.editor'); // Need only for J2.5
			/* @var $editor JEditor */
			$editor = JEditor::getInstance(JFactory::getConfig()->get('editor'));
			$width  = $field->getParam('width', '100%');
			$height = $field->getParam('height', '60');
			$cols   = $field->getParam('cols', '80');
			$rows   = $field->getParam('rows', '60');

			return $editor->display($field->db_name, $fieldvalue, $width, $height, $cols, $rows);
		}

		/**
		 * getSearchHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return string
		 */
		public function getSearchHTML($field, $fieldvalue = NULL)
		{
			$fieldid    = $field->getHTMLId();
			$css_class  = $this->getCSSClass($field);
			$input_name = $field->page . '%' . $field->db_name;

			return "<input type='text' name='{$input_name}' id='{$fieldid}' class='{$css_class}' value='" . trim($fieldvalue) . "'>";
		}
	}
