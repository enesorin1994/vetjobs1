<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FieldType_radioBox
	 */
	class FieldType_radioBox extends FactoryFieldTypes
	{
		/**
		 * type_name
		 *
		 * @var string
		 */
		var $type_name = "Radio Button Options";
		/**
		 * class_name
		 *
		 * @var string
		 */
		var $class_name = "radioBox";
		/**
		 * has_options
		 *
		 * @var int
		 */
		var $has_options = 1;
		/**
		 * multiple
		 *
		 * @var int
		 */
		var $multiple = 0;
		/**
		 * sql_type
		 *
		 * @var string
		 */
		var $sql_type = "varchar";
		/**
		 * length
		 *
		 * @var int
		 */
		var $length = 250;
		/**
		 * store_as_id
		 *
		 * @var bool
		 */
		var $store_as_id = FALSE;

		/**
		 * getFieldHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return mixed
		 */
		public function getFieldHTML($field, $fieldvalue = NULL)
		{
			$fieldid          = $field->getHTMLId();
			$css_class        = $this->getCSSClass($field);
			$style_attributes = ($field->style_attr) ? "style='{$field->style_attr}'" : "";

			$options = $field->getOptions();

			$opt_array = array();
			if (count($options)) foreach ($options as $k => $option) $opt_array[] = JHTML::_('select.option', $option->option_name, JText::_($option->option_name));

			return JHTML::_('select.radiolist', $opt_array, $field->db_name, "class='$css_class' $style_attributes id='$fieldid'", 'value', 'text', $fieldvalue);
		}

		/**
		 * getSearchHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return mixed
		 */
		public function getSearchHTML($field, $fieldvalue = NULL)
		{
			$fieldid          = $field->getHTMLId();
			$css_class        = $this->getCSSClass($field);
			$style_attributes = ($field->style_attr) ? "style='{$field->style_attr}'" : "";

			$options = $field->getOptions();

			$opt_array   = array();
			$opt_array[] = JHTML::_('select.option', '', JText::_('FACTORY_ALL'));
			if (count($options)) foreach ($options as $k => $option) $opt_array[] = JHTML::_('select.option', $option->option_name, JText::_($option->option_name));

			return JHTML::_('select.radiolist', $opt_array, $field->page . '%' . $field->db_name, "class='$css_class' $style_attributes id='$fieldid'", 'value', 'text', $fieldvalue);
		}

		/**
		 * getSQLFilter
		 *
		 * @param      $field
		 * @param      $filter
		 * @param null $tableAlias
		 *
		 * @return string
		 */
		public function getSQLFilter($field, $filter, $tableAlias = NULL)
		{
			$cfg = CustomFieldsFactory::getConfig();
			$db  = JFactory::getDbo();

			if ($tableAlias) $table_alias = $tableAlias . ".";
			else
				$table_alias = isset($cfg['aliases'][$field->own_table]) ? ($cfg['aliases'][$field->own_table] . ".") : "";


			$sql = " " . $table_alias . $field->db_name . " LIKE " . $db->quote('%' . $filter . '%');

			return $sql;
		}

		/**
		 * getTemplateHTML
		 *
		 * @param $field
		 * @param $fieldvalue
		 *
		 * @return string
		 */
		public function getTemplateHTML($field, $fieldvalue)
		{

			$translations = array();
			$values       = explode(',', $fieldvalue);
			foreach ($values as $v) {
				$translations[] = JText::_($v);
			}

			return implode(',', $translations);
		}

	}


