<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FieldType_textArea
	 */
	class FieldType_textArea extends FactoryFieldTypes
	{
		/**
		 * type_name
		 *
		 * @var string
		 */
		var $type_name = "Simple Text Area";
		/**
		 * class_name
		 *
		 * @var string
		 */
		var $class_name = "textArea";
		/**
		 * has_options
		 *
		 * @var int
		 */
		var $has_options = 0;
		/**
		 * multiple
		 *
		 * @var int
		 */
		var $multiple = 0;
		/**
		 * sql_type
		 *
		 * @var string
		 */
		var $sql_type = "text";
		/**
		 * length
		 *
		 * @var null
		 */
		var $length = NULL;
		/**
		 * store_as_id
		 *
		 * @var bool
		 */
		var $store_as_id = FALSE;
		/**
		 * _params
		 *
		 * @var array
		 */
		var $_params = array("cols" => "numeric", "rows" => "numeric");

		/**
		 * getFieldHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return string
		 */
		public function getFieldHTML($field, $fieldvalue = NULL)
		{
			$fieldid          = $field->getHTMLId();
			$css_class        = $this->getCSSClass($field);
			$cols             = $field->getParam('cols', 60);
			$rows             = $field->getParam('rows', 15);
			$style_attributes = ($field->style_attr) ? "style='{$field->style_attr}'" : "";

			$fieldvalue = str_replace("<br />", PHP_EOL, $fieldvalue);
			$fieldvalue = str_replace("<br/>", PHP_EOL, $fieldvalue);
			$fieldvalue = str_replace("<br>", PHP_EOL, $fieldvalue);

			return "<textarea name='{$field->db_name}' class='{$css_class}' rows='$rows' cols='$cols' id='{$fieldid}' $style_attributes >{$fieldvalue}</textarea>";

		}

		/**
		 * getSearchHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return string
		 */
		public function getSearchHTML($field, $fieldvalue = NULL)
		{
			$fieldid    = $field->getHTMLId();
			$css_class  = $this->getCSSClass($field);
			$input_name = $field->page . '%' . $field->db_name;

			return "<input name='{$input_name}' class='{$css_class}' id='{$fieldid}' type='text' value='{$fieldvalue}' />";
		}

	}
