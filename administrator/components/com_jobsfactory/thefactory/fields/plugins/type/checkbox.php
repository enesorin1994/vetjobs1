<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FieldType_checkbox
	 */
	class FieldType_checkbox extends FactoryFieldTypes
	{
		/**
		 * type_name
		 *
		 * @var string
		 */
		var $type_name = "CheckBox";
		/**
		 * class_name
		 *
		 * @var string
		 */
		var $class_name = "checkBox";
		/**
		 * has_options
		 *
		 * @var int
		 */
		var $has_options = 1;
		/**
		 * multiple
		 *
		 * @var int
		 */
		var $multiple = 1;
		/**
		 * sql_type
		 *
		 * @var string
		 */
		var $sql_type = "varchar";
		/**
		 * length
		 *
		 * @var int
		 */
		var $length = 250;
		/**
		 * store_as_id
		 *
		 * @var bool
		 */
		var $store_as_id = FALSE;

		/**
		 * getSearchHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return string
		 */
		public function getSearchHTML($field, $fieldvalue = NULL)
		{
			$f          = clone $field;
			$f->db_name = $f->page . '%' . $f->db_name;

			return $this->getFieldHTML($f, $fieldvalue);
		}

		/**
		 * getFieldHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return string
		 */
		public function getFieldHTML($field, $fieldvalue = NULL)
		{
			$db = JFactory::getDbo();

			/* @var $field JTheFactoryFieldsTable */
			if ($fieldvalue) $value_arr = explode(',', $fieldvalue);
			else
				$value_arr = array();
			$css_class        = $this->getCSSClass($field);
			$style_attributes = ($field->style_attr) ? "style='{$field->style_attr}'" : "";
			$fieldid          = $field->getHTMLId();

			$checkboxes = array();
			$options    = $field->getOptions();
			// We must assure that array items are found in the checkbox options list
			foreach ($value_arr as $k => $itemVal) {
				$value_arr[$k] = html_entity_decode(trim($itemVal), ENT_QUOTES, 'UTF-8');
			}
			// For compulsory fields, if one of the defined options is checked,
			// the required restriction must be removed from all options (checkboxes) in order to not break the js validator.
			if(1 == $field->compulsory && count($value_arr)){
				$css_class = str_replace('required', '', $css_class);
			}

			if (count($options)) foreach ($options as $k => $option) {
				$selected = '';
				if (in_array($db->escape(trim($option->option_name)), $value_arr)) $selected = "checked='checked'";
				$valueStriped = $db->escape(trim($option->option_name));
				$checkboxes[] = "<span class='chk-item-box'><input type='checkbox' class='{$css_class}' $style_attributes name='{$field->db_name}[]' id='{$fieldid}_{$k}' $selected value=\"" . $valueStriped . "\" > " . trim(JText::_($option->option_name)) . "</span>";
			}

			return implode("\r\n", $checkboxes);
		}

		/**
		 * getValue
		 *
		 * @param $field
		 * @param $source_array
		 *
		 * @return string
		 */
		public function getValue($field, $source_array)
		{
			$val = parent::getValue($field, $source_array);
			if (!is_array($val)) $val = array($val);

			return implode(", ", $val);
		}

		/**
		 * htmlSearchLabel
		 *
		 * @param $field
		 * @param $searchValue
		 *
		 * @return string
		 */
		public function htmlSearchLabel($field, $searchValue)
		{
			$searchValue = (array)$searchValue;

			return implode(', ', $searchValue);
		}

		/**
		 * getSQLFilter
		 *
		 * @param      $field
		 * @param      $filter
		 * @param null $tableAlias
		 *
		 * @return string
		 */
		public function getSQLFilter($field, $filter, $tableAlias = NULL)
		{
			$cfg    = CustomFieldsFactory::getConfig();
			$db     = JFactory::getDBO();
			$filter = (array)$filter;

			if ($tableAlias) $table_alias = $tableAlias . ".";
			else
				$table_alias = isset($cfg['aliases'][$field->own_table]) ? ($cfg['aliases'][$field->own_table] . ".") : "";
			$sql = array();

			foreach ($filter as $v) $sql[] = "(" . $table_alias . $field->db_name . " LIKE " . $db->quote('%' . $v . '%') . ")";
			$ret = implode(' OR ', $sql);

			return ($ret) ? "($ret)" : $ret;
		}

		/**
		 * getTemplateHTML
		 *
		 * @param $field
		 * @param $fieldvalue
		 *
		 * @return string
		 */
		public function getTemplateHTML($field, $fieldvalue)
		{

			$translations = array();
			$values       = explode(',', $fieldvalue);
			foreach ($values as $v) {

				$translations[] = JText::_(stripslashes($v));
			}

			return implode(', ', $translations);
		}
	}
