<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FieldType_image
	 */
	class FieldType_image extends FactoryFieldTypes
	{
		/**
		 * type_name
		 *
		 * @var string
		 */
		var $type_name = "Image";
		/**
		 * class_name
		 *
		 * @var string
		 */
		var $class_name = "image";
		/**
		 * has_options
		 *
		 * @var int
		 */
		var $has_options = 0;
		/**
		 * multiple
		 *
		 * @var int
		 */
		var $multiple = 0;
		/**
		 * sql_type
		 *
		 * @var string
		 */
		var $sql_type = "varchar";
		/**
		 * length
		 *
		 * @var int
		 */
		var $length = 80;
		/**
		 * store_as_id
		 *
		 * @var bool
		 */
		var $store_as_id = FALSE;
		/**
		 * _params
		 *
		 * @var array
		 */
		var $_params = array("file_extensions" => "text", "upload_path" => "text", "thumbnail_width" => "numeric", "thumbnail_height" => "numeric", "image_width" => "numeric", "image_height" => "numeric");

		/**
		 * getFieldHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return string
		 */
		public function getFieldHTML($field, $fieldvalue = NULL)
		{
			$css_class        = $this->getCSSClass($field);
			$style_attributes = ($field->style_attr) ? "style='{$field->style_attr}'" : "";
			$fieldid          = $field->getHTMLId();
			$upload_path      = $field->getParam('upload_path', 'media/' . APP_EXTENSION . '/files/' . $field->db_name);

			if ($fieldvalue) {
				$url_path = str_replace(DIRECTORY_SEPARATOR, '/', $upload_path);

				$ret = "<img src='" . JURI::root() . "$url_path/$fieldvalue' width='150' />
                    <input type='hidden'  name='{$field->db_name}' id='{$fieldid}' class='{$css_class}' value='{$fieldvalue}' />";
				if ($field->compulsory) {
					$ret .= "<br /> " . JText::_("FACTORY_REPLACE_WITH") . ":  <br />";
					$ret .= "<br /> <input type='file' name='{$field->db_name}_replace' id='{$fieldid}' {$style_attributes} />";
				} else
					$ret .= "<input type='checkbox' value='1' class='{$css_class}' name='{$field->db_name}_delete' /> " . JText::_("FACTORY_REMOVE");

				return $ret;

			} else
				return " <input type='file' name='{$field->db_name}' class='{$css_class}' id='{$fieldid}' {$style_attributes} />";
		}

		/**
		 * getValue
		 *
		 * @param $field
		 * @param $source_array
		 *
		 * @return null|string
		 */
		public function getValue($field, $source_array)
		{
			jimport('joomla.filesystem.file');
			$input = JFactory::getApplication()->input;

			require_once(JPATH_SITE . '/components/' . APP_EXTENSION . '/thefactory/front.images.php');
			$imgTrans           = new JTheFactoryImages();
			$upload_path        = $field->getParam('upload_path', 'media/' . APP_EXTENSION . '/files/' . $field->db_name);
			$allowed_extensions = explode(',', $field->getParam('file_extensions', 'jpg,gif,jpeg,png'));

			$delete_file = JArrayHelper::getValue($source_array, "{$field->db_name}_delete", 0, "INT");

			if ($delete_file) {
				return "";
			}

			$files = $input->files->getArray($_FILES);
			$file  =isset($files[$field->db_name . '_replace'])?$files[$field->db_name . '_replace']:null;
			if (!$file['name']) $file = isset($files[$field->db_name])?$files[$field->db_name]:null;

			$fname =isset($file['name'])?$file['name']:null;
			if (!is_uploaded_file($file['tmp_name'])) return NULL;
			$ext = strtolower(JFile::getExt($fname));
			if (!in_array($ext, $allowed_extensions)) return NULL;
			$file_name = JFile::makesafe('custom-' . trim($field->db_name) . '-' . time() . ".$ext");

			JFile::upload($file['tmp_name'], $upload_path . '/' . $file_name);
			$imgTrans->resize_image_no_prefix($upload_path . '/' . $file_name, $field->getParam("image_width", "800") - 10, $field->getParam("image_height", "600") - 10);

			return $file_name;
		}


		/**
		 * getTemplateHTML
		 *
		 * @param $field
		 * @param $fieldvalue
		 *
		 * @return string
		 */
		public function getTemplateHTML($field, $fieldvalue)
		{
			if (!$fieldvalue) return "";

			JHTML::_("behavior.modal");
			$upload_path = $field->getParam('upload_path', 'media/' . APP_EXTENSION . '/files/' . $field->db_name);
			$url_path    = str_replace(DIRECTORY_SEPARATOR, '/', $upload_path);

			$css_atribs   = array("border:none", "vertical-align:middle");
			$css_atribs[] = "width:" . $field->getParam("thumbnail_width", 150) . 'px';
			$css_atribs[] = "height:" . $field->getParam("thumbnail_height", 150) . 'px';

			$rel = "{handler: 'iframe', size: {x: " . $field->getParam("image_width", "800") . ", y: " . $field->getParam("image_height", "600") . "}}";
			$css = "style='" . implode(";", $css_atribs) . "' ";

			return "<a class='modal' rel=\"{$rel}\" href='" . JURI::root() . "{$url_path}/{$fieldvalue}' ><img src='" . JURI::root() . "{$url_path}/{$fieldvalue}' {$css} /></a>";
		}


	} // End Class
