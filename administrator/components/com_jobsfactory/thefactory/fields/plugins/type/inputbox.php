<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FieldType_inputBox
	 */
	class FieldType_inputBox extends FactoryFieldTypes
	{

		/**
		 * type_name
		 *
		 * @var string
		 */
		var $type_name = "Text Input Field";
		/**
		 * class_name
		 *
		 * @var string
		 */
		var $class_name = "inputBox";
		/**
		 * has_options
		 *
		 * @var int
		 */
		var $has_options = 0;
		/**
		 * multiple
		 *
		 * @var int
		 */
		var $multiple = 0;
		/**
		 * sql_type
		 *
		 * @var string
		 */
		var $sql_type = "varchar";
		/**
		 * length
		 *
		 * @var int
		 */
		var $length = 250;
		/**
		 * store_as_id
		 *
		 * @var bool
		 */
		var $store_as_id = FALSE;
		/**
		 * _params
		 *
		 * @var array
		 */
		var $_params = array("size" => "numeric", "inSearchFormsShowAsSelectList" => "radio_bool");

		/**
		 * getFieldHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return string
		 */
		public function getFieldHTML($field, $fieldvalue = NULL)
		{
			$fieldid          = $field->getHTMLId();
			$css_class        = $this->getCSSClass($field);
			$style_attributes = ($field->style_attr) ? "style='{$field->style_attr}'" : "";
			$size             = $field->getParam('size', '30');

			return "<input type='text' name='{$field->db_name}' id='{$fieldid}' size='$size' $style_attributes class='{$css_class}' value='" . trim($fieldvalue) . "'>";
		}

		/**
		 * getSearchHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return mixed|string
		 */
		public function getSearchHTML($field, $fieldvalue = NULL)
		{
			$inSearchFormsShowAsSelectList = $field->getParam('inSearchFormsShowAsSelectList', 0);

			if ($inSearchFormsShowAsSelectList) {

				return $this->getInputAsSelectListHTML($field, $fieldvalue);

			} else { // List as a normal text input field
				$f          = clone $field;
				$f->db_name = $f->page . '%' . $f->db_name;

				return $this->getFieldHTML($f, $fieldvalue);
			}

		}

		/**
		 * getInputAsSelectListHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return mixed
		 */
		protected function getInputAsSelectListHTML($field, $fieldvalue = NULL)
		{
			$fieldid          = $field->getHTMLId();
			$css_class        = $this->getCSSClass($field);
			$size             = $field->getParam('size', 1);
			$width            = $field->getParam('width', 150);
			$style_attributes = "style='width:" . $width . "px';{$field->style_attr}'";

			$cfg = CustomFieldsFactory::getConfig();
			$db  = JFactory::getDbo();

			$db->setQuery("SELECT DISTINCT  `{$field->db_name}` AS `option_name` FROM " . $cfg['tables'][$field->page] . " WHERE `{$field->db_name}` IS NOT NULL");
			$options = $db->loadObjectList();

			$opt_array   = array();
			$opt_array[] = JHTML::_('select.option', '', JText::_('FACTORY_ALL'));
			if (count($options)) foreach ($options as $k => $option) $opt_array[] = JHTML::_('select.option', $option->option_name, JText::_($option->option_name));

			return JHTML::_('select.genericlist', $opt_array, $field->page . '%' . $field->db_name, "size='$size' class='$css_class' $style_attributes id='$fieldid'", 'value', 'text', $fieldvalue);

		}


	}


