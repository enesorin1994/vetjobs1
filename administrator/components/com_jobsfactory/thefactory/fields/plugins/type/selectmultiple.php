<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FieldType_selectmultiple
	 */
	class FieldType_selectmultiple extends FactoryFieldTypes
	{
		/**
		 * type_name
		 *
		 * @var string
		 */
		var $type_name = "Multiple Value Select List";
		/**
		 * class_name
		 *
		 * @var string
		 */
		var $class_name = "selectmultiple";
		/**
		 * has_options
		 *
		 * @var int
		 */
		var $has_options = 1;
		/**
		 * multiple
		 *
		 * @var int
		 */
		var $multiple = 1;
		/**
		 * sql_type
		 *
		 * @var string
		 */
		var $sql_type = "varchar";
		/**
		 * length
		 *
		 * @var int
		 */
		var $length = 250;
		/**
		 * store_as_id
		 *
		 * @var bool
		 */
		var $store_as_id = FALSE;
		/**
		 * _params
		 *
		 * @var array
		 */
		var $_params = array("size" => "numeric", "width" => "numeric");

		/**
		 * getValue
		 *
		 * @param $field
		 * @param $source_array
		 *
		 * @return string
		 */
		public function getValue($field, $source_array)
		{
			$val = parent::getValue($field, $source_array);
			if (!is_array($val)) $val = array($val);

			return implode(",", $val);
		}

		/**
		 * getSQLFilter
		 *
		 * @param      $field
		 * @param      $filter
		 * @param null $tableAlias
		 *
		 * @return string
		 */
		public function getSQLFilter($field, $filter, $tableAlias = NULL)
		{
			$cfg = CustomFieldsFactory::getConfig();
			$db  = JFactory::getDbo();

			if ($tableAlias) $table_alias = $tableAlias . ".";
			else
				$table_alias = isset($cfg['aliases'][$field->own_table]) ? ($cfg['aliases'][$field->own_table] . ".") : "";


			$sql = " " . $table_alias . $field->db_name . " LIKE " . $db->quote('%' . $filter . '%');

			return $sql;
		}

		/**
		 * getFieldHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return mixed
		 */
		public function getFieldHTML($field, $fieldvalue = NULL)
		{
			$db        = JFactory::getDbo();
			$fieldid   = $field->getHTMLId();
			$css_class = $this->getCSSClass($field);
			$size      = $field->getParam('size', 5);
			$width     = $field->getParam('width', 100);

			if (is_array($fieldvalue)) $value_arr = $fieldvalue;
			else
				$value_arr = explode(",", $fieldvalue);

			foreach ($value_arr as $k => $item) {
				$value_arr[$k] = html_entity_decode($item, ENT_QUOTES, 'UTF-8');
			}

			$style_attributes = "style='width:{$width}px;{$field->style_attr}'";
			$options          = $field->getOptions();

			$opt_array = array();
			if (count($options)) foreach ($options as $k => $option) {
				$valueStriped = $db->escape(trim($option->option_name));
				$opt_array[]  = JHTML::_('select.option', $valueStriped, JText::_($option->option_name));
			}

			return JHTML::_('select.genericlist', $opt_array, $field->db_name . '[]', "multiple size='$size' class='$css_class' $style_attributes", 'value', 'text', $value_arr, $fieldid);
		}

		/**
		 * getSearchHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return mixed
		 */
		public function getSearchHTML($field, $fieldvalue = NULL)
		{
			$fieldid   = $field->getHTMLId();
			$css_class = $this->getCSSClass($field);
			$size      = $field->getParam('size', 5);
			$width     = $field->getParam('width', 100);

			$style_attributes = "style='width:{$width}px;{$field->style_attr}'";
			$options          = $field->getOptions();

			$opt_array = array();
			if (count($options)) foreach ($options as $k => $option) $opt_array[] = JHTML::_('select.option', $option->option_name, JText::_($option->option_name));

			return JHTML::_('select.genericlist', $opt_array, $field->page . '%' . $field->db_name, "size='$size' class='$css_class' $style_attributes id='$fieldid'", 'value', 'text', $fieldvalue);

		}

		/**
		 * htmlSearchLabel
		 *
		 * @param $field
		 * @param $searchValue
		 *
		 * @return string
		 */
		public function htmlSearchLabel($field, $searchValue)
		{

			$searchValue = (array)$searchValue;

			return implode(',', $searchValue);
		}

		/**
		 * getTemplateHTML
		 *
		 * @param $field
		 * @param $fieldvalue
		 *
		 * @return string
		 */
		public function getTemplateHTML($field, $fieldvalue)
		{

			$translations = array();
			$values       = explode(',', $fieldvalue);
			foreach ($values as $v) {
				$translations[] = JText::_(stripslashes($v));
			}

			return implode(',', $translations);
		}

	}

