<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FieldType_selectList
	 */
	class FieldType_selectList extends FactoryFieldTypes
	{
		/**
		 * type_name
		 *
		 * @var string
		 */
		var $type_name = "Single Value Select List";
		/**
		 * class_name
		 *
		 * @var string
		 */
		var $class_name = "selectList";
		/**
		 * has_options
		 *
		 * @var int
		 */
		var $has_options = 1;
		/**
		 * multiple
		 *
		 * @var int
		 */
		var $multiple = 0;
		/**
		 * sql_type
		 *
		 * @var string
		 */
		var $sql_type = "varchar";
		/**
		 * length
		 *
		 * @var int
		 */
		var $length = 200;
		/**
		 * store_as_id
		 *
		 * @var bool
		 */
		var $store_as_id = FALSE;
		/**
		 * _params
		 *
		 * @var array
		 */
		var $_params = array("size" => "numeric", "width" => "numeric", "filterOptsInSearchFrm" => "radio_bool");

		/**
		 * getFieldHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return mixed
		 */
		public function getFieldHTML($field, $fieldvalue = NULL)
		{
			$fieldid   = $field->getHTMLId();
			$css_class = $this->getCSSClass($field);
			$size      = $field->getParam('size', 1);
			$width     = $field->getParam('width', 100);

			$style_attributes = "style='width:$width;{$field->style_attr}'";
			$options          = $field->getOptions();

			$opt_array = array();
			if (!$field->compulsory) $opt_array[] = JHTML::_('select.option', '', JText::_(''));
			if (count($options)) foreach ($options as $k => $option) $opt_array[] = JHTML::_('select.option', $option->option_name, JText::_($option->option_name));

			return JHTML::_('select.genericlist', $opt_array, $field->db_name, "size='$size' class='$css_class' $style_attributes", 'value', 'text', $fieldvalue, $fieldid);
		}

		/**
		 * getSearchHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return mixed
		 */
		public function getSearchHTML($field, $fieldvalue = NULL)
		{
			$fieldid               = $field->getHTMLId();
			$css_class             = $this->getCSSClass($field);
			$size                  = $field->getParam('size', 1);
			$width                 = $field->getParam('width', 100);
			$filterOptsInSearchFrm = $field->getParam('filterOptsInSearchFrm', 0);

			$style_attributes = "style='width:" . $width . "px;{$field->style_attr}'";
			$options          = $field->getOptions();

			if ($filterOptsInSearchFrm) {
				$cfg = CustomFieldsFactory::getConfig();

				$db = JFactory::getDbo();
				$db->setQuery("SELECT DISTINCT  `{$field->db_name}` AS `option_name` FROM " . $cfg['tables'][$field->page]);
				$usedFieldOptions = $db->loadObjectList();

				foreach ($options as $k => $fopt) {
					$isUsed = FALSE;
					foreach ($usedFieldOptions as $ufopt) {
						if ($fopt->option_name == $ufopt->option_name) {
							$isUsed = TRUE;
						}
					}
					if (!$isUsed) {
						unset($options[$k]);
					}

				}
			}
			$opt_array   = array();
			$opt_array[] = JHTML::_('select.option', '', JText::_('FACTORY_ALL'));
			if (count($options)) foreach ($options as $k => $option) $opt_array[] = JHTML::_('select.option', $option->option_name, JText::_($option->option_name));

			return JHTML::_('select.genericlist', $opt_array, $field->page . '%' . $field->db_name, "size='$size' class='$css_class' $style_attributes id='$fieldid'", 'value', 'text', $fieldvalue);

		}

		/**
		 * getTemplateHTML
		 *
		 * @param $field
		 * @param $fieldvalue
		 *
		 * @return string
		 */
		public function getTemplateHTML($field, $fieldvalue)
		{
			return JText::_($fieldvalue);
		}
	}

