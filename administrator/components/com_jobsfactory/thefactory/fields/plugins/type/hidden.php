<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FieldType_hidden
	 */
	class FieldType_hidden extends FactoryFieldTypes
	{
		/**
		 * type_name
		 *
		 * @var string
		 */
		var $type_name = "Hidden field";
		/**
		 * class_name
		 *
		 * @var string
		 */
		var $class_name = "hidden";
		/**
		 * has_options
		 *
		 * @var int
		 */
		var $has_options = 0;
		/**
		 * multiple
		 *
		 * @var int
		 */
		var $multiple = 0;
		/**
		 * sql_type
		 *
		 * @var string
		 */
		var $sql_type = "varchar";
		/**
		 * length
		 *
		 * @var int
		 */
		var $length = 250;
		/**
		 * store_as_id
		 *
		 * @var bool
		 */
		var $store_as_id = FALSE;
		/**
		 * _params
		 *
		 * @var array
		 */
		var $_params = array("default_value" => "text");

		/**
		 * getFieldHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return string
		 */
		public function getFieldHTML($field, $fieldvalue = NULL)
		{
			$fieldid = $field->getHTMLId();

			return "<input type='hidden' name='{$field->db_name}' id='{$fieldid}' value={$fieldvalue} />" . $fieldvalue;
		}

		/**
		 * getSearchHTML
		 *
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return string
		 */
		public function getSearchHTML($field, $fieldvalue = NULL)
		{
			$fieldid    = ($field->field_id) ? ($field->field_id) : ($field->db_name);
			$css_class  = $this->getCSSClass($field);
			$input_name = $field->page . '%' . $field->db_name;

			return "<input type='text' name='{$input_name}' id='{$fieldid}' class='{$css_class}' value='" . trim($fieldvalue) . "'>";
		}
	}
