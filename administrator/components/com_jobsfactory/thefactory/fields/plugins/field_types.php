<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class FactoryFieldTypes
	 */
	abstract class FactoryFieldTypes extends JObject
	{

		/**
		 * type_name
		 *
		 * @var null
		 */
		var $type_name = NULL;
		/**
		 * class_name
		 *
		 * @var null
		 */
		var $class_name = NULL;
		/**
		 * has_options
		 *
		 * @var null
		 */
		var $has_options = NULL;
		/**
		 * multiple
		 *
		 * @var null
		 */
		var $multiple = NULL;
		/**
		 * sql_type
		 *
		 * @var null
		 */
		var $sql_type = NULL;
		/**
		 * length
		 *
		 * @var null
		 */
		var $length = NULL;
		/**
		 * store_as_id
		 *
		 * @var bool
		 */
		var $store_as_id = FALSE;

		/**
		 * _params
		 *
		 * @var null
		 */
		var $_params = NULL; //parameter definitions

		/**
		 * @param $field
		 *
		 * @return string
		 */
		public function getCSSClass($field)
		{
			$css_classes = array();
			if ($field->css_class) $css_classes[] = $field->css_class;
			if ($field->compulsory == 1) $css_classes[] = "required";
			if ($field->validate_type) $css_classes[] = "validate-" . strtolower($field->validate_type);

			return implode(" ", $css_classes);
		}

		/**
		 * @param $field
		 * @param $source_array
		 *
		 * @return mixed
		 */
		public function getValue($field, $source_array)
		{
			return JArrayHelper::getValue($source_array, $field->db_name);
		}

		/**
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return mixed
		 */
		public function getSearchHTML($field, $fieldvalue = NULL)
		{
			//abstract
			return $field->db_name;
		}

		/**
		 * @param      $field
		 * @param null $fieldvalue
		 *
		 * @return mixed
		 */
		public function getFieldHTML($field, $fieldvalue = NULL)
		{
			//abstract
			return $field->db_name;
		}

		/**
		 * @param $field
		 * @param $fieldvalue
		 *
		 * @return mixed
		 */
		public function getTemplateHTML($field, $fieldvalue)
		{
			return $fieldvalue;
		}

		/**
		 * @param      $field
		 * @param      $filter
		 * @param null $tableAlias
		 *
		 * @return string
		 */
		public function getSQLFilter($field, $filter, $tableAlias = NULL)
		{

			$cfg = CustomFieldsFactory::getConfig();
			$db  = JFactory::getDbo();
			if ($tableAlias) $table_alias = $tableAlias . ".";
			else
				$table_alias = isset($cfg['aliases'][$field->own_table]) ? ($cfg['aliases'][$field->own_table] . ".") : "";


			$sql = " " . $table_alias . $field->db_name . "=" . $db->quote($filter);

			return $sql;
		}

		/**
		 * @param $field
		 * @param $searchValue
		 *
		 * @return mixed
		 */
		public function htmlSearchLabel($field, $searchValue)
		{
			return $searchValue;
		}

	} // End Class
