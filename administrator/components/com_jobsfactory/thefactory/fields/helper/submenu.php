<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * JTheFactoryFieldsSubmenu
	 */
	class JTheFactoryFieldsSubmenu
	{
		/**
		 * subMenuEditField
		 *
		 * @static
		 *
		 */
		public static function subMenuEditField()
		{
			$task     = JFactory::getApplication()->input->getCmd('task', '');
			$jVersion = new JVersion();
			if ($jVersion->isCompatible('3.0')) {
				JHtmlSidebar::addEntry(JText::_('FACTORY_FIELD_LIST'), 'index.php?option=' . APP_EXTENSION . '&task=fields.listfields', 'fields.listfields' == $task ? TRUE : FALSE);
			} else {
				JSubMenuHelper::addEntry(JText::_('FACTORY_FIELD_LIST'), 'index.php?option=' . APP_EXTENSION . '&task=fields.listfields', FALSE);
			}

		}

		/**
		 * subMenuListFields
		 *
		 * @static
		 *
		 */
		public static function subMenuListFields()
		{
			$task     = JFactory::getApplication()->input->getCmd('task', '');
			$jVersion = new JVersion();
			if ($jVersion->isCompatible('3.0')) {
				JHtmlSidebar::addEntry(JText::_('FACTORY_FIELD_LIST'), 'index.php?option=' . APP_EXTENSION . '&task=fields.listfields', 'fields.listfields' == $task ? TRUE : FALSE);
				JHtmlSidebar::addEntry(JText::_('FACTORY_PUBLISH_ON_TEMPLATE'), 'index.php?option=' . APP_EXTENSION . '&task=positions.listfields', 'positions.listfields' == $task ? TRUE : FALSE);
			} else {
				JSubMenuHelper::addEntry(JText::_('FACTORY_FIELD_LIST'), 'index.php?option=' . APP_EXTENSION . '&task=fields.listfields', FALSE);
				JSubMenuHelper::addEntry(JText::_('FACTORY_PUBLISH_ON_TEMPLATE'), 'index.php?option=' . APP_EXTENSION . '&task=positions.listfields', FALSE);
			}
		}

	}
