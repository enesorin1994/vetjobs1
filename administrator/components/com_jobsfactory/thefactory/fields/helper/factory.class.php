<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class CustomFieldsFactory
	{
		/**
		 * getFieldType
		 *
		 * @static
		 *
		 * @param string $typename
		 *
		 * @return FactoryFieldTypes
		 */
		public static function getFieldType($typename)
		{
			static $instances;

			if (!isset($instances[$typename])) {
				$field_class = "FieldType_" . $typename;
				if (!class_exists($field_class)) require_once(dirname(__FILE__) . '/../plugins/type/' . strtolower($typename) . '.php');
				$instances[$typename] = new $field_class;
			}

			return $instances[$typename];
		}

		/**
		 * getFieldValidator
		 *
		 * @static
		 *
		 * @param $validatorname
		 *
		 * @return FactoryFieldValidator
		 */
		public static function getFieldValidator($validatorname)
		{
			static $instances;

			if (!isset($instances[$validatorname])) {
				$field_class = "FieldValidator_" . $validatorname;
				if (!class_exists($field_class)) require_once(dirname(__FILE__) . '/../plugins/validators/' . strtolower($validatorname) . '.php');
				$instances[$validatorname] = new $field_class;
			}

			return $instances[$validatorname];

		}

		/**
		 * getConfig
		 *
		 * @static
		 *
		 * @return array  Configuration array ("pages"=>, "tables"=> , "pk"=> , "aliases"=> , "has_category"=>)
		 */
		public static function getConfig()
		{
			static $instance;

			if (!isset($instance)) {
				$instance = array();
				$MyApp    = JTheFactoryApplication::getInstance();


				$instance['pages']   = array();
				$instance['tables']  = array();
				$instance['pk']      = array();
				$instance['aliases'] = array();

				$pages               = explode(",", $MyApp->getIniValue('pages', 'custom-fields'));
				$page_names          = explode(",", $MyApp->getIniValue('page_names', 'custom-fields'));
				$tables              = explode(",", $MyApp->getIniValue('tables', 'custom-fields'));
				$pk                  = explode(",", $MyApp->getIniValue('pk', 'custom-fields'));
				$aliases             = explode(",", $MyApp->getIniValue('aliases', 'custom-fields'));
				$pages_with_category = explode(",", $MyApp->getIniValue('pages_with_category', 'custom-fields'));

				$i = 0;
				foreach ($pages as $page) {
					$instance['pages'][$page]         = $page_names[$i];
					$instance['tables'][$page]        = $tables[$i];
					$instance['pk'][$page]            = $pk[$i];
					$instance['aliases'][$tables[$i]] = $aliases[$i];
					$instance['has_category'][$page]  = in_array($page, $pages_with_category);
					$i++;
				}
			}

			return $instance;
		}

		/**
		 * getFieldObject
		 *
		 * @static
		 *
		 * @param string $db_name field name in DB
		 *
		 * @return JObject  Field Table row
		 */
		static function getFieldObject($db_name)
		{
			static $instances;

			if (!isset($instances[$db_name])) {
				$db = JFactory::getDbo();
				$q  = "SELECT * FROM `#__" . APP_PREFIX . "_fields` WHERE `db_name` = '{$db_name}'";
				$db->setQuery($q);
				$instances[$db_name] = $db->loadObject();
			}

			return $instances[$db_name];
		}

		/**
		 * _filterField
		 *
		 * @param $field_list
		 * @param $page
		 *
		 * @return array
		 */
		private static function _filterField($field_list, $page)
		{
			$page_fields = array();
			foreach ($field_list as $field) if ($field->page == $page) $page_fields[] = $field;

			return $page_fields;
		}

		/**
		 * getFieldsList
		 *
		 * @static
		 *
		 * @param null $page
		 *
		 * @return array|mixed
		 */
		public static function getFieldsList($page = NULL)
		{
			static $instance;
			if (!isset($instance)) {
				$db = JFactory::getDbo();
				$db->setQuery("SELECT * FROM #__" . APP_PREFIX . "_fields WHERE `status`= 1 ORDER BY `ordering`");
				$instance = $db->loadObjectList();
			}
			if (!$page || !count($instance)) return $instance;
			else {
				$res = self::_filterField($instance, $page);

				return $res;
			}
		}

		/**
		 * getSearchableFieldsList
		 *
		 * @static
		 *
		 * @param null $page
		 *
		 * @return array|mixed
		 */
		public static function getSearchableFieldsList($page = NULL)
		{
			static $instance;
			if (!isset($instance)) {
				$db = JFactory::getDbo();
				$db->setQuery("SELECT * FROM #__" . APP_PREFIX . "_fields WHERE `search` = 1 AND `status`= 1 ORDER BY `ordering`");
				$instance = $db->loadObjectList();
			}
			if (!$page || !count($instance)) return $instance;
			else {
				$res = self::_filterField($instance, $page);

				return $res;
			}

		}

		/**
		 * getValidatorsList
		 *
		 * @static
		 * @return array|mixed
		 */
		public static function getValidatorsList()
		{
			static $validators;
			if (!isset($validators)) {
				jimport('joomla.filesystem.folder');
				$validators = JFolder::files(dirname(__FILE__) . '/../plugins/validators', '\.php$');
				$validators = preg_replace('/\.php$/', '', $validators);
			}

			return $validators;

		}

		/**
		 * getFieldTypesList
		 *
		 * @static
		 * @return array|mixed
		 */
		public static function getFieldTypesList()
		{
			static $ftypes;
			if (!isset($ftypes)) {
				jimport('joomla.filesystem.folder');
				$ftypes = JFolder::files(dirname(__FILE__) . '/../plugins/type', '\.php$');
				$ftypes = preg_replace('/\.php$/', '', $ftypes);
			}

			return $ftypes;

		}

		/**
		 * getPagesList
		 *
		 * @static
		 * @return array
		 */
		public static function getPagesList()
		{
			$MyApp = JTheFactoryApplication::getInstance();
			$pages = explode(",", $MyApp->getIniValue('pages', 'custom-fields'));

			return $pages;
		}

		/**
		 * addValidatorJS
		 *
		 * @static
		 *
		 * @param $validatorname
		 *
		 * @return mixed
		 */
		public static function addValidatorJS($validatorname)
		{
			static $instances;
			if (!$validatorname) return;
			if (isset($instances[$validatorname])) return;
			$validator = self::getFieldValidator($validatorname);
			$js        = $validator->validateJS();
			if ($js) {
				$doc = JFactory::getDocument();
				$doc->addScriptDeclaration($js);
			}
		}

		/**
		 * @param $table string Table Name
		 *
		 * @return mixed
		 */
		public static function getTableFields($table)
		{
			static $instances;
			if (isset($instances[$table])) return $instances[$table];

			$db = JFactory::getDbo();
			$db->setQuery("SELECT * FROM #__" . APP_PREFIX . "_fields WHERE own_table = '{$table}' AND status = 1 ORDER BY ordering ");
			$instances[$table] = $db->loadObjectList();

            if (isset($instances[$table])) return $instances[$table];
		}

	} // End Class
