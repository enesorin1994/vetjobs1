<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: fields
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<div class = '<?php echo $this->classWidth; ?>'>
	<fieldset class = "adminform">
		<legend><?php echo JText::_('FACTORY_MAIN_SETTINGS'); ?></legend>
		<table class = "paramlist admintable" border = "0" width = "100%">
			<tr>
				<td class = "paramlist_key" align = "right" width = "250">
					<label><?php echo JText::_("FACTORY_FIELD_LABEL"); ?> </label>
				</td>
				<td class = "paramlist_value">
					<input type = "text" name = "name" style = "width:200px;" value = "<?php echo $this->field->name; ?>" />
					<?php echo $this->img_required; ?>
				</td>
			</tr>
			<tr>
				<td class = "paramlist_key" align = "right">
					<label class = "hasTip" title = "<?php echo JText::_("FACTORY_F_FIELDDBNAME_HELP"); ?>"><?php echo JText::_("FACTORY_DATABASE_FIELD_NAME"); ?></label>
				</td>
				<td class = "paramlist_value">
					<input type = "text" name = "db_name" style = "width:200px;"
					       value = "<?php echo $this->field->db_name; ?>" <?php if ($this->field->id) {
						?> readonly = "readonly" disabled = "disabled" <?php } ?> />
					<?php echo $this->img_required; ?>
				</td>
			</tr>
			<tr>
				<td class = "paramlist_key" align = "right">
					<label><?php echo JText::_("FACTORY_FIELD_TYPE"); ?> </label>
				</td>
				<td class = "paramlist_value">
					<?php echo $this->lists->get("field_types_html"); ?>
					<?php echo $this->img_required; ?>
				</td>
			</tr>
			<tr>
				<td class = "paramlist_key" align = "right">
					<label><?php echo JText::_("FACTORY_FIELD_SECTION"); ?> </label>
				</td>
				<td class = "paramlist_value">
					<?php echo $this->lists->get("field_pages"); ?>
					<?php echo $this->img_required; ?>
				</td>
			</tr>
			<tr>
				<td class = "paramlist_key" align = "right">
					<label><?php echo JText::_("FACTORY_FIELD_DESCRIPTION_OR_HELP_TEXT_FOR_USERS"); ?> </label>
				</td>
				<td class = "paramlist_value">
					<textarea name = "help" style = "width:250px;"><?php echo $this->field->help; ?></textarea>
				</td>
			</tr>
			<tr>
				<td class = "paramlist_key" align = "right">
					<label class = "hasTip" title = "<?php echo JText::_("FACTORY_F_ENABLED_HELP"); ?>"><?php echo JText::_("FACTORY_FIELD_ENABLED"); ?> </label>
				</td>
				<td class = "paramlist_value">
					<?php echo $this->lists->get("status"); ?>
				</td>
			</tr>
		</table>
	</fieldset>
</div>
