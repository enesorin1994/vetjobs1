<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: fields
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<div class = '<?php echo $this->classWidth; ?>'>
	<fieldset class = "adminform">
		<legend><?php echo JText::_('FACTORY_HTML_INPUT_FORM_DISPLAY_SETTINGS'); ?></legend>
		<table class = "paramlist admintable" border = "0">
			<tr>
				<td class = "paramlist_key" align = "right" width = "200px">
					<label class = "hasTip" title = "<?php echo JText::_("FACTORY_F_FIELDID_HELP"); ?>"><?php echo JText::_("FACTORY_HTML_FIELD_ID"); ?> </label>
				</td>
				<td class = "paramlist_value">
					<input type = "text" name = "field_id" style = "width:200px;" readonly = "readonly"
					       value = "<?php echo $this->field->getHTMLId(); ?>" />
				</td>
			</tr>
			<tr>
				<td class = "paramlist_key" align = "right">
					<label><?php echo JText::_("FACTORY_CSS_CLASS"); ?> </label>
				</td>
				<td class = "paramlist_value">
					<input type = "text" name = "css_class" style = "width:200px;"
					       value = "<?php echo $this->field->css_class; ?>" />
				</td>
			</tr>
			<tr>
				<td class = "paramlist_key" align = "right">
					<label><?php echo JText::_("FACTORY_INLINE_CSS_STYLE_ATTRIBUTES"); ?> </label>
				</td>
				<td class = "paramlist_value">
					<textarea name = "style_attr" style = "width:250px;"><?php echo $this->field->style_attr; ?></textarea>
				</td>
			</tr>
		</table>
	</fieldset>
</div>
