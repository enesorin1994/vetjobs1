<?php
/**------------------------------------------------------------------------
thefactory - The Factory Class Library - v 3.0.0
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: thefactory
 * @subpackage: fields
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');
?>
<div class='<?php echo $this->classWidth; ?>'>
    <fieldset class="adminform">
        <legend><?php echo JText::_('FACTORY_FIELD_BEHAVIOUR'); ?> </legend>
        <table class="paramlist admintable" border="0">
            <tr>
                <td class="paramlist_key" align="right" width="200">
                    <label class="hasTip" title="<?php echo JText::_("FACTORY_F_COMPULSORY_HELP"); ?>"><?php echo JText::_("FACTORY_COMPULSORY_FIELD"); ?> </label>
                </td>
                <td class="paramlist_value">
			<?php echo $this->lists->get("compulsory"); ?>
                </td>
            </tr>
            <tr>
                <td class="paramlist_key" align="right">
                    <label><?php echo JText::_("FACTORY_FIELD_VALIDATOR"); ?> </label>
                </td>
                <td class="paramlist_value">
			<?php echo $this->lists->get("validate_type"); ?>
                </td>
            </tr>
            <tr>
                <td class="paramlist_key" align="right">
                    <label class="hasTip" title="<?php echo JText::_("FACTORY_F_SEARCH_HELP"); ?>"><?php echo JText::_("FACTORY_SEARCHABLE_FIELD");?> </label>
                </td>
                <td class="paramlist_value">
			<?php echo $this->lists->get("search"); ?>
                </td>
            </tr>
            <tr>
                <td class="paramlist_key" align="right">
                    <label><?php echo JText::_("FACTORY_ORDERING");?> </label>
                </td>
                <td class="paramlist_value">
                    <input type="text" name="ordering" style="width:20px;"
                           value="<?php echo $this->field->ordering; ?>"/>
                </td>
            </tr>
        </table>
    </fieldset>
</div>
