<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: fields
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	JHtml::_('formbehavior.chosen', 'select');
?>

<div class = "row-fluid">
	<div class = "span12">
		<form action = "index.php" xmlns = "http://www.w3.org/1999/html" method = "get" name = "adminForm" id = "adminForm">
			<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
			<input type = "hidden" name = "task" value = "fields.listfields" />
			<input type = "hidden" name = "boxchecked" value = "0" />

			<?php if (!empty($this->sidebar)): ?>
				<div id = "j-sidebar-container" style = "padding-right: 15px;">
					<span style = "white-space: nowrap;"><?php echo $this->sidebar; ?></span>
				</div>
			<?php endif; ?>


			<div id = "j-main-container">
				<table width = "100%" cellpadding = "0" cellspacing = "1">
					<tr>
						<td><?php echo JText::_("FACTORY_SHOW_ONLY_FIELDS_FOR_PAGE"), "&nbsp;", $this->filter_html['page']; ?></td>
					</tr>
				</table>
				<br />
				<table class = "table adminlist table-condensed" cellspacing = "1">
					<thead>
					<tr>
						<th width = "5" align = "center">#</th>
						<th width = "80" align = "center">
							<a href = "javascript:Joomla.submitbutton('fields.reorder')" class = "saveorder"
							   title = "<?php echo JText::_('FACTORY_SAVE_ORDERING'); ?>"></a>
						</th>
						<th class = "title" width = "10%" nowrap = "nowrap"><?php echo JText::_("FACTORY_SECTION"); ?></th>
						<th class = "title"><?php echo JText::_("FACTORY_FIELD_NAME"); ?></th>
						<th class = "title" width = "10%"><?php echo JText::_("FACTORY_HTML_ID"); ?></th>
						<th class = "title" width = "10%" nowrap = "nowrap"><?php echo JText::_("FACTORY_FIELD_TYPE"); ?></th>
						<th class = "title" width = "3%"><?php echo JText::_("FACTORY_ENABLED"); ?></th>
						<th class = "title" width = "3%"><?php echo JText::_("FACTORY_COMPULSORY"); ?></th>
						<th class = "title" width = "3%"><?php echo JText::_("FACTORY_SEARCHABLE"); ?></th>
						<th class = "title" width = "3%"><?php echo JText::_("FACTORY_CATEGORY_FILTER"); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$k = 0;
						for ($i = 0, $n = count($this->rows); $i < $n; $i++) {
							$row       = &$this->rows[$i];
							$link      = 'index.php?option=' . APP_EXTENSION . '&task=fields.edit&id=' . $row->id;
							$link_page = 'index.php?option=' . APP_EXTENSION . '&task=fields.listfields&filter_page=' . $row->page;
							?>
							<tr>
								<td align = "center"><?php echo JHtml::_('grid.id', $i, $row->id); ?></td>
								<td align = "center">
									<input type = "text" size = "5" name = "order_<?php echo $row->id; ?>" value = "<?php echo $row->ordering; ?>"
									       class = "text_area input-mini" style = "text-align: center" />
								</td>
								<td align = "center"><a href = "<?php echo $link_page; ?>"><?php echo $row->page; ?></a></td>
								<td><a href = "<?php echo $link; ?>"><?php echo htmlspecialchars($row->name, ENT_QUOTES); ?></a></td>
								<td align = "center"><?php echo $row->id; ?></td>
								<td align = "center"><?php echo $row->ftype; ?></td>
								<td align = "center"><?php echo ($row->status) ? JText::_("FACTORY_YES") : JText::_("FACTORY_NO"); ?></td>
								<td align = "center"><?php echo ($row->compulsory) ? JText::_("FACTORY_YES") : JText::_("FACTORY_NO"); ?></td>
								<td align = "center"><?php echo ($row->search) ? JText::_("FACTORY_YES") : JText::_("FACTORY_NO"); ?></td>
								<td align = "center"><?php echo ($row->categoryfilter) ? JText::_("FACTORY_YES") : JText::_("FACTORY_NO"); ?></td>

							</tr>
							<?php
							$k = 1 - $k;
						}
					?>
					</tbody>
					<tfoot>
					<tr>
						<td colspan = "11">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
					</tfoot>
				</table>
			</div>

		</form>
	</div>
</div>
