<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JHTMLCustomFields
	 */
	abstract class JHTMLCustomFields
	{
		/**
		 * FieldType_Params
		 *
		 * @param      $params
		 * @param null $paramvalues
		 *
		 * @return null|string
		 */
		static function FieldType_Params(&$params, $paramvalues = NULL)
		{
			if (!$params || !count($params)) return NULL;
			$html = "
            	<fieldset class=\"adminform\">
            	<legend>" . JText::_('FACTORY_CUSTOM_FIELD_PARAMETERS') . "</legend>
            	<table class=\"paramlist admintable\" border=\"0\">

        ";

			$paramobj = new JRegistry($paramvalues);
			foreach ($params as $param => $paramtype) {
				$func = 'Param_' . $paramtype;
				$html .= self::$func($param, $paramobj->get($param));
			}
			$html .= "</table></fieldset>";

			return $html;
		}

		/**
		 * Param_numeric
		 *
		 * @param $param_name
		 * @param $current_value
		 *
		 * @return string
		 */
		public static function Param_numeric($param_name, $current_value)
		{
			$label = "PARAM_LABEL_" . strtoupper($param_name);

			return '<tr><td class="paramlist_key" width="200" align="right"><label>' . JText::_($label) . '</label></td>' . '<td class="paramlist_value">' . '<input type="text" name="params[' . $param_name . ']" value="' . $current_value . '" size="10" class="inputbox validate-numeric"/>' . '</td></tr>';
		}

		/**
		 * Param_text
		 *
		 * @param $param_name
		 * @param $current_value
		 *
		 * @return string
		 */
		public static function Param_text($param_name, $current_value)
		{
			$label = "PARAM_LABEL_" . strtoupper($param_name);

			return '<tr><td class="paramlist_key" width="200" align="right"><label>' . JText::_($label) . '</label></td>' . '<td class="paramlist_value">' . '<input type="text" name="params[' . $param_name . ']" value="' . $current_value . '" size="10" class="inputbox"/>' . '</td></tr>';
		}

		/**
		 * Param_radio_bool
		 *
		 * @param $param_name
		 * @param $current_value
		 *
		 * @return string
		 */
		public static function Param_radio_bool($param_name, $current_value)
		{
			$label = "PARAM_LABEL_" . strtoupper($param_name);

			return '<tr><td class="paramlist_key" width="200" align="right"><label>' . JText::_($label) . '</label></td>' . '<td class="paramlist_value">' . JHtml::_('select.booleanlist', "params[$param_name]", NULL, $current_value) . '</td></tr>';
		}

		/**
		 * JHTMLCustomFields::EditFieldByName()
		 *  Function to get the HTML code for one specific field
		 *
		 * @param string $db_name Custom field DBName
		 * @param class  $row     Row with values
		 *
		 * @return string Html to Edit a custom field
		 */

		public static function EditFieldByName($db_name, $row)
		{
			$field = CustomFieldsFactory::getFieldObject($db_name);
			if (!$field) return "";

			$field_object = JTable::getInstance('FieldsTable', 'JTheFactory');
			$field_object->bind($field);

			return self::EditField($field_object, $row);
		}

		/**
		 * EditField
		 *
		 * @param $field
		 * @param $row
		 *
		 * @return mixed
		 */
		public static function EditField(&$field, $row)
		{
			$field_type  = CustomFieldsFactory::getFieldType($field->ftype);
			$field->html = $field_type->getFieldHTML($field, $row->{$field->db_name});

			return $field->html;

		}

		/**
		 * SearchField
		 *
		 * @param      $field
		 * @param null $row
		 *
		 * @return mixed
		 */
		public static function SearchField(&$field, $row = NULL)
		{
			$field_type  = CustomFieldsFactory::getFieldType($field->ftype);
			$field->html = $field_type->getSearchHTML($field, $row ? $row->{$field->db_name} : NULL);

			return $field->html;
		}

		/**
		 * DisplayField
		 *
		 * @param $field
		 * @param $row
		 *
		 * @return string
		 */
		public static function DisplayField(&$field, $row)
		{
			return '<label class="custom_field">' . JText::_($field->name) . '</label> ' . $row->{$field->db_name};
		}

		/**
		 * DisplaySearchHtml
		 *
		 * @param        $fieldlist
		 * @param string $style
		 *
		 * @return mixed
		 */
		public static function DisplaySearchHtml($fieldlist, $style = 'table')
		{
			$flist        = array();
			$field_object = JTable::getInstance('FieldsTable', 'JTheFactory');

			foreach ($fieldlist as $field) {
				$field_object->bind($field);
				$f          = new stdClass();
				$f->field   = clone $field;
				$f->value   = NULL;
				$field_type = CustomFieldsFactory::getFieldType($field->ftype);
				$f->html    = $field_type->getSearchHTML($field_object);
				$flist[]    = $f;
			}
			$func = 'DisplayFieldsHtml_' . ucfirst($style);
			$html = self::$func($flist);

			return $html;
		}

		/**
		 * DisplayFieldsHtml
		 *
		 * @param        $row
		 * @param        $fieldlist
		 * @param string $style
		 *
		 * @return null
		 */
		public static function DisplayFieldsHtml(&$row, $fieldlist, $style = 'table')
		{
			if (!count($fieldlist)) return NULL;
			$page = $fieldlist[0]->page;
			$cfg  = CustomFieldsFactory::getConfig();

			$category_filter = array();
			if ($cfg['has_category'][$page]) {
				$db = JFactory::getDbo();
				$db->setQuery("SELECT fid FROM #__" . APP_PREFIX . "_fields_categories WHERE cid = '" . $row->cat . "'");
				$category_filter = $db->loadColumn();
			}
			$flist        = array();
			$field_object = JTable::getInstance('FieldsTable', 'JTheFactory');

			foreach ($fieldlist as $field) {

				if ($field->categoryfilter && !in_array($field->id, $category_filter)) continue;

				$field_type = CustomFieldsFactory::getFieldType($field->ftype);

				$field_object->bind($field);

				$f        = new stdClass();
				$f->field = clone $field;
				$f->value = $row->{$field->db_name};
				$f->html  = $field_type->getFieldHTML($field_object, $row->{$field->db_name});
				$flist[]  = $f;
			}

			$func = 'DisplayFieldsHtml_' . ucfirst($style);
			$html = self::$func($flist);

			return $html;
		}

		/**
		 * DisplayFieldsHtml_Table
		 *
		 * @param $flist
		 *
		 * @return null|string
		 */
		public static function DisplayFieldsHtml_Table($flist)
		{
			if (!count($flist)) return NULL;
			$MyApp = JTheFactoryApplication::getInstance();
			$icon  = $MyApp->assets_url . 'images/tooltip.png';
			$html  = "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
			foreach ($flist as $f) {
				$tooltip = "";
				if ($f->field->help) $tooltip = JHtml::_('tooltip', $f->field->help, NULL, $icon);
				$html .= "
                <tr>
                    <td><label class='custom_field'>" . JText::_($f->field->name) . "</label> $tooltip</td>
                    <td>" . $f->html . "</td>
                </tr>
            ";
			}
			$html .= "</table>";

			return $html;
		}

		/**
		 * DisplayFieldsHtml_Divs
		 *
		 * @param $flist
		 *
		 * @return null|string
		 */
		public static function DisplayFieldsHtml_Divs($flist)
		{
			if (!count($flist)) return NULL;

			$MyApp = JTheFactoryApplication::getInstance();
			$icon  = $MyApp->assets_url . 'images/tooltip.png';
			$html  = '';

			foreach ($flist as $f) {
				$tooltip = "";
				if ($f->field->help) $tooltip = JHtml::_('tooltip', $f->field->help, NULL, $icon);

				$html .= '<div class="auction_search_field">
                <label class="auction_lables">
                    <label class="custom_field">' . JText::_($f->field->name) . ' </label>' . '</label>
                ' . $f->html . '&nbsp;' . $tooltip . '</div>';
			}

			return $html;
		}

	} // End Class
