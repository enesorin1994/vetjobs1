<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 *
	 * Table to extend for other tables that accept custom fields
	 *  provides html display of fields for a parent item
	 **/
	class FactoryFieldsTbl extends JTable
	{

		/**
		 * _category_field
		 *
		 * @var null
		 */
		private $_category_field = NULL;
		/**
		 * _system_fields
		 *
		 * @var null
		 */
		private $_system_fields = NULL;
		/**
		 * _validation_errors
		 *
		 * @var array
		 */
		private $_validation_errors = array();
		/**
		 * _custom_fields
		 *
		 * @var null
		 */
		private $_custom_fields = NULL;

		/**
		 *  Regular loading of a row with custom fields
		 *
		 * @param string          $table
		 * @param string          $key
		 * @param JDatabaseDriver $db
		 * @param null            $system_fields
		 * @param null            $category_field
		 */
		public function __construct($table, $key, $db, $system_fields = NULL, $category_field = NULL)
		{

			$this->_category_field = $category_field;
			$this->_system_fields  = $system_fields;

			parent::__construct($table, $key, $db);

			$this->_custom_fields = CustomFieldsFactory::getTableFields($table);

			for ($i = 0; $i < count($this->_custom_fields); $i++) {
				$fieldname        = $this->_custom_fields[$i]->db_name;
				$this->$fieldname = NULL;
			}

		}

		/**
		 * getCategoryField
		 *    returns the category field
		 *
		 * @return string
		 */
		public function getCategoryField()
		{
			return $this->_category_field;
		}

		/**
		 * getValidationErrors
		 *     returns the errors Array
		 *
		 * @return array
		 */
		public function getValidationErrors()
		{
			return $this->_validation_errors;

		}

		/**
		 * getCustomFields
		 *     returns the custom fields array
		 *
		 * @return array
		 */
		public function getCustomFields()
		{
			return $this->_custom_fields;
		}

		/**
		 * check
		 *
		 * @return bool
		 */
		public function check()
		{
			$result                   = TRUE;
			$this->_validation_errors = array();
			$allowed_fields           = array();

			if ($this->_category_field) {
				$db       = $this->getDbo();
				$category = $this->{$this->_category_field};
				//Table has a category field, and we get all fields that are assigned to that category
				$db->setQuery("SELECT fid FROM #__" . APP_PREFIX . "_fields_categories WHERE cid = '{$category}'");
				$allowed_fields = $db->loadColumn();
			}

			foreach ($this->_custom_fields as $field) {
				if ($this->_category_field && $field->categoryfilter && !in_array($field->id, $allowed_fields)) {
					continue;
				}
				$fieldvalue = $this->{$field->db_name};
				if ($field->compulsory && ($fieldvalue === NULL || $fieldvalue == '')) {
					//attention 0 can be a non empty value !
					$this->_validation_errors[] = JText::_("FACTORY_FIELD") . " " . $field->name . " " . JText::_("FACTORY_IS_REQUIRED");

				}
				if (!$field->validate_type) continue;

				$validator = CustomFieldsFactory::getFieldValidator($field->validate_type);

				if (!$validator->validateValue($fieldvalue, $field->params)) {
					$result                   = FALSE;
					$this->_validation_errors = array_merge($this->_validation_errors, $validator->errormessages);
				}
			}

			$result = $result && parent::check();

			return $result;
		}

		/**
		 * bind
		 *
		 * @param mixed $src
		 * @param array $ignore
		 *
		 * @return bool
		 */
		public function bind($src, $ignore = array())
		{
			$res = parent::bind($src, $ignore);
			if ($src == $_GET || $src == $_POST || $src == $_REQUEST) {
				if (!is_array($ignore)) {
					$ignore = explode(' ', $ignore);
				}
				$fieldObj = JTable::getInstance('FieldsTable', 'JTheFactory');

				if (is_array($this->_custom_fields)) foreach ($this->_custom_fields as $field) if (!in_array($field->db_name, $ignore)) {
					$fieldObj->bind($field);
					$fieldPlugin             = CustomFieldsFactory::getFieldType($field->ftype);
					$val                     = $fieldPlugin->getValue($fieldObj, $src);
					$this->{$field->db_name} = $val;
				}
			}

			return $res;
		}

		/**
		 * store
		 *
		 * @param bool $quicksave
		 *
		 * @return bool
		 */
		public function store($quicksave = FALSE)
		{
			$allowed_fields = array();

			if (!$quicksave) {
				if ($this->_category_field) {
					$db       = $this->getDbo();
					$category = $this->{$this->_category_field};
					//Table has a category field, and we get all fields that are assigned to that category
					$db->setQuery("SELECT fid FROM #__" . APP_PREFIX . "_fields_categories WHERE cid = '{$category}'");
					$allowed_fields = $db->loadColumn();
				}

				foreach ($this->_custom_fields as $field) {
					if ($this->_category_field && $field->categoryfilter && !in_array($field->id, $allowed_fields)) {
						//Field is not allowed in this category
						$this->{$field->db_name} = NULL;
						continue;
					}
				}
			}
			$s = parent::store();

			return $s;
		}


	} // End Class
