<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryFieldsToolbar
	{
		/**
		 * display
		 *
		 * @static
		 *
		 * @param null $task
		 */
		public static function display($task = NULL)
		{
			$appname = JTheFactoryApplication::getInstance()->getApplicationName();
			JToolBarHelper::title(JText::_('FACTORY_CUSTOM_FIELDS') . ' - ' . JText::_('FACTORY_CUSTOM_FIELDS_FIELDS_LIST') . ' - ' . $appname);
			switch ($task) {
				default:
				case 'listfields':
					JToolBarHelper::custom('fields.edit', 'new.png', 'new_f2.png', JText::_('FACTORY_NEW_FIELD'), FALSE);
					JToolBarHelper::custom('fields.edit', 'edit.png', 'edit_f2.png', JText::_('FACTORY_EDIT_FIELD'), TRUE);
					JToolBarHelper::deleteList("", "fields.deletefield", JText::_('FACTORY_DELETE_FIELDS'));
					JToolBarHelper::custom('positions.listfields', 'html', 'html_f2', JText::_('FACTORY_PUBLISH_ON_TEMPLATE'), FALSE);
					JToolBarHelper::custom("settingsmanager", 'back', 'back', JText::_('FACTORY_BACK'), FALSE);

					JTheFactoryFieldsSubmenu::subMenuListFields();
					break;
				case 'edit':
					JToolBarHelper::save("fields.savefield");
					JToolBarHelper::save2new("fields.savefield2new");
					JToolBarHelper::apply("fields.applyfield");
					JToolBarHelper::custom('fields.listfields', 'back', 'back', JText::_('FACTORY_BACK'), FALSE);
					JTheFactoryFieldsSubmenu::subMenuEditField();
					break;
			}

		}
	}
