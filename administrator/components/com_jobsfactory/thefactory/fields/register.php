<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryFieldsRegister
	{
		/**
		 * registerModule
		 *
		 * @static
		 *
		 * @param null $app
		 */
		public static function registerModule($app = NULL)
		{
			if (!$app) $app = JTheFactoryApplication::getInstance();
			if ($app->getIniValue('use_custom_fields')) {
				JLoader::register('FactoryFieldsTbl', $app->app_path_admin . 'fields/tables/table.class.php');
				JLoader::register('CustomFieldsFactory', $app->app_path_admin . 'fields/helper/factory.class.php');
				JLoader::register('FactoryFieldTypes', $app->app_path_admin . 'fields/plugins/field_types.php');
				JLoader::register('FactoryFieldValidator', $app->app_path_admin . 'fields/plugins/field_validator.php');
				JLoader::register('JTheFactoryListModel', $app->app_path_admin . 'fields/models/listmodel.php');
				JHtml::addIncludePath($app->app_path_admin . 'fields/html');
				JTable::addIncludePath($app->app_path_admin . 'fields/tables');
				JTheFactoryHelper::loadModuleLanguage('fields');
			}
		}
	}
