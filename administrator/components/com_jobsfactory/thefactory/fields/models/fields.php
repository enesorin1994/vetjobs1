<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.application.component.model');

	/**
	 * Class JTheFactoryModelFields
	 */
	class JTheFactoryModelFields extends JModelLegacy
	{
		/**
		 * context
		 *
		 * @var string
		 */
		var $context = 'fields';
		/**
		 * limit
		 *
		 * @var null
		 */
		var $limit = NULL;
		/**
		 * limitstart
		 *
		 * @var null
		 */
		var $limitstart = NULL;
		/**
		 * ordering
		 *
		 * @var null
		 */
		var $ordering = NULL;
		/**
		 * ordering_dir
		 *
		 * @var string
		 */
		var $ordering_dir = 'ASC';
		/**
		 * _total
		 *
		 * @var null
		 */
		var $_total = NULL;
		/**
		 * _pagination
		 *
		 * @var null
		 */
		var $_pagination = NULL;
		/**
		 * _tablename
		 *
		 * @var null|string
		 */
		var $_tablename = NULL;
		/**
		 * _tablename_category
		 *
		 * @var null|string
		 */
		var $_tablename_category = NULL;

		/**
		 *
		 */
		public function __construct()
		{
			$this->context             = APP_EXTENSION . "_fields.";
			$this->_tablename          = '#__' . APP_PREFIX . '_fields';
			$this->_tablename_category = '#__' . APP_PREFIX . '_fields_categories';

			parent::__construct();
		}

		/**
		 * getLimitFromRequest
		 *
		 */
		public function getLimitFromRequest()
		{
			$app              = JFactory::getApplication();
			$this->limit      = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->get('list_limit'), 'int');
			$this->limitstart = $app->getUserStateFromRequest($this->context . 'limitstart', 'limitstart', 0, 'int');

			// In case limit has been changed, adjust limitstart accordingly
			$this->limitstart = ($this->limit != 0 ? (floor($this->limitstart / $this->limit) * $this->limit) : 0);
		}

		/**
		 * getOrdering
		 *
		 */
		public function getOrdering()
		{
			$app                = JFactory::getApplication();
			$this->ordering     = $app->getUserStateFromRequest($this->context . 'ordering', 'ordering', 'ordering', 'string');
			$this->ordering_dir = $app->getUserStateFromRequest($this->context . 'ordering_dir', 'ordering_dir', 'ASC', 'string');
		}

		/**
		 * getFields
		 *
		 * @param bool $enabled_filter
		 * @param null $filter_page
		 *
		 * @return mixed
		 */
		public function getFields($enabled_filter = FALSE, $filter_page = NULL)
		{
			$db = $this->getDbo();
			$this->getOrdering();
			$this->getLimitFromRequest();
			$where = 'WHERE 1=1';
			$ord   = ', `ordering` ASC';
			if ($filter_page) $where .= " AND `page`='{$filter_page}'";
			if ($enabled_filter) $where .= " AND `status`='1'";

			$db->setQuery("SELECT COUNT(*) FROM `" . $this->_tablename . "` {$where}");
			$this->_total = $db->loadResult();

			if (isset($this->ordering) && $this->ordering != 0) {
				$ordDir = isset($this->ordering_dir) ? $this->ordering_dir : 'ASC';
				$ord .= ', ' . $this->ordering . ' ' . $ordDir;
			}


			$db->setQuery("SELECT * FROM `" . $this->_tablename . "` {$where} ORDER BY page ASC {$ord}", $this->limitstart, $this->limit);

			return $db->loadObjectList();
		}

		/**
		 * getPagination
		 *
		 * @return JPagination|null
		 */
		public function getPagination()
		{
			if ($this->_pagination) return $this->_pagination;
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->_total, $this->limitstart, $this->limit);

			return $this->_pagination;
		}

		/**
		 * getAssignedCats
		 *
		 * @param $field
		 *
		 * @return mixed
		 */
		public function getAssignedCats(&$field)
		{
			$db = $this->getDbo();
			$db->setQuery("SELECT `cid` FROM `" . $this->_tablename_category . "` WHERE `fid`='{$field->id}'");

			return $db->loadColumn();
		}

		/**
		 * setAssignedCats
		 *
		 * @param $field
		 * @param $catids
		 */
		public function setAssignedCats(&$field, $catids)
		{
			$db = $this->getDbo();
			// delete category assignments
			$sql = "DELETE FROM #__" . APP_PREFIX . "_fields_categories WHERE `fid` = '{$field->id}'";
			$db->setQuery($sql);
			$db->execute();

			if (!count($catids)) return; //no categories assigned
			// add category assignemtns
			$inserts = array();
			foreach ($catids as $catid) {
				$inserts[] = " ('{$field->id}', '{$catid}')";
			}
			if (count($inserts)) {
				$sql = "INSERT INTO #__" . APP_PREFIX . "_fields_categories (`fid`,`cid`) VALUES " . implode(",", $inserts);
				$db->setQuery($sql);
				$db->execute();
			}
		}

		/**
		 * hasAssignedCat
		 *
		 * @param $field
		 * @param $catid
		 *
		 * @return bool
		 */
		public function hasAssignedCat(&$field, $catid)
		{
			$db = $this->getDbo();
			$db->setQuery("SELECT count(*) FROM `" . $this->_tablename_category . "` where `fid`='{$field->id}' and `cid`='$catid'");

			return $db->loadResult() > 0;
		}

	}
