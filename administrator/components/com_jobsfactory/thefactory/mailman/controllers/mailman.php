<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: mailman
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryMailmanController
	 */
	class JTheFactoryMailmanController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		public $name = 'Mailman';
		/**
		 * _name
		 *
		 * @var string
		 */
		public $_name = 'Mailman';

		/**
		 * Mails
		 *
		 */
		public function Mails()
		{
			$current_mail_type = JFactory::getApplication()->input->getString('mail_type', '');

			$model = JModelLegacy::getInstance('Mailman', 'JTheFactoryModel');
			$rows  = $model->getMailList();

			$current_mail = $rows[0];
			$options      = array();
			foreach ($rows as $row) {
				$options[] = JHtml::_('select.option', $row->mail_type, JText::_(APP_PREFIX . '_mail_' . $row->mail_type));
				if ($row->mail_type == $current_mail_type) $current_mail = $row;
			}
			$select_html = JHtml::_('select.genericlist', $options, 'mail_type', "onchange='this.form.submit();'", 'value', 'text', $current_mail_type);

			$title = JText::_(APP_PREFIX . '_mail_' . $current_mail->mail_type);
			$help  = JText::_(APP_PREFIX . '_mail_' . $current_mail->mail_type . '_help');
			jimport('joomla.html.editor'); // TODO Factory: To be removed at some time because this is useful only for J2.5 but not for J3.0
			$editor = JEditor::getInstance(JFactory::getConfig()->get('editor'));

			$view                  = $this->getView('mails');
			$view->mail_type       = $current_mail_type;
			$view->rows            = $rows;
			$view->current_mail    = $current_mail;
			$view->title           = $title;
			$view->help            = $help;
			$view->mailtype_select = $select_html;
			$view->editor          = $editor;
			$view->shortcuts       = $model->getShortcuts();

			$view->display('mail');

			return;

		}

		/**
		 * Save
		 *
		 */
		public function Save()
		{
			$input    = JFactory::getApplication()->input;
			$mailtype = $input->getString('mail_type', '');
			$subject  = $input->getString('subject', '');
			$content  = $input->getHtml('mailbody', '');
			$enabled  = $input->getInt('enabled', 0);

			$row = JTable::getInstance('MailmanTable', 'JTheFactory');

			if (!$enabled) {
				$row->load($mailtype);
				$row->enabled = 0;

			} else {
				$row->bind(JTheFactoryHelper::getRequestArray('post', 'html'));
				$f            = JFilterInput::getInstance(array(), array(), 1, 1, 1);
				$row->content = $f->clean($_POST['mailbody'], 'html');
			}
			$row->store();

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=mailman.mails&mail_type=' . $mailtype, JText::_("FACTORY_MAILTEXT_SAVED"));

			return;
		}
	}

