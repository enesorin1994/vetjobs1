
<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: mailman
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	JHtml::_('behavior.tooltip');
	JHtml::_('formbehavior.chosen', 'select');
	$doc = JFactory::getDocument();
	$doc->addScript(JURI::root() . 'administrator/components/' . APP_EXTENSION . '/thefactory/mailman/js/mailman.js');
	$doc->addStyleDeclaration("
		.legend-table{
			margin-top:20px;
			font-size: 11px;
		}
		.legend-title{
			background-color: lightgrey;
			border-bottom:1px solid #999999;
		}
		.legend-item-row:nth-child(even){
			background-color:#f8f8f8;
		}

		.legend-item-row:hover{
			background-color: #dce7f2;
		}
	");
?>

<fieldset class = "adminform">
	<form action = "index.php" method = "post" name = "adminForm" id = "adminForm">
		<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
		<input type = "hidden" name = "task" value = "mailman.mails" />
		<?php echo $this->mailtype_select; ?>
		<input type = 'checkbox' name = 'enabled' value = '1'
		       onclick = 'toggleMail();' <?php echo ($this->current_mail->enabled) ? "checked" : ""; ?> />
		<?php echo ($this->current_mail->enabled) ? JText::_("FACTORY_ENABLED") : ("<div><h2>" . JText::_("FACTORY_CURRENT_MAILTYPE_NOT_ENABLED") . "</h2></div>"); ?>

		<table width = "100%">
			<tr>
				<td width = "400px" align = "left" valign = "top">
					<table class = "legend-table">
						<tr>
							<td align = "center" class = "legend-title"><b><?php echo JText::_("FACTORY_SHORTCUTS_LEGEND"); ?></b></td>
						</tr>
						<tr>
							<td height = "10%"></td>
						</tr>
						<tr>
							<td valign = "top">
								<?php echo $this->display('legend'); ?>
							</td>
						</tr>
					</table>
				</td>
				<td align = "left" valign = "top">
					<table>
						<tr>
							<th class = "title">
								<?php echo $this->title; ?>&nbsp;
								<?php echo JHTML::_('tooltip', $this->help); ?>
							</th>
						</tr>
						<tr>
							<td>
								<input name = "subject" value = "<?php echo $this->current_mail->subject; ?>"
								       size = "80"
								       style = "width: 99%;"
									<?php echo $this->current_mail->enabled ? "" : "disabled"; ?>>
							</td>
						</tr>
						<tr id = "mailbody-tr"
						    style = "display: <?php echo ($this->current_mail->enabled) ? "table-row" : "none"; ?>;">
							<td valign = "top">
								<?php echo $this->editor->display('mailbody', $this->current_mail->content, '100%', '300', '200', '200', array('article', 'image', 'pagebreak', 'readmore')); ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

	</form>
</fieldset>
