<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: mailman
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryViewMailmanMails
	 */
	class JTheFactoryViewMailmanMails extends JTheFactoryView
	{
		/**
		 * display
		 *
		 * @param null $tpl
		 */
		public function display($tpl = NULL)
		{
			parent::display($tpl);
		}

	} // End Class
