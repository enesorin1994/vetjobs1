<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: mailman
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.application.component.model');

	/**
	 * Class JTheFactoryModelMailman
	 */
	class JTheFactoryModelMailman extends JModelLegacy
	{
		/**
		 * table_name
		 *
		 * @var null|string
		 */
		public $table_name = NULL;

		/**
		 *
		 */
		public function __construct()
		{
			$this->table_name = '#__' . APP_PREFIX . '_mails';
			parent::__construct();
		}

		/**
		 * getMailList
		 *
		 * @return mixed
		 */
		public function getMailList()
		{
			$db = $this->getDbo();
			// Check for existence of `group` and `ordering` columns
			// Because this columns are added later (in the same time)
			//
			$db->setQuery("SHOW COLUMNS FROM `{$this->table_name}`");
			$emailTableColumns = $db->loadObjectList('Field');
			$orderBy           = '';
			if (array_key_exists('group', $emailTableColumns)) {
				$orderBy = ' ORDER BY `group`,`ordering`';
			}

			// Get emails list
			$db->setQuery("SELECT * FROM `{$this->table_name}` {$orderBy}");

			return $db->loadObjectList();

		}

		/**
		 * getShortcuts
		 *
		 * @return array
		 */
		public function getShortcuts()
		{
			$myApp     = JTheFactoryApplication::getInstance();
			$short     = explode(',', $myApp->getIniValue('shortcuts', 'mail-settings'));
			$short_d   = explode(',', $myApp->getIniValue('shortcuts_description', 'mail-settings'));
			$shortcuts = array();
			for ($i = 0; $i < count($short); $i++) {
				$shortcuts[$short[$i]] = $short_d[$i];
			}

			return $shortcuts;
		}

	}
