<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: mailman
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryMailmanTable
	 */
	class JTheFactoryMailmanTable extends JTable
	{
		/**
		 * id
		 *
		 * @var
		 */
		public $id;
		/**
		 * mail_type
		 *
		 * @var
		 */
		public $mail_type;
		/**
		 * content
		 *
		 * @var
		 */
		public $content;
		/**
		 * subject
		 *
		 * @var
		 */
		public $subject;
		/**
		 * enabled
		 *
		 * @var
		 */
		public $enabled;

		/**
		 * @param JDatabaseDriver $db
		 */
		public function __construct($db)
		{
			parent::__construct('#__' . APP_PREFIX . '_mails', 'mail_type', $db);
		}

		/**
		 * store
		 *
		 * @param bool $updateNulls
		 *
		 * @return bool
		 */
		public function store($updateNulls = FALSE)
		{
			$k  = $this->getKeyName();
			$db = $this->getDbo();
			if ($this->$k) {
				$ret = $db->updateObject($this->getTableName(), $this, $k, $updateNulls);
			} else {
				$ret = $db->insertObject($this->getTableName(), $this, $k);
			}

			if (!$ret) {
				$this->setError(strtolower(get_class($this)) . "::store failed <br />" . $db->getErrorMsg());

				return FALSE;
			} else {
				return TRUE;
			}
		}

	} // End Class
