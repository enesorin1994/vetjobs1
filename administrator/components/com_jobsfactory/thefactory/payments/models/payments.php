<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.application.component.model');

	/**
	 * Class JTheFactoryModelPayments
	 */
	class JTheFactoryModelPayments extends JModelList
	{
		/**
		 * context
		 *
		 * @var string
		 */
		var $context = 'payments';
		/**
		 * tablename
		 *
		 * @var null|string
		 */
		var $tablename = NULL;
		/**
		 * pagination
		 *
		 * @var null
		 */
		var $pagination = NULL;

		/**
		 * JTheFactoryModelPayments constructor
		 */
		public function __construct()
		{
			$this->context   = APP_EXTENSION . "_payments.";
			$this->tablename = '#__' . APP_PREFIX . '_payment_log';
			JTheFactoryHelper::tableIncludePath('payments');

			$config['filter_fields'] = array(
				'p.id',
				'p.date',
				'p.status',
				'u.username',
				'p.amount',
				'p.orderid',
				'p.refnumber',
				'p.payment_method'
			);
			parent::__construct($config);
		}

		protected function populateState($ordering = NULL, $direction = NULL)
		{
			parent::populateState('p.date', 'DESC');
		}

		/**
		 * getPaymentsList
		 *
		 * @param string $username_filter
		 *
		 * @return mixed
		 */
		public function getPaymentsList($username_filter = '')
		{

			$db  = $this->getDbo();
			$app = JFactory::getApplication();

			$limit      = (int)$app->getUserStateFromRequest($this->context . "limit", 'limit', $app->get('list_limit'));
			$limitstart = (int)$app->getUserStateFromRequest($this->context . "limitstart", 'limitstart', 0);

			jimport('joomla.html.pagination');
			$this->pagination = new JPagination($this->getTotal($username_filter), $limitstart, $limit);

			$q = $db->getQuery(TRUE);
			$q->select('p.*, u.username')
				->from($this->tablename . ' p')
				->leftJoin('#__users u ON u.id=p.userid')
				->order($db->escape($this->getState('list.ordering', 'p.date')) . ' ' .
					$db->escape($this->getState('list.direction', 'DESC')));

			if (!empty($username_filter)) {
				$q->where('u.username LIKE \'%' . $db->escape($username_filter) . '%\'');
			}

			$db->setQuery($q, $limitstart, $limit);

			return $db->loadObjectList();
		}

		/**
		 * getTotal
		 *
		 * @param string $username_filter
		 *
		 * @return mixed
		 */
		public function getTotal($username_filter = '')
		{
			$db = $this->getDbo();
			$q  = $db->getQuery(TRUE);
			$q->select('COUNT(1)')->from($this->tablename . ' p')->leftJoin('#__users u ON u.id=p.userid');
			if (!empty($username_filter)) {
				$q->where('u.username LIKE \'%' . $db->escape($username_filter) . '%\'');
			}
			$db->setQuery($q);

			return $db->loadResult();
		}

	} // End Class
