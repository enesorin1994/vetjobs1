<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.application.component.model');
	use Joomla\Registry\Registry as JRegistry;

	/**
	 * Class JTheFactoryModelGateways
	 */
	class JTheFactoryModelGateways extends JModelLegacy
	{
		/**
		 * context
		 *
		 * @var string
		 */
		var $context = 'gateways';
		/**
		 * tablename
		 *
		 * @var null|string
		 */
		var $tablename = NULL;

		/**
		 * JTheFactoryModelGateways constructor
		 */
		public function __construct()
		{
			$this->context   = APP_EXTENSION . "_gateways.";
			$this->tablename = '#__' . APP_PREFIX . '_paysystems';

			parent::__construct();
		}

		/**
		 * getGatewayList
		 *
		 * @param bool $enabled
		 *
		 * @return mixed
		 */
		public function getGatewayList($enabled = TRUE)
		{
			$where = ($enabled) ? "WHERE `enabled`=1" : "";
			$db    = $this->getDbo();
			$db->setQuery("SELECT * FROM `{$this->tablename}` {$where} ORDER BY `ordering` ");

			return $db->loadObjectList();
		}

		/**
		 * toggle
		 *
		 * @param $itemname
		 *
		 * @return string
		 */
		public function toggle($itemname)
		{
			$db = $this->getDbo();
			$db->setQuery("SELECT `enabled` FROM `{$this->tablename}` WHERE `id`='{$itemname}' ");
			$enabled = $db->loadResult() ? "0" : "1";
			$db->setQuery("UPDATE `{$this->tablename}` SET `enabled`='{$enabled}' WHERE `id`='{$itemname}' ");
			$db->execute();

			return $enabled ? (JText::_("FACTORY_GATEWAY_ENABLED")) : (JText::_("FACTORY_GATEWAY_DISABLED"));
		}

		/**
		 * setdefault
		 *
		 * @param $itemname
		 */
		public function setdefault($itemname)
		{
			$db = $this->getDbo();
			$db->setQuery("UPDATE `{$this->tablename}` SET `isdefault`=0 ");
			$db->execute();

			$db->setQuery("UPDATE `{$this->tablename}` SET `isdefault`=1 WHERE `id`='{$itemname}' ");
			$db->execute();
		}

		/**
		 * saveGatewayParams
		 *
		 * @param $gateway
		 * @param $params
		 */
		public function saveGatewayParams($gateway, $params)
		{
			$db = $this->getDbo();
			$db->setQuery('UPDATE `' . $this->tablename . '` SET `params`=' . $db->quote($params) . " WHERE `classname`=" . $db->quote($gateway));
			$db->execute();

		}

		/**
		 * loadGatewayParams
		 *
		 * @param $gateway
		 *
		 * @return JRegistry
		 */
		public function loadGatewayParams($gateway)
		{
			$db = $this->getDbo();
			$db->setQuery('SELECT `params` FROM `' . $this->tablename . '` WHERE `classname`=' . $db->quote($gateway));
			$params = $db->loadResult();
			$config = new JRegistry($params);

			return $config;
		}

		/**
		 * getGatewayObject
		 *
		 * @param $name
		 *
		 * @return null
		 */
		public function getGatewayObject($name)
		{
			$MyApp = JTheFactoryApplication::getInstance();
			$path  = $MyApp->app_path_admin . 'payments/plugins/gateways/' . strtolower($name) . '/controller.php';
			if (!file_exists($path)) return NULL;
			require_once($path);
			if (!class_exists($name)) return NULL;
			$gateway = new $name();

			return $gateway;
		}
	}
