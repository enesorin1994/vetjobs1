<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/

	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');
	jimport('joomla.application.component.model');

	/**
	 *      Status - P=pending, C=completed, X=cancelled, R=refunded
	 */
	class JTheFactoryModelOrders extends JModelLegacy
	{
		/**
		 * @var string
		 */
		var $context = 'orders';
		/**
		 * @var null|string
		 */
		var $tablename = NULL;
		/**
		 * @var null|string
		 */
		var $table_items = NULL;
		/**
		 * @var null
		 */
		var $pagination = NULL;

		/**
		 * JTheFactoryModelOrders constructor
		 */
		public function __construct()
		{
			$this->context     = APP_EXTENSION . "_orders.";
			$this->tablename   = '#__' . APP_PREFIX . '_payment_orders';
			$this->table_items = '#__' . APP_PREFIX . '_payment_orderitems';
			JTheFactoryHelper::tableIncludePath('payments');
			parent::__construct();
		}

		/**
		 * createNewOrder
		 *
		 * @param        $items
		 * @param null   $total
		 * @param null   $currency
		 * @param null   $userid
		 * @param string $status
		 *
		 * @return mixed
		 */
		public function createNewOrder($items, $total = NULL, $currency = NULL, $userid = NULL, $status = 'P')
		{
			if (!$userid) {
				$my     = JFactory::getUser();
				$userid = $my->id;
			}
			if (!is_array($items)) $items = array($items);

			$order                 = JTable::getInstance('OrdersTable', 'JTheFactory');
			$orderitem             = JTable::getInstance('OrderItemsTable', 'JTheFactory');
			$date                  = JFactory::getDate();
			$order->id             = NULL;
			$order->orderdate      = $date->toSQL();
			$order->userid         = $userid;
			$order->order_total    = $total;
			$order->order_currency = $currency;
			$order->status         = $status;

			$order->store();

			$t = 0;
			$c = NULL;
			foreach ($items as $item) {
				$orderitem->id          = NULL;
				$orderitem->orderid     = $order->id;
				$orderitem->itemname    = $item->itemname;
				$orderitem->itemdetails = $item->itemdetails;
				$orderitem->iteminfo    = $item->iteminfo;
				$orderitem->price       = $item->price;
				$orderitem->currency    = $item->currency;
				$orderitem->params      = $item->params;
				$orderitem->quantity    = $item->quantity;

				$orderitem->store();

				$t += $item->price;
				$c = $item->currency;
			}
			if (!$total) {
				$order->order_total    = $t;
				$order->order_currency = $c;

				$order->store();
			}

			return $order;
		}

		/**
		 * getOrderItems
		 *
		 * @param      $orderid
		 * @param null $itemname
		 *
		 * @return mixed
		 */
		public function getOrderItems($orderid, $itemname = NULL)
		{
			$db = $this->getDbo();
			$db->setQuery("SELECT * FROM `{$this->table_items}` WHERE `orderid`='$orderid'" . ($itemname ? " AND `itemname`='$itemname'" : ""));

			return $db->loadObjectList();
		}

		/**
		 * getOrdersList
		 *
		 * @param null $username
		 *
		 * @internal param null $userid
		 *
		 * @return mixed
		 */
		public function getOrdersList($username = NULL)
		{
			jimport('joomla.html.pagination');
			$db         = $this->getDbo();
			$sqlFilters = $this->setSqlConditions($username);

			$this->pagination = new JPagination($this->getTotal($username), $sqlFilters['limitstart'], $sqlFilters['limit']);

			$db->setQuery("SELECT `o`.*, `u`.`username`
						 FROM `{$this->tablename}` AS `o`
						 LEFT JOIN `#__users` AS `u` ON `u`.`id` = `o`.`userid`
						 {$sqlFilters['ord_items_join']}
						 {$sqlFilters['where']}
						 ORDER BY `id` DESC", $sqlFilters['limitstart'], $sqlFilters['limit']);

			return $db->loadObjectList();
		}

		/**
		 * getTotal
		 *
		 * @param null $username
		 *
		 * @internal param null $userid
		 *
		 * @return mixed
		 */
		public function getTotal($username = NULL)
		{
			$db         = $this->getDbo();
			$sqlFilters = $this->setSqlConditions($username);

			$db->setQuery("SELECT COUNT(*)
						 FROM `{$this->tablename}` AS `o`
						 LEFT JOIN `#__users` AS `u` ON `u`.`id` = `o`.`userid`
						 {$sqlFilters['ord_items_join']}
						 {$sqlFilters['where']}
				");


			return $db->loadResult();
		}

		/**
		 * Prepare MySql conditions and joins
		 * for getOrderList and for getTotal methods
		 * Note: created for preventing duplicate code
		 *
		 * @param string $username
		 *
		 * @return array    $data   Holder for all prepared MySql conditions
		 */
		private function setSqlConditions($username)
		{
			$db  = $this->getDbo();
			$app = JFactory::getApplication();
			// Output holder
			$data = array();

			$data['limit']      = $limit = $app->getUserStateFromRequest($this->context . "limit", 'limit', $app->get('list_limit'));
			$data['limitstart'] = $limitstart = $app->getUserStateFromRequest($this->context . "limitstart", 'limitstart', 0);

			$data['status'] = $status = $app->getUserStateFromRequest($this->context . "status", 'status', 0);
			$data['type']   = $type = $app->getUserStateFromRequest($this->context . "type", 'type', 0);

			// Apply filters
			$where          = 'WHERE 1';
			$ord_items_join = '';

			if ($username) $where .= " AND `u`.`username` LIKE '%" . $db->escape($username) . "%' ";
			if ($status) $where .= " AND `o`.`status`= '" . $db->escape($status) . "'";

			if ($type && 'to_me' == $type) {
				$ord_items_join = "LEFT JOIN `{$this->table_items}` AS `ord_items` ON `ord_items`.`orderid` = `o`.`id`";
				$where .= " AND `ord_items`.`itemname`= 'withdraw'";

			} elseif ($type && 'by_me' == $type) {
				$ord_items_join = "LEFT JOIN `{$this->table_items}` AS `ord_items` ON `ord_items`.`orderid` = `o`.`id`";
				$where .= " AND `ord_items`.`itemname`!= 'withdraw'";
			}

			$data['where']          = $where;
			$data['ord_items_join'] = $ord_items_join;

			return $data;
		}

		/**
		 * confirmOrder
		 *
		 * @param $order
		 *
		 * @return mixed|null
		 */
		public function confirmOrder($order)
		{
			if ($order->status == 'C') return NULL;

			$paylog = JTable::getInstance('PaymentLogTable', 'JTheFactory');
			if ($order->paylogid) $paylog->load($order->paylogid);
			$date                   = new JDate();
			$paylog->date           = $date->toSQL();
			$paylog->amount         = $order->order_total;
			$paylog->currency       = $order->order_currency;
			$paylog->refnumber      = JText::_("FACTORY_ADMIN_APPROVED");
			$paylog->invoice        = $order->id;
			$paylog->ipn_response   = "";
			$paylog->ipn_ip         = $_SERVER['REMOTE_ADDR'];
			$paylog->status         = 'ok';
			$paylog->userid         = $order->userid;
			$paylog->orderid        = $order->id;
			$paylog->payment_method = JText::_("FACTORY_ADMIN_APPROVED");
			$paylog->store();

			$order->status   = 'C';
			$order->paylogid = $paylog->id;
			$order->store();

			return $paylog;
		}

	} // End Class
