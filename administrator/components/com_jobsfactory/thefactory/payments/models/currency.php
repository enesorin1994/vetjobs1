<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.application.component.model');

	/**
	 * Class JTheFactoryModelCurrency
	 */
	class JTheFactoryModelCurrency extends JModelLegacy
	{
		/**
		 * context
		 *
		 * @var string
		 */
		var $context = 'currency';
		/**
		 * tablename
		 *
		 * @var null|string
		 */
		var $tablename = NULL;

		/**
		 *
		 */
		public function __construct()
		{
			$this->context   = APP_EXTENSION . "_currency";
			$this->tablename = '#__' . APP_PREFIX . '_currency';
			JTheFactoryHelper::tableIncludePath('payments');
			parent::__construct();
		}

		/**
		 * getDefault
		 *
		 * @return mixed
		 */
		public function getDefault()
		{
			$db = $this->getDbo();
			//ensure that if there is no default set, we get a currency
			$db->setQuery("SELECT `name` FROM `{$this->tablename}` ORDER BY `default`=1 DESC", 0, 1);

			return $db->loadResult();
		}

		/**
		 * getCurrencyList
		 *
		 * @return mixed
		 */
		public function getCurrencyList()
		{
			$db = $this->getDbo();
			$db->setQuery("SELECT * FROM `{$this->tablename}` ORDER BY `ordering`");

			return $db->loadObjectList();

		}

		/**
		 * getYahooCurrency
		 *
		 * @param $currency_from
		 * @param $currency_to
		 *
		 * @return bool|float
		 */
		public function getYahooCurrency($currency_from, $currency_to)
		{

			$result = '';

			$url    = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s=' . $currency_from . $currency_to . '=X';
			$handle = @fopen($url, 'r');

			if ($handle) {
				$result = fgets($handle, 4096);
				fclose($handle);
			}

			if (!$result) {
				return FALSE;
			}

			$response = explode(',', $result); /* Get all the contents to an array */
			// Get new rate
			$newRate = floatval(number_format($response[1], 4));

			if ($newRate) {
				return $newRate;
			} else {
				return FALSE;
			}

		}

		/**
		 * convertToDefaultCurrency
		 *
		 * @param $amount
		 * @param $currency
		 *
		 * @return mixed
		 */
		public function convertToDefaultCurrency($amount, $currency)
		{
			if (!$currency) return $amount;
			$db = JFactory::getDbo();
			$db->setQuery('SELECT * from #__' . APP_PREFIX . '_currency WHERE name=' . $db->quote($currency));
			$p = $db->loadObject();

			return $amount * $p->convert;
		}

		/**
		 * convertCurrency
		 *
		 * @param $amount
		 * @param $currency
		 * @param $tocurrency
		 *
		 * @return mixed
		 */
		public function convertCurrency($amount, $currency, $tocurrency)
		{
			if (!$tocurrency || !$currency) return $amount;

			$db = JFactory::getDbo();
			$db->setQuery('SELECT * from #__' . APP_PREFIX . '_currency WHERE name=' . $db->quote($currency));
			$c1 = $db->loadObject();
			$db->setQuery('SELECT * from #__' . APP_PREFIX . '_currency WHERE name=' . $db->quote($tocurrency));
			$c2 = $db->loadObject();

			$convert = $c2->convert ? ($c1->convert / $c2->convert) : 0;

			return $amount * $convert;
		}
	}
