<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.application.component.model');

	class JTheFactoryModelPricing extends JModelLegacy
	{
		var $context = 'pricing';
		var $tablename = NULL;

		/**
		 * JTheFactoryModelPricing constructor
		 */
		public function __construct()
		{
			$this->context   = APP_EXTENSION . "_pricing.";
			$this->tablename = '#__' . APP_PREFIX . '_pricing';

			parent::__construct();
		}

		/**
		 * getPricingList
		 *
		 * @return mixed
		 */
		public function getPricingList()
		{
			$db = $this->getDbo();
			$db->setQuery("SELECT * FROM `{$this->tablename}` WHERE `itemname` NOT IN ('comission') ORDER BY `ordering` ");
			//$db->setQuery("SELECT * FROM `{$this->tablename}` ORDER BY `ordering` ");

			return $db->loadObjectList();
		}

		/**
		 * toggle
		 *
		 * @param $itemname
		 *
		 * @return string
		 */
		public function toggle($itemname)
		{
			$db = $this->getDbo();
			$db->setQuery("SELECT `enabled` FROM `{$this->tablename}` WHERE `itemname`='{$itemname}' ");
			$enabled = $db->loadResult() ? "0" : "1";
			$db->setQuery("UPDATE `{$this->tablename}` SET `enabled`='$enabled' WHERE `itemname`='{$itemname}' ");
			$db->execute();

			return $enabled ? (JText::_("FACTORY_ITEM_ENABLED")) : (JText::_("FACTORY_ITEM_DISABLED"));
		}
	} // End Class
