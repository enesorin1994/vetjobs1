<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<form action = "index.php" method = "get" name = "adminForm" id = "adminForm">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "id" value = "<?php echo $this->order->id; ?>" />
	<input type = "hidden" name = "task" value = "orders.listing" />
	<input type = "hidden" name = "boxchecked" value = "0" />

	<div class = 'pull-left' style = "width: 100%;clear: both;">
		<fieldset class = "adminFieldList">
			<legend><?php echo JText::_('FACTORY_ORDER_NUMBER') . " " . $this->order->id; ?></legend>
			<table class = "paramlist admintable" border = "0" width = "880px">
				<tr>
					<td class = "paramlist_key" align = "left" width = "250px">
						<?php echo JText::_("FACTORY_ORDER_DATE"); ?>
					</td>
					<td class = "paramlist_value">
						<?php echo JHtml::_('date', $this->order->orderdate, $this->cfg->date_format . ' ' . $this->cfg->date_time_format); ?>
					</td>
				</tr>
				<tr>
					<td class = "paramlist_key" align = "left" width = "250">
						<?php echo JText::_("FACTORY_USERNAME"); ?>
					</td>
					<td class = "paramlist_value">
						<?php echo $this->user->username; ?>
						<span style = "font-size:11px;">
							(
							<span>
								<strong><?php echo JText::_("FACTORY_USER_CITY"); ?>:</strong><?php echo $this->user->city; ?>
								<strong><?php echo JText::_("FACTORY_USER_ADDRESS"); ?>:</strong><?php echo $this->user->address; ?>
								<strong><?php echo JText::_("FACTORY_USER_PHONE"); ?>:</strong><?php echo $this->user->phone; ?>
								<strong><?php echo JText::_("FACTORY_USER_EMAIL"); ?>:</strong><?php echo $this->user->email; ?>
							</span>
							)
						</span>
					</td>
				</tr>
				<tr>
					<td class = "paramlist_key" align = "left" width = "250">
						<?php echo JText::_("FACTORY_ORDER_TOTAL"); ?>
					</td>
					<td class = "paramlist_value">
						<?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $this->order->order_total), " ", $this->order->order_currency; ?>
					</td>
				</tr>
				<tr>
					<td class = "paramlist_key" align = "left" width = "250">
						<?php echo JText::_("FACTORY_STATUS"); ?>
					</td>
					<td class = "paramlist_value"><strong>
							<?php
								switch ($this->order->status) {
									case "P":
										echo "<span class='badge badge-info'>" . JText::_("FACTORY_PENDING") . "</span>";
										break;
									case "C":
										echo "<span class='badge badge-success'>" . JText::_("FACTORY_CONFIRMED") . "</span>";
										break;
									case "X":
										echo "<span class='badge badge-warning'>" . JText::_("FACTORY_CANCELLED") . "</span>";
										break;

								}
							?></strong>
					</td>
				</tr>
				<tr>
					<td class = "paramlist_key" align = "left" width = "250">
						<?php echo JText::_("FACTORY_PAYMENT_LOG_ID"); ?>
					</td>
					<td class = "paramlist_value">
						<?php if ($this->order->paylogid): ?>
							<a href = "index.php?option=<?php echo APP_EXTENSION; ?>&task=payments.viewdetails&id=<?php echo $this->order->paylogid; ?>">
								<?php echo $this->order->paylogid; ?>
							</a>
						<?php else: ?>
							-
						<?php endif; ?>
					</td>
				</tr>

			</table>
		</fieldset>
	</div>
	<div class = 'pull-left' style = "width: 100%;clear: both;margin-top:30px;">
		<fieldset class = "adminFieldList">
			<legend><?php echo JText::_('FACTORY_ORDER_DETAILS'), " #", $this->order->id; ?></legend>
			<table class = "paramlist adminlist table table-condensed" border = "0" width = "100%">
				<thead>
				<tr>
					<th>#</th>
					<th><?php echo JText::_("FACTORY_ITEM_NAME"); ?></th>
					<th>&nbsp;</th>
					<th><?php echo JText::_("FACTORY_QUANTITY"); ?></th>
					<th><?php echo JText::_("FACTORY_ITEM_PRICE"); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php
					$i = 1;
					foreach ($this->orderitems as $row): ?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td><?php echo JText::_('FACTORY_PAY_FOR_' . $row->itemname); ?></td>
							<td><?php echo $row->itemdetails; ?></td>
							<td><?php echo $row->quantity; ?></td>
							<td><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $row->price), " ", $row->currency; ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</fieldset>
	</div>
</form>
