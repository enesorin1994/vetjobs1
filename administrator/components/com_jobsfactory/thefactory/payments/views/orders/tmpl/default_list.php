<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<form action = "index.php" method = "get" name = "adminForm" id = "adminForm">
	<input type = "hidden" name = "filter_order" value = "<?php echo $this->sortColumn; ?>" />
	<input type = "hidden" name = "filter_order_Dir" value = "<?php echo $this->sortDirection; ?>" />

	<div id = "filter-bar" class = "btn-toolbar">
		<div class = "filter-search btn-group pull-left">
			<input type = "text" name = "filter_username" id = "filter_username" size = "30"
			       placeholder = "<?php echo JText::_('FACTORY_USERNAME_FILTER'); ?>"
			       value = "<?php echo $this->filter_username; ?>" class = "text_area" style = "font-size: 12px;" />
		</div>
		<div class = "btn-group">
			<button onclick = "this.form.submit();" class = "btn tip hasTooltip"><i class = "icon-search"></i></button>
			<button onclick = "this.form.filter_username.value='';this.form.submit();" class = "btn tip hasTooltip"><i class = "icon-remove"></i></button>
		</div>
	</div>
	<div class = "clearfix"></div>

	<table class = "adminlist table table-condensed" cellspacing = "1">
		<thead>
		<tr>
			<th width = "60" align = "center"><?php echo JHTML::_('grid.sort', JText::_("FACTORY_ORDER_NR"), 'o.id', $this->sortDirection, $this->sortColumn); ?></th>
			<th width = "200" align = "center"><?php echo JHTML::_('grid.sort', JText::_("FACTORY_ORDER_DATE"), 'o.orderdate', $this->sortDirection, $this->sortColumn); ?></th>
			<th class = "title" width = "250"><?php echo JHTML::_('grid.sort', JText::_("FACTORY_USERNAME"), 'u.username', $this->sortDirection, $this->sortColumn); ?></th>
			<th class = "title" width = "150" nowrap = "nowrap"><?php echo JHTML::_('grid.sort', JText::_("FACTORY_ORDER_TOTAL"), 'o.order_total', $this->sortDirection, $this->sortColumn); ?></th>
			<th class = "title" width = "10%"><?php echo JHTML::_('grid.sort', JText::_("FACTORY_PAYMENT_LOG"), 'o.paylogid', $this->sortDirection, $this->sortColumn); ?></th>
			<th class = "title" width = "10%"><?php echo JHTML::_('grid.sort', JText::_("FACTORY_ORDER_STATUS"), 'o.status', $this->sortDirection, $this->sortColumn); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
			$k = 0;
			for ($i = 0, $n = count($this->orders); $i < $n; $i++) {
				$row = &$this->orders[$i];

				$link_orderitems = 'index.php?option=' . APP_EXTENSION . '&task=orders.viewdetails&id=' . $row->id;

				$link_paylog = 'index.php?option=' . APP_EXTENSION . '&task=payments.viewdetails&id=' . $row->paylogid;
				$img_paylog  = $row->paylogid ? "icon-16-allow.png" : "publish_r.png";
				?>
				<tr class = "<?php echo "row$k"; ?>">
					<td align = "left"><?php echo JHtml::_('grid.id', $i, $row->id, FALSE, 'id'), " ", $row->id; ?></td>
					<td align = "left"><?php echo JHtml::_('date', $row->orderdate, $this->cfg->date_format . ' ' . $this->cfg->date_time_format); ?></td>
					<td align = "left"><?php echo JHtml::_('link', 'index.php?option=' . APP_EXTENSION . '&task=detailUser&cid[]=' . $row->userid, $row->username); ?></td>
					<td align = "center">
						<a href = "<?php echo $link_orderitems; ?>" title = "<?php echo JText::_("FACTORY_VIEW_ORDER_ITEMS"); ?>">
							<?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $row->order_total), " ", $row->order_currency; ?>
						</a>
					</td>
					<td align = "center">
						<?php if ($row->paylogid) : ?>
							<a href = "<?php echo $link_paylog; ?>"
							   title = "<?php echo JText::_("FACTORY_VIEW_PAYMENT_LOG_DETAILS"); ?>">
								<?php echo JTheFactoryAdminHelper::imageAdmin($img_paylog); ?>
							</a>
						<?php else: ?>
							<?php echo JTheFactoryAdminHelper::imageAdmin($img_paylog); ?>
						<?php endif; ?>
					</td>
					<td align = "center"><strong style = "font-size: 110%"><?php echo $row->status; ?></strong></td>

				</tr>
				<?php
				$k = 1 - $k;
			}
		?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan = "15">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
		</tfoot>
		<caption>
			<?php echo JText::_('FACTORY_ORDERS_STATUS_X'); ?> |
			<?php echo JText::_('FACTORY_ORDERS_STATUS_P'); ?> |
			<?php echo JText::_('FACTORY_ORDERS_STATUS_C'); ?>
		</caption>
	</table>
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "orders.listing" />
	<input type = "hidden" name = "boxchecked" value = "0" />

</form>
