<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryViewPaymentsOrders extends JTheFactoryView
	{
		public function display($tpl = NULL)
		{
			JFactory::getDocument()->addStyleSheet($img_link = JURI::base() . "components/" . APP_EXTENSION . "/thefactory/payments/css/payments.css");
			$this->addTemplatePath(realpath(dirname(__FILE__) . '/../header/tmpl'));
			parent::display('header');
			parent::display($tpl);
		}

	}
