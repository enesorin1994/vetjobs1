<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	JHtml::_('formbehavior.chosen', 'select');
?>

<form action = "index.php" method = "get" name = "adminForm" id = "adminForm">
	<div class = 'pull-left' style = "width: 100%;clear: both;">
		<h2><?php echo JText::_("FACTORY_ADD_NEW_MANUAL_PAYMENT"); ?></h2>
		<fieldset class = "adminFieldList">
			<legend><?php echo JText::_('FACTORY_PAYMENT_DETAILS'); ?></legend>
			<table class = "paramlist admintable" border = "0" width = "880">
				<tr>
					<td class = "paramlist_key" align = "left" width = "250">
						<label><?php echo JText::_("FACTORY_AMOUNT"); ?></label>
					</td>
					<td class = "paramlist_value">
						<input name = "amount" size = "20" value = "">&nbsp;<?php echo JHtml::_("payments.currencylist"); ?>
					</td>
				</tr>
				<tr>
					<td class = "paramlist_key" align = "left" width = "250">
						<label><?php echo JText::_("FACTORY_REFERENCE_NUMBER"); ?></label>
					</td>
					<td class = "paramlist_value">
						<input name = "refnumber" size = "20" value = "">
					</td>
				</tr>
				<tr>
					<td class = "paramlist_key" align = "left" width = "250">
						<label><?php echo JText::_("FACTORY_STATUS"); ?></label>
					</td>
					<td class = "paramlist_value">
						<?php echo JHtml::_("payments.statuslist"); ?>
					</td>
				</tr>
				<tr>
					<td class = "paramlist_key" align = "left" width = "250">
						<label><?php echo JText::_("FACTORY_USER"); ?></label>
					</td>
					<td class = "paramlist_value">
						<?php echo JHtml::_("payments.userlist"); ?>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "payments.savepayment" />
</form>
