<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<style type = "text/css">
	.icon-48-thefactory-orders {
		background-image: url(<?php echo $this->assets_url;?>images/payments/orders-48.png);
	}

	.icon-48-thefactory-payments {
		background-image: url(<?php echo $this->assets_url;?>images/payments/payments-48.png);
	}

	.icon-48-thefactory-balances {
		background-image: url(<?php echo $this->assets_url;?>images/payments/balances-48.png);
	}

</style>
<div id = "cpanel">
	<?php
		$link = 'index.php?option=' . APP_EXTENSION . '&amp;task=orders.listing';
		JTheFactoryAdminHelper::quickIconButton($link, 'payments/orders.png', JText::_("FACTORY_ORDERS_BUTTON"));

		$link = 'index.php?option=' . APP_EXTENSION . '&amp;task=payments.listing';
		JTheFactoryAdminHelper::quickIconButton($link, 'payments/payments.png', JText::_("FACTORY_PAYMENTS_BUTTON"));

		$link = 'index.php?option=' . APP_EXTENSION . '&amp;task=balances.listing';
		JTheFactoryAdminHelper::quickIconButton($link, 'payments/balances.png', JText::_("FACTORY_BALANCES_BUTTON"));
	?>
</div>
