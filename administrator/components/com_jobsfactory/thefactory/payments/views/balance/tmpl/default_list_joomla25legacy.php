<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<form name = "adminForm" id = "adminForm" action = "index.php" method = "get">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "balances.listing" />

	<div>
		<span class = "label"><?php echo JText::_('FACTORY_USERNAME_FILTER'); ?>:</span>
		<input type = "text"
		       name = "filter_userid"
		       id = "filter_userid"
		       size = "30"
		       style = "font-size: 12px;"
		       value = "<?php echo $this->filter_userid; ?>" class = "inputbox"
		       title = "<?php echo JText::_('FACTORY_PART_OF_USERNAME'); ?>"
			/>
		&nbsp;&nbsp;&nbsp;
		<?php echo $this->filter_balances; ?>
		<input type = "submit" class = "btn" name = "filterbutton" value = "<?php echo JText::_("FACTORY_FILTER"); ?>" />
	</div>
	<div style = "clear:both;"></div>
	<table class = "adminlist table table-condensed">
		<thead>
		<tr>
			<th width = "30"><?php echo JText::_("FACTORY_USERID"); ?></th>
			<th width = "*%"><?php echo JText::_("FACTORY_USER_NAME"); ?></th>
			<th width = "100"><?php echo JText::_("FACTORY_AMOUNT"); ?></th>
			<?php if ($this->withdraw) : ?>
				<th width = "150"><?php echo JText::_("FACTORY_REQ_WITHDRAWAL"); ?></th>
				<th width = "150"><?php echo JText::_("FACTORY_WITHDRAWN_BEFORE"); ?></th>
			<?php endif; ?>
			<th width = "80"><?php echo JText::_("FACTORY_CURRENCY"); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
			$odd = 0;
			foreach ($this->userbalances as $userbalance) {
				?>
				<tr class = "row<?php echo($odd = 1 - $odd); ?>">
					<td align = "center"><?php echo $userbalance->userid; ?></td>
					<td><?php echo $userbalance->username; ?></td>
					<td align = "right"><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $userbalance->balance); ?></td>
					<?php if ($this->withdraw) : ?>
						<td align = "right">
							<?php echo JHtml::_('payments.paywithdrawamount', $userbalance->userid, $userbalance->req_withdraw, 'pay_paypal'); ?>
						</td>
						<td align = "right"><?php echo JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $userbalance->withdrawn_until_now); ?></td>
					<?php endif; ?>
					<td><?php echo $userbalance->currency; ?></td>
				</tr>
			<?php } ?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan = "15">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
		</tfoot>

	</table>
</form>
