<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/
	// Access the file from Joomla environment
	defined('_JEXEC') or die('Restricted access');
?>

<div class = "payment_withdraw">
	<div class = "info_payment">
		<?php

			$amountToPay = JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $this->userbalance->req_withdraw);
			echo sprintf(JText::_('FACTORY_WITHDRAW_PAYMENT_INFO'), $amountToPay, trim($this->userbalance->currency), "<span class='username'>" . trim($this->user->username) . "</span>", JHtml::date($this->userbalance->last_withdraw_date, $this->cfg->date_format));
		?>
	</div>
	<?php
		// Display PayPal payment form
		echo $this->withdrawForm;
	?>
</div>
