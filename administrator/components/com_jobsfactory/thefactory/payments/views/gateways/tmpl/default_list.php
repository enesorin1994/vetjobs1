<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<h2><?php echo JText::_("FACTORY_PAYMENT_GATEWAYS_LIST"); ?></h2>

<form action = "index.php" method = "get" name = "adminForm" id = "adminForm">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "gateways.listing" />
	<input type = "hidden" name = "boxchecked" value = "0" />

	<table class = "adminlist table table-striped table-condensed" cellspacing = "1">
		<thead>
		<tr>
			<th width = "60" align = "center">#</th>
			<th width = "80" style="text-align: center;">
				<a href = "javascript:Joomla.submitbutton('gateways.reorder')" class = "saveorder"
				   title = "<?php echo JText::_('FACTORY_SAVE_ORDERING'); ?>"><i class="icon-list-view"></i><?php echo JText::_('FACTORY_SAVE_ORDERING'); ?></a>
			</th>
			<th class = "title"><?php echo JText::_("FACTORY_PAYMENT_SYSTEM"); ?></th>
			<th class = "title" width = "30%" nowrap = "nowrap"><?php echo JText::_("FACTORY_CLASS_NAME"); ?></th>
			<th class = "title" width = "10%"><?php echo JText::_("FACTORY_IS_DEFAULT_GATEWAY"); ?></th>
			<th class = "title" width = "10%"><?php echo JText::_("FACTORY_ENABLED"); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
			$k = 0;
			for ($i = 0, $n = count($this->gateways); $i < $n; $i++) {
				$row          = & $this->gateways[$i];
				$link         = 'index.php?option=' . APP_EXTENSION . '&task=gateways.edit&id=' . $row->id;
				$link_enable  = 'index.php?option=' . APP_EXTENSION . '&task=gateways.toggle&id=' . $row->id;
				$link_default = 'index.php?option=' . APP_EXTENSION . '&task=gateways.setdefault&id=' . $row->id;
				$img_enabled  = $row->enabled ? "tick.png" : "publish_r.png";
				$img_default  = $row->isdefault ? "featured.png" : "disabled.png";
				$alt_enabled  = $row->enabled ? JText::_('FACTORY_ENABLED') : JText::_('FACTORY_DISABLED');
				$alt_default  = $row->isdefault ? JText::_('FACTORY_IS_DEFAULT_GATEWAY') : JText::_('FACTORY_SET_DEFAULT');
				?>
				<tr class = "<?php echo "row$k"; ?>">
					<td align = "center"><?php echo $i + 1, '. ', JHtml::_('grid.id', $i + 1, $row->id, FALSE, 'id'); ?></td>
					<td align = "center">
						<input type = "text" size = "5" name = "order_<?php echo $row->id; ?>" value = "<?php echo $row->ordering; ?>"
						       class = "text_area input-mini" style = "text-align: center" />
					</td>
					<td align = "left"><a href = "<?php echo $link; ?>"><?php echo $row->paysystem; ?></a></td>
					<td align = "center"><?php echo $row->classname; ?></td>
					<td align = "center">
						<a href = "<?php echo $link_default; ?>">
							<?php echo JTheFactoryAdminHelper::imageAdmin($img_default, $alt_default); ?>
						</a>
					</td>
					<td align = "center">
						<a href = "<?php echo $link_enable; ?>">
							<?php echo JTheFactoryAdminHelper::imageAdmin($img_enabled, $alt_enabled); ?>
						</a>
					</td>

				</tr>
				<?php
				$k = 1 - $k;
			}
		?>
		</tbody>
	</table>

</form>
