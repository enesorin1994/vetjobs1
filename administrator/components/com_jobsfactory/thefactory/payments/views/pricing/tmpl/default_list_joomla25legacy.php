<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<form name = "adminForm" id = "adminForm" action = "index.php" method = "post">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "themes.setdefault" />

	<div id = "cpanel" class = "pricing">
		<div style = "width: 50%;float:left;">
			<div class = '<?php echo $this->classWidth; ?>'>

				<?php foreach ($this->pricing as $item): ?>
					<div style = "float: left;position: relative;">

						<div class = "icon">

							<?php
								$link = 'index.php?option=' . APP_EXTENSION . '&task=pricing.config&item=' . $item->itemname;
								$image = JHtml::_("image", JURI::base() . "components/" . APP_EXTENSION . "/pricing/{$item->itemname}/icon.png", $item->itemname);

								echo '<a href="' . $link . '" style="position:relative;z-index:1000;overflow:auto;">' . $image . '</a>';
							?>

							<a href = "index.php?option=<?php echo APP_EXTENSION; ?>&task=pricing.toggle&pricingitem=<?php echo $item->itemname; ?>" class = "pricing-enable">
								<?php
									$img = ($item->enabled) ? "tick.png" : "icon-16-deny.png";
									$toggle = ($item->enabled) ? "Click to disable" : "Click to enable";
									echo JTheFactoryAdminHelper::imageAdmin($img, $toggle, 'title="' . JText::_($toggle) . '"');
								?>
							</a>

						</div>
						<span style = "position: relative;bottom:0;right:10px;"><?php echo JText::_($item->name); ?></span>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</div>
</form>
