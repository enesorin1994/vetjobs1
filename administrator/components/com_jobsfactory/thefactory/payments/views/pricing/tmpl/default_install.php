<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<form action = "index.php" method = "post" name = "adminForm" id = "adminForm" enctype = "multipart/form-data">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "pricing.doupload" />

	<div class = "info_header">
		<h2><?php echo JText::_("FACTORY_UPLOAD_PRICING_ITEM_ZIP_PACK") ?></h2>
	</div>
	<table class = "adminlist">
		<tr>
			<td><?php echo JText::_("FACTORY_NEW_PRICING_ZIP"); ?> :</td>
			<td><input name = "pack" type = "file" value = "" size = "60" /></td>
		</tr>
	</table>
</form>
