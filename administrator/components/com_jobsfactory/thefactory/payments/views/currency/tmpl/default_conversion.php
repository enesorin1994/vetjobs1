<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<h1><?php echo JText::_("FACTORY_REFRESHING__CURRENCY_CONVERSION_RATES"); ?></h1>
<div>
	<h2><?php echo JText::_("FACTORY_USING_YAHOO_CONVERSION"); ?></h2>
	<span class = "check-currency"><?php echo JHtml::_('link', 'http://finance.yahoo.com/currency-investing', JText::_('FACTORY_SEE_CURRENCIES_IN_YAHOO'), array('target' => '_blank')); ?></span>
</div>
<div><h3><?php echo JText::_("FACTORY_DEFAULT_CURRENCY"), ": $this->default_currency"; ?></h3></div>

<ul>
	<?php foreach ($this->results as $res): ?>
		<li><?php echo $res; ?></li>
	<?php endforeach; ?>
</ul>
<form name = "adminForm" id = "adminForm" action = "index.php" method = "get">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "currencies.listing" />
	<input type = "hidden" name = "boxchecked" value = "" />
</form>
