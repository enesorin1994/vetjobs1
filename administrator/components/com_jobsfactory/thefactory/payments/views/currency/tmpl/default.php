<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>
<div class = "row-fluid">
	<div class = "span12">
		<form name = "adminForm" id = "adminForm" action = "index.php" method = "get">
			<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
			<input type = "hidden" name = "task" value = "currencies.listing" />
			<input type = "hidden" name = "boxchecked" value = "" />
			<table class = "table adminlist table-condensed">
				<thead>
				<tr>
					<th width = "5"><input type = "checkbox" name = "toggle" value = ""
					                       onclick = "Joomla.checkAll(this);" /></th>
					<th width = "80"><?php echo JText::_("FACTORY_CURRENCY"); ?></th>
					<th width = "150"><?php echo JText::_("FACTORY_DEFAULT_CURRENCY"); ?></th>
					<th><?php echo JText::_("FACTORY_CONVERSION_RATE"); ?></th>
					<th width = "50">
						<?php echo JHtml::_('grid.order', $this->currencies, 'filesave.png', 'currencies.reorder'); ?>
					</th>
					<th align = "left" width = "20"><?php echo JText::_("FACTORY_DELETE"); ?></th>
				</tr>
				</thead>
				<?php
					$odd = 0;
					foreach ($this->currencies as $k => $currency) {
						?>
						<tr>
							<td align = "center">
								<?php echo JHTML::_('grid.id', $k, $currency->id); ?>
							</td>
							<td><?php echo $currency->name; ?></td>
							<?php if ($currency->default): ?>
								<td><?php echo JTheFactoryAdminHelper::imageAdmin('featured.png'); ?></td>
							<?php else: ?>
								<td>
									<a href = "index.php?option=<?php echo APP_EXTENSION; ?>&task=currencies.setdefault&cid=<?php echo $currency->id; ?>">
										<?php echo JTheFactoryAdminHelper::imageAdmin('disabled.png'); ?>
									</a>
								</td>
							<?php endif; ?>
							<td><?php echo number_format($currency->convert, 4); ?></td>
							<td><input type = "text" size = "5" name = "order_<?php echo $currency->id; ?>"
							           value = "<?php echo $currency->ordering; ?>" class = "text_area input-mini" style = "text-align: center" /></td>
							<td align = "center">
								<?php
									$link = 'index.php?option=' . APP_EXTENSION . '&task=currencies.delete&cid[]=' . $currency->id;
								?>
								<a href = "<?php echo $link; ?>">
									<?php echo JTheFactoryAdminHelper::imageAdmin('publish_x.png', JText::_('FACTORY_DELETE')); ?>
								</a>
							</td>
						</tr>
					<?php } ?>
			</table>
		</form>
	</div>
</div>
