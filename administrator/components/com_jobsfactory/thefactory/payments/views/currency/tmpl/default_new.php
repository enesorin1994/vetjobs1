<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<form name = "adminForm" id = "adminForm" action = "index.php" method = "post">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "currencies.save" />

	<div class = "row-fluid">
		<div class = "span2">
			<?php echo JText::_("FACTORY_CURRENCY"); ?>
		</div>
		<div class = "span10">
			<input name = "name" value = "" />
		</div>
	</div>
	<div class = "row-fluid">
		<div class = "span2">
			<?php echo JText::_("FACTORY_CONVERSION_RATE"); ?> </div>
		<div class = "span10">
			<input name = "convert" value = "" />
		</div>
	</div>

</form>
