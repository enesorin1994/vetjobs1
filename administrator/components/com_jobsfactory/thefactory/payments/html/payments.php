<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JHTMLPayments
	 */
	abstract class JHTMLPayments
	{
		/**
		 * StatusList
		 *
		 * @param null $defaultvalue
		 *
		 * @return mixed
		 */
		public static function StatusList($defaultvalue = NULL)
		{
			$status[] = JHtml::_('select.option', 'ok', 'ok');
			$status[] = JHtml::_('select.option', 'error', 'error');
			$status[] = JHtml::_('select.option', 'manual_check', 'manual_check');

			return JHtml::_('select.genericlist', $status, 'paymentstatus', '', 'value', 'text', $defaultvalue);
		}

		/**
		 * StatusText
		 *
		 * @param $orderstatus
		 *
		 * @return string
		 */
		public static function StatusText($orderstatus)
		{
			if ($orderstatus == 'P') return JText::_("FACTORY_PENDING");
			if ($orderstatus == 'C') return JText::_("FACTORY_COMPLETED");
			if ($orderstatus == 'X') return JText::_("FACTORY_CANCELLED");

			return '';
		}

		/**
		 * CurrencyList
		 *
		 * @return mixed
		 */
		public static function CurrencyList()
		{
			$model        = JModelLegacy::getInstance('Currency', 'JTheFactoryModel');
			$currency     = $model->getDefault();
			$currencies[] = JHtml::_('select.option', $currency, $currency);

			return JHtml::_('select.genericlist', $currencies, 'currency');
		}

		/**
		 * UserList
		 *
		 * @param null $defaultuser
		 *
		 * @return mixed
		 */
		public static function UserList($defaultuser = NULL)
		{
			$db = JFactory::getDbo();
			$db->setQuery("SELECT `username` AS text,`id` AS `value` FROM `#__users` ORDER BY `username`");
			$users = $db->loadObjectList();

			return JHtml::_('select.genericlist', $users, 'userid', '', 'value', 'text', $defaultuser);
		}

		/**
		 * PayWithdrawAmount
		 *
		 * @param        $userid
		 * @param null   $withdraw_amount
		 * @param string $payment_gateway
		 *
		 * @return null|string
		 */
		public static function PayWithdrawAmount($userid, $withdraw_amount = NULL, $payment_gateway = 'pay_paypal')
		{
			$result = JTheFactoryFiltersHelper::trigger('filterPriceToDisplay', $withdraw_amount);
			if (floatval($withdraw_amount > 0)) {
				$queryUrl = http_build_query(array('option' => APP_EXTENSION, 'task' => 'balances.withdrawForm', 'gateway' => $payment_gateway, 'userId' => $userid));
				$img_link = JURI::base() . "components/" . APP_EXTENSION . "/thefactory/payments/images/pay_now.png";
				$alt_text = JText::_('FACTORY_PAY_NOW');
				$result .= JHtml::_('link', JRoute::_('index.php?' . $queryUrl), "<img src='{$img_link}' alt = '{$alt_text}' title = '{$alt_text}' style = 'float:left;' />");
			}

			return $result;

		}

		/**
		 * OrderDetails
		 *
		 * @param int $orderid
		 *
		 * @return string
		 */
		public static function OrderDetails($orderid = 0)
		{
			$model = JModelLegacy::getInstance('Orders', 'JTheFactoryModel');
			$items = $model->getOrderItems($orderid);
			if (count($items) <= 0) return "";

			return $items[0]->itemdetails . ((count($items) == 1) ? "" : "[..]");

		}
	}
