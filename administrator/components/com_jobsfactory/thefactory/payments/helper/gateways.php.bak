<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/

	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryGatewaysHelper
	 */
	class JTheFactoryGatewaysHelper
	{
		/**
		 * isPackXML
		 *
		 * @param $manifest_file
		 *
		 * @return bool
		 */
		public static function isPackXML($manifest_file)
		{
			$xml = simplexml_load_file($manifest_file);
			if (!$xml) {
				return FALSE;
			}
			if (!isset($xml->attributes()->type)) return FALSE;
			if ((string)$xml->attributes()->type != "gateway") return FALSE;

			return TRUE;
		}

		/**
		 * parseQueries
		 *
		 * @param $element
		 *
		 * @return bool|int
		 */
		public static function parseQueries($element)
		{

			if (!$element || !count($element->children())) {
				// Either the tag does not exist or has no children therefore we return zero files processed.
				return 0;
			}
			// Get the array of query nodes to process
			$queries = $element->children();
			if (count($queries) == 0) {
				// No queries to process
				return 0;
			}
			// Get the database connector object
			$db = JFactory::getDbo();
			// Process each query in the $queries array (children of $tagName).
			foreach ($queries as $query) {
				$db->setQuery($query->data());

				if (!$db->execute()) {
					JFactory::getApplication()->enqueueMessage(JText::sprintf('JLIB_INSTALLER_ERROR_SQL_ERROR'), 'error');

					return FALSE;
				}
			}

			return (int)count($queries);
		}

		/**
		 * installGatewayPack
		 *
		 * @param $sourcepath
		 *
		 * @return null
		 */
		public static function installGatewayPack($sourcepath)
		{
			if (!self::isPackXML($sourcepath . "/manifest.xml")) {
				JFactory::getApplication()->enqueueMessage(JText::_("FACTORY_FILE_IS_NOT_A_VALID_GATEWAY_MANIFEST") . " {$sourcepath}/manifest.xml", 'error');

				return NULL;
			}
			$xml        = simplexml_load_file($sourcepath . "/manifest.xml");
			$destfolder = (string)$xml->attributes()->folder;

			$MyApp       = JTheFactoryApplication::getInstance();
			$destination = $MyApp->app_path_admin . 'payments/plugins/gateways/' . strtolower($destfolder);

			jimport("joomla.filesystem.folder");
			JFolder::copy($sourcepath, $destination);

			$db = JFactory::getDbo();
			$db->setQuery("select max(`ordering`)+1 from #__" . APP_PREFIX . "_paysystems");
			$maxordering = $db->loadResult();

			$gw = JTable::getInstance('GatewaysTable', 'JTheFactory');

			$gw->paysystem = (string)$xml->name;
			$gw->classname = (string)$xml->attributes()->folder;
			$gw->enabled   = 0;
			$gw->params    = NULL;
			$gw->ordering  = $maxordering;
			$gw->isdefault = 0;
			$gw->store();

			self::parseQueries($xml->queries);

		}

		/**
		 * unpackGatewayPack
		 *
		 * @param $p_filename
		 *
		 * @return bool
		 */
		public static function unpackGatewayPack($p_filename)
		{
			// Path to the archive
			$archivename = $p_filename;

			// Temporary folder to extract the archive into
			$tmpdir = uniqid('install_');

			// Clean the paths to use for archive extraction
			$extractdir  = JPath::clean(dirname($p_filename) . '/' . $tmpdir);
			$archivename = JPath::clean($archivename);
			jimport('joomla.filesystem.archive');
			// Do the unpacking of the archive
			$result = JArchive::extract($archivename, $extractdir);

			if ($result === FALSE) {
				return FALSE;
			}


			/*
			 * Let's set the extraction directory and package file in the result array so we can
			 * cleanup everything properly later on.
			 */
			$retval['extractdir']  = $extractdir;
			$retval['packagefile'] = $archivename;

			/*
			 * Try to find the correct install directory.  In case the package is inside a
			 * subdirectory detect this and set the install directory to the correct path.
			 *
			 * List all the items in the installation directory.  If there is only one, and
			 * it is a folder, then we will set that folder to be the installation folder.
			 */
			$dirList = array_merge(JFolder::files($extractdir, ''), JFolder::folders($extractdir, ''));

			if (count($dirList) == 1) {
				if (JFolder::exists($extractdir . '/' . $dirList[0])) {
					$extractdir = JPath::clean($extractdir . '/' . $dirList[0]);
				}
			}

			/*
			 * We have found the install directory so lets set it and then move on
			 * to detecting the extension type.
			 */
			$retval['dir'] = $extractdir;

			return $retval;
		}

	}
