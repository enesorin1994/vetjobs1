<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *-------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryCartHelper
	 */
	class JTheFactoryCartHelper
	{
		/**
		 * getCartTasks
		 *
		 * Return an array containing allowed tasks to work with Cart system
		 *
		 * @return array
		 */
		public static function getCartTasks()
		{
			$MyApp     = JTheFactoryApplication::getInstance();
			$cartTasks = explode(',', $MyApp->getIniValue('use_cart_tasks', 'payments'));

			// Remove any white spaces
			return array_map(function ($item) {
				return trim($item);
			}, $cartTasks);
		}

		/**
		 * addItem
		 *
		 * Add item to Cart session
		 *
		 * @param object $item        Order item stdClass
		 * @param string $returnToUrl Return to URL; set without using JRoute (is added internally)
		 *
		 * @throws Exception
		 * @return null
		 */
		public static function addItem($item, $returnToUrl = '')
		{
			$app   = JFactory::getApplication();
			$input = $app->input;

			// Return to default task
			if (empty($returnToUrl)) {
				$returnToUrl = "index.php?option=" . APP_EXTENSION;
			}

			if (in_array($input->getCmd('task'), self::getCartTasks())) {
				JTheFactoryEventsHelper::triggerEvent('onAddToCart', array($item));

				$app->redirect(JRoute::_($returnToUrl));

				return NULL;
			}

			return NULL;
		}

		/**
		 * setActionLabel
		 *
		 * Use default button label or Cart specific
		 *
		 * @param $label
		 * @param $task
		 *
		 * @return string
		 */
		public static function setActionLabel($label, $task)
		{

			if (in_array($task, self::getCartTasks())) {
				return JText::_('FACTORY_CART_LABEL_ADD_TO_CART');
			}

			return $label;
		}
	}

