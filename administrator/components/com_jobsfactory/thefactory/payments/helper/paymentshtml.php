<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryPaymentsHtmlHelper
	 */
	class JTheFactoryPaymentsHtmlHelper
	{
		/**
		 * quickIconButton
		 *
		 * @param $link
		 * @param $image
		 * @param $text
		 *
		 * @return string
		 */
		public static function quickIconButton($link, $image, $text)
		{
			$html = "
    	<div style=\"float:left;\">
    		<div class=\"icon\">
    			<a href=\"{$link}\">" . JTheFactoryAdminHelper::imageAdmin($image, $text) . "<span>" . JText::_($text) . "</span></a>
    		</div>
    	</div>";

			return $html;
		}
	}
