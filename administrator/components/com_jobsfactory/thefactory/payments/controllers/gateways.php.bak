<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/

	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryGatewaysController
	 */
	class JTheFactoryGatewaysController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'Gateways';
		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'Gateways';

		/**
		 *
		 */
		public function __construct()
		{
			parent::__construct('payments');
		}

		/**
		 * Listing
		 *
		 */
		public function Listing()
		{
			$model = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');

			$rows           = $model->getGatewayList(FALSE);
			$view           = $this->getView('gateways');
			$view->gateways = $rows;

			$view->display('list');
		}

		/**
		 * Toggle
		 *
		 */
		public function Toggle()
		{
			$ids   = JFactory::getApplication()->input->get('id', array(0), 'array');
			$model = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');

			if (!is_array($ids)) $ids = array($ids);
			foreach ($ids as $id) $msg = $model->toggle($id);

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=gateways.listing", $msg);
		}

		/**
		 * Reorder
		 *
		 */
		public function Reorder()
		{
			$db = JFactory::getDBO();
			$r  = JTheFactoryHelper::getRequestArray();

			foreach ($r as $k => $v) if (substr($k, 0, 6) == 'order_') {
				$id = substr($k, 6);
				$db->setQuery("update `#__" . APP_PREFIX . "_paysystems` set `ordering`='$v' where id=$id ");
				$db->execute();
			}


			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=gateways.listing");
		}

		/**
		 * SetDefault
		 *
		 */
		public function SetDefault()
		{
			$id    = JFactory::getApplication()->input->get('id', array(0), 'array');
			$model = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');

			if (is_array($id)) $id = $id[0];

			$msg = $model->setdefault($id);

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=gateways.listing", $msg);

		}

		/**
		 * getGatewayObject
		 *
		 * @return mixed
		 */
		private function getGatewayObject()
		{
			$input = JFactory::getApplication()->input;
			$id    = $input->get('id', array(), 'array');
			if (is_array($id)) $id = $id[0];
			if ($id) {
				$table = JTable::getInstance('GatewaysTable', 'JTheFactory');
				$table->load($id);

				$classname = $table->classname;
			} else
				$classname = $input->getString('classname', '');


			$model = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');

			return $model->getGatewayObject($classname);
		}

		/**
		 * Edit
		 *
		 */
		public function Edit()
		{

			$gateway = $this->getGatewayObject();

			$gateway->showAdminForm();

		}

		/**
		 * Save
		 *
		 */
		public function Save()
		{
			$gateway = $this->getGatewayObject();

			$gateway->saveAdminForm();

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=gateways.listing", JText::_('FACTORY_CONFIGURATION_SAVED'));
		}

		/**
		 * Install
		 *
		 */
		public function Install()
		{
			$view = $this->getView('gateways');
			$view->display('install');

		}

		/**
		 * doUpload
		 *
		 * @return bool
		 */
		public function doUpload()
		{
			$files    = JFactory::getApplication()->input->files->getArray($_FILES);
			$userfile = $files['pack'];

			$lang = JFactory::getLanguage();
			$lang->load('com_installer');
			// Make sure that file uploads are enabled in php
			if (!(bool)ini_get('file_uploads')) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=gateways.install", JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLFILE'), 'error');

				return TRUE;
			}

			// Make sure that zlib is loaded so that the package can be unpacked
			if (!extension_loaded('zlib')) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=gateways.install", JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLZLIB'), 'error');

				return TRUE;
			}

			// If there is no uploaded file, we have a problem...
			if (!is_array($userfile)) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=gateways.install", JText::_('COM_INSTALLER_MSG_INSTALL_NO_FILE_SELECTED'), 'error');

				return TRUE;
			}

			// Check if there was a problem uploading the file.
			if ($userfile['error'] || $userfile['size'] < 1) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=gateways.install", JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLUPLOADERROR'), 'error');

				return TRUE;
			}
			// Build the appropriate paths
			$config   = JFactory::getConfig();
			$tmp_dest = $config->get('tmp_path') . '/' . $userfile['name'];
			$tmp_src  = $userfile['tmp_name'];

			// Move uploaded file
			jimport('joomla.filesystem.file');
			JFile::upload($tmp_src, $tmp_dest);
			jimport('joomla.installer.helper');

			$package = JTheFactoryGatewaysHelper::unpackGatewayPack($tmp_dest);
			// Was the package unpacked?
			if (!$package) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=gateways.install", JText::_('COM_INSTALLER_UNABLE_TO_FIND_INSTALL_PACKAGE'), 'error');

				return TRUE;
			}

			JTheFactoryGatewaysHelper::installGatewayPack($package['dir']);
			if (!is_file($package['packagefile'])) {
				$config                 = JFactory::getConfig();
				$package['packagefile'] = $config->get('tmp_path') . '/' . $package['packagefile'];
			}

			JInstallerHelper::cleanupInstall($package['packagefile'], $package['extractdir']);
			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=gateways.listing", JText::_("FACTORY_PAYMENT_GATEWAY_INSTALLED"));

		}

	}
