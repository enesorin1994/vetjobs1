<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryOrdersController
	 */
	class JTheFactoryOrdersController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		public $name = 'Orders';
		/**
		 * _name
		 *
		 * @var string
		 */
		public $_name = 'Orders';

		/**
		 *
		 */
		public function __construct()
		{
			parent::__construct('payments');
		}

		/**
		 * Listing
		 *
		 */
		public function Listing()
		{
			$app             = JFactory::getApplication();
			$cfg             = JTheFactoryHelper::getConfig();
			$filter_username = $app->getUserStateFromRequest(APP_EXTENSION . "_payments." . 'filter_username', 'filter_username', '', 'string');

			$model = JModelLegacy::getInstance('Orders', 'JTheFactoryModel');
			$rows  = $model->getOrdersList($filter_username);

			$view                  = $this->getView('orders');
			$view->cfg             = $cfg;
			$view->orders          = $rows;
			$view->pagination      = $model->get('pagination');
			$view->filter_username = $filter_username;

			$view->sortDirection = $model->getState('list.direction');
			$view->sortColumn    = $model->getState('list.ordering');

			$jVersion = new JVersion();
			$jVersion->isCompatible('3.0') ? $view->display('list') : $view->display('list_joomla25legacy');
		}

		/**
		 * ViewDetails
		 *
		 */
		public function ViewDetails()
		{
			$orderid = JFactory::getApplication()->input->getInt('id', 0);
			$order   = JTable::getInstance('OrdersTable', 'JTheFactory');
			$model   = JModelLegacy::getInstance('Orders', 'JTheFactoryModel');
			$cfg     = JTheFactoryHelper::getConfig();

			if (!$orderid || !$order->load($orderid)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orders.listing', JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}

			$items = $model->getOrderItems($orderid);
			$user  = JTheFactoryUserProfile::getInstance(@$cfg->{APP_PREFIX . '_opt_profile_mode'}, $order->userid);

			$view             = $this->getView('orders');
			$view->orderitems = $items;
			$view->order      = $order;
			$view->user       = $user;
			$view->cfg        = $cfg;

			$view->display('details');

		}

		/**
		 * Manual confirmation
		 */
		public function Confirm()
		{
			$orderid = JFactory::getApplication()->input->get('id', array(), 'array');
			if (is_array($orderid)) $orderid = $orderid[0];

			$order = JTable::getInstance('OrdersTable', 'JTheFactory');
			$model = JModelLegacy::getInstance('Orders', 'JTheFactoryModel');

			if (!$orderid || !$order->load($orderid)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orders.listing', JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}
			if ($order->status == 'C') {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orders.viewdetails&id=' . $orderid, JText::_("FACTORY_ORDER_IS_ALREADY_CONFIRMED"));

				return;
			}

			$paylog = $model->confirmOrder($order);

			JTheFactoryEventsHelper::triggerEvent('onPaymentForOrder', array($paylog, $order));

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orders.viewdetails&id=' . $orderid, JText::_("FACTORY_ORDER_WAS_CONFIRMED"));
		}

		/**
		 * Cancel
		 *
		 */
		public function Cancel()
		{
			$orderid = JFactory::getApplication()->input->get('id', array(), 'array');
			if (is_array($orderid)) $orderid = $orderid[0];

			$order = JTable::getInstance('OrdersTable', 'JTheFactory');

			if (!$orderid || !$order->load($orderid)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orders.listing', JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}
			if ($order->status == 'X') {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orders.viewdetails&id=' . $orderid, JText::_("FACTORY_ORDER_IS_ALREADY_CANCELLED"));

				return;
			}
			$oldOrderStatus = $order->status;
			$order->status  = 'X';
			$order->store();

			if ('C' == $oldOrderStatus) {
				// Refund balance
				JTheFactoryEventsHelper::triggerEvent('onOrderCanceled', array($order));
			}

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=orders.viewdetails&id=' . $orderid, JText::_("FACTORY_ORDER_WAS_CANCELLED"));
		}
	}
