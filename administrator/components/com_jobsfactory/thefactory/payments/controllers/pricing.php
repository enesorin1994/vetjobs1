<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryPricingController
	 */
	class JTheFactoryPricingController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'Pricing';
		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'Pricing';

		/**
		 * JTheFactoryPricingController constructor
		 */
		public function __construct()
		{
			parent::__construct('payments');
		}

		/**
		 * execute
		 *
		 * @param string $task
		 *
		 * @return mixed
		 */
		public function execute($task)
		{
			$item = JFactory::getApplication()->input->getString('item', '');
			if ($item) {
				$controllername = 'J' . APP_PREFIX . 'Admin' . ucfirst($item) . 'Controller';
				require_once(JPATH_ADMINISTRATOR . '/components/' . APP_EXTENSION . '/pricing/' . strtolower($item) . '/controllers/admin.php');
				$controller = new $controllername();
				$res        = $controller->execute($task);
				$this->setRedirect($controller->redirect, $controller->message, $controller->messageType);

				return $res;
			} else {
				return parent::execute($task);
			}
		}

		/**
		 * Listing
		 */
		public function Listing()
		{
			$model = JModelLegacy::getInstance('Pricing', 'JTheFactoryModel');
			$rows  = $model->getPricingList();
			// TODO Factory: For J2.5 compatibility
			$jVersion   = new JVersion();
			$classWidth = $jVersion->isCompatible('3.0') ? 'fltlft' : 'width-100 fltlft';

			$view             = $this->getView('pricing');
			$view->pricing    = $rows;
			$view->classWidth = $classWidth;

			// TODO Factory: For J2.5 compatibility
			if ($jVersion->isCompatible('3.0')) {
				$view->display('list');
			} else {
				$view->display('list_joomla25legacy');
			}

		}

		/**
		 * Toggle
		 */
		public function Toggle()
		{
			$item  = JFactory::getApplication()->input->getString('pricingitem', '');
			$model = JModelLegacy::getInstance('Pricing', 'JTheFactoryModel');
			$msg   = $model->toggle($item);

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=pricing.listing", $msg);
		}

		/**
		 * Install
		 */
		public function Install()
		{
			$view = $this->getView('pricing');
			$view->display('install');
		}

		/**
		 * doUpload
		 *
		 * @return bool
		 */
		public function doUpload()
		{
			$files    = JFactory::getApplication()->input->files->getArray($_FILES);
			$userfile = $files['pack'];

			$lang = JFactory::getLanguage();
			$lang->load('com_installer');
			// Make sure that file uploads are enabled in php
			if (!(bool)ini_get('file_uploads')) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=pricing.install", JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLFILE'), 'error');

				return TRUE;
			}

			// Make sure that zlib is loaded so that the package can be unpacked
			if (!extension_loaded('zlib')) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=pricing.install", JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLZLIB'), 'error');

				return TRUE;
			}

			// If there is no uploaded file, we have a problem...
			if (!is_array($userfile)) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=pricing.install", JText::_('COM_INSTALLER_MSG_INSTALL_NO_FILE_SELECTED'), 'error');

				return TRUE;
			}

			// Check if there was a problem uploading the file.
			if ($userfile['error'] || $userfile['size'] < 1) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=pricing.install", JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLUPLOADERROR'), 'error');

				return TRUE;
			}
			// Build the appropriate paths
			$config   = JFactory::getConfig();
			$tmp_dest = $config->get('tmp_path') . '/' . $userfile['name'];
			$tmp_src  = $userfile['tmp_name'];

			// Move uploaded file
			jimport('joomla.filesystem.file');
			JFile::upload($tmp_src, $tmp_dest);
			jimport('joomla.installer.helper');

			$package = JTheFactoryPricingHelper::unpackPricingPack($tmp_dest);
			// Was the package unpacked?
			if (!$package) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=pricing.install", JText::_('COM_INSTALLER_UNABLE_TO_FIND_INSTALL_PACKAGE'), 'error');

				return TRUE;
			}

			JTheFactoryPricingHelper::installPricingPack($package['dir']);
			if (!is_file($package['packagefile'])) {
				$config                 = JFactory::getConfig();
				$package['packagefile'] = $config->get('tmp_path') . '/' . $package['packagefile'];
			}

			JInstallerHelper::cleanupInstall($package['packagefile'], $package['extractdir']);

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=pricing.listing", JText::_("FACTORY_PRICING_ITEM_INSTALLED"));

		}
	}
