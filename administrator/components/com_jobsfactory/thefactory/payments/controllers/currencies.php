<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	/**
	 * Class JTheFactoryCurrenciesController
	 */
	class JTheFactoryCurrenciesController extends JTheFactoryController
	{

		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'Currencies';
		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'Currencies';

		/**
		 *
		 */
		public function __construct()
		{
			parent::__construct('payments');
		}

		/**
		 * Listing
		 *
		 */
		public function Listing()
		{
			$db = JFactory::getDbo();

			$db->setQuery("SELECT * FROM `#__" . APP_PREFIX . "_currency` order by `ordering`");
			$rows = $db->loadObjectList();

			// Check if default currency exist
			// If not, set first currency to be default currency
			$isDefault = FALSE;
			if (count($rows)) {
				foreach ($rows as $currency) {
					if ($currency->default) {
						$isDefault = TRUE;
					}
				}
			}
			if (!$isDefault) {
				$db->setQuery("SELECT * FROM `#__" . APP_PREFIX . "_currency` ORDER BY `ordering` LIMIT 1");
				$db->execute();
				// Is mandatory to exist at least one currency
				if ($db->getAffectedRows()) {
					$firstCurrency = $db->loadObject();
					JFactory::getApplication()->input->set('cid', $firstCurrency->id);
					JFactory::getApplication()->input->set('isUserAction', 0);
					$this->SetDefault();
				}
			}

			// Load again currencies with default set
			$db->setQuery("SELECT * FROM `#__" . APP_PREFIX . "_currency` order by `ordering`");
			$rows = $db->loadObjectList();

			$view             = $this->getView('currency', 'html');
			$view->currencies = $rows;

			$view->display();

		}

		/**
		 * Edit
		 *
		 */
		public function Edit()
		{
			$cids = JFactory::getApplication()->input->get('cid', array(0), 'array');
			if (is_array($cids)) $cids = $cids[0];
			$currtable = JTable::getInstance('CurrencyTable', 'JTheFactory');

			if (!$currtable->load($cids)) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=currencies.listing", JText::_("FACTORY_CURRENCY_DOES_NOT_EXIST"));

				return;
			}

			$view           = $this->getView('currency', 'html');
			$view->currency = $currtable;

			$view->display("edit");

		}

		/**
		 * Delete
		 *
		 */
		public function Delete()
		{
			$cids = JFactory::getApplication()->input->get('cid', array(), 'array');
			$msg  = "";
			if (count($cids)) {
				$cid_list = implode(',', $cids);
				$db       = JFactory::getDbo();

				// Check to see if default currency was selected to be deleted
				$db->setQuery("SELECT * FROM `#__" . APP_PREFIX . "_currency` WHERE `id` IN({$cid_list}) AND `default` = 1");
				$db->execute();
				if ($db->getAffectedRows()) {
					$msgDefCurrency = sprintf(JText::_('FACTORY_DEFAULT_CURRENCY_WAS_NOT_DELETED'), '<br />', $db->loadObject()->name);
				}

				$db->setQuery("DELETE FROM `#__" . APP_PREFIX . "_currency` WHERE `id` IN ({$cid_list}) AND `default` IS NULL");
				$db->execute();
				$msg = $db->getAffectedRows() . ' ' . JText::_("FACTORY_ROWS_DELETED") . $msgDefCurrency;
			}
			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=currencies.listing", $msg);
		}

		/**
		 * SetDefault
		 *
		 */
		public function SetDefault()
		{
			$cid          = JFactory::getApplication()->input->getInt('cid', 0);
			$isUserAction = JFactory::getApplication()->input->getInt('isUserAction', 1);
			// Some environments don't load the table.
			JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_jobsfactory/thefactory/payments/tables');
			
			$currtable = JTable::getInstance('CurrencyTable', 'JTheFactory');
			if ($cid && $currtable->load($cid)) {
				$db = JFactory::getDbo();
				$db->setQuery("UPDATE `#__" . APP_PREFIX . "_currency` set `default`=null");
				$db->execute();
				$db->setQuery("UPDATE `#__" . APP_PREFIX . "_currency` set `default`=1 where id={$cid}");
				$db->execute();
				$db->setQuery("UPDATE `#__" . APP_PREFIX . "_currency` set `convert`=`convert`/{$currtable->convert}");
				$db->execute();
				$msg = JText::_("FACTORY_NEW_DEFAULT_CURRENCY") . ": " . $currtable->name;
				JTheFactoryEventsHelper::triggerEvent('onDefaultCurrencyChange');
			} else
				$msg = JText::_("FACTORY_CURRENCY_NOT_FOUND");

			if ($isUserAction) {
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=currencies.listing", $msg);
			}

		}

		/**
		 * NewItem
		 *
		 */
		public function NewItem()
		{
			$view = $this->getView('currency', 'html');
			$view->display("new");
		}

		/**
		 * Save
		 *
		 */
		public function Save()
		{
			$input = JFactory::getApplication()->input;
			/** @var $currtable JTheFactoryCurrencyTable */
			$currtable  = JTable::getInstance('CurrencyTable', 'JTheFactory');
			$id         = $input->getInt('id', 0);
			$olddefault = 0;
			$oldorder   = 0;
			if ($currtable->load($id)) {
				$olddefault = $currtable->default;
				$oldorder   = $currtable->ordering;
			}

			$currtable->bind(JTheFactoryHelper::getRequestArray('post'));
			$currtable->default  = $olddefault;
			$currtable->ordering = $oldorder;
			JTheFactoryEventsHelper::triggerEvent('onBeforeCurrencySaved', array($currtable));
			$currtable->store();
			JTheFactoryEventsHelper::triggerEvent('onAfterCurrencySaved', array($currtable));
			$session                    = JFactory::getSession();
			$gatewayCurrencyUnsupported = $session->get('gatewayCurrencyUnsupported', array(), 'theFactory');
			$msg                        = '';
			if (is_array($gatewayCurrencyUnsupported) && count($gatewayCurrencyUnsupported)) {
				$msg = JText::sprintf('FACTORY_ON_AFTER_CURRENCY_SAVED_MSG', implode(', ', array_keys(array_flip($gatewayCurrencyUnsupported))), implode(', ', array_keys($gatewayCurrencyUnsupported)));
			}
			if ($this->doTask == 'saveadd') $this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=currencies.newitem", $msg, 'warning');
			else
				$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=currencies.listing", $msg, 'warning');

		}

		/**
		 * saveadd
		 *
		 */
		public function saveadd()
		{
			self::save();
		}

		/**
		 * reorder
		 *
		 */
		public function reorder()
		{
			$db = JFactory::getDbo();
			$r  = JTheFactoryHelper::getRequestArray();

			foreach ($r as $k => $v) if (substr($k, 0, 6) == 'order_') {
				$id = substr($k, 6);
				$db->setQuery("update `#__" . APP_PREFIX . "_currency` set `ordering`='$v' where id=$id ");
				$db->execute();
			}


			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=currencies.listing");
		}

		/**
		 * RefreshConversions
		 *
		 */
		public function RefreshConversions()
		{
			$model     = JModelLegacy::getInstance('Currency', 'JTheFactoryModel');
			$currtable = JTable::getInstance('CurrencyTable', 'JTheFactory');

			$currencies       = $model->getCurrencyList();
			$default_currency = $model->getDefault();
			$results          = array();
			foreach ($currencies as $currency) {
				if ($currency->name == $default_currency) {
					$currtable->load($currency->id);
					$currtable->convert = 1;
					$currtable->store();
					$results[] = $currency->name . " ---> " . $default_currency . " = 1";
					continue;
				}
				$conversion = $model->getYahooCurrency($currency->name, $default_currency);

				if ($conversion === FALSE) {
					$results[] = JText::_("FACTORY_ERROR_CONVERTING") . " {$currency->name} --> $default_currency";
					continue;
				}

				$currtable->load($currency->id);
				$currtable->convert = $conversion;
				$currtable->store();
				$results[] = $currency->name . " ---> " . $default_currency . " = $conversion";

			}

			JTheFactoryEventsHelper::triggerEvent('onConversionRateChange');

			$view                   = $this->getView('currency', 'html');
			$view->default_currency = $default_currency;
			$view->results          = $results;

			$view->display("conversion");

		}
	}
