<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryBalancesController
	 */
	class JTheFactoryBalancesController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'Balances';
		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'Balances';

		/**
		 * JTheFactoryBalancesController constructor
		 */
		public function __construct()
		{
			parent::__construct('payments');
		}

		/**
		 * Listing
		 */
		public function Listing()
		{
			$model = JModelLegacy::getInstance('Balance', 'JTheFactoryModel');
			$rows  = $model->getBalancesList();

			$opts   = array();
			$opts[] = JHTML::_('select.option', '', JText::_("FACTORY_ALL"));
			$opts[] = JHTML::_('select.option', '1', JText::_("FACTORY_ALL_WITH_NON_ZERO_BALANCES"));
			$opts[] = JHTML::_('select.option', '2', JText::_("FACTORY_ALL_WITH_NEGATIVE_BALANCES"));
			if ($model->isWithdrawEnabled()) $opts[] = JHTML::_('select.option', '3', JText::_("FACTORY_ALL_WITH_REQUEST_WITHDRAWAL"));

			$filter_balances = JHtml::_('select.genericlist', $opts, 'filter_balances', "class='inputbox'", 'value', 'text', $model->get('filter_balances'));

			$view                  = $this->getView('balance');
			$view->withdraw        = $model->isWithdrawEnabled();
			$view->userbalances    = $rows;
			$view->filter_userid   = $model->get('filter_userid');
			$view->filter_balances = $filter_balances;
			$view->pagination      = $model->get('pagination');

			$view->display('list');
		}

		/**
		 * PayPal form displayed in admin payments
		 * as payment for withdrawal request
		 */
		public function withdrawForm()
		{
			// Push to template various variables
			$MyApp = JTheFactoryApplication::getInstance();
			// Get request variables
			$input   = JFactory::getApplication()->input;
			$gateway = $input->getString('gateway', 'pay_paypal');
			$userId  = $input->getInt('userId', 0);

			// TheFactory Config
			$cfg = JTheFactoryHelper::getConfig();

			// Load necessaries models
			$modelGateways = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$modelOrders   = JModelLegacy::getInstance('Orders', 'JTheFactoryModel');
			$model         = JModelLegacy::getInstance('Balance', 'JTheFactoryModel');

			// Load PayPal gateway
			$gw      = $modelGateways->getGatewayObject($gateway);
			$balance = $model->getUserBalance($userId);

			// Load user profile for receiver
			$receiver = new JTheFactoryUserProfile();
			$receiver->getUserProfile($userId);
			$payment_param_field = $MyApp->getIniValue("payment_param_field", "payments");
			$payment_param       = new JRegistry();
			$payment_param->set($gateway . '.' . $payment_param_field, $receiver->$payment_param_field);

			// Create New pending order
			$items              = new stdClass();
			$items->itemname    = 'withdraw';
			$items->itemdetails = JText::_('FACTORY_WITHDRAW_ORDER_ITEM_DETAILS');
			$items->iteminfo    = $receiver->userid;
			$items->price       = $balance->req_withdraw;
			$items->quantity    = 1;
			$items->currency    = $balance->currency;
			$items->params      = '';

			$order = $modelOrders->createNewOrder($items, $balance->req_withdraw, $balance->currency, $userId, 'P');

			// Create gateway links for return | notify | cancel
			$urls               = array();
			$urls['return_url'] = JRoute::_('index.php?option=' . APP_EXTENSION . '&task=balances.listing');
			$urls['notify_url'] = JURI::root() . 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.ipn&orderid=' . $order->id . '&gateway=' . $gw->name;
			$urls['cancel_url'] = JRoute::_('index.php?option=' . APP_EXTENSION . 'task=balances.listing');

			$formhtml = $gw->getPaymentForm($order, array($items), $urls, $receiver, NULL, $payment_param);
			// Set view and assign view variables
			$view               = $this->getView('balance');
			$view->withdrawForm = $formhtml;
			$view->cfg          = $cfg;
			$view->userbalance  = $balance;
			$view->user         = $receiver;

			$view->display('do_withdraw');
		}

	}
