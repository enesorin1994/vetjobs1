<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryPaymentsController
	 */
	class JTheFactoryPaymentsController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'Payments';
		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'Payments';

		/**
		 *
		 */
		public function __construct()
		{
			parent::__construct();
		}

		/**
		 * Listing
		 *
		 */
		public function Listing()
		{

			$app             = JFactory::getApplication();
			$cfg             = JTheFactoryHelper::getConfig();
			$filter_username = $app->getUserStateFromRequest(APP_EXTENSION . "_payments." . 'filter_username', 'filter_username', '', 'string');

			$model = JModelLegacy::getInstance('Payments', 'JTheFactoryModel');
			$rows  = $model->getPaymentsList($filter_username);

			$view                  = $this->getView('paylog');
			$view->cfg             = $cfg;
			$view->payments        = $rows;
			$view->pagination      = $model->get('pagination');
			$view->filter_username = $filter_username;

			$view->sortDirection = $model->getState('list.direction');
			$view->sortColumn    = $model->getState('list.ordering');

			// TODO Factory: J25 compatibility
			$jVersion = new JVersion();
			$jVersion->isCompatible('3.0') ? $view->display('list') : $view->display('list_joomla25legacy');

		}

		/**
		 * ViewDetails
		 *
		 */
		public function ViewDetails()
		{
			$cfg      = JTheFactoryHelper::getConfig();
			$paylog   = JTable::getInstance('PaymentLogTable', 'JTheFactory');
			$paylogid = JFactory::getApplication()->input->getInt('id', 0);

			if (!$paylogid || !$paylog->load($paylogid)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=payments.listing', JText::_("FACTORY_PAYMENT_DOES_NOT_EXIST"));

				return;
			}

			$user = JFactory::getUser($paylog->userid);

			$view         = $this->getView('paylog');
			$view->cfg    = $cfg;
			$view->user   = $user;
			$view->paylog = $paylog;

			$view->display('details');

		}

		/**
		 * Manual payment confirmation
		 */
		public function Confirm()
		{
			$paylogid = JFactory::getApplication()->input->get('id', array(), 'array');
			if (is_array($paylogid)) $paylogid = $paylogid[0];

			$paylog = JTable::getInstance('PaymentLogTable', 'JTheFactory');

			if (!$paylogid || !$paylog->load($paylogid)) {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=payments.listing', JText::_("FACTORY_PAYMENT_DOES_NOT_EXIST"));

				return;
			}

			if ($paylog->status == 'ok') {
				$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=payments.viewdetails&id=' . $paylogid, JText::_("FACTORY_PAYMENT_ALREADY_CONFIRMED"));

				return;
			}
			$paylog->status = 'ok';
			$paylog->store();

			$order = JTable::getInstance('OrdersTable', 'JTheFactory');

			if (!$paylog->orderid) {
				//Still no order attached to this payment?
				$error = JText::_('FACTORY_PAYMENT_DID_NOT_MATCH_AN_ORDER');
				JTheFactoryEventsHelper::triggerEvent('onPaymentIPNError', array($paylog, $error));
				exit;
			}

			if (!$order->load($paylog->orderid)) {
				//Still no order attached to this payment?
				$error = JText::_('FACTORY_PAYMENT_DID_NOT_MATCH_AN_ORDER');
				JTheFactoryEventsHelper::triggerEvent('onPaymentIPNError', array($paylog, $error));
				exit;
			}

			$date              = new JDate();
			$order->modifydate = $date->toSQL();

			if ($paylog->status == 'ok') $order->status = 'C';
			$order->paylogid = $paylog->id;
			$order->store();
			JTheFactoryEventsHelper::triggerEvent('onPaymentForOrder', array($paylog, $order));

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=payments.viewdetails&id=' . $paylogid, JText::_("FACTORY_PAYMENT_CONFIRMED"));


		}

		/**
		 * NewPayment
		 *
		 */
		public function NewPayment()
		{
			$view = $this->getView('paylog');
			$view->display('new');
		}

		/**
		 * SavePayment
		 *
		 */
		public function SavePayment()
		{
			$input         = JFactory::getApplication()->input;
			$userid        = $input->getInt('userid', 0);
			$amount        = $input->getFloat('amount', 0);
			$currency      = $input->getString('currency', '');
			$refnumber     = $input->getString('refnumber', '');
			$paymentstatus = $input->getString('paymentstatus', '');

			$paylog                 = JTable::getInstance('PaymentLogTable', 'JTheFactory');
			$date                   = new JDate();
			$paylog->date           = $date->toSQL();
			$paylog->amount         = $amount;
			$paylog->currency       = $currency;
			$paylog->refnumber      = $refnumber;
			$paylog->invoice        = -1;
			$paylog->status         = $paymentstatus;
			$paylog->userid         = $userid;
			$paylog->payment_method = 'manual_payment';
			$paylog->store();

			if ($paylog->status == 'ok') {
				$model = JModelLegacy::getInstance('Balance', 'JTheFactoryModel');
				$model->increaseBalance($amount, $userid);
			}
			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=payments.listing', JText::_("FACTORY_PAYMENT_SAVED_USER_ACCOUNT_CREDITED"));
		}
	}
