<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryPaymentsToolbar
	{
		/**
		 * display
		 *
		 * @static
		 *
		 * @param $task
		 */
		public static function display($task)
		{
			$appname = JTheFactoryApplication::getInstance()->getApplicationName();
			JToolBarHelper::title(JText::_('FACTORY_RECEIVED_PAYMENTS_LIST') . ' - ' . $appname, 'thefactory-payments');
			switch ($task) {
				default:
					break;
				case 'listing':
					JToolBarHelper::custom('payments.confirm', 'checkin', 'checkin', JText::_('FACTORY_CONFIRM_PAYMENT'), TRUE);
					JToolBarHelper::custom('payments.newpayment', 'new', 'new', JText::_('FACTORY_ADD_PAYMENT'), FALSE);
					break;
				case 'viewdetails':
					JToolBarHelper::title(JText::_('FACTORY_PAYMENT_DETAILS'));
					JToolBarHelper::custom('payments.confirm', 'checkin', 'checkin', JText::_('FACTORY_CONFIRM_PAYMENT'), FALSE);
					JToolBarHelper::custom('payments.listing', 'back', 'back', JText::_('FACTORY_BACK_TO_LISTING'), FALSE);
					break;
				case 'newpayment':
					JToolBarHelper::custom('payments.savepayment', 'save', 'save', JText::_('FACTORY_SAVE_PAYMENT'), FALSE);
					JToolBarHelper::custom('payments.listing', 'back', 'back', JText::_('FACTORY_CANCEL'), FALSE);
					break;
			}

		}
	}
