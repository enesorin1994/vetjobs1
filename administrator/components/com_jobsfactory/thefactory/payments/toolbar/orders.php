<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryOrdersToolbar
	{
		/**
		 * display
		 *
		 * @static
		 *
		 * @param $task
		 */
		public static function display($task)
		{
			$appname = JTheFactoryApplication::getInstance()->getApplicationName();
			JToolBarHelper::title(JText::_('FACTORY_CURRENT_ORDERS_LIST') . ' - ' . $appname, 'thefactory-orders');
			switch ($task) {
				default:
					break;
				case 'listing':
					JToolBarHelper::custom('orders.confirm', 'checkin', 'checkin', JText::_('FACTORY_CONFIRM_ORDER'), TRUE);
					JToolBarHelper::custom('orders.cancel', 'cancel', 'cancel', JText::_('FACTORY_CANCEL_ORDER'), TRUE);
					break;
				case 'viewdetails':
					JToolBarHelper::title(JText::_('FACTORY_ORDER_DETAIL'));
					JToolBarHelper::custom('orders.confirm', 'checkin', 'checkin', JText::_('FACTORY_CONFIRM_ORDER'), FALSE);
					JToolBarHelper::custom('orders.cancel', 'cancel', 'cancel', JText::_('FACTORY_CANCEL_ORDER'), FALSE);
					JToolBarHelper::custom('orders.listing', 'back', 'back', JText::_('FACTORY_BACK_TO_LISTING'), FALSE);
					break;
			}

		}
	}
