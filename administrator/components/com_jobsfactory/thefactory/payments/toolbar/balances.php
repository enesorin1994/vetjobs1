<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryBalancesToolbar
	{
		/**
		 * display
		 *
		 * @static
		 *
		 * @param $task
		 */
		public static function display($task)
		{
			$appname = JTheFactoryApplication::getInstance()->getApplicationName();
			JToolBarHelper::title(JText::_('FACTORY_USER_BALANCES') . ' - ' . $appname, 'thefactory-balances');
			switch ($task) {
				default:
				case 'listing':
					JToolBarHelper::custom('payments.newpayment', 'new', 'new', JText::_('FACTORY_ADD_FUNDS'), FALSE);
					break;
				case 'withdrawForm':
					JToolBarHelper::title(JText::_('FACTORY_USER_BALANCES') . ': <span style=" color: #146295;font-size:12px;">[' . JText::_('FACTORY_PAY_REQUESTED_WITHDRAW_AMOUNT') . ']</span>');
					break;
			}
		}
	}
