<?php
/**------------------------------------------------------------------------
 * thefactory - The Factory Class Library - v 3.0.0
 * ------------------------------------------------------------------------
 *
 * @author    thePHPfactory
 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *            Websites: http://www.thePHPfactory.com
 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build     : 01/04/2012
 * @package   : thefactory
 * @subpackage: payments
 *            -------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');
require_once(realpath(dirname(__FILE__) . '/../../../classes/gateways.php'));
use Joomla\Registry\Registry as JRegistry;


/**
 * Class Pay_Paypal Gateways
 */
class Pay_Mollie extends TheFactoryPaymentGateway
{
	/**
	 * @var string
	 */
	var $name = 'pay_mollie';
	/**
	 * @var string
	 */
	var $fullname = 'Mollie Payment Method';
	/**
	 * @var string
	 */
	var $action = '';
	/**
	 * Supported currencies list
	 *
	 * @var array
	 */
	protected $_supportedCurrencies = array(
		'EUR'
	);

	/**
	 * PayPal constructor
	 */
	public function __construct()
	{
		parent::__construct();

		// Load the Mollie API
		require_once JPATH_COMPONENT_ADMINISTRATOR .'/thefactory/library/Mollie/API/Autoloader.php';
		$this->mollie = new Mollie_API_Client;

		$modelGateways = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
		$pp_params     = $modelGateways->loadGatewayParams($this->name);
		// Set API key.
		$key = $pp_params->get('api_key', null);

		if(is_null($key))
		{
			echo 'No API key defined for Mollie Payment Gateway';
			return;
		}
		else
		{
			$this->mollie->setApiKey($key);
		}

		//$model  = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
		//$params = $model->loadGatewayParams($this->name);

	}

	/**
	 * Getter for PayPal supported currencies
	 *
	 * @return array
	 */
	public function getSupportedCurrencies()
	{
		return $this->_supportedCurrencies;
	}


	/**
	 * getPaymentForm
	 *
	 * @param      $order
	 * @param      $items
	 * @param      $urls
	 * @param null $shipping
	 * @param null $tax
	 *
	 * @return mixed|string
	 */
	public function getPaymentForm($order, $items, $urls, $shipping = NULL, $tax = NULL, $params = NULL)
	{
		$Itemid = JFactory::getApplication()->input->getInt('Itemid', 0);

		$result = "<form method='get' action='index.php'>
				<input type='hidden' name='option' value='" . APP_EXTENSION . "' />
				<input type='hidden' name='task' value='orderprocessor.gateway' />
				<input type='hidden' name='orderid' value='{$order->id}' />
				<input type='hidden' name='gateway' value='{$this->name}' />
				<input type='hidden' name='Itemid' value='$Itemid' />";

		$result .= "<input type='image' src='" . $this->getLogo() . "' name='submit' alt='" . JText::_("FACTORY_BUY_NOW") . "' style='margin-left: 30px;' border='0'>
		  				</form>";

		return $result;
	}

	public function processTask()
	{
		$input   = JFactory::getApplication()->input;
		$task2   = $input->getString('task2', 'step1');
		$Itemid  = $input->getInt('Itemid', 0);;
		$orderid = $input->getInt('orderid', 0);

		// Check if order exists
		$order = JTable::getInstance('OrdersTable', 'JTheFactory');

		if (!$order->load($orderid)) {
			$app = JFactory::getApplication();
			$app->redirect('index.php?option=' . APP_EXTENSION . '&Itemid=' . $Itemid, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

			return;
		}

		// Check if currency is valid for Mollie
		if($order->order_currency != 'EUR')
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?option=' . APP_EXTENSION . '&Itemid=' . $Itemid, JText::_("FACTORY_CURRENCY_NOT_SUPPORTED"));

			return;
		}


		$redirectUrl = JUri::base() . 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $order->id . '&Itemid=' . $Itemid;
		$ipn = JUri::base() . 'index.php?option=' . APP_EXTENSION . '&task=orderprocessor.ipn&gateway=pay_mollie&orderid=' . $order->id;

		//var_dump($ipn);exit;

		// Create Mollie Payment
		$payment = $this->mollie->payments->create(array(
			"amount"      => $order->order_total,
			"description" => "Auction Payment",
			"redirectUrl" => $redirectUrl,
			"metadata"    => array('order_id' => $orderid),
			"webhookUrl" => $ipn
		));

		if($payment)
		{
			// Build Payment Log(Payments) entry
			$paylog               = JTable::getInstance('PaymentLogTable', 'JTheFactory');
			$date                 = new JDate();
			$paylog->date         = $date->toSQL();
			$paylog->amount       = $order->order_total;
			$paylog->currency     = $order->order_currency;

			// Mollie Unique ID used for validating payment
			$paylog->refnumber    = $payment->id;

			$paylog->invoice      = $orderid;
			$paylog->ipn_response = print_r($_REQUEST, TRUE);
			$paylog->ipn_ip       = $_SERVER['REMOTE_ADDR'];
			$paylog->status       = 'manual_check';
			$paylog->userid       = $order->userid;
			$paylog->orderid      = $order->id;

			if (property_exists($paylog, 'gatewayid')) {
				$paylog->gatewayid = isset($this->id) ? $this->id : 0;;
			}

			$paylog->payment_method = $this->name;
			$paylog->store();

			// Update Order(Orders)
			$order->paylogid = $paylog->id;
			$order->store();

			header("Location: " . $payment->getPaymentUrl());
			exit;
		}
	}

	/**
	 * Process IPN
	 *
	 * @return mixed
	 */
	public function processIPN()
	{
		// Get mollie payment
		$input   = JFactory::getApplication()->input;
		$payment    = $this->mollie->payments->get($input->get('id'));

		// Get order
		/*$order = JTable::getInstance('OrdersTable', 'JTheFactory');

		if (!$order->load($payment->metadata->order_id)) {
			$app = JFactory::getApplication();
			$app->redirect('index.php?option=' . APP_EXTENSION, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

			return false;
		}*/

		// Get pay log
		$paylog = JTable::getInstance('PaymentLogTable', 'JTheFactory');
		$paylog->load(array('refnumber' => $payment->id));

		//echo '<pre>'; var_dump($payment, $payment->isPaid());exit;

		// Check if gateway payment is paid
		if ($payment->isPaid())
		{
			$paylog->status = 'ok';

			// Confirm order
			//$order->paylogid = $paylog->id;
			//$order->status = 'C';

			//JTheFactoryEventsHelper::triggerEvent('onPaymentForOrder', array($paylog, $order));

		}
		elseif (!$payment->isOpen())
		{
			// Cancel payment
			$paylog->status = 'cancelled';
			//$order->status = 'X';
			//$error = 'Payment is no longer valid!';
			//JTheFactoryEventsHelper::triggerEvent('onPaymentIPNError', array($paylog, $error));

		}

		// Update component payment log
		$date                   = new JDate();
		$paylog->date           = $date->toSQL();
		$paylog->amount         = $payment->amount;

		$paylog->ipn_response   = print_r($_REQUEST, TRUE);
		$paylog->ipn_ip         = $_SERVER['REMOTE_ADDR'];
		$paylog->userid         = NULL;
		//$paylog->orderid        = $payment->metadata->order_id;
		$paylog->payment_method = $this->name;

		$paylog->store();
		//$order->store();

		return $paylog;
	}
} // End Class


