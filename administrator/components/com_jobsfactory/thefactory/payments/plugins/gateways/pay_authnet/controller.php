<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');
	require_once(realpath(dirname(__FILE__) . '/../../../classes/gateways.php'));

	JHtml::_('behavior.tooltip');

	/**
	 * Class Pay_Authnet
	 */
	class Pay_Authnet extends TheFactoryPaymentGateway
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'pay_authnet';
		/**
		 * fullname
		 *
		 * @var string
		 */
		var $fullname = 'Authorize net Payment Gateway';
		/**
		 * loginID
		 *
		 * @var
		 */
		protected $loginID;
		/**
		 * transactionKey
		 *
		 * @var
		 */
		protected $transactionKey;
		/**
		 * gatewayUrl
		 *
		 * @var string
		 */
		protected $gatewayUrl;
		/**
		 * testMode
		 *
		 * @var
		 */
		protected $testMode;

		/**
		 *
		 */
		public function __construct()
		{
			parent::__construct();

			$model  = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params = $model->loadGatewayParams($this->name);

			$this->loginID        = $params->get('loginID');
			$this->testMode       = $params->get('sandboxMode');
			$this->transactionKey = $params->get('transactionKey');

			$this->gatewayUrl = 'https://secure.authorize.net/gateway/transact.dll';
			if ($params->get('sandboxMode')) {
				$this->gatewayUrl = 'https://test.authorize.net/gateway/transact.dll';
			}

		}

		/**
		 * Supported currencies list
		 *
		 * @var array
		 */
		protected $_supportedCurrencies = array(
			'AUD', // Australian Dollar (A $)
			'CAD', // Canadian Dollar (C $)
			'EUR', // Euro (€)
			'GBP', // British Pound (£)
			'USD', // U.S. Dollar ($)
			'NZD' // New Zealand Dollar ($)
		);

		/**
		 * Getter for Pay Example supported currencies
		 *
		 * @return array
		 */
		public function getSupportedCurrencies()
		{
			return $this->_supportedCurrencies;
		}

		/**
		 * getPaymentForm
		 *
		 * @param  object   $order JTheFactoryOrdersTable
		 * @param  array    $items
		 * @param           $urls
		 * @param null      $shipping
		 * @param null      $tax
		 *
		 * @return string
		 */
		public function getPaymentForm($order, $items, $urls, $shipping = NULL, $tax = NULL, $params = NULL)
		{
			// Load gateway parameters
			$model  = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params = $model->loadGatewayParams($this->name);

			// Set fingerprint (Security mode)

			// a sequence number is randomly generated
			$sequence = rand(1, 1000);
			// a timestamp is generated
			$timeStamp = time();

			// The following lines generate the SIM fingerprint.  PHP versions 5.1.2 and
			// newer have the necessary hmac function built in.  For older versions, it
			// will try to use the mhash library.

			if (phpversion() >= '5.1.2') {
				$fingerprint = hash_hmac("md5", $this->loginID . "^" . $sequence . "^" . $timeStamp . "^" . $order->order_total . "^" . $order->order_currency, $this->transactionKey);
			} else {
				$fingerprint = bin2hex(mhash(MHASH_MD5, $this->loginID . "^" . $sequence . "^" . $timeStamp . "^" . $order->order_total . "^" . $order->order_currency, $this->transactionKey));
			}

			$loggedUser     = JFactory::getUser();
			$submitImageUrl = JURI::root() . "administrator/components/" . APP_EXTENSION . "/thefactory/payments/plugins/gateways/" . $this->name . "/logo.png";

			// Create the HTML form containing necessary SIM post values
			$result = "<form target='_blank' method='post' action='" . $this->gatewayUrl . "'>";
			// Additional fields can be added here as outlined in the SIM integration
			// guide at: http://developer.authorize.net

			// Set used version
			$result .= "<input type = 'hidden' name = 'x_version' value = '3.1' />";
			// Order
			$result .= "<input type = 'hidden' name = 'x_amount' value = '" . $order->order_total . "' />";
			$result .= "<input type = 'hidden' name = 'x_currency_code' value = '" . $order->order_currency . "' />";
			$result .= "<input type = 'hidden' name = 'x_description' value = '" . $items[0]->itemdetails . "' />";
			$result .= "<input type = 'hidden' name = 'x_invoice_num' value = '" . $order->id . "' />";
			// Customer
			$result .= "<input type = 'hidden' name = 'x_first_name' value = '" . $loggedUser->name . "' />";
			$result .= "<input type = 'hidden' name = 'x_email' value = '" . $loggedUser->email . "' />";
			$result .= "<input type = 'hidden' name = 'x_cust_id' value = '" . $loggedUser->id . "' />";
			// Fingerprint security
			$result .= "<input type = 'hidden' name = 'x_login' value = '" . $this->loginID . "' />";
			$result .= "<input type = 'hidden' name = 'x_fp_sequence' value = '" . $sequence . "' />";
			$result .= "<input type = 'hidden' name = 'x_fp_timestamp' value = '" . $timeStamp . "' />";
			$result .= "<input type = 'hidden' name = 'x_fp_hash' value = '" . $fingerprint . "' />";
			// isSandbox?
			$result .= "<input type = 'hidden' name = 'x_test_request' value = '" . $this->testMode . "' />";
			// Gateway form customization
			$result .= "<input type = 'hidden' name = 'x_header_html_payment_form' value = '" . $params->get('header_html_payment_form') . "' />";
			$result .= "<input type = 'hidden' name = 'x_footer_html_payment_form' value = '" . $params->get('footer_html_payment_form') . "' />";
			$result .= "<input type = 'hidden' name = 'x_header2_html_payment_form' value = '" . $params->get('header2_html_payment_form') . "' />";
			$result .= "<input type = 'hidden' name = 'x_footer2_html_payment_form' value = '" . $params->get('footer2_html_payment_form') . "' />";
			$result .= "<input type = 'hidden' name = 'x_color_background' value = '" . $params->get('color_background') . "' />";
			$result .= "<input type = 'hidden' name = 'x_color_link' value = '" . $params->get('color_link') . "' />";
			$result .= "<input type = 'hidden' name = 'x_color_text' value = '" . $params->get('color_text') . "' />";
			$result .= "<input type = 'hidden' name = 'x_logo_url' value = '" . $params->get('logo_url') . "' />";
			$result .= "<input type = 'hidden' name = 'x_background_url' value = '" . $params->get('background_url') . "' />";
			$result .= "<input type = 'hidden' name = 'x_cancel_url_text' value = '" . $params->get('cancel_url_text') . "' />";
			// Url-s
			$result .= "<input type = 'hidden' name = 'x_relay_response' value = '1' />";
			$result .= "<input type = 'hidden' name = 'x_relay_url' value = '" . $urls['notify_url'] . "' />";
			$result .= "<input type = 'hidden' name = 'x_cancel_url' value = '" . $urls['cancel_url'] . "' />";
			// Submit button
			$result .= "<input type = 'hidden' name = 'x_show_form' value = 'PAYMENT_FORM' />";
			$result .= "<input type = 'image' src = '" . $submitImageUrl . "' alt = '" . JText::_("FACTORY_BUY_NOW") . "' />";
			$result .= "</form>";


			return $result;
		}

		/**
		 * processIPN
		 *
		 * @return mixed
		 */
		function processIPN()
		{
			/* @var $input JInput */
			$input = JFactory::getApplication()->input;

			$paylog = JTable::getInstance('PaymentLogTable', 'JTheFactory');

			$ordersTable = JTable::getInstance('OrdersTable', 'JTheFactory');
			$ordersTable->load($input->getInt('x_invoice_num', 0));

			// Paylog data here
			$date                   = new JDate();
			$paylog->date           = $date->toSQL();
			$paylog->amount         = $input->getFloat('x_amount', 0);
			$paylog->currency       = $ordersTable->get('order_currency');
			$paylog->refnumber      = $input->getString('x_invoice_num', '');
			$paylog->invoice        = $input->getInt('x_invoice_num', 0);
			$paylog->ipn_response   = print_r($_REQUEST, TRUE);
			$paylog->ipn_ip         = $_SERVER['REMOTE_ADDR'];
			$paylog->status         = 'error';
			$paylog->userid         = NULL;
			$paylog->orderid        = $input->getInt('x_invoice_num', 0);
			$paylog->payment_method = $this->name;

			/**
			 *  Response code
			 *
			 *  1 = Approved
			 *    2 = Declined
			 *    3 = Error
			 *    4 = Held for Review
			 * */
			$response_code = $input->getInt('x_response_code', '');

			switch ($response_code) {
				case 1:
					$paylog->status = 'ok';
					break;
				case 2:
				case 3:
					$paylog->status = 'error';
					break;
				default:
				case 4:
					$paylog->status = 'manual_check';
					break;
			}

			$paylog->store();

			return $paylog;
		}
	}
