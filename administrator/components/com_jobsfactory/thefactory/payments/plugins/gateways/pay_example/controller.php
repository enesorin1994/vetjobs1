<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	require_once(realpath(dirname(__FILE__) . '/../../../classes/gateways.php'));

	/**
	 * Class Pay_Example
	 */
	class Pay_Example extends TheFactoryPaymentGateway
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'pay_paypal';
		/**
		 * fullname
		 *
		 * @var string
		 */
		var $fullname = 'Paypal Payment Gateway';
		/**
		 * Supported currencies list
		 *
		 * @var array
		 */
		protected $_supportedCurrencies = array();

		/**
		 * Getter for Pay Example supported currencies
		 *
		 * @return array
		 */
		public function getSupportedCurrencies()
		{
			return $this->_supportedCurrencies;
		}

		/**
		 * getPaymentForm
		 *
		 * @param      $order
		 * @param      $items
		 * @param      $urls
		 * @param null $shipping
		 * @param null $tax
		 *
		 * @return string
		 */
		public function getPaymentForm($order, $items, $urls, $shipping = NULL, $tax = NULL, $params = NULL)
		{
			$model  = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params = $model->loadGatewayParams($this->name);
			/*
			 * use $params to get the stored in the database
			 */
			$result = 'your html for the form payment here';

			return $result;
		}

		/**
		 * processIPN
		 *
		 * @return mixed
		 */
		public function processIPN()
		{
			$model  = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params = $model->loadGatewayParams($this->name);
			/*
			 * use $params to get the stored in the database
			 */


			$paylog = JTable::getInstance('PaymentLogTable', 'JTheFactory');
			//paylog data here

			//validate IPN

			$paylog->store();

			return $paylog;
		}
	}


