<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	require_once(realpath(dirname(__FILE__) . '/../../../classes/gateways.php'));

	/**
	 * Class Pay_Banktransfer
	 */
	class Pay_Banktransfer extends TheFactoryPaymentGateway
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'pay_banktransfer';
		/**
		 * fullname
		 *
		 * @var string
		 */
		var $fullname = 'Bank Transfer Payment Method';
		/**
		 * Supported currencies list
		 *
		 * @var array
		 */
		protected $_supportedCurrencies = array();

		/**
		 * Getter for Pay Example supported currencies
		 *
		 * @return array
		 */
		public function getSupportedCurrencies()
		{
			return $this->_supportedCurrencies;
		}

		/**
		 * getPaymentForm
		 *
		 * @param      $order
		 * @param      $items
		 * @param      $urls
		 * @param null $shipping
		 * @param null $tax
		 *
		 * @return mixed|string
		 */
		public function getPaymentForm($order, $items, $urls, $shipping = NULL, $tax = NULL, $params = NULL)
		{
			$Itemid = JFactory::getApplication()->input->getInt('Itemid', 0);


			$result = "<form method='get' action='index.php'>
				<input type='hidden' name='option' value='" . APP_EXTENSION . "' />
				<input type='hidden' name='task' value='orderprocessor.gateway' />
				<input type='hidden' name='orderid' value='{$order->id}' />
				<input type='hidden' name='gateway' value='{$this->name}' />
				<input type='hidden' name='Itemid' value='$Itemid' />

	";


			$result .= "<input type='image' src='" . $this->getLogo() . "' name='submit' alt='" . JText::_("FACTORY_BUY_NOW") . "' style='margin-left: 30px;' border='0'>
		  </form>
	";

			return $result;

		}

		/**
		 * processTask
		 *
		 * @return mixed
		 */
		public function processTask()
		{
			$input   = JFactory::getApplication()->input;
			$task2   = $input->getString('task2', 'step1');
			$Itemid  = $input->getInt('Itemid', 0);
			$orderid = $input->getInt('orderid', 0);

			if ($task2 == 'step2') {
				$this->savePayment();
				$app = JFactory::getApplication();
				$app->redirect('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.returning&orderid=' . $orderid . '&Itemid=' . $Itemid . '&gateway=' . $this->name);

				return;
			} else {
				$this->showBankForm();
			}
		}

		/**
		 * savePayment
		 *
		 * @return mixed
		 */
		public function savePayment()
		{
			$input   = JFactory::getApplication()->input;
			$Itemid  = $input->getInt('Itemid', 0);
			$orderid = $input->getInt('orderid', 0);

			$order = JTable::getInstance('OrdersTable', 'JTheFactory');

			if (!$order->load($orderid)) {
				$app = JFactory::getApplication();
				$app->redirect('index.php?option=' . APP_EXTENSION . '&Itemid=' . $Itemid, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}

			$paylog               = JTable::getInstance('PaymentLogTable', 'JTheFactory');
			$date                 = new JDate();
			$paylog->date         = $date->toSQL();
			$paylog->amount       = $order->order_total;
			$paylog->currency     = $order->order_currency;
			$paylog->refnumber    = $input->getString('customer_note', '');
			$paylog->invoice      = $orderid;
			$paylog->ipn_response = print_r($_REQUEST, TRUE);
			$paylog->ipn_ip       = $_SERVER['REMOTE_ADDR'];
			$paylog->status       = 'manual_check';
			$paylog->userid       = $order->userid;
			$paylog->orderid      = $order->id;

			if (property_exists($paylog, 'gatewayid')) {
				$paylog->gatewayid = isset($this->id) ? $this->id : 0;;
			}

			$paylog->payment_method = $this->name;
			$paylog->store();

			$order->paylogid = $paylog->id;
			$order->store();
		}

		/**
		 * showBankForm
		 *
		 * @return mixed
		 */
		public function showBankForm()
		{
			$input   = JFactory::getApplication()->input;
			$Itemid  = $input->getInt('Itemid', 0);
			$orderid = $input->getInt('orderid', 0);

			$model  = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params = $model->loadGatewayParams($this->name);
			$order  = JTable::getInstance('OrdersTable', 'JTheFactory');
			if (!$order->load($orderid)) {
				$app = JFactory::getApplication();
				$app->redirect('index.php?option=' . APP_EXTENSION . '&Itemid=' . $Itemid, JText::_("FACTORY_ORDER_DOES_NOT_EXIST"));

				return;
			}
			$info = $params->get('bank_info', '');
			// Set string to be translatable if admin input start with COM_[COMPONENT_NAME]_
			if (FALSE !== strpos(strip_tags($info), strtoupper(APP_EXTENSION . '_'))) {
				$info = JText::_(strip_tags($info));
			}

			$doc = JFactory::getDocument();
			$doc->addStyleDeclaration("
			#banktransfer-payment-plugin-wrapper{
				width:100%;
				border:none;
			}
			#banktransfer-payment-plugin-wrapper tr,
			#banktransfer-payment-plugin-wrapper td {
				border:none;
			}
			#banktransfer-payment-plugin-wrapper textarea{
				padding: 10px;
				color:#555555;
				font-family:'Trebuchet MS', Arial, sans-serif;
				font-size:11px;
			}
			#banktransfer-payment-plugin-wrapper .pay-info span{
				display:inline-block;
				font-size:11px;
				color:#555555;
				margin-bottom:25px;
			}

			#banktransfer-payment-plugin-wrapper .button{
				margin:10px 10px 0 0;
				background: url('images/buttons_bg.png') transparent !important; /* Remove left arrow */
				border: 1px solid #aaaaaa !important;
				color: #494949 !important;
				padding:10px 20px 10px;
			}
		");
			echo "<form method='post' action='index.php'>
				<table id='banktransfer-payment-plugin-wrapper''>
					<tr style:'border:none;'>
						<td><h2>" . JText::_("FACTORY_ORDER") . " #{$order->id} - {$order->order_total} {$order->order_currency}</h2></td>
					</tr>
					<tr>
						<td class='pay-info'>
							<strong>" . JText::_("FACTORY_PAYMENT_INFORMATION") . "</strong><br />
							<span>" . $info . "</span>
						</td>
					</tr>
					<tr>
						<td>
							<strong>" . JText::_("FACTORY_CUSTOMER_NOTE") . "</strong><br />
							<textarea name='customer_note' style='width:90%; height:200px;resize:vertical;'></textarea>
						</td>
					</tr>
					<tr>
						<td>
						<input type='submit' class='button btn' value='" . JText::_("FACTORY_BUY_NOW") . "' />
						" . JHtml::_('link', JRoute::_('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $order->id), JText::_('FACTORY_BUY_LATER')) . "
						</td>
					</tr>
				</table>
				<input type='hidden' name='option' value='" . APP_EXTENSION . "' />
				<input type='hidden' name='task' value='orderprocessor.gateway' />
				<input type='hidden' name='task2' value='step2' />
				<input type='hidden' name='orderid' value='{$order->id}' />
				<input type='hidden' name='gateway' value='{$this->name}' />
				<input type='hidden' name='Itemid' value='$Itemid' />
			</form>
	";

		}
	} // End Class
