<?php

	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/


	defined('_JEXEC') or die('Restricted access');


	require_once(realpath(dirname(__FILE__) . '/../../../classes/gateways.php'));


	/**
	 * Class Pay_epay
	 */
	class Pay_epay extends TheFactoryPaymentGateway

	{

		/**
		 * name
		 *
		 * @var string
		 */

		var $name = 'pay_epay';

		/**
		 * fullname
		 *
		 * @var string
		 */

		var $fullname = 'ePay Payment Gateway';

		/**
		 * Supported currencies list
		 *
		 * @var array
		 */

		protected $_supportedCurrencies = array(

			'EUR', // Euro
			'USD', // U.S. Dollar
			'GBP', // British Pound
			'HKD', // Hong Kong Dollar
			'SGD', // Singapore Dollar
			'JPY', // Japanese Yen
			'CAD', // Canadian Dollar
			'AUD', // Australian Dollar
			'CHF', // Swiss Franc
			'DKK', // Danish Krone
			'SEK', // Swedish Krona
			'NOK', // Norwegian Krone
			'ILS', // Israeli Shekel
			'MYR', // Malaysian Ringgit
			'NZD', // New Zealand Dollar
			'TRY', // New Turkish Lira
			'AED', // Utd. Arab Emir. Dirham
			'MAD', // Moroccan Dirham
			'QAR', // Qatari Rial
			'SAR', // Saudi Riyal
			'TWD', // Taiwan Dollar
			'THB', // Thailand Baht
			'CZK', // Czech Koruna
			'HUF', // Hungarian Forint
			'SKK', // Slovakian Koruna
			'EEK', // Estonian Kroon
			'BGN', // Bulgarian Leva
			'PLN', // Polish Zloty
			'ISK', // Iceland Krona
			'INR', // Indian Rupee
			'LVL', // Latvian Lat
			'KRW', // South-Korean Won
			'ZAR', // South-African Rand
			'RON', // Romanian Leu New
			'HRK', // Croatian Kuna
			'LTL', // Lithuanian Litas
			'JOD', // Jordanian Dinar
			'OMR', // Omani Rial
			'RSD', // Serbian dinar
			'TND', // Tunisian Dinar
		);


		/**
		 * Getter for epay payment supported currencies
		 *
		 * @return array
		 */

		public function getSupportedCurrencies()

		{

			return $this->_supportedCurrencies;
		}


		/**
		 * getPaymentForm
		 *
		 * @param      $order
		 * @param      $items
		 * @param      $urls
		 * @param null $shipping
		 * @param null $tax
		 *
		 * @return mixed|string
		 */

		public function getPaymentForm($order, $items, $urls, $shipping = NULL, $tax = NULL)

		{

			$model          = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params         = $model->loadGatewayParams($this->name);
			$merchantnumber = $params->get('merchantnumber', '');

			$result = "<form name='epayForm' action='https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/Default.aspx' method='post' >";
			$result .= "<input type='hidden' name='merchantnumber' value='$merchantnumber'>";
			$result .= "<input type='hidden' name='windowstate' value='3'>";
			$result .= "<input type='hidden' name='paymenttype ' value='1,3,4'>";
			$result .= "<input type='hidden' name='orderid' value='{$order->id}'>";
			$result .= "<input type='hidden' name='accepturl' value='" . $urls['return_url'] . "'>";
			$result .= "<input type='hidden' name='cancelurl' value='" . $urls['cancel_url'] . "'>";
			$result .= "<input type='hidden' name='callbackurl' value='" . $urls['notify_url'] . "'>";
			$result .= "<input type='hidden' name='amount' value='{$order->order_total}'>";
			$result .= "<input type='hidden' name='currency' value='{$order->order_currency}'>";
			$result .= "<input type='image' src='http://www.epay.dk/images/layout/logo.gif' name='submit' alt='" . JText::_("FACTORY_BUY_NOW") . "' style='margin-left: 30px;'>";
			$result .= "</form>";

			return $result;
		}


		/**
		 * processIPN
		 *
		 * @return mixed
		 */

		public function processIPN()

		{
			exit;

			$input = JFactory::getApplication()->input;

			$model          = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params         = $model->loadGatewayParams($this->name);
			$paypal_address = $params->get('paypalemail', '');

			$paylog = JTable::getInstance('PaymentLogTable', 'JTheFactory');

			$date                   = new JDate();
			$paylog->date           = $date->toSQL();
			$paylog->amount         = $input->getString('amount', '');
			$paylog->currency       = $input->getString('currency', '');
			$paylog->refnumber      = $input->getString('mb_transaction_id', '');
			$paylog->invoice        = $input->getString('transaction_id', '');
			$paylog->ipn_response   = print_r($_REQUEST, TRUE);
			$paylog->ipn_ip         = $_SERVER['REMOTE_ADDR'];
			$paylog->status         = 'error';
			$paylog->userid         = NULL;
			$paylog->orderid        = $input->getString('mb_transaction_id', '');
			$paylog->payment_method = $this->name;

			$receiver_email = $input->getString('pay_to_email', '');
			$payment_status = $input->getString('status', '');

			switch ($payment_status) {
				case "2":
					$paylog->status = 'ok';
					break;
				case "-1":
				case "-2":
				case "-3":
					$paylog->status = 'error';
					break;
				default:
				case "0":
					$paylog->status = 'manual_check';
					break;
			}


			if ($receiver_email <> $paypal_address) {
				$paylog->status = 'error';
			}

			$paylog->store();

			return $paylog;
		}


	} // End Class

