<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	require_once(realpath(dirname(__FILE__) . '/../../../classes/gateways.php'));

	/**
	 * Class Pay_2checkout
	 */
	class Pay_2checkout extends TheFactoryPaymentGateway
	{
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'pay_2checkout';
		/**
		 * fullname
		 *
		 * @var string
		 */
		var $fullname = '2checkout Payment Gateway';
		/**
		 * Supported currencies list
		 *
		 * @var array
		 */
		protected $_supportedCurrencies = array();

		/**
		 * Getter for Pay Example supported currencies
		 *
		 * @return array
		 */
		public function getSupportedCurrencies()
		{
			return $this->_supportedCurrencies;
		}

		/**
		 * showAdminForm
		 */
		public function  showAdminForm()
		{
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_("FACTORY_NOTE_2CHECKOUT_DOES_NOT_SUPPORT"));
			parent::showAdminForm();
		}

		/**
		 * getPaymentForm
		 *
		 * @param      $order
		 * @param      $items
		 * @param      $urls
		 * @param null $shipping
		 * @param null $tax
		 *
		 * @return mixed|string
		 */
		public function getPaymentForm($order, $items, $urls, $shipping = NULL, $tax = NULL, $params = NULL)
		{

			$model  = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params = $model->loadGatewayParams($this->name);

			$test_mode = $params->get('test_mode', 0);
			$x_login   = $params->get('x_login', '');

			$result = "<form name='check2outForm' action='https://www.2checkout.com/checkout/purchase' method='post'>";
			$result .= "<input type='hidden' name='x_login' value='$x_login' />";
			if ($test_mode) $result .= "<input type='hidden' name='demo' value='Y' />";
			$result .= "<input type='hidden' name='x_email_merchant' value='TRUE' />";
			$result .= "<input type='hidden' name='list_currency' value='{$order->order_currency}' />
			<input type='hidden' name='x_invoice_num' value='{$order->id}' />
			<input type='hidden' name='merchant_order_id' value='{$order->id}' />
			<input type='hidden' name='x_receipt_link_url' value='" . $urls['notify_url'] . "' />";

			$result .= "<input type='hidden' name='x_amount' value='{$order->order_total}' />";
			$result .= "<input type='submit' name='submit' value='Pay 2checkout' class='btn' />";
			$result .= "</form>";

			return $result;

		}

		/**
		 * processIPN
		 *
		 * @return mixed
		 */
		public function processIPN()
		{
			$input = JFactory::getApplication()->input;

			$paylog                 = JTable::getInstance('PaymentLogTable', 'JTheFactory');
			$date                   = new JDate();
			$paylog->date           = $date->toSQL();
			$paylog->amount         = $input->getFloat('x_amount', 0);
			$paylog->currency       = '';
			$paylog->refnumber      = $input->getString('x_trans_id', '');
			$paylog->invoice        = $input->getString('x_invoice_num', '');
			$paylog->ipn_response   = print_r($_REQUEST, TRUE);
			$paylog->ipn_ip         = $_SERVER['REMOTE_ADDR'];
			$paylog->status         = 'error';
			$paylog->userid         = NULL;
			$paylog->orderid        = $input->getString('x_invoice_num', '');
			$paylog->payment_method = $this->name;

			$payment_status = $input->getString('x_2checked', '');
			$paylog->status = ($payment_status == 'Y') ? 'ok' : 'error';

			$paylog->store();

			return $paylog;

		}

	} // End Class
