<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');
	require_once(realpath(dirname(__FILE__) . '/../../../classes/gateways.php'));
	use Joomla\Registry\Registry as JRegistry;


	/**
	 * Class Pay_Paypal Gateways
	 */
	class Pay_Paypal extends TheFactoryPaymentGateway
	{
		/**
		 * @var string
		 */
		var $name = 'pay_paypal';
		/**
		 * @var string
		 */
		var $fullname = 'Paypal Payment Method';
		/**
		 * @var string
		 */
		var $action = '';
		/**
		 * Supported currencies list
		 *
		 * @var array
		 */
		protected $_supportedCurrencies = array(
			'AUD', // Australian Dollar (A $)
			'CAD', // Canadian Dollar (C $)
			'EUR', // Euro (€)
			'GBP', // British Pound (£)
			'JPY', // Japanese Yen (¥)
			'USD', // U.S. Dollar ($)
			'NZD', // New Zealand Dollar ($)
			'CHF', // Swiss Franc
			'HKD', // Hong Kong Dollar ($)
			'SGD', // Singapore Dollar ($)
			'SEK', // Swedish Krona
			'DKK', // Danish Krone
			'PLN', // Polish Zloty
			'NOK', // Norwegian Krone
			'HUF', // Hungarian Forint
			'CZK', // Czech Koruna
			'ILS', // Israeli New Shekel
			'MXN', // Mexican Peso
			'BRL', // Brazilian Real (only for Brazilian members)
			'MYR', // Malaysian Ringgit (only for Malaysian members)
			'PHP', // Philippine Peso
			'TWD', // New Taiwan Dollar
			'THB', // Thai Baht
			'TRY', // Turkish Lira (only for Turkish members)
			'RUB' // Russian Ruble
		);

		/**
		 * PayPal constructor
		 */
		public function __construct()
		{
			parent::__construct();

			$model  = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params = $model->loadGatewayParams($this->name);

			if ($params->get('use_sandbox', 0))
				$this->action = "https://www.sandbox.paypal.com/cgi-bin/webscr";
			else
				$this->action = "https://www.paypal.com/cgi-bin/webscr";
		}

		/**
		 * Getter for PayPal supported currencies
		 *
		 * @return array
		 */
		public function getSupportedCurrencies()
		{
			return $this->_supportedCurrencies;
		}

		/**
		 * Generate payment form
		 *
		 * @param  object $order Order object
		 * @param  array  $items Items order object
		 * @param  array  $urls  Gateway return | notify | cancel urls
		 * @param  null   $shipping
		 * @param  null   $tax
		 *
		 * @param null    $params
		 *
		 * @return string
		 */
		public function getPaymentForm($order, $items, $urls, $shipping = NULL, $tax = NULL, $params = NULL)
		{
			$modelGateways = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$pp_params     = $modelGateways->loadGatewayParams($this->name);

			if ($params) {
				$image = JURI::root() . "administrator/components/" . APP_EXTENSION . "/thefactory/payments/images/pay_now_big.png";
				/** @var $params JRegistry */
				$paypal_address = $params->get('pay_paypal.paypalemail', '');
				$attrib         = "style='width:150px;height:60px; border:none;background:none;margin-left: 30px;float:left;'";
			} else {
				$image          = "https://www.paypal.com/en_US/i/btn/x-click-but23.gif";
				$paypal_address = $pp_params->get('paypalemail', '');
				$attrib         = "style='margin-left: 30px;'";
			}

			if (!$paypal_address) {
				echo "<div class='error'>" . sprintf(JText::_("FACTORY_PAYMENT_GATEWAY_INTITALIZATION_MISSING"), $this->fullname) . "</div>";

				return "";
			}

			$result = "<form name='paypalForm' action='" . $this->action . "' method='post'>";
			$result .= "<input type='hidden' name='cmd' value='_cart' /><input type='hidden' name='upload' value='1' />";
			$result .= "<input type='hidden' name='business' value='" . $paypal_address . "' />";
			$result .= "<input type='hidden' name='invoice' value='" . $order->id . "' />";
			$result .= "<input type='hidden' name='return' value='" . $urls['return_url'] . "' />";
			$result .= "<input type='hidden' name='cancel_return' value='" . $urls['cancel_url'] . "' />";
			$result .= "<input type='hidden' name='notify_url' value='" . $urls['notify_url'] . "' />";
			$result .= "<input type='hidden' name='rm' value='2' />";
			$result .= "<input type='hidden' name='no_note' value='1' />";
			$result .= "<input type='hidden' name='bn' value='ThePHPFactory_SP' />";
			$result .= "<input type='hidden' name='currency_code' value='" . $order->order_currency . "' />";
			$result .= "<input type='image' src='{$image}' border='0' name='submit' alt='" . JText::_("FACTORY_BUY_NOW") . "' {$attrib} />";

			if (!$shipping) $result .= "<input type='hidden' name='no_shipping' value='1' />";
			if (!$tax) $result .= "<input type='hidden' name='tax' value='0' />";
			$discount = 0;
			for ($i = 0; $i < count($items); $i++) if ($items[$i]->price > 0) {
				$result .= "<input type='hidden' name='item_name_" . ($i + 1) . "' value='" . $items[$i]->itemdetails . "' />";
				$result .= "<input type='hidden' name='item_number_" . ($i + 1) . "' value='" . $items[$i]->itemname . "' />";
				$result .= "<input type='hidden' name='amount_" . ($i + 1) . "' value='" . $items[$i]->price . "' />";
				$result .= "<input type='hidden' name='quantity_" . ($i + 1) . "' value='" . $items[$i]->quantity . "' />";
			} else $discount += $items[$i]->price;
			$discount = -$discount;
			if ($discount > 0) $result .= "<input type='hidden' name='discount_amount_cart' value='{$discount}' />";
			$result .= "</form>";

			return $result;
		}


		/**
		 * Process IPN
		 *
		 * @return mixed
		 */
		public function processIPN()
		{
			$input          = JFactory::getApplication()->input;
			$model          = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params         = $model->loadGatewayParams($this->name);
			$paypal_address = $params->get('paypalemail', '');

			$paylog                 = JTable::getInstance('PaymentLogTable', 'JTheFactory');
			$date                   = new JDate();
			$paylog->date           = $date->toSQL();
			$paylog->amount         = $input->getFloat('mc_gross', 0);
			$paylog->currency       = $input->getString('mc_currency', '');
			$paylog->refnumber      = $input->getString('txn_id', '');
			$paylog->invoice        = $input->getInt('invoice', 0);
			$paylog->ipn_response   = print_r($_REQUEST, TRUE);
			$paylog->ipn_ip         = $_SERVER['REMOTE_ADDR'];
			$paylog->status         = 'error';
			$paylog->userid         = NULL;
			$paylog->orderid        = $input->getInt('invoice', 0);
			$paylog->payment_method = $this->name;

			$receiver_email = $input->getString('receiver_email', '');
			$payment_status = $input->getString('payment_status', '');
			switch ($payment_status) {
				case "Completed":
				case "Processed":
					$paylog->status = 'ok';
					break;
				case "Failed":
				case "Denied":
				case "Canceled-Reversal":
				case "Canceled_Reversal":
				case "Expired":
				case "Voided":
				case "Reversed":
				case "Refunded":
					$paylog->status = 'error';
					break;
				default:
				case "In-Progress":
				case "Pending":
					$paylog->status = 'manual_check';
					break;
			}
			if ('withdraw' != $input->getWord('custom', '')) {
				$IPN_validated = $this->validate_ipn();

				if (!$IPN_validated || (trim($receiver_email) <> trim($paypal_address))) {
					$paylog->status = 'error';
				} elseif (!$params->get('auto_accept', 1)) {
					$paylog->status = 'manual_check';
				}
			}
			$paylog->store();

			return $paylog;
		}

		/**
		 * @param null $d
		 *
		 * @return JRegistry
		 */
		public function getUserGatewayParam($d = NULL)
		{
			$d     = (count($d)) ? $d : $_REQUEST;
			$input = new JInput($d);
			$reg   = new JRegistry();
			$reg->set('pay_paypal.paypalemail', $input->get('pay_paypal.paypalemail', '', 'string'));

			return $reg;
		}

		/**
		 * Validate Remote IP
		 *
		 * @return bool
		 */
		private function validate_remote_ip()
		{
			//DEV PURPOSE
			//return true;
			$paypal_iplist  = gethostbynamel('www.paypal.com');
			$paypal_iplist2 = gethostbynamel('notify.paypal.com');
			$paypal_iplist  = array_merge($paypal_iplist, $paypal_iplist2);

			$paypal_sandbox_hostname = 'ipn.sandbox.paypal.com';
			$remote_hostname         = gethostbyaddr($_SERVER['REMOTE_ADDR']);

			$valid_ip = FALSE;

			if ($paypal_sandbox_hostname == $remote_hostname) {
				$valid_ip = TRUE;
			} else {
				$ips = "";
				// Loop through all allowed IPs and test if the remote IP connected here
				// is a valid IP address
				foreach ($paypal_iplist as $ip) {
					$ips .= "$ip,\n";
					$parts       = explode(".", $ip);
					$first_three = $parts[0] . "." . $parts[1] . "." . $parts[2];
					if (preg_match("/^$first_three/", $_SERVER['REMOTE_ADDR'])) {
						$valid_ip = TRUE;
					}
				}
			}

			return $valid_ip;
		}

		/**
		 * Validate IPN
		 *
		 * @return bool|string
		 */
		public function validate_ipn()
		{
			// Set to TRUE only for debugging purpose
			$debug_PayPal_IPN = FALSE;
			$IPN_logs         = JTable::getInstance('IPN_logs', 'JTable');


			$ch = curl_init(); // Start the curl handler
			// Generate the post string from the _POST vars as well as load the
			// _POST vars into an array so we can play with them from the calling
			// script.
			$post_string = '';
			foreach ($_POST as $field => $value) {
				$post_string .= $field . '=' . urlencode($value) . '&';
			}
			$post_string .= "cmd=_notify-validate"; // append ipn command
			if (JDEBUG && $debug_PayPal_IPN) {

				$IPN_logs->gateway       = 'pay_paypal';
				$IPN_logs->event         = 'IPN handler';
				$IPN_logs->log           = 'The IPN gateway validator was called';
				$IPN_logs->date_creation = date('Y-m-d h:m:s');
				$IPN_logs->store();
			}
			// open the connection to PayPal
			if (!$ch) {
				if (JDEBUG && $debug_PayPal_IPN) {
					$IPN_logs->gateway       = 'pay_paypal';
					$IPN_logs->event         = 'curl_error_connection';
					$IPN_logs->log           = curl_error($ch) . "\n\n" . 'Error connecting PayPal through curl ';
					$IPN_logs->date_creation = date('Y-m-d h:m:s');
					$IPN_logs->store();
				}

				return FALSE;
			} else {
				curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
				curl_setopt($ch, CURLOPT_URL, $this->action); // parse the PayPal URL
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // SSL is not verified
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($ch, CURLOPT_FAILONERROR, 1);
				curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

				if (!($response = curl_exec($ch))) {
					if (JDEBUG && $debug_PayPal_IPN) {
						$IPN_logs->gateway       = 'pay_paypal';
						$IPN_logs->event         = 'curl_error_exec';
						$IPN_logs->log           = curl_error($ch) . "\n\n" . 'IPN not verified for invoice ' . JFactory::getApplication()->input->getString('invoice', '') . "\n\n" . 'Url_action:' . $this->action;
						$IPN_logs->date_creation = date('Y-m-d h:m:s');
						$IPN_logs->store();
					}
					curl_close($ch);

					return FALSE;
				}


				curl_close($ch);
			}

			if (preg_match("/VERIFIED/i", $response)) {
				if (JDEBUG && $debug_PayPal_IPN) {
					$IPN_logs->gateway       = 'pay_paypal';
					$IPN_logs->event         = 'IPN response';
					$IPN_logs->log           = 'IPN successfuly verified: response is > ' . $response;
					$IPN_logs->date_creation = date('Y-m-d h:m:s');
					$IPN_logs->store();
				}

				return TRUE;
			} else {
				if (JDEBUG && $debug_PayPal_IPN) {
					$IPN_logs->gateway       = 'pay_paypal';
					$IPN_logs->event         = 'IPN response';
					$IPN_logs->log           = 'IPN not successfuly verified:  response is > ' . $response;
					$IPN_logs->date_creation = date('Y-m-d h:m:s');
					$IPN_logs->store();
				}

				return FALSE;
			}

		}

	} // End Class


