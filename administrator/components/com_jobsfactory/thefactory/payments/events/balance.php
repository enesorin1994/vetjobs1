<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryEventBalance
	 */
	class JTheFactoryEventBalance extends JTheFactoryEvents
	{
		/**
		 * getItemName
		 *
		 * @return string
		 */
		public function getItemName()
		{
			return "Balance";
		}

		/**
		 * getContext
		 *
		 * @return string
		 */
		public function getContext()
		{
			return APP_PREFIX . "." . self::getItemName();
		}

		// Increase user balance after payment confirmation
		/**
		 * onPaymentForOrder
		 *
		 * @param $paylog
		 * @param $order
		 */
		public function onPaymentForOrder($paylog, $order)
		{
			if (!$order->status == 'C') return;
			$modelorder = JTheFactoryPricingHelper::getModel('orders');
			// Payment item must be Balance
			$items = $modelorder->getOrderItems($order->id, self::getItemName());
			if (!is_array($items) || !count($items)) return; //no Listing items in order

			foreach ($items as $item) {
				if ($item->itemname != self::getItemName()) continue;

				$modelbalance = JTheFactoryPricingHelper::getModel('balance');
				$modelbalance->increaseBalance($item->price * $item->quantity, $order->userid);
			}

		}

		/**
		 * Refund user for order canceled
		 *
		 * @param $order
		 *
		 * @return null
		 */
		public function onOrderCanceled($order)
		{
			if ('X' != $order->status) {
				return NULL;
			}
			/* @var $balance JTheFactoryBalanceTable */
			/* @var $currency JTheFactoryModelCurrency */
			/* @var $modelorder JTheFactoryModelOrders */

			$modelorder = JTheFactoryPricingHelper::getModel('orders');
			$items      = $modelorder->getOrderItems($order->id, self::getItemName());

			$balance  = JTable::getInstance('BalanceTable', 'JTheFactory');
			$currency = JTheFactoryPricingHelper::getModel('currency');

			if ($balance->load(array('userid' => $order->userid))) {

				// !!! Refund must be done in user balance currency

				// Exception: If order contain balance item
				// we must substract it from balance
				if (count($items)) {
					$balance->balance -= $currency->convertCurrency($order->order_total, $order->order_currency, $balance->currency);
				} else {
					$balance->balance += $currency->convertCurrency($order->order_total, $order->order_currency, $balance->currency);
				}
				$balance->store();
			}
		}

		/**
		 * onDefaultCurrencyChange
		 *
		 */
		public function onDefaultCurrencyChange()
		{

			$db = JFactory::getDbo();

			$db->setQuery('SELECT * FROM #__' . APP_PREFIX . '_payment_balance');
			$balances = $db->loadObjectList();

			$currency    = JTheFactoryPricingHelper::getModel('currency');
			$newCurrency = $currency->getDefault();

			foreach ($balances as $b) {
				$balance = JTable::getInstance('BalanceTable', 'JTheFactory');
				$balance->bind($b);

				$balance->balance             = $currency->convertCurrency($balance->balance, $balance->currency, $newCurrency);
				$balance->req_withdraw        = $currency->convertCurrency($balance->req_withdraw, $balance->currency, $newCurrency);
				$balance->withdrawn_until_now = $currency->convertCurrency($balance->withdrawn_until_now, $balance->currency, $newCurrency);
				$balance->currency            = $newCurrency;

				$balance->store();
			}

		}
	} // End Class
