<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryEventCart
	 */
	class JTheFactoryEventCart extends JTheFactoryEvents
	{
		/**
		 * getItemName
		 *
		 * @return string
		 */
		public function getItemName()
		{
			return "cart";
		}

		/**
		 * getContext
		 *
		 * @return string
		 */
		public function getContext()
		{
			return APP_PREFIX . "." . self::getItemName();
		}

		/**
		 * onAddToCart
		 *
		 *
		 * Event responsible for adding items to Cart session
		 *
		 * @param      array $items Array of order items objects
		 */
		public function onAddToCart($items)
		{
			$cartState           = new stdClass();
			$cartState->items    = array();
			$cartState->quantity = 0;

			$session   = JFactory::getSession();
			$namespace = APP_PREFIX . '_shopping_cart';


			if ($session->has('cart_info', $namespace)) {

				$sessCart         = $session->get('cart_info', '', $namespace);
				$cartState->items = $sessCart->items;
			}


			$cartState->items[] = $items;

			$cartState->quantity = count($cartState->items);
			$session->set('cart_info', $cartState, $namespace);
		}

		/**
		 * onBeforeCheckout
		 *
		 *
		 * Create new pending order containing cart items
		 *
		 * Clear cart session before checkout
		 *
		 * @param array $orderItems Array of order items objects
		 * @param float $orderTotal Cart total price
		 *
		 */
		public function onBeforeCheckout($orderItems, $orderTotal)
		{

			$app       = JFactory::getApplication();
			$my        = JFactory::getUser();
			$session   = JFactory::getSession();
			$namespace = APP_PREFIX . '_shopping_cart';

			JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . APP_EXTENSION . DS . 'thefactory' . DS . 'payments' . DS . 'models');
			$modelCurrency = JModelLegacy::getInstance('Currency', 'JTheFactoryModel');

			// Remove cart content from session
			if ($session->has('cart_info', $namespace)) {
				$session->clear('cart_info', $namespace);
			}

			/* @var $modelOrders JTheFactoryModelOrders */
			$modelOrders = JModelLegacy::getInstance('orders', 'JTheFactoryModel');

			if (0 == $orderTotal) {
				$order = $modelOrders->createNewOrder($orderItems, $orderTotal, $modelCurrency->getDefault(), $my->id, 'C');

				JTheFactoryEventsHelper::triggerEvent('onPaymentForOrder', array(NULL, $order));
				$app->redirect('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.details&orderid=' . $order->id, FALSE);

			} else {
				$order = $modelOrders->createNewOrder($orderItems, $orderTotal, $modelCurrency->getDefault(), $my->id, 'P');

				$app->redirect('index.php?option=' . APP_EXTENSION . '&task=orderprocessor.checkout&orderid=' . $order->id, FALSE);
			}


		}
	}
