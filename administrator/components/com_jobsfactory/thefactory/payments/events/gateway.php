<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryEventGateway
	 */
	class JTheFactoryEventGateway extends JTheFactoryEvents
	{
		/**
		 * getItemName
		 *
		 * @return string
		 */
		public function getItemName()
		{
			return "Gateway";
		}

		/**
		 * getContext
		 *
		 * @return string
		 */
		public function getContext()
		{
			return APP_PREFIX . "." . self::getItemName();
		}

		/**
		 * onAfterCurrencySaved
		 *
		 * @param $currencyTable
		 */
		public function onAfterCurrencySaved($currencyTable)
		{
			$session = JFactory::getSession();

			/* @var $modelGateways JTheFactoryModelGateways */
			$modelGateways              = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$gatewayCurrencyUnsupported = array();
			$gateways                   = $modelGateways->getGatewayList(FALSE);

			// Fetch all available payment gateways
			// in order to see if added currency is supported by each gateway
			if (is_array($gateways) && count($gateways)) {
				foreach ($gateways as $gw) {
					$objGateway = $modelGateways->getGatewayObject($gw->classname);

					// Fetch only for gateways which have a list of supported currencies
					if (count($objGateway->getSupportedCurrencies())) {
						if (!in_array(strtoupper($currencyTable->name), $objGateway->getSupportedCurrencies())) {
							$gatewayCurrencyUnsupported[$gw->paysystem] = $currencyTable->name;
						}
					}
				}

			}

			if (count($gatewayCurrencyUnsupported)) {
				$session->set('gatewayCurrencyUnsupported', $gatewayCurrencyUnsupported, 'theFactory');
			}

		}
	} // End Class
