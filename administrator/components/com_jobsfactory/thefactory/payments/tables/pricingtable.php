<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	/**
	 * Class JTheFactoryPricingTable
	 */
	class JTheFactoryPricingTable extends JTable
	{
		/**
		 * id
		 *
		 * @var
		 */
		public $id;
		/**
		 * itemname
		 *
		 * @var
		 */
		public $itemname;
		/**
		 * pricetype
		 *
		 * @var
		 */
		public $pricetype;
		/**
		 * name
		 *
		 * @var
		 */
		public $name;
		/**
		 * price
		 *
		 * @var
		 */
		public $price;
		/**
		 * currency
		 *
		 * @var
		 */
		public $currency;
		/**
		 * enabled
		 *
		 * @var
		 */
		public $enabled;
		/**
		 * params
		 *
		 * @var
		 */
		public $params;
		/**
		 * ordering
		 *
		 * @var
		 */
		public $ordering;

		/**
		 * @param JDatabaseDriver $db
		 */
		public function __construct($db)
		{
			parent::__construct('#__' . APP_PREFIX . '_pricing', 'id', $db);
		}
	}
