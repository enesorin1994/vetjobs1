<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	/**
	 * @class JTheFactoryOrdersTable
	 */
	class JTheFactoryOrdersTable extends JTable
	{
		/**
		 * id
		 *
		 * @var null
		 */
		public $id = NULL;
		/**
		 * orderdate
		 *
		 * @var null
		 */
		public $orderdate = NULL;
		/**
		 * modifydate
		 *
		 * @var null
		 */
		public $modifydate = NULL;
		/**
		 * userid
		 *
		 * @var null
		 */
		public $userid = NULL;
		/**
		 * order_total
		 *
		 * @var null
		 */
		public $order_total = NULL;
		/**
		 * order_currency
		 *
		 * @var null
		 */
		public $order_currency = NULL;
		/**
		 * status
		 *
		 * @var null
		 */
		public $status = NULL; // C=confirmed, P=pending, X=cancelled, R=refunded
		/**
		 * paylogid
		 *
		 * @var null
		 */
		public $paylogid = NULL;
		/**
		 * params
		 *
		 * @var null
		 */
		public $params = NULL;

		/**
		 * @param JDatabaseDriver $db
		 */
		public function __construct($db)
		{
			parent::__construct('#__' . APP_PREFIX . '_payment_orders', 'id', $db);
		}

	}
