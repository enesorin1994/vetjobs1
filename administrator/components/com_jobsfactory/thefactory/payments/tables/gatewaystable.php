<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	/**
	 * Class JTheFactoryGatewaysTable
	 */
	class JTheFactoryGatewaysTable extends JTable
	{
		/**
		 * id
		 *
		 * @var null
		 */
		public $id = NULL;
		/**
		 * paysystem
		 *
		 * @var null
		 */
		public $paysystem = NULL;
		/**
		 * classname
		 *
		 * @var null
		 */
		public $classname = NULL;
		/**
		 * enabled
		 *
		 * @var null
		 */
		public $enabled = NULL;
		/**
		 * params
		 *
		 * @var null
		 */
		public $params = NULL;
		/**
		 * ordering
		 *
		 * @var null
		 */
		public $ordering = NULL;
		/**
		 * isdefault
		 *
		 * @var null
		 */
		public $isdefault = NULL;

		/**
		 * @param JDatabaseDriver $db
		 */
		public function __construct($db)
		{
			parent::__construct('#__' . APP_PREFIX . '_paysystems', 'id', $db);
		}
	}
