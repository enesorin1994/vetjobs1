<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryPaymentLogTable
	 */
	class JTheFactoryPaymentLogTable extends JTable
	{
		/**
		 * id
		 *
		 * @var null
		 */
		public $id = NULL;
		/**
		 * date
		 *
		 * @var null
		 */
		public $date = NULL;
		/**
		 * amount
		 *
		 * @var null
		 */
		public $amount = NULL;
		/**
		 * currency
		 *
		 * @var null
		 */
		public $currency = NULL;
		/**
		 * refnumber
		 *
		 * @var null
		 */
		public $refnumber = NULL;
		/**
		 * invoice
		 *
		 * @var null
		 */
		public $invoice = NULL;
		/**
		 * ipn_response
		 *
		 * @var null
		 */
		public $ipn_response = NULL;
		/**
		 * ipn_ip
		 *
		 * @var null
		 */
		public $ipn_ip = NULL;
		/**
		 * status
		 *
		 * @var null
		 */
		public $status = NULL;
		/**
		 * userid
		 *
		 * @var null
		 */
		public $userid = NULL;
		/**
		 * orderid
		 *
		 * @var null
		 */
		public $orderid = NULL;
		/**
		 * gatewayid
		 *
		 * @var null
		 */
		public $gatewayid = NULL;
		/**
		 * payment_method
		 *
		 * @var null
		 */
		public $payment_method = NULL;

		/**
		 * @param JDatabaseDriver $db
		 */
		public function __construct($db)
		{
			parent::__construct('#__' . APP_PREFIX . '_payment_log', 'id', $db);
		}

	}
