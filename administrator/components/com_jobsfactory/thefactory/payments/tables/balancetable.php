<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	/**
	 * Class JTheFactoryBalanceTable
	 */
	class JTheFactoryBalanceTable extends JTable
	{
		/**
		 * id
		 *
		 * @var null
		 */
		public $id = NULL;
		/**
		 * userid
		 *
		 * @var null
		 */
		public $userid = NULL;
		/**
		 * balance
		 *
		 * @var null
		 */
		public $balance = NULL;
		/**
		 * req_withdraw
		 *
		 * @var null
		 */
		public $req_withdraw = NULL;
		/**
		 * last_withdraw_date
		 *
		 * @var null
		 */
		public $last_withdraw_date = NULL;
		/**
		 * paid_withdraw_date
		 *
		 * @var null
		 */
		public $paid_withdraw_date = NULL;
		/**
		 * withdrawn_until_now
		 *
		 * @var null
		 */
		public $withdrawn_until_now = NULL;
		/**
		 * currency
		 *
		 * @var null
		 */
		public $currency = NULL;

		/**
		 * @param JDatabaseDriver $db
		 */
		public function __construct($db)
		{
			parent::__construct('#__' . APP_PREFIX . '_payment_balance', 'userid', $db);
		}

		/**
		 * addBalance
		 *
		 * @param      $userid
		 * @param null $amount
		 * @param null $currency
		 */
		public function addBalance($userid, $amount = NULL, $currency = NULL)
		{
			$db = $this->getDbo();
			$db->setQuery("INSERT INTO `" . $this->getTableName() . "` (`userid`, `balance`, `currency`) VALUES(
            " . $db->quote($userid) . ",
            " . $db->quote($amount) . ",
            " . $db->quote($currency) . "
        ) ");
			$db->execute();
		}
	}
