<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class TheFactoryPaymentGateway
	 */
	class TheFactoryPaymentGateway extends JObject
	{
		/**
		 * id
		 *
		 * @var null
		 */
		var $id = NULL;
		/**
		 * name
		 *
		 * @var null
		 */
		var $name = NULL;
		/**
		 * fullname
		 *
		 * @var null
		 */
		var $fullname = NULL;
		/**
		 * formxml
		 *
		 * @var null|string
		 */
		var $formxml = NULL;
		/**
		 * pluginfolder
		 *
		 * @var null|string
		 */
		var $pluginfolder = NULL;
		/**
		 * Supported currencies list
		 *
		 * @var array
		 */
		protected $_supportedCurrencies = array();

		/**
		 * TheFactoryPaymentGateway constructor
		 */
		public function __construct()
		{
			$MyApp              = JTheFactoryApplication::getInstance();
			$lang               = JFactory::getLanguage();
			$this->formxml      = $MyApp->app_path_admin . 'payments/plugins/gateways/' . strtolower($this->name) . '/form.xml';
			$this->pluginfolder = $MyApp->app_path_admin . 'payments/plugins/gateways/' . strtolower($this->name);

			$lang->load('thefactory.gateway.' . $this->name, $MyApp->app_path_admin);

			$tableGateway = JTable::getInstance('GatewaysTable', 'JTheFactory');
			$tableGateway->load(array('classname' => $this->name));
			$this->id = $tableGateway->id;
		}

		/**
		 * Getter for supported currencies
		 *
		 * @return array
		 */
		public function getSupportedCurrencies()
		{
			return $this->_supportedCurrencies;
		}

		/**
		 * getLogo
		 *
		 * @return string
		 */
		public function getLogo()
		{
			$uri = JURI::base();
			$uri .= (substr(strrev($uri), 1, 13) == strrev('administrator')) ? "" : "administrator/";

			return $uri . "components/" . APP_EXTENSION . "/thefactory/payments/plugins/gateways/" . strtolower($this->name) . "/logo.png";
		}

		/**
		 * saveAdminForm
		 */
		public function saveAdminForm()
		{
			$data = JTheFactoryHelper::getRequestArray('post', 'html');
			unset($data['option']);
			unset($data['task']);
			unset($data['classname']);
			unset($data['id']);
			$config = new JRegistry($data);
			$params = $config->toString('INI');
			$model  = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$model->saveGatewayParams($this->name, $params);

		}

		/**
		 * showAdminForm
		 */
		public function showAdminForm()
		{
			$model  = JModelLegacy::getInstance('Gateways', 'JTheFactoryModel');
			$params = $model->loadGatewayParams($this->name);

			jimport('joomla.form.form');
			$form = JForm::getInstance($this->name, $this->formxml);
			$form->bind($params->toArray());

			$fieldsets = $form->getFieldsets();

			if (file_exists($this->pluginfolder . '/logo.png')) echo "<div style='padding:15px;'><img src='" . $this->getLogo() . "' border='0'></div>";
			echo "<form name='adminForm' id='adminForm' action='index.php' method='post'>";
			echo "<input name='option' type='hidden' value='" . APP_EXTENSION . "'>";
			echo "<input name='task' type='hidden' value='gateways.save'>";
			echo "<input name='classname' type='hidden' value='{$this->name}'>";
			if (count($fieldsets)) foreach ($fieldsets as $fieldset) {
				$fields = $form->getFieldset((string)$fieldset->name);
				$this->showFieldSet($fields, (string)$fieldset->name);
			}
			else {
				$fields = $form->getFieldset();
				$this->showFieldSet($fields);
			}
			echo "</form>";
			self::setToolbar();
		}

		/**
		 * getPaymentForm
		 *
		 * @param      $order
		 * @param      $items
		 * @param      $urls
		 * @param null $shipping
		 * @param null $tax
		 * @param null $params - used for withdrawel forms
		 *
		 *
		 * @return mixed
		 */
		public function getPaymentForm($order, $items, $urls, $shipping = NULL, $tax = NULL, $params = NULL)
		{
			//has to be implemented in each gateway
			return;
		}

		/**
		 * processIPN
		 *
		 * @return mixed
		 */
		public function processIPN()
		{
			//has to be implemented in each gateway
			return;
		}

		/**
		 * processTask
		 *
		 * @return mixed
		 */
		public function processTask()
		{
			//entry point for the "gateway" task of the paymentsprocessor controller
			//see bank wire gateway for sample
			return;
		}

		/**
		 * allowsWithdrawel
		 *
		 *      returns true if the gateway can handle fund withdrawels requests
		 *
		 * @return
		 */
		public function allowsWithdrawel()
		{
			//Notifies if the gateway can be used for user withdrawels
			//Not all gateways can be used for withdrawing funds
			//some have just merchant accounts
			return FALSE;
		}

		/**
		 * TheFactoryPaymentGateway::getUserGatewayParam()
		 *
		 * @param mixed $d
		 *
		 * @return JRegistry with parameters
		 */
		public function getUserGatewayParam($d = NULL)
		{
			//returns text params
			//for gateway
			return new JRegistry();
		}

		/**
		 * showFieldSet
		 *
		 * @param      $fields
		 * @param null $setname
		 */
		private function showFieldSet($fields, $setname = NULL)
		{
			if ($setname) echo "<fieldset class='adminform'><legend>{$setname}</legend>";
			echo "<table width='100%'>";
			foreach ($fields as $field) {
				echo "
                    <tr>
                        <td class='paramlist_key' width='300' valign='top'>" . JText::sprintf($field->label, strtoupper(APP_EXTENSION)) . "</td>
                        <td>{$field->input}</td>
                    </tr>
            ";
			}
			echo "</table>";
			if ($setname) echo "</fieldset>";
		}

		/**
		 * setToolbar
		 */
		private function setToolbar()
		{
			JToolBarHelper::save('gateways.save');
			JToolBarHelper::cancel('gateways.listing');

		}
	} // End Class
