<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: payments
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryPriceItem
	 */
	class JTheFactoryPriceItem extends JObject
	{
		/**
		 * contex
		 *
		 * @var null|string
		 */
		var $contex = NULL;

		/**
		 * JTheFactoryPriceItem constructor
		 */
		public function __construct()
		{
			$this->contex = APP_PREFIX . '.orderclass';
		}

		/**
		 * getInstance
		 *
		 * @static
		 * @return JTheFactoryOrder
		 */
		public static function getInstance()
		{
			static $instance;

			if (!is_object($instance)) $instance = new JTheFactoryOrder();

			return $instance;

		}

		/**
		 * addOrderItem
		 *
		 * @static
		 *
		 * @param null $item
		 *
		 * @return array
		 */
		public static function addOrderItem($item = NULL)
		{
			static $items = array();
			if ($item) $items[] = $item;

			return $items;
		}

	} // End Class
