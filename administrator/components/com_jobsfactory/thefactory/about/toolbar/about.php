<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @package   : thefactory
	 * @subpackage: about
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryAboutToolbar
	{
		static function display($task = NULL)
		{
			$appname = JTheFactoryApplication::getInstance()->getApplicationName(TRUE);
			JToolBarHelper::title(JText::_('FACTORY_ABOUT_THE_FACTORY_EXTENSION') . ' - ' . $appname);
			switch ($task) {
				default:
					JToolBarHelper::back();
					break;
			}

		}
	}

