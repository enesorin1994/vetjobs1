<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: about
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	/**
	 * Class JTheFactoryAboutController
	 */
	class JTheFactoryAboutController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		public $name = 'About';
		/**
		 * modulename
		 *
		 * @var string
		 */
		public $modulename = 'about';

		/**
		 *
		 */
		public function __construct()
		{
			parent::__construct();
			$this->registerDefaultTask('main');
		}

		/**
		 * Main
		 *
		 */
		public function Main()
		{
			$MyApp = JTheFactoryApplication::getInstance();

			$filename = $MyApp->getIniValue('version_root') . '/' . APP_EXTENSION . ".xml";
			$doc      = JTheFactoryHelper::remote_read_url($filename);
			$xml      = simplexml_load_string($doc);

			$view                 = $this->getView('main');
			$view->isnew_version  = !(version_compare(COMPONENT_VERSION, (string)$xml->latestversion) >= 0);
			$view->latestversion  = (string)$xml->latestversion;
			$view->versionhistory = (string)$xml->versionhistory;
			$view->downloadlink   = (string)$xml->downloadlink;
			$view->aboutfactory   = html_entity_decode((string)$xml->aboutfactory);
			$view->otherproducts  = html_entity_decode((string)$xml->otherproducts);

			$view->build = (string)$xml->build;

			$view->display();
		}

	}
