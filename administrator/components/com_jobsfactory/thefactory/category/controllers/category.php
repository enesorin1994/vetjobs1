<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: category
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.application.component.model');

	/**
	 * Class JTheFactoryCategoryController
	 */
	class JTheFactoryCategoryController extends JTheFactoryController
	{
		/**
		 * name
		 *
		 * @var string
		 */
		public $name = 'Category';
		/**
		 * modulename
		 *
		 * @var string
		 */
		public $modulename = 'category';

		/**
		 * execute
		 *
		 * @param mixed|string $task
		 *
		 * @return mixed
		 */
		public function execute($task)
		{
			//test if legacy or Joomla native
			if (!in_array(strtolower($task),array('quickaddcat')) && !$this->getApp()->getIniValue('use_legacy_model', 'categories')) {
				$this->setRedirect(JRoute::_('index.php?option=com_categories&extension=' . APP_EXTENSION));

				return;
			}

			return parent::execute($task);
		}

		/**
		 *  Display categories
		 *
		 */
		public function Categories()
		{
			/**
			 * @var JTheFactoryModelCategory $catModel
			 */
			$catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');
			$cats     = $catModel->getCategoryTree(0, TRUE);

			$view             = $this->getView();
			$view->categories = $cats;

			$view->display();
		}


		/**
		 * DelCategories
		 *
		 */
		public function DelCategories()
		{
			/**
			 * @var JTheFactoryModelCategory $catModel
			 */
			$cids = JFactory::getApplication()->input->get("cid", array(), 'array');

			$catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');
			$nr       = $catModel->delCategory($cids);

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=category.categories', $nr . " " . JText::_('FACTORY_CATEGORIES_DELETED'));

		}


		/**
		 * EditCat
		 *
		 */
		public function EditCat()
		{
			/**
			 * @var JTheFactoryModelCategory $catModel
			 * @var JTheFactoryTableCategory $cattable
			 */
			$cid = JFactory::getApplication()->input->get("cid", array(), 'array');
			if (is_array($cid)) $cid = $cid[0];

			$catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');

			$cattable = JTable::getInstance('Category', 'JTheFactoryTable');
			$cattable->load($cid);

			$cats = $catModel->getCategoryTree(0, TRUE);

			JHtml::_('formbehavior.chosen', 'select');

			$view       = $this->getView();
			$view->row  = $cattable;
			$view->cats = $cats;

			$view->display('form');
		}


		/**
		 * SaveCat
		 *
		 * @return JControllerLegacy
		 */
		public function SaveCat()
		{
			/** @var $input  JInput */
			$input = JFactory::getApplication()->input;

			$id        = $input->getInt('id', 0);
			$newparent = $input->getInt('parent', 0); // move to

			/** @var  $catobj JTheFactoryTableCategory */
			$catobj = JTable::getInstance('Category', 'JTheFactoryTable');
			$catobj->load($id);
			$oldParent = $catobj->parent;

			// Proceed to move
			if ($id && ($newparent != $oldParent)) {
				/** @var $catModel JTheFactoryModelCategory */
				$catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');
				// Prevent switching parent category
				// with its child
				if ($catModel->detectParentSwitchedWithChild($id, $newparent)) {
					return $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=category.editcat&cid=' . $id, JText::_('FACTORY_PARENT_TO_CHILD_SWITCH_DETECTED'));
				}
				$catModel->moveCategory($id, $newparent);
			}
			// Load edited category with parent changed !Important
			$catobj->load($id);
			// Ignore parent because is already moved
			$catobj->bind(JTheFactoryHelper::getRequestArray('post', 'html'), array('parent', 'hash'));
			// If is new category then keep the parent
			if (!$id) {
				$catobj->parent = $newparent;
			}
			$catobj->store();


			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=category.categories', JText::_('FACTORY_CATEGORY_SAVED'));
		}


		/**
		 * NewCat
		 *
		 */
		public function NewCat()
		{
			$cattable = JTable::getInstance('Category', 'JTheFactoryTable');

			$view      = $this->getView();
			$view->row = $cattable;

			$view->display('form');
		}


		/**
		 * saveCatOrder
		 *
		 */
		public function saveCatOrder()
		{
			$cat = JTable::getInstance('Category', 'JTheFactoryTable');

			foreach ($_REQUEST as $k => $v) {
				if (substr($k, 0, 6) == 'order_') {
					$id = substr($k, 6);
					$cat->load($id);
					$cat->ordering = $v;
					$cat->store();
				}
			}

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=category.categories', JText::_('FACTORY_ORDERING_SAVED'));
		}

		/**
		 * Quick endless level categories to ROOT
		 *
		 * @modified: 23/09/09
		 **/
		public function QuickAddCat()
		{
			$textcats = JFactory::getApplication()->input->getString('quickadd', '');
			$catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');
			$catModel->quickAddFromText($textcats);

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=category.categories', JText::_('FACTORY_CATEGORY_ADDED'));
		}


		/**
		 * showMoveCategories - Class Method
		 *
		 * Move categories in other parent category
		 */
		public function showMoveCategories()
		{
			JHtml::_('formbehavior.chosen', 'select');

			$database = JFactory::getDbo();
			$cid_arr  = JFactory::getApplication()->input->get('cid', array(), 'array');
			$cid_list = implode(",", $cid_arr);

			$catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');

			$database->setQuery("SELECT * FROM `" . $catModel->getCategoryTable() . "` WHERE `id` IN ($cid_list);");
			$changed_cats = $database->loadObjectList();

			$html_tree      = JHtml::_('factorycategory.select', 'parent', '', 0, FALSE, FALSE, TRUE, JText::_('FACTORY_ROOT_CATEGORY'));
			$view           = $this->getView();
			$view->cid_list = $cid_list;
			$view->cats     = $changed_cats;
			$view->parent   = $html_tree;

			$view->display('movecat');
		}


		/**
		 * doMoveCategories - Class Method
		 *
		 * Move categories in tree
		 */
		public function doMoveCategories()
		{
			/**
			 * @var  $input    JInput
			 * @var  $catModel JTheFactoryModelCategoryLegacy
			 */
			$input = JFactory::getApplication()->input;
			// Cid format 3,5,6,8,12
			$cid = $input->getString('cid', '');
			// Category Root cannot be moved
			if (!$cid)
				return NULL;

			// Category to switch on
			$categoryMoveTo = $input->getInt('parent', 0);

			$catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');

			// Prevent switching parent category
			// with its child
			$cids = explode(',', $cid);
			static $i = 0;
			foreach ($cids as $c_id) {
				if ($catModel->detectParentSwitchedWithChild($c_id, $categoryMoveTo)) {
					$i++;
				}
			}
			if ($i) {
				return $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=category.categories', JText::_('FACTORY_PARENT_TO_CHILD_SWITCH_DETECTED'));
			}

			$nr = $catModel->moveCategory($cid, $categoryMoveTo);

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=category.categories', $nr . " " . JText::_('FACTORY_CATEGORIES_MOVED'));
		}


		/**
		 * Publish_Cat
		 *
		 */
		public function Publish_Cat()
		{

			$cids        = JFactory::getApplication()->input->get("cid", array(), 'array');
			$nr          = count($cids);
			$cids_string = implode(",", $cids);

			$catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');
			$db       = JFactory::getDbo();
			$db->setQuery("UPDATE `" . $catModel->getCategoryTable() . "` SET status = 1 WHERE id in ($cids_string)");
			$db->execute();

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=category.categories', $nr . " " . JText::_('FACTORY_CATEGORIES_PUBLISHED'));
		}


		/**
		 * Unpublish_Cat
		 *
		 */
		public function Unpublish_Cat()
		{

			$cids        = JFactory::getApplication()->input->get("cid", array(), 'array');
			$nr          = count($cids);
			$cids_string = implode(",", $cids);

			$catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');
			$db       = JFactory::getDbo();
			$db->setQuery("UPDATE `" . $catModel->getCategoryTable() . "` SET status = 0 WHERE id in ($cids_string)");
			$db->execute();

			$this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=category.categories', $nr . " " . JText::_('FACTORY_CATEGORIES_UNPUBLISHED'));
		}

	}
