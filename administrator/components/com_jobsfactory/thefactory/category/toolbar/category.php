<?php
/**------------------------------------------------------------------------
thefactory - The Factory Class Library - v 3.0.0
------------------------------------------------------------------------
 * @author    thePHPfactory
 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *            Websites: http://www.thePHPfactory.com
 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build     : 01/04/2012
 * @package   : thefactory
 * @subpackage: category
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JTheFactoryCategoryToolbar
{
    /**
     * display
     *
     * @static
     *
     * @param null $task
     */
    public static function display($task=null)
    {
        $appname=JTheFactoryApplication::getInstance()->getApplicationName();
        JToolBarHelper::title(JText::_('FACTORY_CATEGORY_MANAGEMENT').' - '.$appname);
        switch ($task)
        {
            default:
            case 'categories':
                JToolBarHelper::custom('category.newcat', 'new.png', 'new_f2.png', JText::_('FACTORY_NEW_CATEGORY'), false);
                JToolBarHelper::custom('category.editcat', 'edit.png', 'edit_f2.png', JText::_('FACTORY_EDIT_CATEGORIES'), true);
                JToolBarHelper::custom('category.showmovecategories', 'move.png', 'move_f2.png', JText::_('FACTORY_MOVE_CATEGORIES'), true);
                JToolBarHelper::deleteList("", "category.delcategories");
                JToolBarHelper::custom('settingsmanager', 'back', 'back', JText::_('FACTORY_BACK'), false);
                break;
            case 'newcat':
            case 'editcat':
                JToolBarHelper::save("category.savecat");
	            JToolBarHelper::custom('category.categories', 'back', 'back', JText::_('FACTORY_BACK'), false);
                break;
            case 'showmovecategories':
	            // Is already present in form a button for this task
	            JToolBarHelper::custom('category.categories', 'back', 'back', JText::_('FACTORY_BACK'), false);
                //JToolBarHelper::custom('category.doMoveCategories', 'save.png', 'save_f2.png', JText::_('FACTORY_MOVE_TO_SELECTED_CATEGORY'), false);
                break;
        }

    }
}
