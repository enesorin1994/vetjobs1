<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: category
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	abstract class JHtmlFactoryCategory
	{
		/**
		 * select
		 *
		 * @static
		 *
		 * @param        $inputname
		 * @param string $html_attribs
		 * @param null   $selectedcat
		 * @param bool   $include_disabled
		 * @param bool   $parents_disabled
		 * @param bool   $include_empty_opt
		 * @param null   $empty_opt_label
		 *
		 * @return mixed|string
		 */
		public static function select($inputname, $html_attribs = '', $selectedcat = NULL, $include_disabled = FALSE, $parents_disabled = FALSE, $include_empty_opt = FALSE, $empty_opt_label = NULL)
		{
			JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/' . APP_EXTENSION . '/thefactory/category/models');
			$catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');

			$app  = JTheFactoryApplication::getInstance();
			$root = $app->getIniValue('use_legacy_model', 'categories') ? 0 : 1;

			$cat_tree = $catModel->getCategoryTree($root, $include_disabled);

			if (!count($cat_tree)) return "";
			$options = array();
			if ($include_empty_opt) $options[] = JHTML::_('select.option', '', $empty_opt_label ? $empty_opt_label : JText::_('FACTORY_ANY_CATEGORY'), 'value', 'text');

			foreach ($cat_tree as $category) {
				// TheFactory category vs Joomla category
				$catName = '';
				if (property_exists($category, 'catname')) {
					$catName = $category->catname;
				} else if (property_exists($category, 'title')) {
					$catName = $category->title;
				}

				if (!$catName) {
					continue;
				}

				//TODO: nr_children nu e intors de model
				$spacer    = str_pad('', ($category->depth - 1) * 3, '-');
				$disabled  = ($parents_disabled && $category->nr_children);
				$options[] = JHTML::_('select.option', $category->id, $spacer . stripslashes($catName), 'value', 'text', $disabled);
			}

			$html_tree = JHTML::_('select.genericlist', $options, $inputname, $html_attribs, 'value', 'text', $selectedcat);

			return $html_tree;

		}
	} // End Class
