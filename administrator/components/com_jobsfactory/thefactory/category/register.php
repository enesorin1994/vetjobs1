<?php
/**------------------------------------------------------------------------
thefactory - The Factory Class Library - v 3.0.0
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 01/04/2012
 * @package: thefactory
 * @subpackage: category
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JTheFactoryCategoryRegister
{
    static function registerModule($app=null)
    {
        if (!$app) $app=JTheFactoryApplication::getInstance();
        if ($app->getIniValue('use_category_management'))
        {
            JLoader::register('JTheFactoryCategoryTable', $app->app_path_admin.'category/table.category.php');
            JHtml::addIncludePath($app->app_path_admin.'category/html');
            JModelLegacy::addIncludePath($app->app_path_admin.'category/models');
            JTheFactoryHelper::loadModuleLanguage('category');
        }
    }
}
