<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: category
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryTableCategory
	 */
	class JTheFactoryTableCategory extends JTable
	{
		/**
		 * id
		 *
		 * @var null
		 */
		public $id = NULL;
		/**
		 * catname
		 *
		 * @var null
		 */
		public $catname = NULL;
		/**
		 * parent
		 *
		 * @var null
		 */
		public $parent = NULL;
		/**
		 * hash
		 *
		 * @var null
		 */
		public $hash = NULL;
		/**
		 * description
		 *
		 * @var null
		 */
		public $description = NULL;
		/**
		 * ordering
		 *
		 * @var null
		 */
		public $ordering = NULL;
		/**
		 * status
		 *
		 * @var null
		 */
		public $status = NULL;

		/**
		 * @param JDatabaseDriver $db
		 */
		public function __construct($db)
		{
			$myApp = JTheFactoryApplication::getInstance();
			parent::__construct($myApp->getIniValue('table', 'categories'), 'id', $db);
		}

		/**
		 * store
		 *
		 * @param bool $updateNulls
		 *
		 * @return bool
		 */
		public function store($updateNulls = FALSE)
		{
			if (!$this->ordering) {
				$db = $this->getDbo();
				$db->setQuery("SELECT MAX(`ordering`) FROM `" . $this->getTableName() . "`");
				$this->ordering = $db->loadResult();
			}
			$lang       = JFactory::getLanguage();
			$this->hash = md5(strtolower(JFilterOutput::stringURLSafe($lang->transliterate($this->catname))));

			return parent::store($updateNulls);
		}
	}
