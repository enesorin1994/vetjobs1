<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: category
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>
<form name = "adminForm" id = "adminForm" method = "post" action = "index.php">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "category.doMoveCategories" />
	<input type = "hidden" name = "cid" value = "<?php echo $this->cid_list; ?>" />

	<table class = "paramlist admintable">
		<tr>
			<td class = "paramlist_key" style = "width:110px !important;"><?php echo JText::_("FACTORY_MOVE_CATEGORIES"); ?></td>
			<td>
				<?php foreach ($this->cats as $cat) {
					echo $cat->catname . "<br />";
				}?>
			</td>
		</tr>
		<tr>
			<td class = "paramlist_key"><?php echo JText::_("FACTORY_TO_CATEGORY"); ?></td>
			<td>
				<?php echo $this->parent; ?>
			</td>
		</tr>
		<tr>
			<td colspan = "2" align = "center" class = "paramlist_key">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan = "2" align = "center">
				<a href = "javascript:void(0);" class="btn btn-large btn-wide" onclick = "document.adminForm.submit();">
					<?php echo JText::_("FACTORY_MOVE"); ?>
				</a>
			</td>
		</tr>
	</table>

</form>
