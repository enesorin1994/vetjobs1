<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: category
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<form action = "index.php" method = "post" name = "adminForm" id = "adminForm">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "category.savecat" />
	<input type = "hidden" name = "id" value = "<?php echo $this->row->id; ?>" />
	<input type = "hidden" name = "ordering" value = "<?php echo $this->row->ordering; ?>" />

	<table class = "adminForm" align = "left">
		<tr valign = "top">
			<td class = "paramlist_key" width = "10%"><label><?php echo JText::_('FACTORY_NAME'); ?> </label>&nbsp;</td>
			<td>
				<input type = "text" name = "catname" value = "<?php echo $this->row->catname; ?>" />
			</td>
		</tr>
		<tr valign = "top">
			<td class = "paramlist_key" width = "10%"><label><?php echo JText::_('FACTORY_DESCRIPTION'); ?> </label>&nbsp;
			</td>
			<td>
				<textarea name = "description" cols = "30" rows = "10"><?php echo $this->row->description; ?></textarea>
			</td>
		</tr>
		<tr valign = "top">
			<td class = "paramlist_key" width = "10%"><label><?php echo JText::_('FACTORY_PARENT'); ?> </label>&nbsp;</td>
			<td>

				<?php echo JHtml::_('factorycategory.select', 'parent', '', $this->row->parent, TRUE, FALSE, TRUE, JText::_('FACTORY_ROOT_CATEGORY')); ?>
			</td>
		</tr>
	</table>

</form>

