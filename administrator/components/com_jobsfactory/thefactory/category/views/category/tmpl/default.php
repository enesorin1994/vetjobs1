<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: category
	 *            -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	$doc = JFactory::getDocument();

	$doc->addStyleDeclaration("
		.cat-quick-add{
			width:100%;
			-moz-box-sizing: border-box;
			-webkit-box-sizing: border-box;
			box-sizing: border-box;
		}
	");

	$doc->addScriptDeclaration("
        	function delecat(nr,enable){
                el=document.getElementById('cat_new_'+nr);
                el.disabled=enable;
            }
            function showQuickAdd()
            {
                el=document.getElementById('quickadd');
                el.style.display='block';
                el=document.getElementById('quickaddbutton');
                el.style.display='none';
            }
         ");
?>

<form action = "index.php" method = "post" name = "adminForm" id = "adminForm">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "" />
	<input type = "hidden" name = "boxchecked" value = "0" />
	<table class = "adminlist table table-condensed" width = "100%">
		<tr>
			<th width = "30"><input type = "checkbox" name = "toggle" value = ""
			                        onclick = "Joomla.checkAll(this);" /></th>
			<th width = "40">
				<a href = "javascript:Joomla.submitbutton('category.savecatorder')" class = "saveorder"
				   title = "<?php echo JText::_('FACTORY_SAVE_ORDERING'); ?>"></a>
			</th>
			<th width = "25%"><?php echo JText::_('FACTORY_CATEGORIES'); ?></th>
			<th width = "*%"><?php echo JText::_('FACTORY_DESCRIPTION'); ?></th>
			<th width = "5%"><?php echo JText::_('FACTORY_STATUS'); ?></th>
		</tr>
		</thead>

		<?php foreach ($this->categories as $row): ?>
			<tr>
				<td><?php echo JHTML::_('grid.id', $row->id, $row->id, FALSE); ?></td>
				<td><input name = 'order_<?php echo $row->id; ?>' type = 'text' class = 'inputbox  input-mini' size = '1'
				           value = '<?php echo $row->ordering; ?>' /></td>
				<td><span style = "padding-left: <?php echo $row->depth * 20 ?>px;">
            <sup>|<span style = "text-decoration:underline;">&nbsp;&nbsp;&nbsp;</span></sup>
            <a href = 'index.php?option=<?php echo APP_EXTENSION; ?>&task=category.editcat&cid=<?php echo $row->id; ?>'><?php echo $row->catname; ?></a>
            </span>
				</td>
				<td><?php echo $row->description; ?></td>
				<td><?php
						if ($row->status) {
							$img  = 'tick.png';
							$alt  = JText::_('FACTORY_PUBLISHED__UNPUBLISH');
							$task = "category.unpublish_cat";
						} else {
							$img  = 'publish_x.png';
							$alt  = JText::_('FACTORY_UNPUBLISHED__PUBLISH');
							$task = "category.publish_cat";
						}


					?>
					<span class = "editlinktip hasTip" title = "<?php echo JText::_('FACTORY_PUBLISH_INFORMATION') . " " . $alt; ?>">
    		 	<a href = "index.php?option=<?php echo APP_EXTENSION; ?>&task=<?php echo $task; ?>&cid[]=<?php echo $row->id; ?>">
			        <?php echo JTheFactoryAdminHelper::imageAdmin($img, $alt); ?>
                </span>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>

	<div id = "quickaddbutton">
		<button class = "button btn" onclick = "showQuickAdd();"
		        type = "button"><?php echo JText::_('FACTORY_QUICKADD'); ?></button>
	</div>
	<div style = "display:none;" id = "quickadd"><br />
		<span style = "font-size:18px;"><?php echo JText::_('FACTORY_TO_QUICK_ADD_CATEGORIES'); ?></span><br /><br />
		<textarea name = "quickadd" class = "cat-quick-add" cols = "40" rows = "10"></textarea><br />

		<div class = "form-actions">
			<button class = "btn btn-large"
			        onclick = "Joomla.submitform('category.quickaddcat');"><?php echo JText::_('FACTORY_QUICKADD'); ?></button>
		</div>
	</div>
</form>
