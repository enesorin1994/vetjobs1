<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: category
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	jimport('joomla.application.component.model');
	jimport('joomla.application.component.helper');

	/**
	 * Class JTheFactoryModelCategoryJoomla
	 */
	class JTheFactoryModelCategoryJoomla extends JModelLegacy
	{
		/**
		 *
		 */
		public function __construct()
		{
			parent::__construct();
		}

		/**
		 * getCategoryCount
		 *
		 * @param bool $include_disabled
		 *
		 * @return mixed
		 */
		public function getCategoryCount($include_disabled = FALSE)
		{

			$db    = $this->getDbo();
			$query = $db->getQuery(TRUE);

			$where = array(
				'extension=' . $db->quote(APP_EXTENSION),
				($include_disabled ? '1' : 'published=1')
			);

			$query->select('COUNT(1)')
				->from('#__categories')
				->where($where);

			$db->setQuery($query);

			return $db->loadResult();
		}

		/**
		 * getCategoryTree
		 *
		 * @param int  $parentId
		 * @param bool $include_disabled
		 *
		 * @return mixed
		 */
		public function getCategoryTree($parentId = 1, $include_disabled = FALSE)
		{

			$db    = $this->getDbo();
			$query = $db->getQuery(TRUE);

			$where = array(
				'c.extension=' . $db->quote(APP_EXTENSION),
				'p.id=' . $db->quote($parentId)
			);

			if (!$include_disabled) {
				$where[] = 'c.published=1';
			}

			$query->select('c.*, c.level-p.level AS depth')
				->from('#__categories AS c')
				->leftJoin('#__categories AS p ON c.lft BETWEEN p.lft AND p.rgt')
				->where($where)
				->order('c.lft');

			$db->setQuery($query);

			return $db->loadObjectList('id');
		}

		/**
		 * getCategoryPathString
		 *
		 * @param $catid
		 *
		 * @return mixed
		 */
		public function getCategoryPathString($catid)
		{

			$db    = $this->getDbo();
			$query = $db->getQuery(TRUE);

			$query->select('path')
				->from('#__categories')
				->where('id=' . $db->quote($catid));

			$db->setQuery($query);

			return $db->loadResult();
		}

		/* TODO Factory: develop category breadcrumb on category.joomla model */
		/**
		 * getCategoryBreadcrumb
		 *
		 * @param      $catid
		 * @param null $link
		 *
		 * @return array
		 */
		public function getCategoryBreadcrumb($catid, $link = NULL)
		{
			$cat_array = array();

			return $cat_array;
		}

		/**
		 * delCategory
		 *
		 * @param $cids
		 *
		 * @return int
		 */
		public function delCategory($cids)
		{
			if (!is_array($cids) && $cids) $cids = array($cids);
			if (!count($cids)) return 0;

			$db    = $this->getDbo();
			$query = $db->getQuery(TRUE);

			$query->delete('#__categories')
				->where("`id` IN (" . implode(',', $cids) . ")");
			$db->setQuery($query);
			$db->execute();

			$nr = $db->getAffectedRows();

			$query->clear();
			$query->update('#__categories')
				->set("`parent`=0")
				->where("`parent` IN (" . implode(',', $cids) . ")");
			$db->execute();

			return $nr;
		}

		/**
		 * quickAddFromText
		 *
		 * @param     $quicktext
		 * @param int $parentid
		 *
		 * @return bool
		 */
		public function quickAddFromText($quicktext, $parentid = 0)
		{

			if ('WIN' == substr(PHP_OS, 0, 3)) {
				$separator = "\r\n";
			} else {
				$separator = "\n";
			}

			$textcats = explode($separator, $quicktext);

			$stack = array($parentid);

			$last_id        = 0;
			$prevcat_spaces = 0;

			JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_categories/tables');
			$cattable = JTable::getInstance('Category', 'CategoriesTable');

			foreach ($textcats as $key => $cat) {
				$cat = trim($cat);
				if (!$cat) continue; //skip empty categories
				$cat_spaces = 0;
				if (preg_match('/^[ ]*/', $cat, $matches)) $cat_spaces = strlen($matches[0]);
				$cat = trim($cat);
				if (!$cat) continue; //skip empty categories

				if ($cat_spaces > $prevcat_spaces) array_push($stack, $last_id);
				if ($cat_spaces < $prevcat_spaces) {
					$diff_level = $prevcat_spaces - $cat_spaces;
					for ($j = 0; $j < $diff_level; $j++) {
						array_pop($stack);
					}
				}
				$cattable->id              = 0;
//				$cattable->parent_id       = end($stack);
				$cattable->title           = $cat;
                $cattable->extension       = APP_EXTENSION;
				$cattable->alias           = '';
				$cattable->published       = '1';
				$cattable->access          = '1';
				$cattable->created_user_id = '0';
				$cattable->language        = '*';
                $cattable->level           = null;
                $cattable->setLocation(end($stack),'last-child');//parent_id

				if (!$cattable->check()) continue;

				if (!$cattable->store()) {
					if ($cattable->getError() == JText::_('JLIB_DATABASE_ERROR_CATEGORY_UNIQUE_ALIAS')) {
						$cattable->alias = $cattable->alias . '_' . uniqid();
						if (!$cattable->store()) continue;
					} else continue;
				}

            		// Rebuild the path for the category:
            		if (!$cattable->rebuildPath($cattable->id))
            		{
            			$this->setError($cattable->getError());            
            			return false;
            		}
				$prevcat_spaces = $cat_spaces;
				$last_id        = $cattable->id;
			}

			return TRUE;
		}

		/**
		 * Detect switching parent category with its child
		 *
		 * - For Joomla categories this is not necessary
		 * - It is created just for maintaining consistency
		 * - in adapter class
		 *
		 * @param $catMoved
		 * @param $catMoveTo
		 *
		 * @return null
		 */
		public function detectParentSwitchedWithChild($catMoved, $catMoveTo)
		{
			return NULL;
		}
	}
