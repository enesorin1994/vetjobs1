<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: category
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	jimport('joomla.application.component.model');
	jimport('joomla.application.component.helper');

	/**
	 * Class JTheFactoryModelCategoryLegacy
	 */
	class JTheFactoryModelCategoryLegacy extends JModelLegacy
	{
		/**
		 * @var null|string
		 */
		private $category_table = NULL;
		/**
		 * @var null
		 */
		private $_category_tree = NULL;

		public function __construct()
		{
			parent::__construct();
			$myApp = JTheFactoryApplication::getInstance();

			$this->category_table = $myApp->getIniValue('table', 'categories');
		}

		public function getCategoryTable()
		{
			return $this->category_table;
		}

		/**
		 * @param      $parent
		 * @param bool $include_disabled
		 *
		 * @return mixed
		 */
		public function getCategoryCount($parent = -1, $include_disabled = FALSE)
		{
			$w = array();

			if ($parent >= 0) $w[] = "`parent`=$parent";
			if (!$include_disabled) $w[] = "`status`=1";

			$where = count($w) ? (" WHERE " . implode(' AND ', $w)) : "";

			$db = $this->getDbo();
			$q  = "SELECT COUNT(*) FROM `{$this->category_table}` AS `c` {$where}";
			$db->setQuery($q);

			return $db->loadResult();
		}

		/**
		 * @param int  $parent_cat
		 * @param bool $include_disabled
		 *
		 * @return mixed
		 */
		public function getCategoryTree($parent_cat = 0, $include_disabled = FALSE)
		{
			if (isset($this->_category_tree[$parent_cat][$include_disabled])) // already got
				return $this->_category_tree[$parent_cat][$include_disabled];

			$db   = $this->getDbo();
			$tree = array();

			if ($parent_cat) {
				$disabled = $include_disabled ? "" : " and `status`=1";
				$q        = "SELECT *,0 AS `depth`,
					(SELECT count(*)  FROM `{$this->category_table}`  WHERE parent=c.id {$disabled}) AS nr_children
					FROM `{$this->category_table}` AS `c` WHERE `id`='$parent_cat'  ORDER BY `ordering`";
				$db->setQuery($q);
				$tree[] = $db->loadObject();
			}

			if (count($tree) <= 0 || $tree[0]->nr_children)
				$tree = array_merge($tree, self::getCategoryTreeRecursive($parent_cat, $include_disabled));
			$this->_category_tree[$parent_cat][$include_disabled] = $tree;

			return $this->_category_tree[$parent_cat][$include_disabled];
		}

		/**
		 * @param int  $parent_cat
		 * @param bool $include_disabled
		 *
		 * @return array
		 */
		private function getCategoryTreeRecursive($parent_cat = 0, $include_disabled = FALSE)
		{
			static $depth = 1;

			$disabled = $include_disabled ? "" : " and `status`=1";

			$db = new JTheFactoryDatabaseCache('category');
			$q  = "SELECT *,{$depth} AS `depth`,
					(SELECT count(*)  FROM `{$this->category_table}`  WHERE parent=c.id {$disabled}) AS nr_children
					FROM `{$this->category_table}` AS `c` WHERE `parent`='$parent_cat' ORDER BY `ordering`";
			$db->setQuery($q);
			$tree       = array();
			$categories = $db->loadObjectList();
			foreach ($categories as $cat) {
				$tree[] = $cat;
				if ($cat->nr_children) {
					$depth++;
					$tree = array_merge($tree, self::getCategoryTreeRecursive($cat->id, $include_disabled));
					$depth--;
				}
			}

			return $tree;
		}

		/**
		 * @param $catid
		 *
		 * @return array
		 */
		private function getCategoryPathArray($catid)
		{
			$db         = $this->getDbo();
			$path_array = array();
			while ($catid) {
				$db->setQuery("SELECT * FROM `{$this->category_table}` WHERE `id`='$catid'");
				$cat          = $db->loadObject();
				$path_array[] = $cat;

				$catid = $cat->parent;
			}

			return array_reverse($path_array, TRUE);
		}

		/**
		 * @param $catid
		 *
		 * @return string
		 */
		public function getCategoryPathString($catid)
		{
			$cat_array = $this->getCategoryPathArray($catid);
			$catstring = "";
			foreach ($cat_array as $cat) $catstring .= (($catstring) ? " &#187; " : "") . $cat->catname;

			return $catstring;

		}

		/**
		 * @param      $catid
		 *
		 * @param null $link
		 *
		 * @return string
		 */
		public function getCategoryBreadcrumb($catid, $link = NULL)
		{
			$cat_array = $this->getCategoryPathArray($catid);
			foreach ($cat_array as $objCat) {
				$objCat->link = JRoute::_($link . $objCat->id);
			}

			return $cat_array;
		}

		/**
		 * @param $cids
		 *
		 * @return mixed
		 */
		public function delCategory($cids)
		{
			if (!is_array($cids) && $cids) $cids = array($cids);
			if (!count($cids)) return 0;

			$db = $this->getDbo();
			$db->setQuery("DELETE FROM `{$this->category_table}` WHERE `id` IN (" . implode(',', $cids) . ")");
			$db->execute();
			$nr = $db->getAffectedRows();
			$db->setQuery("UPDATE `{$this->category_table}` SET `parent`=0 WHERE `parent` IN (" . implode(',', $cids) . ")");
			$db->execute();

			return $nr;
		}

		/**
		 * @param     $quicktext
		 * @param int $parentid
		 *
		 * @return void
		 */
		function quickAddFromText($quicktext, $parentid = 0)
		{

			if ('WIN' == substr(PHP_OS, 0, 3)) {
				$separator = "\r\n";
			} else {
				$separator = "\n";
			}

			$textcats = explode($separator, $quicktext);

			$stack = array($parentid);

			$last_id = NULL;

			$prevcat_spaces = 0;
			$ordering       = 0;
			$cattable       = JTable::getInstance('Category', 'JTheFactoryTable');

			$lang = JFactory::getLanguage();

			foreach ($textcats as $key => $cat) {

				$cat = strip_tags($cat);
				if (!$cat) continue; //skip empty categories
				$cat_spaces = 0;
				if (preg_match('/^[ ]*/', $cat, $matches)) $cat_spaces = strlen($matches[0]);

				if ($cat_spaces > $prevcat_spaces) array_push($stack, $last_id);
				if ($cat_spaces < $prevcat_spaces) {
					$diff_level = $prevcat_spaces - $cat_spaces;
					for ($j = 0; $j < $diff_level; $j++) {
						array_pop($stack);
					}
				}
				$cattable->id       = NULL;
				$cattable->catname  = $cat;
				$cattable->parent   = end($stack);
				$cattable->ordering = $ordering;
				$cattable->hash     = md5(strtolower(JTheFactoryHelper::str_clean($lang->transliterate($cat))));
				$cattable->store();

				$ordering       = $cattable->ordering + 1;
				$prevcat_spaces = $cat_spaces;
				$last_id        = $cattable->id;


			}
		}

		/**
		 * @param string $catid     Categories to move (comma delimited)
		 * @param int    $newparent New Parent id
		 *
		 * @return int                 Nr Categories moved
		 */
		public function moveCategory($catid, $newparent)
		{
			$db = $this->getDBO();

			// Move cid to new parent
			$db->setQuery("UPDATE `{$this->category_table}`
						   SET `parent` = '{$newparent}'
						   WHERE `id` IN ({$catid})
			");
			$db->execute();
			$nr = $db->getAffectedRows();

			return $nr;
		}

		/**
		 * Detect switching parent category with its child
		 *
		 * @param $catMoved
		 * @param $catMoveTo
		 *
		 * @return bool
		 */
		public function detectParentSwitchedWithChild($catMoved, $catMoveTo)
		{
			$db = $this->getDbo();

			$catRoot = 0;
			static $detected = FALSE;

			$db->setQuery("SELECT `parent` FROM `{$this->category_table}` WHERE `id` = '{$catMoveTo}'");
			$parentCatMoveTo = $db->loadResult();

			if ($catRoot == $parentCatMoveTo) {
				$detected = FALSE;
			} else if ($parentCatMoveTo && ($parentCatMoveTo == $catMoved)) {
				$detected = TRUE;
			} else {
				$this->detectParentSwitchedWithChild($catMoved, $parentCatMoveTo);
			}

			return $detected;
		}
	}
