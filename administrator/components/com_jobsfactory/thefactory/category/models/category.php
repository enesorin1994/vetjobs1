<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: category
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');


	jimport('joomla.application.component.model');
	jimport('joomla.application.component.helper');

	/**
	 * JTheFactoryModelCategory
	 */
	class JTheFactoryModelCategory extends JModelLegacy
	{
		/**
		 * @var  JThefactoryModelCategoryLegacy $catmodel
		 */
		private $catmodel;

		/**
		 *
		 */
		public function __construct()
		{
			parent::__construct();
			$myApp = JTheFactoryApplication::getInstance();

			$legacy = $myApp->getIniValue('use_legacy_model', 'categories');
			if ($legacy) {
				require_once(dirname(__FILE__) . '/category.legacy.php');
				$this->catmodel = JModelLegacy::getInstance('CategoryLegacy', 'JThefactoryModel');
			} else {
				require_once(dirname(__FILE__) . '/category.joomla.php');
				$this->catmodel = JModelLegacy::getInstance('CategoryJoomla', 'JThefactoryModel');
			}

		}

		/**
		 * @param      $parent
		 * @param bool $include_disabled
		 *
		 * @return mixed
		 */
		public function getCategoryCount($parent = -1, $include_disabled = FALSE)
		{
			return $this->catmodel->getCategoryCount($parent, $include_disabled);
		}

		/**
		 * @param int  $parent_cat
		 * @param bool $include_disabled
		 *
		 * @return mixed
		 */
		public function getCategoryTree($parent_cat = 0, $include_disabled = FALSE)
		{
			return $this->catmodel->getCategoryTree($parent_cat, $include_disabled);
		}

		/**
		 * @param $catid
		 *
		 * @return string
		 */
		public function getCategoryPathString($catid)
		{
			return $this->catmodel->getCategoryPathString($catid);
		}

		/**
		 * @param      $catid
		 * @param null $link
		 *
		 * @return mixed
		 */
		public function getCategoryBreadcrumb($catid, $link = NULL)
		{
			return $this->catmodel->getCategoryBreadcrumb($catid, $link);
		}

		/**
		 * @param $cids
		 *
		 * @return mixed
		 */
		public function delCategory($cids)
		{
			return $this->catmodel->delCategory($cids);
		}

		/**
		 * @param     $quicktext
		 * @param int $parentid
		 *
		 * @return
		 */
		public function quickAddFromText($quicktext, $parentid = 0)
		{
			return $this->catmodel->quickAddFromText($quicktext, $parentid);
		}

		/**
		 * @param string $catid     Categories to move (comma delimited)
		 * @param int    $newparent New Parent id
		 *
		 * @return int                 Nr Categories moved
		 */
		public function moveCategory($catid, $newparent)
		{
			return $this->catmodel->moveCategory($catid, $newparent);
		}

		/**
		 * Detect switching parent category with its child
		 *
		 * @param $catMoved
		 * @param $catMoveTo
		 *
		 * @return mixed
		 */
		public function detectParentSwitchedWithChild($catMoved, $catMoveTo)
		{
			return $this->catmodel->detectParentSwitchedWithChild($catMoved, $catMoveTo);
		}

		/**
		 * @return mixed Return categories (legacy only) table name
		 */
		public function getCategoryTable()
		{
			$myApp  = JTheFactoryApplication::getInstance();
			$legacy = $myApp->getIniValue('use_legacy_model', 'categories');

			if ($legacy) {
				return $this->catmodel->getCategoryTable();
			}

			return NULL;
		}

	}
