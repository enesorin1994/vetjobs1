<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: events
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryFiltersHelper
	 */
	class JTheFactoryFiltersHelper
	{
		/**
		 * @var array
		 */
		protected static $filters = array();

		/**
		 * registerFilters
		 *    registers all Classes in the inputFolder. Classes in the folder must inherit JTheFactoryFilters
		 *    name convention JTheFactoryFilter[name] must be in file [name].php
		 *
		 * @static
		 *
		 * @param string $filterfiles_path folder containing classes
		 */
		public static function registerFilters($filterfiles_path)
		{
			jimport("joomla.filesystem.folder");
			jimport("joomla.filesystem.file");

			$files = JFolder::files($filterfiles_path, '\.php$');
			if (count($files)) {
				foreach ($files as $file) {
					$classname = 'JTheFactoryFilter' . ucfirst(substr($file, 0, strlen($file) - 4));

					require_once "{$filterfiles_path}/{$file}";

					if (class_exists($classname)) {
						self::$filters[] = new $classname;
					}
				}
			}
		}

		/**
		 * Trigger filter
		 *
		 * @param string $filter
		 * @param null   $args
		 *
		 * @return null
		 */
		public static function trigger($filter, $args = NULL)
		{

			foreach (self::$filters as $objFilter) {
				if (is_callable(array($objFilter, $filter))) {
					return call_user_func(array($objFilter, $filter), $args);
				}
			}

			return NULL;
		}
	}
