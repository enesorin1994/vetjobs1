<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: custom_fields
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	class JTheFactoryFiltersRegister
	{
		/**
		 * registerModule
		 *
		 * @static
		 *
		 * @param null $app
		 */
		public static function registerModule($app = NULL)
		{
			if (!$app) $app = JTheFactoryApplication::getInstance();

			if ($app->getIniValue('use_filters')) {
				JLoader::register('JTheFactoryFilters', $app->app_path_admin . 'filters/filters.class.php');
				JLoader::register('JTheFactoryFiltersHelper', $app->app_path_admin . 'filters/filters.helper.php');

				if ($app->frontpage) {
					JTheFactoryFiltersHelper::registerFilters(JPATH_SITE . '/components/com_' . strtolower($app->getIniValue('name')) . '/filters');
				} else {
					JTheFactoryFiltersHelper::registerFilters(JPATH_SITE . '/administrator/components/com_' . strtolower($app->getIniValue('name')) . '/filters');
				}

			}

		}
	} // End Class
