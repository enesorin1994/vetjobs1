<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegration
	 */
	class JTheFactoryIntegration
	{
		/**
		 * mode
		 *
		 * @var null
		 */
		var $mode = NULL;
		/**
		 * table
		 *
		 * @var null
		 */
		var $table = NULL;
		/**
		 * keyfield
		 *
		 * @var null
		 */
		var $keyfield = NULL;


		/**
		 * detectIntegration
		 *
		 * @return bool
		 */
		public static function detectIntegration()
		{
			//abstract
			return FALSE;
		}

		/**
		 * getUserProfile
		 *
		 * @param $userid
		 *
		 * @return bool
		 */
		public function getUserProfile($userid)
		{
			//abstract
			return FALSE;

		}

		/**
		 * getIntegrationFields
		 *
		 * @return array
		 */
		public static function getIntegrationFields()
		{
			$MyApp = JTheFactoryApplication::getInstance();
			$flist = $MyApp->getIniValue("fields_list", "profile-integration");
			if ($flist) return explode(",", $flist);
			else
				return array();

		}

		/**
		 * getIntegrationArray
		 *
		 * @return array
		 */
		public static function getIntegrationArray()
		{
			//abstract
			return array();
		}

		/**
		 * getProfileLink
		 *
		 * @param $userid
		 *
		 * @return null
		 */
		public function getProfileLink($userid)
		{
			//abstract
			return NULL;
		}

		/**
		 * getUserList
		 *
		 * @param int  $limitstart
		 * @param int  $limit
		 * @param null $filters
		 * @param null $ordering
		 *
		 * @return null
		 */
		public function getUserList($limitstart = 0, $limit = 30, $filters = NULL, $ordering = NULL)
		{
			return NULL;
		}

		/**
		 * getUserListCount
		 *
		 * @param null $filters
		 *
		 * @return int
		 */
		public function getUserListCount($filters = NULL)
		{
			return 0;
		}

		/**
		 * checkProfile
		 *
		 * @param null $userid
		 *
		 * @return bool
		 */
		public function checkProfile($userid = NULL)
		{
			return TRUE;
		}

		/**
		 * setGMapCoordinates
		 *
		 * @param $userid
		 * @param $x
		 * @param $y
		 *
		 * @return bool|mixed
		 */
		public function setGMapCoordinates($userid, $x, $y)
		{
			$db     = JFactory::getDBO();
			$arr    = $this->getIntegrationArray();
			$xfield = $arr['googleMaps_x'];
			$yfield = $arr['googleMaps_y'];

			if (!$xfield || !$yfield) return FALSE;

			$query = "UPDATE `{$this->table}` AS `profile`
					 SET `profile`.`{$xfield}`='$x',
					       `profile`.`{$yfield}`='$y'
					 WHERE `profile`.`{$this->keyfield}`='{$userid}'
				";
			$db->setQuery($query);

			return $db->execute();
		}

	} // End Class
