<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegrationCB
	 */
	class JTheFactoryIntegrationCB extends JTheFactoryIntegration
	{

		/**
		 * mode
		 *
		 * @var string
		 */
		var $mode = "cb";
		/**
		 * table
		 *
		 * @var string
		 */
		var $table = "#__comprofiler";
		/**
		 * keyfield
		 *
		 * @var string
		 */
		var $keyfield = "user_id";

		/**
		 * detectIntegration
		 *
		 * @return bool
		 */
		public static function detectIntegration()
		{
			$database = JFactory::getDbo();
			$database->setQuery("SELECT COUNT(*) FROM `#__extensions` WHERE `element`='com_comprofiler'");
			$database->loadResult();

			return $database->loadResult() > 0;
		}

		/**
		 * getUserProfile
		 *
		 * @param int $userid
		 *
		 * @return array
		 */
		public function getUserProfile($userid = 0)
		{
			$cbinclude = JPATH_ADMINISTRATOR . "/components/com_comprofiler/plugin.foundation.php";
			if (!file_exists($cbinclude)) {
				JFactory::getApplication()->enqueueMessage(JText::_('FACTORY_CB_NOT_INSTALLED'), 'warning');

				return array();

			}
			// CB Fields
			require_once($cbinclude);

			cbimport('cb.database');
			cbimport('cb.tables');

			$cbUser           = CBUser::getInstance($userid);
			$userdata         = $cbUser->getUserData();
			$userdata->userid = $userid;

			return get_object_vars($userdata);
		}

		/**
		 * getIntegrationArray
		 *
		 * @return mixed
		 */
		public static function getIntegrationArray()
		{

			static $fieldmap = NULL;

			if (!$fieldmap) {
				$MyApp     = JTheFactoryApplication::getInstance();
				$tablename = $MyApp->getIniValue("field_map_table", "profile-integration");


				$database = JFactory::getDbo();
				$database->setQuery("SELECT `field`,`assoc_field` FROM `" . $tablename . "`");
				$r = $database->loadAssocList();

				for ($i = 0; $i < count($r); $i++) {
					$fieldmap[$r[$i]['field']] = $r[$i]['assoc_field'];
				}
			}

			return $fieldmap;
		}

		/**
		 * getProfileLink
		 *
		 * @param $userid
		 *
		 * @return string
		 */
		public function getProfileLink($userid)
		{
			//abstract
			$link = JURI::root() . "index.php?option=com_comprofiler";
			if ($userid) $link .= "&user={$userid}&task=userprofile";

			return $link;
		}

		/**
		 * _buildWhere
		 *
		 * @param $filters
		 *
		 * @return array
		 */
		public function _buildWhere($filters)
		{
			$integrationArray = $this->getIntegrationArray();
			$where            = array();
			$w                = array();
			if (count($filters)) foreach ($filters as $key => $value) if (is_array($value)) {
				foreach ($value as $k => $v) {
					if (isset($integrationArray[$k]) && $integrationArray[$k]) {
						if (strpos($v, '%') !== FALSE) $w[] = 'profile.' . $integrationArray[$k] . " LIKE '$v'";
						else
							$w[] = 'profile.' . $integrationArray[$k] . "='$v'";
					}
					if ($k == 'username') {
						if (strpos($v, '%') !== FALSE) $w[] = "u.username LIKE '$v'";
						else
							$w[] = "u.username='$v'";
					}
				}
				if (count($w)) $where[] = '(' . implode(' OR ', $w) . ')';
			} else {
				if (isset($integrationArray[$key]) && $integrationArray[$key]) {
					if (strpos($value, '%') !== FALSE) $where[] = 'profile.' . $integrationArray[$key] . " LIKE '$value'";
					else
						$where[] = 'profile.' . $integrationArray[$key] . "='$value'";
				}
				if ($key == 'username') {
					if (strpos($v, '%') !== FALSE) $w[] = "u.username LIKE '$v'";
					else
						$w[] = "u.username='$v'";
				}
			}

			return $where;
		}

		/**
		 * getUserList
		 *
		 * @param int  $limitstart
		 * @param int  $limit
		 * @param null $filters
		 * @param null $ordering
		 *
		 * @return mixed
		 */
		public function getUserList($limitstart = 0, $limit = 30, $filters = NULL, $ordering = NULL)
		{
			$db                = JFactory::getDbo();
			$integrationArray  = $this->getIntegrationArray();
			$integrationFields = $this->getIntegrationFields();

			$colums = array('u.id as userid', 'u.username', 'profile.*');

			foreach ($integrationFields as $field) if ($integrationArray[$field]) $colums[] = 'profile.' . $integrationArray[$field] . " as `{$field}`";
			else
				$colums[] = "'' as `{$field}`"; //field not defined, thus provided empty


			$query = "select " . implode(',', $colums);

			$query .= " from #__users u ";
			$query .= " left join #__comprofiler profile on u.id=profile.user_id ";

			$where = $this->_buildWhere($filters);

			if (count($where)) $query .= " WHERE " . implode(' AND ', $where);

			if (count($ordering)) $query .= " ORDER BY " . implode(',', $ordering);

			$db->setQuery($query, $limitstart, $limit);
			$list = $db->loadObjectList();

			return $list;
		}

		/**
		 * getUserListCount
		 *
		 * @param null $filters
		 *
		 * @return mixed
		 */
		public function getUserListCount($filters = NULL)
		{
			$db    = JFactory::getDbo();
			$query = "select count(*) ";

			$query .= " from #__users u ";
			$query .= " left join #__comprofiler profile on u.id=profile.user_id ";

			$where = $this->_buildWhere($filters);

			if (count($where)) $query .= " WHERE " . implode(',', $where);

			$db->setQuery($query);
			$total = $db->loadResult();

			return $total;
		}

		/**
		 * checkProfile
		 *
		 * @param null $userid
		 *
		 * @return bool
		 */
		public function checkProfile($userid = NULL)
		{
			return TRUE;
		}
	} // End Class
