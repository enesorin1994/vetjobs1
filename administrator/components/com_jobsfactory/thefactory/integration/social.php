<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegrationSocial
	 */
	class JTheFactoryIntegrationSocial extends JTheFactoryIntegration
	{
		/**
		 * mode
		 *
		 * @var string
		 */
		public $mode = "social";
		/**
		 * table
		 *
		 * @var string
		 */
		public $table = "#__socialfactory_users";
		/**
		 * keyfield
		 *
		 * @var string
		 */
		public $keyfield = "user_id";


		/**
		 * detectIntegration
		 *
		 * @return bool
		 */
		public static function detectIntegration()
		{
			$database = JFactory::getDBO();
			$database->setQuery("SELECT COUNT(*) FROM `#__extensions` WHERE `element`='com_socialfactory'");

			return $database->loadResult() > 0;
		}

		/**
		 * getUserProfile
		 *
		 * @param int $userid
		 *
		 * @return array
		 */
		public function getUserProfile($userid = 0)
		{
			$db    = JFactory::getDBO();
			$query = $db->getQuery(TRUE);
			$query->select('*')
				->from($this->table)
				->where('user_id = ' . $db->quote($userid));

			$db->setQuery($query);
			$userdata = $db->loadObject();
			// Trick inserted for compatibility with all of the others thefactory components
			// where is used userid instead of user_id used by socialfactory
			$userdata->userid = isset($userdata->user_id) ? $userdata->user_id : NULL;


			if ($userdata) {
				// Extract json encoded, profile field
				if (property_exists($userdata, 'profile')) {
					foreach (json_decode($userdata->profile) as $profileField => $profileFieldValue) {
						$userdata->$profileField = $profileFieldValue;
					}
				}

				foreach ($userdata as $k => $v) {

					// Ignore other fields than custom fields
					$pos_1 = strpos($k, 'field_');
					$pos_2 = strpos($k, 'visible_');
					if (FALSE === $pos_1 || FALSE !== $pos_2) {
						continue;
					}

					$fieldId = filter_var($k, FILTER_SANITIZE_NUMBER_INT);
					// Get correspondent field name for this user data profile field from #__socialfactory_fields
					$query = "SELECT `name` FROM `#__socialfactory_fields` WHERE `id` = '{$fieldId}'";
					$db->setQuery($query);
					$field = strtolower($db->loadResult());

					if (!empty($field)) {
						$replacedKey            = str_replace(' ', '_', $field);
						$userdata->$replacedKey = $v;
					}

				}

				return get_object_vars($userdata);
			} else {
				return array();
			}
		}

		/**
		 * getIntegrationArray
		 *
		 * @return array
		 */
		public static function getIntegrationArray()
		{
			$MyApp     = JTheFactoryApplication::getInstance();
			$tablename = $MyApp->getIniValue("field_map_table", "profile-integration");
			$fieldmap  = array();

			$database = JFactory::getDBO();
			$database->setQuery("SELECT `field`, `assoc_field` FROM `" . $tablename . "`");
			$r = $database->loadAssocList();

			for ($i = 0; $i < count($r); $i++) {
				$fieldmap[$r[$i]['field']] = $r[$i]['assoc_field'];
			}

			return $fieldmap;

		}

		/**
		 * getProfileLink
		 *
		 * @param $userid
		 *
		 * @return string
		 */
		public function getProfileLink($userid)
		{

			$dbo   = JFactory::getDBO();
			$query = $dbo->getQuery(TRUE)
				->select('p.id,p.menu_item_id')
				->from('#__socialfactory_pages AS p')
				->where('type = "showprofile"');
			$dbo->setQuery($query);

			$page = $dbo->loadObject();

			$link = JURI::root() . "index.php?option=com_socialfactory&view=page&task=page.display&page_id=" . $page->id;
			if ($userid) {
				$link .= "&user_id=" . $userid;
			}
			$link .= '&Itemid=' . $page->menu_item_id;

			return $link;
		}

		/**
		 * _buildWhere
		 *
		 * @param $filters
		 *
		 * @return array
		 */
		public function _buildWhere($filters)
		{
			$integrationArray = $this->getIntegrationArray();
			$where            = array();
			$w                = array();

			if (count($filters)) foreach ($filters as $key => $value) {
				if (is_array($value)) {
					foreach ($value as $k => $v) if (isset($integrationArray[$k])) {
						if (strpos($v, '%') !== FALSE) $w[] = 'sfProfile.' . $integrationArray[$k] . " LIKE '$v'";
						else
							$w[] = 'sfProfile.' . $integrationArray[$k] . "='$v'";
					}
					if (count($w)) $where[] = '(' . implode(' OR ', $w) . ')';

				} elseif (isset($integrationArray[$key])) {
					if (strpos($value, '%') !== FALSE) $where[] = 'sfProfile.' . $integrationArray[$key] . " LIKE '$value'";
					else
						$where[] = 'sfProfile.' . $integrationArray[$key] . "='$value'";
				}
			}

			return $where;
		}

		/**
		 * getUserList
		 *
		 * @param int  $limitstart
		 * @param int  $limit
		 * @param null $filters
		 * @param null $ordering
		 *
		 * @return mixed
		 */
		public function getUserList($limitstart = 0, $limit = 30, $filters = NULL, $ordering = NULL)
		{
			$db                = JFactory::getDBO();
			$integrationArray  = $this->getIntegrationArray();
			$integrationFields = $this->getIntegrationFields();

			$colums = array('u.username', 'sFprofile.*');

			foreach ($integrationFields as $field) {
				if ($field == 'googleX' || $field == 'googleY') continue;
				if ($integrationArray[$field]) $colums[] = 'sfProfile.' . $integrationArray[$field] . " AS `{$field}`";
				else
					$colums[] = "'' AS `{$field}`"; //field not defined, thus provided empty
			}

			$query = "SELECT " . implode(',', $colums);

			$query .= " FROM `#__users` AS `u` ";
			$query .= " LEFT JOIN `#__socialfactory_users` AS `sfProfile` ON `u`.`id` = `sfProfile`.`user_id` ";

			$where = $this->_buildWhere($filters);

			if (count($where)) $query .= " WHERE " . implode(',', $where);

			if (count($ordering)) $query .= " ORDER BY " . implode(',', $ordering);

			$db->setQuery($query, $limitstart, $limit);
			$list = $db->loadObjectList();

			return $list;

		}

		/**
		 * getUserListCount
		 *
		 * @param null $filters
		 *
		 * @return mixed
		 */
		public function getUserListCount($filters = NULL)
		{
			$db    = JFactory::getDBO();
			$query = "SELECT COUNT(*) ";

			$query .= " FROM `#__users` AS `u` ";
			$query .= " LEFT JOIN `#__socialfactory_users` AS `sfProfile` ON `u`.`id` = `sfProfile`.`user_id` ";

			$where = $this->_buildWhere($filters);

			if (count($where)) $query .= " WHERE " . implode(',', $where);

			$db->setQuery($query);
			$total = $db->loadResult();

			return $total;
		}

		/**
		 * checkProfile
		 *
		 * @param null $userid
		 *
		 * @return bool
		 */
		public function checkProfile($userid = NULL)
		{
			return TRUE;
		}

	}
