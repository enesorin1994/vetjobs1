<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegrationCBHelper
	 */
	class JTheFactoryIntegrationCBHelper
	{
		/**
		 * InstallCBPlugin
		 *
		 * @param $plugintitle
		 * @param $tabtitle
		 * @param $pluginname
		 * @param $folder
		 * @param $class
		 *
		 * @return mixed
		 */
		public static function InstallCBPlugin($plugintitle, $tabtitle, $pluginname, $folder, $class)
		{
			$database = JFactory::getDBO();
			jimport('joomla.filesystem.file');
			jimport('joomla.filesystem.folder');

			$query = "SELECT `id` FROM `#__comprofiler_plugin` WHERE `element` = '$pluginname'";
			$database->setQuery($query);
			$plugid = $database->loadResult();

			if (!$plugid) {
				$query = "INSERT INTO `#__comprofiler_plugin` SET
			`name` = '$plugintitle',
			`element` = '$pluginname',
			`type` = 'user',
			`folder` = '$folder',
			`ordering` = 99,
			`published` = 1,
			`iscore` = 0
		";
				$database->setQuery($query);
				$database->execute();

				$plugid = $database->insertid();
			}
			$query = "SELECT COUNT(*) FROM `#__comprofiler_tabs` WHERE `pluginid` = '{$plugid}'";
			$database->setQuery($query);
			$tabs = $database->loadResult();
			if (!$tabs) {
				$query = "INSERT INTO `#__comprofiler_tabs` SET
								`title` = '$tabtitle',
								`ordering` = 999,
								`enabled` = 1,
								`pluginclass` = '$class',
								`pluginid` = '{$plugid}',
								`fields` = 0,
								`displaytype` = 'tab',
								`position` = 'cb_tabmain',
								`viewaccesslevel` = 2
						";
				$database->setQuery($query);
				$database->execute();
			}

			$sourceFolder      = JPATH_ROOT . "/components/" . APP_EXTENSION . "/installer/cb_plug/";
			$destinationFolder = JPATH_ROOT . '/components/com_comprofiler/plugin/user/' . $folder;

			if (!JFolder::exists($destinationFolder)) JFolder::create($destinationFolder);

			$source_file      = $sourceFolder . DS . $pluginname . ".php";
			$destination_file = $destinationFolder . DS . $pluginname . ".php";
			JFile::copy($source_file, $destination_file);

			$source_file      = $sourceFolder . DS . $pluginname . ".xml";
			$destination_file = $destinationFolder . DS . $pluginname . ".xml";
			JFile::copy($source_file, $destination_file);

			return $plugintitle;
		}


	} // End Class
