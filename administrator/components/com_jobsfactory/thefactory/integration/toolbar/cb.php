<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegrationCBToolbar
	 */
	class JTheFactoryIntegrationCBToolbar
	{
		/**
		 * display
		 *
		 * @param null $task
		 */
		public static function display($task = NULL)
		{
			$appname = JTheFactoryApplication::getInstance()->getApplicationName();
			JToolBarHelper::title(JText::_('FACTORY_COMMUNITY_BUILDER_INTEGRATION') . ' - ' . $appname);
			switch ($task) {
				default:
					JToolBarHelper::save('integrationcb.save', JText::_('FACTORY_SAVE'));
					JToolBarHelper::cancel('integration');
					JToolBarHelper::custom('integrationcb.installPlugins', 'upload', 'upload', JText::_("FACTORY_INSTALL_CB_PLUGINS"), FALSE);
					break;
				case 'installPlugins':
					JToolBarHelper::custom('integrationcb.display', 'back', 'back', JText::_("FACTORY_BACK"), FALSE);
					break;
			}

		}
	}
