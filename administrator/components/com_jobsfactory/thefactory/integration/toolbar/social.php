<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegrationSocialToolbar
	 */
	class JTheFactoryIntegrationSocialToolbar
	{
		/**
		 * display
		 *
		 * @param null $task
		 */
		public static function display($task = NULL)
		{
			$appname = JTheFactoryApplication::getInstance()->getApplicationName();
			JToolBarHelper::title(JText::_('FACTORY_SOCIAL_FACTORY_INTEGRATION') . ' - ' . $appname);
			switch ($task) {
				default:
					JToolBarHelper::save('integrationsocial.save', JText::_('FACTORY_SAVE'));
					JToolBarHelper::cancel('integration');
					break;
				case 'installPlugins':
					JToolBarHelper::custom('integrationsocial.display', 'back', 'back', JText::_("FACTORY_BACK"), FALSE);
					break;
			}

		}
	}
