<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegrationComponent
	 */
	class JTheFactoryIntegrationComponent extends JTheFactoryIntegration
	{
		/**
		 * mode
		 *
		 * @var string
		 */
		var $mode = "component";
		/**
		 * table
		 *
		 * @var null|string
		 */
		var $table = NULL;
		/**
		 * keyfield
		 *
		 * @var string
		 */
		var $keyfield = 'userid';

		/**
		 *
		 */
		public function __construct()
		{
			$MyApp          = JTheFactoryApplication::getInstance();
			$this->table    = $MyApp->getIniValue("table", "extended-profile");
			$this->keyfield = $MyApp->getIniValue("field_userid", "extended-profile");
		}

		/**
		 * detectIntegration
		 *
		 * @return bool
		 */
		public static function detectIntegration()
		{
			return TRUE; //Native Integration is always available
		}

		/**
		 * getUserProfile
		 *
		 * @param int $userid
		 *
		 * @return array
		 */
		public function getUserProfile($userid = 0)
		{
			if (!$userid) {
				$user   = JFactory::getUser();
				$userid = $user->id;
			}
			$db = JFactory::getDbo();
			$db->setQuery("select * from `" . $this->table . "` where `" . $this->keyfield . "`='$userid'");
			$userdata = $db->loadObject();
			if (!$userdata) {
				$fields = $db->getTableColumns("{$this->table}");
				foreach ($fields as $field => $type) $fields[$field] = "";

				return $fields;
			} else
				return get_object_vars($userdata);
		}

		/**
		 * getIntegrationArray
		 *
		 * @return array
		 */
		public static function getIntegrationArray()
		{
			//Native profile has symmetric integration array
			$flist    = self::getIntegrationFields();
			$intarray = array();
			foreach ($flist as $k => $v) {
				$intarray[$v] = $v;
			}

			return $intarray;
		}

		/**
		 * getProfileLink
		 *
		 * @param $userid
		 *
		 * @return null
		 */
		public function getProfileLink($userid)
		{
			//abstract
			return NULL;
		}

		/**
		 * _buildWhere
		 *
		 * @param $filters
		 *
		 * @return array
		 */
		public function _buildWhere($filters)
		{
			$integrationArray = $this->getIntegrationArray();
			/*
				$fields=CustomFieldsFactory::getSearchableFieldsList("user_profile");
				HUH?
				for($i=0;$i<count($fields);$i++)
					$integrationArray[$fields[$i]["row_id"]]=$fields[$i]["row_id"];

			*/
			$where = array();
			$w     = array();
			if (count($filters)) foreach ($filters as $key => $value) if (is_array($value)) {
				foreach ($value as $k => $v) {
					if (isset($integrationArray[$k]) && $integrationArray[$k]) {
						if (strpos($v, '%') !== FALSE) $w[] = 'profile.' . $integrationArray[$k] . " LIKE '$v'";
						else
							$w[] = 'profile.' . $integrationArray[$k] . "='$v'";
					}
					if ($k == 'username') {
						if (strpos($v, '%') !== FALSE) $w[] = "u.username LIKE '$v'";
						else
							$w[] = "u.username='$v'";
					}
				}
				if (count($w)) $where[] = '(' . implode(' OR ', $w) . ')';

			} else {
				if (isset($integrationArray[$key]) && $integrationArray[$key]) {
					if (strpos($value, '%') !== FALSE) $where[] = 'profile.' . $integrationArray[$key] . " LIKE '$value'";
					else
						$where[] = 'profile.' . $integrationArray[$key] . "='$value'";
				}
				if ($key == 'username') {
					if (strpos($v, '%') !== FALSE) $w[] = "u.username LIKE '$v'";
					else
						$w[] = "u.username='$v'";
				}
			}

			return $where;
		}

		/**
		 * getUserList
		 *
		 * @param int  $limitstart
		 * @param int  $limit
		 * @param null $filters
		 * @param null $ordering
		 *
		 * @return mixed
		 */
		public function getUserList($limitstart = 0, $limit = 30, $filters = NULL, $ordering = NULL)
		{
			$db = JFactory::getDbo();


			$colums = array('u.username', 'profile.*');

			$query = "select " . implode(',', $colums);
			$query .= " from #__users u ";
			// List only users with integration profile created
			$query .= " left join `{$this->table}` profile on u.id=profile.`{$this->keyfield}` ";

			$where = $this->_buildWhere($filters);
			if (count($where)) $query .= " WHERE " . implode(' AND ', $where);

			if (count($ordering)) $query .= " ORDER BY " . implode(',', $ordering);

			$db->setQuery($query, $limitstart, $limit);
			$list = $db->loadObjectList();

			return $list;


		}

		/**
		 * getUserListCount
		 *
		 * @param null $filters
		 *
		 * @return mixed
		 */
		public function getUserListCount($filters = NULL)
		{

			$db    = JFactory::getDBO();
			$query = "select count(*) ";

			$query .= " from #__users u ";
			// Count only users with integration profile created
			$query .= " left join `{$this->table}` profile on u.id=profile.`{$this->keyfield}` ";

			$where = $this->_buildWhere($filters);

			if (count($where)) $query .= " WHERE " . implode(' AND ', $where);

			$db->setQuery($query);
			$total = $db->loadResult();

			return $total;
		}

		/**
		 * checkProfile
		 *
		 * @param null $userid
		 *
		 * @return mixed
		 */
		public function checkProfile($userid = NULL)
		{

			if (!$userid) {
				$user   = JFactory::getUser();
				$userid = $user->id;
			}

			$db    = JFactory::getDbo();
			$query = "SELECT COUNT(*) ";
			$query .= "FROM `{$this->table}` `profile` WHERE `profile`.`{$this->keyfield}`='{$userid}'";

			$db->setQuery($query);
			$total = $db->loadResult();

			return $total;

		}

	} // End Class
