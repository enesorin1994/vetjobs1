<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
?>

<h2><?php echo JText::_('FACTORY_INSTALLED_PLUGINS'); ?></h2>

<form action = "index.php" method = "post" name = "adminForm" id = "adminForm">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>" />
	<input type = "hidden" name = "task" value = "integrationcb.save" />
	<table>
		<?php foreach ($this->plugins as $k => $plugin): ?>
			<tr>
				<td>
					<b><?php echo $plugin; ?></b>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</form>
