<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * Websites: http://www.thePHPfactory.com
	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 * -------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	JHtml::_('formbehavior.chosen', 'select');
?>

<h2><?php echo JText::_('FACTORY_CHOOSE_THE_FIELD_MAPPINGS'); ?></h2>

<form action = "index.php" method = "post" name = "adminForm" id = "adminForm">
	<input type = "hidden" name = "option" value = "<?php echo APP_EXTENSION; ?>">
	<input type = "hidden" name = "task" value = "integrationcb.save">
	<table>
		<?php foreach ($this->integrationFields as $k => $field): ?>
			<tr>
				<td><?php echo defined(APP_PREFIX . "_cbfield_" . strtolower($field)) ? constant(APP_PREFIX . "_cbfield_" . strtolower($field)) : $field; ?>

				</td>
				<td>
					<?php
						$selected = isset($this->integrationArray[$field]) ? ($this->integrationArray[$field]) : NULL;
						$disabled = ($this->love_detected) ? "" : "disabled";
						echo JHTML::_("select.genericlist", $this->lovefields, $field, "class='inputbox' $disabled", 'value', 'text', $selected);
					?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</form>
