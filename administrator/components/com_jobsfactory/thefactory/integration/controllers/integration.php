<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 * -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegrationController
	 */
	class JTheFactoryIntegrationController extends JControllerLegacy
	{
		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'Integration';
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'Integration';
		/**
		 * modulename
		 *
		 * @var string
		 */
		var $modulename = 'Integration';

		/**
		 *
		 */
		public function __construct()
		{
			$MyApp  = JTheFactoryApplication::getInstance();
			$config = array('view_path' => $MyApp->app_path_admin . 'integration/views');
			parent::__construct($config);
		}

		/**
		 * getView
		 *
		 * @param string $name
		 * @param string $type
		 * @param string $prefix
		 * @param array  $config
		 *
		 * @return JViewLegacy
		 */
		public function getView($name = '', $type = '', $prefix = '', $config = array())
		{
			$MyApp                   = JTheFactoryApplication::getInstance();
			$config['template_path'] = $MyApp->app_path_admin . 'integration/views/integration/tmpl';

			return parent::getView($name, $type, 'JTheFactoryView', $config);
		}

	}
