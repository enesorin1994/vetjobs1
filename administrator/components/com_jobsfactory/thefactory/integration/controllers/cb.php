<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegrationCBController
	 */
	class JTheFactoryIntegrationCBController extends JControllerLegacy
	{

		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'IntegrationCB';
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'IntegrationCB';
		/**
		 * modulename
		 *
		 * @var string
		 */
		var $modulename = 'Integration';

		/**
		 *
		 */
		public function __construct()
		{
			$MyApp  = JTheFactoryApplication::getInstance();
			$config = array('view_path' => $MyApp->app_path_admin . 'integration/views',);
			JLoader::register('JTheFactoryIntegrationCB', $MyApp->app_path_admin . 'integration/cb.php');
			JLoader::register('JTheFactoryIntegrationCBToolbar', $MyApp->app_path_admin . 'integration/toolbar/cb.php');
			JLoader::register('JTheFactoryIntegrationCBHelper', $MyApp->app_path_admin . 'integration/helper/cb.php');

			parent::__construct($config);
		}

		/**
		 * execute
		 *
		 * @param string $task
		 *
		 * @return mixed
		 */
		public function execute($task)
		{
			JTheFactoryIntegrationCBToolbar::display($task);

			return parent::execute($task);
		}

		/**
		 * getView
		 *
		 * @param string $name
		 * @param string $type
		 * @param string $prefix
		 * @param array  $config
		 *
		 * @return JViewLegacy
		 */
		public function getView($name = '', $type = 'html', $prefix = '', $config = array())
		{
			$MyApp                   = JTheFactoryApplication::getInstance();
			$config['template_path'] = $MyApp->app_path_admin . 'integration/views/integrationcb/tmpl';

			return parent::getView($name, $type, 'JTheFactoryView', $config);
		}

		/**
		 * display
		 *
		 */
        public function display($cachable = false, $urlparams = array())
		{
			$cbinclude = JPATH_ADMINISTRATOR . "/components/com_comprofiler/plugin.foundation.php";
			if (!file_exists($cbinclude)) {
				JFactory::getApplication()->enqueueMessage(JText::_('FACTORY_CB_NOT_INSTALLED'), 'warning');

				return;

			}
			require_once($cbinclude);

			cbimport('language.all');

			$integrationFields = JTheFactoryIntegrationCB::getIntegrationFields();
			$integrationArray  = JTheFactoryIntegrationCB::getIntegrationArray();

			$database = JFactory::getDBO();
			$query    = "SELECT `name` AS value,`title` AS text FROM `#__comprofiler_fields` ORDER BY `name`";
			$database->setQuery($query);
			$cbfields = $database->loadObjectList();
			foreach ($cbfields as &$f) {
				$f->text = defined($f->text) ? constant($f->text) : $f->text;
			}
			$cbfields = array_merge(array(JHTML::_("select.option", '', '-' . JText::_("FACTORY_NONE") . '-')), $cbfields);
			JHtml::_('formbehavior.chosen', 'select');

			$view                    = $this->getView();
			$view->integrationFields = $integrationFields;
			$view->integrationArray  = $integrationArray;
			$view->cbfields          = $cbfields;
			$view->cb_detected       = JTheFactoryIntegrationCB::detectIntegration();

			$view->display();
		}

		/**
		 * save
		 *
		 */
		public function save()
		{
			$MyApp     = JTheFactoryApplication::getInstance();
			$tablename = $MyApp->getIniValue("field_map_table", "profile-integration");

			$fields = JTheFactoryIntegrationCB::getIntegrationFields();
			$db     = JFactory::getDbo();

			foreach ($fields as $field) {
				$cb = JFactory::getApplication()->input->get($field, NULL);
				$db->setQuery("SELECT * FROM `$tablename` WHERE `field`='$field'");
				$res = $db->loadObject();
				if ($res) $db->setQuery("UPDATE `$tablename` SET `assoc_field`='$cb' WHERE `field`='$field'");
				else
					$db->setQuery("INSERT INTO `$tablename` SET `assoc_field`='$cb' ,`field`='$field'");
				$db->execute();
			}

			JTheFactoryEventsHelper::triggerEvent('onIntegrationFieldsSaved', array('comprofiler'));

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=integrationCB.display", JText::_("FACTORY_SETTINGS_SAVED"));
		}


		/**
		 * installPlugins
		 *
		 */
		public function installPlugins()
		{
			jimport('joomla . filesystem . folder');
			jimport('joomla . filesystem . file');
			$plugin_dir        = JPATH_ROOT . "/components/" . APP_EXTENSION . "/installer/cb_plug/";
			$files             = JFolder::files($plugin_dir, '\.xml$');
			$installed_plugins = array();

			foreach ($files as $file) {

				if (!$xml = simplexml_load_file($plugin_dir . $file)) continue;

				if (!is_object($xml) || ($xml->getName() != 'cbinstall')) continue;

				$title    = $xml->name;
				$tab      = $xml->tabs->tab;
				$tabtitle = $tab->attributes()->name;

				$class       = $tab->attributes()->class;
				$pluginfiles = $xml->files->filename;
				$plugin      = $pluginfiles->attributes()->plugin;
				$folder      = 'plug_' . JFile::stripExt($plugin);

				$installed_plugins[] = JTheFactoryIntegrationCBHelper::InstallCBPlugin($title, $tabtitle, $plugin, $folder, $class);
				unset($xml);
			}

			$view          = $this->getView();
			$view->plugins = $installed_plugins;

			$view->display('plugininstall');

		}

	}


