<?php
	/**------------------------------------------------------------------------
	 * thefactory - The Factory Class Library - v 3.0.0
	 * ------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011-2014 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 01/04/2012
	 * @package   : thefactory
	 * @subpackage: integration
	 *            -------------------------------------------------------------------------*/

	defined('_JEXEC') or die('Restricted access');

	/**
	 * Class JTheFactoryIntegrationSocialController
	 */
	class JTheFactoryIntegrationSocialController extends JControllerLegacy
	{
		/**
		 * _name
		 *
		 * @var string
		 */
		var $_name = 'IntegrationSocial';
		/**
		 * name
		 *
		 * @var string
		 */
		var $name = 'IntegrationSocial';
		/**
		 * modulename
		 *
		 * @var string
		 */
		var $modulename = 'Integration';

		/**
		 *
		 */
		public function __construct()
		{
			$MyApp  = JTheFactoryApplication::getInstance();
			$config = array('view_path' => $MyApp->app_path_admin . 'integration/views',);
			JLoader::register('JTheFactoryIntegrationSocial', $MyApp->app_path_admin . 'integration/social.php');
			JLoader::register('JTheFactoryIntegrationSocialToolbar', $MyApp->app_path_admin . 'integration/toolbar/social.php');
			JLoader::register('JTheFactoryIntegrationSocialHelper', $MyApp->app_path_admin . 'integration/helper/social.php');

			parent::__construct($config);
		}

		/**
		 * @param string $task
		 *
		 * @return mixed|void
		 */
		public function execute($task)
		{
			JTheFactoryIntegrationSocialToolbar::display($task);

			return parent::execute($task);
		}

		/**
		 * @param string $name
		 * @param string $type
		 * @param string $prefix
		 * @param array  $config
		 *
		 * @return object
		 */
		public function getView($name = '', $type = 'html', $prefix = '', $config = array())
		{
			$MyApp                   = JTheFactoryApplication::getInstance();
			$config['template_path'] = $MyApp->app_path_admin . 'integration/views/integrationsocial/tmpl';

			return parent::getView($name, $type, 'JTheFactoryView', $config);
		}

		/**
		 * display
		 *
		 */
		public function display()
		{
			$integrationFields = JTheFactoryIntegrationSocial::getIntegrationFields();
			$integrationArray  = JTheFactoryIntegrationSocial::getIntegrationArray();
			JTheFactoryEventsHelper::triggerEvent('onBeforeDisplayFieldsMapping', array('socialfactory'));
			$db = JFactory::getDBO();

			// Get Social Factory available fields

			$query = $db->getQuery(TRUE);
			$query->select('id AS value, name AS text')
				->from('#__socialfactory_fields')
				->where('published = 1');
			$db->setQuery($query);
			$sfDbFields = $db->loadObjectList();

			foreach ($sfDbFields as $k => $field) {
				$sfDbFields[$k]->value = 'field_' . $field->value;
			}

			$socialFields = array_merge(array(JHTML::_("select.option", '', '-' . JText::_("FACTORY_NONE") . '-')), $sfDbFields);

			$view = $this->getView();
			$view->assignRef('integrationFields', $integrationFields);
			$view->assignRef('integrationArray', $integrationArray);
			$view->assignRef('socialfields', $socialFields);
			$view->assign('social_detected', JTheFactoryIntegrationSocial::detectIntegration());

			$view->display();
		}


		/**
		 * save
		 *
		 */
		public function save()
		{
			$MyApp     = JTheFactoryApplication::getInstance();
			$tablename = $MyApp->getIniValue("field_map_table", "profile-integration");

			$fields = JTheFactoryIntegrationSocial::getIntegrationFields();
			$db     = JFactory::getDBO();

			foreach ($fields as $field) {
				$assoc_fd = JFactory::getApplication()->input->get($field, NULL);
				$db->setQuery("SELECT * FROM `$tablename` WHERE `field`='$field'");
				$res = $db->loadObject();
				if ($res) $db->setQuery("UPDATE `$tablename` SET `assoc_field` = '$assoc_fd' WHERE `field` = '$field'");
				else
					$db->setQuery("INSERT INTO `$tablename` SET `assoc_field` = '$assoc_fd', `field` = '$field'");
				$db->execute();
			}

			JTheFactoryEventsHelper::triggerEvent('onIntegrationFieldsSaved', array('socialfactory'));

			$this->setRedirect("index.php?option=" . APP_EXTENSION . "&task=integrationSocial.display", JText::_("FACTORY_SETTINGS_SAVED"));
		}


	}


