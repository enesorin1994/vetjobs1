<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class JJobsfactoryAdminController extends JControllerLegacy
{
    function execute($task)
    {
        if (file_exists(JPATH_COMPONENT_ADMINISTRATOR . DS . 'toolbar.admin.php'))
            require_once JPATH_COMPONENT_ADMINISTRATOR . DS . 'toolbar.admin.php';

        if (file_exists(JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'jobsfactory.php'))
            require_once JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers'. DS. 'jobsfactory.php';


        $lang = JFactory::getLanguage();
        $lang->load('com_jobsfactory.sys');

        parent::execute($task);
    }

    function PaymentManager()
    {
        $view = $this->getView('paymentmanager', 'html');
        $view->display();
    }

    function Jobs()
    {

        $app    = JFactory::getApplication();
        $db     = JFactory::getDbo();
        $query  = $db->getQuery(true);
        $cfg    = JTheFactoryHelper::getConfig();

        if ($cfg->admin_approval) {
            JToolBarHelper::custom('approve_toggle', 'apply', 'apply', JText::_('COM_JOBS_TOGGLE_APPROVAL_STATUS'), true);
        }
        $where = array();

        $context = 'com_jobsfactory.jobs';
        $reset = JFactory::getApplication()->input->getInt('reset');

        if ($reset) {
            $app->setUserState($context, null);
            $filter_authorid = '';
            $filter_approved = '';
            $search = '';
        } else {
            $filter_authorid = $app->getUserStateFromRequest($context . 'filter_authorid', 'filter_authorid', '', 'string');
            $filter_approved = $app->getUserStateFromRequest($context . 'filter_approved', 'filter_approved', '', 'string');
            $search = $app->getUserStateFromRequest($context . 'search', 'search', '', 'string');
        }

        $filter_order = $app->getUserStateFromRequest($context . 'filter_order', 'filter_order', '', 'cmd');
        $filter_order_Dir = $app->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', '', 'word');
        $search = JString::strtolower($search);

        $items_per_page = isset($cfg->nr_items_per_page) ? $cfg->nr_items_per_page : $app->getCfg('list_limit');
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $items_per_page, 'int');
        $limitstart = $app->getUserStateFromRequest($context . 'limitstart', 'limitstart', 0, 'int');

        // In case limit has been changed, adjust limitstart accordingly
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        if (!$filter_order) {
            $filter_order = 'a.title';
        }
        $order = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir . '';

        if ($search)
            $where[] = " a.title LIKE '%$search%' ";
        if ($filter_authorid) {
            $where[] = " u.username like '%$filter_authorid%' ";
        }
        if ($cfg->admin_approval && $filter_approved !== '') {
            $where[] = " a.approved= '$filter_approved'";
        }

        // Build the where clause of the content record query
        $where = (count($where) ? ' WHERE ' . implode(' AND ', $where) : '');

        // Get the total number of records
        $query = 'SELECT COUNT(*)' .
            ' FROM #__jobsfactory_jobs AS a' .
            ' LEFT JOIN #__jobsfactory_categories AS cc ON cc.id = a.cat ' .
            ' LEFT JOIN #__users AS u ON u.id = a.userid' .
            $where;
        $db->setQuery($query);
        $total = $db->loadResult();

        // Create the pagination object
        jimport('joomla.html.pagination');
        $pagination = new JPagination($total, $limitstart, $limit);

        // Get the jobs
        $query = 'SELECT a.*, cc.catname AS name, ' .
            ' u.name AS editor, ' .
            ' count(DISTINCT c.userid) as nr_applicants ' .
            ' FROM #__jobsfactory_jobs AS a' .
            ' LEFT JOIN #__jobsfactory_categories AS cc ON cc.id = a.cat ' .
            ' LEFT JOIN #__users AS u ON u.id = a.userid' .
            ' LEFT JOIN `#__jobsfactory_candidates` AS c ON `c`.`job_id`=`a`.`id` and c.cancel=0 ' .
            $where .
            ' GROUP BY a.id ' .
            $order;

        $db->setQuery($query, $pagination->limitstart, $pagination->limit);
        $rows = $db->loadObjectList();

        $view = $this->getView('job', 'html');

        $view->filter_authorid = $filter_authorid;
        $view->order_Dir = $filter_order_Dir;
        $view->order = $filter_order;
        $view->filter_approved = $filter_approved;
        $view->search = $search;
        $view->cfg = $cfg;
        $view->jobs = $rows;
        $view->pagination = $pagination;

        $view->display('list');
    }

    function Approve_Toggle()
    {
        $db     = JFactory::getDbo();
        $query  = $db->getQuery(true);
        $cid    = $this->input->get('cid', array(), 'array');

        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        $cids = implode(',', $cid);

        $query = 'UPDATE #__jobsfactory_jobs' .
            ' SET approved = 1-coalesce(approved,0)' .
            ' WHERE id IN ( ' . $cids . ' ) ';
        $db->setQuery($query);

        try
        {
        	$db->execute();
        }
        catch (RuntimeException $e)
        {
        	JFactory::getApplication()->enqueueMessage($e->getMessage(),'error');
        	return false;
        }
        $total = $db->getAffectedRows();
        $msg = JText::sprintf('%s Item(s) Affected', $total);

        $job = JTable::getInstance('jobs', 'Table');
        foreach ($cid as $id)
        {
            $job->load($id);
            $user = JFactory::getUser($job->userid);
            if ($job->approved) {
                $job->SendMails(array($user), 'admin_job_approved');
            } else {
                $job->SendMails(array($user), 'admin_job_unapproved');
            }
        }

        $this->setRedirect('index.php?option=com_jobsfactory&task=jobs', $msg);
    }

    function Publish()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $cid    = $this->input->get('cid', array(), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        $cids = implode(',', $cid);

        $query = 'UPDATE #__jobsfactory_jobs' .
            ' SET published = 1' .
            ' WHERE id IN ( ' . $cids . ' ) ';
        $db->setQuery($query);

        try
        {
        	$db->execute();
        }
        catch (RuntimeException $e)
        {
        	JFactory::getApplication()->enqueueMessage($e->getMessage(),'error');
        	return false;
        }
        $total = $db->getAffectedRows();
        $msg = JText::sprintf('%s Item(s) successfully Published', $total);
        $this->setRedirect('index.php?option=com_jobsfactory&task=jobs', $msg);
    }

    function Unpublish()
    {
        $db     = JFactory::getDbo();
        $query  = $db->getQuery(true);

        $cid    = $this->input->get('cid', array(), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        $cids = implode(',', $cid);

        $query = 'UPDATE #__jobsfactory_jobs' .
            ' SET published = 0' .
            ' WHERE id IN ( ' . $cids . ' ) ';
        $db->setQuery($query);

        try
        {
        	$db->execute();
        }
        catch (RuntimeException $e)
        {
        	JFactory::getApplication()->enqueueMessage($e->getMessage(),'error');
        	return false;
        }
        $total = $db->getAffectedRows();
        $msg = JText::sprintf('%s Item(s) successfully Unpublished', $total);
        $this->setRedirect('index.php?option=com_jobsfactory&task=jobs', $msg);
    }

    function Block()
    {
        $db     = JFactory::getDbo();
        $query  = $db->getQuery(true);

        $cid    = $this->input->get('cid', array(), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }
        $cids = implode(',', $cid);

        $query = 'UPDATE #__jobsfactory_jobs' .
            ' SET close_by_admin = 1' .
            ' WHERE id IN ( ' . $cids . ' ) ';
        $db->setQuery($query);

        try
        {
        	$db->execute();
        }
        catch (RuntimeException $e)
        {
        	JFactory::getApplication()->enqueueMessage($e->getMessage(),'error');
        	return false;
        }
        $total = $db->getAffectedRows();

        $job = JTable::getInstance('jobs', 'Table');
        foreach ($cid as $id)
        {
            $job->load($id);
            $user = JFactory::getUser($job->userid);
            $job->SendMails(array($user), 'job_admin_close_job');
        }

        $msg = JText::sprintf('%s Item(s) successfully Banned', $total);
        $this->setRedirect('index.php?option=com_jobsfactory&task=jobs', $msg);
    }

    function Unblock()
    {
        $db     = JFactory::getDbo();
        $query  = $db->getQuery(true);

        $cid    = $this->input->get('cid', array(), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }
        $cids = implode(',', $cid);

        $query = 'UPDATE #__jobsfactory_jobs' .
            ' SET close_by_admin = 0' .
            ' WHERE id IN ( ' . $cids . ' ) ';
        $db->setQuery($query);

        try
        {
        	$db->execute();
        }
        catch (RuntimeException $e)
        {
        	JFactory::getApplication()->enqueueMessage($e->getMessage(),'error');
        	return false;
        }

        $total = $db->getAffectedRows();
        $msg = JText::sprintf('%s Item(s) successfully opened', $total);
        $this->setRedirect('index.php?option=com_jobsfactory&task=jobs', $msg);
    }

    function CloseOffer()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $cid    = $this->input->get('cid', array(), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }
        $cids = implode(',', $cid);

        $query = 'UPDATE #__jobsfactory_jobs' .
            ' SET close_offer = 1' .
            ' WHERE id IN ( ' . $cids . ' ) ';
        $db->setQuery($query);

        try
        {
        	$db->execute();
        }
        catch (RuntimeException $e)
        {
        	JFactory::getApplication()->enqueueMessage($e->getMessage(),'error');
        	return false;
        }

        $total = $db->getAffectedRows();
        $msg = JText::sprintf('%s Item(s) successfully Closed', $total);
        $this->setRedirect('index.php?option=com_jobsfactory&task=jobs', $msg);
    }

    function OpenOffer()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $cid    = $this->input->get('cid', array(), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }
        $cids = implode(',', $cid);

        $query = 'UPDATE #__jobsfactory_jobs' .
            ' SET close_offer = 0' .
            ' WHERE id IN ( ' . $cids . ' ) ';
        $db->setQuery($query);

        try
        {
        	$db->execute();
        }
        catch (RuntimeException $e)
        {
        	JFactory::getApplication()->enqueueMessage($e->getMessage(),'error');
        	return false;
        }
        $total = $db->getAffectedRows();
        $msg = JText::sprintf('%s Item(s) successfully opened', $total);
        $this->setRedirect('index.php?option=com_jobsfactory&task=jobs', $msg);
    }

    function Remove()
    {
        $cid    = $this->input->get('cid', array(0), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        $job = JTable::getInstance('jobs', 'Table');
        foreach ($cid as $id)
        {
            $job->load($id);
            $job->delete($id);
        }
        $this->setRedirect("index.php?option=com_jobsfactory&task=jobs", JText::_("COM_JOBS_REMOVED"));
    }

    function RemoveUser()
    {
        $cid    = $this->input->get('cid', array(0), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        $job = JTable::getInstance('users', 'Table');
        foreach ($cid as $id)
        {
            $job->load($id);
            $job->delete($id);
        }
        $this->setRedirect("index.php?option=com_jobsfactory&task=users", JText::_("COM_JOBS_REMOVED"));
    }

    function Edit()
    {
        $cfg    = JTheFactoryHelper::getConfig();
        $cid    = $this->input->get('cid', array(0), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        $view = $this->getView('job', 'html');
        JArrayHelper::toInteger($cid, array(0));

        $row = JTable::getInstance('jobs', 'Table');
        $row->load($cid[0]);

        $fields = CustomFieldsFactory::getFieldsList("jobs");

        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile($row->userid);
        $row->userdetails = clone $userprofile;
        $row->messages = $row->getMessages();
        // requested experience
        $assignedStr = $row->getRequestedExperiences($row);
        if (!empty($assignedStr)) {
           $assigned = explode(',', $assignedStr);
        } else {
           $assigned[] = '1';
        }

        $lists['experience_request'] = JHtml::_('experiencelevel.selectlist','experience_request[]','style="width:200px" multiple="multiple" size=7', $assigned);
        $lists['studies_request']  = JHtml::_('studieslevel.selectlist', 'studies_request', '', $row->studies_request);

        $scripttoggle = 'onchange="if(this.value==\'0\') { document.getElementById(\'countrylist\').style.display=\'none\';
                    document.getElementById(\'statelist\').style.display=\'none\'; } else { document.getElementById(\'countrylist\').style.display=\'\';
                    document.getElementById(\'statelist\').style.display=\'\'; } "
                   ';

        if (($cfg->enable_location_management) || ($row->id && $row->job_country) ) {
           $lists['use_location'] = JHtml::_('jobuselocation.selectlist', 'use_location', $scripttoggle, 1);
        }
        else
            $lists['use_location'] = JHtml::_('jobuselocation.selectlist', 'use_location', $scripttoggle, 0);

        if ($cfg->enable_location_management) {
            $locationModel = JobsHelperTools::getLocationModel();

            $lists['country'] = JHtml::_('country.selectlist', 'job_country', "id='job_country' class='required' onchange='jobsRefreshLocations(this);'", $row->job_country);

            $job_location_state = $locationModel->getLocationName($row->job_location_state);
            $lists['states'] = JHtml::_('factorylocation.select', 'job_location_state', "onchange='jobsRefreshCities(this);'", $row->job_location_state, false); //$city->country

            if ($cid[0] && $row->job_cityid) {
                $lists['cities'] = JHtml::_('factorycity.select', 'job_cityid', "", $row->job_cityid, false);
            } else {
                if ($cid[0] && $row->job_cityname)
                    $lists['cities'] = $row->job_cityname;
                else
                    $lists['cities'] = JHtml::_('factorycity.select', 'job_cityid', "", '', false);
            }
        } else {
            $lists['cities'] = $row->job_cityname;
        }

        $tag_obj = JTable::getInstance('tags', 'Table');
        $row->tags = $tag_obj->getTagsAsString($row->id);

        $feat[] = JHtml::_('select.option', 'none', JText::_("COM_JOBS_NONE"));
        $feat[] = JHtml::_('select.option', 'featured', JText::_("COM_JOBS_FEATURED"));

        $lists['featured'] = JHtml::_('select.genericlist', $feat, 'featured', 'class="inputbox" id="featured" style="width:120px;"', 'value', 'text', $row->featured);

        $lists['category'] = JHtml::_('factorycategory.select', 'cat', '', $row->cat);

        if ($cfg->admin_approval) {
            JToolBarHelper::custom('approve_toggle', 'apply', 'apply',
                ($row->approved) ? JText::_('COM_JOBS_UNAPPROVE') : JText::_('COM_JOBS_APPROVE'), false);
        }

        $view->row = $row;
        $view->cfg = $cfg;
        $view->fields = $fields;

		if (isset($lists["featured"]))
		{
			$view->featured = $lists["featured"];
		}

		if (isset($lists["category"]))
		{
			$view->category = $lists["category"];
		}

		if (isset($lists["currency"]))
		{
			$view->currency = $lists["currency"];
		}

		if (isset($lists['experience_request']))
		{
			$view->experience_request = $lists['experience_request'];
		}

		if (isset($lists['studies_request']))
		{
			$view->studies_request = $lists['studies_request'];
		}

		if (isset($lists['country']))
		{
			$view->countries = $lists['country'];
		}

		if (isset($lists['states']))
		{
			$view->states = $lists['states'];
		}

		if (isset($lists['cities']))
		{
			$view->cities = $lists['cities'];
		}

		if (isset($lists['use_location']))
		{
			$view->use_location = $lists['use_location'];
		}

        if ($cfg->enable_location_management)
		{
            $view->job_location_state = $job_location_state;
        }

        $view->display('edit');
    }

    function RefreshCity()
    {
        $parentid = JFactory::getApplication()->input->getInt('locationid');
        $db = JFactory::getDbo();
        //if ($enabled_only)
        $filter = "`status`=1 and ";
        $db->setQuery("SELECT c.* FROM `#__jobsfactory_cities` c WHERE {$filter} `location`=" . $parentid . " order by `ordering`");

        $cities = $db->loadObjectList();
        $options = array();

        foreach ($cities as $city)
        {
            //TODO: nr_children nu e intors de model
            $city->depth = 1;
            $spacer = str_pad('', ($city->depth - 1) * 3, '-');
            $options[] = JHtml::_('select.option', "$city->id", $spacer . stripslashes($city->cityname));
        }

        if (!empty($options)) {
            $html_tree = JHtml::_('select.genericlist', $options, 'job_cityid', "", 'value', 'text', $options[0]->value);
        } else {
            $html_tree = '<input class="inputbox required" type="text" size="60" name="job_cityname" value="" alt="job_cityname">';
        }

        echo $html_tree;
    }

    function RefreshLocation()
    {
        $parentName = $this->input->getCmd('countryid');
        $db = JFactory::getDbo();
        //if ($enabled_only)
        $filter = "`status`=1 and ";
        $db->setQuery("SELECT l.* FROM `#__jobsfactory_locations` l WHERE {$filter} `country`='" . $parentName . "' order by `ordering`");

        $locations = $db->loadObjectList();
        $options = array();

        foreach ($locations as $location)
        {
            $location->depth = 1;
            $spacer = str_pad('', ($location->depth - 1) * 3, '-');
            $options[] = JHtml::_('select.option', $location->id, $spacer . stripslashes($location->locname));
        }

        if (!empty($options)) {
            //$html_tree = JHtml::_('select.genericlist', $options, 'job_locationid', "", 'value', 'text', $options[0]->value);
            $html_tree = JHtml::_('select.genericlist', $options, 'job_location_state', "onchange='jobsRefreshCities(this);'", 'value', 'text', $options[0]->value);
        } else {
            $html_tree = '';
        }

        echo $html_tree;
    }

    function SaveClose()
    {
        $this->Save();
        $this->setRedirect("index.php?option=com_jobsfactory&task=jobs", JText::_("COM_JOBS_JOB_SAVED"));
    }

    function Save()
    {
        $id = JFactory::getApplication()->input->getInt('id');

        $job = JTable::getInstance('jobs', 'Table');
        $model = JModelLegacy::getInstance('Job', 'JobsAdminModel');

        if (!$job->load($id)) {
            JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_ERROR_LOADING_JOB_ID"). $id,'warning');
            return false;
        }

        $model->bindJob($job);
        $err = $model->saveJob($job);

        if (count($err)) {
            $err_message = implode('<br/>', $err);
            $this->setRedirect("index.php?option=com_jobsfactory&task=edit&cid[]={$id}", $err_message);
            return false;
        }

        $this->setRedirect("index.php?option=com_jobsfactory&task=edit&cid[]={$id}", JText::_("COM_JOBS_JOB_SAVED"));
    }

    function SettingsManager()
    {
        $db = JFactory::getDbo();
        $db->setQuery("SELECT * FROM #__jobsfactory_paysystems WHERE enabled=1");
        $gateways = $db->loadObjectList();

        $db->setQuery("SELECT * FROM #__jobsfactory_pricing WHERE enabled=1");
        $items = $db->loadObjectList();

        $db->setQuery("SELECT * FROM #__jobsfactory_log WHERE event='cron' ORDER BY logtime DESC limit 1");
        $log = $db->loadObject();

        $cfg = JTheFactoryHelper::getConfig();

        $view = $this->getView('settingspanel', 'html');
        $view->gateways = $gateways;
        $view->items = $items;
        if ($log)
            $view->latest_cron_time = $log->logtime;
        else
            $view->latest_cron_time = JText::_('COM_JOBS_NEVER');

        $view->cfg = $cfg;
        $view->display();
    }

    function Cronjob_Info()
    {
        $cfg = JTheFactoryHelper::getConfig();
        $db = JFactory::getDbo();
        $db->setQuery("SELECT * FROM #__jobsfactory_log WHERE event='cron' ORDER BY logtime DESC limit 1");
        $log = $db->loadObject();

        $view = $this->getView('settingspanel', 'html');
        $view->cfg = $cfg;
        $view->cronlog = $log;
        $view->display('cronsettings');
    }

    function BlockUser()
    {
        $cids    = $this->input->get('cid', array(), 'array');
        if (empty($cids)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        foreach ($cids as $id) {
            $user = JTable::getInstance('User');
            $user->load($id);
            $user->block = 1;
            $user->store();
        }
        $this->setRedirect('index.php?option=com_jobsfactory&task=users', JText::sprintf('%s User(s) successfully Blocked ', count($cids)));
    }

    function UnblockUser()
    {
        $cids    = $this->input->get('cid', array(), 'array');
        if (empty($cids)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        foreach ($cids as $id) {
            $user = JTable::getInstance('User');
            $user->load($id);
            $user->block = 0;
            $user->store();
        }
        $this->setRedirect('index.php?option=com_jobsfactory&task=users', JText::sprintf('%s User(s) successfully Blocked ', count($cids)));
    }

    function _setProfileField($field, $value)
    {
        $app = JFactory::getApplication();
        $database = JFactory::getDbo();
        $query = $database->getQuery(true);

        $profile = JobsHelperTools::getUserProfileObject();
        $cid    = $app->input->get('cid', array(), 'array');

        $tableName = $profile->getIntegrationTable();
        $tableKey = $profile->getIntegrationKey();
        $integration = $profile->getIntegrationArray();

        if (count($cid) < 1) {
            $app->enqueueMessage(JText::_("COM_JOBS_NO_USER_SELECTED"), 'warning');
            return false;
        }
        if (!$integration[$field]) {
            $app->enqueueMessage(JText::_("COM_JOBS_PROFILE_INTEGRATION_NOT_SET_FOR") . ": $field", 'warning');
            return false;
        }
        $cids = implode(',', $cid);

        $query = "UPDATE `$tableName` "
            . "\n SET " . $integration[$field] . "='$value' "
            . "\n WHERE `$tableKey` IN ( $cids )";
        $database->setQuery($query);

        return $database->execute();
    }

    function setVerify()
    {
        if ($this->_setProfileField('verified', 1)) $msg = JText::_("COM_JOBS_VERIFIED_STATUS_CHANGED");
        else $msg = '';
        $this->setRedirect(JURI::root() . 'administrator/index.php?option=com_jobsfactory&task=users', $msg);
    }

    function unsetVerify()
    {
        if ($this->_setProfileField('verified', 0)) $msg = JText::_("COM_JOBS_VERIFIED_STATUS_CHANGED");
        else $msg = '';
        $this->setRedirect(JURI::root() . 'administrator/index.php?option=com_jobsfactory&task=users', $msg);

    }

    function setPowerseller()
    {
        if ($this->_setProfileField('powerseller', 1)) $msg = JText::_("COM_JOBS_POWERSELLER_STATUS_CHANGED");
        else $msg = '';
        $this->setRedirect(JURI::root() . 'administrator/index.php?option=com_jobsfactory&task=users', $msg);
    }

    function unsetPowerseller()
    {
        if ($this->_setProfileField('powerseller', 0)) $msg = JText::_("COM_JOBS_POWERSELLER_STATUS_CHANGED");
        else $msg = '';
        $this->setRedirect(JURI::root() . 'administrator/index.php?option=com_jobsfactory&task=users', $msg);
    }

    function dashboard()
    {
        $app = JFactory::getApplication();
        $cfg = JTheFactoryHelper::getConfig();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $jnow	= JFactory::getDate();

        $query = "SELECT COUNT(*) FROM #__jobsfactory_jobs WHERE close_offer = 0 AND close_by_admin <> 1 AND '".$jnow->toSql()."' <= end_date   ";
        $db->setQuery($query);
        $nr_active = $db->loadResult();

        $query = "SELECT COUNT(*) FROM #__jobsfactory_jobs ";
        $db->setQuery($query);
        $nr_total = $db->loadResult();

        $query = "SELECT COUNT(*) FROM #__jobsfactory_jobs WHERE close_offer = 0 AND published = 1";
        $db->setQuery($query);
        $nr_published = $db->loadResult();

        $query = "SELECT COUNT(*) FROM #__jobsfactory_jobs WHERE close_offer = 0 AND published = 0 AND closed_date <= end_date ";
        $db->setQuery($query);
        $nr_unpublished = $db->loadResult();

        $query = "SELECT COUNT(*) FROM #__jobsfactory_jobs WHERE close_by_admin = 1";
        $db->setQuery($query);
        $nr_blocked = $db->loadResult();

        $query = "SELECT COUNT(*) FROM #__jobsfactory_jobs WHERE close_offer = 1 OR '".$jnow->toSql()."' >= end_date ";
        $db->setQuery($query);
        $nr_expired = $db->loadResult();

        $query = "SELECT COUNT(*) FROM #__jobsfactory_messages";
        $db->setQuery($query);
        $nr_messages = $db->loadResult();

        $query = "SELECT COUNT(distinct userid) FROM #__jobsfactory_jobs";
        $db->setQuery($query);
        $nr_a_companies = $db->loadResult();

        $query = "SELECT COUNT(distinct userid) FROM #__jobsfactory_candidates";
        $db->setQuery($query);
        $nr_a_candidates = $db->loadResult();

        $query = "SELECT COUNT(*) FROM #__users";
        $db->setQuery($query);
        $nr_r_users = $db->loadResult();

        $filter_order_Dir = $app->getUserStateFromRequest('dashboard.filter_order_Dir', 'filter_order_Dir', 'asc', 'word');
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart = $app->getUserStateFromRequest('global.limitstart', 'limitstart', 0, 'int');
        // In case limit has been changed, adjust limitstart accordingly
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        // Get the total number of records
        $query = "SELECT COUNT(*) " .
            "FROM #__users";
        $db->setQuery($query);
        $total = $db->loadResult();

        // Create the pagination object
        jimport('joomla.html.pagination');
        $pagination = new JPagination($total, $limitstart, $limit);

        // Get the jobs
        $query = 'SELECT a.*, cc.catname AS name, ' .
            ' u.name AS editor, ' .
            ' count(DISTINCT c.userid) as nr_applicants ' .
            ' FROM #__jobsfactory_jobs AS a' .
            ' LEFT JOIN #__jobsfactory_categories AS cc ON cc.id = a.cat ' .
            ' LEFT JOIN #__users AS u ON u.id = a.userid' .
            ' LEFT JOIN `#__jobsfactory_candidates` AS c ON `c`.`job_id`=`a`.`id` and c.cancel=0 ' .
            ' GROUP BY a.id ' .
            ' ORDER BY a.id DESC ';

        $db->setQuery($query, $pagination->limitstart, 5);
        $jobs = $db->loadObjectList();

        // Get the payments
        $q=$db->getQuery(true);
        $q->select('p.*,u.username')->from('#__'.APP_PREFIX.'_payment_log'.' p')->leftJoin('#__users u ON u.id=p.userid')->order('p.id DESC');
        $db->setQuery($q, 0, 5);
        $payments = $db->loadObjectList();

        // table ordering
        $lists['nr_r_users'] = $nr_r_users;
        $lists['nr_a_users'] = $nr_a_companies + $nr_a_candidates;
        $lists['nr_a_candidates'] = $nr_a_candidates;
        $lists['nr_messages'] = $nr_messages;
        $lists['nr_total'] = $nr_total;
        $lists['nr_active'] = $nr_active;
        $lists['nr_published'] = $nr_published;
        $lists['nr_unpublished'] = $nr_unpublished;
        $lists['nr_blocked'] = $nr_blocked;
        $lists['nr_expired'] = $nr_expired;
        $lists['order_Dir'] = $filter_order_Dir;

        $view = $this->getView('dashboard', 'html');
        //$view->rows = $rows;
        $view->jobs = $jobs;
        $view->payments = $payments;
        $view->pagination = $pagination;
        $view->lists = $lists;
        $view->cfg = $cfg;

        $view->display();
    }


    function Users()
    {
        $cfg    = JTheFactoryHelper::getConfig();
        $app    = JFactory::getApplication();

        $db     = JFactory::getDbo();
        $query  = $db->getQuery(true);
        $where  = array();

        $context = 'com_jobsfactory.users';
        $filter_order = $app->getUserStateFromRequest($context . 'filter_order', 'filter_order', '', 'cmd');
        $filter_order_Dir = $app->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', '', 'word');
        $search = $app->getUserStateFromRequest($context . 'search', 'search', '', 'string');
        $search = JString::strtolower($search);
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart = $app->getUserStateFromRequest($context . 'limitstart', 'limitstart', 0, 'int');
        // In case limit has been changed, adjust limitstart accordingly
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        if (!$filter_order) {
            $filter_order = 'u.name';
        }
        $order = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir . '';

        if ($search)
            $where[] = " username LIKE '%$search%' ";

        // Build the where clause of the content record query
        $where = (count($where) ? ' WHERE ' . implode(' AND ', $where) : '');

        // Get the total number of records
        $query = "SELECT COUNT(*) " .
            "FROM #__users" . $where;
        $db->setQuery($query);
        $total = $db->loadResult();

        // Create the pagination object
        jimport('joomla.html.pagination');
        $pagination = new JPagination($total, $limitstart, $limit);

        $profile = JobsHelperTools::getUserProfileObject();
        $tableName = $profile->getIntegrationTable();
        $tableKey = $profile->getIntegrationKey();

        $integration = $profile->getIntegrationArray();
        $sel_fields = array("`b`.$tableKey AS id");

        foreach ($integration as $k => $v)
		{
			if($k == 'name')
			{
				$name_field_name = $v;
			}
            if('name' == $k || 'username' == $v || 'email' == $v)continue; //columns loaded from #__users not from integration table
            if ($v) $sel_fields[] = "b.`$v` as $k";
        }

		if ($name_field_name)
		{
			$name = $name_field_name;
		}
		else
		{
			$name = 'name';
		}

        $s = implode(',', $sel_fields);
        // Get the users
        $query = "SELECT u.id as userid,
                    u.username as username,
                    b.`$name` as name,
                    u.email AS email,
                    u.block,
                count( j.userid ) as nr_jobs,
                sum( if(featured<>'none',1,0)) as nr_featured,
                sum( if(published='1' and close_offer<>'1',1,0)) as nr_published,
                sum( if(close_offer='1',1,0)) as nr_closed,
                sum( if(close_by_admin='1',1,0)) as nr_blocked,
                $s
                FROM #__users AS u " .
                " LEFT JOIN  `$tableName` AS b ON u.id=b.`$tableKey` " .
                " LEFT JOIN #__jobsfactory_jobs AS j ON j.userid = u.id " .
                $where .
                " GROUP BY u.id " . $order;

        $db->setQuery($query, $pagination->limitstart, $pagination->limit);
        $rows = $db->loadObjectList();

        $query = " SELECT u.id, count(*) as applied "
                    . " FROM `#__jobsfactory_candidates` as a "
                    . " LEFT JOIN `#__users` as u ON a.userid=u.id "
                    . " LEFT JOIN #__jobsfactory_jobs AS j ON j.userid = u.id "
                    . " WHERE a.cancel=0 "
                    . " GROUP BY u.id ";
        $db->setQuery($query);
        $lists['applied'] = $db->loadObjectList('id');

        $view = $this->getView('users', 'html');
        $view->users = $rows;
        $view->lists = $lists;
        $view->pagination = $pagination;
        $view->order_Dir = $filter_order_Dir;
        $view->order = $filter_order;
        $view->search = $search;
        $view->cfg = $cfg;

        $view->display('list');
    }

    function DetailUser()
    {
        $app = JFactory::getApplication();
        $cid = $app->input->get('cid', array(), 'array');
        $database = JFactory::getDbo();
        $query = $database->getQuery(true);

        if (count($cid) < 1) {
            echo "<script> alert('" . JText::_("COM_JOBS_NO_USER_SELECTED") . "'); window.history.go(-1);</script>\n";
            exit;
        }

        $id = $cid[0];
        $userprofile = JobsHelperTools::getUserProfileObject();

        if (!$userprofile->checkProfile($id)) {
            echo "<script>alert('" . JText::_("COM_JOBS_PROFILE_NOT_FILLED_YET") . "');</script>";
            echo "<script>history.go(-1);</script>";
            return;
        }
        $userprofile->getUserProfile($id);

        $lists = array();
        $lists["user_fields"] = CustomFieldsFactory::getFieldsList('user_profile');

        $query = "SELECT count(*) AS nr_jobs, max(start_date) AS last_ad_date,min(start_date) AS first_ad_date  FROM #__jobsfactory_jobs WHERE userid=$id";
        $database->setQuery($query);
        $res = $database->loadAssocList();

        $lists['nr_jobs']           = $res[0]['nr_jobs'];
        $lists['last_ads_placed']   = $res[0]['last_ad_date'];
        $lists['first_ads_placed']  = $res[0]['first_ad_date'];
        $lists['age']               = JobsHelperUser::getUserAge($userprofile->userid);

        $lists['isCompany']         = JobsHelperTools::isCompany($userprofile->userid);
        $lists['isCandidate']       = JobsHelperTools::isCandidate($userprofile->userid);

        if ($lists['isCompany']) {
            $editorAboutUs = JEditor::getInstance(JFactory::getConfig()->get('editor'));
            $lists["about_us"] = $editorAboutUs->display('about_us', $userprofile->about_us, '100%', '400', '70', '15');
        }

        JTheFactoryHelper::modelIncludePath('payments');
        $model      = JModelLegacy::getInstance('Balance', 'JTheFactoryModel');
        $balance    = $model->getUserBalance($id);

        $view = $this->getView('users', 'html');
        $view->user = clone $userprofile;
        $view->lists = $lists;
        $view->balance = $balance;

        $view->display();
    }

    function Reported_Jobs()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $app = JFactory::getApplication();
        $where = array();

        $context        = 'com_jobsfactory.reported_jobs';
        $filter_order   = $app->getUserStateFromRequest($context . 'filter_order', 'filter_order', 'title', 'cmd');
        $filter_order_Dir   = $app->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', '', 'word');
        $filter_progress    = $app->getUserStateFromRequest($context . 'filter_progress', 'filter_progress', '', 'string');
        $filter_solved  = $app->getUserStateFromRequest($context . 'filter_solved', 'filter_solved', '0', 'string');
        $search         = $app->getUserStateFromRequest($context . 'search', 'search', '', 'string');
        $search         = JString::strtolower($search);
        $limit          = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart     = $app->getUserStateFromRequest($context . 'limitstart', 'limitstart', 0, 'int');
        // In case limit has been changed, adjust limitstart accordingly
        $limitstart     = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        $order = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir . '';

        if ($search)
            $where[] = "b.title LIKE '%$search%' OR a.message LIKE '%$search%'";
        if (isset($filter_progress) && $filter_progress != "") {
            $where[] = "a.processing = '$filter_progress' ";
        }
        if (isset($filter_solved) && $filter_solved != "") {
            $where[] = "a.solved = '$filter_solved' ";
        }

        // Build the where clause of the content record query
        $where = (count($where) ? ' WHERE ' . implode(' AND ', $where) : '');

        // Get the total number of records
        $query = "SELECT count(*) FROM #__jobsfactory_report_jobs a " .
            "LEFT JOIN #__jobsfactory_jobs AS b ON a.job_id=b.id " .
            "LEFT JOIN #__users AS u ON a.userid=u.id " .
            $where;
        $db->setQuery($query);
        $total = $db->loadResult();

        // Create the pagination object
        jimport('joomla.html.pagination');
        $pagination = new JPagination($total, $limitstart, $limit);

        // Get the articles
        $query = "SELECT a.*,b.title,u.username,b.close_by_admin FROM #__jobsfactory_report_jobs a " .
            "LEFT JOIN #__jobsfactory_jobs AS b ON a.job_id=b.id " .
            "LEFT JOIN #__users AS u ON a.userid=u.id " .
            $where . $order;
        $db->setQuery($query, $pagination->limitstart, $pagination->limit);
        $rows = $db->loadObjectList();

        // table ordering
        $lists['order_Dir'] = $filter_order_Dir;
        $lists['order'] = $filter_order;

        // search filter
        $lists['search'] = $search;
        $lists['progress'] = $filter_progress;
        $lists['solved'] = $filter_solved;

        $view = $this->getView('job', 'html');

        $view->jobs = $rows;
        $view->pagination = $pagination;
        $view->order_Dir = $filter_order_Dir;
        $view->order = $filter_order;
        $view->search = $search;
        $view->progress = $filter_progress;
        $view->solved = $filter_solved;

        $view->display('list_reported');
    }


	public function solved($status = 'solved')
	{
		$app = JFactory::getApplication();
		$cid = $app->input->get('cid', array(), 'array');
		$database = JFactory::getDbo();

		switch ($status) {
			default:
			case "solved":
				$set = " solved=1, processing=0 ";
				break;
			case "unsolved":
				$set = " solved=0 ";
				break;
			case "processing":
				$set = " solved=0, processing=1 ";
				break;
			case "unprocessing":
				$set = " processing=0 ";
				break;
		}

		$cids = implode(',', $cid);

		$query = "UPDATE #__jobsfactory_report_jobs"
			. "\n SET $set "
			. "\n WHERE job_id IN ( $cids )";
		$database->setQuery($query);

		try
		{
			$database->execute();
		}
		catch (RuntimeException $e)
		{
			echo "<script type='text/javascript'> alert('" . $e->getMessage() . "'); window.history.go(-1); </script>\n";
			exit();
		}

		$this->setRedirect(JURI::root() . 'administrator/index.php?option=com_jobsfactory&task=reported_jobs', JText::_("COM_JOBS_REPORT_STATUS_CHANGED"));
	}

	public function unsolved()
	{
		$this->solved('unsolved');
	}

	// Replaced by individual functions.
    /*function Change_Reported_Status()
    {
        $app = JFactory::getApplication();
        $cid = $app->input->get('cid', array(), 'array');
        $database = JFactory::getDbo();
        $query = $database->getQuery(true);

		if (count($cid) < 1) {
			echo "<script type='text/javascript'> alert('" . JText::_("COM_JOBS_NO_JOB_SELECTED") . "'); window.history.go(-1);</script>\n";
			exit;
		}

        $status = $app->input->getString('status');


        switch ($status) {
            default:
            case "solved":
                $set = " solved=1, processing=0 ";
                break;
            case "unsolved":
                $set = " solved=0 ";
                break;
            case "processing":
                $set = " solved=0, processing=1 ";
                break;
            case "unprocessing":
                $set = " processing=0 ";
                break;
        }

        $cids = implode(',', $cid);

        $query = "UPDATE #__jobsfactory_report_jobs"
            . "\n SET $set "
            . "\n WHERE job_id IN ( $cids )";
        $database->setQuery($query);

		try
		{
			$database->execute();
		}
		catch (RuntimeException $e)
		{
			echo "<script type='text/javascript'> alert('" . $e->getMessage() . "'); window.history.go(-1); </script>\n";
			exit();
		}

        $this->setRedirect(JURI::root() . 'administrator/index.php?option=com_jobsfactory&task=reported_jobs', JText::_("COM_JOBS_REPORT_STATUS_CHANGED"));
    }*/

    function Googlemap_Tool()
    {
        $app = JFactory::getApplication();
        $cfg = JTheFactoryHelper::getConfig();

        $format = $app->input->get('format', 'html');
        JHtml::_('behavior.framework', true);

        $view = $this->getView('settingspanel', $format);
        $view->cfg = $cfg;

        $view->display('googlemap_tool');
    }

    function Write_Admin_Message()
    {
        $job_id = JFactory::getApplication()->input->getInt('job_id', 0);
        $return_task = JFactory::getApplication()->input->getWord('return_task', 'jobs');

        $job = JTable::getInstance('jobs', 'Table');
        if (!$job->load($job_id)) {
            $this->setRedirect("index.php?option=com_jobsfactory&task=$return_task", JText::_("COM_JOBS_ERROR__JOB_DOES_NOT_EXIST") . $job_id);
            return;
        }

        $usr = JFactory::getUser($job->userid);
        $job->username = $usr->username;

        $view = $this->getView('job', 'html');
        $view->job = $job;
        $view->user = $usr;
        $view->return_task = $return_task;

        $view->display('adminmessage');
    }

    function Send_Message_Job()
    {
        $app = JFactory::getApplication();
        $job_id = $app->input->getInt('job_id', 0);
        $return_task = $app->input->getWord('return_task', 'jobs');
        $message = $app->input->getHtml('message', '');
        $job = JTable::getInstance('jobs', 'Table');

        if (!$job->load($job_id)) {
            $this->setRedirect("index.php?option=com_jobsfactory&task=$return_task", JText::_("COM_JOBS_ERROR__JOB_DOES_NOT_EXIST") . $job_id);
            return;
        }
        if (!$message) {
            $this->setRedirect("index.php?option=com_jobsfactory&task=edit&cid=" . $job->id, JText::_("COM_JOBS_YOU_CAN_NOT_SEND_AN_EMPTY_MESSAGE"));
            return;
        }

        $job->store();
        $job->sendNewMessage($message);

        $owner = JFactory::getUser($job->userid);

        $job->SendMails(array($owner), 'job_admin_message');

        $this->setRedirect("index.php?option=com_jobsfactory&task=edit&cid=" . $job->id, JText::_("COM_JOBS_MESSAGE_SENT"));
    }

    function ChangeProfileIntegration()
    {
        $MyApp  = JTheFactoryApplication::getInstance();
        $cfg    = JTheFactoryHelper::getConfig();
        $cfg->profile_mode = $this->input->get('profile_mode');

        JTheFactoryHelper::modelIncludePath('config');

        $formxml = JPATH_ROOT . DS . "administrator" . DS . "components" . DS . APP_EXTENSION . DS . $MyApp->getIniValue('configxml');
        $model = JModelLegacy::getInstance('Config', 'JTheFactoryModel', array('formxml' => $formxml));

        $model->save($cfg);

        $this->setRedirect("index.php?option=com_jobsfactory&task=integration", JText::_("COM_JOBS_SETTINGS_SAVED"));
    }

    function Integration()
    {
        $cfg = JTheFactoryHelper::getConfig();

        $profile_modes = array();
        $profile_modes[] = JHTML::_('select.option', 'component', 'Component Profile');
        $profile_modes[] = JHTML::_('select.option', 'cb', 'Community Builder');
        $profile_modes[] = JHTML::_('select.option', 'love', 'Love Factory');

        $view = $this->getView('settingspanel', 'html');
        $view->profile_select_list = JHTML::_("select.genericlist", $profile_modes, 'profile_mode', '', 'value', 'text', $cfg->profile_mode);
        $view->current_profile_mode = $cfg->profile_mode;
        $view->configure_link = "index.php?option=com_jobsfactory&task=integrationconfiguration";

        $view->display('integration');
    }

    function IntegrationConfiguration()
    {
        $cfg = JTheFactoryHelper::getConfig();
        if ($cfg->profile_mode == 'component') {
            $view = $this->getView('settingspanel', 'html');
            $view->display('integration');
            return;
        }
        else
        {
            $MyApp = JTheFactoryApplication::getInstance();
            $integrationClass = 'JTheFactoryIntegration' . ucfirst($cfg->profile_mode);
            JLoader::register($integrationClass, $MyApp->app_path_admin . 'integration/' . $cfg->profile_mode . '.php');

            $controller_class = 'JTheFactoryIntegration' . $cfg->profile_mode . 'Controller';
            $controller = new $controller_class;
            $controller->execute('display');
            return;
        }
    }

    function PurgeCache()
    {
        jimport('joomla.filesystem.folder');
        $dir = JOB_TEMPLATE_CACHE;
        if (!JFolder::exists($dir))
            JFolder::create($dir);
        $requested_by = $_SERVER['HTTP_REFERER'];
        if (JFolder::exists($dir)) {
            if (is_writable($dir)) {
                $smarty = new JTheFactorySmarty();
                $smarty->clear_compiled_tpl();
                $this->setRedirect($requested_by, JText::_("COM_JOBS_CACHED_CLEARED"));
            } else
                $this->setRedirect($requested_by, JText::_("COM_JOBS_PERMISSION_UNAVAILABLE_FOR_THIS"));
        } else
            $this->setRedirect($requested_by, JText::_("COM_JOBS_CACHE_FOLDER_DOESNT_EXIST"));
    }

    function Del_Comment()
    {
        $cid    = $this->input->get('cid', array(), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        foreach ($cid as $id) {
            $db = JFactory::getDBO();
            $db->setQuery("DELETE FROM #__jobsfactory_messages WHERE id = $id");
            $db->execute();
        }

        $this->setRedirect("index.php?option=com_jobsfactory&task=comments_administrator", JText::_("COM_JOBS_COMMENT_REMOVED"));
    }

    function Toggle_Comment()
    {
        $cid    = $this->input->get('cid', array(), 'array');
        if (empty($cid)) {
            JFactory::getApplication()->enqueueMessage(JText::_("JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST"),'warning');
        }

        foreach ($cid as $id) {
            $db = JFactory::getDbo();
            $db->setQuery("UPDATE #__jobsfactory_messages SET published=1-published WHERE id = $id");
            $db->execute();
        }

        $this->setRedirect("index.php?option=com_jobsfactory&task=comments_administrator", JText::_("COM_JOBS_COMMENT_STATUS_TOGGLED"));
    }

    function Comments_Administrator()
    {
        $app = JFactory::getApplication();

        $context = 'com_jobsfactory.comments.';
        $filter_order = $app->getUserStateFromRequest($context . 'filter_order', 'filter_order', 'modified', 'cmd');
        $filter_order_Dir = $app->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', 'ASC', 'word');

        $limit = $app->getUserStateFromRequest($context . 'limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart = $app->getUserStateFromRequest($context . 'limitstart', 'limitstart', 0, 'int');

        // In case limit has been changed, adjust limitstart accordingly
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        $db = JFactory::getDbo();
        $db->setQuery("SELECT COUNT(*) FROM #__jobsfactory_messages");
        $total = $db->loadResult();

        $db->setQuery(
            " SELECT r.* , u.username as rauthor, ur.username as user_replied, a.title as jobtitle
    		FROM `#__jobsfactory_messages` as r
    		LEFT JOIN #__users AS u ON r.userid1 = u.id 
    		LEFT JOIN #__users AS ur ON r.userid2 = ur.id 
    		LEFT JOIN `#__jobsfactory_jobs` as a on r.job_id=a.id
    		ORDER BY $filter_order $filter_order_Dir ", $limitstart, $limit
        );

        $rows = $db->loadObjectList();

        jimport('joomla.html.pagination');
        $pagination = new JPagination($total, $limitstart, $limit);

        $view = $this->getView('messages', 'html');
        $view->order = $filter_order;
        $view->order_Dir = $filter_order_Dir;
        $view->messages = $rows;
        $view->pagination = $pagination;

        $view->display();
    }

    function installTemplates()
    {
        jimport('joomla.filesystem.folder');
        if (JFolder::exists(JPATH_SITE . DS . 'components' . DS . 'com_jobsfactory' . DS . 'templates-dist')) {
            JFolder::delete(JPATH_SITE . DS . 'components' . DS . 'com_jobsfactory' . DS . 'templates');
            JFolder::move(JPATH_SITE . DS . 'components' . DS . 'com_jobsfactory' . DS . 'templates-dist',
                JPATH_SITE . DS . 'components' . DS . 'com_jobsfactory' . DS . 'templates');
            $message = JText::_('COM_JOBS_TEMPLATES_OVERWRITTEN');
        } else
        {
            $message = JText::_('COM_JOBS_NEW_TEMPLATES_NOT_FOUND_PLEASE_CHECK_INSTALLATION_KIT');
        }

        $this->setRedirect('index.php?option=com_jobsfactory&task=settingsmanager', $message);
    }

    function postUpgrade()
    {
        $view = $this->getView('install', 'html');
        $view->display("upgrade");
    }

}
