<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableLocation extends JTable {
	var $id             = null;
	var $locname        = null;
	var $country        = null;
    var $hash           = null;
	var $ordering       = null;
	var $status		    = null;

	function __construct( &$db )
	{
        $myApp=JTheFactoryApplication::getInstance();
		parent::__construct($myApp->getIniValue('table','locations'),'id',$db);
	}

    function getLocationId($location) {
        $db = $this->getDbo();

        $db->setQuery("SELECT id FROM #__jobsfactory_locations WHERE locname LIKE '{$location}' OR id ='{$location}'  ");
        return $db->loadResult();
    }
}

