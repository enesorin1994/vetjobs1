<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class TableCandidates extends JTable
{
	var $id					= null;
	var $job_id			= null;
	var $userid				= null;
	var $comments			= null;
	var $cancel				= null;
	var $accept				= null;
	var $modified			= null;
    var $message  			= null;
  
	function __construct(&$db)
	{
		parent::__construct( '#__jobsfactory_candidates', 'id', $db );
	}

    public function get($property, $default = null)
    {
        if (method_exists($this, 'get' . ucfirst($property))) { //Getter
            if (isset($this->_cache) && is_object($this->_cache) && isset($this->_cache->$property)) {
                return $this->_cache->$property;
            }
            $method = 'get' . ucfirst($property);
            $res = self::$method($default);
            $this->setCache($property, $res);
            return $res;
        }
        if (strpos($property, '.')) { //External object !
            $p = explode('.', $property);
            $class = 'JobsHelper' . ucfirst($p[0]) . 'Candidate';
            array_shift($p);
            if (count($p) == 1) $p = $p[0];
            if (class_exists($class))
                return call_user_func(array($class, 'get'), $this, $p);

        }
        return parent::get($property, $default);
    }

    function setCache($property, $value)
    {
        if (!isset($this->_cache) || !is_object($this->_cache)) {
            $this->_cache = new StdClass();
        }
        $this->_cache->$property = $value;
    }

    function setCacheObject($candidate_object)
    {
        //used to store extended properties got by model
        $this->_cache = $candidate_object;
    }

    function getUsername()
    {
        $user = JFactory::getUser($this->userid);
        return $user ? $user->username : "";
    }

    function getName()
    {
        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile($this->userid);
        return $userprofile ? $userprofile->surname. ' '. $userprofile->name : "";
    }

    function isMyJob()
    {
        $my = JFactory::getUser();
        if ($my->id && $my->id == $this->userid) return true;
        else return false;
    }

    /**
        *
        * @param $userlist
        * @param $mailtype
        */
       function SendMails($userlist, $mailtype)
       {

           $config = JFactory::getConfig(); //joomla config
           $cfg = JTheFactoryHelper::getConfig(); //jobs config
           $mail_from = $config->get("mailfrom");
           $sitename = $config->get("sitename");

           set_time_limit(0);
           ignore_user_abort();
           JTheFactoryHelper::tableIncludePath('mailman');
           $mail_body = JTable::getInstance('MailmanTable', 'JTheFactory');
           if (!$mail_body->load($mailtype)) return;
           if (!$mail_body->enabled) return;
           if (count($userlist) <= 0) return;
           // single query
           $catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');
           $catname = $catModel->getCategoryPathString($this->cat);

           $user = clone JobsHelperTools::getUserProfileObject();

           $db = JFactory::getDbo();
           $query = $db->getQuery(true);

           //query candidate phone
           foreach ($userlist as $u) {

               $user->getUserProfile($u->id);
               $dateformat = ($cfg->enable_hour) ? ($cfg->date_format . " " . $cfg->date_time_format) : ($cfg->date_format);

               $mess = str_replace("%NAME%", $user->name, $mail_body->content);
               $mess = str_replace("%SURNAME%", $user->surname, $mess);
               $mess = str_replace("%CATTITLE%", $catname, $mess);
               $mess = str_replace("%JOBTITLE%", $this->title, $mess);
               $mess = str_replace("%JOBLINK%", JURI::root() . '/index.php?option=com_jobsfactory&task=viewapplications&id=' . $this->id, $mess);
               $mess = str_replace("%EMPLOYEREMAIL%", $user->email, $mess);
               $mess = str_replace("%EMPLOYERPHONE%", $user->phone, $mess);
               $mess = str_replace("%STARTDATE%", JHtml::date($this->start_date, $cfg->date_format, false), $mess);
               $mess = str_replace("%ENDDATE%", JHtml::date($this->end_date, $dateformat, false), $mess);

               $subj = str_replace("%NAME%", $user->name, $mail_body->subject);
               $subj = str_replace("%SURNAME%", $user->surname, $subj);
               $subj = str_replace("%CATTITLE%", $catname, $subj);
               $subj = str_replace("%JOBTITLE%", $this->title, $subj);
               $subj = str_replace("%JOBLINK%", JURI::root() . '/index.php?option=com_jobsfactory&task=viewapplications&id=' . $this->id, $subj);
               $subj = str_replace("%EMPLOYEREMAIL%", $user->email, $subj);
               $subj = str_replace("%EMPLOYERPHONE%", $user->phone, $subj);
               $subj = str_replace("%STARTDATE%", JHtml::date($this->start_date, $cfg->date_format, false), $subj);
               $subj = str_replace("%ENDDATE%", JHtml::date($this->end_date, $dateformat, false), $subj);

               if ($user->email) {
                   JFactory::getMailer()->sendMail($mail_from, $sitename, $user->email, $subj, $mess, true);
               } else {
                   if ($user->contactemail)
                       JFactory::getMailer()->sendMail($mail_from, $sitename, $user->contactemail, $subj, $mess, true);
               }
           }
       }
}
