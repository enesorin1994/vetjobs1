<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class TableCountry extends JTable
{
    var $id;
    var $name;
    var $simbol;
    var $active;  

	function __construct(&$db)
	{
		parent::__construct( '#__jobsfactory_country', 'id', $db );
	}

    function getCountryId($name) {
        $db = JFactory::getDbo();
        $db->setQuery("SELECT id FROM #__jobsfactory_country WHERE name LIKE '{$name}' ");

        return $db->loadResult();
    }

    function getFilterCountryId($name) {
        $db = JFactory::getDbo();
        $db->setQuery("SELECT id FROM #__jobsfactory_country WHERE name LIKE '%{$name}%' ");

        return $db->loadAssocList();
    }

    function getCountryName($countryid) {
        $db = JFactory::getDbo();
        $countryName = '';

        if (isset($countryid) && $countryid != 0) {
            $db->setQuery("SELECT name FROM #__jobsfactory_country WHERE id ='{$countryid}' ");
            $countryName = $db->loadResult();
        }

        return $countryName;
    }
 
}

