<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableJobs extends FactoryFieldsTbl
{
    var $id;
    var $userid;
    var $title;
    var $published;
    var $job_type;
    var $shortdescription;
    var $description;
    var $languages_request;
    var $experience_request;
    var $studies_request;
    var $benefits;
    var $job_cityname;
    var $job_cityid;
    var $job_location_state;
    var $about_visibility;
    var $employer_visibility;
    var $jobuniqueID;
    var $start_date;
    var $end_date;
    var $jobs_available;
    var $closed_date;
    var $close_offer;
    var $close_by_admin;
    var $hits;
    var $modified;
    var $cat;
    var $has_file;
    var $file_name;
    var $show_candidate_nr;
    var $featured;
    var $cancel_reason;
    var $googlex;
    var $googley;
    var $approved;

    function __construct(&$db)
    {
        parent::__construct('#__jobsfactory_jobs', 'id', $db, null, 'cat');
    }

    public function get($property, $default = null)
    {
        if (method_exists($this, 'get' . ucfirst($property))) { //Getter
            if (isset($this->_cache) && is_object($this->_cache) && isset($this->_cache->$property)) {
                return $this->_cache->$property;
            }
            $method = 'get' . ucfirst($property);
            $res = self::$method($default);
            $this->setCache($property, $res);
            return $res;
        }
        if (strpos($property, '.')) { //External object !
            $p = explode('.', $property);
            $class = 'JobsHelper' . ucfirst($p[0]) . 'Job';

            array_shift($p);
            if (count($p) == 1) $p = $p[0];
            if (class_exists($class))
                return call_user_func(array($class, 'get'), $this, $p);
        }

        return parent::get($property, $default);
    }

    function setCache($property, $value)
    {
        if (!isset($this->_cache) || !is_object($this->_cache)) {
            $this->_cache = new StdClass();
        }
        $this->_cache->$property = $value;
    }

    function setCacheObject($job_object)
    {
        //used to store extended properties got by model
        $this->_cache = $job_object;
    }

    public function bind($array, $ignore = '')
    {
       return parent::bind($array, $ignore);
    }

    function getFavorite()
    {
        $result = 0;
        $user = JFactory::getUser();
        if ($user->id) {
            $db = $this->getDbo();
            $db->setQuery("select count(*) from `#__jobsfactory_watchlist` where `job_id`={$this->id} and `userid`={$user->id}");
            $result = $db->loadResult() ? 1 : 0;
        }
        return $result;
    }

    function getUsername()
    {
        $user = JFactory::getUser($this->userid);
        return $user ? $user->username : "";
    }

    function getCompanyname()
    {
        $user = JFactory::getUser($this->userid);

        $db = $this->getDbo();
        $db->setQuery("SELECT name FROM `#__jobsfactory_users` WHERE `userid` = '{$user->id}' ");
        $companyname = $db->loadResult();

        return $companyname ? $companyname : "";
    }

    function getCatname()
    {
        $cattable = JTable::getInstance('category', 'JTheFactoryTable');
        $cattable->load($this->cat);
        return $cattable->catname;
    }

    function getStartDate_Text()
    {
        $cfg = JTheFactoryHelper::getConfig();
        return JHtml::date($this->start_date, $cfg->date_format, false);
    }

    function getEndDate_Text()
    {
        if ($this->end_date && $this->end_date != '0000-00-00 00:00:00') {
            $cfg = JTheFactoryHelper::getConfig();
            $dateformat = ($cfg->enable_hour) ? ($cfg->date_format . " " . $cfg->date_time_format) : ($cfg->date_format);
            return JHtml::date($this->end_date, $dateformat, false);
        } else return "";
    }

    /**
     * @return string
     */
    function getClosed_Date_Text()
    {
        if ($this->closed_date && $this->closed_date != '0000-00-00 00:00:00') {
            $cfg = JTheFactoryHelper::getConfig();
            return JHtml::date($this->closed_date, $cfg->date_format);
        } else return "";
    }

    function getModifiedDate_Text()
    {
        if ($this->modified && $this->modified != '0000-00-00 00:00:00') {
            $cfg = JTheFactoryHelper::getConfig();
            $dateformat = ($cfg->enable_hour) ? ($cfg->date_format . " " . $cfg->date_time_format) : ($cfg->date_format);
            return JHtml::date($this->modified, $dateformat, false);
        } else return "";
    }

    function getCountdown()
    {
        return JobsHelperDateTime::dateToCountdown($this->end_date);
    }

    function getExpired()
    {
        $diff = JobsHelperDateTime::dateDiff($this->end_date);
        return ($diff>0);
    }

    public function getTags()
    {
        if (!$this->id) return "";
        $tag_obj = JTable::getInstance('tags', 'Table');
        return $tag_obj->getTagsAsString($this->id);
    }

    function getJob_country()
    {
        if (!$this->id) return "";
        $db=$this->getDbo();
        $db->setQuery("SELECT name FROM `#__jobsfactory_country` WHERE `id` = '{$this->job_country}' ");
        return $db->loadResult();
    }


    function getNr_Applicants()
    {
        if ($this->show_candidate_nr || $this->isMyJob())
        {
            $db=$this->getDbo();
    		$db->setQuery("SELECT count(distinct userid) FROM `#__jobsfactory_candidates` WHERE `job_id` = '{$this->id}' and cancel=0 ");
            return $db->loadResult();
        } else
            return "-";
    }

    function getNr_NewApplicants()
    {
        if ($this->show_candidate_nr || $this->isMyJob())
        {
            $db=$this->getDbo();
            $db->setQuery("SELECT count(distinct userid) FROM `#__jobsfactory_candidates` WHERE `job_id` = '{$this->id}' and cancel=0 AND `action` = '' ");
            return $db->loadResult();
        } else
            return "0";
    }

    function getNr_Applications()
    {
        if ($this->show_candidate_nr || $this->isMyJob())
        {
            $db=$this->getDbo();
    		$db->setQuery("SELECT count(*) FROM `#__jobsfactory_candidates` WHERE `job_id` = '{$this->id}' and cancel=0 ");
            return $db->loadResult();
        } else
            return "-";

    }

    function isAllowedImage($ext)
    {
        return in_array(strtoupper($ext), array('JPEG', 'JPG', 'GIF', 'PNG'));
    }

    function getCompany($userid)
    {
        $db = $this->getDbo();
        $db->setQuery("SELECT name FROM #__jobsfactory_users WHERE userid = '{$userid}'");
        return $db->loadResult();
    }

    function isMyJob()
    {
        $my = JFactory::getUser();
        if ($my->id && $my->id == $this->userid) return true;
        else return false;
    }

    function getMessages()
    {
        $user_sql = "";
        if ($this->employer_visibility == EMPLOYER_TYPE_CONFIDENTIAL && !$this->isMyJob()) {
            $user = JFactory::getUser();
            $userid = (int)$user->id;
            $user_sql = " AND (m.userid1 in (0, '" . $userid . "') or m.userid2 in (0 ,'" . $userid . "'))"; //Only Mine and Guest messages
        }

        $sql = "SELECT m.*, u.username AS fromuser , p.username AS touser FROM #__jobsfactory_messages AS m" .
            " LEFT JOIN #__users AS u ON m.userid1 = u.id " .
            " LEFT JOIN #__users AS p ON m.userid2 = p.id " .
            " WHERE m.job_id = '" . $this->id . "' and `published`=1 " . $user_sql . " ORDER BY m.modified DESC";

        $db = $this->getDbo();
        $db->setQuery($sql);
        return $db->loadObjectList();
    }

    function getJobType($jobtype){

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query ="SELECT a.typename
                   FROM `#__jobsfactory_jobtype` as a
                   WHERE a.id='".$jobtype."'";

        $db->setQuery($query);
        return $db->loadResult();
    }

    function getExperienceRequested($experience){

        $cond = ($experience == '') ? 1 : $experience;

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query ="SELECT e.levelname
                   FROM `#__jobsfactory_experiencelevel` as e
                   WHERE e.id IN (".$cond.")";

        $db->setQuery($query);
        $requested_experience = $db->loadObjectList();

        if ($requested_experience) {
            $experience = array();

            foreach ($requested_experience as $k=>$expname) {
                $experience[] = "{$expname->levelname}";
            }
        }

        if (count($experience)) {
            return implode(",", $experience);
        } else {
            return '';
        }

    }

    function getStudiesRequested($studiesId){

        $cond = ($studiesId == 0) ? 1 : $studiesId;

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query ="SELECT s.levelname
                   FROM `#__jobsfactory_studieslevel` as s
                   WHERE s.id='".$cond."'";

        $db->setQuery($query);
        return $db->loadResult();
    }


    function getRequestedExperiences(&$job)
    {
        $db = $this->getDbo();
        $db->setQuery("SELECT experience_request FROM `#__jobsfactory_jobs` where `id`='{$job->id}'");

        return $db->loadResult();
    }

    function setRequestedExperiences(&$job,$requested_experience)
    {
        if (!count($requested_experience)) retun; //no experiences assigned
        // add category assignments
        $inserts = array();
        foreach ($requested_experience as $expid){
            $inserts[] = "{$expid}";
        }

        if (count($inserts))
        {
            return implode(",",$inserts);
            //return $inserts;

        } else {
            return '(1)';
        }
    }

    function getApplicationList()
    {
        $my = JFactory::getUser();
        $where = " where job_id='{$this->id}' and a.cancel=0";

        if ($this->employer_visibility == EMPLOYER_TYPE_CONFIDENTIAL && !$this->isMyJob() )
            //Private job offer
            $where .= "  and userid='{$my->id}'";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query ="SELECT a.*, u.name, u.username
                   FROM `#__jobsfactory_candidates` as a
                  LEFT JOIN `#__users` as u ON a.userid=u.id
                  $where
		        ";

        $db->setQuery($query);
        return $db->loadObjectList();
    }

    public function delete($id = null)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        if (file_exists(JOB_UPLOAD_FOLDER . "{$this->id}.attach"))
            @unlink(JOB_UPLOAD_FOLDER . "{$this->id}.attach");

        $db->setQuery("DELETE FROM #__jobsfactory_candidates WHERE job_id='$this->id'"); //remove applications
        $db->execute();
        $db->setQuery("DELETE FROM #__jobsfactory_tags WHERE job_id='$this->id'"); //remove tags
        $db->execute();
        $db->setQuery("DELETE FROM #__jobsfactory_report_jobs WHERE job_id='$this->id'"); //remove reports
        $db->execute();
        $db->setQuery("DELETE FROM #__jobsfactory_messages WHERE job_id='$this->id'"); //remove messages
        $db->execute();
        $db->setQuery("DELETE FROM #__jobsfactory_jobs WHERE id='$this->id'"); //remove the job
        $db->execute();
    }

    function sendNewMessage($message, $message_to = null, $reply_to_message = null)
    {
        $my =  JFactory::getUser();
        $m = JTable::getInstance('messages', 'Table');
        if (!$reply_to_message) {
            $m->parent_message = 0;
            if ($message_to)
                $m->userid2 = $message_to;
            else
                $m->userid2 = $this->userid;
        } else {
            $replytom = JTable::getInstance('messages', 'Table');
            $replytom->load($reply_to_message);
            $replytom->wasread = 1;
            $replytom->store();

            $m->parent_message = $reply_to_message;
            $m->userid2 = $replytom->userid1;
        }

        $m->job_id = $this->id;
        $m->userid1 = $my->id;
        $m->modified = gmdate('Y-m-d H:i:s');
        $m->message = $message;

        JTheFactoryEventsHelper::triggerEvent('onBeforeSendMessage', array($this, $m));
        $m->store();
        JTheFactoryEventsHelper::triggerEvent('onAfterSendMessage', array($this, $m));
    }

    /**
     *
     * @param $userlist
     * @param $mailtype
     */
    function SendMails($userlist, $mailtype)
    {

        $config = JFactory::getConfig(); //joomla config
        $cfg = JTheFactoryHelper::getConfig(); //jobs config
        $mail_from = $config->get("mailfrom");
        $sitename = $config->get("sitename");

        set_time_limit(0);
        ignore_user_abort();

        JTheFactoryHelper::tableIncludePath('mailman');
        $mail_body  = JTable::getInstance('MailmanTable', 'JTheFactory');

        if (!$mail_body->load($mailtype)) return;
        if (!$mail_body->enabled) return;
        if (count($userlist) <= 0) return;

        // single query
        $catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');
        $catname = $catModel->getCategoryPathString($this->cat);

        $user = clone JobsHelperTools::getUserProfileObject();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        foreach ($userlist as $u) {
            if ($mailtype == 'job_admin_approval') {
                $user->name = $u->name;
                $user->email = $u->email;
            } else {
                $user->getUserProfile($u->id);
            }
            $dateformat = ($cfg->enable_hour) ? ($cfg->date_format . " " . $cfg->date_time_format) : ($cfg->date_format);

            $mess = str_replace("%NAME%", $user->name, $mail_body->content);
            $mess = str_replace("%SURNAME%", $user->surname, $mess);
            $mess = str_replace("%CATTITLE%", $catname, $mess);
            $mess = str_replace("%JOBTITLE%", $this->title, $mess);
            $mess = str_replace("%JOBLINK%", JURI::root() . 'index.php?option=com_jobsfactory&task=viewapplications&id=' . $this->id, $mess);
            $mess = str_replace("%EMPLOYEREMAIL%", ($user->email) ? $user->email : $user->contactemail, $mess);
            $mess = str_replace("%EMPLOYERPHONE%", ($user->phone) ? $user->phone : $user->contactphone, $mess);
            $mess = str_replace("%STARTDATE%", JHtml::date($this->start_date, $cfg->date_format, false), $mess);
            $mess = str_replace("%ENDDATE%", JHtml::date($this->end_date, $dateformat, false), $mess);

            $subj = str_replace("%NAME%", $user->name, $mail_body->subject);
            $subj = str_replace("%SURNAME%", $user->surname, $subj);
            $subj = str_replace("%CATTITLE%", $catname, $subj);
            $subj = str_replace("%JOBTITLE%", $this->title, $subj);
            $subj = str_replace("%JOBLINK%", JURI::root() . 'index.php?option=com_jobsfactory&task=viewapplications&id=' . $this->id, $subj);
            $subj = str_replace("%EMPLOYEREMAIL%", ($user->email) ? $user->email : $user->contactemail, $subj);
            $subj = str_replace("%EMPLOYERPHONE%", ($user->phone) ? $user->phone : $user->contactphone, $subj);
            $subj = str_replace("%STARTDATE%", JHtml::date($this->start_date, $cfg->date_format, false), $subj);
            $subj = str_replace("%ENDDATE%", JHtml::date($this->end_date, $dateformat, false), $subj);

			if (!$user->email && !$user->contactemail)
			{
				$user->email = JFactory::getUser($u->id)->email;
			}
			
            if ($user->email) {
                JFactory::getMailer()->sendMail($mail_from, $sitename, $user->email, $subj, $mess, true);
            } else {
                if ($user->contactemail)
                    JFactory::getMailer()->sendMail($mail_from, $sitename, $user->contactemail, $subj, $mess, true);
            }
        }
    }

    function loadFromSession()
    {
        $session = JFactory::getSession();
        if (!$session->has("temp_job", "jobs"))
            return false;

        $c = $session->get("temp_job", null, "jobs");
        $c = unserialize($c);
        if (is_object($c)) {
            $fields = get_object_vars($c);
            foreach ($fields as $k => $v)
                if (!is_object($c->$k) && !(substr($k, 0, 1) == '_'))
                    $this->$k = $c->$k;
        }
        unset($c);
        $session->clear("temp_job", "jobs");

    }

    function saveToSession()
    {
        $session = JFactory::getSession();
        $c = new stdClass(); //due to weird errors the object must be dumbed down
        $fields = get_object_vars($this);
        foreach ($fields as $k => $v)
            if (!is_object($this->$k) && !(substr($k, 0, 1) == '_'))
                $c->$k = $this->$k;
        $session->set("temp_job", serialize($c), "jobs");

    }

    function clearSavedSession()
    {
        $session = JFactory::getSession();
        $session->clear("temp_job", "jobs");
    }

}
