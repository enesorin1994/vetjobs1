<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class TableMessages extends JTable
{
  var $id;
  var $job_id;
  var $userid1;
  var $userid2;
  var $parent_message;
  var $message;
  var $app_id;
  var $modified;
  var $wasread;
  var $published;    

	function __construct(&$db)
	{
		parent::__construct( '#__jobsfactory_messages', 'id', $db );
	}
}
