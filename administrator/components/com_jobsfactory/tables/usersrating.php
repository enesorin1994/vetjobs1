<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class TableUsersRating extends FactoryFieldsTbl
{
    var $id;
    var $userid;
    var $user_rating;
    var $users_count;


    function __construct(&$db)
    {
    	parent::__construct( '#__jobsfactory_users_rating', 'id', $db );
    }

    function isMyCompany()
    {
        $my = JFactory::getUser();
        if ($my->id && $my->id == $this->userid) return true;
        else return false;
    }

    public function bind($array, $ignore = '')
    {
        return parent::bind($array, $ignore);
    }

}

