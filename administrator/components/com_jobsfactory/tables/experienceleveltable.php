<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');
jimport('joomla.html.parameter');

class JTheFactoryExperiencelevelTable extends JTable {

    var $id                = null;
    var $levelname          = null;
    var $published         = null;
    var $ordering          = null;

    function __construct( &$db )
    {
        $myApp = JTheFactoryApplication::getInstance();
        parent::__construct($myApp->getIniValue('table','experiencelevel'),'id',$db);
    }

    public function bind($array, $ignore = '')
    {
        return parent::bind($array, $ignore);
    }

    function getHTMLId()
    {
        if ($this->page)
            $fieldid=$this->page."_".$this->db_name;
        elseif($this->db_name)
            $fieldid="cfield_".$this->db_name;
        else
            return null;
        
        $fieldid=strtolower($fieldid);
        $fieldid=str_replace(
            array(" ","'",'"',"(",")",".","!","?"),
            "_",$fieldid
        );
        return $fieldid;
    }

}
