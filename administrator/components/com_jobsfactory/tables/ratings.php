<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class TableRatings extends FactoryFieldsTbl
{
    var $id;
    var $userid;//user voting
    var $rated_userid;
    var $rating_user;


    function __construct(&$db)
    {
    	parent::__construct( '#__jobsfactory_ratings', 'id', $db );
    }

    public function bind($array, $ignore = '')
    {
        return parent::bind($array, $ignore);
    }

}

