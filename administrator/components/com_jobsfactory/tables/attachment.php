<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableAttachment extends JTable
{

    var $id = null;
    var $fileName = null;
    var $fileExt = null;
    var $fileType = null;
    var $jobId = null;
    var $userid = null;
    var $featured = null;
    var $langId = null;

    function __construct(&$db)
    {
        parent::__construct('#__jobsfactory_attachments', 'id', $db);
    }

    function setCacheObject($job_object)
    {
        //used to store extended properties got by model
        $this->_cache = $job_object;
    }

    function isMyResume()
    {
        $my = JFactory::getUser();
        if ($my->id && $my->id == $this->userid) return true;
        else return false;
    }

    function delete($id){

        $db = $this->getDbo();
        $db->setQuery("SELECT * FROM #__jobsfactory_attachments WHERE id='$id' AND fileType = 'resume' ");
        $pdfresume = $db->loadObject();

        if ($pdfresume != null) {
            if (file_exists(RESUME_UPLOAD_FOLDER . "{$pdfresume->id}.attach"))
                    @unlink(RESUME_UPLOAD_FOLDER . "{$pdfresume->id}.attach");
        }
    }

}
