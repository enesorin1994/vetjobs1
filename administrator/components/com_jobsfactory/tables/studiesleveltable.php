<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

class JTheFactoryStudieslevelTable extends JTable {

    var $id                = null;
    var $levelname          = null;
    var $published         = null;
    var $ordering          = null;

    function __construct( &$db )
    {
        $myApp = JTheFactoryApplication::getInstance();
        parent::__construct($myApp->getIniValue('table','studieslevel'),'id',$db);
    }

    /**
     * Method to bind an associative array or object to the JTable instance.  This
     * method only binds properties that are publicly accessible and optionally
     * takes an array of properties to ignore when binding.
     *
     * @param   array  $array   Named array
     * @param   mixed  $ignore  An optional array or space separated list of properties
     *                          to ignore while binding. [optional]
     *
     * @return  mixed  Null if operation was satisfactory, otherwise returns an error string
     *
     * @since   2.5
     */
    public function bind($array, $ignore = '')
    {
        if (isset($array['params']) && is_array($array['params']))
        {
            $registry = new JRegistry;
            $registry->loadArray($array['params']);
            $array['params'] = (string) $registry;
        }

        return parent::bind($array, $ignore);
    }


    public function store($updateNulls = false)
   	{
       	return parent::store($updateNulls);
   	}

    function getHTMLId()
    {
        if ($this->page)
            $fieldid=$this->page."_".$this->db_name;
        elseif($this->db_name)
            $fieldid="cfield_".$this->db_name;
        else
            return null;
        
        $fieldid=strtolower($fieldid);
        $fieldid=str_replace(
            array(" ","'",'"',"(",")",".","!","?"),
            "_",$fieldid
        );
        return $fieldid;
    }

}
