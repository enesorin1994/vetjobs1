<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableUserExperience extends JTable
{
    var $id;
    var $userid;
    var $company;
    var $position;
    var $salary;
    var $city;
    var $country;
    var $domain;
    var $start_date;
    var $end_date;
    var $exp_description;

    function __construct(&$db)
    {
        parent::__construct('#__jobsfactory_userexperience', 'id', $db);
    }

    function getUserExperience($user_id,$resume_id)
    {
        $db = $this->getDbo();
        $db->setQuery("SELECT * FROM " . $this->_tbl . "
                WHERE userid='" . $user_id . "' ORDER BY start_date DESC");//AND `published`=1
        $tmp = $db->loadObjectList();
        $experienceList = array();

        if ($tmp)
            foreach ($tmp as $key => $value)
                $experienceList[] = $value;

        return $experienceList;
    }

    function getCatname($domain)
    {
        JTheFactoryHelper::tableIncludePath('category');
        $cattable = JTable::getInstance('category', 'JTheFactoryTable');

        $cattable->load($domain);
        return $cattable->catname;
    }

}

