<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class TableUsers extends FactoryFieldsTbl
{
    var $id;
    var $userid;
    var $isCompany;
    var $name;
    var $surname;
    var $birth_date;
    var $gender;
    var $marital_status;
    var $address;
    var $city;
    var $country;
    var $phone;
    var $email;
    var $picture;
    var $googleMaps_x;
    var $googleMaps_y;
    var $shortdescription;
    var $about_us;
    var $website;
    var $activitydomain;
    var $contactname;
    var $contactposition;
    var $contactphone;
    var $contactemail;
    var $facebook;
    var $twitter;
    var $YM;
    var $Hotmail;
    var $Skype;
    var $linkedIN;
    var $total_experience;
    var $desired_salary;
    var $user_achievements;
    var $user_goals;
    var $user_hobbies;
    var $file_name;
    var $modified;
    var $verified;
    var $powerseller;
    var $isVisible;
    var $useruniqueid;

    function __construct(&$db)
    {
    	parent::__construct( '#__jobsfactory_users', 'userid', $db );
    }

    function isAllowedImage($ext)
    {
        return in_array(strtoupper($ext), array('JPEG', 'JPG', 'GIF', 'PNG'));
    }

    function isMyCompany()
    {
        $my = JFactory::getUser();
        if ($my->id && $my->id == $this->userid) return true;
        else return false;
    }

    public function delete($id = null)
    {
        $db = JFactory::getDbo();

        $db->setQuery("DELETE FROM #__jobsfactory_candidates WHERE userid='$this->userid' "); //remove applications
        $db->execute();
        $db->setQuery("DELETE FROM #__jobsfactory_education WHERE userid='$this->userid' "); //remove education
        $db->execute();
        $db->setQuery("DELETE FROM #__jobsfactory_jobs WHERE userid='$this->userid' "); //remove the jobs
        $db->execute();
        $db->setQuery("DELETE FROM #__jobsfactory_messages WHERE userid1='$this->userid' OR userid2='$this->userid'"); //remove messages
        $db->execute();

        //payment balance?
        $db->setQuery("DELETE FROM #__jobsfactory_watchlist WHERE userid='$this->userid' "); //remove watchlist
        $db->execute();

        $db->setQuery("DELETE FROM #__jobsfactory_watchlist_companies WHERE (userid='$this->userid' OR companyid='$this->userid') "); //remove reports
        $db->execute();

        $db->setQuery("DELETE FROM #__jobsfactory_report_jobs WHERE userid='$this->userid'"); //remove reports
        $db->execute();

        $db->setQuery("DELETE FROM #__jobsfactory_users WHERE userid='$this->userid' "); //remove the user record
        $db->execute();
    }

    function SendMails($userlist, $mailtype)
    {

        $config = JFactory::getConfig(); //joomla config
        $cfg = JTheFactoryHelper::getConfig(); //jobs config
        $mail_from = $config->get("mailfrom");
        $sitename = $config->get("sitename");

        set_time_limit(0);
        ignore_user_abort();
        JTheFactoryHelper::tableIncludePath('mailman');
        $mail_body = JTable::getInstance('MailmanTable', 'JTheFactory');
        if (!$mail_body->load($mailtype)) return;
        if (!$mail_body->enabled) return;
        if (count($userlist) <= 0) return;
        // single query
        $user = clone JobsHelperTools::getUserProfileObject();

        foreach ($userlist as $u) {

            $user->getUserProfile($u->id);
            $dateformat = ($cfg->enable_hour) ? ($cfg->date_format . " " . $cfg->date_time_format) : ($cfg->date_format);

            $mess = str_replace("%NAME%", $user->name, $mail_body->content);
            $mess = str_replace("%SURNAME%", $user->surname, $mess);
            $mess = str_replace("%JOBTITLE%", $this->title, $mess);
            $mess = str_replace("%JOBLINK%", JURI::root() . '/index.php?option=com_jobsfactory&task=viewapplications&id=' . $this->id, $mess);
            $mess = str_replace("%EMPLOYEREMAIL%", $user->email, $mess);
            $mess = str_replace("%EMPLOYERPHONE%", $user->phone, $mess);
            $mess = str_replace("%CURRENCY%", $this->currency, $mess);
            $mess = str_replace("%STARTDATE%", JHtml::date($this->start_date, $cfg->date_format, false), $mess);
            $mess = str_replace("%ENDDATE%", JHtml::date($this->end_date, $dateformat, false), $mess);

            $subj = str_replace("%NAME%", $user->name, $mail_body->subject);
            $subj = str_replace("%SURNAME%", $user->surname, $subj);
            $subj = str_replace("%JOBTITLE%", $this->title, $subj);
            $subj = str_replace("%JOBLINK%", JURI::root() . '/index.php?option=com_jobsfactory&task=viewapplications&id=' . $this->id, $subj);
            $subj = str_replace("%EMPLOYEREMAIL%", $user->email, $subj);
            $subj = str_replace("%EMPLOYERPHONE%", $user->phone, $subj);
            $subj = str_replace("%CURRENCY%", $this->currency, $subj);
            $subj = str_replace("%STARTDATE%", JHtml::date($this->start_date, $cfg->date_format, false), $subj);
            $subj = str_replace("%ENDDATE%", JHtml::date($this->end_date, $dateformat, false), $subj);

            if ($user->email) {
                JFactory::getMailer()->sendMail($mail_from, $sitename, $user->email, $subj, $mess, true);
            } else {
               if ($user->contactemail)
                   JFactory::getMailer()->sendMail($mail_from, $sitename, $user->contactemail, $subj, $mess, true);
            }
        }
    }

    public function bind($array, $ignore = '')
    {
        return parent::bind($array, $ignore);
    }

    function setCache($property, $value)
    {
        if (!isset($this->_cache) || !is_object($this->_cache)) {
            $this->_cache = new StdClass();
        }
        $this->_cache->$property = $value;
    }

    public function get($property, $default = null)
    {
        if (method_exists($this, 'get' . ucfirst($property))) { //Getter
            if (isset($this->_cache) && is_object($this->_cache) && isset($this->_cache->$property)) {
                return $this->_cache->$property;
            }
            $method = 'get' . ucfirst($property);
            $res = self::$method($default);
            $this->setCache($property, $res);
            return $res;
        }

        if (strpos($property, '.')) { //External object !    //JobsHelperLinksUser
            $p = explode('.', $property);
            $class = 'JobsHelper' . ucfirst($p[0]) . 'User';

            array_shift($p);
            if (count($p) == 1) $p = $p[0];
            if (class_exists($class))
                return call_user_func(array($class, 'get'), $this, $p);
        }

        return parent::get($property, $default);
    }

    function getUser_country($countryId)
    {
        if (!$this->country && !$countryId) return "";
        $db = $this->getDbo();

        $id = (isset($countryId)) ? $countryId : $this->country;

        $db->setQuery("SELECT name FROM `#__jobsfactory_country` WHERE `id` = '{$id}' ");
        return $db->loadResult();
    }

}

