<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class TableEducation extends JTable
{
    var $id;
    var $userid;
    var $study_level;
    var $edu_institution;
    var $city;
    var $country;
    var $start_date;
    var $end_date;

	function __construct(&$db)
	{
		parent::__construct( '#__jobsfactory_education', 'id', $db );
	}
    
    function getUserEducation($user_id,$resume_id)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query = "SELECT edu.*, sl.levelname, c.name as country
                FROM `#__jobsfactory_education` edu
                    LEFT JOIN `#__jobsfactory_users` res ON res.id=edu.resume_id
                    LEFT JOIN `#__jobsfactory_studieslevel` sl ON sl.id=edu.study_level
                    LEFT JOIN `#__jobsfactory_country` c ON c.id=edu.country
                      WHERE edu.userid='" . $user_id . "' AND sl.`published`=1 AND edu.resume_id = '" .$resume_id."'
                      ORDER BY edu.start_date DESC ";
        $db->setQuery($query);
        $tmp = $db->loadObjectList();

        $educationList = array();

        if ($tmp)
            foreach ($tmp as $key => $value)
                $educationList[] = $value;
        return $educationList;
    }

}

