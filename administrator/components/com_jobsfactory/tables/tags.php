<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class TableTags extends JTable
{
    
  var $id;
  var $job_id;
  var $tagname;
  
	function __construct(&$db)
	{
		parent::__construct( '#__jobsfactory_tags', 'id', $db );
	}
    
    function setTags($parent_id,$tags){

        $cfg = JTheFactoryHelper::getConfig();

        $db = $this->getDbo();
        $db->setQuery("DELETE FROM ".$this->_tbl." WHERE job_id='$parent_id'");
        $db->execute();

        $tag_arr=explode(',',$tags);

        for ($i=0; $i<min(count($tag_arr),$cfg->max_nr_tags); $i++){

            $this->id       = null;
            $this->job_id   = $parent_id;
            $this->tagname  = trim($tag_arr[$i]);
            if( $this->tagname != "")
            	$this->store();
        }
    }

    function getTagsAsArray($parent_id)
    {
        $db = $this->getDbo();
    	$db->setQuery("SELECT tagname FROM ".$this->_tbl." WHERE job_id='".$parent_id."' ORDER BY id");
    	$tmp = $db->loadObjectList("tagname");
		$tagList = array();
		if($tmp)
    	foreach ( $tmp as $key=> $value)
    		$tagList[] = $key;

		return $tagList;
    }

    function getTagsAsString($parent_id)
    {
        $arr = self::getTagsAsArray($parent_id);

        return implode(",", $arr);
    }

 
}
