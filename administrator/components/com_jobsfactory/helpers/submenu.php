<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

class JobsFactorySubmenu
{
    static function subMenuEditJobtype($vName)
    {
        JHtmlSidebar::addEntry(
			JText::_('Job type list'),
			'index.php?option='.APP_EXTENSION.'&task=jobtype.listjobtype',
            $vName == 'listjobtype'
		);
		    
    }

    static function subMenuListJobtype($vName)
    {
        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_SETTINGS'),
            'index.php?option=com_jobsfactory&task=settingsmanager',
            $vName == 'settingsmanager'
        );

        JHtmlSidebar::addEntry(
            JText::_('Job type list'),
            'index.php?option='.APP_EXTENSION.'&task=jobtype.listjobtype',
            $vName == 'listjobtype'
        );

    }

    static function subMenuEditExperiencelevel($vName)
    {
        JHtmlSidebar::addEntry(
            JText::_('Experience level list'),
            'index.php?option='.APP_EXTENSION.'&task=experiencelevel.listexperiencelevel',
            $vName == 'listexperiencelevel'
        );

    }

    static function subMenuListExperiencelevel($vName)
    {
        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_SETTINGS'),
            'index.php?option=com_jobsfactory&task=settingsmanager',
            $vName == 'settingsmanager'
        );

        JHtmlSidebar::addEntry(
            JText::_('Experience level list'),
            'index.php?option='.APP_EXTENSION.'&task=experiencelevel.listexperiencelevel',
            $vName == 'listexperiencelevel'
        );

    }

    static function subMenuSettingsManager($vName)
    {
        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_SETTINGS'),
            'index.php?option=com_jobsfactory&task=settingsmanager',
            $vName == 'settingsmanager'
        );
    }

    static function subMenuEditStudieslevel($vName)
    {
        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_STUDIES_LEVEL_LIST'),
            'index.php?option='.APP_EXTENSION.'&task=studieslevel.liststudieslevel',
            $vName == 'liststudieslevel'
        );

    }
    static function subMenuListStudieslevel($vName)
    {
        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_SETTINGS'),
            'index.php?option=com_jobsfactory&task=settingsmanager',
            $vName == 'settingsmanager'
        );

        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_STUDIES_LEVEL_LIST'),
            'index.php?option='.APP_EXTENSION.'&task=studieslevel.liststudieslevel',
            $vName == 'liststudieslevel'
        );

    }

}