<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JobsHelper
{
	public static $extension = 'com_jobsfactory';

	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	$vName	The name of the active view.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public static function addSubmenu($vName)
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_JOBS_MENU_LIST'),
            'index.php?option=com_jobsfactory&task=jobs',
			$vName == 'jobs'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_JOBS_MENU_PAYMENTS'),
			'index.php?option=com_jobsfactory&task=payments.listing',
			$vName == 'payments.listing'
        );

		JHtmlSidebar::addEntry(
			JText::_('COM_JOBS_MENU_MESSAGES'),
			'index.php?option=com_jobsfactory&task=comments_administrator',
			$vName == 'comments_administrator'
		);
        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_MENU_REPORTED'),
            'index.php?option=com_jobsfactory&task=reported_jobs',
            $vName == 'reported_jobs'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_MENU_USERS'),
            'index.php?option=com_jobsfactory&task=users',
            $vName == 'users'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_MENU_SETTINGS'),
            'index.php?option=com_jobsfactory&task=settingsmanager',
            $vName == 'settingsmanager'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_MENU_DASHBOARD'),
            'index.php?option=com_jobsfactory&task=dashboard',
            $vName == 'dashboard'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_JOBS_MENU_ABOUT'),
            'index.php?option=com_jobsfactory&task=about.main',
            $vName == 'about.main'
        );
	}
}
