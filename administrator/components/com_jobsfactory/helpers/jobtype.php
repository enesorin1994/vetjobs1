<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

class JTheFactoryJobtypeHelper
{
    function addJSLanguageStrings()
    {
        /* @var JDocument $doc */
        $doc=JFactory::getDocument();
        $doc->addScriptDeclaration(
        "
            var js_lang_fields=Array();
            js_lang_fields['field_must_be_saved']='".JText::_('You must save the field first!')."';
        "
        );
    }
}
