<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.model'); 

//class JTheFactoryModelJobtype extends JModelLegacy
class JobsAdminModelJobtype extends JModelLegacy
{
    var $context='jobtype';
    var $limit=null;
    var $limitstart=null;
    var $ordering=null;
    var $ordering_dir='ASC';
    var $_total=null;
    var $_pagination=null;
    var $_tablename=null;

    function __construct()
    {
        $this->context=APP_EXTENSION."_jobtype.";
        $this->_tablename='#__'.APP_PREFIX.'_jobtype';

        parent::__construct();
    }    
    function getLimitFromRequest()
    {
		$app	= JFactory::getApplication();
		$this->limit		= $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
		$this->limitstart	= $app->getUserStateFromRequest($this->context.'limitstart', 'limitstart', 0, 'int');

		// In case limit has been changed, adjust limitstart accordingly
		$this->limitstart = ( $this->limit != 0 ? (floor($this->limitstart / $this->limit) * $this->limit) : 0 );

        
    }
    function getOrdering()
    {
		$app	= JFactory::getApplication();
		$this->ordering	= $app->getUserStateFromRequest($this->context.'ordering', 'ordering', 'ordering', 'string');
		$this->ordering_dir	= $app->getUserStateFromRequest($this->context.'ordering_dir', 'ordering_dir', 'ASC', 'string');
    }

    function getJobtypes($enabled_filter=false)
    {
        $db = $this->getDbo();
        $this->getOrdering();
        $this->getLimitFromRequest();
        $where = 'where 1=1';

        if ($enabled_filter) $where.=" and `published`='1'";

        $db->setQuery("SELECT COUNT(*) FROM `".$this->_tablename."` {$where}");
        $this->_total=$db->loadResult();

		$db->setQuery("SELECT * FROM `".$this->_tablename."` {$where} ORDER BY {$this->ordering} {$this->ordering_dir}",
            $this->limitstart,$this->limit);

        return $db->loadObjectList();
    }

    function getPagination()
    {
        if($this->_pagination) return $this->_pagination;
		jimport('joomla.html.pagination');
		$this->_pagination = new JPagination($this->_total, $this->limitstart, $this->limit);
        return $this->_pagination;
    }

}
