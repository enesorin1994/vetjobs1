<?php
    /**------------------------------------------------------------------------
    com_jobsfactory - Jobs Factory 1.6.5
    ------------------------------------------------------------------------
     * @author thePHPfactory
     * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
     * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     * Websites: http://www.thePHPfactory.com
     * Technical Support: Forum - http://www.thePHPfactory.com/forum/
     * @package: Jobs
    -------------------------------------------------------------------------*/

    defined('_JEXEC') or die('Restricted access');

    jimport('joomla.application.component.model');
    jimport('joomla.application.component.helper');

    class JobsAdminModelJob extends JModelLegacy
    {

        var $_name = 'job';
        var $name = 'job';

        function saveJob($job)
        {
            $isNew = !(bool)$job->id;

            $cfg = JTheFactoryHelper::getConfig();

            $error = $this->validateSave($job);
            if (count($error) > 0) return $error;

            $tags = $job->tags;
            unset($job->tags);
            if ($cfg->admin_approval) {
                if (!$job->id)
                    $job->approved = 0;
            } else
                $job->approved = 1;

            if (!$e = $job->store()) {
                $error[] = $e;
                return $error;
            }

            // TAGS
            $tag_obj = JTable::getInstance('tags', 'Table');
            $tag_obj->setTags($job->id, $tags);

            $this->uploadAtachment($job);
            $this->deleteFiles($job);

            return $error;
        }

        function bindJob(&$job)
        {
            $cfg = JTheFactoryHelper::getConfig();
            $app = JFactory::getApplication();

            $job->id = $app->input->getInt('id', null);
            $job->load($job->id);

            $requested_experience = $app->input->get('experience_request', array(), 'array');
            $exp_ids = $this->setRequestedExperiences($job, $requested_experience);
            $requested_studies = $app->input->getInt('studies_request', '');

            $job->bind(JTheFactoryHelper::getRequestArray('request'));

            $job->title = $app->input->getHtml("title");
            $job->cat = $app->input->getInt("cat");
            $job->shortdescription = $app->input->getHtml('shortdescription', '');

            $f  = new  JFilterInput(array(),array(),1,1);
            $job->description = $f->clean($_REQUEST['description'],'html');
            $job->benefits = $f->clean($_REQUEST['benefits'],'html');

            $job->tags = $app->input->getHtml('tags', '');
            //$job->tags = preg_replace('/[^a-zA-Z0-9_]/', '', $app->input->getString("tags"));
            $job->experience_request = $exp_ids;
            $job->studies_request = $requested_studies;

            $customFieldsList = CustomFieldsFactory::getFieldsList('jobs');
            // Dates Custom Fields are saved in Unix timestamp
			foreach ($customFieldsList as $cfield) {
				if ('date' == $cfield->ftype) {

					if (empty($job->{$cfield->db_name})) {
						continue;
					}
					// ToDo Factory: Refactor next 3 lines to use JRegistry
					$params = explode("\n", $cfield->params);
					$param = explode("=", $params[1]);
					$date_format = $param[1];
					// Var to save in db
                    // Var from _POST
					$job->{$cfield->db_name} = strtotime(
                        JobsHelperDateTime::DateToIso(
							$job->{$cfield->db_name},
							$date_format
						)
					);
				}
			}

            if ($job->id) {
                // edit custom field prep
                $job->start_date = JobsHelperDateTime::DateToIso($job->start_date);
                $job->end_date = JobsHelperDateTime::DateToIso($job->end_date);
                $job->start_date = JobsHelperDateTime::isoDateToUTC($job->start_date); //store all in UTC
                $job->end_date = JobsHelperDateTime::isoDateToUTC($job->end_date); //store all in UTC

                if ($job->file_name != "")
                    $this->file_name = $job->file_name;

                $this->attachment = $job->file_name;
                if (isset($_FILES["attachment"])) {
                    $this->attachment = null;
                    if ($cfg->enable_attach && is_uploaded_file(@$_FILES["attachment"]['tmp_name']))
                        $this->attachment = $_FILES["attachment"];
                }

                if ($job->jobuniqueID == '') {
                    $job->jobuniqueID = uniqid(); //microtime().
                }

                $job->modified = gmdate('Y-m-d H:i:s');

            } else {

                // new custom field prep
                $job->end_date = JobsHelperDateTime::DateToIso($job->end_date);
                $job->start_date = JobsHelperDateTime::DateToIso($job->start_date);
                $job->start_date = JobsHelperDateTime::isoDateToUTC($job->start_date); //store all in UTC
                $job->end_date = JobsHelperDateTime::isoDateToUTC($job->end_date); //store all in UTC
                $job->modified = $job->start_date;

                $user = JFactory::getUser();
                $job->userid = $user->id;

                $this->attachment = null;
                if (isset($_FILES["attachment"])) {
                    if ($cfg->enable_attach && is_uploaded_file(@$_FILES["attachment"]['tmp_name']))
                        $this->attachment = $_FILES["attachment"];
                }

                $job->jobuniqueID = uniqid(); //microtime()


                if (!$cfg->jobpublish_enable)
                    $job->published = $cfg->jobpublish_val;
                if (!$cfg->employervisibility_enable)
                    $job->employer_visibility = $cfg->employervisibility_val;
            }
        }

        function deleteFiles($job)
        {
            $my = JFactory::getUser();
            $database = JFactory::getDBO();
            $app = JFactory::getApplication();

            $delete_atachment = $app->input->get('delete_atachment', null);
            $id = (int)$job->id;

            if ($delete_atachment) {
                $query = "UPDATE #__jobsfactory_jobs SET atachment = '' WHERE id = $id";
                $database->setQuery($query);
                $database->execute();

                @unlink(JOB_UPLOAD_FOLDER . "{$id}.attach");
            }
        }

        function uploadAtachment($job)
        {
            if (isset($this->attachment)) {

                $file_name = "{$job->id}.attach";
                $path = JOB_UPLOAD_FOLDER . "$file_name";

                if (move_uploaded_file($this->attachment['tmp_name'], $path)) {
                    $job->file_name = $this->attachment["name"];
                    $job->store();
                }
            }
        }

        function validateSave($item)
        {

            $cfg = JTheFactoryHelper::getConfig();
            $app = JFactory::getApplication();
            $error = array();

            // Title validation
            if ($item->title == "" || !isset($item->title))
                $error[] = JText::_("COM_JOBS_TITLE_CAN_NOT_BE_EMPTY");

            // Dates validations
            if (!$item->id) {

                $start_date = $item->start_date;
                $end_date = $item->end_date;

                if (!$start_date) {
                    $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID");
                }
                if (!$end_date) {
                    $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID");
                }

                $year = date("Y", strtotime($start_date));
                $month = date("m", strtotime($start_date));
                $day = date("d", strtotime($start_date));
                $year_end = date("Y", strtotime($end_date));
                $month_end = date("m", strtotime($end_date));
                $day_end = date("d", strtotime($end_date));

                if ($year < 1900 && $year > 2200) {
                    $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID") . "<br />";
                }
                if ($month < 1 && $month > 12) {
                    $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID") . "<br />";
                }
                if ($day < 1 && $day > 31) {
                    $error[] = JText::_("COM_JOBS_ERR_START_DATE_VALID") . "<br />";
                }

                if ($year_end < 1900 && $year_end > 2200) {
                    $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID") . "<br />";
                }
                if ($month_end < 1 && $month_end > 12) {
                    $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID") . "<br />";
                }
                if ($day_end < 1 && $day_end > 31) {
                    $error[] = JText::_("COM_JOBS_ERR_END_DATE_VALID") . "<br />";
                }

                $datedif = mktime(0, 0, 0, $month_end, $day_end, $year_end) - mktime(0, 0, 0, $month, $day, $year);
                if ($datedif < 0) {
                    $error[] = JText::_("COM_JOBS_ERR_END_BIGGER_THAN_START") . "<br />";
                }

                $datedif = mktime(0, 0, 0, $month_end, $day_end, $year_end) - mktime(0, 0, 0, $month, $day, $year);
                if ($cfg->availability > 0)
                    if (floor($datedif / 60 / 60 / 24) >= $cfg->availability * 31) {
                        $error[] = JText::_("COM_JOBS_ERROR_MAX_AVAILABILITY"). ' ' . $cfg->availability . JText::_("COM_JOBS_MONTHS"). "<br/>";
                    }
            }

            //ATTACHEMENT validations
            if ($cfg->enable_attach) {
                if ($this->attachment) {
                    if (is_array($this->attachment)) {
                        $ext = strtolower(JFile::getExt($this->attachment['name']));
                    } else {
                        $ext = strtolower(JFile::getExt($this->attachment));
                    }
                    if ($cfg->attach_extensions) {
                        $allowed = explode(",", $cfg->attach_extensions);
                        if (!in_array($ext, $allowed)) {
                            $error [] = JText::_('COM_JOBS_ATTACHED_FILE_EXTENSION_NOT_ALLOWED_USE') . $cfg->attach_extensions;
                            unset($this->attachment);
                        }
                    }
                    if ($cfg->attach_max_size) {
                        if (filesize($this->attachment['tmp_name']) > 1024 * $cfg->attach_max_size) {
                            $error [] = JText::_('COM_JOBS_ATTACHED_FILE_IS_TOO_LARGE_MAXIMUM_ALLOWED') . $cfg->attach_max_size . ' kB';
                            unset($this->attachment);
                        }
                    }
                } elseif ($cfg->attach_compulsory)
                    $error[] = JText::_("COM_JOBS_ERR_ATTACHMENT_COMPULSORY") . "<br />";
            }

            //Custom Validations
            if (!$item->check())
                $error = array_merge($error, $item->_validation_errors);


            return $error;
        }

        function getNrFieldsWithFilters()
        {
            $database = JFactory::getDbo();
            $database->setQuery("SELECT count(*) FROM #__jobsfactory_fields WHERE page='jobs' AND categoryfilter=1");
            return $database->loadResult();
        }


        function getRequestedExperiences(&$job)
        {
            $db = JFactory::getDbo();
            $db->setQuery("SELECT experience_request FROM `#__jobsfactory_jobs` WHERE `id`='{$job->id}'");

            return $db->loadResult();
        }

        function setRequestedExperiences(&$job, $requested_experience)
        {
            if (!count($requested_experience)) return; //no experiences assigned
            // add category assignemtns
            $inserts = array();
            foreach ($requested_experience as $expid) {
                $inserts[] = "{$expid}";
            }

            if (count($inserts)) {
                return implode(",", $inserts);
            } else {
                return '(1)';
            }
        }


    }
