<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

    class JobsAdminModelLocation extends JModelLegacy
    {
        var $location_table = '';
        var $_location_tree = null;
        var $pagination = null;

        function __construct()
        {
            parent::__construct();
            $myApp = JTheFactoryApplication::getInstance();

            $this->location_table = $myApp->getIniValue('table', 'locations');
        }

        function getLocationCount($parent = -1, $include_disabled = false)
        {
            $w = array();

            if ($parent > 0)
                $w[] = "`locname`=$parent";
            if (!$include_disabled)
                $w[] = "`status`=1";
            $where = count($w) ? (" WHERE " . implode(' AND ', $w)) : "";

            $db = JFactory::getDbo();
            $q = "SELECT count(*) FROM `{$this->location_table}` c $where";

            $db->setQuery($q);
            return $db->loadResult();
        }

        function getFirstLocation($parentid = 0)
        {
            $db = JFactory::getDbo();
            $db->setQuery("SELECT * FROM `{$this->location_table}` c where status=1 and location='{$parentid}' order by `ordering`", 0, 1);
            return $db->loadObject();
        }

        function getTotal($default_order,$where){
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            // Get the total number of records
            $condition = ($default_order == 0) ? $where : '';
            $query = "SELECT COUNT(*) FROM `{$this->location_table}` c {$condition} ";
            $db->setQuery($query);

            return $db->loadResult();
        }

        function getLocationTree($parent_location = 0, $include_disabled = false,$default_order=1,$reset=null,$filter_country=null,$filter_approved=null,$search=null)
        {
            if (isset($this->_location_tree[$include_disabled])) // already got
                return $this->_location_tree[$include_disabled];

            $db = JFactory::getDbo();
            jimport('joomla.html.pagination');

            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');

            $app = JFactory::getApplication();
            $context = 'com_jobsfactory.location.locations.';
            $where = array();

            $limit = $app->getUserStateFromRequest($context."limit", 'limit', $app->getCfg('list_limit'), 'int');
            $limitstart = $app->getUserStateFromRequest($context . 'limitstart', 'limitstart', 0, 'int');

            // In case limit has been changed, adjust limitstart accordingly
            $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

            $filter_order = $app->getUserStateFromRequest($context . 'filter_order', 'filter_order', '', 'cmd');
            $filter_order_Dir = $app->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', '', 'word');

            if (!$filter_order) {
                $filter_order = 'c.locname';
            }
            $order = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir . '';

            if ($search)
                $where[] = " c.locname LIKE '%$search%' ";

            if ($filter_country) {
                $countryTable = JTable::getInstance('Country', 'Table');
                $ids_filter_country = $countryTable->getFilterCountryId($filter_country);

                if (is_array($ids_filter_country)) {

                    $ids = array();
                    foreach ($ids_filter_country as $countryid) {
                        $ids[] = "{$countryid['id']}";
                    }

                    if (count($ids)) {
                        $id_filter_country_cond = implode(",", $ids);
                    } else {
                        $id_filter_country_cond = '';
                    }

                    if ($id_filter_country_cond != '')
                     $where[] = " c.country IN ($id_filter_country_cond) ";
                }
            }
            $where[] = " c.status= '1'";

            // Build the where clause of the content record query
            $where = (count($where) ? ' WHERE ' . implode(' AND ', $where) : '');

            $this->pagination = new JPagination($this->getTotal($default_order,$where), $limitstart, $limit);

            $tree = array();

            if ($default_order == 0) {
                $q = "SELECT c.*, cy.name as countryname FROM `{$this->location_table}` c
                        LEFT JOIN `#__jobsfactory_country` cy ON cy.id = c.country
                        {$where} {$order} "; //ORDER BY `ordering` ASC
                $db->setQuery($q,$limitstart,$limit);
            } else {
                $q = "SELECT * FROM `{$this->location_table}` c ORDER BY `ordering` ASC ";
                $db->setQuery($q);
                //$db->setQuery($q,$limitstart,$limit);
            }

            $current_location = $db->loadObjectList();
            $tree[] = $current_location;

            $this->_location_tree[$include_disabled] = $current_location;

            return $this->_location_tree[$include_disabled];
        }

        function getLocationTreeRecursive($parent_location = 0, $include_disabled = false)
        {
            static $depth = 1;
            $db = JFactory::getDbo();

            $count_qry = "select count(*) from {$this->location_table} c " .
                (($include_disabled) ? "" : " and status=1");

            if (!$include_disabled)
                $where[] = "status=1";

            $q = "SELECT *,($count_qry) as nr_children
                FROM {$this->location_table} c
                 ORDER BY c.`id` ";
            $db->setQuery($q);
            $sublocations = $db->loadObjectList();

            return $sublocations;
        }

        function getLocationsNested($parentid = 0, $enabled_only = true)
        {
            //build whole tree
            $counter_current = 0;
            $location_array = $this->recurseLocationsNested(0, $enabled_only, $counter_current);
            if ($parentid > 0 && count($location_array)) {
                $node = null;
                $sub_tree = array();
                for ($i = 0; $i < count($location_array); $i++)
                    if ($location_array[$i]->id == $parentid) {
                        $node = $location_array[$i];
                        break;
                    }
                if (!$node) return $sub_tree; //node not found
                for ($i = 0; $i < count($location_array); $i++)
                    if ($location_array[$i]->left >= $node->left && $location_array[$i]->right <= $node->right)
                        $sub_tree[] = $location_array[$i];
                return $sub_tree;
            }
            return $location_array;
        }

        function recurseLocationsNested($parentid, $enabled_only, &$counter)
        {
            $location_array = array();
            $filter = '';
            $db = JFactory::getDbo();

            if ($enabled_only)
                $filter = "`status`=1 and ";
            $db->setQuery("SELECT c.* FROM {$this->location_table} c WHERE {$filter} `location`='" . $parentid . "' order by `ordering`");
            $locations = $db->loadObjectList();
            foreach ($locations as $location) {
                $counter++;
                $location->left = $counter;
                $c = $this->recurseLocationsNested($location->id, $enabled_only, $counter);
                $counter++;
                $location->right = $counter;

                $location_array[] = $location;

                $location_array = array_merge($c, $location_array);
            }

            return $location_array;
        }

        function getMaxOrdering()
        {
            $db = JFactory::getDbo();
            $db->setQuery("select max(`ordering`) FROM {$this->location_table}");
            return $db->loadResult();
        }

        function getLocationPathArray($locationid)
        {
            $db = JFactory::getDbo();
            $path_array = array();

            //while ($locationid) {
            if (isset($locationid)) {
                $db->setQuery("SELECT * FROM {$this->location_table} WHERE id='$locationid'");
                $location = $db->loadObject();

                $path_array[] = $location;
                //$locationid = $location->country;
            }

            return $path_array;
        }

        function getLocationPathString($locationid)
        {
            $location_array = $this->getLocationPathArray($locationid);
            $locationstring = "";

            foreach ($location_array as $location)
                if (isset($location)) {
                    $locationstring .= (($locationstring) ? "/" : "") . $location->locname;
                }

            return $locationstring;
        }

        function getLocationName($locationid) {
            $db = JFactory::getDbo();
            $location = '';

            $db->setQuery("SELECT locname FROM {$this->location_table} WHERE id='$locationid'");
            $location = $db->loadResult();

            return $location;
        }

        function delLocation($cids)
        {
            if (!is_array($cids) && $cids) $cids = array($cids);
            if (!count($cids)) return;

            $db = JFactory::getDbo();
            $db->setQuery("DELETE FROM `{$this->location_table}` WHERE id IN (" . implode(',', $cids) . ")");
            $db->execute();
            $nr = $db->getAffectedRows();

            $db->setQuery("UPDATE `#__jobsfactory_cities` SET location=0 WHERE location IN (" . implode(',', $cids) . ")");
            $db->execute();

            return $nr;
        }

        function quickAddFromText($quicktext,$parentid)
        {
            if ('WIN' == substr(PHP_OS, 0, 3)) {
                $separator = "\r\n";
            } else {
                $separator = "\n";
            }

            $textlocations = explode($separator, $quicktext);
            $stack = array(0);

            $i = 0;
            $last_id = null;

            $prevcat = '';
            $prevlocation_spaces = 0;
            $ordering = $this->getMaxOrdering();
            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');
            $locationtable = JTable::getInstance('Location', 'Table');

            $lang = JFactory::getLanguage();

            foreach ($textlocations as $key => $location) {
                if (!trim($location)) continue; //skip empty locations
                $ordering++;
                $location_spaces = 0;
                if (preg_match('/^[ ]*/', $location, $matches))
                    $location_spaces = strlen($matches[0]);

                if ($location_spaces > $prevlocation_spaces)
                    array_push($stack, $last_id);
                if ($location_spaces < $prevlocation_spaces) {
                    $diff_level = $prevlocation_spaces - $location_spaces;
                    for ($j = 0; $j < $diff_level; $j++) {
                        array_pop($stack);
                    }
                }
                $location = trim($location);
                $locationtable->id = null;
                $locationtable->locname = $location;
                $locationtable->country = $parentid;
                $locationtable->ordering = $ordering;
                $locationtable->hash = md5(strtolower(JTheFactoryHelper::str_clean($lang->transliterate($location))));
                $locationtable->store();

                $prevcat = $location;
                $prevcat_spaces = $location_spaces;
                $last_id = $locationtable->id;

                $locationpath = $this->getLocationPathString($locationtable->id);
                $locationpath = JFilterOutput::stringURLSafe($lang->transliterate($locationpath));
            }

        }

        function getLocationChildren($rootcat = 0)
        {
            //TODO:return lista cu idurile copiilor
            $db = JFactory::getDbo();
            $db->setQuery("SELECT id FROM `#__jobsfactory_cities` WHERE location = '{$rootcat}'");

            $res = $db->loadColumn();
            return $res;
        }

    }
