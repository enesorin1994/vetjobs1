<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

    class JobsAdminModelCity extends JModelLegacy
    {
        var $city_table = '';
        var $_city_tree = null;
        var $pagination = null;

        function __construct()
        {
            parent::__construct();
            $myApp = JTheFactoryApplication::getInstance();

            $this->city_table = $myApp->getIniValue('table', 'cities');
        }

        function getCityCount($parent = -1, $include_disabled = false)
        {
            $w = array();

            if ($parent >= 0)
                $w[] = "`cityname`=$parent";
            if (!$include_disabled)
                $w[] = "`status`=1";

            $where = count($w) ? (" WHERE " . implode(' AND ', $w)) : "";

            $db = JFactory::getDbo();
            $q = "SELECT count(*) FROM `{$this->city_table}` c $where";
            $db->setQuery($q);
            return $db->loadResult();
        }


        function getFirstCity($parentid = 0)
        {
            $db = JFactory::getDbo();
            $db->setQuery("SELECT * FROM `{$this->city_table}` c where status=1 and location='{$parentid}' order by `ordering`", 0, 1);
            return $db->loadObject();
        }

        function getCityName($cityid) {
            $db = JFactory::getDbo();
            $city = '';

            $db->setQuery("SELECT cityname FROM {$this->city_table} WHERE id='$cityid'");
            $city = $db->loadResult();

            return $city;
        }

        function getTotal(){
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            // Get the total number of records
            $query = "SELECT COUNT(*) FROM `{$this->city_table}` ";
            $db->setQuery($query);

            return $db->loadResult();
        }

        function getCityTree($parent_city = 0, $include_disabled = false,$reset=null,$filter_location=null,$filter_approved=null,$search=null)
        {
            if (isset($this->_city_tree[$include_disabled])) // already got
                return $this->_city_tree[$include_disabled];

            $db = JFactory::getDbo();

            jimport('joomla.html.pagination');
            $app = JFactory::getApplication();
            $context = 'com_jobsfactory.city.cities.';
            $where = array();

            $limit = $app->getUserStateFromRequest($context."limit", 'limit', $app->getCfg('list_limit'), 'int');
            $limitstart = $app->getUserStateFromRequest($context . 'limitstart', 'limitstart', 0, 'int');

            // In case limit has been changed, adjust limitstart accordingly
            $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

            $filter_order = $app->getUserStateFromRequest($context . 'filter_order', 'filter_order', '', 'cmd');
            $filter_order_Dir = $app->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', '', 'word');

            if (!$filter_order) {
                $filter_order = 'c.cityname';
            }
            $order = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir . '';

            if ($search)
                $where[] = " c.cityname LIKE '%$search%' ";

            if ($filter_location) {
                $where[] = " l.locname like '%$filter_location%' ";
            }

            if (!$include_disabled)
                $where[] = " c.status= '1'";

            $cfg    = JTheFactoryHelper::getConfig();
            /*if ($cfg->admin_approval && $filter_approved !== '') {
                $where[] = " a.approved= '$filter_approved'";
            }*/

            $this->pagination = new JPagination($this->getTotal(), $limitstart, $limit);

            $tree = array();
            //$where = '';
            if ($parent_city) {
                $where[] =  ' location ='."$parent_city".' ';
            }
             // Build the where clause of the content record query
            $where = (count($where) ? ' WHERE ' . implode(' AND ', $where) : '');

            $q = "SELECT c.*, l.locname FROM `{$this->city_table}` c
                    LEFT JOIN `#__jobsfactory_locations` l ON l.id = c.location" .
                    " $where  $order ";
                    //ORDER BY `location` ASC, `cityname` ASC

            $db->setQuery($q,$limitstart,$limit);
            $current_city = $db->loadObjectList();
            $tree[] = $current_city;
            $this->_city_tree[$include_disabled] = $current_city;

            return $this->_city_tree[$include_disabled];
        }

        function getCityTreeRecursive($parent_city = 0, $include_disabled = false)
        {
            static $depth = 1;
            $db = JFactory::getDbo();

            $count_qry = "select count(*) from {$this->city_table} c " .
                (($include_disabled) ? "" : " and status=1");

            if (!$include_disabled)
                $where[] = "status=1";

            $q = "SELECT *,($count_qry) as nr_children
                FROM {$this->city_table} c
                 ORDER BY c.`id` ";

            $db->setQuery($q);
            $subcities = $db->loadObjectList();

            return $subcities;
        }

        function getCitiesNested($parentid = 0, $enabled_only = true)
        {
            //build whole tree
            $counter_current = 0;
            $city_array = $this->recurseCitiesNested(0, $enabled_only, $counter_current);
            if ($parentid > 0 && count($city_array)) {
                $node = null;
                $sub_tree = array();
                for ($i = 0; $i < count($city_array); $i++)
                    if ($city_array[$i]->id == $parentid) {
                        $node = $city_array[$i];
                        break;
                    }
                if (!$node) return $sub_tree; //node not found
                for ($i = 0; $i < count($city_array); $i++)
                    if ($city_array[$i]->left >= $node->left && $city_array[$i]->right <= $node->right)
                        $sub_tree[] = $city_array[$i];
                return $sub_tree;
            }
            return $city_array;
        }

        function recurseCitiesNested($parentid, $enabled_only, &$counter)
        {
            $city_array = array();
            $filter = '';
            $db = JFactory::getDbo();

            if ($enabled_only)
                $filter = "`status`=1 and ";
            $db->setQuery("SELECT c.* FROM {$this->city_table} c WHERE {$filter} `parent`='" . $parentid . "' order by `ordering`");
            $cities = $db->loadObjectList();
            foreach ($cities as $city) {
                $counter++;
                $city->left = $counter;
                $c = $this->recurseCitiesNested($city->id, $enabled_only, $counter);
                $counter++;
                $city->right = $counter;

                $city_array[] = $city;

                $city_array = array_merge($c, $city_array);
            }

            return $city_array;
        }

        function getMaxOrdering()
        {
            $db = JFactory::getDbo();
            $db->setQuery("select max(`ordering`) FROM {$this->city_table}");
            return $db->loadResult();
        }

        function getCityPathArray($cityid)
        {
            $db = JFactory::getDbo();
            $path_array = array();

            //while ($cityid) {
            if (isset($cityid)) {
                $db->setQuery("select * FROM {$this->city_table} where id='$cityid'");
                $city = $db->loadObject();
                $path_array[] = $city;

                //$cityid = $city->location;
            }

            return $path_array;
        }

        function getCityPathString($cityid)
        {
            $city_array = $this->getCityPathArray($cityid);
            $citiestring = "";

            foreach ($city_array as $city) {
                if (isset($city)) {
                    $citiestring .= (($citiestring) ? "/" : "") . $city->cityname;
                }
            }
            return $citiestring;

        }

        function delCity($cids)
        {
            if (!is_array($cids) && $cids) $cids = array($cids);
            if (!count($cids)) return;

            $db = JFactory::getDbo();
            $db->setQuery("delete from `{$this->city_table}` where id in (" . implode(',', $cids) . ")");
            $db->execute();
            $nr = $db->getAffectedRows();

            return $nr;
        }

        function quickAddFromText($quicktext,$parentid)
        {

            if ('WIN' == substr(PHP_OS, 0, 3)) {
                $separator = "\r\n";
            } else {
                $separator = "\n";
            }

            $textcities = explode($separator, $quicktext);

            $stack = array(0);

            $i = 0;
            $last_id = null;

            $prevcat = '';
            $prevcity_spaces = 0;
            $ordering = $this->getMaxOrdering();

            JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jobsfactory/tables');
            $citytable = JTable::getInstance('City', 'Table');

            $lang = JFactory::getLanguage();

            foreach ($textcities as $key => $city) {
                if (!trim($city)) continue; //skip empty cities
                $ordering++;
                $city_spaces = 0;
                if (preg_match('/^[ ]*/', $city, $matches))
                    $city_spaces = strlen($matches[0]);

                if ($city_spaces > $prevcity_spaces)
                    array_push($stack, $last_id);

                if ($city_spaces < $prevcity_spaces) {
                    $diff_level = $prevcity_spaces - $city_spaces;

                    for ($j = 0; $j < $diff_level; $j++) {
                        array_pop($stack);
                    }
                }

                $city = trim($city);
                $citytable->id = null;
                $citytable->cityname = $city;
                $citytable->location = $parentid;
                $citytable->ordering = $ordering;
                $citytable->hash = md5(strtolower(JTheFactoryHelper::str_clean($lang->transliterate($city))));
                $citytable->store();

                $prevcat = $city;
                $prevcat_spaces = $city_spaces;
                $last_id = $citytable->id;

                $citypath = $this->getCityPathString($citytable->id);
                $citypath = JFilterOutput::stringURLSafe($lang->transliterate($citypath));

            }
        }

        function getCityChildren($rootcat = 0)
        {
            //TODO:return lista cu idurile copiilor
            $db = JFactory::getDbo();
            $db->setQuery("SELECT id FROM `{$this->city_table}` WHERE parent = '{$rootcat}'");
            $res = $db->loadColumn();

            $subcat = array();

            for ($i = 0; $i < count($res); $i++)
                $subcat = array_merge($subcat, $this->getCityChildren($res[$i]));

            return array_merge($res, $subcat);
        }

    }
