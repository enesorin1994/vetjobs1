<?php
    /**------------------------------------------------------------------------
    com_jobsfactory - Jobs Factory 1.6.5
    ------------------------------------------------------------------------
     * @author thePHPfactory
     * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
     * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     * Websites: http://www.thePHPfactory.com
     * Technical Support: Forum - http://www.thePHPfactory.com/forum/
     * @package: Jobs
    -------------------------------------------------------------------------*/

	// Access the file from Joomla environment
	defined('_JEXEC') or die('Restricted access');


	/**
	 * Class JTheFactoryEventAddHelpingButton
	 */
	class JTheFactoryEventAddHelpingButton extends JTheFactoryEvents
	{

		/**
		 * onAfterExecuteTask
		 *
		 * @param $controller
		 */
		public function onAfterExecuteTask($controller)
		{
			$app = JTheFactoryApplication::getInstance();

			$baseHelpingPath = 'http://wiki.thefactory.ro/doku.php?id=joomla30:' . 'jobsfactory' . ':';

			$task        = JApplicationCms::getInstance()->input->getCmd('task', 'jobs');
			$helpingPage = $baseHelpingPath . $task;
			$curl        = JTheFactoryHelper::remote_read_url($helpingPage);

			// In case to not exist a helping page for related task then
			// Add a helping button which open the wiki entry point for this component
			if (!(FALSE === strpos($curl, 'This topic does not exist yet'))) {
				$helpingPage = $baseHelpingPath . 'jobsfactory';

			}
			// Add helping button for current task
			JToolbarHelper::help(NULL, FALSE, $helpingPage);

		}
	}
