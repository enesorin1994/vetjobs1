<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JTheFactoryEventSettings extends JTheFactoryEvents
{
    function onDisplaySettings($form,$groups,$data)
    {
        JHtml::_('behavior.framework');
        $doc= JFactory::getDocument();
        $doc->addScript(JURI::base() . 'components/'.APP_EXTENSION.'/js/jobsfactory_settings.js');
    }

    function onBeforeSaveSettings()
    {
    }
    function onAfterSaveSettings()
    {
    }
}

