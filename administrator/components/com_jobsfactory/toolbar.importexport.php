<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

switch ($task) {
    case 'importexport':
        JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '. JText::_('COM_JOBS_IMPORT__EXPORT_JOBS').' ]</small></small>' , 'categories.png' );
        JToolBarHelper::back();
    break;
    case 'showadmimportform':
    case 'importcsv':
        JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '. JText::_('COM_JOBS_IMPORT_FROM_CSV').' ]</small></small>' , 'categories.png' );
        JToolBarHelper::custom( "importexport.importcsv", 'upload', 'upload', JText::_('COM_JOBS_UPLOAD'), false );
        JToolBarHelper::back();
    break;
    case 'exportToXls':
        JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '. JText::_('COM_JOBS_EXPORT_JOBS').' ]</small></small>' , 'categories.png' );
        JToolBarHelper::custom( "importexport.importexport", 'back', 'back', JText::_('COM_JOBS_BACK'), false );
    break;

    case 'importexportloc':
        JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '. JText::_('COM_JOBS_IMPORT__EXPORT_LOCATIONS').' ]</small></small>' , 'categories.png' );
        JToolBarHelper::back();
    break;
    case 'showadmimportformloc':
    case 'importcsvloc':
        JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '. JText::_('COM_JOBS_IMPORT_LOCATIONS_FROM_CSV').' ]</small></small>' , 'categories.png' );
        JToolBarHelper::custom( "importexport.importcsvloc", 'upload', 'upload', JText::_('COM_JOBS_UPLOAD'), false );
        JToolBarHelper::back();
    break;

    case 'exportToXlsloc':
        JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '. JText::_('COM_JOBS_EXPORT_LOCATIONS').' ]</small></small>' , 'categories.png' );
        JToolBarHelper::custom( "importexport.importexportloc", 'back', 'back', JText::_('COM_JOBS_BACK'), false );
    break;
}
