<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

if (file_exists(JPATH_COMPONENT_ADMINISTRATOR.DS. 'helpers' .DS. 'submenu.php'))
        require JPATH_COMPONENT_ADMINISTRATOR.DS. 'helpers' .DS. 'submenu.php';

switch ($task) {
	case 'edit':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_('COM_JOBS_EDIT_JOB').' ]</small></small>' , 'categories.png' );
        JToolBarHelper::save('saveclose');
        JToolBarHelper::apply('save');
		JToolBarHelper::back('JTOOLBAR_BACK','index.php?option=com_jobsfactory&task=jobs');
	break;
	case 'view':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_('COM_JOBS_VIEW_JOB').' ]</small></small>' , 'categories.png' );
		JToolBarHelper::back();
	break;
	case 'write_admin_message':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_('COM_JOBS_WRITE_MESSAGE_TO_USER').' ]</small></small>' , 'categories.png' );
	break;
    case 'comments_administrator':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_('COM_JOBS_MESSAGE_ADMINISTRATOR').' ]</small></small>' , 'categories.png' );
        JToolBarHelper::custom( 'del_comment', 'delete', 'delete', JText::_('COM_JOBS_DELETE'), true );
        JToolBarHelper::custom( 'toggle_comment', 'cancel', 'cancel', JText::_('COM_JOBS_ENABLEDISABLE'), true );
    break;
    case '':
	case 'jobs':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>['.JText::_('COM_JOBS_JOBLIST').']</small></small>' , 'categories.png' );
		JToolBarHelper::custom( 'unblock', 'apply', 'apply', JText::_('COM_JOBS_UNBLOCK'), true );
		JToolBarHelper::custom( 'block', 'cancel', 'cancel', JText::_('COM_JOBS_BLOCK'), true );
		JToolBarHelper::custom( 'remove', 'delete', 'cancel', JText::_('COM_JOBS_DELETE_JOBS'), true );
	break;
	case 'users':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>['.JText::_('COM_JOBS_MANAGE_USERS').']</small></small>' , 'categories.png' );
		JToolBarHelper::custom('unblockuser','apply','apply',JText::_('COM_JOBS_UNBLOCK'),false);
		JToolBarHelper::custom('blockuser','cancel','cancel',JText::_('COM_JOBS_BLOCK'),false);
		JToolBarHelper::divider();
		JToolBarHelper::custom( 'setverify', 'apply', 'apply', JText::_('COM_JOBS_SET_AS_VERIFY'), true );
		JToolBarHelper::custom( 'unsetverify', 'apply', 'apply', JText::_('COM_JOBS_UNSET_VERIFY'), true );
		JToolBarHelper::divider();
		JToolBarHelper::custom( 'setpowerseller', 'apply', 'apply', JText::_('COM_JOBS_SET_AS_POWERSELLER'), true );
		JToolBarHelper::custom( 'unsetpowerseller', 'cancel', 'cancel', JText::_('COM_JOBS_UNSET_POWERSELLER'), true );
        //JToolBarHelper::deleteList(JText::_('COM_JOBS_ARE_YOU_SURE_YOU_WANT_TO_DELETE_THESE_USERS'), 'removeuser');
		break;
	case 'detailUser':
        JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>['.JText::_('COM_JOBS_MANAGE_USERS').']</small></small>' , 'categories.png' );
        JToolBarHelper::custom('users','back','back',JText::_('COM_JOBS_BACK'),false);
	    break;
    case 'paymentmanager':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_('COM_JOBS_PAYMENT_MANAGEMENT').' ]</small></small>' , 'categories.png' );
        break;    
	case 'reported_jobs':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_( 'COM_JOBS_REPORTED_JOBS' ).' ]</small></small>' , 'categories.png' );
		JToolBarHelper::custom( 'solved', 'save', 'save', JText::_('COM_JOBS_MARK_AS_SOLVED'));
		JToolBarHelper::custom( 'unsolved', 'cancel', 'cancel', JText::_('COM_JOBS_MARK_AS_UNSOLVED'));
		break;
    case 'settingsmanager':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_( 'COM_JOBS_EXTENSION_CONTROL_PANEL' ).' ]</small></small>' , 'categories.png' );
	break;
    case 'integration':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_( 'COM_JOBS_CONFIGURE_USER_PROFILE' ).' ]</small></small>' , 'categories.png' );
        JToolBarHelper::apply('changeprofileintegration');
        JHtmlSidebar::addEntry(JText::_('COM_JOBS_SETTINGS'),'index.php?option=com_jobsfactory&task=settingsmanager',false);
	break;
    case'dashboard':
        JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_( 'COM_JOBS_DASHBOARD' ).' ]</small></small>' , 'categories.png' );
        JToolBarHelper::custom( 'about.main', 'help', 'help', JText::_('COM_JOBS_ABOUT_COMPONENT'), false );
    break;
    case 'cronjob_info':
		JToolBarHelper::title(  JText::_( 'COM_JOBS_JOBSFACTORY' ) . ': <small><small>[ '.JText::_( 'COM_JOBS_CRON_INFO' ).' ]</small></small>' , 'categories.png' );
		JToolBarHelper::custom( 'settingsmanager', 'back', 'back', JText::_('COM_JOBS_BACK'), false );
        JHtmlSidebar::addEntry(JText::_("COM_JOBS_SETTINGS"),'index.php?option='.APP_EXTENSION.'&task=settingsmanager',($task=="settingsmanager")?1:0);
        JHtmlSidebar::addEntry(JText::_("COM_JOBS_GENERAL_SETTINGS"),'index.php?option='.APP_EXTENSION.'&task=config.display',($task=="config.display")?1:0);
    break;
    case 'listjobtype':
        JToolBarHelper::title( JText::_( 'COM_JOBS_JOBTYPE_MANAGEMENT' ));
        JToolBarHelper::custom( 'jobtype.newjobtype', 'new.png', 'new_f2.png', JText::_('New Job type'), false );
        JToolBarHelper::custom( 'jobtype.editjobtype', 'edit.png', 'edit_f2.png', JText::_('Edit Job type'), true );
        JToolBarHelper::deleteList("","jobtype.deletejobtype");
        JobsFactorySubmenu::subMenuListJobtype('listjobtype');
        break;
    case 'newjobtype':
    case 'editjobtype':
        JToolBarHelper::title( JText::_( 'COM_JOBS_JOBTYPE_MANAGEMENT' ));
        JToolBarHelper::save( "jobtype.savejobtype" );
        JToolBarHelper::save2new( "jobtype.savejobtype2new" );
        JToolBarHelper::apply( "jobtype.applyjobtype" );
        JToolBarHelper::back();
        JobsFactorySubmenu::subMenuEditJobtype('listjobtype');
    break;
    case 'cities':
        JToolBarHelper::title( JText::_( 'COM_JOBS_CITY_MANAGEMENT' ));
        JToolBarHelper::custom( 'city.newcity', 'new.png', 'new_f2.png', JText::_('FACTORY_NEW_CITY'), false );
        JToolBarHelper::custom( 'city.editcity', 'edit.png', 'edit_f2.png', JText::_('FACTORY_EDIT_CITIES'), true );
        JToolBarHelper::custom( 'city.showmovecities', 'move.png', 'move_f2.png', JText::_('FACTORY_ASSIGN_LOCATION'), true );
        JToolBarHelper::deleteList("","city.delcities");
        JobsFactorySubmenu::subMenuSettingsManager('settingsmanager');
    break;
    case 'newcity':
    case 'editcity':
        JToolBarHelper::title( JText::_( 'COM_JOBS_CITY_MANAGEMENT' ));
        JToolBarHelper::save( "city.savecity" );
        JToolBarHelper::back();
        JobsFactorySubmenu::subMenuSettingsManager('settingsmanager');
    break;
    case 'showmovecities':
        JToolBarHelper::title( JText::_( 'COM_JOBS_CITY_MANAGEMENT' ));
        JToolBarHelper::custom( 'city.doMoveCities', 'save.png', 'save_f2.png', JText::_('FACTORY_ASSIGN_TO_SELECTED_COUNTRY_CITY'), false );
        JobsFactorySubmenu::subMenuSettingsManager('settingsmanager');
    break;
    case 'locations':
        JToolBarHelper::title( JText::_( 'FACTORY_LOCATION_MANAGEMENT' ));
        JToolBarHelper::custom( 'location.newlocation', 'new.png', 'new_f2.png', JText::_('FACTORY_NEW_LOCATION'), false );
        JToolBarHelper::custom( 'location.editlocation', 'edit.png', 'edit_f2.png', JText::_('FACTORY_EDIT_LOCATIONS'), true );
        JToolBarHelper::custom( 'location.showmovelocations', 'move.png', 'move_f2.png', JText::_('FACTORY_ASSIGN_COUNTRY'), true );
        JToolBarHelper::deleteList("","location.dellocations");
        JobsFactorySubmenu::subMenuSettingsManager('settingsmanager');
    break;
    case 'newlocation':
    case 'editlocation':
        JToolBarHelper::title( JText::_( 'FACTORY_LOCATION_MANAGEMENT' ));
        JToolBarHelper::save( "location.savelocation" );
        JToolBarHelper::back();
        JobsFactorySubmenu::subMenuSettingsManager('settingsmanager');
    break;
    case 'showmovelocations':
        JToolBarHelper::title( JText::_( 'FACTORY_LOCATION_MANAGEMENT' ));
        JToolBarHelper::custom( 'location.doMoveLocations', 'save.png', 'save_f2.png', JText::_('FACTORY_ASSIGN_TO_SELECTED_COUNTRY_LOCATION'), false );
        JobsFactorySubmenu::subMenuSettingsManager('settingsmanager');
    break;
    case 'listexperiencelevel':
        JToolBarHelper::title( JText::_( 'COM_JOBS_EXPERIENCE_MANAGEMENT' ));
        JToolBarHelper::custom( 'experiencelevel.newexperiencelevel', 'new.png', 'new_f2.png', JText::_('New Experience level'), false );
        JToolBarHelper::custom( 'experiencelevel.editexperiencelevel', 'edit.png', 'edit_f2.png', JText::_('Edit Experience level'), true );
        JToolBarHelper::deleteList("","experiencelevel.deleteexperiencelevel");
        JobsFactorySubmenu::subMenuListExperiencelevel('listexperiencelevel');
    break;
    case 'newexperiencelevel':
    case 'editexperiencelevel':
        JToolBarHelper::title( JText::_( 'COM_JOBS_EXPERIENCE_MANAGEMENT' ));
        JToolBarHelper::save( "experiencelevel.saveexperiencelevel" );
        JToolBarHelper::save2new( "experiencelevel.saveexperiencelevel2new" );
        JToolBarHelper::apply( "experiencelevel.applyexperiencelevel" );
        JToolBarHelper::back();
        JobsFactorySubmenu::subMenuEditExperiencelevel('listexperiencelevel');
    break;
    case 'liststudieslevel':
        JToolBarHelper::title( JText::_( 'COM_JOBS_STUDIES_LEVELS' ));
        JToolBarHelper::custom( 'studieslevel.newstudieslevel', 'new.png', 'new_f2.png', JText::_('COM_JOBS_NEW_STUDY_LEVEL'), false );
        JToolBarHelper::custom( 'studieslevel.editstudieslevel', 'edit.png', 'edit_f2.png', JText::_('COM_JOBS_EDIT_STUDY_LEVEL'), true );
        JToolBarHelper::deleteList("","studieslevel.deletestudieslevel");
        JobsFactorySubmenu::subMenuListStudieslevel('liststudieslevel');
    break;
    case 'newstudieslevel':
    case 'editstudieslevel':
        JToolBarHelper::title( JText::_( 'COM_JOBS_STUDIES_LEVELS' ));
        JToolBarHelper::save( "studieslevel.savestudieslevel" );
        JToolBarHelper::save2new( "studieslevel.savestudieslevel2new" );
        JToolBarHelper::apply( "studieslevel.applystudieslevel" );
        JToolBarHelper::back();
        JobsFactorySubmenu::subMenuEditStudieslevel('liststudieslevel');
    break;

}
