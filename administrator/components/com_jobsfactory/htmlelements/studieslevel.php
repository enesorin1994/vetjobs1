<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

abstract class JHTMLStudieslevel
{
    static function selectlist($name = 'study_level', $attributes = '', $defaultvalue = null)
    {

        $db = JFactory::getDbo();
        $db->setQuery("select id as `value`,`levelname` as text from `#__" . APP_PREFIX . "_studieslevel` where `published`=1 order by `ordering`,`levelname`");
        $studieslevel = $db->loadObjectList();

        if (!$defaultvalue) {
            $db->setQuery("select `levelname` as value from `#__" . APP_PREFIX . "_studieslevel` where `id`=1");
            $defaultvalue = $db->loadResult();
        }

        $emptyopt[] = JHTML::_('select.option', '', JText::_("COM_JOBS__ANY_LEVEL"));
        $studieslevel = array_merge($emptyopt, $studieslevel);

        return JHtml::_('select.genericlist', $studieslevel, $name, $attributes, 'value', 'text', $defaultvalue);
    }

}
