<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JHTMLListJobs {

    function selectOrderDir($selected) {

        $opts = array();
        $opts[] = JHTML::_('select.option', 'DESC', JText::_('COM_JOBS_APPLICATION_ORDER_DESC'));
        $opts[] = JHTML::_('select.option', 'ASC', JText::_('COM_JOBS_APPLICATION_ORDER_ASC'));

        return JHTML::_('select.genericlist', $opts, 'filter_order_Dir', 'class="inputbox" onchange="document.jobForm.submit();"', 'value', 'text', $selected);
    }

    function inputKeyword($value) {

        return '<input type="text" id="search_box" name="keyword" size="30" value="'.$value.'" />';
    }

    static function inputsHiddenFilters($filters) {

        $html = '';

        foreach($filters as $name=>$filter) {
            if(empty($filter)) {
                continue;
            }

            if(is_array($filter)) {
                foreach($filter as $opt) {
                    $html .= '<input type="hidden" name="'.$name.'[]" value="'.$opt.'" />'.PHP_EOL;
                }
            } else {
                $html .= '<input type="hidden" name="'.$name.'" value="'.$filter.'" />'.PHP_EOL;
            }
        }

        return $html;
    }

    static function htmlLabelFilters($filters,$asArray=true) {

        $database = JFactory::getDBO();
        $cfg    = JTheFactoryHelper::getConfig();
        $job = JTable::getInstance('jobs', 'Table');

        $searchstrings = array();
        if ( $filters->get('keyword') ) {
            $searchstrings[JText::_('COM_JOBS_FILTER_KEYWORD')] = $filters->get('keyword');
        }

        if ($filters->get('userid')) {
            $u = JFactory::getUser($filters->get('userid'));
            if($u && !$u->block) {
                $searchstrings[JText::_('COM_JOBS_FILTER_USERS')] = $u->username;
            }
        }

        if ($filters->get('cat')) {
            $database->setQuery("select catname from #__jobsfactory_categories where id='" . $database->escape($filters->get('cat')) . "'");
            $catname = $database->loadResult();
            $searchstrings[JText::_('COM_JOBS_FILTER_CATEGORY')] = $catname;
        }

        if ($filters->get('location')) {
            $database->setQuery("select locname from #__jobsfactory_locations where id='" . $database->escape($filters->get('location')) . "'");
            $locname = $database->loadResult();
            $searchstrings[JText::_('COM_JOBS_FILTER_LOCATION')] = $locname;
        }

        if ($filters->get('locationcity')) {
            $database->setQuery("select cityname from #__jobsfactory_cities where id='" . $database->escape($filters->get('locationcity')) . "'");
            $locname = $database->loadResult();
            $searchstrings[JText::_('COM_JOBS_FILTER_CITY')] = $locname;
        }

        if ($filters->get('afterd')) {
            $searchstrings[JText::_('COM_JOBS_FILTER_START_DATE')] = $filters->get('afterd');
        }

        if ($filters->get('befored')) {
            $searchstrings[JText::_('COM_JOBS_FILTER_END_DATE')] = $filters->get('befored');
        }

        if ($filters->get('tag')) {
            $searchstrings[JText::_('COM_JOBS_FILTER_TAGS')] = $filters->get('tag');
        }

        if ($filters->get('inarch')) {
            $searchstrings[JText::_('COM_JOBS_FILTER_ARCHIVE')] = ($filters->get('inarch')==1) ? JText::_('COM_JOBS_YES') : JText::_('COM_JOBS_NO');
        }

        if ($filters->get('country')) {
            $database->setQuery("SELECT name from `#__jobsfactory_country` where id='" . $database->escape($filters->get('country')) . "'");
            $countryname = $database->loadResult();
            $searchstrings[JText::_('COM_JOBS_FILTER_COUNTRY')] = $countryname;
            //$searchstrings[JText::_('COM_JOBS_FILTER_COUNTRY')] = $filters->get('country');
        }

        if ($filters->get('city')) {
            $searchstrings[JText::_('COM_JOBS_FILTER_CITY')] = $filters->get('city');
        }

        if ($filters->get('job_cityid')) {
            $database->setQuery("select cityname from #__jobsfactory_cities where id='" . $database->escape($filters->get('job_cityid')) . "'");
            $cityname = $database->loadResult();
            $searchstrings[JText::_('COM_JOBS_FILTER_CITY')] = $cityname;
            //$searchstrings[JText::_('COM_JOBS_FILTER_CITY')] = $filters->get('job_cityid');
        }

        if ($filters->get('job_location_state')) {
            $database->setQuery("select locname from #__jobsfactory_locations where id='" . $database->escape($filters->get('job_location_state')) . "'");
            $locname = $database->loadResult();
            $searchstrings[JText::_('COM_JOBS_FILTER_LOCATION')] = $locname;
        }

        if ($filters->get('area')) {
            $searchstrings[JText::_('COM_JOBS_FILTER_AREA')] = $filters->get('area');
        }

        if ($filters->get('job_type')) {
            $jobType = $job->getJobType($filters->get('job_type'));
            $searchstrings[JText::_('COM_JOBS_FILTER_JOB_TYPE')] = $jobType;
        }

        if ($filters->get('studies_request')) {
            $jobStudiesRequested = $job->getStudiesRequested($filters->get('studies_request'));
            $searchstrings[JText::_('COM_JOBS_FILTER_STUDIES')] = $jobStudiesRequested;
        }

        if ($filters->get('experience_request')) {

            $id_experience = $filters->get('experience_request');
            $database->setQuery("SELECT levelname FROM #__jobsfactory_experiencelevel WHERE id='$id_experience' ");
            $experiece_name = $database->LoadResult();

            $searchstrings[JText::_('COM_JOBS_FILTER_EXPERIENCE')] = $experiece_name;
        }

        if ($filters->get('jinterval')) {
            $jinterval = $filters->get('jinterval');
            $searchstrings[JText::_('COM_JOBS_FILTER_DATE_POSTED')] = $jinterval .' ' . JText::_('COM_JOBS_DAYS');
        }

        //integration filter labels
        $profile = JobsHelperTools::getUserProfileObject();
        $integrationArray = $profile->getIntegrationArray();
        foreach($integrationArray as $alias=>$fieldName) {
            if($fieldName) {
                if($fValue = $filters->get('user_profile%'.$fieldName)) {
                    $searchstrings[JText::_( strtoupper('JOB_'.$alias) )] = $fValue;
                }
            }
        }

        //custom fields filter labels
        $searchableFields = CustomFieldsFactory::getSearchableFieldsList();
        foreach($searchableFields as $field) {
            $requestKey = $field->page.'%'.$field->db_name;
            if($filters->get($requestKey)) {
                $ftype = CustomFieldsFactory::getFieldType($field->ftype);
                $searchstrings[JText::_($field->name)] = $ftype->htmlSearchLabel($field, $filters->get($requestKey));;
            }
        }

        return $searchstrings;
    }

    static function DisplayFieldsHtml(&$row, $fieldlist, $style='div')
    {
       if (!count($fieldlist)) return null;
       $page = $fieldlist[0]->page;
       $cfg = CustomFieldsFactory::getConfig();

       $category_filter=array();
       if ($cfg['has_category'][$page])
       {
           $db = JFactory::getDbo();
           $db->setQuery("SELECT fid FROM #__".APP_PREFIX."_fields_categories WHERE cid = '".$row->cat."'");
           $category_filter=$db->loadColumn();
       }
       $flist = array();
       $field_object = JTable::getInstance('FieldsTable', 'JTheFactory');

       foreach ($fieldlist as $field)
       {
           if ($field->categoryfilter && !in_array($field->id, $category_filter)) continue;

           $field_type = CustomFieldsFactory::getFieldType($field->ftype);

           $field_object->bind($field);
           $f = new stdClass();
           $f->field = clone $field;
           $f->value = $row->{$field->db_name};
           $f->html = $field_type->getFieldHTML($field_object, $row->{$field->db_name});
           $flist[] = $f;
       }
       $func = 'DisplayFieldsHtml_'.ucfirst($style);
       $html = self::$func($flist);

       return $html;
    }

    static function DisplayFieldsHtml_Table($flist)
    {
        if (!count($flist)) return null;
        $html="<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
        foreach ($flist as $f)
        {
            $tooltip = "";
            $icon = JUri::root().'administrator/components/'.APP_EXTENSION.'/thefactory/assets/images/tooltip.png';
            if ($f->field->help) $tooltip=JHtml::_('tooltip', $f->field->help, null, $icon);
            $html.="
                <tr>
                    <td><label class='custom_field'>".JText::_($f->field->name).": </label> $tooltip</td>
                    <td>".$f->html."</td>
                </tr>
            ";
        }
        $html.="</table>";
        return $html;
    }


    static function DisplayFieldsHtml_Div($flist)
    {
        if (!count($flist)) return null;
        $html = '';

        foreach($flist as $f)
        {
            $tooltip='';

            if ($f->field->help) $tooltip=JHtml::_('tooltip',$f->field->help).'&nbsp;';

            $html .= '<div class="control-group">';
            $html .= '<div class="job_edit_field_container">';


            if('hidden' != $f->field->ftype){
                $html .= '<label class="control-label job_lables">'.$tooltip.JText::_($f->field->name);
                if ($f->field->compulsory) {
                    $html .= '&nbsp;' . JHtml::image(JUri::root() . 'components/com_jobsfactory/images/requiredField.png', JText::_('COM_JOBS_FIELD_REQUIRED'));
                }
                $html .= '</label>';
            }

            if ($f->field->ftype != 'radioBox') {
                $html.='<div class="controls">'.'<div class="job_values">'.$f->html.'</div>'.'</div></div>';
            } else {
                $html.= $f->html.'</div>';
            }

            $html .= '</div>';
        }

        return $html;
    }
}
