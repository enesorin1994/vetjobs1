<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

abstract class JHtmlFactoryLocation
{
    static function select($inputname, $html_attribs = '', $selectedloc = null, $include_disabled = false, $parents_disabled = false, $include_empty_opt = false, $empty_opt_label = null)
    {
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models');
        $locationModel = JModelLegacy::getInstance('Location', 'JobsAdminModel');

        $location_tree = $locationModel->getLocationTree(0, $include_disabled,1);

        if (!count($location_tree))
            return "";
        $options = array();
        if ($include_empty_opt)
            $options[] = JHTML::_('select.option', '',
                $empty_opt_label ? $empty_opt_label : JText::_('COM_JOBS__ANY_LOCATION'),
                'value', 'text');

        foreach ($location_tree as $location)
        {
            //TODO: nr_children nu e intors de model
            $location->depth = 1;
            $spacer = str_pad('', ($location->depth - 1) * 3, '-');
            $disabled = ($parents_disabled && $location->nr_children);
            $options[] = JHTML::_('select.option', $location->id,
                $spacer . stripslashes($location->locname),
                'value', 'text', $disabled);
        }

        $emptyopt[] = JHTML::_('select.option', '', JText::_("COM_JOBS__CHOOSE_LOCATION"));
        $options = array_merge($emptyopt, $options);

        $html_tree = JHTML::_('select.genericlist', $options, $inputname, $html_attribs, 'value', 'text', $selectedloc);

        return $html_tree;
    }
}
