<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

abstract class JHTMLResumelanguage
{
    static function selectlist($name='resume_language',$attributes='',$defaultvalue=null)
    {
        $opts = array();
        $opts[] = JHTML::_('select.option', 0,JText::_("English"));
        $opts[] = JHTML::_('select.option', 1,JText::_("French"));
        $opts[] = JHTML::_('select.option', 2,JText::_("Dutch"));
        $opts[] = JHTML::_('select.option', 3,JText::_("Romana"));
        return JHTML::_('select.genericlist',  $opts, $name, $attributes ,  'value', 'text', $defaultvalue);
    }

}
