<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

abstract class JHTMLCountry
{
    static function selectlist($name = 'country', $attributes = '', $defaultvalue = null, $only_existing_users = false)
    {
        if (!$only_existing_users) {
            $db = JFactory::getDbo();
            $db->setQuery("select `name` as text,`id` as value from `#__" . APP_PREFIX . "_country` where `active`=1 order by `name` ");
            $country = $db->loadObjectList();
        } else {
            $model = JModelLegacy::getInstance('User', 'jobsModel');
            $countrylist = $model->getUserCountries();
            $country = array();

            foreach ($countrylist as $c)
                $country[] = JHTML::_('select.option', $c->country, $c->text);
        }
        $emptyopt[] = JHTML::_('select.option', '', JText::_("COM_JOBS__CHOOSE_COUNTRY"));
        $country = array_merge($emptyopt, $country);

        return JHtml::_('select.genericlist', $country, $name, $attributes, 'value', 'text', $defaultvalue);
    }

}
