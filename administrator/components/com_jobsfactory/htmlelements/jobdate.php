<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

abstract class JHTMLJobDate
{
    static function calendar($isodate, $name, $attr = array(), $idname)
    {
        $cfg = JTheFactoryHelper::getConfig();

        if ($name == 'start_date') {
            $result = JHtml::_('calendar', $isodate, $name, $name, JobsHelperDateTime::dateFormatConversion($cfg->date_format.' H:i:s'), $attr);
        }
        else
            $result = JHtml::_('calendar', $isodate, $name, $name, JobsHelperDateTime::dateFormatConversion($cfg->date_format), $attr);
			//$result = JHTML::_('calendar', $isodate, $name, $idname, JobsHelperDateTime::dateFormatConversion($cfg->date_format), array('class' => 'inputbox validate-date', 'size' => '25', 'maxlength' => '19'));

        if ($isodate) { //ISODATES and JHtml::_('calendar') doesn't take kindly all formats       
			if ($name == 'start_date') {            
				$result = str_replace(' value="' . htmlspecialchars($isodate, ENT_COMPAT, 'UTF-8') . '"',
                	' value="' . htmlspecialchars(JHtml::date($isodate, $cfg->date_format.' H:i:s', false), ENT_COMPAT, 'UTF-8') . '"',
                	$result
            	);
			} else {
				$result = str_replace(' value="' . htmlspecialchars($isodate, ENT_COMPAT, 'UTF-8') . '"',
                	' value="' . htmlspecialchars(JHtml::date($isodate, $cfg->date_format, false), ENT_COMPAT, 'UTF-8') . '"',
                	$result
            	);
        	}
		}
        return $result;
    }

}
