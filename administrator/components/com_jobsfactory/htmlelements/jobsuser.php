<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

abstract class JHTMLJobsUser
{
    static function selectlist($name='userid',$attributes='',$defaultvalue=null)
    {
        $db = JFactory::getDbo();
        /*$query="SELECT DISTINCT u.id as `value`, u.username as `text` FROM #__users u WHERE u.block!=1 ORDER BY username";
        $db->setQuery($query);
        $users = $db->loadObjectList();*/

        $cfg = JTheFactoryHelper::getConfig();
        $task = JFactory::getApplication()->input->getCmd('task','Show_Search');

        $profile = JobsHelperTools::getUserProfileObject();
        $tableName  = $profile->getIntegrationTable();
        $tableKey = $profile->getIntegrationKey();

        $query = JTheFactoryDatabase::getQuery();
        $companiesGroup = implode(',',$cfg->company_groups);

        $query->select('DISTINCT u.id as `value`');
        $query->select('u.username as `text`');
        $query->from('#__users', 'u');
        //$query="SELECT DISTINCT u.id as `value`, u.username as `text` FROM #__users u WHERE u.block!=1 ORDER BY username";
        if ($task == 'show_search' && $profile) {

            $query->join('left', $tableName, 'p', "p.`$tableKey` =`u`.`id` ");
            $query->join('left', '#__user_usergroup_map', 'm', "p.`$tableKey` =`m`.`user_id` ");
            $query->where('m.group_id IN ('."$companiesGroup".')');
        }
        $query->where(' u.block!=1 ');
        $query->order('u.username');
        $db->setQuery($query);
        $users = $db->loadObjectList();

        $users= array_merge(array(JHTML::_("select.option","",JText::_("COM_JOBS__ANY_USER"))),$users);
        return JHTML::_("select.genericlist",$users,$name,$attributes,'value', 'text', $defaultvalue);
    }

}
