<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

abstract class JHTMLJobtype
{
    static function selectlist($name='job_type',$attributes='',$defaultvalue=null)
    {
        $db = JFactory::getDbo();
        $db->setQuery("select `typename` as text,`id` as value from `#__".APP_PREFIX."_jobtype` WHERE `published` = 1 order by `ordering`,`typename`");
        $jobtypes = $db->loadObjectList();

        if (!$defaultvalue){
            $db->setQuery("select `typename` as value from `#__".APP_PREFIX."_jobtype` where `id`=1");
            $defaultvalue=$db->loadResult();
        }

        $emptyopt[] = JHTML::_('select.option', '', JText::_("COM_JOBS__CHOOSE_JOB_TYPE"));
        $jobtypes = array_merge($emptyopt, $jobtypes);

        return JHtml::_('select.genericlist',$jobtypes,$name,$attributes,'value','text',$defaultvalue);
    }

}
