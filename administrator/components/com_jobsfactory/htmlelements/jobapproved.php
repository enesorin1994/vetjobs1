<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

abstract class JHTMLJobApproved
{
    static function selectlist($name='filter_approved',$attributes='',$defaultvalue=null)
    {
        $opts = array();
        $opts[] = JHTML::_('select.option', "",JText::_("COM_JOBS__ANY_JOB"));
        $opts[] = JHTML::_('select.option', 1,JText::_("COM_JOBS_APPROVED"));
        $opts[] = JHTML::_('select.option', 0,JText::_("COM_JOBS_PENDING_APPROVAL"));
        return JHTML::_('select.genericlist',  $opts, $name, $attributes ,  'value', 'text', $defaultvalue);
    }

}
