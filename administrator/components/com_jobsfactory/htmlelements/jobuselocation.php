<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

abstract class JHTMLJobuselocation
{
    static function selectlist($name='use_location',$attributes='',$defaultvalue=null)
    {
        $opts = array();
        $cfg    = JTheFactoryHelper::getConfig();

        if ($cfg->enable_location_management) {
            $opts[] = JHTML::_('select.option', 1,JText::_("COM_JOBS_LOAD_COUNTRY_AND_LOCATION_LIST"));
            $opts[] = JHTML::_('select.option', 0,JText::_("COM_JOBS_HIDE_COUNTRY_AND_LOCATION_LIST"));
        } else {
            $opts[] = JHTML::_('select.option', 1,JText::_("COM_JOBS_LOAD_COUNTRY_LIST"));
            $opts[] = JHTML::_('select.option', 0,JText::_("COM_JOBS_HIDE_COUNTRY_LIST"));
        }

        return JHTML::_('select.radiolist',  $opts, $name, $attributes ,  'value', 'text', $defaultvalue);
    }

}
