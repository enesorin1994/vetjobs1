<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

abstract class JHtmlFactoryCity
{
    static function select($inputname, $html_attribs = '', $selectedcity = null, $include_disabled = false, $parents_disabled = false, $include_empty_opt = false, $empty_opt_label = null)
    {
        JModelLegacy::addIncludePath(JPATH_ROOT.DS."administrator". DS . 'components' . DS . 'com_jobsfactory' . DS . 'models');
        //JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'thefactory' . DS . 'city' . DS . 'models');

        if(!JModelLegacy::getInstance('City', 'JobsAdminModel'))
		{
			require_once(JPATH_ROOT. '/administrator/components/com_jobsfactory/models/city.php');
		}
		$cityModel = JModelLegacy::getInstance('City', 'JobsAdminModel');

        $selectedloc = 0;
        if ($selectedcity) {
            $db = JFactory::getDbo();
            $filter = "`status`=1 and ";
            $db->setQuery("SELECT c.location FROM `#__jobsfactory_cities` c WHERE {$filter} `id`=" . $selectedcity . " ");
            $selectedloc = $db->loadResult();
        }

        $city_tree = $cityModel->getCityTree($selectedloc, $include_disabled);

        if (!count($city_tree))
            return "";
        $options = array();

        if ($include_empty_opt)
            $options[] = JHTML::_('select.option', '',
                $empty_opt_label ? $empty_opt_label : JText::_('COM_JOBS__ANY_CITY'),
                'value', 'text');


        foreach ($city_tree as $city)
        {
            //TODO: nr_children nu e intors de model
            $city->depth = 1;
            $spacer = str_pad('', ($city->depth - 1) * 3, '-');
            $disabled = ($parents_disabled && $city->nr_children);
            $options[] = JHTML::_('select.option', $city->id,
                $spacer . stripslashes($city->cityname),
                'value', 'text', $disabled);
        }

        $emptyopt[] = JHTML::_('select.option', '', JText::_("COM_JOBS__CHOOSE_CITY"));
        $options = array_merge($emptyopt, $options);

        $html_tree = JHTML::_('select.genericlist', $options, $inputname, $html_attribs, 'value', 'text', $selectedcity);

        return $html_tree;

    }
}
