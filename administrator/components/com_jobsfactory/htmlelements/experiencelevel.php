<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

abstract class JHTMLExperiencelevel
{
    static function selectlist($name='experience_level',$attributes='',$defaultvalue=null)
    {
        $db = JFactory::getDbo();
        $db->setQuery("select id as `value`, `levelname` as text from `#__".APP_PREFIX."_experiencelevel` where `published`=1 order by `ordering`,`levelname`");
        $levelnames = $db->loadObjectList();

        if (!$defaultvalue){
            $db->setQuery("select `id` as value from `#__".APP_PREFIX."_experiencelevel` where `id`=1");
            $defaultvalue=$db->loadResult();
        }

        return JHtml::_('select.genericlist',$levelnames,$name,$attributes,'value','text',$defaultvalue);
    }

}
