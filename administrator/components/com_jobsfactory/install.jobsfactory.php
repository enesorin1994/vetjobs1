<?php 
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.8.0
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');
defined('DS') || define('DS', DIRECTORY_SEPARATOR);

class com_jobsfactoryInstallerScript
{
    public $installer;
    
	public function install($adapter)
    {

    }
	public function update($adapter)
    {
        if (!is_object($this->installer)) {
            $this->installer = new TheFactoryJobsfactoryInstaller('com_jobsfactory', $adapter);
        }

        $this->installer->AddMessage("<h1>Your upgrade from Jobs Factory v. " . $this->installer->getPreviousVersion() . " to v. " . $this->installer->getCurrentVersion() . " has finished</h1>");
        $this->installer->AddMessage($this->installer->askTemplateOverwrite());
    }
	public function uninstall($adapter)
    {
        
    }
	public function preflight($route,$adapter)
    {
        require_once($adapter->getParent()->getPath('source') . DS. 'administrator' . DS . 'components'.DS.'com_jobsfactory'.DS.'thefactory'.DS.'installer'.DS.'installer.php');
        require_once($adapter->getParent()->getPath('source') . DS . 'components' . DS . 'com_jobsfactory' . DS . 'installer' . DS . 'jobsfactory_installer.php');
        $this->installer=new TheFactoryJobsfactoryInstaller('com_jobsfactory',$adapter);
        
        if (!$this->installer->versionprevious){ //new install
            $this->installer->AddMenuItem("Jobs MainMenu","Jobs","show-jobs","index.php?option=com_jobsfactory&task=listjobs",1 );
            $this->installer->AddMenuItem("Jobs MainMenu","Categories","categories","index.php?option=com_jobsfactory&task=categories", 1 );
        	$this->installer->AddMenuItem("Jobs MainMenu","Search","search-jobs","index.php?option=com_jobsfactory&task=show_search", 1 );
            $this->installer->AddMenuItem("Jobs MainMenu","Companies List","all-companies-list","index.php?option=com_jobsfactory&view=companies&task=listcompanies", 2 );
            $this->installer->AddMenuItem("Jobs MainMenu","Candidates List","all-candidates-list","index.php?option=com_jobsfactory&view=candidates&task=listcandidates", 2 );
            $this->installer->AddMenuItem("Jobs MainMenu","My Jobs Openings","my-jobs","index.php?option=com_jobsfactory&task=myjobs", 2 );
            $this->installer->AddMenuItem("Jobs MainMenu","Add New Job","add-new-job","index.php?option=com_jobsfactory&task=form", 2 );
            $this->installer->AddMenuItem("Jobs MainMenu","Applications","my-applications","index.php?option=com_jobsfactory&controller=applications&task=myapplications", 2 );
            $this->installer->AddMenuItem("Jobs MainMenu","Watchlist","my-watchlist","index.php?option=com_jobsfactory&controller=watchlist&task=watchlist", 2 );
            $this->installer->AddMenuItem("Jobs MainMenu","Profile","jobsfactory-profile","index.php?option=com_jobsfactory&task=userdetails&controller=user",  2 );

            $this->installer->AddSQLFromFile('install.jobsfactory.inserts.sql');
            
        }
    
        $this->installer->AddCBPlugin('Jobs Factory - Google Map','Google Map','jobs_googlemap','getjobsmymap');
        $this->installer->AddCBPlugin('Jobs Factory - My Applications','My Applications','jobs_my_applications','getmyapplicationsTab');
        $this->installer->AddCBPlugin('Jobs Factory - My Jobs','My Jobs','jobs_my_jobs','getmyjobsTab');
        $this->installer->AddCBPlugin('Jobs Factory - My TaskPad','Jobs Taskpad','jobs_my_taskpad','myjobsTaskPad');
        $this->installer->AddCBPlugin('Jobs Factory - My Watchlist','My Watchlist','jobs_my_watchlist','getmyjobswatchlistTab');

       //$this->installer->AddMessageFromFile('install.notes.txt');
        
        $this->installer->AddMessage("Thank you for purchasing <strong>Jobs Factory</strong>");
        $this->installer->AddMessage("Please set up your <strong>Jobs Factory</strong> in the <a href='".JURI::root()."/administrator/index.php?option=com_jobsfactory&task=settingsmanager'>admin panel</a></p>");
        $this->installer->AddMessage("Visit us at <a target='_blank' href='http://www.thePHPfactory.com'>thePHPfactory.com</a> to learn  about new versions and/or to give us feedback<br>");
        $this->installer->AddMessage("(c) 2006 - " . date('Y') . " thePHPfactory.com");

    }
	public function postflight($route,$adapter)
    {
        
        if ($this->installer->versionprevious){
            $this->installer->upgrade();
            //$adapter->getParent()->set('redirect_url',"index.php?option=com_jobsfactory&task=postupgrade");
        }
        else {
            $this->installer->install();
            JFolder::move(JPATH_SITE.DS.'components'.DS.'com_jobsfactory'.DS.'templates-dist',
                JPATH_SITE.DS.'components'.DS.'com_jobsfactory'.DS.'templates');
            JFile::copy(
                JPATH_SITE.DS.'components'.DS.'com_jobsfactory'.DS.'options-dist.php',
                JPATH_SITE.DS.'components'.DS.'com_jobsfactory'.DS.'options.php'
            );
            
        }

        if(!JFolder::exists(JPATH_SITE.DS.'media'.DS.'com_jobsfactory'.DS.'files'))
            JFolder::create(JPATH_SITE.DS.'media'.DS.'com_jobsfactory'.DS.'files');
        if(!JFolder::exists(JPATH_SITE.DS.'media'.DS.'com_jobsfactory'.DS.'images'))
            JFolder::create(JPATH_SITE.DS.'media'.DS.'com_jobsfactory'.DS.'images');
        if(!JFolder::exists(JPATH_ROOT.DS.'cache'.DS.'com_jobsfactory'.DS.'templates'))
            JFolder::create(JPATH_ROOT.DS.'cache'.DS.'com_jobsfactory'.DS.'templates');


		$messageGroups="<h3><span style='color: green;'>Please set up your <strong>Jobs Factory groups (4 steps): </strong></span></h3> ";

		$messageGroups.="<h4> 1. From Joomla Administrator Menu 'Users' choose Groups to define Companies and Candidates groups.  <br />";

		$messageGroups.="Group Parent must be 'Registered'. <br />";

		$messageGroups.="Group names can be Companies and Candidates or set your own names. <br /><br />";

		$messageGroups.="2. Define 2 new Viewing Access Levels for new created groups: <br />";

		$messageGroups.="First access level <br />";

		$messageGroups.="Level Title - Companies <br />";

		$messageGroups.="AND check 'Companies' in the list 'User Groups Having Viewing Access'.<br />";

		$messageGroups.="Second access level <br />";

		$messageGroups.="Level Title - Candidates <br />";

		$messageGroups.="AND check 'Candidates' in the list 'User Groups Having Viewing Access'.<br /><br />";

		$messageGroups.="3. From Joomla Administrator menu 'Menus'->Jobs MainMenu <br /><br />";

		$messageGroups.="Set access level group to 'Companies' for menu items: 'Add New Job', 'Candidates List', 'My Jobs Openings' <br />";

		$messageGroups.="For 'Companies List' menu item set access Public or Registered, as you consider. <br />";

		$messageGroups.="Set access level group to 'Candidates' for 'Applied List' menu item. <br /><br />";

		$messageGroups.="4. From Jobs Factory <a href='".JURI::root()."administrator/index.php?option=com_jobsfactory&task=config.display'>Settings->General Settings->ACL tab</a> set 'User Groups' for groups: <br />";

		$messageGroups.="Candidates - choose 'Candidates' from the drop down list <br />";

		$messageGroups.="and Companies - choose 'Companies' from the drop down list <br />";

		$messageGroups.="and Save.   </h4>";

        $message=is_array($this->installer->extension_message)?implode("\r\n",$this->installer->extension_message):$this->installer->extension_message;
		$message .= $messageGroups;

		$error=is_array($this->installer->errors)?implode("<br/>",$this->installer->errors):$this->installer->errors;
        $warning=is_array($this->installer->warnings)?implode("<br/>",$this->installer->warnings):$this->installer->warnings;

        if ($error)  JFactory::getApplication()->enqueueMessage($error, 'error');
        if ($warning) JFactory::getApplication()->enqueueMessage($warning, 'warning');
        $adapter->getParent()->set('extension_message',$message);



        $session = JFactory::getSession();
        $session->set('com_jobsfactory_install_msg',$message);
    }
}
