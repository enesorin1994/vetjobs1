<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

if(strpos($task,'.'))
{
    $c=substr($task,0,strpos($task,'.'));
    if (in_array($c,array('themes','fields','positions','mailman','pricing','gateways','currencies','location','city','category','integration')))
        JHtmlSidebar::addEntry(
                JText::_('COM_JOBS_SETTINGS'),
                'index.php?option='.APP_EXTENSION.'&task=settingsmanager',
                $vName == 'settingsmanager'
        );
    if (in_array($c,array('orders','payments','balances')))
        JHtmlSidebar::addEntry(
                JText::_('COM_JOBS_PAYMENTS'),
                'index.php?option='.APP_EXTENSION.'&task=payments.listing',
                $vName == 'payments.listing'
        );
    return;
}

