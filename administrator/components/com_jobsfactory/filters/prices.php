<?php
    /**------------------------------------------------------------------------
    com_jobsfactory - Jobs Factory 1.6.5
    ------------------------------------------------------------------------
     * @author thePHPfactory
     * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
     * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     * Websites: http://www.thePHPfactory.com
     * Technical Support: Forum - http://www.thePHPfactory.com/forum/
     * @package: Jobs
    -------------------------------------------------------------------------*/

	// Access the file from Joomla environment
	defined('_JEXEC') or die('Restricted access');
	// Include front price filter definition
	// in order preventing duplicate since is used the same behaviour

	if (file_exists(JPATH_SITE . '/components/'.APP_EXTENSION.'/filters/prices.php')) {
		require_once JPATH_SITE . '/components/'.APP_EXTENSION.'/filters/prices.php';
	}

