<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Pay per contact
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JTheFactoryEventPrice_Contact extends JTheFactoryEvents
{
    function getItemName()
    {
        return "contact";
    }
    function getContext()
    {
        return APP_PREFIX.".".self::getItemName();
    }

    function &getModel()
    {
        jimport('joomla.application.component.model');
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'pricing'.DS.self::getItemName().DS.'models');
        $model = JModelLegacy::getInstance(self::getItemName(),'JobsfactoryPricingModel');
        return $model;
    }


    function onBeforeDisplay($task,$smarty)
    {
        if (!is_object($smarty))
            return;

        $task=strtolower($task);

        if(!in_array($task, array('userprofile','showusers') ) )
            return;
        
        $Itemid = JFactory::getApplication()->input->getInt('Itemid');
        $user = JFactory::getUser();
        $model = self::getModel();
        $price = $model->getItemPrice();

        $url_buy = 'index.php?option='.APP_EXTENSION.'&task=buy_contact&id=%s&Itemid='.$Itemid;
        if (isset($smarty->_tpl_vars["user"]) && is_object($smarty->_tpl_vars["user"]))
        {
            $userid = isset($smarty->_tpl_vars["user"]->userid )?$smarty->_tpl_vars["user"]->userid:$smarty->_tpl_vars["user"]->id;
            if ($user->id!==$userid && !$model->checkContact($userid))
            {
                $url=sprintf($url_buy,$userid);
    			$smarty->_tpl_vars["user"]->name = "hidden&nbsp;<a href='$url'>".JText::_("COM_JOBS_BUY_THIS_CONTACT_FOR")." ".number_format($price->price,2)." ".$price->currency."</a>";
    			$smarty->_tpl_vars["user"]->surname = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                $smarty->_tpl_vars["user"]->website = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                $smarty->_tpl_vars["user"]->city = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                $smarty->_tpl_vars["user"]->country = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
    			$smarty->_tpl_vars["user"]->phone = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                $smarty->_tpl_vars["user"]->email = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
    			$smarty->_tpl_vars["user"]->address = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                $smarty->_tpl_vars["user"]->contactemail = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                $smarty->_tpl_vars["user"]->contactname = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                $smarty->_tpl_vars["user"]->contactposition = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                $smarty->_tpl_vars["user"]->contactphone = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
    			$smarty->_tpl_vars["user"]->paypalemail = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
    			$smarty->_tpl_vars["user"]->YM = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
    			$smarty->_tpl_vars["user"]->Hotmail = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
    			$smarty->_tpl_vars["user"]->Skype = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                $smarty->_tpl_vars["user"]->linkedIN = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                $smarty->_tpl_vars["user"]->facebook = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
            }
        }
        
        if(isset($smarty->_tpl_vars["users"]) && is_array($smarty->_tpl_vars["users"]))
        {
            for($i=0;$i<count($smarty->_tpl_vars["users"]);$i++)
            {
                $userid=isset($smarty->_tpl_vars["users"][$i]->userid )?$smarty->_tpl_vars["users"][$i]->userid:$smarty->_tpl_vars["users"][$i]->id;
                if ($user->id!==$userid && !$model->checkContact($userid))
                {
                    $url=sprintf($url_buy,$userid);
        			$smarty->_tpl_vars["users"][$i]->name = "hidden&nbsp;<a href='$url'>".JText::_("COM_JOBS_BUY_THIS_CONTACT_FOR")." ".number_format($price->price,2)." ".$price->currency."</a>";
        			$smarty->_tpl_vars["users"][$i]->surname = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->website = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->city = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->country = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->phone = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->email = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
        			$smarty->_tpl_vars["users"][$i]->address = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->contactemail = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->contactname = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->contactposition = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->contactphone = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->paypalemail = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
        			$smarty->_tpl_vars["users"][$i]->YM = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
        			$smarty->_tpl_vars["users"][$i]->Hotmail = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
        			$smarty->_tpl_vars["users"][$i]->Skype = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->linkedIN = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                    $smarty->_tpl_vars["users"][$i]->facebook = "<span class='job_hidden'>".JText::_("COM_JOBS_HIDDEN")."</span>";
                }
           }
        }   
    }
    function onBeforeExecuteTask(&$stopexecution)
    {
        $app = JFactory::getApplication();
        $task = $app->input->getCmd('task','listjobs');

        if ($task=='buy_contact'){
            $user = JFactory::getUser();
            $id = $app->input->getInt("id");
            $model = self::getModel();

            if ($user->id==$id && $model->checkContact($id))
            {
                JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_CONTACT_IS_ALREADY_PURCHASED"),'warning');
                $app->redirect(JobsHelperRoute::getUserdetailsRoute($id, false));
                return;
            }
            $modelorder = JTheFactoryPricingHelper::getModel('orders');
            $modelbalance = JTheFactoryPricingHelper::getModel('balance');

            $price = $model->getItemPrice();
            $balance = $modelbalance->getUserBalance();
            $item = $model->getOderitem($id);

            if (JobsHelperPrices::comparePrices($price,array("price"=>$balance->balance,"currency"=>$balance->currency))>0)
            {
                $order=$modelorder->createNewOrder($item,$price->price,$price->currency,null,'P');
                $app->redirect(JobsHelperRoute::getCheckoutRoute($order->id));
                return;
            }
            //get funds from account, create confirmed order
            $balance_minus=JobsHelperPrices::convertCurrency($price->price,$price->currency,$balance->currency);
            $modelbalance->decreaseBalance($balance_minus);

            $order=$modelorder->createNewOrder($item,$price->price,$price->currency,null,'C');
            $model->addContact($id, $order->userid);
            $app->redirect(JobsHelperRoute::getUserdetailsRoute($id));
            return;
            
        }
    }
    function onPaymentForOrder($paylog,$order)
    {
        if ($order->status!='C') return;

        $modelorder=JTheFactoryPricingHelper::getModel('orders');
        $items=$modelorder->getOrderItems($order->id,self::getItemName());

        if (!is_array($items)||!count($items)) return; //no Listing items in order

        $model=self::getModel();

        foreach($items as $item){
            if (!$item->iteminfo) continue; //JobID is stored in iteminfo
            if ($item->itemname!=self::getItemName()) continue;

            $model->addContact($item->iteminfo, $order->userid);
        }
    }

    /**
     * onDefaultCurrencyChange
     */
    public function onDefaultCurrencyChange()
    {
        jimport('joomla.application.component.model');
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'pricing' . DS . self::getItemName() . DS . 'models');

        $modelApp = JModelLegacy::getInstance('Contact', APP_PREFIX . 'PricingModel');
        $r = $modelApp->getItemPrices();

        $modelCurrency = JModelLegacy::getInstance('Currency', 'JTheFactoryModel');
        $default_currency = $modelCurrency->getDefault();

        /* *****************************************************
         *                      $r return
         * *****************************************************
         * stdClass Object
         * (
         *	    [default_price] => 50.00
         *	    [default_currency] => EUR
         * 	    [price_powerseller] => 5
         *	    [price_verified] => 1
         * )
         * *****************************************************/

        // Convert prices to new default currency
        $itemPrices = array(
            'default_price' => number_format(JobsHelperPrices::convertToDefaultCurrency($r->default_price, $r->default_currency), 2),
            'currency' => $default_currency,
            'price_powerseller' => number_format(JobsHelperPrices::convertToDefaultCurrency($r->price_powerseller, $r->default_currency), 2),
            'price_verified' => number_format(JobsHelperPrices::convertToDefaultCurrency($r->price_verified, $r->default_currency), 2)
        );
        // Save converted prices
        $modelApp->saveItemPrices($itemPrices);
    }
}
