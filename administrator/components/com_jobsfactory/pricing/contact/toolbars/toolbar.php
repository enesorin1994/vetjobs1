<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Pay per contact
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JobsFactoryAdminContactToolbar
{
    public static function display($task=null)
    {
        JToolBarHelper::title( JText::_( 'COM_JOBS_PAYPERCONTACT__CONFIGURATION' ));
        switch($task)
        {
            default:
        		JToolBarHelper::apply('pricing.save');
        		JToolBarHelper::cancel('pricing.listing');
                JHtmlSidebar::addEntry(
              			JText::_('COM_JOBS_PAYMENT_ITEMS'),
              			'index.php?option='.APP_EXTENSION.'&task=pricing.listing',
              			false
              		);
            break;
         }

    }
}
