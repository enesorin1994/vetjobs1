<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Pay per listing
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JTheFactoryEventPrice_Listing extends JTheFactoryEvents
{
    function getItemName()
    {
        return "listing";
    }

    function getContext()
    {
        return APP_PREFIX . "." . self::getItemName();
    }

    function getModel()
    {
        jimport('joomla.application.component.model');
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'pricing' . DS . self::getItemName() . DS . 'models');
        $model = JModelLegacy::getInstance(self::getItemName(), 'JobsfactoryPricingModel');
        return $model;
    }

    /**
     * @param $task
     * @param $smarty
     */
    function onBeforeDisplay($task, $smarty)
    {
        if (!is_object($smarty))
            return;
        if (!in_array($task, array('republish', 'new', 'form', 'edit', 'editjob', 'newjob')))
            return;

        $id = JFactory::getApplication()->input->getInt("id");
        $curent_info = $smarty->get_template_vars('payment_items_header');

        if (in_array($task, array('form', 'edit', 'editjob')) && $id) {
            $orderitems = JobsHelperJob::getOrderItemsForJob($id, self::getItemName());
            if (count($orderitems)) {
                $priceinfo = JText::_("COM_JOBS_PAYMENT_FOR_THIS_JOB_IS_PENDING");
                foreach ($orderitems as $item)
                    if ($item->status=='C' || $item->status=='X')
                        return;
                //Job was payed for! or order was cancelled
                $smarty->assign('payment_items_header', $curent_info . $priceinfo);
                return;
            }
        }
        $job = $smarty->get_template_vars('job');

        $model = self::getModel();
        $price = $model->getItemPrice($job->cat);

        $modelbalance = JTheFactoryPricingHelper::getModel('balance');
        $balance = $modelbalance->getUserBalance();

        if (!floatval($price->price)) {
            $priceinfo = JText::_("COM_JOBS_PUBLISHING_IN_THIS_CATEGORY_IS_FREE");
        } else {
            $priceinfo = JText::_("COM_JOBS_YOUR_CURRENT_BALANCE_IS"). " " . number_format($balance->balance, 2) . " " . $balance->currency . "<br />";
            $priceinfo .= JText::_("COM_JOBS_PUBLISHING_IN_THIS_CATEGORY_WILL_COST"). " " . number_format($price->price, 2) . " " . $price->currency;
            if ($balance->currency && $balance->currency <> $price->currency) $priceinfo .= " (" .
                number_format(JobsHelperPrices::convertCurrency($price->price, $price->currency, $balance->currency), 2) . " " . $balance->currency . ")";
            $priceinfo .= "<br />";
            if (JobsHelperPrices::comparePrices($price, array("price" => $balance->balance, "currency" => $balance->currency)) > 0)
                $priceinfo .= JText::_("COM_JOBS_YOUR_FUNDS_ARE_INSUFFICIENT") . "<br/>";
        }
        $smarty->assign('payment_items_header', $curent_info . $priceinfo);

    }

    function onAfterSaveJobSuccess($job)
    {
        if (!$job->published) return; //not published yet

        $orderitems = JobsHelperJob::getOrderItemsForJob($job->id, self::getItemName());
        if (count($orderitems)) {
            foreach ($orderitems as $item)
                if ($item->status == 'C')
                    return;
            //Job was payed for!
        }

        $model = self::getModel();
        $price = $model->getItemPrice($job->cat);
        if (!floatval($price->price)) return; // Free publishing

        $modelbalance = JTheFactoryPricingHelper::getModel('balance');
        $balance = $modelbalance->getUserBalance();


        if (JobsHelperPrices::comparePrices($price, array("price" => $balance->balance, "currency" => $balance->currency)) > 0) {
            //insufficient funds
            $job->published = 0;
            $j = JTable::getInstance('jobs', 'Table');
            $j->bind($job);
            $j->store();
            //$job->store();

            $modelorder = JTheFactoryPricingHelper::getModel('orders');
            $item = $model->getOderitem($job);
            $order = $modelorder->createNewOrder($item, $price->price, $price->currency, null, 'P');
            $session = JFactory::getSession();
            $session->set('checkout-order', $order->id, self::getContext());
            return;
        }
        //get funds from account, create confirmed order
        $balance_minus = JobsHelperPrices::convertCurrency($price->price, $price->currency, $balance->currency);
        $modelbalance->decreaseBalance($balance_minus);

        $modelorder = JTheFactoryPricingHelper::getModel('orders');
        $item = $model->getOderitem($job);
        $order = $modelorder->createNewOrder($item, $price->price, $price->currency, null, 'C');

    }

    function onAfterExecuteTask($controller)
    {
        $session = JFactory::getSession();
        $orderid = $session->get('checkout-order', 0, self::getContext());
        $session->set('checkout-order', null, self::getContext());
        $session->clear('checkout-order', self::getContext());
        if ($orderid) $controller->setRedirect(JobsHelperRoute::getCheckoutRoute($orderid));

    }

    function onPaymentForOrder($paylog, $order)
    {
        if (!$order->status == 'C') return;

        $modelorder = JTheFactoryPricingHelper::getModel('orders');
        $items = $modelorder->getOrderItems($order->id, self::getItemName());

        if (!is_array($items) || !count($items)) return; //no Listing items in order

        $date = new JDate();
        $job = JTable::getInstance('jobs', 'Table');

        foreach ($items as $item) {
            if (!$item->iteminfo) continue; //JobID is stored in iteminfo
            if ($item->itemname != self::getItemName()) continue;

            if (!$job->load($item->iteminfo)) continue; //job no longer exists
            $job->modified = $date->toSql();
            $job->published = 1;
            $job->store();

            JTheFactoryEventsHelper::triggerEvent('onAfterSaveJobSuccess', array($job)); //for email notifications
        }
    }

    /**
     * onDefaultCurrencyChange
     */
    public function onDefaultCurrencyChange()
    {
        jimport('joomla.application.component.model');
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'pricing' . DS . self::getItemName() . DS . 'models');

        $modelListing = JModelLegacy::getInstance('Listing', APP_PREFIX . 'PricingModel');
        $r = $modelListing->getItemPrices();

        $modelCurrency = JModelLegacy::getInstance('Currency', 'JTheFactoryModel');
        $default_currency = $modelCurrency->getDefault();

        /* *****************************************************
         *                      $r return
         * *****************************************************
         * stdClass Object
         * (
         *	    [default_price] => 50.00
         *	    [default_currency] => EUR
         * 	    [price_powerseller] => 5
         *	    [price_verified] => 1
         *          [category_pricing_enabled] => 1
         *	    [category_pricing] => Array
         *		        (
         *		            [1] => Array
         *		                (
         *		                    [category] => 1
         *		                    [price] => 5
         *		                )
         *
         *		            [2] => Array
         *		                (
         *		                    [category] => 2
         *		                    [price] => 3
         *		                )
         *
         *		        )
         *  )
         * *****************************************************/

        // Convert prices to new default currency
        $itemPrices = array(
            'default_price' => number_format(JobsHelperPrices::convertToDefaultCurrency($r->default_price, $r->default_currency), 2),
            'currency' => $default_currency,
            'price_powerseller' => isset($r->price_powerseller) ? number_format(JobsHelperPrices::convertToDefaultCurrency($r->price_powerseller, $r->default_currency), 2) : null,
            'price_verified' => isset($r->price_verified) ? number_format(JobsHelperPrices::convertToDefaultCurrency($r->price_verified, $r->default_currency), 2) : null,
            'category_pricing_enabled' => $r->category_pricing_enabled,
            'category_pricing' => array()
        );

        if (isset($r->category_pricing) && count($r->category_pricing)) {
            foreach ($r->category_pricing as $arrCat) {
                $itemPrices['category_pricing'][$arrCat['category']] = isset($arrCat['price']) ? number_format(
                    JobsHelperPrices::convertToDefaultCurrency($arrCat['price'], $r->default_currency),
                    2) : null;
            }
        }

        // Save converted prices
        $modelListing->saveItemPrices($itemPrices);
    }

}
