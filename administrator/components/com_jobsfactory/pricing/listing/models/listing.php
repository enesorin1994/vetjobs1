<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Pay per listing
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.html.parameter');

class JobsfactoryPricingModelListing extends JModelLegacy
{
    var $name = "listing";
    var $description = "Pay per listing";
    var $context = 'listing';

    function loadPricingObject()
    {
        $db = JFactory::getDbo();
        $db->setQuery("select * from `#__" . APP_PREFIX . "_pricing` where `itemname`='" . $this->name . "'");
        return $db->loadObject();
    }

    function getItemPrices()
    {
        $db = JFactory::getDbo();
        $r = $this->loadPricingObject();
        $params = new JRegistry($r->params);

        $res = new stdClass();
        $res->default_price = $r->price;
        $res->default_currency = $r->currency;
        $res->price_powerseller = $params->get('price_powerseller');
        $res->price_verified = $params->get('price_verified');
        $res->category_pricing_enabled = $params->get('category_pricing_enabled');

        $db->setQuery("select category,price from `#__" . APP_PREFIX . "_pricing_categories` where `itemname`='" . $this->name . "'");
        $res->category_pricing = $db->loadAssocList('category');


        return $res;
    }

    function getItemPrice($category)
    {
        $userprofile = JobsHelperTools::getUserProfileObject();
        $userprofile->getUserProfile();

        $r = $this->loadPricingObject();
        $params = new JRegistry($r->params);

        if ($userprofile->powerseller)
            $defaultprice = $params->get('price_powerseller', $r->price);
        elseif ($userprofile->verified)
            $defaultprice = $params->get('price_verified', $r->price);
        else
            $defaultprice = $r->price;

        $db = JFactory::getDbo();
        $db->setQuery("select price from `#__" . APP_PREFIX . "_pricing_categories` where `itemname`='" . $this->name . "' and category='$category'");
        $price = $db->loadResult();
        $res = new stdClass();
        $res->price = ($price === NULL) ? $defaultprice : $price;
        $res->currency = $r->currency;

        return $res;
    }

    function saveItemPrices($d)
    {
        $params = new JRegistry();
        $params->set('price_powerseller', JArrayHelper::getValue($d, 'price_powerseller'));
        $params->set('price_verified', JArrayHelper::getValue($d, 'price_verified'));
        $params->set('category_pricing_enabled', JArrayHelper::getValue($d, 'category_pricing_enabled'));
        $p = $params->toString('INI');
        $price = JArrayHelper::getValue($d, 'default_price');
        $currency = JArrayHelper::getValue($d, 'currency');

        $db = JFactory::getDbo();
        $db->setQuery("update `#__" . APP_PREFIX . "_pricing`
            set `price`='$price',`currency`='$currency',
            `params`='$p'
            where `itemname`='listing'");
        $db->execute();

        $db->setQuery("delete from `#__" . APP_PREFIX . "_pricing_categories` where `itemname`='" . $this->name . "'");
        $db->execute();

        $category_pricing = JArrayHelper::getValue($d, 'category_pricing', array(), 'array');
        foreach ($category_pricing as $k => $v)
            if (!empty($v) || ($v === '0')) {
                $db->setQuery("insert into `#__" . APP_PREFIX . "_pricing_categories` (`category`,`price`,`itemname`) values ('$k','$v','" . $this->name . "')");
                $db->execute();
            }
    }

    function getOderitem($job)
    {
        $price = $this->getItemPrice($job->cat);
        $item = new stdClass();
        $item->itemname = $this->name;
        $item->itemdetails = JText::_($this->description);
        $item->iteminfo = $job->id;
        $item->price = $price->price;
        $item->currency = $price->currency;
        $item->quantity = 1;
        $item->params = '';
        return $item;
    }
}
