<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Pay per listing
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class JobsfactoryPricingViewListingConfig extends JViewLegacy
{
    function display($tpl = null)
    {
        JHtml::_('behavior.framework');
        JHtml::_('bootstrap.tooltip');
        JHtml::_('dropdown.init');

        $doc= JFactory::getDocument();
        $doc->addStyleSheet(JURI::base().'components/com_jobsfactory/css/jobsfactory.css');
        $this->sidebar = JHtmlSidebar::render();

        parent::display($tpl);
    }

}

