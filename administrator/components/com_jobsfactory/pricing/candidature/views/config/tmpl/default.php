<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');
?>
<form name="adminForm" id="adminForm" action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="post">
<fieldset>
    <legend><?php echo JText::_("COM_JOBS_GENERAL_SETTINGS"); ?></legend>
    <table width="100%" class="paramlist admintable">
    <tbody>
    <tr>
        <td width="220" class="paramlist key"><?php echo JText::_("COM_JOBS_CURRENCY_USED"); ?>: </td>
        <td><?php echo JHtml::_('currency.selectlist','currency','',$this->currency);?></td>
    </tr>
    <tr>
        <td width="220" class="paramlist key"><?php echo JText::_("COM_JOBS_DEFAULT_LISTING_PRICE"); ?>: </td>
        <td><input value="<?php echo $this->default_price;?>" class="inputbox" name="default_price" size="5"></td>
    </tr>
    </tbody>
    </table>
</fieldset>
<fieldset>
    <legend><?php echo JText::_("COM_JOBS_SPECIAL_PRICES"); ?></legend>
    <table width="100%" class="paramlist admintable">
    <tbody>
    <tr>
        <td width="220" class="paramlist key"><?php echo JText::_("COM_JOBS_PRICE_FOR_POWERSELLERS"); ?>: </td>
        <td><input value="<?php echo $this->price_powerseller;?>" class="inputbox" name="price_powerseller" size="5"></td>
    </tr>
    <tr>
        <td width="220" class="paramlist key"><?php echo JText::_("COM_JOBS_PRICE_FOR_VERIFIED_USERS"); ?>: </td>
        <td><input value="<?php echo $this->price_verified;?>" class="inputbox" name="price_verified" size="5"></td>
    </tr>
    </tbody>
    </table>
</fieldset>
<input type="hidden" name="task" value="">
<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>">
<input type="hidden" name="item" value="<?php echo $this->itemname;?>">

</form>
