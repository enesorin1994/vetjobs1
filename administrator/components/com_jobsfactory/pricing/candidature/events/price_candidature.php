<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Pay per application
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JTheFactoryEventPrice_Candidature extends JTheFactoryEvents
{
    function getItemName()
    {
        return "candidature";
    }

    function getContext()
    {
        return APP_PREFIX . "." . self::getItemName();
    }

    function &getModel()
    {
        jimport('joomla.application.component.model');
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'pricing' . DS . self::getItemName() . DS . 'models');
        $model = JModelLegacy::getInstance(self::getItemName(), 'JobsPricingModel');
        return $model;
    }


    function onBeforeDisplay($task, $smarty)
    {
        if (!is_object($smarty))
            return;
        if (!in_array($task, array('viewapplications', 'details')))
            return;
        $id = JFactory::getApplication()->input->getInt("id");
        $job = $smarty->get_template_vars('job');
        $curent_info = $smarty->get_template_vars('payment_items_header');
        if ($job->isMyJob() || $job->close_offer) return; //TODO: Verify if job Expired
        $model = self::getModel();
        $price = $model->getItemPrice($job->cat);
        if (!floatval($price->price)) {
            return;
        } else {
            $priceinfo = JText::_("COM_JOBS_PRICE_PER_APPLICATION") . " " . number_format($price->price, 2) . " " . $price->currency;
        }
        $smarty->assign('payment_items_header', $curent_info . $priceinfo);

    }

    function onAfterSaveApplication($job, $application)
    {
        if ($application->cancel) return; //not published yet

        $orderitems = JobsHelperJob::getOrderItemsForJob($application->id, self::getItemName());
        if (count($orderitems)) {
            foreach ($orderitems as $item)
                if ($item->status == 'C')
                    return;
            //Job was payed for!
        }

        $model = self::getModel();
        $price = $model->getItemPrice($job->cat);
        if (!floatval($price->price)) return; // Free publishing


        $modelbalance = JTheFactoryPricingHelper::getModel('balance');
        $balance = $modelbalance->getUserBalance();


        if (JobsHelperPrices::comparePrices($price, array("price" => $balance->balance, "currency" => $balance->currency)) > 0) {
            //insufficient funds
            //hide application
            $application->cancel = 0;
            $application->store();

            $modelorder = JTheFactoryPricingHelper::getModel('orders');
            $item = $model->getOderitem($application);
            $order = $modelorder->createNewOrder($item, $price->price, $price->currency, null, 'P');
            $session = JFactory::getSession();
            $session->set('checkout-order', $order->id, self::getContext());
            return;
        }
        //get funds from account, create confirmed order
        $balance_minus = JobsHelperPrices::convertCurrency($price->price, $price->currency, $balance->currency);

        $modelbalance->decreaseBalance($balance_minus);

        $modelorder = JTheFactoryPricingHelper::getModel('orders');
        $item = $model->getOderitem($application);
        $order = $modelorder->createNewOrder($item, $price->price, $price->currency, null, 'C');

    }

    function onAfterExecuteTask($controller)
    {
        $session = JFactory::getSession();
        $orderid = $session->get('checkout-order', 0, self::getContext());
        $session->set('checkout-order', null, self::getContext());
        $session->clear('checkout-order', self::getContext());
        if ($orderid) $controller->setRedirect(JobsHelperRoute::getCheckoutRoute($orderid));

    }

    function onPaymentForOrder($paylog, $order)
    {
        if (!$order->status == 'C') return;
        $modelorder = JTheFactoryPricingHelper::getModel('orders');
        $items = $modelorder->getOrderItems($order->id, self::getItemName());
        if (!is_array($items) || !count($items)) return; //no Listing items in order

        $date           = new JDate();
        $candidature    = JTable::getInstance('candidates', 'Table');
        $job            = JTable::getInstance('jobs', 'Table');

        foreach ($items as $item) {
            if (!$item->iteminfo) continue; //JobID is stored in iteminfo
            if ($item->itemname != self::getItemName()) continue;

            if (!$candidature->load($item->iteminfo)) continue; //job no longer exists
            $candidature->modified = $date->toSql();
            $candidature->cancel = 0;
            $candidature->store();
            $job->load($candidature->job_id);
            JTheFactoryEventsHelper::triggerEvent('onAfterSaveApplication', array($job, $candidature)); //for email notifications

        }

    }

    /**
     * onDefaultCurrencyChange
     */
    public function onDefaultCurrencyChange()
    {
        jimport('joomla.application.component.model');
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'pricing' . DS . self::getItemName() . DS . 'models');

        $modelApp = JModelLegacy::getInstance('Candidature', APP_PREFIX . 'PricingModel');
        $r = $modelApp->getItemPrices();

        $modelCurrency = JModelLegacy::getInstance('Currency', 'JTheFactoryModel');
        $default_currency = $modelCurrency->getDefault();

        /* *****************************************************
         *                      $r return
         * *****************************************************
         * stdClass Object
         * (
         *	    [default_price] => 50.00
         *	    [default_currency] => EUR
         * 	    [price_powerseller] => 5
         *	    [price_verified] => 1
         * )
         * *****************************************************/

        // Convert prices to new default currency
        $itemPrices = array(
            'default_price' => number_format(JobsHelperPrices::convertToDefaultCurrency($r->default_price, $r->default_currency), 2),
            'currency' => $default_currency,
            'price_powerseller' => number_format(JobsHelperPrices::convertToDefaultCurrency($r->price_powerseller, $r->default_currency), 2),
            'price_verified' => number_format(JobsHelperPrices::convertToDefaultCurrency($r->price_verified, $r->default_currency), 2)
        );
        // Save converted prices
        $modelApp->saveItemPrices($itemPrices);
    }
}
