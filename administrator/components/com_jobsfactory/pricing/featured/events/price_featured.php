<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Pay per featured
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JTheFactoryEventPrice_Featured extends JTheFactoryEvents
{
    function getItemName()
    {
        return "featured";
    }
    function getContext()
    {
        return APP_PREFIX.".".self::getItemName();
    }
    function &getModel()
    {
        jimport('joomla.application.component.model');
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'pricing'.DS.self::getItemName().DS.'models');
        $model = JModelLegacy::getInstance(self::getItemName(),'JobsfactoryPricingModel');
        return $model;
    }


    function onBeforeDisplay($task,$smarty)
    {
        if (!is_object($smarty))
            return;
        if(!in_array($task, array('viewapplications','details') ) )
            return;

        $job = $smarty->get_template_vars('job');
        $curent_info = $smarty->get_template_vars('payment_items_header');

        if ($job->close_offer || !$job->published || !$job->isMyJob())
            return;

        $orderitems = JobsHelperJob::getOrderItemsForJob($job->id,self::getItemName());
        if (count($orderitems)){
            $priceinfo=JText::_("COM_JOBS_PAYMENT_FOR_FEATURE_JOB_IS_PENDING");
            foreach ($orderitems as $item)
                if ($item->status=='C' || $item->status=='X')
                    return;//Job was payed for! or order was cancelled
            $smarty->assign('payment_items_header',$curent_info.$priceinfo);
            return;
        }

        $model = self::getModel();
        $price = $model->getItemPrice($job->cat);

        if (!floatval($price->price) || $job->featured == 'featured'){
            return;
        } else {
            
            $priceinfo = "<a href='".JobsHelperRoute::getFeaturedRoute($job->id)."'>";
            $priceinfo .= JText::_("COM_JOBS_UPGRADE_TO_FEATURED_FOR").number_format($price->price,2)." ".$price->currency;
            $priceinfo .= "</a><br/>";
        }
        $smarty->assign('payment_items_header',$curent_info.$priceinfo);

    }
    function onBeforeExecuteTask(&$stopexecution)
    {
        $app = JFactory::getApplication();
        $task = $app->input->getCmd('task','listjobs');

        if ($task=='setfeatured'){

            $id = $app->input->getInt("id");
            $job = JTable::getInstance('jobs', 'Table');

            if (!$job->load($id)){
                $app->enqueueMessage(JText::_("COM_JOBS_ERROR_LOADING_JOB_ID").$id,'warning');
                $app->redirect(JobsHelperRoute::getJobDetailRoute($id));
                return;
            }
            if(!$job->isMyJob()){
                JFactory::getApplication()->enqueueMessage(JText::_("COM_JOBS_THIS_JOB_DOES_NOT_BELONG_TO_YOU"),'warning');
                $app->redirect(JobsHelperRoute::getJobDetailRoute($id));
                return;
            }

            $model = self::getModel();
            $modelorder = JTheFactoryPricingHelper::getModel('orders');
            $modelbalance = JTheFactoryPricingHelper::getModel('balance');

            $price = $model->getItemPrice();
            $balance = $modelbalance->getUserBalance();
            $item = $model->getOderitem($job);

            if (JobsHelperPrices::comparePrices($price,array("price"=>$balance->balance,"currency"=>$balance->currency))>0)
            {
                $order=$modelorder->createNewOrder($item,$price->price,$price->currency,null,'P');
                $app->redirect(JobsHelperRoute::getCheckoutRoute($order->id));
                return;
            }
            //get funds from account, create confirmed order
            $balance_minus = JobsHelperPrices::convertCurrency($price->price,$price->currency,$balance->currency);
            $modelbalance->decreaseBalance($balance_minus);

            $order = $modelorder->createNewOrder($item,$price->price,$price->currency,null,'C');
            $job->featured = 'featured';
            $job->store();
            $app->redirect(JobsHelperRoute::getJobDetailRoute($job->id));
            return;
        }

    }
    function onPaymentForOrder($paylog,$order)
    {

        if (!$order->status=='C') return;

        $modelorder = JTheFactoryPricingHelper::getModel('orders');
        $items = $modelorder->getOrderItems($order->id,self::getItemName());

        if (!is_array($items)||!count($items)) return; //no Listing items in order

        $date = new JDate();
        $job = JTable::getInstance('jobs', 'Table');
        foreach($items as $item){
            if (!$item->iteminfo) continue; //JobID is stored in iteminfo
            if ($item->itemname!=self::getItemName()) continue;

            if(!$job->load($item->iteminfo)) continue; //job no longer exists

            $job->modified = $date->toSql();
            $job->featured = 'featured';
            $job->store();
        }
    }

    /**
     * onDefaultCurrencyChange
     */
    public function onDefaultCurrencyChange()
    {
        jimport('joomla.application.component.model');
        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'pricing' . DS . self::getItemName() . DS . 'models');

        $modelListing = JModelLegacy::getInstance('Featured', APP_PREFIX . 'PricingModel');
        $r = $modelListing->getItemPrices();

        $modelCurrency = JModelLegacy::getInstance('Currency', 'JTheFactoryModel');
        $default_currency = $modelCurrency->getDefault();

        /* *****************************************************
         *                      $r return
         * *****************************************************
         * stdClass Object
         * (
         *	    [default_price] => 50.00
         *	    [default_currency] => EUR
         * 	    [price_powerseller] => 5
         *	    [price_verified] => 1
         *  )
         * *****************************************************/

        // Convert prices to new default currency
        $itemPrices = array(
            'default_price' => number_format(JobsHelperPrices::convertToDefaultCurrency($r->default_price, $r->default_currency), 2),
            'currency' => $default_currency,
            'price_powerseller' => isset($r->price_powerseller) ? number_format(JobsHelperPrices::convertToDefaultCurrency($r->price_powerseller, $r->default_currency), 2) : null,
            'price_verified' => isset($r->price_verified) ? number_format(JobsHelperPrices::convertToDefaultCurrency($r->price_verified, $r->default_currency), 2) : null
        );

        // Save converted prices
        $modelListing->saveItemPrices($itemPrices);
    }
}
