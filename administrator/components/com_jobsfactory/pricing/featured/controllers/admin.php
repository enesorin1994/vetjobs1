<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Pay per featured
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JjobsfactoryAdminFeaturedController extends JControllerLegacy
{
    var $name='AdminFeatured';
    var $_name='AdminFeatured';
    var $itemname='featured';
    var $itempath=null;

	function __construct()
	{
	    $this->itempath=JPATH_COMPONENT_ADMINISTRATOR.DS.'pricing'.DS.$this->itemname;
	   $config=array(
            'view_path'=>$this->itempath.DS."views"
       );
       JLoader::register(APP_PREFIX . 'AdminFeaturedToolbar',$this->itempath.DS.'toolbars'.DS.'toolbar.php');
       JLoader::register(APP_PREFIX . 'AdminFeaturedHelper',$this->itempath.DS.'helpers'.DS.'helper.php');
       jimport('joomla.application.component.model');
       JModelLegacy::addIncludePath($this->itempath.DS.'models');
       JTable::addIncludePath($this->itempath.DS.'tables');
       $lang=JFactory::getLanguage();
       $lang->load(APP_PREFIX.'.'.$this->itemname);

       parent::__construct($config);
    }
    function getView( $name = '', $type = 'html', $prefix = '', $config = array() )
    {
        $MyApp=JTheFactoryApplication::getInstance();
        $config['template_path']=$this->itempath.DS.'views'.DS.strtolower($name).DS."tmpl";

        return parent::getView($name,$type,$prefix,$config);
    }

    function execute($task)
    {
        JobsFactoryAdminFeaturedToolbar::display($task);
        return parent::execute($task);
    }

    function config()
    {
        JHtml::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'htmlelements');

        $model = JModelLegacy::getInstance('Featured',APP_PREFIX.'PricingModel');
        $r  = $model->getItemPrices();

        $view=$this->getView('Config','html','JobsPricingViewFeatured');
        $view->assign('currency',$r->default_currency);
        $view->assign('default_price',$r->default_price);
        $view->assign('price_powerseller',$r->price_powerseller);
        $view->assign('price_verified',$r->price_verified);
        $view->assign('itemname',$this->itemname);

        $view->display();
    }

    function save()
    {
        //$d=JTheFactoryHelper::getRequestArray('post');
        $d = JFactory::getApplication()->input->getArray($_POST);
        $model = JModelLegacy::getInstance('Featured',APP_PREFIX.'PricingModel');
        $model->saveItemPrices($d);

        $this->setRedirect('index.php?option='.APP_EXTENSION.'&task=pricing.config&item='.$this->itemname,JText::_('COM_JOBS_SETTINGS_SAVED'));
    }

    function listing() {
        $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=pricing.listing', JText::_('FACTORY_PRICING_SETTINGS_CANCELED'));
    }
}
