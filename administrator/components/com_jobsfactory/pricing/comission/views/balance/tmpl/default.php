<?php 
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 -------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php
    JHtml::_('behavior.framework');
    JHtml::_('bootstrap.tooltip');
    JHtml::_('behavior.multiselect');
    JHtml::_('dropdown.init');
    JHtml::_('formbehavior.chosen', 'select');

    JHtml::script(JURI::root().'administrator/components/'.APP_EXTENSION.'/pricing/listing/js/listing.js');
?>
<div style="float: left;">
    <?php $this->display('menu');?>
</div>


<?php if(!empty( $this->sidebar)): ?>
            <div id="j-sidebar-container" class="span2">
                  <?php echo $this->sidebar; ?>
            </div>
<?php endif; ?>
<div class="span10" id="j-main-container">
    <table class="table table-striped">
    <tr>
        <td width="*%" valign="top">
            <form name="adminForm" id="adminForm" action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="get">
            <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
            <input type="hidden" name="task" value="pricing.balance" />
            <input type="hidden" name="item" value="comission" />
            <input type="hidden" name="boxchecked" value="" />

            <?php echo JText::_("COM_JOBS_FILTER_BALANCE"),": ",$this->filterbox?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th width="5"><?php echo JText::_("COM_JOBS_USERID");?></th>
                    <th width="5"><?php echo JText::_("COM_JOBS_USER_NAME");?></th>
                    <th width="5"><?php echo JText::_("COM_JOBS_LAST_PAYMENT_DATE");?></th>
                    <th width="80"><?php echo JText::_("COM_JOBS_BALANCE");?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $odd=0;
                foreach ($this->userbalances as $userbalance) {?>
                    <tr class="row<?php echo ($odd=1-$odd);?>">
                        <td align="center">
                            <a href="index.php?option=<?php echo APP_EXTENSION;?>&task=detailUser&cid[]=<?php echo $userbalance->userid?>">
                                <?php echo $userbalance->userid;?>
                            </a>
                        </td>
                        <td>
                            <a href="index.php?option=<?php echo APP_EXTENSION;?>&task=detailUser&cid[]=<?php echo $userbalance->userid?>">
                                <?php echo $userbalance->username;?>
                            </a>
                        </td>
                        <td><?php echo $userbalance->lastpayment;?></td>
                        <td><?php echo number_format($userbalance->balance,2);?>&nbsp;<?php echo $userbalance->currency;?></td>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="15">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>

            </table>
            </form>
        </td>
    </tr>
    </table>
</div>
