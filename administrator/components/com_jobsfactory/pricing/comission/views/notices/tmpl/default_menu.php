<?php 
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<?php
    $app = JFactory::getApplication();
    $task = $app->input->getCmd('task');
?>

<style>
    td.current-comission{
        background: #fff;
        border: 1px #CCCCCC solid;
    }
</style>
<fieldset>
    <table width="200">
    <tr>
        <td align="center" height="100" valign="top" <?php echo ($task=='pricing.config')?"class='current-comission'":"";?>>
        	<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=pricing.config&item=comission">
    	    	<img src="<?php echo JURI::root();?>/administrator/components/<?php echo APP_EXTENSION;?>/pricing/comission/images/plugin_config.png" height="40" border="0" style="float: none;"/><br />
        	   <strong><?php echo JText::_('COM_JOBS_CONFIG');?></strong>
            </a>     
        </td>
    </tr>
    <tr>
        <td align="center" height="100" valign="top" <?php echo ($task=='pricing.balance')?"class='current-comission'":"";?>>
        	<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=pricing.balance&item=comission">
    	    	<img src="<?php echo JURI::root();?>/administrator/components/<?php echo APP_EXTENSION;?>/pricing/comission/images/stats.png" height="40" border="0" style="float: none;"/><br />
        	   <strong><?php echo JText::_('COM_JOBS_CANDIDATES_BALANCE');?></strong>
            </a>     
        </td>
    </tr>
    <tr>
        <td align="center" height="100" valign="top" <?php echo ($task=='pricing.jobs')?"class='current-comission'":"";?>>
        	<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=pricing.jobs&item=comission">
    	    	<img src="<?php echo JURI::root();?>/administrator/components/<?php echo APP_EXTENSION;?>/pricing/comission/images/jobs.png" height="40" border="0" style="float: none;"/><br />
        	   <strong><?php echo JText::_('COM_JOBS_JOB_COMMISSIONS');?></strong>
            </a>     
        </td>
    </tr>
    <tr>
        <td align="center" height="100" valign="top" <?php echo ($task=='pricing.payments')?"class='current-comission'":"";?>>
        	<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=pricing.payments&item=comission">
    	    	<img src="<?php echo JURI::root();?>/administrator/components/<?php echo APP_EXTENSION;?>/pricing/comission/images/payments.png" height="40" border="0" style="float: none;"/><br />
        	   <strong><?php echo JText::_('COM_JOBS_PAYMENTS');?></strong>
            </a>     
        </td>
    </tr>
    <tr>
        <td align="center" height="100" valign="top" <?php echo ($task=='pricing.notices')?"class='current-comission'":"";?>>
        	<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=pricing.notices&item=comission">
    	    	<img src="<?php echo JURI::root();?>/administrator/components/<?php echo APP_EXTENSION;?>/pricing/comission/images/plugin_email.png" height="40" border="0" style="float: none;"/><br />
        	   <strong><?php echo JText::_('COM_JOBS_SEND_PAYMENT_NOTICES');?></strong>
            </a>     
        </td>
    </tr>
    </table>
</fieldset>
