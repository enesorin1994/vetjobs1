<?php 
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access'); ?>

<table class="table table-striped">
<tr>
    <td width="200">
        <?php $this->display('menu');?>
    </td>
    <td width="*%" valign="top">
        <form name="adminForm" id="adminForm" action="<?php echo JRoute::_('index.php?option=com_jobsfactory'); ?>" method="get">
        <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
        <input type="hidden" name="task" value="pricing.jobs" />
        <input type="hidden" name="item" value="comission" />
        <input type="hidden" name="boxchecked" value="" />

        <table class="table table-striped">
            <thead>
            <tr>
                <th width="50"><?php echo JText::_("COM_JOBS_JOBID");?></th>
                <th width="*%"><?php echo JText::_("COM_JOBS_JOB_TITLE");?></th>
                <th width="80"><?php echo JText::_("COM_JOBS_WINNING_APPLICATION");?></th>
                <th width="150"><?php echo JText::_("COM_JOBS_DATE");?></th>
                <th width="80"><?php echo JText::_("COM_JOBS_COMMISSION");?></th>
            </tr>
            </thead>
        <tbody>
        <?php
        $odd=0;
        foreach ($this->jobs as $jobs) {?>
            <tr class="row<?php echo ($odd=1-$odd);?>">
                <td><?php echo $jobs->job_id;?></td>
                <td>
                    <a href="index.php?option=com_jobsfactory&task=edit&cid[]=<?php echo $jobs->job_id;?>">
                    <?php echo $jobs->title;?>
                    </a>
                </td>
                <td><?php echo number_format($jobs->application_price,2)," ",$jobs->currency;?></td>
                <td><?php echo $jobs->comission_date;?></td>
                <td><?php echo $jobs->amount," ",$jobs->currency;?></td>
            </tr>
        <?php } ?>
        </tbody>
            <tfoot>
                <tr>
                    <td colspan="15">
                        <?php echo $this->pagination->getListFooter(); ?>
                    </td>
                </tr>
            </tfoot>
        
        </table>
        </form>
    </td>
</tr>
</table>
