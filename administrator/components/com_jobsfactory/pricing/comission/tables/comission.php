<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Comission
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JTableComission extends JTable {
    var $id=null;
    var $userid=null;
    var $job_id=null;
    var $app_id=null;
    var $comission_date=null;
    var $amount=null;
    var $currency=null;

    function __construct(&$db) {
        parent::__construct('#__'.APP_PREFIX.'_pricing_comissions', 'id', $db);
    }
        
}


