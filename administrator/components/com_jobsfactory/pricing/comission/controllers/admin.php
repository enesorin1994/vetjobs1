<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Comission
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JjobsfactoryAdminComissionController extends JControllerLegacy
{
    var $name = 'AdminComission';
    var $_name = 'AdminComission';
    var $itemname = 'comission';
    var $itempath = null;

    function __construct()
    {
        JHtml::_('behavior.framework');
        $this->itempath = JPATH_COMPONENT_ADMINISTRATOR . DS . 'pricing' . DS . $this->itemname;
        $config = array(
            'view_path' => $this->itempath . DS . "views"
        );
        JLoader::register(APP_PREFIX . 'AdminComissionToolbar', $this->itempath . DS . 'toolbars' . DS . 'toolbar.php');
        JLoader::register(APP_PREFIX . 'AdminComissionHelper', $this->itempath . DS . 'helpers' . DS . 'helper.php');
        jimport('joomla.application.component.model');
        JModelLegacy::addIncludePath($this->itempath . DS . 'models');
        JTable::addIncludePath($this->itempath . DS . 'tables');
        $lang = JFactory::getLanguage();
        $lang->load(APP_PREFIX . '.' . $this->itemname);
        parent::__construct($config);

    }

    function getView($name = '', $type = 'html', $prefix = 'JobsPricingViewComission', $config = array())
    {
        $MyApp = JTheFactoryApplication::getInstance();
        $config['template_path'] = $this->itempath . DS . 'views' . DS . strtolower($name) . DS . "tmpl";
        return parent::getView($name, $type, $prefix, $config);
    }

    function execute($task)
    {
        JobsFactoryAdminComissionToolbar::display($task);
        return parent::execute($task);
    }

    function config()
    {
        jimport('joomla.html.editor');
        JHtml::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'htmlelements');

        $model = JModelLegacy::getInstance('Comission', APP_PREFIX . 'PricingModel');
        $r = $model->getItemPrices();

        JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'thefactory' . DS . 'category' . DS . 'models');
        $catModel = JModelLegacy::getInstance('Category', 'JTheFactoryModel');
        $cattree = $catModel->getCategoryTree();

        $pricing = $model->loadPricingObject();
        $params = new JRegistry($pricing->params);

        $editorName = JFactory::getConfig()->get('editor');
        $editor = JEditor::getInstance($editorName);

        $view = $this->getView('Config');

        $view->default_price = $r->default_price;
        $view->price_powerseller = $r->price_powerseller;
        $view->price_verified = $r->price_verified;
        $view->category_pricing_enabled = $r->category_pricing_enabled;
        $view->category_pricing = $r->category_pricing;
        $view->category_tree = $cattree;
        $view->itemname = $this->itemname;
        $view->editor = $editor;
        $view->email_text = base64_decode($params->get('email_text'));

        $view->display();
    }

    function save()
    {
        $d=JTheFactoryHelper::getRequestArray('post');
        $model = JModelLegacy::getInstance('Comission', APP_PREFIX . 'PricingModel');
        $model->saveItemPrices($d);

        $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=pricing.config&item=' . $this->itemname, JText::_('COM_JOBS_SETTINGS_SAVED'));
    }

    function balance()
    {
        $filter = JFactory::getApplication()->input->getString('filter');

        JTheFactoryHelper::modelIncludePath('payments');
        $balancemodel = JModelLegacy::getInstance('Balance', 'JTheFactoryModel');
        $model = JModelLegacy::getInstance('Comission', APP_PREFIX . 'PricingModel');

        if ($filter == 'negative')
            $balancemodel->set('filters', 'p.balance<0');
        $rows = $balancemodel->getBalancesList();
        foreach ($rows as $row)
        {
            $row->lastpayment = $model->getLastPaymentDate($row->userid);
        }
        $filterbox = JHTML::_('select.genericlist',
            array(
                JHTML::_('select.option', '', JText::_("COM_JOBS__FILTER")),
                JHTML::_('select.option', 'negative', JText::_("COM_JOBS_JUST_NEGATIVE_BALANCES")),
            )
            , 'filter', "onchange='this.form.submit()'", 'value', 'text', $filter);


        $view = $this->getView('Balance');
        $view->assign('filterbox', $filterbox);
        $view->assign('userbalances', $rows);
        $view->assign('pagination', $balancemodel->get('pagination'));
        $view->display();

    }

    function payments()
    {
        $model = JModelLegacy::getInstance('Comission', APP_PREFIX . 'PricingModel');
        $payments = $model->getPaymentsList();

        $view = $this->getView('Payments');
        $view->assign('payments', $payments);
        $view->assign('pagination', $model->get('pagination'));
        $view->display();

    }

    function notices()
    {
        JTheFactoryHelper::modelIncludePath('payments');
        $balancemodel = JModelLegacy::getInstance('Balance', 'JTheFactoryModel');
        $balancemodel->set('filters', 'p.balance<0');
        $rows = $balancemodel->getBalancesList();

        $view = $this->getView('Notices');
        $view->assign('userbalances', $rows);
        $view->assign('pagination', $balancemodel->get('pagination'));
        $view->display();

    }

    function sendnotice()
    {
        $userid = JFactory::getApplication()->input->getInt('userid'); //If null or 0 - ALL USERS
        $model = JModelLegacy::getInstance('Comission', APP_PREFIX . 'PricingModel');
        $users = $model->getNegativeBalanceUsers($userid);

        foreach ($users as $user)
        {
            $model->sendNotificationMail($user);
        }

        $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=pricing.notices&item=comission', count($users) . " " . JText::_('COM_JOBS_NOTIFICATIONS_SENT'));

    }

    function jobs()
    {
        $model = JModelLegacy::getInstance('Comission', APP_PREFIX . 'PricingModel');
        $jobs = $model->getJobComissions();

        $view = $this->getView('Jobs');
        $view->assign('jobs', $jobs);
        $view->assign('pagination', $model->get('pagination'));
        $view->display();
    }

    function listing() {
        $this->setRedirect('index.php?option=' . APP_EXTENSION . '&task=pricing.listing', JText::_('FACTORY_PRICING_SETTINGS_CANCELED'));
    }


}
