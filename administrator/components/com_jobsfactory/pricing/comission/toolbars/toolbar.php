<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
 * @subpackage: Comission
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

class JobsFactoryAdminComissionToolbar
{
    public static function display($task=null)
    {
        
        JToolBarHelper::title( JText::_( 'COM_JOBS_COMMISSION' ));

        JHtmlSidebar::addEntry(
             JText::_('COM_JOBS_PAYMENT_ITEMS'),
            'index.php?option='.APP_EXTENSION.'&task=pricing.listing',
             'listing'
         );

        switch($task)
        {
            case 'config':
            default:
                JToolBarHelper::title( JText::_( 'COM_JOBS_COMMISSION__CONFIGURATION' ));
        		JToolBarHelper::apply('pricing.save');
        		JToolBarHelper::cancel('pricing.listing');
            break;
            case 'balance':
                JToolBarHelper::title( JText::_( 'COM_JOBS_COMMISSION__USER_BALANCES' ));
            break;
            case 'payments':
                JToolBarHelper::title( JText::_( 'COM_JOBS_COMMISSION__USER_PAYMENTS' ));
            break;
            case 'notices':
                JToolBarHelper::title( JText::_( 'COM_JOBS_COMMISSION__NOTIFY_USERS' ));
            break;
         }

    }
}
