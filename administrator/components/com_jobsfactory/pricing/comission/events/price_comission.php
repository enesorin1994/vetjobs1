<?php
    /**------------------------------------------------------------------------
    com_jobsfactory - Jobs Factory 1.6.5
    ------------------------------------------------------------------------
     * @author thePHPfactory
     * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
     * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     * Websites: http://www.thePHPfactory.com
     * Technical Support: Forum - http://www.thePHPfactory.com/forum/
     * @package: Jobs
     * @subpackage: Comission
    -------------------------------------------------------------------------*/

    defined('_JEXEC') or die('Restricted access');

    class JTheFactoryEventPrice_Comission extends JTheFactoryEvents
    {
        function getItemName()
        {
            return "comission";
        }

        function getContext()
        {
            return APP_PREFIX . "." . self::getItemName();
        }

        function getModel()
        {
            jimport('joomla.application.component.model');
            JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'pricing' . DS . self::getItemName() . DS . 'models');
            $model = JModelLegacy::getInstance(self::getItemName(), 'JobsFactoryPricingModel');
            return $model;
        }

        function getTable()
        {
            JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'pricing' . DS . self::getItemName() . DS . 'tables');
            $table = JTable::getInstance('Comission');
            return $table;
        }

        function onBeforeDisplay($task, $smarty)
        {
            if (!is_object($smarty))
                return;
            if (!in_array($task, array('viewapplications')))
                return;

            $curent_info = $smarty->get_template_vars('payment_items_header');

            $job = $smarty->get_template_vars('job');
            if ($job->close_offer || !$job->published || !$job->isMyJob())
                return;

            $model = self::getModel();
            $price = $model->getItemPrice($job->cat);

            $modelbalance = JTheFactoryPricingHelper::getModel('balance');
            $balance = $modelbalance->getUserBalance();
            if (!floatval($price->price)) {
                $priceinfo = JText::_("COM_JOBS_THERE_IS_NO_COMMISSION_FOR_THIS_CATEGORY");
            } else {

                $priceinfo = JText::_("COM_JOBS_YOUR_CURRENT_BALANCE_IS") . number_format($balance->balance, 2) . " " . $balance->currency . "<br/>";
                $priceinfo .= JText::_("COM_JOBS_COMMISSION_FOR_THIS_CATEGORY_IS") . number_format($price->price, 2) . "% <br/>";

                if ($balance->currency ) { //<> $job->currency
                    $amount = JobsHelperPrices::convertCurrency($job->get('lowest_application'), $job->currency, $balance->currency);
                    $priceinfo .= " (" . number_format($amount * $price->price / 100, 2) . " $balance->currency)";
                }
                $priceinfo .= "<br/>";
            }
            $smarty->assign('payment_items_header', $curent_info . $priceinfo);

        }

        function onAfterAcceptApplication($job, $application)
        {
            if (!$job->published) return; //not published yet

            $orderitems = JobsHelperJob::getOrderItemsForJob($job->id, self::getItemName());
            if (count($orderitems)) {
                foreach ($orderitems as $item)
                    if ($item->status == 'C')
                        return;
                //Job was payed for!
            }

            $model = self::getModel();
            $price = $model->getItemPrice($job->cat);
            if (!floatval($price->price)) return; // Free publishing


            $modelbalance = JTheFactoryPricingHelper::getModel('balance');
            $balance = $modelbalance->getUserBalance();

            $comission_amount = $price->price * $application->application_price / 100;
            $currency = $job->currency;
            $funds_delta = 0;
            if (JobsHelperPrices::comparePrices(array("price" => $comission_amount, "currency" => $currency),
                array("price" => $balance->balance, "currency" => $balance->currency)) > 0
            ) {
                $funds_delta = JobsHelperPrices::convertCurrency($balance->balance, $balance->currency, $currency);
                if ($funds_delta <= 0) $funds_delta = 0; //if he has some funds - get the rest
                $has_funds = false;
            } else
                $has_funds = true;

            $balance_minus = JobsHelperPrices::convertCurrency($comission_amount, $currency, $balance->currency);
            $modelbalance->decreaseBalance($balance_minus);


            $modelorder = JTheFactoryPricingHelper::getModel('orders');
            $items = array($model->getOderitem($job, $application));
            if ($funds_delta > 0) {
                $item = new stdClass();
                $item->itemname = JText::_("COM_JOBS_EXISTING_FUNDS");
                $item->itemdetails = JText::_("COM_JOBS_EXISTING_FUNDS");
                $item->iteminfo = 0;
                $item->price = -$funds_delta;
                $item->currency = $currency;
                $item->quantity = 1;
                $item->params = '';
                $items[] = $item;
            }
            $order = $modelorder->createNewOrder($items, $comission_amount - $funds_delta, $currency, null, $has_funds ? 'C' : 'P');
            if (!$has_funds) {
                $session = JFactory::getSession();
                $session->set('checkout-order', $order->id, self::getContext());
            }

            $user = JFactory::getUser();
            $date = new JDate();

            $comission_table = self::getTable();
            $comission_table->userid = $user->id;
            $comission_table->job_id = $job->id;
            $comission_table->app_id = $application->id;
            $comission_table->comission_date = $date->toSql();
            $comission_table->amount = $comission_amount;
            $comission_table->currency = $currency;
            $comission_table->store();

        }

        function onAfterExecuteTask($controller)
        {
            $session = JFactory::getSession();
            $orderid = $session->get('checkout-order', 0, self::getContext());
            $session->set('checkout-order', null, self::getContext());
            $session->clear('checkout-order', self::getContext());
            if ($orderid) $controller->setRedirect(JobsHelperRoute::getCheckoutRoute($orderid));

        }

        function onPaymentForOrder($paylog, $order)
        {
            if (!$order->status == 'C') return;
            $modelorder = JTheFactoryPricingHelper::getModel('orders');
            $items = $modelorder->getOrderItems($order->id, self::getItemName());
            if (!is_array($items) || !count($items)) return; //no Listing items in order

            foreach ($items as $item) {
                if ($item->itemname != self::getItemName()) continue;

                $modelbalance = JTheFactoryPricingHelper::getModel('balance');

                $currency = JTheFactoryPricingHelper::getModel('currency');
                $default_currency = $currency->getDefault();
                $amount = JobsHelperPrices::convertCurrency($item->price, $item->currency, $default_currency);
                $modelbalance->increaseBalance($amount, $order->userid);
            }

        }

        function onBeforeExecuteTask(&$stopexecution)
        {
            $task = JFactory::getApplication()->input->getCmd('task', 'listjobs');
            if ($task == 'paycomission') {
                $stopexecution = true; //task is fully processed here
                $app = JFactory::getApplication();
                $user = JFactory::getUser();
                $modelbalance = JTheFactoryPricingHelper::getModel('balance');
                $balance = $modelbalance->getUserBalance();

                if ($balance->balance >= 0) {
                    JError::raiseNotice(501, JText::_("COM_JOBS_YOU_HAVE_A_POSITIVE_BALANCE"));
                    $app->redirect(JobsHelperRoute::getAddFundsRoute());
                    return;
                }

                $model = self::getModel();
                $modelorder = JTheFactoryPricingHelper::getModel('orders');
                $item = $model->getOderitemFromBalance($balance);
                $order = $modelorder->createNewOrder($item, $item->price, $item->currency, null, 'P');
                $app->redirect(JobsHelperRoute::getCheckoutRoute($order->id));
                return;
            }

        }


    }
