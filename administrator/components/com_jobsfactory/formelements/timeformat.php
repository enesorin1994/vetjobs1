<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldTimeFormat extends JFormField
{
	protected $type = 'DateFormat';
	protected function getInput()
	{
        $time_format[] = JHtml::_("select.option", 'H:i', 'H:i');
        $time_format[] = JHtml::_("select.option", 'h:iA', 'h:iA');

        $html = JHtml::_("select.genericlist",$time_format,$this->name,"",'value', 'text',$this->value);
        $html.="&nbsp;<span id='{$this->id}_span'>&nbsp;</span>";

        return $html;
	}
}
