<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldGalleryList extends JFormField
{
	protected $type = 'GalleryList';

	protected function getInput()
	{
        $galleries_plugins[] = JHtml::_('select.option', 'scrollgallery', 'Scroll Gallery');
        $galleries_plugins[] = JHtml::_('select.option', 'lytebox', 'Lytebox');
        $galleries_plugins[] = JHtml::_('select.option', 'slider', 'Picture Slider');

        return JHtml::_("select.genericlist", $galleries_plugins,$this->name,"class='selectlist'" ,'value', 'text',$this->value);
	}
}
