<?php
/**------------------------------------------------------------------------
com_jobsfactory - Jobs Factory 1.6.5
------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldDateFormat extends JFormField
{
	protected $type = 'DateFormat';
	protected function getInput()
	{
        $date_format[] = JHtml::_("select.option", 'Y-m-d', 'Y-m-d');
        $date_format[] = JHtml::_("select.option", 'm/d/Y', 'm/d/Y');
        $date_format[] = JHtml::_("select.option", 'd/m/Y', 'd/m/Y');
        $date_format[] = JHtml::_("select.option", 'd.m.Y', 'd.m.Y');
        $date_format[] = JHtml::_("select.option", 'D, F d Y', 'D, F d Y');

        $html= JHtml::_("select.genericlist",$date_format,$this->name,"" ,'value', 'text',$this->value);
        $html.="&nbsp;<span id='{$this->id}_span'>&nbsp;</span>";

        return $html;
	}
}
