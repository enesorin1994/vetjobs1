<?php
/**------------------------------------------------------------------------
com_swapfactory - Swap Factory 1.4.1
------------------------------------------------------------------------
 * @author  thePHPfactory
 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
 * @build: 17/03/2014
 * @package   : Swap
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldDecimalSymbol extends JFormField
{
	protected $type = 'DecimalSymbol';
	protected function getInput()
	{
		$cfg = JTheFactoryHelper::getConfig();
		$comma_checked='';
		$dot_checked='';
		if ((string) $this->value=='comma') $comma_checked="checked='checked'";
		if ((string) $this->value=='dot') $dot_checked="checked='checked'";

		$comma_focus = 'document.getElementById("grp").innerHTML="<strong>.</strong>" ';
		$dot_focus = 'document.getElementById("grp").innerHTML="<strong>,</strong>" ';

		$html="
            <table width='100%'>
            <tr>
                <td>
           		<label class='btn btn-default'>
                <input type='radio' value='comma' name='{$this->name}' $comma_checked onfocus='{$comma_focus}' />
                <strong style='font-size:20px;'>,</strong>
                </label>
                </td>
                <td>
                <label class='btn btn-default'>
                <input type='radio' value='dot' name='{$this->name}' $dot_checked onfocus='{$dot_focus}' />
                <strong style='font-size:20px;'>.</strong>
                </label>
                </td>

                <td>Digital grouping symbol :
                <strong><span id='grp' style='font-size:20px;'>
            ";

		if ($cfg->decimal_symbol == 'dot') {
			$html .= '<strong>,</strong>';
		}
		if ($cfg->decimal_symbol == 'comma') {
			$html .= '<strong>.</strong>';
		}

		$html .= "</span></strong></td>
            </tr>
            </table>
	    ";
		return $html;
	}
}
