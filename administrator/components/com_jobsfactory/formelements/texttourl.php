<?php
	/**-----------------------------------------------------------------------------
	 * com_swapfactory - Swap Factory 3.6.0
	 * -----------------------------------------------------------------------------
	 *
	 * @author    thePHPfactory
	 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 *            Websites: http://www.thePHPfactory.com
	 *            Technical Support: Forum - http://www.thePHPfactory.com/forum/
	 * @build     : 25/02/2015
	 * @package   : Swap Factory
	 * @subpackage: FormElements
	 **----------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');

	jimport('joomla.form.formfield');

	/**
	 * Class JFormFieldTextToUrl
	 */
	class JFormFieldTextToUrl extends JFormField
	{
		/**
		 * @var string
		 */
		protected $type = 'texttourl';

		/**
		 * getInput
		 *
		 * @return string
		 */
		protected function getInput()
		{
			$name = $this->name;
			$url = $this->element['url'];
			$url_text = $this->element['url_text'];

			$html = "<input type='text' name='" . $this->name . "' value='" . $this->value . "' />";
			$html .= " <a target='_blank' href=" . $url . ">". $url_text . "</a>";

			return $html;
		}
	}
