CREATE TABLE IF NOT EXISTS `#__jobsfactory_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(255) NOT NULL,
  `fileExt` varchar(10) NOT NULL,
  `fileType` enum('attachment','image') NOT NULL,
  `jobId` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobId` (`jobId`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_candidates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `comments` text NOT NULL,
  `cancel` int(11) DEFAULT NULL,
  `action` varchar(100) NOT NULL,
  `accept` tinyint(4) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_jobid` (`job_id`),
  KEY `ix_userid` (`userid`),
  KEY `ixaccept` (`accept`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catname` varchar(250) NOT NULL,
  `parent` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `iparent` (`parent`),
  KEY `ihash` (`hash`),
  KEY `istatus` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cityname` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ixcityname` (`cityname`),
  KEY `istatus` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_companies_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL DEFAULT '',
  `simbol` char(3) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `iname` (`name`),
  KEY `iactive` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `convert` decimal(15,5) DEFAULT NULL,
  `default` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `resume_id` int(11) NOT NULL,
  `study_level` int(4) NOT NULL,
  `edu_institution` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `ix_userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_experiencelevel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `levelname` varchar(150) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `db_name` varchar(50) NOT NULL,
  `page` varchar(100) NOT NULL,
  `ftype` varchar(150) NOT NULL,
  `compulsory` tinyint(1) NOT NULL,
  `categoryfilter` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `own_table` varchar(100) NOT NULL,
  `validate_type` varchar(100) NOT NULL,
  `css_class` varchar(100) NOT NULL,
  `style_attr` text NOT NULL,
  `search` tinyint(1) NOT NULL,
  `params` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `help` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `istatus` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_fields_assoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(50) DEFAULT NULL,
  `assoc_field` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_fields_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fid` (`fid`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_fields_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) NOT NULL,
  `option_name` varchar(255) NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ixfid` (`fid`),
  KEY `ixordering` (`ordering`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_fields_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldid` int(11) NOT NULL,
  `templatepage` varchar(100) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `params` text,
  PRIMARY KEY (`id`),
  KEY `ixfieldid` (`fieldid`),
  KEY `ixpage` (`templatepage`),
  KEY `ixpageposition` (`templatepage`,`position`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) NOT NULL DEFAULT '',
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `job_type` int(11) NOT NULL DEFAULT '0',
  `shortdescription` text,
  `description` text NOT NULL,
  `languages_request` mediumtext NOT NULL,
  `experience_request` text NOT NULL,
  `studies_request` tinyint(1) NOT NULL,
  `benefits` text NOT NULL,
  `job_cityname` varchar(255) NOT NULL,
  `job_cityid` int(11) NOT NULL,
  `job_location_state` int(11) NOT NULL,
  `job_country` INT(11) NOT NULL,
  `about_visibility` tinyint(4) NOT NULL,
  `employer_visibility` tinyint(4) NOT NULL DEFAULT '0',
  `jobuniqueID` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jobs_available` int(4) NOT NULL DEFAULT '1',
  `closed_date` date NOT NULL DEFAULT '0000-00-00',
  `close_offer` int(11) NOT NULL DEFAULT '0',
  `close_by_admin` int(11) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cat` int(11) NOT NULL DEFAULT '0',
  `has_file` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(255) NOT NULL,
  `show_candidate_nr` tinyint(2) DEFAULT NULL,
  `featured` enum('featured','none') DEFAULT 'none',
  `cancel_reason` text NOT NULL,
  `googlex` varchar(255) NOT NULL,
  `googley` varchar(255) NOT NULL,
  `approved` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `iuserid` (`userid`),
  KEY `ititle` (`title`),
  KEY `icat` (`cat`),
  KEY `idate1` (`start_date`),
  KEY `idate2` (`end_date`),
  KEY `ipublished` (`published`),
  KEY `icloseoffer` (`close_offer`),
  KEY `icloseadmin` (`close_by_admin`),
  KEY `ijobcityid` (`job_cityid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_jobtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typename` varchar(150) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locname` varchar(250) NOT NULL,
  `country` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ihash` (`hash`),
  KEY `istatus` (`status`),
  KEY `icountry` (`country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priority` enum('log','notice','warning','error','system') DEFAULT 'log',
  `event` varchar(50) DEFAULT NULL,
  `logtime` datetime DEFAULT NULL,
  `log` text,
  PRIMARY KEY (`id`),
  KEY `ixeventtype` (`event`),
  KEY `ixdate` (`logtime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_type` varchar(250) DEFAULT NULL,
  `group` enum('candidate','company','admin','watchlist','job') DEFAULT 'company',
  `content` text,
  `subject` varchar(250) DEFAULT NULL,
  `enabled` int(1) unsigned zerofill DEFAULT '0',
  `ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ixmailtype` (`mail_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL DEFAULT '0',
  `userid1` int(11) NOT NULL DEFAULT '0',
  `userid2` int(11) NOT NULL DEFAULT '0',
  `parent_message` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `app_id` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `wasread` int(1) DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `iuserid1` (`userid1`),
  KEY `iuserid2` (`userid2`),
  KEY `iparent` (`parent_message`),
  KEY `iapp` (`app_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_payment_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `balance` decimal(11,2) DEFAULT NULL,
  `req_withdraw` decimal(11,0) NOT NULL DEFAULT '0' COMMENT 'Requested withdraw amount',
  `last_withdraw_date` date NOT NULL,
  `paid_withdraw_date` date NOT NULL,
  `withdrawn_until_now` decimal(11,0) NOT NULL DEFAULT '0' COMMENT 'Withdraw until now',
  `currency` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ixuserid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_payment_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `currency` varchar(11) DEFAULT NULL,
  `refnumber` varchar(100) DEFAULT NULL,
  `invoice` varchar(50) DEFAULT NULL,
  `ipn_response` text,
  `ipn_ip` varchar(100) DEFAULT NULL,
  `status` enum('ok','error','manual_check','cancelled','refunded') DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `orderid` int(11) NOT NULL,
  `gatewayid` int(11) DEFAULT NULL,
  `payment_method` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ixdate` (`date`),
  KEY `ixuserid` (`userid`),
  KEY `ixstatus` (`status`),
  KEY `ixref` (`refnumber`),
  KEY `ixinvoice` (`invoice`),
  KEY `ixobjectid` (`orderid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_payment_orderitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) DEFAULT NULL,
  `itemname` varchar(30) DEFAULT NULL,
  `itemdetails` varchar(250) DEFAULT NULL,
  `iteminfo` varchar(150) DEFAULT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `quantity` int(11) DEFAULT '1',
  `params` text,
  PRIMARY KEY (`id`),
  KEY `ixorderid` (`orderid`),
  KEY `ixiteminfo` (`iteminfo`),
  KEY `ixitemname` (`itemname`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_payment_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderdate` datetime DEFAULT NULL,
  `modifydate` datetime DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `order_total` decimal(11,2) DEFAULT NULL,
  `order_currency` varchar(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `paylogid` int(11) DEFAULT NULL,
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_paysystems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paysystem` varchar(50) DEFAULT NULL,
  `classname` varchar(50) DEFAULT NULL,
  `enabled` int(1) DEFAULT '1',
  `params` text,
  `ordering` int(11) DEFAULT NULL,
  `isdefault` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_pricing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemname` varchar(50) DEFAULT NULL,
  `pricetype` enum('percent','fixed') DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` decimal(9,2) DEFAULT NULL,
  `currency` varchar(11) DEFAULT NULL,
  `enabled` int(1) DEFAULT NULL,
  `params` text,
  `ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ixitemname` (`itemname`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_pricing_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL,
  `price` decimal(11,2) NOT NULL,
  `itemname` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `icat` (`category`),
  KEY `ipriceitem` (`itemname`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_pricing_comissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `app_id` int(11) NOT NULL,
  `comission_date` datetime DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `currency` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ixuserid` (`userid`),
  KEY `ixjobid` (`job_id`),
  KEY `ixappid` (`app_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_pricing_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `purchase_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ixuserid` (`userid`),
  KEY `ixcontactid` (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT 'gives rating',
  `rated_userid` int(11) NOT NULL,
  `rating_user` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `rated_userid` (`rated_userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS  `#__jobsfactory_report_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `message` varchar(200) NOT NULL DEFAULT '',
  `processing` int(11) NOT NULL DEFAULT '0',
  `solved` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `ixuserid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_studieslevel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `levelname` varchar(150) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `tagname` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ixjobs` (`job_id`),
  KEY `ixtags` (`tagname`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_userexperience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `salary` int(11) NOT NULL,
  `city` varchar(150) NOT NULL,
  `country` int(11) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `exp_description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ixtags` (`company`),
  KEY `ixjobs` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `isCompany` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `surname` varchar(50) NOT NULL DEFAULT '',
  `birth_date` datetime NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `marital_status` varchar(200) NOT NULL,
  `address` varchar(150) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(150) NOT NULL DEFAULT '',
  `phone` text NOT NULL,
  `email` varchar(150) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `googleMaps_x` varchar(255) DEFAULT NULL,
  `googleMaps_y` varchar(255) DEFAULT NULL,
  `shortdescription` text NOT NULL,
  `about_us` text NOT NULL,
  `website` varchar(200) DEFAULT NULL,
  `activitydomain` varchar(255) DEFAULT NULL,
  `contactname` varchar(100) NOT NULL,
  `contactposition` varchar(255) NOT NULL,
  `contactphone` text NOT NULL,
  `contactemail` varchar(255) NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `YM` varchar(255) DEFAULT NULL,
  `Hotmail` varchar(255) DEFAULT NULL,
  `Skype` varchar(255) DEFAULT NULL,
  `linkedIN` text NOT NULL,
  `total_experience` int(4) NOT NULL,
  `desired_salary` int(4) NOT NULL,
  `user_achievements` text NOT NULL,
  `user_goals` text NOT NULL,
  `user_hobbies` text NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `verified` int(1) DEFAULT '0',
  `powerseller` int(1) DEFAULT '0',
  `isVisible` tinyint(1) NOT NULL,
  `useruniqueid` varchar(255) NOT NULL,
  `cv_notified` INT(1) NULL DEFAULT '0',
  `cv_date_notified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `ixname` (`name`),
  KEY `ixsurname` (`surname`),
  KEY `ixcity` (`city`),
  KEY `ixcountry` (`country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_users_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT 'rated user',
  `user_rating` decimal(5,2) NOT NULL,
  `users_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_watchlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `job_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ixjobid` (`job_id`),
  KEY `ixuserid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_watchlist_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ixcatid` (`catid`),
  KEY `ixuserid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_watchlist_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `cityid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ixcatid` (`cityid`),
  KEY `ixuserid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_watchlist_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `companyid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ixcatid` (`companyid`),
  KEY `ixuserid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jobsfactory_watchlist_loc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `locationid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ixcatid` (`locationid`),
  KEY `ixuserid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;