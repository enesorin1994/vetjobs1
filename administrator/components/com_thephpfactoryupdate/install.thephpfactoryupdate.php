<?php
    /**------------------------------------------------------------------------
   	------------------------------------------------------------------------
   	 * @author    thePHPfactory
   	 * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
   	 * @license   - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
   	 * Websites: http://www.thePHPfactory.com
   	 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
   	 * @package   : Updates
   	-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');
class COM_ThePHPFactoryUpdateInstallerScript
{
	public function postflight($route, $adapter)
	{
		$message=(string)$adapter->manifest->thefactorymessage;
		$app=JFactory::getApplication();
		$app->enqueueMessage($message,'warning');
	}
}
