<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

class AdvertisementFactoryTablePayment extends JTable
{
    var $id = null;
    var $received_at = null;
    var $payment_date = null;
    var $user_id = null;
    var $amount = null;
    var $currency = null;
    var $order_id = null;
    var $status = null;
    var $gateway = null;
    var $log = null;
    var $refnumber = null;
    var $errors = null;

    public function __construct(&$db)
    {
        parent::__construct('#__advertisementfactory_payments', 'id', $db);
    }

    public function store($updateNulls = false)
    {
        if (!parent::store($updateNulls)) {
            return false;
        }

        if ('ok' == $this->status) {
            $order = JTable::getInstance('Order', 'AdvertisementFactoryTable');
            $ad = JTable::getInstance('Ad', 'AdvertisementFactoryTable');

            $order->load($this->order_id);
            $ad->load($order->ad_id);

            $ad->updateFromPrice(new JRegistry($order->price_saved), $this->id);
        }

        return true;
    }
}
