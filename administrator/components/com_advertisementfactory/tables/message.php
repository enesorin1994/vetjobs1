<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

class AdvertisementFactoryTableMessage extends JTable
{
    public function __construct(&$db)
    {
        parent::__construct('#__advertisementfactory_messages', 'id', $db);
    }

    public function store($updateNulls = false)
    {
        if (!$this->id) {
            $this->user_id = JFactory::getUser()->id;
            $this->created_at = JFactory::getDate()->toSql();
        }

        return parent::store($updateNulls);
    }
}
