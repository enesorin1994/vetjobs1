<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

class AdvertisementFactoryTableAd extends FactoryTable
{
    var $id = null;
    var $type = null;
    var $name = null;
    var $title = null;
    var $contents = null;
    var $keywords = null;
    var $description = null;
    var $url = null;
    var $filename = null;
    var $module_id = null;
    var $user_id = null;
    var $params = null;
    var $reason = null;
    var $history = null;
    var $price_type = null;
    var $purchased = null;
    var $served = null;
    var $publish_up = null;
    var $publish_down = null;
    var $paid = null;
    var $state = null;

    public function __construct(&$db)
    {
        parent::__construct('#__advertisementfactory_ads', 'id', $db);
    }

    public function publish($pks = null, $state = 1, $userId = 0)
    {
        // Initialise variables.
        $k = $this->_tbl_key;

        // Sanitize input.
        JArrayHelper::toInteger($pks);
        $userId = (int)$userId;
        $state = (int)$state;

        // If there are no primary keys set check to see if the instance key is set.
        if (empty($pks)) {
            if ($this->$k) {
                $pks = array($this->$k);
            } // Nothing to set publishing state on, return false.
            else {
                $this->setError(JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
                return false;
            }
        }

        // Get an instance of the table
        $table = JTable::getInstance('Ad', 'AdvertisementFactoryTable');

        // For all keys
        foreach ($pks as $pk) {
            // Load the banner
            if (!$table->load($pk)) {
                $this->setError($table->getError());
            }

            // Change the state
            $table->state = $state;

            // Store the row
            if (!$table->store()) {
                $this->setError($table->getError());
            }
        }
        return count($this->getErrors()) == 0;
    }

    public function hit($type = 'impression')
    {
        $table = JTable::getInstance('Track', 'AdvertisementFactoryTable');
        $data = array(
            'ad_id'   => $this->id,
            'user_id' => JFactory::getUser()->id,
            'ip'      => JFactory::getApplication()->input->server->get('REMOTE_ADDR'),
            'type'    => $type,
        );

        if ($table->mark($data)) {
            $this->served++;
        }

        // Check if the ad expired.
        if ($this->served == $this->purchased) {
            $this->state = 2;
            $this->updateHistory();
        }

        return parent::store(false);
    }

    public function check()
    {
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        $lang = JFactory::getLanguage();
        $lang->load('com_advertisementfactory', JPATH_ADMINISTRATOR);

        switch ($this->type) {
            case 'keyword':
                // Check for banned keywords
                $banned = explode("\n", $component->params->get('keywords_banned'));

                foreach ($banned as &$keyword) {
                    $keyword = trim($keyword);
                }

                if (in_array($this->keywords, $banned)) {
                    $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_AD_CHECK_ERRROR_BANNED_KEYWORD'));
                    return false;
                }

                // Check for minimum letters
                $min_length = $component->params->get('keywords_min_length', 3);
                if (strlen($this->keywords) < $min_length) {
                    $this->setError(JText::sprintf('COM_ADVERTISEMENTFACTOR_AD_CHECK_ERROR_KEYWORD_MIN_LENGTH', $min_length));
                    return false;
                }
                break;

            case 'thumbnail':
            case 'banner':
            case 'modal':
            case 'fullpage':
            case 'popup':
                if (!$this->processFile()) {
                    return false;
                }

                // Check image size
                $path = $this->getFilePath();

                // No image to check or image is set to be removed
                if (JFile::exists($path)) {
                    // Get image size
                    $size = getimagesize($path);

                    // Get image required size
                    $requiremets = $this->getImageRequirements();

                    if (!$this->checkImageSize($size, $requiremets)) {
                        return false;
                    }
                }
                break;
        }

        return parent::check();
    }

    public function store($updateNulls = false)
    {
        // Remove file if ad type doesn't require a file
        if (in_array($this->type, array('link', 'keyword'))) {
            $this->removeFile();
        }

        // Update history
        if ($this->id) {
            $this->updateHistory();
        }

        // No need to save the reason
        unset($this->reason);

        $this->processAd();

        return parent::store($updateNulls);
    }

    protected function processAd()
    {
        switch ($this->type) {
            case 'article':
                // Get the article
                $params = $this->params instanceof JRegistry ? $this->params : new JRegistry($this->params);
                $content = JTable::getInstance('Content');
                $content->load($params->get('article_id', 0));

                if (1 != $this->state && !$content->id) {
                    return true;
                }

                $content->title = $this->title;
                $content->introtext = nl2br($this->contents);
                //$content->fulltext = $this->contents;
                $content->catid = $params->get('category', 0);
                $content->language = '*';
                $content->state = 0;
                $content->access = 1;

                // Check if the article is published
                if (1 == $this->state) {
                    $content->state = 1;
                    $content->publish_up = $this->publish_up;
                    $content->publish_down = $this->publish_down;
                } else {
                    $content->state = 0;
                }

                $content->check();
                $content->store();

                $params->set('article_id', $content->id);
                $this->params = $params->toString();
                break;
        }

        return true;
    }

    public function load($keys = null, $reset = true)
    {
        if (!parent::load($keys, $reset)) {
            return false;
        }

        if (isset($this->params)) {
            $params = new JRegistry($this->params);
            $this->params = $params;
        }

        return true;
    }

    public function bind($array, $ignore = '')
    {
        if (is_array($array) && isset($array['params']) && is_array($array['params'])) {
            $params = new JRegistry();
            $params->loadArray($array['params']);
            $array['params'] = (string)$params;
        }

        if (!parent::bind($array, $ignore)) {
            return false;
        }

        // Format the url
        if (!empty($this->url) && (strpos($this->url, 'http://') !== 0 && strpos($this->url, 'https://') !== 0)) {
            $this->url = 'http://' . $this->url;
        }

        return true;
    }

    public function delete($pk = null)
    {
        if (!parent::delete($pk)) {
            return false;
        }

        $this->removeFile();
        $this->removeTrackData();

        return true;
    }

    public function getFileSource()
    {
        return $this->getFileBaseSource() . $this->filename;
    }

    public function getFilePath()
    {
        return $this->getFileBasePath() . DS . $this->filename;
    }

    public function getImageRequirements()
    {
        switch ($this->type) {
            case 'thumbnail':
            case 'banner':
                $module = JTable::getInstance('Module');
                $module->load($this->module_id);

                jimport('joomla.html.parameter');
                $parameters = new JRegistry($module->params);

                $width = (int)$parameters->get('width', 0);
                $height = (int)$parameters->get('height', 0);
                $force_size = (bool)$parameters->get('force_size', false);
                break;

            case 'fullpage':
                $component = JComponentHelper::getComponent('com_advertisementfactory');

                $width = (int)$component->params->get('fullpageads_image_width', 0);
                $height = (int)$component->params->get('fullpageads_image_height', 0);
                $force_size = true;
                break;

            case 'modal':
                $component = JComponentHelper::getComponent('com_advertisementfactory');

                $width = (int)$component->params->get('modalads_image_width', 0);
                $height = (int)$component->params->get('modalads_image_height', 0);
                $force_size = true;
                break;

            case 'popup':
                $component = JComponentHelper::getComponent('com_advertisementfactory');

                $width = (int)$component->params->get('popupads_image_width', 0);
                $height = (int)$component->params->get('popupads_image_height', 0);
                $force_size = true;
                break;
        }

        return array(
            'width'      => $width,
            'height'     => $height,
            'force_size' => $force_size,
        );
    }

    public function updateFromPrice($price, $payment_id)
    {
        $settings = JComponentHelper::getParams('com_advertisementfactory');

        $this->paid = 1;
        $this->state = $settings->get('enable_auto_approve', 0) ? 1 : 4;

        $this->payment_id = $payment_id;
        $this->purchased = $price->get('value');
        $this->served = 0;
        $this->price_type = $price->get('price_type');

        if (3 == $price->get('price_type')) {
            $purchaseTypes = JModelLegacy::getInstance('PurchaseTypes', 'AdvertisementFactoryModel');
            $module = JTable::getInstance('Module');

            $module->load($price->get('module_id'));

            $availability = $purchaseTypes->getAvailabilityForModule($price->get('module_id'), $module->params);
            $availability = is_null($availability) ? 'now' : $availability;

            $dateUp = JFactory::getDate($availability);
            $dateDown = JFactory::getDate($availability . '+' . $price->get('value') . ' months');

            $this->publish_up = $dateUp->toSql();
            $this->publish_down = $dateDown->toSql();
        } else {
            $dbo = JFactory::getDbo();

            $this->publish_up = $dbo->getNullDate();
            $this->publish_down = $dbo->getNullDate();
        }

        $this->updateHistory();
        $this->processAd();

        // Alert admin new ad was published
        $this->alertAdmin();

        return parent::store(false);
    }

    protected function updateHistory()
    {
        // Check if we're changing the state
        $query = $this->_db->getQuery(true)
            ->select('state, history')
            ->from($this->_tbl)
            ->where($this->_tbl_key . ' = ' . $this->id);
        $this->_db->setQuery($query);
        $existing = $this->_db->loadObject();

        if ($existing->state == $this->state) {
            return false;
        }

        return $this->appendToHistory($this->state, $this->reason, false, $existing->history);
    }

    public function appendToHistory($state, $reason = null, $return = false, $history = null)
    {
        $date = JFactory::getDate();
        $states = array(
            0 => 'COM_ADVERTISEMENTFACTORY_AD_STATE_CHANGED_UNPUBLISHED',
            1 => 'COM_ADVERTISEMENTFACTORY_AD_STATE_CHANGED_PUBLISHED',
            2 => 'COM_ADVERTISEMENTFACTORY_AD_STATE_CHANGED_EXPIRED',
            3 => 'COM_ADVERTISEMENTFACTORY_AD_STATE_CHANGED_REJECTED',
            4 => 'COM_ADVERTISEMENTFACTORY_AD_STATE_CHANGED_PENDING',
        );

        $update = '<b>' . $date . '</b>' . "\n"
            . $states[$state] . "\n"
            . ($reason ? '<i>"' . $reason . '"</i>' . "\n" : '')
            . "\n\n";

        if ($return) {
            return $update;
        }

        $this->history = $update . (is_null($history) ? $this->history : $history);

        return true;
    }

    protected function getFileBasePath()
    {
        return JPATH_SITE . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'storage' . DS . 'images';
    }

    protected function getFileBaseSource()
    {
        return JURI::root() . 'components/com_advertisementfactory/storage/images/';
    }

    protected function processFile()
    {
        // Delete current thumbnail?
        $delete = $this->deleteCurrentImage();

        if ($delete) {
            $this->removeFile();
        }

        $files = JFactory::getApplication()->input->files->get('jform', array(), 'array');

        $array = array();
        foreach ($files as $file => $params) {
            foreach ($params as $key => $val) {
                $array[$key][$file] = $val;
            }
        }

        $files = $array;

        if (!$files) {
            return true;
        }

        // Check if a file was uploaded
        if ($files['error']['filename'] === 4) {
            return true;
        }

        // Check if there are any errors
        if ($files['error']['filename'] !== 0) {
            $this->setError(JText::sprintf('COM_ADVERTISEMENTFACTORY_AD_FILE_SAVE_ERROR_ERROR_NO', $files['error']['filename']));
            return true;
        }

        // Check if file is an image
        $size = getimagesize($files['tmp_name']['filename']);
        if (!$size) {
            $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_AD_FILE_SAVE_ERROR_NOT_AN_IMAGE'));
            return false;
        }

        // Check if user folder exists
        $folder = $this->getFileBasePath();
        if (!JFolder::exists($folder)) {
            JFolder::create($folder);
        }

        // Get image requirements
        $requiremets = $this->getImageRequirements();

        // All it's ok, upload the image
        $name = strtolower(JFilterOutput::stringURLSafe(JFile::stripExt($files['name']['filename'])));
        $ext = strtolower(JFilterOutput::stringURLSafe(JFile::getExt($files['name']['filename'])));
        $name = (strlen($name) > 10 ? substr($name, 0, 10) : $name) . '-' . time() . '.' . $ext;

        $src = $files['tmp_name']['filename'];
        $dest = $folder . DS . $name;

        if ($requiremets['force_size']) {
            if (!$this->checkImageSize($size, $requiremets)) {
                return false;
            }

            JFile::upload($src, $dest);
        } elseif (!$requiremets['width'] || !$requiremets['height']) {
            JFile::upload($src, $dest);
        } else {
            require_once(JPATH_COMPONENT_SITE . DS . 'lib' . DS . 'resize' . DS . 'resizeIMG.php');

            $resize = new ResizeImage($src);
            $resize->resize_scale($requiremets['width'], $requiremets['height'], $dest, false);

            if (!$resize->close()) {
                $this->setError(JText::_('COM_ADVERTISEMENTFACTORY_AD_FILE_SAVE_ERROR_IMAGE_RESIZE'));
                return false;
            }
        }

        $this->removeFile();
        $this->filename = $name;

        return true;
    }

    protected function removeFile()
    {
        $path = $this->getFilePath();

        if (JFile::exists($path)) {
            JFile::delete($path);
        }

        $this->filename = '';

        return true;
    }

    protected function removeTrackData()
    {
        $dbo = $this->getDbo();

        $query = $dbo->getQuery(true)
            ->delete()
            ->from('#__advertisementfactory_track')
            ->where('ad_id = ' . $dbo->quote($this->id));

        $result = $dbo->setQuery($query)
            ->execute();

        return $result;
    }

    protected function checkImageSize($size, $requirements)
    {
        if (!$requirements['force_size']) {
            return true;
        }

        if ($size[0] != $requirements['width'] || $size[1] != $requirements['height']) {
            $this->setError(JText::sprintf('COM_ADVERTISEMENTFACTORY_AD_FILE_SAVE_ERROR_IMAGE_NOT_CORRECT_SIZE', $requirements['width'], $requirements['height']));
            return false;
        }

        return true;
    }

    protected function deleteCurrentImage()
    {
        return JFactory::getApplication()->input->getInt('adfile_delete', 0);
    }

    public function getUrl()
    {
        $preview = $this->isPreviewMode() ? '&preview=1' : '';

        return JRoute::_('index.php?option=com_advertisementfactory&task=ad.url&id=' . $this->id . $preview);
    }

    protected function isPreviewMode()
    {
        $view = JFactory::getApplication()->input->getCmd('view');
        $option = JFactory::getApplication()->input->getCmd('option');

        return 'previsualize' == $view && 'com_advertisementfactory' == $option;
    }

    protected function alertAdmin()
    {
        // Initialise variables.
        $settings = JComponentHelper::getParams('com_advertisementfactory');
        $groups = $settings->get('admin_alert_group', array());

        // Check if any groups are set to receive alerts.
        if (!$groups) {
            return true;
        }

        // Get users from the groups.
        $users = array();
        foreach ($groups as $group) {
            $array = JAccess::getUsersByGroup($group);
            $users = array_merge($users, $array);
        }

        // Check if any users are in the groups.
        if (!$users) {
            return true;
        }

        // Load the language
        $language = JFactory::getLanguage();
        $language->load('com_advertisementfactory', JPATH_ADMINISTRATOR);

        // Send the mail
        $mailer = JFactory::getMailer();

        $link = JURI::root() . 'administrator/index.php?option=com_advertisementfactory&task=ad.edit&id=' . $this->id;

        $mailer->setSubject(JText::_('COM_ADVERTISEMENTFACTORY_ADMIN_ALERT_SUBJECT'));
        $mailer->setBody(JText::sprintf('COM_ADVERTISEMENTFACTORY_ADMIN_ALERT_BODY', $link));
        $mailer->IsHTML(true);

        foreach ($users as $user) {
            $mailer->addRecipient(JFactory::getUser($user)->email);
        }

        return $mailer->send();
    }
}
