<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

class AdvertisementFactoryTableGateway extends JTable
{
    var $id = null;
    var $title = null;
    var $params = null;
    var $logo = null;
    var $enabled = null;

    public function __construct(&$db)
    {
        parent::__construct('#__advertisementfactory_gateways', 'id', $db);
    }

    public function bind($src, $ignore = array())
    {
        if (isset($src['params']) && is_array($src['params'])) {
            $registry = new JRegistry();
            $registry->loadArray($src['params']);
            $src['params'] = (string)$registry;
        }

        return parent::bind($src, $ignore);
    }
}
