<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

class AdvertisementFactoryTableTrack extends JTable
{
    public function __construct(&$db)
    {
        parent::__construct('#__advertisementfactory_track', 'id', $db);
    }

    public function mark($data)
    {
        // Initialise variables.
        $dbo = $this->getDbo();
        $settings = JComponentHelper::getParams('com_advertisementfactory');
        $interval = $settings->get('interval_' . $data['type'], 60);
        $date = JFactory::getDate('- ' . $interval . ' minutes');

        $query = $dbo->getQuery(true)
            ->select('t.id')
            ->from('#__advertisementfactory_track t')
            ->where('t.ad_id = ' . $dbo->quote($data['ad_id']))
            ->where('t.created_at > ' . $dbo->quote($date->toSql()))
            ->where('t.type = ' . $dbo->quote($data['type']))
            ->where('t.user_id = ' . $dbo->quote($data['user_id']));

        if (!$data['user_id']) {
            $query->where('t.ip = ' . $dbo->quote($data['ip']));
        }

        $result = $dbo->setQuery($query)
            ->loadResult();

        if (!$result) {
            $table = JTable::getInstance('Track', 'AdvertisementFactoryTable');
            $table->save($data);

            return true;
        }

        return false;
    }

    public function check()
    {
        if (!parent::check()) {
            return false;
        }

        if (is_null($this->created_at)) {
            $this->created_at = JFactory::getDate()->toSql();
        }

        if (is_null($this->referer)) {
            $session = JFactory::getSession();
            $referer = JFactory::getApplication()->input->server->getString('HTTP_REFERER');
            $this->referer = $session->get('com_advertisementfactory.url', $referer);
        }

        if (is_null($this->url)) {
            $this->url = JUri::getInstance()->toString();
        }

        return true;
    }
}
