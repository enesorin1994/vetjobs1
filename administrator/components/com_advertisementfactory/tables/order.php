<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

class AdvertisementFactoryTableOrder extends JTable
{
    var $id = null;
    var $title = null;
    var $user_id = null;
    var $ad_id = null;
    var $price_id = null;
    var $price_saved = null;
    var $params = null;
    var $amount = null;
    var $currency = null;
    var $gateway = null;
    var $status = null;

    public function __construct(&$db)
    {
        parent::__construct('#__advertisementfactory_orders', 'id', $db);
    }
}
