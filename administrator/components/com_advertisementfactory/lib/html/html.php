<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

abstract class JHtmlAdvertisementFactory
{
    public static function state($value, $i, $prefix = '', $enabled = true, $checkbox = 'cb', $publish_up = null, $publish_down = null)
    {
        if (is_array($prefix)) {
            $options = $prefix;
            $enabled = array_key_exists('enabled', $options) ? $options['enabled'] : $enabled;
            $checkbox = array_key_exists('checkbox', $options) ? $options['checkbox'] : $checkbox;
            $prefix = array_key_exists('prefix', $options) ? $options['prefix'] : '';
        }

        $states = array(
            0 => array('publish', 'Publish', 'Publish', 'Publish', false, 'unpublish', 'unpublish'),
            1 => array('unpublish', 'Unpublish', 'Unpublish', 'Unpublish', false, 'publish', 'publish'),
            2 => array('publish', 'Publish', 'Publish', 'Publish', false, 'expired', 'expired'),
            3 => array('publish', 'Publish', 'Publish', 'Publish', false, 'deny', 'deny'),
            4 => array('publish', 'Publish', 'Publish', 'Publish', false, 'pending', 'pending'),
            5 => array('unpublish', 'Unpublish', 'Unpublish', 'Unpublish', false, 'apply', 'apply'),
        );

        return JHtml::_('jgrid.state', $states, $value, $i, $prefix, $enabled, true, $checkbox);
    }

    public static function paid($value)
    {
        $states = array(
            0 => 'disabled.png',      // not paid
            1 => 'featured.png',      // paid
            2 => 'icon-16-clear.png', // refunded
        );

        $html = '<img src="' . JURI::root() . 'media/com_advertisementfactory/assets/frontend/images/' . $states[$value] . '" />';

        return $html;
    }
}
