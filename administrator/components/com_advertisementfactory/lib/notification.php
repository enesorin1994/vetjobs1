<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

class FactoryNotification
{
    protected $option;
    protected $xml;

    public function __construct($config = array())
    {
        if (isset($config['option'])) {
            $this->option = $config['option'];
        } else {
            $this->option = JFactory::getApplication()->input->getCmd('option');
        }

        $this->xml = simplexml_load_file(JPATH_ADMINISTRATOR . DS . 'components' . DS . $this->option . DS . 'notifications.xml');
    }

    public static function getInstance($config = array())
    {
        static $instance = null;

        if (is_null($instance)) {
            $instance = new self($config);
        }

        return $instance;
    }

    public function getTypes()
    {
        $options = array();

        foreach ($this->xml->notification as $notification) {
            $options[(string)$notification->attributes()->type] = JText::_((string)$notification->attributes()->label);
        }

        return $options;
    }

    public function getTokens($notification)
    {
        $options = array();
        $notification = $this->xml->xpath('//notifications //notification[@type="' . $notification . '"]');

        if (!$notification) {
            return $options;
        }

        foreach ($notification[0]->option as $option) {
            $options[(string)$option->attributes()->value] = JText::_((string)$option);
        }

        return $options;
    }

    public function sendNotification($type, $receivers, $variables = array())
    {
        foreach ($receivers as $receiver) {
            $user = JFactory::getUser($receiver);
            $receiverLanguage = $user->getParam('language', JComponentHelper::getParams('com_languages')->get('site'));
            $notification = $this->getNotification($type, $receiverLanguage);

            // Check if notification was found.
            if (!$notification) {
                return false;
            }

            $app = JFactory::getApplication();
            $tokens = array_keys($this->getTokens($type));
            $receiverEmail = $user->email;

            $variables['receiver_username'] = $user->username;

            // Prepare subject and body.
            $subject = $this->parseTokens($notification->subject, $tokens, $variables);
            $body = $this->parseTokens($notification->body, $tokens, $variables);

            // Send mail.
            $mailer = JFactory::getMailer();

            $mailer->setSubject($subject);
            $mailer->setBody($body);
            $mailer->addRecipient($receiverEmail);
            $mailer->setSender(array($app->get('mailfrom'), $app->get('fromname')));
            $mailer->isHtml(true);

            $mailer->send();
        }

        return true;
    }

    protected function getNotification($type, $receiverLanguage)
    {
        $dbo = JFactory::getDbo();
        $table = '#__' . str_replace('com_', '', $this->option) . '_notifications';

        $query = $dbo->getQuery(true)
            ->select('n.*')
            ->from($dbo->quoteName($table) . ' n')
            ->where('n.type = ' . $dbo->quote($type))
            ->where('n.published = ' . $dbo->quote(1))
            ->where('(n.lang_code = ' . $dbo->quote($receiverLanguage) . ' OR n.lang_code = ' . $dbo->quote('*') . ')');
        $notifications = $dbo->setQuery($query)
            ->loadObjectList('lang_code');

        if (!$notifications) {
            return false;
        }

        return isset($notifications[$receiverLanguage]) ? $notifications[$receiverLanguage] : $notifications['*'];
    }

    protected function parseTokens($string, $search, $variables)
    {
        $replace = array();

        // Prepare variables.
        foreach ($search as &$variable) {
            $replace[] = isset($variables[$variable]) ? $variables[$variable] : $variable;

            $variable = '%%' . $variable . '%%';
        }

        // Replace variables.
        $string = str_replace($search, $replace, $string);

        // Replace image sources.
        $string = str_replace('src="', 'src="' . JURI::root(), $string);

        return $string;
    }
}
