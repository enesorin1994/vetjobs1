<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

class com_AdvertisementFactoryInstallerScript
{
    private $component = 'com_advertisementfactory';

    public function install($parent)
    {
        return true;
    }

    public function uninstall($parent)
    {
        return true;
    }

    public function update($parent)
    {
        return true;
    }

    public function preflight($type, $parent)
    {
        return true;
    }

    public function postflight($type, $parent)
    {
        $this->createMenus();
        $this->setDefaultSettings();
        $this->setDefaultPermissions();
    }

    protected function setDefaultSettings()
    {
        $settings = '{"backend_preview_token":"' . md5(date('YmdHis')) . '","currency":"EUR","enabled_ad_types":["link","thumbnail","banner","modal","fullpage","keyword"],"available_ad_types":["link","thumbnail","banner","modal","fullpage","keyword"],"fullpageads_image_width":"400","fullpageads_image_height":"200","fullpageads_displays_per_session":"3","fullpageads_clicks_to_display":"10","fullpageads_seconds_to_display":"10","fullpageads_displays_per_session_guests":"10","fullpageads_clicks_to_display_guests":"3","fullpageads_seconds_to_display_guests":"20","modalads_window_width":"600","modalads_window_height":"500","modalads_image_width":"400","modalads_image_height":"200","modalads_displays_per_session":"3","modalads_clicks_to_display":"10","modalads_seconds_to_display":"20","modalads_displays_per_session_guests":"3","modalads_clicks_to_display_guests":"10","modalads_seconds_to_display_guests":"20","keywords_min_length":"3","keywords_banned":""}';

        $extension = JTable::getInstance('Extension');
        $component_id = $extension->find(array('type' => 'component', 'element' => $this->component));

        $extension->load($component_id);
        $extension->params = $settings;

        return $extension->store();
    }

    protected function setDefaultPermissions()
    {
        // Initialise
        $settings = JComponentHelper::getComponent('com_users');
        $asset = JTable::getInstance('Asset', 'JTable');

        // Get Registered users group
        $new_usertype = $settings->params->get('guest_usergroup', 1);

        // Get Administrator users groups
        $adminGroups = $this->getAdministratorGroups();

        $rules = array(
            'frontend.manage' => array($new_usertype => 1),
            'backend.manage'  => $adminGroups,
        );

        $asset->loadByName($this->component);
        $asset->rules = json_encode($rules);

        return $asset->store();
    }

    protected function createMenus()
    {
        $menus = array(
            'menu' => array(
                'menutype'    => 'advertisement-factory',
                'title'       => 'Advertisement Factory Menu',
                'description' => 'Advertisement Factory Menu',
                'access'      => 2,

                'items' => array(
                    'ads' => array(
                        'title'  => 'My Ads',
                        'view'   => 'ads',
                        'access' => 2,
                    ),

                    'purchasetypes' => array(
                        'title'  => 'Available Advertisements',
                        'view'   => 'purchasetypes',
                        'access' => 2,
                    ),

                    'previsualize' => array(
                        'title'  => 'Advertisement Zones',
                        'view'   => 'previsualize',
                        'params' => '&modules=1',
                        'access' => 2,
                    ),
                ),
            ),
        );

        foreach ($menus as $menu) {
            $this->createMenu($menu);
        }
    }

    protected function createMenu($menu)
    {
        // Check if menu exists
        if ($this->menuExists($menu['menutype'])) {
            return false;
        }

        $this->createMenuType($menu['menutype'], $menu['title'], $menu['description']);

        $this->createMenuModule($menu['title'], $menu['menutype'], $menu['access']);

        $this->createMenuItems($menu['items'], $menu['menutype']);
    }

    protected function menuExists($menutype)
    {
        $dbo = JFactory::getDbo();

        $query = $dbo->getQuery(true)
            ->select('m.id')
            ->from('#__menu_types m')
            ->where('m.menutype = "' . $menutype . '"');
        $dbo->setQuery($query);

        return $dbo->loadResult();
    }

    protected function createMenuType($menutype, $title, $description)
    {
        $menu = JTable::getInstance('MenuType');

        $menu->menutype = $menutype;
        $menu->title = $title;
        $menu->description = $description;

        $menu->store();
    }

    protected function createMenuModule($title, $menutype, $access)
    {
        $module = JTable::getInstance('Module');

        $module->title = $title;
        $module->position = 'position-7';
        $module->published = 1;
        $module->module = 'mod_menu';
        $module->access = $access;
        $module->showtitle = 1;
        $module->params = '{"menutype":"' . $menutype . '","startLevel":"1","endLevel":"0","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid"}';
        $module->client_id = 0;
        $module->language = 'en-GB';

        $module->store();

        $dbo = JFactory::getDbo();

        $dbo->setQuery('INSERT INTO `#__modules_menu` (moduleid, menuid) VALUES (' . $module->id . ', 0)');
        $dbo->execute();
    }

    protected function createMenuItems($items, $menutype)
    {
        $extension = JTable::getInstance('Extension');
        $component_id = $extension->find(array('type' => 'component', 'element' => $this->component));

        $items = array_reverse($items, true);

        foreach ($items as $item) {
            $this->createMenuItem($item, $component_id, $menutype);
        }
    }

    protected function createMenuItem($menuItem, $component_id, $menutype)
    {
        $item = JTable::getInstance('Menu');

        $link = isset($menuItem['link']) ? $menuItem['link'] : 'index.php?option=' . $this->component . '&view=' . $menuItem['view'];
        $link .= isset($menuItem['params']) ? $menuItem['params'] : '';

        $item->menutype = $menutype;
        $item->type = 'component';
        $item->published = 1;
        $item->client_id = 0;
        $item->level = 1;
        $item->parent_id = 1;
        $item->component_id = $component_id;
        $item->title = $menuItem['title'];
        $item->alias = JFilterOutput::stringURLSafe($menuItem['title']);
        $item->link = $link;
        $item->access = $menuItem['access'];
        $item->language = '*';

        $item->store();

        $dbo = JFactory::getDbo();

        $dbo->setQuery('UPDATE `#__menu` SET `parent_id`=1, `level`=1 WHERE `id`=' . $item->id);
        $dbo->execute();
    }

    protected function getAdministratorGroups()
    {
        $groups = $this->getAllGroups();
        $array = array();

        foreach ($groups as $group) {
            if (true === JAccess::checkGroup($group->id, 'core.login.admin')) {
                $array[$group->id] = 1;
            }
        }

        return $array;
    }

    protected function getAllGroups()
    {
        $dbo = JFactory::getDBO();
        $query = $dbo->getQuery(true)
            ->select('a.id')
            ->from('#__usergroups AS a')
            ->leftJoin('`#__usergroups` AS b ON a.lft > b.lft AND a.rgt < b.rgt')
            ->group('a.id')
            ->order('a.lft ASC');

        $dbo->setQuery($query);

        return $dbo->loadObjectList();
    }
}
