<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class AdvertisementFactoryController extends JControllerLegacy
{
    protected $default_view = 'dashboard';

    public function display($cachable = false, $urlparams = false)
    {
        $this->addSubmenu($this->input->getCmd('view', $this->default_view));

        return parent::display($cachable, $urlparams);
    }

    protected function addSubmenu($vName)
    {
        $views = array(
            'dashboard',
            'ads',
            'prices',
            'payments',
            'notifications',
            'settings',
            'about',
        );

        foreach ($views as $view) {
            JHtmlSidebar::addEntry(
                JText::_('COM_ADVERTISEMENTFACTORY_SUBMENU_' . strtoupper($view)),
                'index.php?option=com_advertisementfactory&view=' . $view,
                $vName == $view
            );
        }
    }
}
