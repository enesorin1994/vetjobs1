<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelAbout extends JModelLegacy
{
    var $component = null;
    var $remoteManifest = null;

    function __construct()
    {
        parent::__construct();

        $this->component = 'advertisementfactory';
        $this->remoteManifest = $this->_getRemoteManifest();
    }

    function getInformation()
    {
        $information = new stdClass();

        $information->currentVersion = $this->_getCurrentVersion();
        $information->latestVersion = $this->_getLatestVersion();
        $information->remoteManifest = $this->remoteManifest;

        $information->newVersion = $information->latestVersion->numeric > $information->currentVersion->numeric;

        return $information;
    }

    function _getCurrentVersion()
    {
        $file = JPATH_COMPONENT_ADMINISTRATOR . DS . 'advertisementfactory.xml';
        $data = JInstaller::parseXMLInstallFile($file);

        $version = new stdClass();

        $version->text = $data['version'];
        $version->numeric = intval(str_replace('.', '', $data['version']));

        return $version;
    }

    function _getLatestVersion()
    {
        $version = new stdClass();

        $version->text = $this->remoteManifest->latestversion;
        $version->numeric = intval(str_replace('.', '', $this->remoteManifest->latestversion));

        return $version;
    }

    function _getRemoteManifest()
    {
        $filename = 'http://thephpfactory.com/versions/com_advertisementfactory.xml';
        $fileContents = @$this->_remoteReadUrl($filename);

        $xml = simplexml_load_string($fileContents);

        return $xml;
    }

    function _remoteReadUrl($uri)
    {
        if (function_exists('curl_init')) {
            $handle = curl_init();

            curl_setopt($handle, CURLOPT_URL, $uri);
            curl_setopt($handle, CURLOPT_MAXREDIRS, 5);
            curl_setopt($handle, CURLOPT_AUTOREFERER, 1);
            curl_setopt($handle, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($handle, CURLOPT_TIMEOUT, 10);

            $buffer = @curl_exec($handle);

            curl_close($handle);
            return $buffer;
        } elseif (ini_get('allow_url_fopen')) {
            $fp = @fopen($uri, 'r');

            if (!$fp) {
                return false;
            }

            stream_set_timeout($fp, 20);
            $linea = '';
            while ($remote_read = fread($fp, 4096)) {
                $linea .= $remote_read;
            }

            $info = stream_get_meta_data($fp);
            fclose($fp);

            if ($info['timed_out']) {
                return false;
            }

            return $linea;
        } else {
            return false;
        }
    }
}
