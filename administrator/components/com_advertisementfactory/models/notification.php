<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class AdvertisementFactoryModelNotification extends JModelAdmin
{
    public function getTable($type = 'Notification', $prefix = 'AdvertisementFactoryTable', $config = array())
    {
        return parent::getTable($type, $prefix, $config);
    }

    public function getForm($data = array(), $loadData = true)
    {
        // Get the form.
        $form = $this->loadForm(
            'com_advertisementfactory.notification',
            'notification',
            array('control' => 'jform', 'load_data' => $loadData));

        if (empty($form)) {
            return false;
        }

        return $form;
    }

    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = $this->getData();

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }

    protected function getData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_advertisementfactory.edit.notification.data', array());

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }
}
