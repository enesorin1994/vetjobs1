<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AdvertisementFactoryModelStatistics extends JModelList
{
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id',
            );
        }

        parent::__construct($config);
    }

    public function getAd()
    {
        $id = JFactory::getApplication()->input->getInt('id');
        $table = JTable::getInstance('Ad', 'AdvertisementFactoryTable');

        $table->load($id);

        return $table;
    }

    public function getListQuery()
    {
        $db = JFactory::getDbo();
        $ad = $this->getAd();

        // Select the required fields from the table
        $query = $db->getQuery(true)
            ->select('t.*')
            ->from('#__advertisementfactory_track t')
            ->where('t.ad_id = ' . $db->quote($ad->id));

        // Select the username
        $query->select('u.username')
            ->leftJoin('#__users u ON u.id = t.user_id');

        $query->order('t.created_at DESC');

        return $query;
    }

    protected function populateState($ordering = null, $direction = null)
    {
        // List state information.
        parent::populateState('name', 'asc');
    }
}
