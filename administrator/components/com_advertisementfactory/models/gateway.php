<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class AdvertisementFactoryModelGateway extends JModelAdmin
{
    public function getForm($data = array(), $loadData = true)
    {
        $title = isset($this->getState('item')->title) ? $this->getState('item')->title : $this->getItem($data['id'])->title;

        JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'payment');
        JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'payment' . DS . 'gateways' . DS . $title);

        $form = JForm::getInstance('com_advertisementfactory.gateway', 'gateway', array('control' => 'jform'));
        $form->loadFile($title);

        $form->bind($this->loadFormData());

        $language = JFactory::getLanguage();
        $language->load($title, JPATH_COMPONENT_ADMINISTRATOR . DS . 'payment');

        return $form;
    }

    public function getTable($type = 'Gateway', $prefix = 'AdvertisementFactoryTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getItem($pk = null)
    {
        $item = parent::getItem($pk);

        $this->setState('item', $item);

        return $item;
    }

    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_advertisementfactory.edit.gateway.data', array());

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }
}
