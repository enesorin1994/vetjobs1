<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class AdvertisementFactoryModelSettings extends JModelAdmin
{
    protected $_errors = array();

    public function getError($i = null, $toString = true)
    {
        // Find the error
        if ($i === null) {
            // Default, return the last message
            $error = end($this->_errors);
        } elseif (!array_key_exists($i, $this->_errors)) {
            // If $i has been specified but does not exist, return false
            return false;
        } else {
            $error = $this->_errors[$i];
        }

        // Check if only the string is requested
        if ($error instanceof Exception && $toString) {
            return (string)$error;
        }

        return $error;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function setError($error)
    {
        array_push($this->_errors, $error);
    }

    public function getForm($data = array(), $loadData = true)
    {
        // Get the form.
        JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR);

        $form = $this->loadForm(
            'com_advertisementfactory.settings',
            'config',
            array('control' => 'jform', 'load_data' => $loadData),
            false,
            '/config');

        if (empty($form)) {
            return false;
        }

        return $form;
    }

    public function save($data)
    {
        // Save the rules.
        if (isset($data['rules'])) {
            jimport('joomla.legacy.access.rules');
            $rules = new JAccessRules($data['rules']);
            $asset = JTable::getInstance('asset');

            if (!$asset->loadByName('com_advertisementfactory')) {
                $root = JTable::getInstance('asset');
                $root->loadByName('root.1');
                $asset->name = 'com_advertisementfactory';
                $asset->title = 'com_advertisementfactory';
                $asset->setLocation($root->id, 'last-child');
            }

            $asset->rules = (string)$rules;

            if (!$asset->check() || !$asset->store()) {
                $this->setError($asset->getError());
                return false;
            }

            unset($data['rules']);
        }

        // Save component settings.
        $extension = JTable::getInstance('Extension');
        $id = $extension->find(array('element' => 'com_advertisementfactory', 'type' => 'component'));

        $extension->load($id);
        $extension->bind(array('params' => $data));

        return $extension->store();
    }

    public function getStatus()
    {
        jimport('joomla.filesystem.file');
        jimport('joomla.filesystem.folder');

        $status = array();
        $extension = JTable::getInstance('Extension');
        $dbo = JFactory::getDbo();

        // 1. Check storage folder
        $path = JPATH_COMPONENT_SITE . DS . 'storage';
        $status['folder']['storage'] = (object)array('path' => $path, 'exists' => JFolder::exists($path));

        // 2. Check images folder
        $path = JPATH_COMPONENT_SITE . DS . 'storage' . DS . 'images';
        $status['folder']['images'] = (object)array('path' => $path, 'exists' => JFolder::exists($path));

        // Check if system plugin is enabled.
        $result = $extension->find(array('element' => 'advertisementfactory', 'type' => 'plugin'));
        $status['plugin']['system'] = (object)array('plugin' => 'Advertisement Factory System Plugin');
        if (!$result) {
            $status['plugin']['system']->installed = false;
        } else {
            $extension->load($result);
            $status['plugin']['system']->installed = true;
            $status['plugin']['system']->enabled = $extension->enabled;
        }

        // 3. Check if full page ads plugin is enabled
        $result = $extension->find(array('element' => 'advertisementfactoryfullpage', 'type' => 'plugin'));
        $status['plugin']['fullpage'] = (object)array('plugin' => 'Advertisement Factory Full Page Ads');
        if (!$result) {
            $status['plugin']['fullpage']->installed = false;
        } else {
            $extension->load($result);
            $status['plugin']['fullpage']->installed = true;
            $status['plugin']['fullpage']->enabled = $extension->enabled;
        }

        // 4. Check if modal ads plugin is enabled
        $result = $extension->find(array('element' => 'advertisementfactorymodal', 'type' => 'plugin'));
        $status['plugin']['modal'] = (object)array('plugin' => 'Advertisement Factory Modal Ads');
        if (!$result) {
            $status['plugin']['modal']->installed = false;
        } else {
            $extension->load($result);
            $status['plugin']['modal']->installed = true;
            $status['plugin']['modal']->enabled = $extension->enabled;
        }

        // 5. Check if content ads plugin is enabled
        $result = $extension->find(array('element' => 'advertisementfactorykeywords', 'type' => 'plugin'));
        $status['plugin']['keywords'] = (object)array('plugin' => 'Advertisement Factory Keyword Ads');
        if (!$result) {
            $status['plugin']['keywords']->installed = false;
        } else {
            $extension->load($result);
            $status['plugin']['keywords']->installed = true;
            $status['plugin']['keywords']->enabled = $extension->enabled;
        }

        // 6. Check payment gateways
        $query = $dbo->getQuery(true)
            ->select('COUNT(g.id) AS count')
            ->from('#__advertisementfactory_gateways g')
            ->where('g.enabled = 1');
        $dbo->setQuery($query);
        $status['gateways'] = $dbo->loadResult() ? 1 : 0;

        // 7. Check if popup ads plugin is enabled
        $result = $extension->find(array('element' => 'advertisementfactorypopup', 'type' => 'plugin'));
        $status['plugin']['popup'] = (object)array('plugin' => 'Advertisement Factory Popup Ads');
        if (!$result) {
            $status['plugin']['popup']->installed = false;
        } else {
            $extension->load($result);
            $status['plugin']['popup']->installed = true;
            $status['plugin']['popup']->enabled = $extension->enabled;
        }

        return $status;
    }

    protected function loadFormData()
    {
        $result = JComponentHelper::getComponent('com_advertisementfactory');

        return $result->params;
    }
}
