<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.form');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
jimport('joomla.application.component.model');
JFormHelper::loadFieldClass('list');

class JFormFieldAdType extends JFormFieldList
{
    protected $type = 'AdType';

    public static function getOptionsForFilter()
    {
        JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR);
        $config = JForm::getInstance('config', 'config', array(), false, '/config');

        $options = $config->getField('types_of_ads')->getOptions();

        array_unshift($options, array('value' => '', 'text' => JText::_('COM_ADVERTISEMENTFACTORY_SELECT_AD_TYPE')));

        return $options;
    }

    protected function getOptions()
    {
        JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR);
        $config = JForm::getInstance('config', 'config', array(), false, '/config');

        $options = $config->getField('types_of_ads')->getOptions();

        // Remove not available types
        if ('true' != (string)$this->element['readonly'] && 'false' != (string)$this->element['filter'] &&
            (!JFactory::getUser()->authorise('backend.manage', 'com_advertisementfactory') && !JFactory::getUser()->authorise('core.admin'))
        ) {
            $component = JComponentHelper::getComponent('com_advertisementfactory');
            $types = $component->params->get('enabled_ad_types', array());

            foreach ($options as $i => $option) {
                if (!in_array($option->value, $types)) {
                    unset($options[$i]);
                }
            }
        }

        if ((string)$this->element['noblank'] != 'true') {
            array_unshift($options, array('value' => '', 'text' => JText::_('COM_ADVERTISEMENTFACTORY_SELECT_AD_TYPE')));
        }

        return $options;
    }

    protected function getInput()
    {
        if ((string)$this->element['refresh'] == 'true') {
            $view = JFactory::getApplication()->input->getCmd('view');
            $this->onchange = 'Joomla.submitbutton(\'' . $view . '.updateform\');';
        }

        return parent::getInput();
    }
}
