<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

JFormHelper::loadFieldType('List');

class JFormFieldAdvertisementFactoryNotificationType extends JFormFieldList
{
    protected $type = 'AdvertisementFactoryNotificationType';

    protected function getOptions()
    {
        JLoader::discover('Factory', JPATH_COMPONENT_ADMINISTRATOR . DS . 'lib');
        $notifications = FactoryNotification::getInstance();

        $options = $notifications->getTypes();

        array_unshift($options, array('value' => '', 'text' => ''));

        return $options;
    }
}
