<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
jimport('joomla.form.helper');
jimport('joomla.html.parameter');
JFormHelper::loadFieldClass('list');

class JFormFieldAdPaid extends JFormFieldList
{
    protected $type = 'AdPaid';

    public static function getOptionsForFilter()
    {
        $options = array(
            JHtml::_('select.option', 0, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADPAID_NOT_PAID')),
            JHtml::_('select.option', 1, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADPAID_PAID')),
            JHtml::_('select.option', 2, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADPAID_REFUNDED')),
        );

        array_unshift($options, JHtml::_('select.option', '', JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADPAID_SELECT')));

        return $options;
    }

    protected function getOptions()
    {
        $options = array(
            JHtml::_('select.option', 0, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADPAID_NOT_PAID')),
            JHtml::_('select.option', 1, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADPAID_PAID')),
            JHtml::_('select.option', 2, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADPAID_REFUNDED')),
        );

        $options = self::addBlankOption($options);

        return $options;
    }

    protected function addBlankOption($options)
    {
        array_unshift($options, JHtml::_('select.option', '', JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADPAID_SELECT')));

        return $options;
    }
}
