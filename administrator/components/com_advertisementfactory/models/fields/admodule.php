<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
jimport('joomla.form.helper');
jimport('joomla.html.parameter');
JFormHelper::loadFieldClass('list');

class JFormFieldAdModule extends JFormFieldList
{
    protected $type = 'AdModule';

    public static function getOptionsForFilter()
    {
        $dbo = JFactory::getDbo();
        $modules = array(
            'mod_advertisementfactory_links',
            'mod_advertisementfactory_thumbnails',
            'mod_advertisementfactory_banners');

        $query = $dbo->getQuery(true)
            ->select('m.id AS value, m.title AS text, m.params')
            ->from('#__modules m')
            ->where('m.module IN("' . implode('", "', $modules) . '")')
            ->order('m.title ASC');

        // Filter published modules
        $query = self::filterPublishedModules($query);

        $dbo->setQuery($query);
        $modules = $dbo->loadObjectList();
        $modules = self::addBlankOption($modules);

        return $modules;
    }

    protected function getOptions()
    {
        $type = $this->form->getField('type')->value;
        $modules = array();

        if ($type) {
            $dbo = JFactory::getDbo();

            $query = $dbo->getQuery(true)
                ->select('m.id AS value, m.title AS text, m.params')
                ->from('#__modules m')
                ->where('m.module = "mod_advertisementfactory_' . $dbo->escape($type) . 's"')
                ->order('m.title ASC');

            // Filter published modules
            $query = self::filterPublishedModules($query);
            $dbo->setQuery($query);
            $modules = $dbo->loadObjectList();
        }

        $modules = self::addBlankOption($modules);

        return $modules;
    }

    protected function getInput()
    {
        if ((string)$this->element['refresh'] == 'true') {
            $view = JFactory::getApplication()->input->getCmd('view');
            $this->onchange = 'Joomla.submitbutton(\'' . $view . '.updateform\');';
        }

        return parent::getInput();
    }

    protected function addBlankOption($modules)
    {
        array_unshift($modules, JHtml::_('select.option', '', JText::_('COM_ADVERTISEMENTFACTORY_FILTER_MODULE_SELECT')));

        return $modules;
    }

    protected function filterPublishedModules($query)
    {
        $query->where('m.published = 1');

        return $query;
    }
}
