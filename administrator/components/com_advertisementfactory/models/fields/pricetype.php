<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.form');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
jimport('joomla.application.component.model');
JFormHelper::loadFieldClass('list');

class JFormFieldPriceType extends JFormFieldList
{
    protected $type = 'PriceType';

    public function getOptions()
    {
        JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR);
        $config = JForm::getInstance('config', 'config', array(), false, '/config');

        $options = $config->getField('types_of_prices')->getOptions();

        if ($this->form->getField('module_id')) {
            $module_id = $this->form->getField('module_id')->value;
            $module = JTable::getInstance('Module');

            $module->load($module_id);
            $params = new JRegistry($module->params);

            $module_type = $params->get('module_type', 1);
        } elseif ($this->form->getField('ad_id')) {
            $dbo = JFactory::getDbo();
            $ad_id = (int)$this->form->getField('ad_id')->value;

            $query = $dbo->getQuery(true)
                ->select('m.params')
                ->from('#__advertisementfactory_ads a')
                ->leftJoin('#__modules m ON m.id = a.module_id')
                ->where('a.id = ' . $dbo->quote($ad_id));
            $dbo->setQuery($query);
            $params = $dbo->loadResult();

            if ($params) {
                $params = new JRegistry($params);
                $module_type = $params->get('module_type', 1);
            } else {
                $module_type = 1;
            }
        } else {
            $module_type = 1;
        }

        if ('article' == $this->form->getField('type')->value) {
            unset($options[0], $options[1]);
        } else {
            foreach ($options as $i => $option) {
                if (($module_type == 1 && in_array($option->value, array(3))) ||
                    ($module_type == 0 && in_array($option->value, array(1, 2)))
                ) {
                    unset($options[$i]);
                }
            }
        }

        array_unshift($options, array('value' => '', 'text' => JText::_('COM_ADVERTISEMENTFACTORY_SELECT_PRICE_TYPE')));

        return $options;
    }
}
