<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');

class JFormFieldAdPayment extends JFormField
{
    protected $type = 'AdPayment';

    protected function getInput()
    {
        $output = '<div style="float: left; margin-top: 6px;">'
            . ($this->value ? '<a href="index.php?option=com_advertisementfactory&view=payment&layout=edit&id=' . $this->value . '">' . JText::sprintf('COM_ADVERTISEMENTFACTORY_FIELD_ADPAYMENT_PAYMENT', $this->value) . '</a>' : '-')
            . '</div>';

        return $output;
    }
}
