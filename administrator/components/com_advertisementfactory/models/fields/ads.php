<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.form');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
jimport('joomla.application.component.model');
JFormHelper::loadFieldClass('list');

class JFormFieldAds extends JFormFieldList
{
    protected $type = 'Ads';

    public function getOptions()
    {
        return parent::getOptions();
    }

    public function getOptionsAssoc()
    {
        $array = array();
        $options = parent::getOptions();

        foreach ($options as $option) {
            $array[$option->value] = JText::_($option->text);
        }

        return $array;
    }
}
