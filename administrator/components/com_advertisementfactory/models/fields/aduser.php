<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('user');

class JFormFieldAdUser extends JFormFieldUser
{
    public $type = 'AdUser';

    protected function getInput()
    {
        if ((string)$this->element['refresh'] == 'true') {
            $view = JFactory::getApplication()->input->getCmd('view');
            $this->onchange = 'parent.Joomla.submitbutton(\'' . $view . '.updateform\');';
        }

        return parent::getInput();
    }

    protected function getGroups()
    {
        $action = 'frontend.manage';
        $asset = 'com_advertisementfactory';
        $assetRules = JAccess::getAssetRules($asset);
        $groups = $this->getUserGroups();

        foreach ($groups as $i => $group) {
            $inheritedRule = JAccess::checkGroup($group, $action, $asset);
            $assetRule = $assetRules->allow($action, $group);
            $admin = JAccess::checkGroup($group, 'core.admin');

            if (true === $admin) {
                continue;
            }

            if (null === $inheritedRule || false === $inheritedRule) {
                unset($groups[$i]);
            }
        }

        sort($groups);

        return count($groups) ? $groups : null;
    }

    protected function getUserGroups()
    {
        $dbo = JFactory::getDbo();

        $query = $dbo->getQuery(true)
            ->select('g.id')
            ->from('#__usergroups g');
        $dbo->setQuery($query);

        return array_keys($dbo->loadAssocList('id'));
    }
}
