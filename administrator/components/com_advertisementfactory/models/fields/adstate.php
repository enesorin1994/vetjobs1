<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
jimport('joomla.form.helper');
jimport('joomla.html.parameter');
JFormHelper::loadFieldClass('list');

class JFormFieldAdState extends JFormFieldList
{
    protected $type = 'AdState';

    public static function getOptionsForFilter()
    {
        $options = array(
            JHtml::_('select.option', 0, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_UNPUBLISHED')),
            JHtml::_('select.option', 1, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_PUBLISHED')),
            JHtml::_('select.option', 2, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_EXPIRED')),
            JHtml::_('select.option', 3, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_REJECTED')),
            JHtml::_('select.option', 4, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_PENDING')),
        );

        array_unshift($options, JHtml::_('select.option', '', JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_SELECT')));

        return $options;
    }

    protected function getOptions()
    {
        $options = array(
            JHtml::_('select.option', 0, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_UNPUBLISHED')),
            JHtml::_('select.option', 1, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_PUBLISHED')),
            JHtml::_('select.option', 2, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_EXPIRED')),
            JHtml::_('select.option', 3, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_REJECTED')),
            JHtml::_('select.option', 4, JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_PENDING')),
        );

        $options = self::addBlankOption($options);

        return $options;
    }

    protected function addBlankOption($options)
    {
        array_unshift($options, JHtml::_('select.option', '', JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADSTATE_SELECT')));

        return $options;
    }
}
