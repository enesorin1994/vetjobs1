<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
jimport('joomla.filesystem.file');

class JFormFieldAdFile extends JFormField
{
    protected $type = 'AdFile';

    protected function getInput()
    {
        $type = $this->form->getField('type')->value;
        $module_id = isset($this->form->getField('module_id')->value) ? $this->form->getField('module_id')->value : null;
        $requirements = $this->getImageRequirements($type, $module_id);
        $info = $this->getImageInfo($requirements);

        $existing = '';

        $ad = JTable::getInstance('Ad', 'AdvertisementFactoryTable');
        $ad->filename = $this->value;

        $path = $ad->getFilePath();
        $src = $ad->getFileSource();

        if (JFile::exists($path)) {
            JHtml::_('behavior.modal');

            $size = getimagesize($path);

            $existing = '<div>' . JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADFILE_EXISTING_IMAGE') . '</div>'
                . '  <a href="' . $src . '" target="_blank" class="modal">'
                . '    <img style="left; max-width: 400px;" src="' . $src . '" />'
                . '  </a>'
                . '  <div style="clear: both;"></div>'
                . '  <div style="color: #999999; clear: both;"><small>' . $size[0] . 'px' . ' x ' . $size[1] . 'px</small></div>'
                . '  <div style="clear: both;"></div>'
                . '  <input type="checkbox" value="1" name="adfile_delete" id="adfile_delete" />'
                . '  <label for="adfile_delete" style="clear: none;">' . JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADFILE_DELETE_EXISTING') . '</label>'
                . '  <div style="clear: both;"></div>'
                . '  <input type="hidden" value="' . $this->value . '" name="' . $this->name . '" id="' . $this->id . '" />'
                . '';
        }

        $output = '<div>' . $existing;

        if ($requirements) {
            if ($existing) {
                $output .= '<div>' . JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADFILE_NEW_IMAGE') . '</div>';
            }

            $output .= '<input type="file" name="' . $this->name . '" id="' . $this->id . '" />'
                . $info;
        }

        $output .= '</div>';

        return $output;
    }

    protected function getImageInfo($requirements)
    {
        return '<div style="color: #999999; clear: both;"><small>'
            . $requirements['width'] . 'px'
            . ' x '
            . $requirements['height'] . 'px'
            . '</small></div>';
    }

    protected function getImageRequirements($type, $module_id = null)
    {
        $component = JComponentHelper::getComponent('com_advertisementfactory');

        switch ($type) {
            case 'modal':
                return array(
                    'width'  => $component->params->get('modalads_image_width'),
                    'height' => $component->params->get('modalads_image_height'),
                );
                break;

            case 'fullpage':
                return array(
                    'width'  => $component->params->get('fullpageads_image_width'),
                    'height' => $component->params->get('fullpageads_image_height'),
                );
                break;

            case 'thumbnail':
            case 'banner':
                $module = JTable::getInstance('Module');
                $module->load($module_id);

                if (!$module->id) {
                    return null;
                }

                $params = new JRegistry($module->params);

                return array(
                    'width'  => $params->get('width'),
                    'height' => $params->get('height'),
                );
                break;
        }

        JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'tables');
        $table = JTable::getInstance('Ad', 'AdvertisementFactoryTable');

        $table->type = $type;

        return $table->getImageRequirements();
    }
}
