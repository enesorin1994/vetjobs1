<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
jimport('joomla.form.helper');
jimport('joomla.html.parameter');
JFormHelper::loadFieldClass('list');

class JFormFieldAdList extends JFormFieldList
{
    protected $type = 'AdList';

    protected function getOptions()
    {
        $user_id = (int)$this->form->getField('user_id')->value;
        $dbo = JFactory::getDbo();

        $query = $dbo->getQuery(true)
            ->select('a.id AS value, a.name AS text')
            ->from('#__advertisementfactory_ads a')
            ->where('a.user_id = ' . $dbo->quote($user_id))
            ->order('a.name ASC');
        $dbo->setQuery($query);

        $users = $dbo->loadObjectList();
        $users = self::addBlankOption($users);

        return $users;
    }

    protected function getInput()
    {
        if ((string)$this->element['refresh'] == 'true') {
            $view = JFactory::getApplication()->input->getCmd('view');
            $this->onchange = 'Joomla.submitbutton(\'' . $view . '.updateform\');';
        }

        return parent::getInput();
    }

    protected function addBlankOption($users)
    {
        array_unshift($users, JHtml::_('select.option', '', JText::_('COM_ADVERTISEMENTFACTORY_FIELD_ADLIST_SELECT_AD')));

        return $users;
    }
}
