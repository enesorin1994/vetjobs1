<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');

class JFormFieldAdHistory extends JFormField
{
    protected $type = 'AdHistory';

    protected function getInput()
    {
        $history = explode("\n", $this->value);
        $output = '';

        foreach ($history as $line) {
            $output .= JText::_($line) . "<br />";
        }

        return $output;
    }
}
