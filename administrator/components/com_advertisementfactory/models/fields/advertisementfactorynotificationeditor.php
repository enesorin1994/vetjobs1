<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('JPATH_BASE') or die;

JFormHelper::loadFieldType('Editor');

class JFormFieldAdvertisementFactoryNotificationEditor extends JFormFieldEditor
{
    public $type = 'AdvertisementFactoryNotificationType';

    protected function getInput()
    {
        JHtml::script('media/com_advertisementfactory/assets/backend/js/fields/notificationtype.js');
        $this->element['buttons'] = 'article,pagebreak,readmore';

        $html = array();

        $html[] = parent::getInput();

        $html[] = '<table style="clear: both; padding-top: 25px;" class="advertisementfactory-notification-tokens" rel="' . $this->id . '">';
        foreach ($this->getTokens() as $value => $text) {
            $html[] = '<tr>';
            $html[] = '<td><a href="#">%%' . $value . '%%</a></td>';
            $html[] = '<td>-</td>';
            $html[] = '<td>' . $text . '</td>';
            $html[] = '</tr>';
        }
        $html[] = '</table>';

        return implode("\n", $html);
    }

    protected function getTokens()
    {
        JLoader::discover('Factory', JPATH_COMPONENT_ADMINISTRATOR . DS . 'lib');
        $notifications = FactoryNotification::getInstance();

        return $notifications->getTokens($this->form->getValue('type'));
    }
}
