<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class AdvertisementFactoryModelPrice extends JModelAdmin
{
    public function getTable($type = 'Price', $prefix = 'AdvertisementFactoryTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getForm($data = array(), $loadData = true)
    {
        // Get the form.
        $form = $this->loadForm(
            'com_advertisementfactory.price',
            'price',
            array(
                'control'   => 'jform',
                'load_data' => true));

        if (empty($form)) {
            return false;
        }

        return $form;
    }

    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_advertisementfactory.edit.price.data', array());

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }

    protected function preprocessForm(JForm $form, $data, $group = 'content')
    {
        parent::preprocessForm($form, $data, $group);

        $ad_type = is_object($data) ? $data->type : (isset($data['type']) ? $data['type'] : null);
        $module_id = is_object($data) ? $data->module_id : (isset($data['module_id']) ? $data['module_id'] : null);
        $price_type = is_object($data) ? $data->price_type : (isset($data['price_type']) ? $data['price_type'] : null);

        $form->setFieldAttribute('value', 'description', JText::_('COM_ADVERTISEMENTFACTORY_PRICE_VALUE_DESCRIPTION_' . $price_type));

        switch ($ad_type) {
            case 'keyword':
            case 'modal':
            case 'fullpage':
            case 'popup':
                $form->removeField('module_id');
                break;

            case 'article':
                $form->removeField('module_id');
                break;

            case null:
                $form->removeField('module_id');
                $form->removeField('price_type');
                break;

            case 'link':
            case 'thumbnail':
            case 'banner':
                if (!$module_id) {
                    $form->removeField('price_type');
                }
                break;
        }
    }
}
