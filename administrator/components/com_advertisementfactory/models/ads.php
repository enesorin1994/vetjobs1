<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AdvertisementFactoryModelAds extends JModelList
{
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'name', 'type', 'm.title', 'username', 'paid', 'state',
            );
        }

        parent::__construct($config);
    }

    public function getPreviewToken()
    {
        $params = JComponentHelper::getParams('com_advertisementfactory');

        return $params->get('backend_preview_token', null);
    }

    public function getListQuery()
    {
        $db = JFactory::getDbo();

        // Select the required fields from the table
        $query = $db->getQuery(true)
            ->select('a.id, a.type, a.name, a.state, a.paid')
            ->from('#__advertisementfactory_ads a')
            ->group('a.id');

        // Select the username
        $query->select('u.username')
            ->leftJoin('#__users u ON u.id = a.user_id');

        // Select the module
        $query->select('IF (m.title IS NULL, "-", m.title) AS module_title')
            ->leftJoin('#__modules m ON m.id = a.module_id');

        // Filter by module
        $moduleId = $this->getState('filter.module_id', '');
        if (is_numeric($moduleId)) {
            $query->where('a.module_id = ' . (int)$moduleId);
        }

        // Filter by type
        $type = $this->getState('filter.type');
        if ($type) {
            $query->where('a.type = ' . $db->quote($type, true));
        }

        // Filter by paid
        $paid = $this->getState('filter.paid');
        if ($paid) {
            $query->where('a.paid = ' . $db->quote($paid, true));
        }

        // Filter by state
        $state = $this->getState('filter.state');
        if (is_numeric($state)) {
            $query->where('a.state = ' . (int)$state);
        }

        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int)substr($search, 3));
            } else {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                $query->where('(a.name LIKE ' . $search . ')');
            }
        }

        // Add the list ordering clause.
        $query->order($db->escape($this->state->get('list.ordering') . ' ' . $this->state->get('list.direction')));

        return $query;
    }

    public function getTable($type = 'Ad', $prefix = 'AdvertisementFactoryTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    protected function populateState($ordering = null, $direction = null)
    {
        // Load the filter state.
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        // Load the module id
        $moduleId = $this->getUserStateFromRequest($this->context . '.filter.module_id', 'filter_module_id', '');
        $this->setState('filter.module_id', $moduleId);

        // Load the type
        $type = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type', '');
        $this->setState('filter.type', $type);

        // Load the paid state
        $paid = $this->getUserStateFromRequest($this->context . '.filter.paid', 'filter_paid', '');
        $this->setState('filter.paid', $paid);

        // Load the published state
        $state = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
        $this->setState('filter.state', $state);

        // List state information.
        parent::populateState('name', 'asc');
    }
}
