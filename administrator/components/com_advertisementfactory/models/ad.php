<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'adbase.php');

class AdvertisementFactoryModelAd extends AdvertisementFactoryModelAdBase
{
    public function getMessages()
    {
        $id = JFactory::getApplication()->input->getInt('id');

        $dbo = JFactory::getDbo();
        $query = $dbo->getQuery(true)
            ->select('m.*')
            ->from('#__advertisementfactory_messages m')
            ->where('m.ad_id = ' . $dbo->quote($id))
            ->order('m.created_at DESC');

        $query->select('u.username')
            ->leftJoin('#__users u ON u.id = m.user_id');

        $results = $dbo->setQuery($query)
            ->loadObjectList();

        return $results;
    }
}
