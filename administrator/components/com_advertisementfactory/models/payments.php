<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AdvertisementFactoryModelPayments extends JModelList
{
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'refnumber', 'payment_date', 'order_id', 'amount', 'gateway', 'status', 'username',
            );
        }

        parent::__construct($config);
    }

    public function getListQuery()
    {
        $db = JFactory::getDbo();

        // Select the required fields from the table
        $query = $db->getQuery(true)
            ->select('p.id, p.refnumber, p.received_at, p.order_id, p.amount, p.gateway, p.status, p.currency')
            ->from('#__advertisementfactory_payments p');

        // Select the username
        $query->select('u.username')
            ->leftJoin('#__users u ON u.id = p.user_id');

        // Filter by status
        $status = $this->getState('filter.status', '');
        if ('' != $status) {
            $query->where('p.status = ' . $db->quote($status));
        }

        // Filter by method
        $method = $this->getState('filter.method', '');
        if ('' != $method) {
            $query->where('p.gateway = ' . $db->quote($method));
        }

        // Add the list ordering clause.
        $query->order($db->escape($this->state->get('list.ordering') . ' ' . $this->state->get('list.direction')));

        return $query;
    }

    public function getMethods()
    {
        $dbo = JFactory::getDbo();
        $query = $dbo->getQuery(true)
            ->select('DISTINCT p.gateway AS value, p.gateway AS text')
            ->from('#__advertisementfactory_payments p');
        $dbo->setQuery($query);

        return $dbo->loadObjectList();
    }

    public function getTable($type = 'Payment', $prefix = 'TableAdvertisementFactory', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    protected function populateState($ordering = null, $direction = null)
    {
        // Load the status
        $status = $this->getUserStateFromRequest($this->context . '.filter.status', 'filter_status', '');
        $this->setState('filter.status', $status);

        // Load the method
        $method = $this->getUserStateFromRequest($this->context . '.filter.method', 'filter_method', '');
        $this->setState('filter.method', $method);

        // List state information.
        parent::populateState('received_at', 'asc');
    }
}
