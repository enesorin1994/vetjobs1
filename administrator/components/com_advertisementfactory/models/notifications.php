<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AdvertisementFactoryModelNotifications extends JModelList
{
    protected $filters = array('published', 'search', 'language', 'type');

    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array('id', 'subject', 'type', 'lang_code', 'published');
        }

        parent::__construct($config);
    }

    public function getListQuery()
    {
        $query = parent::getListQuery();

        // Get main query.
        $query->select('n.*')
            ->from('#__advertisementfactory_notifications n');

        // Get language title.
        $query->select('l.title AS language_title')
            ->leftJoin('#__languages l ON l.lang_code = n.lang_code');

        $this->addFilters($query);
        $this->addOrder($query);

        return $query;
    }

    public function getFilterOptionsType()
    {
        JLoader::discover('Factory', JPATH_COMPONENT_ADMINISTRATOR . DS . 'lib');
        $notifications = FactoryNotification::getInstance();

        return $notifications->getTypes();
    }

    protected function addFilters($query)
    {
        foreach ($this->filters as $filter) {
            $method = 'addFilter' . ucfirst($filter) . 'Condition';
            if (method_exists($this, $method)) {
                $this->$method($query);
            }
        }
    }

    protected function addOrder($query)
    {
        $orderCol = $this->state->get('list.ordering', 'subject');
        $orderDirn = $this->state->get('list.direction', 'asc');

        $query->order($query->escape($orderCol . ' ' . $orderDirn));
    }

    protected function populateState($ordering = null, $direction = null)
    {
        parent::populateState($ordering, $direction);

        // Load the filters.
        foreach ($this->filters as $filter) {
            $value = $this->getUserStateFromRequest($this->context . '.filter.' . $filter, 'filter_' . $filter, null, 'none', false);
            $this->setState('filter.' . $filter, $value);
        }
    }

    protected function addFilterPublishedCondition($query)
    {
        $filter = $this->getState('filter.published');

        if ('' != $filter) {
            $query->where('n.published = ' . $query->quote($filter));
        }
    }

    protected function addFilterSearchCondition($query)
    {
        $filter = $this->getState('filter.search');

        if ('' != $filter) {
            $condition = $query->quote('%' . $filter . '%');

            $query->where('(n.body LIKE ' . $condition . ' OR n.subject LIKE ' . $condition . ')');
        }
    }

    protected function addFilterLanguageCondition($query)
    {
        $filter = $this->getState('filter.language');

        if ('' != $filter) {
            $query->where('n.lang_code = ' . $query->quote($filter));
        }
    }

    protected function addFilterTypeCondition($query)
    {
        $filter = $this->getState('filter.type');

        if ('' != $filter) {
            $query->where('n.type = ' . $query->quote($filter));
        }
    }
}
