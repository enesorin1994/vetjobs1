<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class AdvertisementFactoryModelAdBase extends JModelAdmin
{
    public function getTable($type = 'Ad', $prefix = 'AdvertisementFactoryTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getForm($data = array(), $loadData = true)
    {
        // Add form path for frontend use
        JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'forms');

        // Get the form.
        $form = $this->loadForm(
            'com_advertisementfactory.ad',
            'ad',
            array(
                'control'   => 'jform',
                'load_data' => true));

        if (empty($form)) {
            return false;
        }

        return $form;
    }

    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = $this->getData();

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }

    public function getItem($pk = null)
    {
        $pk = is_null($pk) ? JFactory::getApplication()->input->getInt('id') : $pk;

        $this->item = parent::getItem($pk);

        return $this->item;
    }

    protected function preprocessForm(JForm $form, $data, $group = 'content')
    {
        parent::preprocessForm($form, $data, $group);

        $ad_type = is_object($data) ? $data->type : (isset($data['type']) ? $data['type'] : null);
        $module_id = is_object($data) ? $data->module_id : (isset($data['module_id']) ? $data['module_id'] : null);
        $filename = is_object($data) ? $data->filename : (isset($data['filename']) ? $data['filename'] : null);

        switch ($ad_type) {
            case null:
                $form->removeField('title');
                $form->removeField('contents');
                $form->removeField('filename');
                $form->removeField('keywords');
                $form->removeField('url');
                break;

            case 'link':
                $form->removeField('contents');
                $form->removeField('filename');
                $form->removeField('keywords');
                break;

            case 'thumbnail':
            case 'banner':
                $form->removeField('title');
                $form->removeField('contents');
                $form->removeField('keywords');

                if (!$module_id && !$filename) {
                    $form->removeField('filename');
                }
                break;

            case 'modal':
            case 'fullpage':
            case 'popup':
                $form->removeField('keywords');
                $form->removeField('module_id');
                break;

            case 'keyword':
                $form->removeField('module_id');
                $form->removeField('filename');
                break;

            case 'article':
                $form->removeField('module_id');
                $form->removeField('filename');
                $form->removeField('keywords');
                $form->removeField('url');
                break;
        }

        JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'forms' . DS . 'ads');
        $form->loadFile($ad_type);

        return $form;
    }

    protected function getData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_advertisementfactory.edit.ad.data', array());

        // Initialise
        $module_id = JFactory::getApplication()->input->getInt('module_id', 0);
        $type = JFactory::getApplication()->input->getCmd('type', '');

        if ($module_id || $type) {
            if (is_null($data)) {
                $data = array();
            }

            if (!isset($data['id']) || !$data['id']) {
                if ($module_id) {
                    $data['module_id'] = $module_id;
                }

                if ($type) {
                    $data['type'] = $type;
                }
            }
        }

        return $data;
    }

    public function getError($i = null, $toString = true)
    {
        // Find the error
        if ($i === null) {
            // Default, return the last message
            $error = end($this->_errors);
        } elseif (!array_key_exists($i, $this->_errors)) {
            // If $i has been specified but does not exist, return false
            return false;
        } else {
            $error = $this->_errors[$i];
        }

        // Check if only the string is requested
        if ($error instanceof Exception && $toString) {
            return (string)$error;
        }

        return $error;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function setError($error)
    {
        array_push($this->_errors, $error);
    }
}
