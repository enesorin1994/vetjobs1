<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AdvertisementFactoryModelPrices extends JModelList
{
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'value', 'price_type', 'type', 'price', 'm.title', 'published',
            );
        }

        parent::__construct($config);
    }

    public function getListQuery()
    {
        $db = JFactory::getDbo();

        // Select the required fields from the table
        $query = $db->getQuery(true)
            ->select('p.id, p.price, p.value, p.state, p.type, p.price_type')
            ->from('#__advertisementfactory_prices p');

        // Select the module title
        $query->select('IF (m.title IS NULL, "-", m.title) AS module_title')
            ->leftJoin('#__modules m ON m.id = p.module_id');

        // Filter by module
        $moduleId = $this->getState('filter.module_id', '');
        if (is_numeric($moduleId)) {
            $query->where('p.module_id = ' . (int)$moduleId);
        }

        // Filter by type
        $type = $this->getState('filter.type');
        if ($type != '') {
            $query->where('p.type = ' . $db->quote($type, true));
        }

        // Filter by published state
        $state = $this->getState('filter.state');
        if (is_numeric($state)) {
            $query->where('p.state = ' . (int)$state);
        } else if ($state === '') {
            $query->where('(p.state IN (0, 1))');
        }

        // Add the list ordering clause.
        $query->order($db->escape($this->state->get('list.ordering') . ' ' . $this->state->get('list.direction')));

        return $query;
    }

    public function getTable($type = 'Price', $prefix = 'TableAdvertisementFactory', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    protected function populateState($ordering = null, $direction = null)
    {
        // Load the published state
        $state = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
        $this->setState('filter.state', $state);

        // Load the module id
        $moduleId = $this->getUserStateFromRequest($this->context . '.filter.module_id', 'filter_module_id', '');
        $this->setState('filter.module_id', $moduleId);

        // Load the type
        $type = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type', '');
        $this->setState('filter.type', $type);

        // List state information.
        parent::populateState('price', 'asc');
    }
}
