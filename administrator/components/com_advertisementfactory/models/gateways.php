<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AdvertisementFactoryModelGateways extends JModelList
{
    public function getListQuery()
    {
        JModelLegacy::addIncludePath(JPATH_COMPONENT_SITE . DS . 'models');
        $payment = JModelLegacy::getInstance('PaymentProxy', 'AdvertisementFactoryModel');
        $payment->updateGateways();

        $db = JFactory::getDbo();

        $query = $db->getQuery(true)
            ->select('g.*')
            ->from('#__advertisementfactory_gateways g');

        return $query;
    }

    public function getForm($data = array(), $loadData = true)
    {
        return null;
    }
}
