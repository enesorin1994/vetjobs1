<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class AdvertisementFactoryModelPayment extends JModelAdmin
{
    public function getTable($type = 'Payment', $prefix = 'AdvertisementFactoryTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getForm($data = array(), $loadData = true)
    {
        // Get the form.
        $form = $this->loadForm('com_advertisementfactory.payment', 'payment', array('control' => 'jform', 'load_data' => $loadData));

        if (empty($form)) {
            return false;
        }

        if ('GET' == JFactory::getApplication()->input->getMethod()) {
            if ('manual-check' == $this->item->status) {
                $form->setFieldAttribute('status', 'readonly', 'false');
            } else {
                $form->setFieldAttribute('status', 'type', 'text');
            }
        }

        return $form;
    }

    public function getItem($pk = null)
    {
        $this->item = parent::getItem($pk);

        $order = JTable::getInstance('Order', 'AdvertisementFactoryTable');
        $order->load($this->item->order_id);

        $this->item->ad_id = $order->ad_id;

        return $this->item;
    }

    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_advertisementfactory.edit.price.data', array());

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }
}
