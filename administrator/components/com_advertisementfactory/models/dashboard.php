<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

class AdvertisementFactoryModelDashboard extends JModelLegacy
{
    function getPending()
    {
        $dbo = JFactory::getDbo();

        $query = $dbo->getQuery(true)
            ->select('a.id, a.name')
            ->from('#__advertisementfactory_ads a')
            ->where('a.state = 4')
            ->where('a.paid = 1');
        $dbo->setQuery($query);

        return $dbo->loadObjectList();
    }

    function getPayments()
    {
        $dbo = JFactory::getDbo();

        $query = $dbo->getQuery(true)
            ->select('p.id, p.gateway, p.status, p.user_id, p.currency, p.amount, p.payment_date, p.received_at')
            ->from('#__advertisementfactory_payments p')
            ->order('p.received_at DESC');

        $query->select('u.username')
            ->leftJoin('#__users u ON u.id = p.user_id');

        $dbo->setQuery($query, 0, 10);

        return $dbo->loadObjectList();
    }
}
