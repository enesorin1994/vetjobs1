<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.html.parameter');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

class TheFactoryPaymentPlugins
{
    protected $options;

    public function __construct($options)
    {
        $this->options = $options;
    }

    public function getGateway($gateway)
    {
        $file = $this->options['basepath'] . DS . 'gateways' . DS . $gateway . DS . $gateway . '.php';

        if (!JFile::exists($file)) {
            return false;
        }

        require_once($file);

        $dbo = JFactory::getDbo();

        $query = $dbo->getQuery(true)
            ->select('g.*')
            ->from('#__' . $this->options['table_prefix'] . '_gateways g')
            ->where('g.title = ' . $dbo->quote($gateway));
        $dbo->setQuery($query);
        $table = $dbo->loadObject();

        $gateway = new $gateway($this->options, $table);

        return $gateway;
    }

    public function getGatewaySelect()
    {
        $gateways = $this->getGateways();

        $dbo = JFactory::getDbo();
        $query = $dbo->getQuery(true)
            ->select('g.*')
            ->from('#__' . $this->options['table_prefix'] . '_gateways g')
            ->where('g.enabled = 1');
        $dbo->setQuery($query);

        $published = $dbo->loadObjectList();
        $output = '';

        foreach ($published as $gateway) {
            if (in_array($gateway->title, $gateways)) {
                $title = $gateway->title;
                $gateway = new $title($this->options, $gateway);

                $output .= '<input type="radio" name="method" value="' . $gateway->getTitle() . '" id="method_' . $gateway->getTitle() . '" />'
                    . '<label for="method_' . $gateway->getTitle() . '"><img src="' . $gateway->getLogo() . '" alt="' . $gateway->getTitle() . '" style="cursor: pointer;" /></label>'
                    . '<br />';
            }
        }

        return $output;
    }

    public function validateGateway($gateway)
    {
        $dbo = JFactory::getDbo();

        $query = $dbo->getQuery(true)
            ->select('g.id')
            ->from('#__' . $this->options['table_prefix'] . '_gateways g')
            ->where('g.enabled = 1')
            ->where('g.title = ' . $dbo->quote($gateway));
        $dbo->setQuery($query);

        return $dbo->loadResult();
    }

    public function updateGateways()
    {
        $this->getGateways();
    }

    protected function getGateways()
    {
        $base = $this->options['basepath'] . DS . 'gateways';
        $folders = JFolder::folders($base);
        $gateways = array();

        foreach ($folders as $folder) {
            $file = $base . DS . $folder . DS . $folder . '.php';

            if (JFile::exists($file)) {
                $gateways[] = $folder;

                JLoader::register($folder, $file);
            }
        }

        $this->addNewGateways($gateways);

        return $gateways;
    }

    protected function addNewGateways($gateways)
    {
        $dbo = JFactory::getDbo();

        $query = $dbo->getQuery(true)
            ->select('g.id, g.title')
            ->from('#__' . $this->options['table_prefix'] . '_gateways g');
        $dbo->setQuery($query);

        $records = $dbo->loadObjectList('title');

        foreach ($gateways as $gateway) {
            if (!array_key_exists($gateway, $records)) {
                $query = ' INSERT INTO #__' . $this->options['table_prefix'] . '_gateways (`title`, `enabled`) VALUES ("' . $gateway . '", 0)';
                $dbo->setQuery($query);
                $dbo->execute();
            }
        }
    }
}
