<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.html.parameter');

abstract class TheFactoryPaymentPlugin
{
    protected $title = null;
    protected $enabled = null;
    protected $logo = null;
    protected $options = null;
    protected $info = null;

    public function __construct($options, $data = null)
    {
        $this->enabled = $data->enabled;
        $this->title = $data->title;
        $this->logo = $data->logo;
        $this->params = new JRegistry($data->params);

        $this->options = $options;

        $this->return_url = JRoute::_('index.php?option=' . $this->options['component'] . '&view=payment&layout=complete', false, -1);
        $this->cancel_return = JRoute::_('index.php?option=' . $this->options['component'] . '&view=payment&layout=cancel', false, -1);
        $this->notify_url = JRoute::_('index.php?option=' . $this->options['component'] . '&task=purchase.notify&gateway=' . $this->title . '&lang=en', false, -1);
        $this->payment_accepted_url = JRoute::_('index.php?option=' . $this->options['component'] . '&view=payment&layout=accepted', false, -1);
        $this->payment_failed_url = JRoute::_('index.php?option=' . $this->options['component'] . '&view=payment&layout=failed', false, -1);

        $this->options['language']->load($this->title, JPATH_COMPONENT_ADMINISTRATOR . DS . 'payment');
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function getLogo()
    {
        if (!empty($this->logo)) {
            return $this->logo;
        }

        $logos = JFolder::files($this->options['basepath'] . DS . 'gateways' . DS . $this->title, '^' . $this->title . '.(jpg|png|gif)');

        if (count($logos)) {
            $logo = $logos[0];

            return $this->options['sourcepath'] . 'gateways/' . $this->title . '/' . $logo;
        }

        return '';
    }

    public function loadInfo($info)
    {
        $this->info = $info;
    }

    public function getParam($key, $default = null)
    {
        return $this->params->get($key, $default);
    }

    public function executeStep($step)
    {
        if (method_exists($this, 'step' . $step)) {
            $this->{'step' . $step}();
        } else {
            throw new Exception($this->title . ' ' . JText::_('does not have step') . ' ' . $step, 500);
        }
    }

    protected function validateUser($id)
    {
        $user = $this->options['user']->load($id);

        if (!$user) {
            return false;
        }

        $order = $this->options['order'];
        if (method_exists($order, 'validateUser') && !$order->validateUser()) {
            return false;
        }

        return true;
    }

    public function showError($error)
    {
        throw new Exception($error, 500);
    }

    protected function redirectToStep($step)
    {
        $app = JFactory::getApplication();

        $app->redirect(JRoute::_('index.php?option=' . $this->options['component'] . '&task=purchase.gateway&step=' . $step . '&method=' . $this->title, false));
    }

    protected function redirect($link)
    {
        $app = JFactory::getApplication();

        $app->redirect($link);
    }

    protected function createOrder()
    {
        return $this->options['order']->create($this->title, $this->info);
    }

    protected function getOrder($id)
    {
        static $order = null;

        if (is_null($order)) {
            $order = $this->options['order'];
            $order = $order->getOrderId($id);

            if (!$order->id) {
                $order = false;
            }
        }

        return $order;
    }

    protected function createPayment($ipn = null)
    {
        $payment = $this->options['payment'];

        $payment->received_at = $this->options['date']->toSql();
        $payment->gateway = $this->title;
        $payment->log = '<b>Request From Gateway:</b>' . "\n";

        $data = is_null($ipn) ? $_POST : $ipn;

        foreach ($data as $variable => $value) {
            $payment->log .= $this->options['dbo']->escape($variable) . ': ' . $this->options['dbo']->escape($value) . "\n";
        }

        return $payment;
    }

    protected function savePayment($payment)
    {
        $payment->errors = implode("\n", $this->errors);
        $payment->log .= "\n" . '<b>Advertisement Factory Response:</b>' . "\n" . $payment->status;

        $payment->store();

        $order = $this->getOrder($payment->order_id);

        if ($order) {
            $order->status = 'processed';
            $order->store();
        }
    }
}
