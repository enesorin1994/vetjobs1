<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

// Initialize Joomla framework
define('_JEXEC', 1);
defined('_JEXEC') or die;
define('DS', DIRECTORY_SEPARATOR);
define('JPATH_BASE', dirname(__FILE__) . '/../../../../../..');

/* Required Files */
require_once JPATH_BASE . '/includes/defines.php';
require_once JPATH_BASE . '/includes/framework.php';

jimport('joomla.application.component.model');
define('JPATH_COMPONENT_ADMINISTRATOR', JPATH_SITE . DS . 'administrator' . DS . 'components' . DS . 'com_advertisementfactory');
JModelLegacy::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'models');
JFactory::getApplication('site');

$model = JModelLegacy::getInstance('PaymentProxy', 'AdvertisementFactoryModel');
$gateway = $model->getGateway('saferpay');

$root = str_replace('administrator/components/com_advertisementfactory/payment/gateways/saferpay/', '', JURI::root());
$redirect = $root . 'index.php?option=com_advertisementfactory&view=payment&layout=failed';

header('Location: ' . $redirect);
