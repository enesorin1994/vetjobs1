<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'payment' . DS . 'TheFactoryPaymentPlugin.php');

class SaferPay extends TheFactoryPaymentPlugin
{
    function step1()
    {
        // Create a new order
        $order = $this->createOrder();

        $base = JURI::root() . 'administrator/components/com_advertisementfactory/payment/gateways/saferpay/';
        $gateway = 'https://www.saferpay.com/hosting/CreatePayInit.asp';

        $array = array();
        $array['spPassword'] = $this->getParam('sppassword');
        $array['ACCOUNTID'] = $this->getParam('account_id');
        $array['ORDERID'] = $order->id;
        $array['AMOUNT'] = $order->amount * 100;
        $array['CURRENCY'] = $order->currency;
        $array['DESCRIPTION'] = urlencode('"' . $order->title . '"');
        $array['SUCCESSLINK'] = '"' . $base . 'success.php"';
        $array['FAILLINK'] = '"' . $base . 'fail.php"';
        $array['BACKLINK'] = '"' . $base . 'back.php"';
        $array['NOTIFYURL'] = '"' . $base . 'notify.php"';

        if ('' == $array['spPassword']) {
            unset($array['spPassword']);
        }

        $attributes = array();
        foreach ($array as $key => $value) {
            $attributes[] = $key . '=' . $value;
        }

        $url = $gateway . '?' . implode('&', $attributes);

        /* get the PayInit URL from the hosting server */
        $payinit_url = join("", file($url));

        ?>
        <script src="http://www.saferpay.com/OpenSaferpayScript.js"></script>

        <h1><?php echo JText::_('PAYMENT_SAFERPAY_CONFIRM'); ?></h1>
        <p><?php echo JText::sprintf('PAYMENT_SAFERPAY_CONFIRM_PURCHASE', $order->title); ?></p>
        <a href="<?php echo $payinit_url; ?>"
           onclick="OpenSaferpayTerminal('<?php echo $payinit_url; ?>', this, 'LINK');"><?php echo JText::_('PAYMENT_SAFERPAY_CONTINUE'); ?></a>
        <?php
    }

    function processIpn()
    {
        $ipn = $this->getIpn();
        $this->errors = array();

        $payment = $this->createPayment();

        $payment->amount = $ipn['amount'];
        $payment->currency = $ipn['currency'];
        $payment->order_id = $ipn['order_id'];
        $payment->refnumber = $ipn['reference'];

        // Check for errors
        $validIpn = $this->validateIpn($ipn);

        // Check if IPN is valid.
        if (false === $validIpn) {
            $payment->status = 'error';
            $this->errors[] = 'IPN not verified';
        } else {
            $this->validatePayment($ipn);

            parse_str(substr($validIpn, 3));

            $password = '' == $this->getParam('sppassword') ? '' : 'spPassword=' . $this->getParam('sppassword');

            $gateway = 'https://www.saferpay.com/hosting/PayComplete.asp';
            $url = $gateway . '?' . $password . '&ACCOUNTID=' . $this->getParam('account_id') . '&ID=' . urlencode($ipn['reference']) . '&TOKEN=' . urlencode($ipn['token']);
            $result = join("", file($url));

            if (!substr($result, 0, 2) == "OK") {
                $this->errors[] = 'Capture has failed.';
                $payment->status = 'manual-check';
            } else {
                $payment->status = count($this->errors) ? 'manual-check' : 'ok';
            }
        }

        $this->savePayment($payment);
    }

    protected function getIpn()
    {
        $data = $_POST['DATA'];
        $signature = $_POST['SIGNATURE'];

        $ipn = array(
            'order_id'   => 0,
            'amount'     => 0,
            'currency'   => '',
            'account_id' => 0,
            'reference'  => '',
            'token'      => '',
            'data'       => $data,
            'signature'  => $signature,
        );

        if (preg_match('/ORDERID=\\\"(.+)\\\"/U', $data, $match)) {
            $ipn['order_id'] = $match[1];
        }

        if (preg_match('/AMOUNT=\\\"(.+)\\\"/U', $data, $match)) {
            $ipn['amount'] = $match[1] / 100;
        }

        if (preg_match('/CURRENCY=\\\"(.+)\\\"/U', $data, $match)) {
            $ipn['currency'] = $match[1];
        }

        if (preg_match('/ACCOUNTID=\\\"(.+)\\\"/U', $data, $match)) {
            $ipn['account_id'] = $match[1];
        }

        if (preg_match('/ ID=\\\"(.+)\\\"/U', $data, $match)) {
            $ipn['reference'] = $match[1];
        }

        if (preg_match('/TOKEN=\\\"(.+)\\\"/U', $data, $match)) {
            $ipn['token'] = $match[1];
        }

        return $ipn;
    }

    protected function validateIpn($ipn)
    {
        $gateway = 'https://www.saferpay.com/hosting/VerifyPayConfirm.asp';
        $url = $gateway . '?DATA=' . urlencode($ipn['data']) . '&SIGNATURE=' . urlencode($ipn['signature']);
        $result = join("", file($url));

        return substr($result, 0, 3) == "OK:" ? $result : false;
    }

    protected function validatePayment($ipn)
    {
        // Validate account id
        if ($this->getParam('account_id') != $ipn['account_id']) {
            $this->errors[] = 'Account id is different from expected';
        }

        // Validate Order
        $order = $this->getOrder($ipn['order_id']);

        if ($order) {
            if ('pending' == $order->status) {
                // Validate amount
                if ($order->amount != $ipn['amount']) {
                    $this->errors[] = JText::_('Payment amount received is different from expected');
                }

                // Validate currency
                if ($order->currency != $ipn['currency']) {
                    $this->errors[] = JText::_('Payment currency is different from expected');
                }
            } else {
                $this->errors[] = JText::_('Order already processed');
            }
        } else {
            $this->errors[] = JText::_('Order not found');
        }
    }
}
