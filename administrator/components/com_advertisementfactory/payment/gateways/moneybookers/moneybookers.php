<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'payment' . DS . 'TheFactoryPaymentPlugin.php');

class Moneybookers extends TheFactoryPaymentPlugin
{
    function step1()
    {
        // Create a new order
        $order = $this->createOrder();

        // Show the confirmation form
        ?>

        <h1><?php echo JText::_('PAYMENT_MONEYBOOKERS_CONFIRM'); ?></h1>
        <p><?php echo JText::sprintf('PAYMENT_MONEYBOOKERS_PURCHASE_CONFIRM"', $order->title); ?></p>

        <br/>

        <form action="<?php echo $this->getParam('action'); ?>" method="post">
            <input type="hidden" name="pay_to_email" value="<?php echo $this->getParam('email'); ?>"/>
            <input type="hidden" name="hide_login" value="0"/>
            <input type="hidden" name="merchant_fields" value="m_userid,m_itemnr"/>
            <input type="hidden" name="m_userid" value="<?php echo $order->user_id; ?>"/>
            <input type="hidden" name="m_itemnr" value="<?php echo $order->id; ?>"/>
            <input type="hidden" name="language" value="EN"/>

            <input type="hidden" name="return_url" value="<?php echo $this->return_url; ?>"/>
            <input type="hidden" name="cancel_url" value="<?php echo $this->cancel_return; ?>"/>
            <input type="hidden" name="status_url" value="<?php echo $this->notify_url; ?>"/>

            <input type="hidden" name="amount" value="<?php echo $order->amount; ?>"/>
            <input type="hidden" name="currency" value="<?php echo $order->currency; ?>"/>
            <input type="hidden" name="detail1_description" value="<?php echo JText::_('PAYMENT_MONEYBOOKERS_AD'); ?>"/>
            <input type="hidden" name="detail1_text" value="<?php echo $order->title; ?>"/>

            <input type="image" src="http://www.moneybookers.com/images/logos/checkout_logos/checkout_120x40px.gif"
                   name="submit" alt="<?php echo JText::_('PAYMENT_MONEYBOOKERS_BUY'); ?>" style="margin-left: 30px;">

        </form>

        <?php
    }

    function processIpn()
    {
        // Get values from request
        $ipn['refnumber'] = trim(stripslashes($_POST['mb_transaction_id']));
        $ipn['order_id'] = trim(stripslashes($_POST['m_itemnr']));
        $ipn['status'] = trim(stripslashes($_POST['status']));
        $ipn['amount'] = trim(stripslashes($_POST['mb_amount']));
        $ipn['currency'] = trim(stripslashes($_POST['mb_currency']));
        $ipn['receiver_email'] = trim(stripslashes($_POST['pay_to_email']));
        $ipn['payer_email'] = trim(stripslashes($_POST['pay_from_email']));
        $ipn['user_id'] = trim(stripslashes($_POST['m_userid']));
        $ipn['md5sig'] = trim(stripslashes($_POST['md5sig']));
        $ipn['userid'] = trim(stripslashes($_POST['m_userid']));

        $payment = $this->createPayment($ipn);

        $payment->payment_date = date('Y-m-d H:i:s');
        $payment->user_id = $ipn['user_id'];
        $payment->amount = $ipn['amount'];
        $payment->currency = $ipn['currency'];
        $payment->order_id = $ipn['order_id'];
        $payment->refnumber = $ipn['refnumber'];

        // Check for errors
        $this->errors = $this->validatePayment();

        switch ($this->status) {
            case '2':
                $payment->status = count($this->errors) ? 'manual-check' : 'ok';
                break;

            case '-1':
            case '-2':
            case '-3':
                $payment->status = 'error';
                break;

            case '0':
            default:
                $payment->status = 'manual-check';
                break;
        }

        $this->savePayment($payment);
    }

    function validatePayment($ipn)
    {
        $errors = array();

        // Validate IPN
        if (true !== $this->validateIpn($ipn)) {
            $errors[] = JText::_('IPN not verified');
        }

        // Validate paypal email
        if ($this->receiver_email != $this->getParam('email')) {
            $errors[] = JText::_('Receiver email is different from expected');
        }

        // Validate user
        if (!$this->validateUser($ipn['user_id'])) {
            $errors[] = JText::_('User doesn\'t exist or is not validated!');
        }

        // Validate Order
        $order = $this->getOrder($ipn['order_id']);

        if ($order) {
            if ('pending' == $order->status) {
                // Validate amount
                if ($order->amount != $ipn['amount']) {
                    $errors[] = JText::_('Payment amount received is different from expected');
                }

                // Validate currency
                if ($order->currency != $ipn['currency']) {
                    $errors[] = JText::_('Payment currency is different from expected');
                }
            } else {
                $errors[] = JText::_('Order already processed');
            }
        } else {
            $errors[] = JText::_('Order not found');
        }

        return $errors;
    }

    function validateIpn($ipn)
    {
        $pay_to_email = $ipn['receiver_email'];
        $md5sig = $ipn['md5sig'];
        $mbooker_address = $this->getParam('email');
        $payment_status = $ipn['status'];

        $verified_ok = (strtolower($mbooker_address) == strtolower($pay_to_email) && $payment_status == '2');

        return $verified_ok;
    }
}
