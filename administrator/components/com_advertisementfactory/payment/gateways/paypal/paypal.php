<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'payment' . DS . 'TheFactoryPaymentPlugin.php');

class Paypal extends TheFactoryPaymentPlugin
{
    function step1()
    {
        // Create a new order
        $order = $this->createOrder();

        ?>

        <h1><?php echo JText::_('PAYMENT_PAYPAL_CONFIRM'); ?></h1>
        <p><?php echo JText::sprintf('PAYMENT_PAYPAL_CONFIRM_PURCHASE', $order->title); ?></p>

        <br/>

        <form action="<?php echo $this->getAction(); ?>" method="post">
            <input type="hidden" name="item_number" value="<?php echo $order->id; ?>"/>
            <input type="hidden" name="on0" value="userid"/>
            <input type="hidden" name="os0" value="<?php echo $order->user_id; ?>"/>
            <input type="hidden" name="amount" value="<?php echo $order->amount; ?>"/>
            <input type="hidden" name="currency_code" value="<?php echo $order->currency; ?>"/>

            <input type="hidden" name="cmd" value="_xclick"/>
            <input type="hidden" name="business" value="<?php echo $this->getParam('email'); ?>"/>
            <input type="hidden" name="item_name" value="<?php echo $order->title; ?>"/>
            <input type="hidden" name="quantity" value="1"/>

            <input type="hidden" name="return" value="<?php echo $this->return_url; ?>"/>
            <input type="hidden" name="cancel_return" value="<?php echo $this->cancel_return; ?>"/>
            <input type="hidden" name="notify_url" value="<?php echo $this->notify_url; ?>"/>

            <input type="hidden" name="tax" value="0"/>
            <input type="hidden" name="no_note" value="1"/>
            <input type="hidden" name="no_shipping" value="1"/>

            <input type="hidden" name="bn" value="ThePHPFactory_SP"/>

            <input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but23.gif" name="submit"
                   style="margin-left: 30px;">
        </form>

        <?php
    }

    function processIpn()
    {
        $ipn = array();

        // Get values from request
        $ipn['refnumber'] = trim(stripslashes($_POST['txn_id']));
        $ipn['order_title'] = trim(stripslashes($_POST['item_name']));
        $ipn['order_id'] = trim(stripslashes($_POST['item_number']));
        $ipn['status'] = trim(stripslashes($_POST['payment_status']));
        $ipn['amount'] = trim(stripslashes($_POST['mc_gross']));
        $ipn['currency'] = trim(stripslashes($_POST['mc_currency']));
        $ipn['receiver_email'] = trim(stripslashes($_POST['receiver_email']));
        $ipn['payer_email'] = trim(stripslashes($_POST['payer_email']));
        $ipn['test_ipn'] = trim(stripslashes($_POST['test_ipn']));
        $ipn['first_name'] = trim(stripslashes($_POST['first_name']));
        $ipn['last_name'] = trim(stripslashes($_POST['last_name']));
        $ipn['user_id'] = trim(stripslashes($_POST['option_selection1']));
        $ipn['date'] = trim(stripslashes($_POST['payment_date']));

        $payment = $this->createPayment();

        $payment->payment_date = $ipn['date'];
        $payment->user_id = $ipn['user_id'];
        $payment->amount = $ipn['amount'];
        $payment->currency = $ipn['currency'];
        $payment->order_id = $ipn['order_id'];
        $payment->refnumber = $ipn['refnumber'];

        // Check for errors
        $this->errors = $this->validatePayment($ipn);

        switch ($ipn['status']) {
            case 'Completed':
            case 'Processed':
                $payment->status = count($this->errors) ? 'manual-check' : 'ok';
                break;

            case 'Failed':
            case 'Denied':
            case 'Canceled-Reversal':
            case 'Expired':
            case 'Voided':
            case 'Reversed':
            case 'Refunded':
                $payment->status = 'error';
                break;

            case 'In-Progress':
            case 'Pending':
            default:
                $payment->status = 'manual-check';
                break;
        }

        $this->savePayment($payment);
    }

    function validatePayment($ipn)
    {
        $errors = array();

        // Validate IPN
        if (true !== $this->validateIpn()) {
            $errors[] = JText::_('IPN not verified');
        }

        // Validate paypal email
        if ($ipn['receiver_email'] != $this->getParam('email')) {
            $errors[] = JText::_('Receiver email is different from expected');
        }

        // Validate user
        if (!$this->validateUser($ipn['user_id'])) {
            $errors[] = JText::_('User doesn\'t exist or is not validated!');
        }

        // Validate Order
        $order = $this->getOrder($ipn['order_id']);

        if ($order) {
            if ('pending' == $order->status) {
                // Validate amount
                if ($order->amount != $ipn['amount']) {
                    $errors[] = JText::_('Payment amount received is different from expected');
                }

                // Validate currency
                if ($order->currency != $ipn['currency']) {
                    $errors[] = JText::_('Payment currency is different from expected');
                }
            } else {
                $errors[] = JText::_('Order already processed');
            }
        } else {
            $errors[] = JText::_('Order not found');
        }

        return $errors;
    }

    function validateIpn()
    {
        // parse the paypal URL
        $url_parsed = parse_url($this->getAction());

        $post_string = '';
        foreach ($_POST as $field => $value) {
            $post_string .= $field . '=' . urlencode($value) . '&';
        }

        $post_string .= "cmd=_notify-validate";

        $fp = fsockopen('ssl://' . $url_parsed['host'], 443, $err_num, $err_str, 20);
        if (!$fp) {
            return 'Fsockopen error no. ' . $err_num . ': ' . $err_str . '. IPN not verified';
        } else {
            fputs($fp, "POST " . $url_parsed['path'] . " HTTP/1.1\r\n");
            fputs($fp, "Host: " . $url_parsed['host'] . ":443\r\n");
            fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
            fputs($fp, "Content-length: " . strlen($post_string) . "\r\n");
            fputs($fp, "Connection: close\r\n\r\n");
            fputs($fp, $post_string . "\r\n\r\n");

            $response = '';
            while (!feof($fp)) {
                $response .= fgets($fp, 1024);
            }

            fclose($fp);
        }

        if (preg_match('/VERIFIED/', $response)) {
            return true;
        }

        return false;
    }

    protected function getAction()
    {
        if ($this->getParam('use_sandbox', 0)) {
            return $this->getParam('action_sandbox');
        }

        return $this->getParam('action');
    }
}
