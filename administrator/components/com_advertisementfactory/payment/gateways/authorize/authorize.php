<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'payment' . DS . 'TheFactoryPaymentPlugin.php');

class Authorize extends TheFactoryPaymentPlugin
{
    function step1()
    {
        $vars = 'GET' == $_SERVER['REQUEST_METHOD'] ? @$_SESSION['authorize_vars'] : array();

        ?>

        <script>
            function validation() {
                // Check required fields
                var errors = false;
                var required = ['authorize_card_num', 'authorize_exp_date'];

                for (var i = 0, count = required.length; i < count; i++) {
                    var el = document.getElementById(required[i]);
                    el.className = '';

                    if ('' == el.value) {
                        errors = true;
                        el.className = 'field-required';
                    }
                }

                if (errors) {
                    return false;
                }

                return true;
            }
        </script>

        <style>
            .warning {
                color: #ff0000;
                font-weight: bold;
                margin-left: 2px;
                font-size: 20px;
            }

            .field-required {
                border-color: #ff0000;
            }

            .authorize td {
                vertical-align: top;
            }

            .info {
                color: #999999;
            }
        </style>

        <form action="index.php" method="POST" onsubmit="return validation();">

            <h1><?php echo JText::_('PAYMENT_AUTHORIZE_CREDIT_CARD_DETAILS'); ?></h1>

            <table class="authorize">

                <!-- Billing details -->
                <tr>
                    <th colspan="2"><?php echo JText::_('PAYMENT_AUTHORIZE_ENTER_BILLING_DETAILS'); ?></th>
                </tr>

                <tr>
                    <td><label for="authorize_card_num"><?php echo JText::_('PAYMENT_AUTHORIZE_CREDIT_CARD_NUMBER'); ?>:<span
                                    class="warning">*</span></label></td>
                    <td><input id="authorize_card_num" name="authorize_card_num" type="text" maxlength="20"
                               value="<?php echo @$vars['authorize_card_num']; ?>"/></td>
                </tr>

                <tr>
                    <td><label for="authorize_exp_date"><?php echo JText::_('PAYMENT_AUTHORIZE_EXPIRATION_DATE'); ?>
                            :<span class="warning">*</span></label></td>
                    <td>
                        <input id="authorize_exp_date" name="authorize_exp_date" type="text" maxlength="20"
                               value="<?php echo @$vars['authorize_exp_date']; ?>"/>
                        <div class="info"><?php echo JText::_('PAYMENT_AUTHORIZE_DATE_FORMAT'); ?></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="padding-top: 20px;">
                        <input type="submit" value="<?php echo JText::_('PAYMENT_AUTHORIZE_CONFIRM'); ?>"/>
                        <span class="warning">*</span> <?php echo JText::_('PAYMENT_AUTHORIZE_MARKED_FIELDS_ARE_REQUIRED'); ?>
                    </td>
                </tr>
            </table>

            <input type="hidden" name="step" value="2"/>

            <input type="hidden" name="option" value="com_advertisementfactory"/>
            <input type="hidden" name="task" value="purchase.gateway"/>
            <input type="hidden" name="method" value="<?php echo $this->title; ?>"/>
        </form>

        <?php
    }

    function step2()
    {
        // Get the values
        $vars = array();

        $vars['authorize_card_num'] = JFactory::getApplication()->input->getString('authorize_card_num', '');
        $vars['authorize_exp_date'] = JFactory::getApplication()->input->getString('authorize_exp_date', '');

        // Check for errors
        $errors = $this->validateAuthorize($vars);

        // If errors were found, show the form again
        if (count($errors)) {
            // Store the values in session, so we can fill in the fields
            $_SESSION['authorize_vars'] = $vars;

            // Show the error
            $this->showError(implode('<br />', $errors));

            // Redirect to step 1
            $this->redirectToStep(1);
        }

        // No errors found, create a new order
        $order = $this->createOrder();

        $post_values = array(
            'x_login'    => $this->getParam('api_login'),
            'x_tran_key' => $this->getParam('transaction_key'),

            'x_version'        => '3.1',
            'x_delim_data'     => 'TRUE',
            'x_delim_char'     => '|',
            'x_relay_response' => 'FALSE',
            'x_test_request'   => $this->getParam('test_mode'),

            'x_type'     => 'AUTH_CAPTURE',
            'x_method'   => 'CC',
            'x_card_num' => $vars['authorize_card_num'],
            'x_exp_date' => $vars['authorize_exp_date'],

            'x_amount'      => $order->amount,
            //'x_currency_code' => 'EUR',
            'x_description' => $order->title,
            'x_invoice_num' => $order->id,
            'x_description' => $order->title,

            'x_first_name' => '',
            'x_last_name'  => '',
            'x_address'    => '',
            'x_state'      => '',
            'x_zip'        => '',
        );

        $post_string = "";
        foreach ($post_values as $key => $value) {
            $post_string .= "$key=" . urlencode($value) . "&";
        }
        $post_string = rtrim($post_string, "& ");

        $request = curl_init($this->getAction()); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
        $post_response = curl_exec($request); // execute curl post and store results in $post_response
        curl_close($request); // close curl object

        // This line takes the response and breaks it into an array using the specified delimiting character
        $response_array = explode($post_values['x_delim_char'], $post_response);

        $this->processIpn($response_array, $order);
    }

    function processIpn($response, $order)
    {
        $ipn = array();

        // Get values from request
        $ipn['refnumber'] = trim(stripslashes($response[6]));
        $ipn['order_title'] = trim(stripslashes($response[8]));
        $ipn['order_id'] = trim(stripslashes($response[7]));
        $ipn['status'] = trim(stripslashes($response[0]));
        $ipn['amount'] = trim(stripslashes($response[9]));

        $payment = $this->createPayment($response);

        $payment->amount = $ipn['amount'];
        $payment->order_id = $ipn['order_id'];
        $payment->refnumber = $ipn['refnumber'];
        $payment->user_id = $order->user_id;

        // Check for errors
        $this->errors = $this->validatePayment();

        switch ($ipn['status']) {
            case '1':
                $payment->status = count($this->errors) ? 'manual-check' : 'ok';
                break;

            case '2':
                $payment->status = 'error';
                break;

            default:
                $payment->status = 'manual-check';
                break;
        }

        $this->savePayment($payment);

        if ($payment->status == 'ok') {
            $this->redirect($this->payment_accepted_url);
        } else {
            if ($ipn['status'] != 1) {
                $this->showError($response[3]);
            } else {
                $this->showError(implode('<br />', $this->errors));
            }

            $this->redirect($this->payment_failed_url);
        }
    }

    function validatePayment($ipn)
    {
        $errors = array();

        // Validate Order
        $order = $this->getOrder($ipn['order_id']);

        if ($order) {
            if ('pending' == $order->status) {
                // Validate amount
                if ($order->amount != $ipn['amount']) {
                    $errors[] = JText::_('Payment amount received is different from expected');
                }
            } else {
                $errors[] = JText::_('Order already processed');
            }
        } else {
            $errors[] = JText::_('Order not found');
        }

        return $errors;
    }

    protected function getAction()
    {
        if ($this->getParam('test_mode')) {
            return $this->getParam('action_test');
        }

        return $this->getParam('action');
    }

    protected function validateAuthorize($vars)
    {
        $errors = array();

        if ('' == $vars['authorize_card_num']) {
            $errors[] = JText::_('PAYMENT_AUTHORIZE_ENTER_CREDIT_CARD_NUMBER');
        }

        if ('' == $vars['authorize_exp_date']) {
            $errors[] = JText::_('PAYMENT_AUTHORIZE_ENTER_EXPIRATION_DATE');
        }

        return $errors;
    }
}
