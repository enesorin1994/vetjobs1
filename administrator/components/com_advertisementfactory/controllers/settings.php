<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

class AdvertisementFactoryControllerSettings extends JControllerLegacy
{
    public function __construct()
    {
        parent::__construct();

        $this->registerTask('apply', 'save');
    }

    public function save()
    {
        $model = $this->getModel('Settings');
        $form = $model->getForm();
        $data = $this->input->get('jform', array(), 'array');

        /* @var $model AdvertisementFactoryModelSettings */
        $data = $model->validate($form, $data);

        if ($model->save($data)) {
            $msg = JText::_('COM_ADVERTISEMENTFACTORY_SAVE_SETTINGS_SUCCESS');
        } else {
            throw new Exception($model->getError(), 500);
            $msg = JText::_('COM_ADVERTISEMENTFACTORY_SAVE_SETTINGS_FAILURE');
        }

        if ('apply' == $this->getTask()) {
            $link = 'index.php?option=com_advertisementfactory&view=settings';
        } else {
            $link = 'index.php?option=com_advertisementfactory';
        }

        $this->setRedirect($link, $msg);

        return true;
    }

    public function cancel()
    {
        $this->setRedirect('index.php?option=com_advertisementfactory');

        return true;
    }
}
