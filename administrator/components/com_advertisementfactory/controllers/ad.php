<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

class AdvertisementFactoryControllerAd extends JControllerForm
{
    public function updateForm()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Initialise variables.
        $app = JFactory::getApplication();
        $lang = JFactory::getLanguage();
        $model = $this->getModel();
        $table = $model->getTable();
        $data = $this->input->get('jform', array(), 'array');
        $checkin = property_exists($table, 'checked_out');
        $context = "$this->option.edit.$this->context";
        $task = $this->getTask();

        // Determine the name of the primary key for the data.
        if (empty($key)) {
            $key = $table->getKeyName();
        }

        // The urlVar may be different from the primary key to avoid data collisions.
        if (empty($urlVar)) {
            $urlVar = $key;
        }

        $recordId = $this->input->getInt($urlVar);

        $session = JFactory::getSession();
        $registry = $session->get('registry');

        // Populate the row id from the session.
        $data[$key] = $recordId;

        // Validate the posted data.
        // Sometimes the form needs some posted data, such as for plugins and modules.
        $form = $model->getForm($data, false);

        if (!$form) {
            $app->enqueueMessage($model->getError(), 'error');

            return false;
        }

        $old_data = $app->getUserState($context . '.data');
        if (!is_null($old_data)) {
            if ($old_data['type'] != $data['type']) {
                $data['module_id'] = '';
            }
        } else {
            $table->load($recordId);

            if ($table->type != $data['type']) {
                $data['module_id'] = '';
            }
        }

        // Save the data in the session.
        $app->setUserState($context . '.data', $data);

        // Redirect back to the edit screen.
        $this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId, $key), false));

        return true;
    }

    public function sendMessage()
    {
        $language = JFactory::getLanguage();
        $language->load('com_advertisementfactory', JPATH_SITE);

        JModelLegacy::addIncludePath(JPATH_COMPONENT_SITE . DS . 'models');

        $id = $this->input->getInt('id');
        $message = $this->input->getString('message');
        $model = $this->getModel('Message', 'AdvertisementFactoryModel');

        if ($model->send($message, $id)) {
            $msg = JText::_('COM_ADVERTISEMENTFACTORY_MESSAGE_SEND_SUCCESS');
        } else {
            $msg = JText::_('COM_ADVERTISEMENTFACTORY_MESSAGE_SEND_ERROR');
            throw new Exception($model->getError(), 500);
        }

        $this->setRedirect(JRoute::_('index.php?option=com_advertisementfactory&task=ad.edit&id=' . $id, false), $msg);
    }
}
