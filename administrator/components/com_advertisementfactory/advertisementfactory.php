<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

// Include dependancies
jimport('joomla.application.component.controller');
jimport('joomla.application.component.model');

if (!JFactory::getUser()->authorise('backend.manage', 'com_advertisementfactory')) {
    throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'), 403);
}

JLoader::discover('Factory', JPATH_ADMINISTRATOR . '/components/com_advertisementfactory/lib');

JModelLegacy::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_advertisementfactory' . DS . 'models');
$model = JModelLegacy::getInstance('ServeAd', 'AdvertisementFactoryModel');
$model->checkExpired();

// Execute the task.
$controller = JControllerLegacy::getInstance('AdvertisementFactory');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
