<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

class AdvertisementFactoryHelper
{
    public static function checkPermissions()
    {
        $input = JFactory::getApplication()->input;

        $view = $input->getCmd('view');
        $task = $input->getCmd('task');

        $xml = simplexml_load_file(JPATH_COMPONENT_SITE . '/permissions.xml');

        $paths = array(
            '/permissions/permission/view[text()="' . $view . '"]/ancestor::permission',
            '/permissions/permission/task[text()="' . $task . '"]/ancestor::permission',
        );

        if (false !== strpos($task, '.')) {
            list($controller, $task) = explode('.', $task);

            $paths[] = '/permissions/permission/controller[text()="' . $controller . '"]/ancestor::permission';
        }

        $permissions = array();
        $query = $xml->xpath(implode(' | ', $paths));

        foreach ($query as $permission) {
            $permission = (string)$permission->attributes()->type;

            if (!in_array($permission, $permissions)) {
                $permissions[] = $permission;
            }
        }

        if (!$permissions) {
            return true;
        }

        foreach ($permissions as $permission) {
            self::checkPermission($permission);
        }
    }

    protected static function checkPermission($permission)
    {
        $user = JFactory::getUser();
        $app = JFactory::getApplication();

        $redirect = null;
        $message = null;

        switch ($permission) {
            case 'frontend_manage':
                if ($user->guest) {
                    $return = base64_encode(JUri::getInstance()->current());
                    $redirect = JRoute::_('index.php?option=com_users&view=login&return=' . $return, false);
                    $message = JText::_('COM_ADVERTISEMENTFACTORY_PERMISSION_LOGGED_MESSAGE');
                } elseif (!JFactory::getUser()->authorise('frontend.manage', 'com_advertisementfactory')) {
                    $redirect = JUri::root();
                    $message = JText::_('COM_ADVERTISEMENTFACTORY_PERMISSION_NOT_AUTH');
                }
                break;
        }

        if ($redirect) {
            if (!self::isXmlHttpRequest()) {
                $app->redirect($redirect, $message);
            }

            echo json_encode(array('status' => 0, 'message' => $message));
            jexit();
        }
    }

    protected static function isXmlHttpRequest()
    {
        return JFactory::getApplication()->input->server->get('HTTP_X_REQUESTED_WITH', '') == 'XMLHttpRequest';
    }
}
