DROP TABLE IF EXISTS `#__advertisementfactory_ads`;
DROP TABLE IF EXISTS `#__advertisementfactory_gateways`;
DROP TABLE IF EXISTS `#__advertisementfactory_orders`;
DROP TABLE IF EXISTS `#__advertisementfactory_payments`;
DROP TABLE IF EXISTS `#__advertisementfactory_prices`;
DROP TABLE IF EXISTS `#__advertisementfactory_messages`;
DROP TABLE IF EXISTS `#__advertisementfactory_notifications`;
DROP TABLE IF EXISTS `#__advertisementfactory_track`;
