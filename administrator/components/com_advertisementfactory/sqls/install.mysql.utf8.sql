DROP TABLE IF EXISTS `#__advertisementfactory_ads`;
CREATE TABLE `#__advertisementfactory_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `contents` mediumtext NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `url` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `module_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `params` mediumtext NOT NULL,
  `history` mediumtext NOT NULL,
  `price_type` varchar(20) NOT NULL,
  `purchased` int(11) NOT NULL,
  `served` int(11) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__advertisementfactory_gateways`;
CREATE TABLE `#__advertisementfactory_gateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '',
  `enabled` tinyint(4) DEFAULT '1',
  `logo` varchar(255) DEFAULT NULL,
  `params` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__advertisementfactory_orders`;
CREATE TABLE `#__advertisementfactory_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `price_saved` mediumtext NOT NULL,
  `params` mediumtext NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currency` varchar(20) NOT NULL,
  `gateway` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__advertisementfactory_payments`;
CREATE TABLE `#__advertisementfactory_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `received_at` datetime NOT NULL,
  `payment_date` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `gateway` varchar(255) NOT NULL,
  `log` mediumtext NOT NULL,
  `refnumber` varchar(255) NOT NULL,
  `errors` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__advertisementfactory_prices`;
CREATE TABLE `#__advertisementfactory_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `price_type` varchar(20) NOT NULL,
  `value` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `module_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__advertisementfactory_messages`;
CREATE TABLE `#__advertisementfactory_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` mediumtext NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__advertisementfactory_notifications`;
CREATE TABLE `#__advertisementfactory_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` mediumtext NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `#__advertisementfactory_track`;
CREATE TABLE `#__advertisementfactory_track` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `referer` varchar(400) NOT NULL,
  `url` varchar(400) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ad_id` (`ad_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_ip` (`ip`),
  KEY `idx_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
