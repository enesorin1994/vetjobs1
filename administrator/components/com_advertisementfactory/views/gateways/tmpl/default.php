<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
        <form action="index.php?option=com_advertisementfactory&view=gateways" method="post" name="adminForm"
              id="adminForm">

            <table class="adminlist table table-striped table-hover">

                <thead>
                <tr>
                    <th>
                        <?php echo JText::_('COM_ADVERTISEMENTFACTORY_GATEWAY_TITLE'); ?>
                    </th>
                    <th width="10%">
                        <?php echo JText::_('COM_ADVERTISEMENTFACTORY_GATEWAY_PUBLISHED'); ?>
                    </th>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td colspan="12"></td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($this->items as $i => $item): ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <td>
                        <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&task=gateway.edit&id=' . $item->id); ?>">
                            <?php echo $this->escape($item->title); ?>
                        </a>
                    </td>
                    <td style="text-align: center;">
                        <?php echo JHtml::_('jgrid.published', $item->enabled, '', '', false); ?>
                    </td>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <div>
                <input type="hidden" name="task" value=""/>
                <?php echo JHtml::_('form.token'); ?>
            </div>
        </form>
    </div>
</div>
