<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewStatistics extends FactoryView
{
    public function display($tpl = null)
    {
        $this->items = $this->get('Items');
        $this->state = $this->get('State');
        $this->pagination = $this->get('Pagination');
        $this->ad = $this->get('Ad');

        $this->listDirn = $this->state->get('list.direction');
        $this->listOrder = $this->state->get('list.ordering');

        $this->addToolbar();

        JHtml::_('behavior.framework');

        parent::display($tpl);
    }

    protected function addToolbar()
    {
        JToolbarHelper::title('Statistics');

        JToolbarHelper::back('JTOOLBAR_BACK', 'index.php?option=com_advertisementfactory&task=ad.edit&id=' . $this->ad->id);
    }
}
