<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">

        <form action="index.php?option=com_advertisementfactory&view=statistics&id=<?php echo $this->ad->id; ?>"
              method="post" name="adminForm" id="adminForm">
            <div class="clr"></div>

            <table class="adminlist table table-striped table-hover">

                <thead>
                <tr>
                    <th width="150px"><?php echo JText::_('Date'); ?></th>
                    <th width="150px"><?php echo JText::_('User / Ip Address'); ?></th>
                    <th width="150px"><?php echo JText::_('Type'); ?></th>
                    <th><?php echo JText::_('Referer / Url'); ?></th>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td colspan="12">
                        <?php echo $this->pagination->getListFooter(); ?>
                    </td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($this->items as $i => $item): ?>
                    <tr class="row<?php echo $i % 2; ?>">
                        <td><?php echo JHtml::_('date', $item->created_at, 'DATE_FORMAT_LC2'); ?></td>
                        <td>
                            <?php if ($item->user_id): ?>
                                <?php echo $item->username; ?>
                            <?php else: ?>
                                <?php echo $item->ip; ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php echo ucfirst($item->type); ?>
                        </td>
                        <td>
                            <?php if ('click' == $item->type): ?>
                                <a href="<?php echo $item->url; ?>"><?php echo JHtml::_('string.abridge', $item->url); ?></a>
                            <?php else: ?>
                                <a href="<?php echo $item->referer; ?>"><?php echo JHtml::_('string.abridge', $item->referer); ?></a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <div>
                <input type="hidden" name="task" value=""/>
                <input type="hidden" name="boxchecked" value="0"/>
                <input type="hidden" name="filter_order" value="<?php echo $this->state->get('list.ordering'); ?>"/>
                <input type="hidden" name="filter_order_Dir"
                       value="<?php echo $this->state->get('list.direction'); ?>"/>
                <?php echo JHtml::_('form.token'); ?>
            </div>
        </form>
    </div>
</div>
