<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewPayment extends FactoryView
{
    public function display($tpl = null)
    {
        // Initialiase variables.
        $this->form = $this->get('Form');
        $this->item = $this->get('Item');
        $this->state = $this->get('State');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors), 500);
        }

        $this->addToolbar();

        JHtml::_('behavior.tooltip');
        JHtml::_('behavior.formvalidation');

        parent::display($tpl);
    }

    protected function addToolbar()
    {
//		JRequest::setVar('hidemainmenu', true);

        JToolBarHelper::title(JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_PAYMENT_EDIT'));

        if ('manual-check' == $this->item->status) {
            JToolBarHelper::apply('payment.apply', 'JTOOLBAR_APPLY');
            JToolBarHelper::save('payment.save', 'JTOOLBAR_SAVE');
        }

        JToolBarHelper::cancel('payment.cancel', 'JTOOLBAR_CANCEL');
    }
}
