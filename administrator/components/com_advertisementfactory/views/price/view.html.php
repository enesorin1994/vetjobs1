<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewPrice extends FactoryView
{
    protected $form;
    protected $item;
    protected $state;

    public function display($tpl = null)
    {
        // Initialiase variables.
        $this->form = $this->get('Form');
        $this->item = $this->get('Item');
        $this->state = $this->get('State');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors), 500);
        }

        $this->addToolbar();

        JHtml::_('behavior.tooltip');
        JHtml::_('behavior.formvalidation');

        parent::display($tpl);
    }

    protected function addToolbar()
    {
//		JRequest::setVar('hidemainmenu', true);

        $isNew = ($this->item->id == 0);

        JToolBarHelper::title($isNew ? JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_PRICE_NEW') : JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_PRICE_EDIT'));

        JToolBarHelper::apply('price.apply', 'JTOOLBAR_APPLY');
        JToolBarHelper::save('price.save', 'JTOOLBAR_SAVE');
        JToolBarHelper::custom('price.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);

        if (!$isNew) {
            //JToolBarHelper::custom('price.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
        }

        if (empty($this->item->id)) {
            JToolBarHelper::cancel('price.cancel', 'JTOOLBAR_CANCEL');
        } else {
            JToolBarHelper::cancel('price.cancel', 'JTOOLBAR_CLOSE');
        }
    }
}
