<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">

        <form action="index.php?option=com_advertisementfactory&view=prices" method="post" name="adminForm"
              id="adminForm">

            <fieldset id="filter-bar">
                <div class="filter-select fltrt">

                    <select name="filter_type" class="inputbox" onchange="this.form.submit()">
                        <?php echo JHtml::_('select.options', JFormFieldAdType::getOptionsForFilter(), 'value', 'text', $this->state->get('filter.type')); ?>
                    </select>

                    <select name="filter_module_id" class="inputbox" onchange="this.form.submit()">
                        <?php echo JHtml::_('select.options', JFormFieldAdModule::getOptionsForFilter(), 'value', 'text', $this->state->get('filter.module_id')); ?>
                    </select>

                    <select name="filter_state" class="inputbox" onchange="this.form.submit()">
                        <option value=""><?php echo JText::_('JOPTION_SELECT_PUBLISHED'); ?></option>
                        <?php echo JHtml::_('select.options', JHtml::_('jgrid.publishedOptions', array('archived' => 0, 'trash' => 0, 'all' => 0)), 'value', 'text', $this->state->get('filter.state'), true); ?>
                    </select>
                </div>

            </fieldset>

            <div class="clr"></div>

            <table class="adminlist table table-striped table-hover">

                <thead>
                <tr>
                    <th width="1%">
                        <input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this)"/>
                    </th>
                    <th>
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PRICES_VALUE', 'value', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PRICES_TYPE', 'price_type', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PRICES_AD_TYPE', 'type', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PRICES_PRICE', 'price', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="20%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PRICES_MODULE', 'm.title', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="5%">
                        <?php echo JHtml::_('grid.sort', 'JPUBLISHED', 'state', $this->listDirn, $this->listOrder); ?>
                    </th>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td colspan="12">
                        <?php echo $this->pagination->getListFooter(); ?>
                    </td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($this->items as $i => $item): ?>
                    <tr class="row<?php echo $i % 2; ?>">
                        <td class="center">
                            <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                        </td>
                        <td>
                            <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&task=price.edit&id=' . (int)$item->id); ?>">
                                <?php echo $this->escape($item->value); ?>
                            </a>
                        </td>
                        <td>
                            <?php echo JText::_('COM_ADVERTISEMENTDACTORY_PRICE_TYPE_' . $item->price_type); ?>
                        </td>
                        <td>
                            <?php echo $item->type; ?>
                        </td>
                        <td class="center">
                            <?php echo $item->price; ?>
                        </td>
                        <td class="center">
                            <?php echo $item->module_title; ?>
                        </td>
                        <td class="center">
                            <?php echo JHtml::_('jgrid.published', $item->state, $i, 'prices.', true); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <div>
                <input type="hidden" name="task" value=""/>
                <input type="hidden" name="boxchecked" value="0"/>
                <input type="hidden" name="filter_order" value="<?php echo $this->state->get('list.ordering'); ?>"/>
                <input type="hidden" name="filter_order_Dir"
                       value="<?php echo $this->state->get('list.direction'); ?>"/>
                <?php echo JHtml::_('form.token'); ?>
            </div>
        </form>

    </div>
</div>
