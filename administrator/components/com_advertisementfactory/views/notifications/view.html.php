<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewNotifications extends FactoryView
{
    public function display($tpl = null)
    {
        $this->items = $this->get('Items');
        $this->state = $this->get('State');
        $this->pagination = $this->get('Pagination');

        $this->filterOptionsType = $this->get('FilterOptionsType');

        $this->listDirn = $this->state->get('list.direction');
        $this->listOrder = $this->state->get('list.ordering');

        $this->addToolbar();

        parent::display($tpl);
    }

    protected function addToolbar()
    {
        JToolBarHelper::title(JText::_('COM_ADVERTISEMENTFACTORY_NOTIFICATIONS_PAGE_TITLE'));

        JToolBarHelper::addNew('notification.add', 'JTOOLBAR_NEW');
        JToolBarHelper::editList('notification.edit', 'JTOOLBAR_EDIT');

        JToolBarHelper::divider();

        JToolBarHelper::custom('notifications.publish', 'publish.png', 'publish_f2.png', 'Publish', true);
        JToolBarHelper::custom('notifications.unpublish', 'unpublish.png', 'unpublish_f2.png', 'Unpublish', true);

        JToolBarHelper::deleteList('', 'notifications.delete', 'JTOOLBAR_DELETE');
    }
}
