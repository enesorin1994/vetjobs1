<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewGateway extends FactoryView
{
    public function display($tpl = null)
    {
        $this->item = $this->get('Item');
        $this->form = $this->get('Form');

        JHtml::_('behavior.tooltip');

        $this->addToolbar();

        parent::display($tpl);
    }

    protected function addToolbar()
    {
        JToolBarHelper::title(JText::sprintf('COM_ADVERTISEMENTFACTORY_GATEWAY_EDIT', $this->item->title));

        JToolBarHelper::apply('gateway.apply', 'JTOOLBAR_APPLY');
        JToolBarHelper::save('gateway.save', 'JTOOLBAR_SAVE');
        JToolBarHelper::cancel('gateway.cancel', 'JTOOLBAR_CANCEL');
    }
}
