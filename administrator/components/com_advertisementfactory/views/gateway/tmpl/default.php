<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div id="j-sidebar-container" class="span2">
    <?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
    <form action="<?php echo JRoute::_('index.php?option=com_advertisementfactory&layout=edit&id=' . (int)$this->item->id); ?>"
          method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
        <div class="width-60 fltlft">
            <fieldset class="adminform">
                <legend><?php echo JText::_('COM_ADVERTISEMENTFACTORY_GATEWAY_DETAILS'); ?></legend>
                <ul class="adminformlist">
                    <?php foreach ($this->form->getFieldset() as $field): ?>
                        <li><?php echo $this->form->renderField($field->fieldname, $field->group); ?></li>
                    <?php endforeach; ?>
                </ul>
                <div class="clr"></div>

            </fieldset>
        </div>

        <input type="hidden" name="task" value=""/>
        <?php echo JHtml::_('form.token'); ?>
    </form>
</div>
