<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<style>
    .ok {
        color: green;
    }

    .manual-check {
        color: orange;
    }

    .error {
        color: red;
    }
</style>

<div>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">

        <form action="index.php?option=com_advertisementfactory&view=payments" method="post" name="adminForm"
              id="adminForm">

            <fieldset id="filter-bar">

                <div class="filter-select fltrt">
                    <select name="filter_status" class="inputbox" onchange="this.form.submit()">
                        <option value=""><?php echo JText::_('COM_ADVERTISEMENTFACTORY_PAYMENTS_FILTER_STATUS_SELECT'); ?></option>
                        <?php echo JHtml::_(
                            'select.options',
                            array(
                                'ok'           => 'ok',
                                'manual-check' => 'manual-check',
                                'error'        => 'error'),
                            'value',
                            'text',
                            $this->state->get('filter.status')); ?>
                    </select>
                </div>

                <div class="filter-select fltrt">
                    <select name="filter_method" class="inputbox" onchange="this.form.submit()">
                        <option value=""><?php echo JText::_('COM_ADVERTISEMENTFACTORY_PAYMENTS_FILTER_METHOD_SELECT'); ?></option>
                        <?php echo JHtml::_('select.options', $this->methods, 'value', 'text', $this->state->get('filter.method')); ?>
                    </select>
                </div>

            </fieldset>

            <div class="clr"></div>

            <table class="adminlist  table table-striped table-hover">

                <thead>
                <tr>
                    <th width="1%">
                        <input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this)"/>
                    </th>
                    <th width="5%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PAYMENTS_ID', 'id', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th>
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PAYMENTS_REF', 'refnumber', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PAYMENTS_USERNAME', 'username', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="15%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PAYMENTS_RECEIVED_AT', 'received_at', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PAYMENTS_ORDER_ID', 'order_id', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PAYMENTS_AMOUNT', 'amount', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PAYMENTS_METHOD', 'gateway', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_PAYMENTS_STATUS', 'status', $this->listDirn, $this->listOrder); ?>
                    </th>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td colspan="12">
                        <?php echo $this->pagination->getListFooter(); ?>
                    </td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($this->items as $i => $item): ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="center">
                        <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                    </td>
                    <td><?php echo $item->id; ?></td>
                    <td>
                        <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&task=payment.edit&id=' . (int)$item->id); ?>">
                            <?php echo $this->escape($item->refnumber); ?>
                        </a>
                    </td>
                    <td><?php echo $item->username; ?></td>
                    <td style="text-align: center;"><?php echo $item->received_at; ?></td>
                    <td style="text-align: center;"><?php echo $item->order_id; ?></td>
                    <td style="text-align: center;"><?php echo $item->amount; ?><?php echo $item->currency; ?></td>
                    <td style="text-align: center;"><?php echo $item->gateway; ?></td>
                    <td style="text-align: center; font-weight: bold;"
                        class="<?php echo $item->status; ?>"><?php echo $item->status; ?></td>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <div>
                <input type="hidden" name="task" value=""/>
                <input type="hidden" name="boxchecked" value="0"/>
                <input type="hidden" name="filter_order" value="<?php echo $this->state->get('list.ordering'); ?>"/>
                <input type="hidden" name="filter_order_Dir"
                       value="<?php echo $this->state->get('list.direction'); ?>"/>
                <?php echo JHtml::_('form.token'); ?>
            </div>
        </form>
    </div>
</div>
