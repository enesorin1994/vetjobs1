<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewPayments extends FactoryView
{
    public function display($tpl = null)
    {
        $this->items = $this->get('Items');
        $this->state = $this->get('State');
        $this->pagination = $this->get('Pagination');
        $this->methods = $this->get('Methods');

        $this->listDirn = $this->state->get('list.direction');
        $this->listOrder = $this->state->get('list.ordering');

        $this->addToolbar();

        parent::display($tpl);
    }

    protected function addToolbar()
    {
        JToolBarHelper::title(JText::_('COM_ADVERTISEMENTFACTORY_PAYMENTS_PAGE_TITLE'));

        JToolBarHelper::custom('payment.gateways', 'creditcard_paypal', 'creditcard_paypal', 'COM_ADVERTISEMENTFACTORY_PAYMENTS_GATEWAYS', false);
        JToolBarHelper::divider();
        JToolBarHelper::editList('payment.edit', 'JTOOLBAR_EDIT');
        JToolBarHelper::divider();
        JToolBarHelper::deleteList('', 'payments.delete', 'JTOOLBAR_DELETE');

        $document = JFactory::getDocument();
        $document->addStyleDeclaration('.icon-32-creditcard_paypal { background: url("' . JURI::root() . 'media/com_advertisementfactory/assets/frontend/images/creditcard_paypal.png") center center no-repeat; width: 48px !important; }');
    }
}
