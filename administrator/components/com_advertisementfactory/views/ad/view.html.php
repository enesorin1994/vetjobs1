<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewAd extends FactoryView
{
    protected $form;
    protected $item;
    protected $state;

    public function display($tpl = null)
    {
        // Initialiase variables.
        $this->form = $this->get('Form');
        $this->item = $this->get('Item');
        $this->state = $this->get('State');
        $this->messages = $this->get('Messages');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors), 500);
        }

        $this->addToolbar();

        JHtml::_('behavior.tooltip');
        JHtml::_('behavior.formvalidation');

        $language = JFactory::getLanguage();
        $language->load('com_advertisementfactory', JPATH_SITE);
        JHtml::stylesheet('media/com_advertisementfactory/assets/frontend/css/views/myad.css');

        parent::display($tpl);
    }

    protected function addToolbar()
    {
//		JRequest::setVar('hidemainmenu', true);

        $isNew = ($this->item->id == 0);

        JToolBarHelper::title($isNew ? JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_AD_NEW') : JText::sprintf('COM_ADVERTISEMENTFACTORY_MANAGER_AD_EDIT', $this->item->name));

        JToolBarHelper::apply('ad.apply', 'JTOOLBAR_APPLY');
        JToolBarHelper::save('ad.save', 'JTOOLBAR_SAVE');
        JToolBarHelper::custom('ad.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);

        if (!$isNew) {
            //JToolBarHelper::custom('ad.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
        }

        if (empty($this->item->id)) {
            JToolBarHelper::cancel('ad.cancel', 'JTOOLBAR_CANCEL');
        } else {
            JToolBarHelper::cancel('ad.cancel', 'JTOOLBAR_CLOSE');
        }
    }
}
