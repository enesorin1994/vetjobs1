<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div id="j-sidebar-container" class="span2">
    <?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">

    <form action="<?php echo JRoute::_('index.php?option=com_advertisementfactory&layout=edit&id=' . (int)$this->item->id); ?>"
          method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
        <div class="width-60 fltlft">
            <fieldset class="adminform">
                <legend><?php echo empty($this->item->id) ? JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_AD_NEW') : JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_AD_DETAILS'); ?></legend>
                <ul class="adminformlist">

                    <?php foreach (array('name', 'id', 'type', 'module_id', 'title', 'contents', 'keywords', 'url', 'filename', 'user_id') as $field): ?>
                        <li><?php echo $this->form->renderField($field); ?></li>
                    <?php endforeach; ?>

                    <?php echo $this->form->renderFieldset('params'); ?>
                </ul>

                <div class="clr"></div>

            </fieldset>
        </div>

        <div class="width-40 fltrt">
            <?php echo JHtml::_('sliders.start', 'ad-sliders-' . $this->item->id, array('useCookie' => 1)); ?>

            <?php echo JHtml::_('sliders.panel', JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_AD_STATE'), 'state-details'); ?>
            <fieldset class="panelform">
                <ul class="adminformlist">
                    <?php foreach (array('state', 'approved', 'reason', 'paid', 'payment_id') as $field): ?>
                        <li><?php echo $this->form->renderField($field); ?></li>
                    <?php endforeach; ?>
                </ul>
            </fieldset>

            <?php echo JHtml::_('sliders.panel', JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_AD_PUBLISHING'), 'publishing-details'); ?>
            <fieldset class="panelform">
                <ul class="adminformlist">
                    <?php foreach (array('price_type', 'purchased', 'served', 'publish_up', 'publish_down') as $field): ?>
                        <li><?php echo $this->form->renderField($field); ?></li>
                    <?php endforeach; ?>

                    <li><a class="btn btn-small"
                           href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&view=statistics&id=' . $this->item->id); ?>"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_AD_STATISTICS'); ?></a>
                    </li>
                </ul>
            </fieldset>

            <?php echo JHtml::_('sliders.panel', JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_AD_HISTORY'), 'history-details'); ?>
            <fieldset class="panelform">
                <ul class="adminformlist">
                    <li><?php echo $this->form->getLabel('history'); ?>
                        <div class="clr"></div>
                        <?php echo $this->form->getInput('history'); ?></li>
                </ul>
            </fieldset>

            <?php echo JHtml::_('sliders.panel', JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_AD_DESCRIPTION'), 'description-details'); ?>
            <fieldset class="panelform">
                <ul class="adminformlist">
                    <li><?php echo $this->form->renderField('description'); ?></li>
                </ul>
            </fieldset>

            <?php echo JHtml::_('sliders.end'); ?>
        </div>

        <div class="clr"></div>

        <input type="hidden" name="task" value=""/>
        <?php echo JHtml::_('form.token'); ?>
    </form>

    <?php if ($this->item->id): ?>
        <?php echo $this->loadTemplate('messages'); ?>
    <?php endif; ?>

</div>
