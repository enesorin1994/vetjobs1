<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewSettings extends FactoryView
{
    public function display($tpl = null)
    {
        // Initialiase variables.
        $this->form = $this->get('Form');
        $this->status = $this->get('Status');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors), 500);
        }

        $this->addToolbar();

        JHtml::_('behavior.tooltip');
        JHtml::_('behavior.formvalidation');

        $this->fieldSets = $this->form->getFieldsets();

        parent::display($tpl);
    }

    protected function addToolbar()
    {
        JToolBarHelper::title(JText::_('COM_ADVERTISEMENTFACTORY_MANAGER_SETTINGS'));

        JToolBarHelper::apply('settings.apply', 'JTOOLBAR_APPLY');
        JToolBarHelper::save('settings.save', 'JTOOLBAR_SAVE');
        JToolBarHelper::cancel('settings.cancel', 'JTOOLBAR_CLOSE');
    }

    protected function renderTemplate($template)
    {
        $this->fieldSet = $template;

        return $this->loadTemplate('tab');
    }
}
