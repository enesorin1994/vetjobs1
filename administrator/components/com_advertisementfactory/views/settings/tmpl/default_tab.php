<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="width-100">
    <fieldset class="adminform">
        <legend><?php echo JText::_('COM_ADVERTISEMENTFACTORY_SETTINGS_FIELDSET_' . strtoupper($this->fieldSet)); ?></legend>

        <ul class="adminformlist">
            <?php echo $this->form->renderFieldset($this->fieldSet); ?>
        </ul>
    </fieldset>
</div>
