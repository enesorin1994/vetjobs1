<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="width-100">
    <fieldset class="adminform">
        <legend><?php echo JText::_('COM_ADVERTISEMENTFACTORY_SETTINGS_FIELDSET_STATUS'); ?></legend>

        <ul class="adminformlist">
            <?php foreach ($this->status['folder'] as $folder => $status): ?>
                <li style="margin-bottom: 5px;">
                    <?php echo JText::sprintf(
                        'COM_ADVERTISEMENTFACTORY_SETTINGS_STATUS_' . strtoupper($folder) . '_FOLDER_' . ($status->exists ? 'TRUE' : 'FALSE'),
                        $status->path,
                        $status->exists); ?>
                </li>
            <?php endforeach; ?>

            <?php foreach ($this->status['plugin'] as $plugin => $status): ?>
                <li style="margin-bottom: 5px;">
                    <?php if (!$status->installed): ?>
                        <?php echo JText::sprintf(
                            'COM_ADVERTISEMENTFACTORY_SETTINGS_STATUS_PLUGIN_NOT_INSTALLED', $status->plugin); ?>
                    <?php else: ?>
                        <?php echo JText::sprintf(
                            'COM_ADVERTISEMENTFACTORY_SETTINGS_STATUS_PLUGIN_ENABLED_' . ($status->enabled ? 'TRUE' : 'FALSE'),
                            $status->plugin); ?>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>

            <li style="margin-bottom: 5px;">
                <?php echo JText::_('COM_ADVERTISEMENTFACTORY_SETTINGS_STATUS_GATEWAY_' . $this->status['gateways']); ?>
            </li>

        </ul>
    </fieldset>
</div>
