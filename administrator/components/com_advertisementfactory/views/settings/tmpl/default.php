<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<style>
    div.current fieldset {
        border: 1px solid #CCCCCC;
    }

    div.current label, div.current span.faux-label {
        margin-top: 5px;
    }

    b.valid {
        color: darkgreen;
    }

    b.warning {
        color: #ff0000;
    }
</style>

<div>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">

        <form action="<?php echo JRoute::_('index.php?option=com_advertisementfactory'); ?>" id="adminForm"
              method="post" name="adminForm" class="form-validate">

            <?php echo JHtml::_('bootstrap.startTabSet', 'settings-tabs', array('active' => 'general')); ?>
            <?php echo JHtml::_('bootstrap.addTab', 'settings-tabs', 'general', JText::_('COM_ADVERTISEMENTFACTORY_SETTINGS_TAB_GENERAL')); ?>

            <div id="page-site" class="tab">
                <div class="noshow">
                    <div class="width-40 fltlft">
                        <?php echo $this->renderTemplate('general'); ?>
                    </div>
                    <div class="width-60 fltrt">
                        <?php echo $this->loadTemplate('status'); ?>
                        <?php echo $this->renderTemplate('notifications'); ?>
                    </div>
                </div>
            </div>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <div class="clr"></div>

            <?php echo JHtml::_('bootstrap.addTab', 'settings-tabs', 'fullpageads', JText::_('COM_ADVERTISEMENTFACTORY_SETTINGS_TAB_FULL_PAGE_ADS')); ?>

            <div id="page-site" class="tab">
                <div class="noshow">
                    <div class="width-50 fltlft">
                        <?php echo $this->renderTemplate('fullpageads_general'); ?>
                    </div>

                    <div class="width-50 fltrt">
                        <?php echo $this->renderTemplate('fullpageads_registered'); ?>
                    </div>

                    <div class="width-50 fltlft">
                        <?php echo $this->renderTemplate('fullpageads_guests'); ?>
                    </div>

                </div>
            </div>

            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <div class="clr"></div>

            <?php echo JHtml::_('bootstrap.addTab', 'settings-tabs', 'modalads', JText::_('COM_ADVERTISEMENTFACTORY_SETTINGS_TAB_MODAL_ADS')); ?>

            <div id="page-site" class="tab">
                <div class="noshow">
                    <div class="width-50 fltlft">
                        <?php echo $this->renderTemplate('modalads_general'); ?>
                    </div>

                    <div class="width-50 fltrt">
                        <?php echo $this->renderTemplate('modalads_registered'); ?>
                    </div>

                    <div class="width-50 fltlft">
                        <?php echo $this->renderTemplate('modalads_guests'); ?>
                    </div>

                </div>
            </div>

            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <div class="clr"></div>

            <?php echo JHtml::_('bootstrap.addTab', 'settings-tabs', 'keywordads', JText::_('COM_ADVERTISEMENTFACTORY_SETTINGS_TAB_KEYWORD_ADS')); ?>

            <div id="page-site" class="tab">
                <div class="noshow">
                    <div class="width-50 fltlft">
                        <?php echo $this->renderTemplate('keywordads_general'); ?>
                    </div>

                </div>
            </div>

            <div class="clr"></div>

            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'settings-tabs', 'popupads', JText::_('COM_ADVERTISEMENTFACTORY_SETTINGS_TAB_POPUP_ADS')); ?>

            <div id="page-site" class="tab">
                <div class="noshow">
                    <div class="width-50 fltlft">
                        <?php echo $this->renderTemplate('popupads_general'); ?>
                    </div>

                    <div class="width-50 fltrt">
                        <?php echo $this->renderTemplate('popupads_registered'); ?>
                    </div>

                    <div class="width-50 fltlft">
                        <?php echo $this->renderTemplate('popupads_guests'); ?>
                    </div>

                </div>
            </div>

            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <div class="clr"></div>

            <?php echo JHtml::_('bootstrap.addTab', 'settings-tabs', 'permissions', JText::_('COM_ADVERTISEMENTFACTORY_SETTINGS_TAB_PERMISSIONS')); ?>
            <div id="page-site" class="tab">
                <div class="noshow">
                    <div class="width-100 fltlft">
                        <?php echo $this->renderTemplate('permissions'); ?>
                    </div>
                </div>
            </div>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <div class="clr"></div>
            <?php echo JHtml::_('bootstrap.endTabSet'); ?>

            <input type="hidden" name="task" value=""/>
            <?php echo JHtml::_('form.token'); ?>
        </form>
    </div>
</div>
