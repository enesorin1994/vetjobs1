<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
        <form action="index.php?option=com_advertisementfactory&view=ads" method="post" name="adminForm" id="adminForm">

            <fieldset id="filter-bar">

                <div class="filter-search fltlft">
                    <label class="filter-search-lbl"
                           for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
                    <input type="text" name="filter_search" id="filter_search"
                           value="<?php echo $this->escape($this->state->get('filter.search')); ?>"
                           title="<?php echo JText::_('COM_ADVERTISEMENTFACTORY_SEARCH_IN_TITLE'); ?>"/>
                    <button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
                    <button type="button"
                            onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
                </div>

                <div class="filter-select fltrt">
                    <select name="filter_type" class="inputbox" onchange="this.form.submit()">
                        <?php echo JHtml::_('select.options', JFormFieldAdType::getOptionsForFilter(), 'value', 'text', $this->state->get('filter.type')); ?>
                    </select>

                    <select name="filter_module_id" class="inputbox" onchange="this.form.submit()">
                        <?php echo JHtml::_('select.options', JFormFieldAdModule::getOptionsForFilter(), 'value', 'text', $this->state->get('filter.module_id')); ?>
                    </select>

                    <select name="filter_paid" class="inputbox" onchange="this.form.submit()">
                        <?php echo JHtml::_('select.options', JFormFieldAdPaid::getOptionsForFilter(), 'value', 'text', $this->state->get('filter.paid')); ?>
                    </select>

                    <select name="filter_state" class="inputbox" onchange="this.form.submit()">
                        <?php echo JHtml::_('select.options', JFormFieldAdState::getOptionsForFilter(), 'value', 'text', $this->state->get('filter.state')); ?>
                    </select>
                </div>

            </fieldset>

            <div class="clr"></div>

            <table class="adminlist table table-striped table-hover">

                <thead>
                <tr>
                    <th width="1%">
                        <input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this)"/>
                    </th>
                    <th>
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_ADS_NAME', 'name', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="5%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_ADS_TYPE', 'type', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="15%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_ADS_MODULE', 'm.title', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_ADVERTISEMENTFACTORY_ADS_USER', 'username', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="5%">
                        <?php echo JHtml::_('grid.sort', 'Paid', 'paid', $this->listDirn, $this->listOrder); ?>
                    </th>
                    <th width="5%">
                        <?php echo JHtml::_('grid.sort', 'Published', 'state', $this->listDirn, $this->listOrder); ?>
                    </th>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td colspan="12">
                        <?php echo $this->pagination->getListFooter(); ?>
                    </td>
                </tr>
                </tfoot>

                <tbody>
                <?php foreach ($this->items as $i => $item): ?>
                    <tr class="row<?php echo $i % 2; ?>">
                        <td class="center">
                            <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                        </td>
                        <td>
                            <a href="<?php echo JRoute::_('index.php?option=com_advertisementfactory&task=ad.edit&id=' . (int)$item->id); ?>"><?php echo $this->escape($item->name); ?></a>
                            <?php if ($item->type != 'article'): ?>
                                &nbsp;
                                <small>
                                    <small>
                                        <a href="<?php echo JRoute::_(JURI::root() . 'index.php?option=com_advertisementfactory&view=previsualize&ad_id=' . (int)$item->id . '&token=' . $this->previewToken); ?>"
                                           target="_blank" style="color: #666666;">preview</a></small>
                                </small>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php echo $item->type; ?>
                        </td>
                        <td class="center">
                            <?php echo $item->module_title; ?>
                        </td>
                        <td class="center">
                            <?php echo $item->username; ?>
                        </td>
                        <td class="center">
                            <?php echo JHtml::_('advertisementfactory.paid', $item->paid); ?>
                        </td>
                        <td class="center">
                            <?php echo JHtml::_('advertisementfactory.state', $item->state, $i, 'ads.', true); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <div>
                <input type="hidden" name="task" value=""/>
                <input type="hidden" name="boxchecked" value="0"/>
                <input type="hidden" name="filter_order" value="<?php echo $this->state->get('list.ordering'); ?>"/>
                <input type="hidden" name="filter_order_Dir"
                       value="<?php echo $this->state->get('list.direction'); ?>"/>
                <?php echo JHtml::_('form.token'); ?>
            </div>
        </form>
    </div>
</div>
