<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewAds extends FactoryView
{
    public function display($tpl = null)
    {
        $this->items = $this->get('Items');
        $this->state = $this->get('State');
        $this->pagination = $this->get('Pagination');
        $this->types = $this->get('Types');
        $this->previewToken = $this->get('PreviewToken');

        $this->listDirn = $this->state->get('list.direction');
        $this->listOrder = $this->state->get('list.ordering');

        $this->addToolbar();

        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'fields' . DS . 'adtype.php');
        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'fields' . DS . 'admodule.php');
        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'fields' . DS . 'adpaid.php');
        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'models' . DS . 'fields' . DS . 'adstate.php');
        require_once(JPATH_COMPONENT_ADMINISTRATOR . DS . 'lib' . DS . 'html' . DS . 'html.php');

        parent::display($tpl);
    }

    protected function addToolbar()
    {
        JToolBarHelper::title(JText::_('COM_ADVERTISEMENTFACTORY_ADS_PAGE_TITLE'));

        JToolBarHelper::addNew('ad.add', 'JTOOLBAR_NEW');
        JToolBarHelper::editList('ad.edit', 'JTOOLBAR_EDIT');

        JToolBarHelper::divider();

        JToolBarHelper::custom('ads.publish', 'publish.png', 'publish_f2.png', 'Publish', true);
        JToolBarHelper::custom('ads.unpublish', 'unpublish.png', 'unpublish_f2.png', 'Unpublish', true);

        JToolBarHelper::deleteList('', 'ads.delete', 'JTOOLBAR_DELETE');
    }
}
