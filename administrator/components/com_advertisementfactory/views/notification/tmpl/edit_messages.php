<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div class="width-60 fltlft">
    <fieldset class="adminform">
        <legend><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYAD_MESSAGES_TITLE'); ?></legend>

        <?php if (!$this->messages): ?>
            <?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYAD_MESSAGES_NO_MESSAGES'); ?>
        <?php else: ?>
            <?php foreach ($this->messages as $this->message): ?>
                <div class="advertisementfactory-message-title"><?php echo JText::sprintf('COM_ADVERTISEMENTFACTORY_MYAD_MESSAGE_TITLE', $this->message->username, JHtml::date($this->message->created_at)); ?></div>
                <div class="advertisementfactory-message-text"><?php echo $this->message->message; ?></div>
            <?php endforeach; ?>
        <?php endif; ?>

        <h3><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYAD_MESSAGES_YOUR_MESSAGE'); ?></h3>

        <form action="<?php echo JRoute::_('index.php?option=com_advertisementfactory&task=ad.sendmessage'); ?>"
              method="post">
            <div class="formelm"><textarea rows="5" style="width: 100%;" name="message"></textarea></div>

            <div class="formelm-buttons">
                <button type="submit"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_MYAD_SEND_MESSAGE') ?></button>
            </div>

            <input type="hidden" name="id" value="<?php echo (int)$this->item->id; ?>"/>
            <?php echo JHTML::_('form.token'); ?>
        </form>

        <div class="clr"></div>
    </fieldset>
</div>
