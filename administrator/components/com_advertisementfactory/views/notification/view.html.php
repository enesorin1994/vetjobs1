<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewNotification extends FactoryView
{
    protected $form;
    protected $item;
    protected $state;

    public function display($tpl = null)
    {
        // Initialise variables.
        $this->form = $this->get('Form');
        $this->item = $this->get('Item');
        $this->state = $this->get('State');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors), 500);
        }

        $this->addToolbar();

        JHtml::_('behavior.tooltip');
        JHtml::_('behavior.formvalidation');

        parent::display($tpl);
    }

    protected function addToolbar()
    {
//		JRequest::setVar('hidemainmenu', true);

        $isNew = ($this->item->id == 0);

        JToolBarHelper::title($isNew ? JText::_('COM_ADVERTISEMENTFACTORY_NOTIFICATION_NEW') : JText::sprintf('COM_ADVERTISEMENTFACTORY_NOTIFICATION_EDIT', $this->item->subject, $this->item->id));

        JToolBarHelper::apply('notification.apply', 'JTOOLBAR_APPLY');
        JToolBarHelper::save('notification.save', 'JTOOLBAR_SAVE');

        if (empty($isNew)) {
            JToolBarHelper::cancel('notification.cancel', 'JTOOLBAR_CANCEL');
        } else {
            JToolBarHelper::cancel('notification.cancel', 'JTOOLBAR_CLOSE');
        }
    }
}
