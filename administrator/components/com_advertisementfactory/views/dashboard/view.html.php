<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AdvertisementFactoryViewDashboard extends FactoryView
{
    public function display($tpl = null)
    {
        JToolBarHelper::title(JText::_('COM_ADVERTISEMENTFACTORY_DASHBOARD_PAGE_TITLE'));

        $this->pending = $this->get('Pending');
        $this->payments = $this->get('Payments');

        parent::display($tpl);
    }
}
