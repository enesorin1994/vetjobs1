<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<div>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
        <div class="width-50 fltlft">
            <fieldset class="adminform">
                <legend><?php echo JText::_('COM_ADVERTISEMENTFACTORY_DASHBOARD_PENDING_ADS'); ?></legend>
                <?php echo $this->loadTemplate('pending'); ?>
            </fieldset>
        </div>

        <div class="width-50 fltlft">
            <fieldset class="adminform">
                <legend><?php echo JText::_('COM_ADVERTISEMENTFACTORY_DASHBOARD_PAYMENTS'); ?></legend>
                <?php echo $this->loadTemplate('payments'); ?>
            </fieldset>
        </div>

        <div class="clr"></div>
    </div>
</div>
