<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<style>
    .test td {
        padding: 5px 10px;
    }

    .test tr.alternate td {
        background-color: #eeeeee;
    }

    .ok {
        color: green;
    }

    .manual-check {
        color: orange;
    }

    .error {
        color: red;
    }
</style>

<table style="width: 100%;" class="test" cellpadding="0" cellspacing="0">
    <?php if ($this->payments): ?>
        <tfoot>
        <tr>
            <td colspan="5" style="border-top: 1px dotted #aaaaaa; padding-top: 20px;">
                <a href="index.php?option=com_advertisementfactory&view=payments"><big><?php echo JText::_('COM_ADVERTISEMENTFACTORY_DASHBOARD_VIEW_ALL_PAYMENTS'); ?></big></a>
            </td>
        </tr>
        </tfoot>
    <?php endif; ?>

    <tbody>
    <?php foreach ($this->payments as $i => $payment): ?>
        <tr class="<?php echo $i % 2 ? '' : 'alternate'; ?>">
            <td>
                <a href="index.php?option=com_advertisementfactory&task=payment.edit&id=<?php echo (int)$payment->id; ?>">
                    <?php echo $payment->username; ?>
                </a>
            </td>

            <td style="width: 70px; text-align: center;">
                <?php echo $payment->amount; ?><?php echo $payment->currency; ?>
            </td>

            <td style="width: 70px;"><?php echo ucfirst($payment->gateway); ?></td>

            <td style="width: 70px; text-align: center;">
                <?php echo $payment->received_at; ?>
            </td>

            <td style="width: 50px; text-align: center; font-weight: bold;" class="<?php echo $payment->status; ?>">
                <?php echo $payment->status; ?>
            </td>
        </tr>
    <?php endforeach; ?>

    <?php if (!$this->payments): ?>
        <tr>
            <td colspan="5"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_DASHBOARD_NO_PAYMENTS_FOUND'); ?></td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
