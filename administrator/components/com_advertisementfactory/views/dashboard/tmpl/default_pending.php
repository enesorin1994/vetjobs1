<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<style>
    .test td {
        padding: 5px 10px;
    }

    .test tr.alternate td {
        background-color: #eeeeee;
    }
</style>

<form action="index.php?option=com_advertisementfactory&task=ads.dashboardpublish" method="POST">
    <table style="width: 100%;" class="test" cellpadding="0" cellspacing="0">

        <tfoot>
        <tr>
            <td colspan="5" style="border-top: 1px dotted #aaaaaa; padding-top: 20px;">
                <a href="index.php?option=com_advertisementfactory&view=ads"><big><?php echo JText::_('COM_ADVERTISEMENTFACTORY_DASHBOARD_VIEW_ALL_ADS'); ?></big></a>
            </td>
        </tr>
        </tfoot>

        <tbody>
        <?php foreach ($this->pending as $i => $pending): ?>
            <tr class="<?php echo $i % 2 ? '' : 'alternate'; ?>">
                <td>
                    <a href="index.php?option=com_advertisementfactory&task=ad.edit&id=<?php echo (int)$pending->id; ?>">
                        <?php echo $pending->name; ?>
                    </a>
                </td>

                <td style="width: 50px; text-align: center;">
                    <a href="<?php echo JRoute::_(JURI::root() . 'index.php?option=com_advertisementfactory&view=previsualize&ad_id=' . (int)$pending->id); ?>"
                       target="_blank">
                        <small><?php echo JText::_('COM_ADVERTISEMENTFACTORY_DASHBOARD_PENDING_AD_PREVIEW'); ?></small>
                    </a>
                </td>

                <td style="width: 50px; text-align: center;">
                    <button onclick="this.form.cid.value = '<?php echo (int)$pending->id; ?>'; this.form.submit;"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_DASHBOARD_PENDING_AD_APPROVE'); ?></button>
                </td>
            </tr>
        <?php endforeach; ?>

        <?php if (!$this->pending): ?>
            <tr>
                <td colspan="5"><?php echo JText::_('COM_ADVERTISEMENTFACTORY_DASHBOARD_NO_ADS_PENDING'); ?></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>

    <input type="hidden" name="cid[]" id="cid" value=""/>
    <?php echo JHtml::_('form.token'); ?>

</form>
