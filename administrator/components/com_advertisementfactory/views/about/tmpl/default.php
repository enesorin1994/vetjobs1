<?php

/**
-------------------------------------------------------------------------
advertisementfactory - Advertisement Factory 3.1.2
-------------------------------------------------------------------------
 * @author thePHPfactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thePHPfactory.com
 * Technical Support: Forum - http://www.thePHPfactory.com/forum/
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

?>

<style>
    .about-component fieldset {
        border: 1px dotted #cccccc;
        -moz-border-radius: 5px;
        padding: 10px 15px 15px 15px;
        background-color: #f6f6f6;
    }

    .about-component legend {
        font-size: 20px;
    }

    .about-component .about {
        font-weight: bold;
        padding-top: 10px;
    }

    .about-component .new-version {
        color: #ff0000;
        text-decoration: blink;
    }

    .about-component img {
        float: none;
    }

    .fb-like {
        float: left;
    }
</style>

<script type="text/javascript">
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook - jssdk'));
</script>

<div>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
        <div class="about-component">
            <fieldset>
                <legend><?php echo JText::_('COM_ADVERTISEMENTFACTORY_ABOUT_LATEST_VERSION_TITLE'); ?></legend>

                <table style="float: left; margin-right: 50px;">
                    <tr>
                        <td><?php echo JText::_('COM_ADVERTISEMENTFACTORY_ABOUT_YOUR_VERSION'); ?>:</td>
                        <td><b><?php echo $this->information->currentVersion->text; ?></b></td>
                    </tr>
                    <tr>
                        <td><?php echo JText::_('COM_ADVERTISEMENTFACTORY_ABOUT_LATEST_VERSION'); ?>:</td>
                        <td><b><?php echo $this->information->latestVersion->text; ?></b></td>
                    </tr>
                    <tr>
                        <td colspan="2"
                            class="about <?php echo $this->information->newVersion ? 'new-version' : ''; ?>">
                            <?php echo JText::_($this->information->newVersion ? 'COM_ADVERTISEMENTFACTORY_ABOUT_NEW_VERSION' : 'COM_ADVERTISEMENTFACTORY_ABOUT_NO_NEW_VERSION'); ?>
                        </td>
                    </tr>
                </table>

                <div class="fb-like" data-href="https://www.facebook.com/theFactoryJoomla" data-send="false"
                     data-layout="box_count" data-width="450" data-show-faces="false"></div>

                <div class="clr"></div>

                <div style="clear: both; padding-top: 20px;">
                    <?php echo $this->information->remoteManifest->versionhistory; ?>
                </div>
            </fieldset>

            <fieldset>
                <legend><?php echo JText::_('COM_ADVERTISEMENTFACTORY_ABOUT_SUPPORT_AND_UPDATES'); ?></legend>
                <?php echo $this->information->remoteManifest->downloadlink; ?>
            </fieldset>

            <fieldset>
                <legend><?php echo JText::_('COM_ADVERTISEMENTFACTORY_ABOUT_NEW_OTHER_PRODUCTS'); ?></legend>
                <?php echo (string)$this->information->remoteManifest->otherproducts; ?>
            </fieldset>

            <fieldset>
                <legend><?php echo JText::_('COM_ADVERTISEMENTFACTORY_ABOUT_ABOUT_FACTORY'); ?></legend>
                <?php echo (string)$this->information->remoteManifest->aboutfactory; ?>
            </fieldset>
        </div>
    </div>
</div>
