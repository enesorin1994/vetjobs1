<?php
	/**------------------------------------------------------------------------
    com_jobsfactory - Jobs Factory 1.8.0
    ------------------------------------------------------------------------
     * @author thePHPfactory
     * @copyright Copyright (C) 2014 SKEPSIS Consult SRL. All Rights Reserved.
     * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     * Websites: http://www.thePHPfactory.com
     * Technical Support: Forum - http://www.thePHPfactory.com/forum/
     * @package: Jobs
	-------------------------------------------------------------------------*/
	defined('_JEXEC') or die('Restricted access');
	
	class pkg_jobsfactoryInstallerScript
	{

		function postflight($route, $adapter)
		{
			$session = JFactory::getSession();

			echo $session->get('com_jobsfactory_install_msg');
			$session->set('com_jobsfactory_install_msg', null);

		}
	}
